<?php
if(!defined('ABSPATH')) {die('You are not allowed to call this page directly.');}
/*
Integration of bbPress into MemberPress
*/
class MeprBbPressCtrl extends MeprBaseCtrl {
  public function load_hooks() {
    //Used to hide forums & topics from direct access
    add_filter('bbp_get_forum_visibility', 'MeprBbPressCtrl::hide_forums', 11, 2);
    //Used to hide forums from non-moderators/admins
    add_filter('pre_get_posts', 'MeprBbPressCtrl::add_forum_exclusions');
    //We're only allowing blocking by forum
    add_filter('mepr-rules-cpts', 'MeprBbPressCtrl::filter_rules_cpts');
    //Don't override bbPress the_content - this is needed when using the forum shortcodes (Not sure if this is still needed???)
    add_filter('mepr-pre-run-rule-content', 'MeprBbPressCtrl::dont_block_the_content', 11, 3);
  }

  public static function hide_forums($status, $forum_id) {
    $post = get_post($forum_id);

    //Let moderators and keymasters see everything
    if(current_user_can('edit_others_topics')) { return $status; }

    if(!isset($post) || !MeprRule::is_locked($post)) { return $status; }

    //If we made it here, it's protected
    return 'hidden';
  }

  public static function add_forum_exclusions($query) {
    $to_hide = array();

    //Not grabbing forums? then we don't care
    if(!isset($query->query_vars['post_type']) || $query->query_vars['post_type'] != 'forum') { return; }

    //Prevent infinite loops since get_posts triggers pre_get_posts again
    if(isset($_REQUEST['mepr_bbp_query_loop'])) { return; }

    //Let moderators and keymasters see everything
    if(current_user_can('edit_others_topics')) { return; }

    $_REQUEST['mepr_bbp_query_loop'] = true;
    $all_forums = get_posts( array( 'post_type' => 'forum', 'numberposts' => -1 ) );
    unset($_REQUEST['mepr_bbp_query_loop']);

    if(!empty($all_forums)) {
      foreach($all_forums as $forum) {
        if(MeprRule::is_locked($forum)) {
          $to_hide[] = $forum->ID;
        }
      }
    }

    $query->query_vars['post__not_in'] = $to_hide;
  }

  public static function filter_rules_cpts($cpts) {
    unset($cpts['reply']);
    unset($cpts['topic']);

    return $cpts;
  }

  public static function dont_block_the_content($block, $current_post, $uri) {
    if(is_user_logged_in() && function_exists('is_bbpress') && is_bbpress()) { return false; }
    return $block;
  }
} //End class
