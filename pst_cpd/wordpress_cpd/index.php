<?php
// See errors
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

// PREVENT THE PAGE FROM BEING CACHED BY THE WEB BROWSER
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

// Start the session
if (!session_id())
    session_start();

// Load the workpress libraries
define('WP_USE_THEMES', false);
require_once("wp-load.php");

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'login';
switch ($action) {

    case 'logout' :
        check_admin_referer('log-out');

        $user = wp_get_current_user();

        wp_logout();

        if (!empty($_REQUEST['redirect_to'])) {
            $redirect_to = $requested_redirect_to = $_REQUEST['redirect_to'];
        } else {
            $redirect_to = 'wp-login.php?loggedout=true';
            $requested_redirect_to = '';
        }

        /**
         * Filters the log out redirect URL.
         *
         * @since 4.2.0
         *
         * @param string $redirect_to The redirect destination URL.
         * @param string $requested_redirect_to The requested redirect destination URL passed as a parameter.
         * @param WP_User $user The WP_User object for the user that's logging out.
         */
        $redirect_to = apply_filters('logout_redirect', $redirect_to, $requested_redirect_to, $user);
        wp_safe_redirect($redirect_to);
        exit();
}


// If the user is not logged in, go to the wordpress login page.
if (is_user_logged_in()) {
    $user = wp_get_current_user();
    echo "Logged in as " . $user->data->display_name;
    //echo '<pre>' . print_r($user, 1) . '</pre>';
    echo '<br/><a href="/tools">Advisory Selling Tools<a>';
    echo '<br/><a href="/?action=logout">Log Out<a>';


} else {
    require_once("wp-login-back.php");
}
?>
    