table sql:

RENAME TABLE  `pst_tools_cpd`.`tgd_condition` TO  `pst_tools_cpd`.`profession_condition` ;

ALTER TABLE  `profession_condition`
CHANGE  `tgd_c_id`  `profession_c_id` INT( 11 ) NOT NULL AUTO_INCREMENT ,
CHANGE  `tgd_c_gt_id`  `profession_c_gt_id` INT( 11 ) NOT NULL ,
CHANGE  `tgd_c_name`  `profession_c_name` VARCHAR( 30 ) NOT NULL ,
CHANGE  `tgd_c_description`  `profession_c_description` TEXT NOT NULL ,
CHANGE  `tgd_c_tooltip`  `profession_c_tooltip` TEXT NULL DEFAULT NULL ,
CHANGE  `tgd_c_type`  `profession_c_type` VARCHAR( 10 ) NOT NULL ,
CHANGE  `tgd_c_uom`  `profession_c_uom` VARCHAR( 10 ) NOT NULL ,
CHANGE  `tgd_c_prev_condition`  `profession_c_prev_condition` INT( 11 ) NULL DEFAULT NULL ;

RENAME TABLE  `pst_tools_cpd`.`tgd_multiplier` TO  `pst_tools_cpd`.`profession_multiplier` ;



ALTER TABLE  `profession_multiplier`
CHANGE  `tgd_m_id`  `profession_m_id` INT( 11 ) NOT NULL AUTO_INCREMENT ,
CHANGE  `tgd_m_gt_id`  `profession_m_gt_id` INT( 11 ) NOT NULL ,
CHANGE  `tgd_m_order`  `profession_m_order` INT( 11 ) NOT NULL ,
CHANGE  `tgd_m_name`  `profession_m_name` VARCHAR( 50 ) NOT NULL ,
CHANGE  `tgd_m_description`  `profession_m_description` TEXT NOT NULL ,
CHANGE  `tgd_m_type`  `profession_m_type` VARCHAR( 10 ) NOT NULL DEFAULT  'dollar',
CHANGE  `tgd_m_operation`  `profession_m_operation` VARCHAR( 5 ) NOT NULL DEFAULT  'mult',
CHANGE  `tgd_m_propagate`  `profession_m_propagate` VARCHAR( 5 ) NOT NULL DEFAULT  'YES';

CREATE TABLE `industry_profession` (
 `profession_id` int(11) NOT NULL AUTO_INCREMENT,
 `profession_industry_id` int(11) NOT NULL COMMENT 'Foreign key reference to industry.',
 `profession_name` varchar(64) NOT NULL COMMENT 'Name of the profession',
 `profession_description` text NOT NULL COMMENT 'Description of the profession ',
 PRIMARY KEY (`profession_id`),
 UNIQUE KEY `profession_id` (`profession_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1

================================
v2

ALTER TABLE  `profession_condition` CHANGE  `profession_c_gt_id`  `profession_c_p_id` INT( 11 ) NOT NULL ;


ALTER TABLE  `profession_condition` DROP FOREIGN KEY  `profession_condition_ibfk_1` ;

ALTER TABLE  `profession_condition` ADD CONSTRAINT  `profession_condition_ibfk_1` FOREIGN KEY (  `profession_c_p_id` ) REFERENCES  `pst_cpd`.`industry_profession` (
`profession_id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `profession_multiplier` CHANGE  `profession_m_gt_id`  `profession_m_p_id` INT( 11 ) NOT NULL ;

ALTER TABLE  `profession_multiplier` DROP FOREIGN KEY  `profession_multiplier_ibfk_1` ;

ALTER TABLE  `profession_multiplier` ADD CONSTRAINT  `profession_multiplier_ibfk_1` FOREIGN KEY (  `profession_m_p_id` ) REFERENCES  `pst_cpd`.`industry_profession` (
`profession_id`
) ON DELETE CASCADE ON UPDATE CASCADE ;


ALTER TABLE  `industry_profession` ADD CONSTRAINT  `profession_industry_ibfk_1` FOREIGN KEY (  `profession_industry_id` ) REFERENCES  `pst_cpd`.`industry` (
 `industry_id`
) ON DELETE CASCADE ON UPDATE CASCADE

=================
CPD:

CREATE TABLE `cpd` (
 `cpd_id` int(11) NOT NULL AUTO_INCREMENT,
 `cpd_user_id` int(11) NOT NULL,
 `cpd_tgd_id` int(11) NOT NULL,
 `cpd_name` varchar(50) NOT NULL,
 `cpd_description` text NOT NULL,
 `cpd_complete_name` varchar(50) NOT NULL DEFAULT 'Deals Actual',
 `cpd_complete_desc` text NOT NULL,
 `cpd_repeat_name` varchar(50) NOT NULL DEFAULT 'Repeat',
 `cpd_repeat_desc` text NOT NULL,
 `cpd_contract_name` varchar(50) NOT NULL DEFAULT 'In Contract',
 `cpd_contract_desc` text NOT NULL,
 `cpd_deposit_name` varchar(50) NOT NULL DEFAULT 'Hard',
 `cpd_deposit_desc` text NOT NULL,
 `cpd_listed_name` varchar(50) NOT NULL DEFAULT 'Deal Listed',
 `cpd_listed_desc` text NOT NULL,
 `cpd_offer_name` varchar(50) NOT NULL DEFAULT 'Offers',
 `cpd_offer_desc` text NOT NULL,
 `cpd_prop_req_name` varchar(50) NOT NULL DEFAULT 'Proposal Requested',
 `cpd_prop_req_desc` text NOT NULL,
 `cpd_prop_pres_name` varchar(50) NOT NULL DEFAULT 'Proposal Presented',
 `cpd_prop_pres_desc` text NOT NULL,
 `cpd_meeting_name` varchar(50) NOT NULL DEFAULT 'Meeting',
 `cpd_meeting_desc` text NOT NULL,
 `cpd_tour_name` varchar(50) NOT NULL DEFAULT 'Tour',
 `cpd_tour_desc` text NOT NULL,
 `cpd_invited_name` varchar(50) NOT NULL DEFAULT 'Invited Maybe',
 `cpd_invited_desc` text NOT NULL,
 `cpd_declined_name` varchar(50) NOT NULL DEFAULT 'Invited Said No',
 `cpd_declined_desc` text NOT NULL,
 `cpd_created` datetime NOT NULL,
 `cpd_updated` datetime NOT NULL,
 PRIMARY KEY (`cpd_id`),
 UNIQUE KEY `cpd_id` (`cpd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

============
DROP TABLE cpd_user_config;
DROP TABLE cpd_user_config_column;
------

deal table:
CREATE TABLE `deal` (
 `deal_id` int(11) NOT NULL AUTO_INCREMENT,
 `deal_user_id` int(11) NOT NULL,
 `deal_cpd_id` int(11) NOT NULL,
 `deal_name` varchar(50) NOT NULL,
 `deal_short_name` varchar(10) NOT NULL,
 `deal_clu` varchar(5) NOT NULL,
 `deal_rwt` varchar(5) NOT NULL,
 `deal_lead` varchar(5) DEFAULT NULL,
 `deal_gen` varchar(5) DEFAULT NULL,
 `deal_product` varchar(50) NOT NULL,
 `deal_short_product` varchar(10) NOT NULL,
 `deal_amount` decimal(10,0) NOT NULL,
 `deal_created` datetime NOT NULL,
 `deal_updated` datetime NOT NULL,
 PRIMARY KEY (`deal_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE  `deal` ADD CONSTRAINT  `fk_deal_cpd` FOREIGN KEY (  `deal_cpd_id` ) REFERENCES  `pst_cpd`.`cpd` (
`cpd_id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

-----
deal_status

CREATE TABLE `deal_status` (
 `ds_id` int(11) NOT NULL AUTO_INCREMENT,
 `ds_deal_id` int(11) NOT NULL,
 `ds_status` varchar(10) NOT NULL,
 `ds_md` varchar(10) NOT NULL,
 `ds_datetime` datetime NOT NULL,
 PRIMARY KEY (`ds_id`),
 KEY `fk_deal_id` (`ds_deal_id`),
 CONSTRAINT `fk_deal_id` FOREIGN KEY (`ds_deal_id`) REFERENCES `deal` (`deal_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1
---------

deal_promo

CREATE TABLE `deal_promo` (
 `dp_id` int(11) NOT NULL AUTO_INCREMENT,
 `dp_deal_id` int(11) NOT NULL,
 `dp_datetime` datetime NOT NULL,
 `dp_name` varchar(20) NOT NULL,
 PRIMARY KEY (`dp_id`),
 KEY `fk_deal_id` (`dp_deal_id`),
 CONSTRAINT `fk_dp_deal_id` FOREIGN KEY (`dp_deal_id`) REFERENCES `deal` (`deal_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1

----------------

deal_update

CREATE TABLE `deal_update` (
 `du_id` int(11) NOT NULL AUTO_INCREMENT,
 `du_deal_id` int(11) NOT NULL,
 `du_datetime` datetime NOT NULL,
 `du_name` varchar(20) NOT NULL,
 PRIMARY KEY (`du_id`),
 KEY `fk_deal_update_id` (`du_deal_id`),
 CONSTRAINT `fk_deal_update_id` FOREIGN KEY (`du_deal_id`) REFERENCES `deal` (`deal_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1

----------------

code table:

INSERT INTO `pst_cpd`.`code_type` (`ct_id`, `ct_abbrev`, `ct_name`, `ct_description`) VALUES (NULL, 'ds', 'Deal Status', 'Deal Status'), (NULL, 'clu', 'Certainty', 'Level of certainty');

INSERT INTO `pst_cpd`.`code_type` (`ct_id`, `ct_abbrev`, `ct_name`, `ct_description`) VALUES (NULL, 'rwt', 'Client Type', 'Client Size or Importance');

INSERT INTO `pst_cpd`.`code` (`code_id`, `code_type_id`, `code_abbrev`, `code_name`, `code_description`) VALUES (NULL, '11', 'complete', 'Complete', 'Completed Deal'), (NULL, '11', 'repeat', 'Repeat', 'Repeat Opportunity');

INSERT INTO `pst_cpd`.`code` (`code_id`, `code_type_id`, `code_abbrev`, `code_name`, `code_description`) VALUES (NULL, '11', 'contract', 'In Contract', 'Deal in Contract
'), (NULL, '11', 'deposit', 'Deposit Received', 'Deposit Received
'), (NULL, '11', 'listed', 'Listed', 'Deal Listed');

INSERT INTO `pst_cpd`.`code` (`code_id`, `code_type_id`, `code_abbrev`, `code_name`, `code_description`)
VALUES (NULL, '11', 'offer', 'Offer Submitted', 'Offer Submitted'),
(NULL, '11', 'requested', 'Proposal Requested', 'Proposal Requested'),
(NULL, '11', 'presented', 'Proposal Presented', 'Proposal Presented'),
(NULL, '11', 'meeting', 'Meeting Set', 'Meeting Scheduled'),
(NULL, '11', 'tour', 'Tour Set', 'Tour Scheduled'),
(NULL, '11', 'maybe', 'Invited Maybe', 'Invitation Made'),
(NULL, '11', 'no', 'Invited – Said No', 'Invitation Declined');

INSERT INTO `pst_cpd`.`code` (`code_id`, `code_type_id`, `code_abbrev`, `code_name`, `code_description`)
VALUES (NULL, '12', 'C', 'Certain', 'Certain to Move'),
(NULL, '12', 'L', 'Likely', 'Likely to Move'),
(NULL, '12', 'U', 'Uncertain', 'Uncertain to Move');

INSERT INTO `pst_cpd`.`code` (`code_id`, `code_type_id`, `code_abbrev`, `code_name`, `code_description`)
VALUES (NULL, '13', 'W', 'Wheat', 'Medium Sized Client'),
(NULL, '13', 'T', 'Tree', 'Major Client'),
(NULL, '13', 'R', 'Radish', 'Small Client');
===========================

5/4/18 - Insert Strategic Deal Status

INSERT INTO `pst_cpd`.`code_type` (`ct_id`, `ct_abbrev`, `ct_name`, `ct_description`)
VALUES (NULL, 'str_ds', 'Strategic Deal Status', 'Strategic Deal Status');

INSERT INTO `pst_cpd`.`code` (`code_id`, `code_type_id`, `code_abbrev`, `code_name`, `code_description`)
VALUES (NULL, '14', 'expl_ph', 'Exploratory Phone', 'A phone or web exploratory meeting has been held'),
(NULL, '14', 'expl_per', 'Exploratory In Person', 'An in person exploratory meeting has been held'),
(NULL, '14', 'pres_per', 'Presentation In Person', 'An in person presentation has been made'),
(NULL, '14', 'pres_ph', 'Presentation Phone', 'A phone or web presentation has been made'),
(NULL, '14', 'inv_per', 'Invited In Person', 'An in person contact has been made'),
(NULL, '14', 'inv_ph', 'Invited Phone', 'A phone or web contact has been made');

=========================

9/4/2018

ALTER TABLE `cpd` DROP `cpd_tgd_id`;
ALTER TABLE  `tgd_user_instance` ADD  `tgd_cpd_id` INT( 10 ) NULL AFTER  `tgd_ui_profession_id` ;
ALTER TABLE  `cpd` ADD  `cpd_profession_id` INT( 10 ) NULL AFTER  `cpd_user_id` ;
ALTER TABLE  `cpd` ADD  `cpd_year` YEAR( 4 ) NOT NULL AFTER  `cpd_profession_id` ;

====================

10/4/2018

ALTER TABLE  `cpd` CHANGE  `cpd_profession_id`  `cpd_profession_id` INT( 10 ) NOT NULL ;


===================
24/4/2018

CREATE TABLE `user_profession` (
 `up_id` int(11) NOT NULL AUTO_INCREMENT,
 `up_user_id` int(11) NOT NULL,
 `up_profession_id` int(11) NOT NULL,
 PRIMARY KEY (`up_id`),
 KEY `Industry_prof_fk` (`up_profession_id`),
 CONSTRAINT `Industry_prof_fk` FOREIGN KEY (`up_profession_id`) REFERENCES `industry_profession` (`profession_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1

========================
25-4-18

- Static profession_id and user_id is added to the user_profession table



ALTER TABLE `cpd`
ADD `cpd_expl_ph_name` VARCHAR(50) NOT NULL DEFAULT 'Explanation over Phone' AFTER `cpd_declined_desc`,
ADD `cpd_expl_ph_desc` TEXT NOT NULL AFTER `cpd_expl_ph_name`,
ADD `cpd_expl_per_name` VARCHAR(50) NOT NULL DEFAULT 'Explanation in Person' AFTER `cpd_expl_ph_desc`,
ADD `cpd_expl_per_desc` TEXT NOT NULL AFTER `cpd_expl_per_name`,
ADD `cpd_pres_per_name` VARCHAR(50) NOT NULL DEFAULT 'Presentation in Person' AFTER `cpd_expl_per_desc`,
ADD `cpd_pres_per_desc` TEXT NOT NULL AFTER `cpd_pres_per_name`,
ADD `cpd_pres_ph_name` VARCHAR(50) NOT NULL DEFAULT 'Presentation over Phone' AFTER `cpd_pres_per_desc`,
ADD `cpd_pres_ph_desc` TEXT NOT NULL AFTER `cpd_pres_ph_name`,
ADD `cpd_inv_per_name` VARCHAR(50) NOT NULL DEFAULT 'Invitation in Person' AFTER `cpd_pres_ph_desc`,
ADD `cpd_inv_per_desc` TEXT NOT NULL AFTER `cpd_inv_per_name`,
ADD `cpd_inv_ph_name` VARCHAR(50) NOT NULL DEFAULT 'Invitation over Phone' AFTER `cpd_inv_per_desc`,
ADD `cpd_inv_ph_desc` TEXT NOT NULL AFTER `cpd_inv_ph_name`;

- updating the cpd column name to match status code:
ALTER TABLE `cpd` CHANGE `cpd_prop_req_name` `cpd_requested_name` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Proposal Requested',
CHANGE `cpd_prop_req_desc` `cpd_requested_desc` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
CHANGE `cpd_prop_pres_name` `cpd_presented_name` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Proposal Presented',
CHANGE `cpd_prop_pres_desc` `cpd_presented_desc` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
CHANGE `cpd_invited_name` `cpd_maybe_name` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Invited Maybe',
CHANGE `cpd_invited_desc` `cpd_maybe_desc` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
CHANGE `cpd_declined_name` `cpd_no_name` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Invited Said No',
CHANGE `cpd_declined_desc` `cpd_no_desc` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

- inserting default column values to user_profession
INSERT INTO  `pst_cpd`.`user_profession` (
`up_id` ,
`up_user_id` ,
`up_profession_id`
)
VALUES (NULL ,  '1',  '1'), (NULL ,  '1',  '2');

=======================================
7-5-18

- active_profession column added to user_profession table, with static profession id
ALTER TABLE  `pst_cpd`.`user_profession`
ADD  `up_profession_active` INT( 1 )
NOT NULL DEFAULT  '0';


========================================
11-5-18

-Add table user_profession_objective
CREATE TABLE  `user_profession_objective` (
 `upo_id` INT( 11 ) NOT NULL AUTO_INCREMENT ,
 `upo_up_id` INT( 11 ) NOT NULL ,
 `upo_year` INT( 11 ) NOT NULL ,
 `upo_floor` INT( 11 ) NOT NULL ,
 `upo_target` INT( 11 ) NOT NULL ,
 `upo_game` INT( 11 ) NOT NULL ,
PRIMARY KEY (  `upo_id` )
);

-Add foreign key to the table
ALTER TABLE  `user_profession_objective`
ADD FOREIGN KEY (  `upo_up_id` )
REFERENCES  `pst_cpd`.`user_profession` (
`up_id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

=========================================
23-05-18

-Add unique constraint to the user_profession_objective for a profession and year
ALTER TABLE `user_profession_objective`
ADD UNIQUE `unique_user_objective`
(`upo_year`, `upo_up_id`);

==========================================
29-05-2018

-Add unique constraint to the user_profession for a profession_id and user_id
ALTER TABLE  `user_profession`
ADD UNIQUE  `unique_user_profession`
(`up_user_id` ,`up_profession_id`);

==========================================
06-06-2018

-Adding table for Quarterly objective.

CREATE TABLE IF NOT EXISTS `user_profession_quarter_objective` (
  `upqo_id` int(11) NOT NULL AUTO_INCREMENT,
  `upqo_upo_id` int(11) NOT NULL,
  `upqo_quarter` int(11) NOT NULL,
  `upqo_floor` int(11) NOT NULL,
  `upqo_target` int(11) NOT NULL,
  `upqo_game` int(11) NOT NULL,
  PRIMARY KEY (`upqo_id`),
  KEY `upqo_upo_id` (`upqo_upo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-Adding foreign key to the table.

ALTER TABLE `user_profession_quarter_objective`
  ADD CONSTRAINT `fk_upqo_upo_id` FOREIGN KEY (`upqo_upo_id`) REFERENCES `user_profession_objective` (`upo_id`);

============================================

11-06-2018

--
-- Table structure for table `tsl`
--

CREATE TABLE IF NOT EXISTS `tsl` (
  `tsl_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsl_user_id` int(11) NOT NULL,
  `tsl_name` varchar(100) NOT NULL,
  `tsl_description` text NOT NULL,
  `tsl_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tsl_modified` datetime NOT NULL,
  `tsl_cpd_id` int(11) DEFAULT NULL,
  `tsl_profession_id` int(11) NOT NULL,
  PRIMARY KEY (`tsl_id`),
  KEY `tsl_user_id` (`tsl_user_id`),
  KEY `tsl_profession_id` (`tsl_profession_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_agent`
--

CREATE TABLE IF NOT EXISTS `tsl_agent` (
  `tsl_agent_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsl_agent_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `tsl_agent_user_id` int(11) NOT NULL,
  PRIMARY KEY (`tsl_agent_id`),
  KEY `agent_user` (`tsl_agent_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=29 ;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_assignee`
--

CREATE TABLE IF NOT EXISTS `tsl_assignee` (
  `tsl_assignee_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsl_assignee_name` varchar(100) NOT NULL,
  `tsl_assignee_user_id` int(11) NOT NULL,
  PRIMARY KEY (`tsl_assignee_id`),
  KEY `tsl_assignee_user_id` (`tsl_assignee_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_attachment`
--

CREATE TABLE IF NOT EXISTS `tsl_attachment` (
  `ta_id` int(11) NOT NULL AUTO_INCREMENT,
  `ta_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `ta_actual_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `ta_user_id` int(11) NOT NULL DEFAULT '0',
  `ta_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `ts_created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ta_id`),
  KEY `ta_user` (`ta_user_id`),
  KEY `ta_type` (`ta_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=71 ;

-- --------------------------------------------------------

============================================

20-06-2018
--
-- Table structure for table `tsl_attachment_used`
--

CREATE TABLE IF NOT EXISTS `tsl_attachment_used` (
  `tau_id` int(11) NOT NULL AUTO_INCREMENT,
  `tau_user_id` int(11) NOT NULL,
  `tau_attachment_id` int(11) NOT NULL,
  `tau_used_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tau_id`),
  KEY `tua_user` (`tau_user_id`),
  KEY `tua_attachment` (`tau_attachment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Constraints for table `tsl_attachment_used`
--
ALTER TABLE `tsl_attachment_used`
  ADD CONSTRAINT `fk_tua_attachment_id` FOREIGN KEY (`tau_attachment_id`) REFERENCES `tsl_attachment` (`ta_id`),
  ADD CONSTRAINT `fk_tua_user_id` FOREIGN KEY (`tau_user_id`) REFERENCES `user` (`user_id`);
-- --------------------------------------------------------

--
-- Table structure for table `tsl_caller`
--

CREATE TABLE IF NOT EXISTS `tsl_caller` (
  `tsl_caller_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsl_caller_name` varchar(20) NOT NULL,
  `tsl_caller_user_id` int(11) NOT NULL,
  PRIMARY KEY (`tsl_caller_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_client`
--

CREATE TABLE IF NOT EXISTS `tsl_client` (
  `tc_id` int(11) NOT NULL AUTO_INCREMENT,
  `tc_agent_id` int(11) NOT NULL,
  `tc_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `tc_phone` varchar(20) COLLATE utf8_bin NOT NULL,
  `tc_email` varchar(100) COLLATE utf8_bin NOT NULL,
  `tc_valid_phone` varchar(3) COLLATE utf8_bin NOT NULL,
  `tc_user_id` int(11) NOT NULL,
  `tc_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tc_cb` tinyint(1) DEFAULT NULL,
  `tc_cb_when` datetime DEFAULT NULL,
  `tc_cnv` tinyint(1) DEFAULT NULL,
  `tc_notes` text COLLATE utf8_bin,
  `tc_result` varchar(11) COLLATE utf8_bin DEFAULT NULL,
  `tc_action` text COLLATE utf8_bin,
  `tc_action_when` datetime DEFAULT NULL,
  `tc_assignee` int(11) DEFAULT NULL,
  `tc_caller` int(11) DEFAULT NULL,
  `tc_last_call` datetime DEFAULT NULL,
  PRIMARY KEY (`tc_id`),
  KEY `client_agent` (`tc_agent_id`),
  KEY `client_user` (`tc_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_client_criteria`
--

CREATE TABLE IF NOT EXISTS `tsl_client_criteria` (
  `tccrit_id` int(11) NOT NULL AUTO_INCREMENT,
  `tccrit_client_id` int(11) NOT NULL,
  `tccrit_criteria_id` int(11) NOT NULL,
  PRIMARY KEY (`tccrit_id`),
  KEY `tccrit_client` (`tccrit_client_id`),
  KEY `tccrit_criteria` (`tccrit_criteria_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for table `tsl_client_criteria`
--
ALTER TABLE `tsl_client_criteria`
  ADD CONSTRAINT `fk_tccrit_client_id` FOREIGN KEY (`tccrit_client_id`) REFERENCES `tsl_client` (`tc_id`),
  ADD CONSTRAINT `fk_tccrit_criteria_id` FOREIGN KEY (`tccrit_criteria_id`) REFERENCES `tsl_criteria` (`tsl_criteria_id`);

-- --------------------------------------------------------

--
-- Table structure for table `tsl_criteria`
--

CREATE TABLE IF NOT EXISTS `tsl_criteria` (
  `tsl_criteria_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsl_criteria_category_id` int(11) NOT NULL,
  `tsl_criteria_name` varchar(100) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`tsl_criteria_id`),
  KEY `criteria_category` (`tsl_criteria_category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Constraints for table `tsl_criteria`
--
ALTER TABLE `tsl_criteria`
  ADD CONSTRAINT `fk_criteria_category_id` FOREIGN KEY (`tsl_criteria_category_id`) REFERENCES `tsl_criteria_category` (`tcc_id`) ON DELETE CASCADE ON UPDATE CASCADE;
-- --------------------------------------------------------

--
-- Table structure for table `tsl_criteria_category`
--

CREATE TABLE IF NOT EXISTS `tsl_criteria_category` (
  `tcc_id` int(11) NOT NULL AUTO_INCREMENT,
  `tcc_up_id` int(11) NOT NULL,
  `tcc_name` varchar(100) NOT NULL,
  PRIMARY KEY (`tcc_id`),
  KEY `category_profession` (`tcc_up_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for table `tsl_criteria_category`
--
ALTER TABLE `tsl_criteria_category`
  ADD CONSTRAINT `fk_category_profession` FOREIGN KEY (`tcc_up_id`) REFERENCES `user_profession` (`up_id`);

-- --------------------------------------------------------

============================================

26-06-2018
--
-- Table structure for table `tsl_email`
--

CREATE TABLE IF NOT EXISTS `tsl_email` (
  `te_id` int(11) NOT NULL AUTO_INCREMENT,
  `te_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `te_outline_id` int(11) NOT NULL,
  PRIMARY KEY (`te_id`),
  KEY `te_outline_id` (`te_outline_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Constraints for table `tsl_email`
--
ALTER TABLE `tsl_email`
  ADD CONSTRAINT `fk_email_list_outline` FOREIGN KEY (`te_outline_id`) REFERENCES `tsl_email_outline` (`teo_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_email_answer`
--

CREATE TABLE IF NOT EXISTS `tsl_email_answer` (
  `tea_id` int(11) NOT NULL AUTO_INCREMENT,
  `tea_question_id` int(11) NOT NULL,
  `tea_client_id` int(11) NOT NULL,
  `tea_email_id` int(11) NOT NULL,
  `tea_content` text NOT NULL,
  PRIMARY KEY (`tea_id`),
  KEY `tea_question_id` (`tea_question_id`,`tea_client_id`,`tea_email_id`),
  KEY `tea_client_id` (`tea_client_id`),
  KEY `tea_survey_id` (`tea_email_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for table `tsl_email_answer`
--
ALTER TABLE `tsl_email_answer`
  ADD CONSTRAINT `fk_ans_email` FOREIGN KEY (`tea_email_id`) REFERENCES `tsl_email` (`te_id`) ON DELETE CASCADE ON UPDATE CASCADE

-- --------------------------------------------------------

--
-- Table structure for table `tsl_email_default`
--

CREATE TABLE IF NOT EXISTS `tsl_email_default` (
  `ted_id` int(11) NOT NULL AUTO_INCREMENT,
  `ted_type` varchar(20) NOT NULL,
  `ted_content` text NOT NULL,
  PRIMARY KEY (`ted_id`),
  KEY `ted_type` (`ted_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_email_outline`
--

CREATE TABLE IF NOT EXISTS `tsl_email_outline` (
  `teo_id` int(11) NOT NULL AUTO_INCREMENT,
  `teo_user_id` int(11) NOT NULL,
  `teo_content` text NOT NULL,
  `teo_version` int(11) NOT NULL,
  `teo_purpose` varchar(10) NOT NULL,
  `teo_event` varchar(10) NOT NULL,
  `teo_type` varchar(10) NOT NULL,
  PRIMARY KEY (`teo_id`),
  KEY `teo_user_id` (`teo_user_id`),
  KEY `teo_purpose` (`teo_purpose`),
  KEY `teo_event` (`teo_event`),
  KEY `teo_type` (`teo_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_email_used`
--

CREATE TABLE IF NOT EXISTS `tsl_email_used` (
  `teu_id` int(11) NOT NULL AUTO_INCREMENT,
  `teu_email_id` int(11) NOT NULL,
  `teu_user_id` int(11) NOT NULL,
  `teu_used_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`teu_id`),
  KEY `teu_email` (`teu_email_id`),
  KEY `teu_user` (`teu_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;


--
-- Constraints for table `tsl_email_used`
--
ALTER TABLE `tsl_email_used`
  ADD CONSTRAINT `fk_teu_email_id` FOREIGN KEY (`teu_email_id`) REFERENCES `tsl_email` (`te_id`),
  ADD CONSTRAINT `fk_teu_user_id` FOREIGN KEY (`teu_user_id`) REFERENCES `user` (`user_id`);


-- --------------------------------------------------------

--
-- Table structure for table `tsl_list`
--

CREATE TABLE IF NOT EXISTS `tsl_list` (
  `tsl_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsl_name` varchar(64) NOT NULL,
  `tsl_user_id` int(11) NOT NULL,
  `tsl_prepared_by` varchar(64) NOT NULL,
  `tsl_criteria` text NOT NULL,
  `tsl_date` date NOT NULL,
  PRIMARY KEY (`tsl_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_member`
--

CREATE TABLE IF NOT EXISTS `tsl_member` (
  `tsl_memeber_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsl_member_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `tsl_member_user_id` int(11) NOT NULL,
  `tsl_member_type` int(11) NOT NULL,
  PRIMARY KEY (`tsl_memeber_id`),
  KEY `tsl_member_user` (`tsl_member_user_id`),
  KEY `tsl_memeber_code` (`tsl_member_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Constraints for table `tsl_member`
--
ALTER TABLE `tsl_member`
  ADD CONSTRAINT `fk_tsl_member_code_id` FOREIGN KEY (`tsl_member_type`) REFERENCES `code` (`code_id`),
  ADD CONSTRAINT `fk_tsl_memeber_user_id` FOREIGN KEY (`tsl_member_user_id`) REFERENCES `user` (`user_id`);


-- --------------------------------------------------------
============================================

6-07-2018
--
-- Table structure for table `tsl_message`
--

CREATE TABLE IF NOT EXISTS `tsl_message` (
  `tm_id` int(11) NOT NULL AUTO_INCREMENT,
  `tm_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `tm_content` text COLLATE utf8_bin NOT NULL,
  `tm_tms_id` int(11) NOT NULL,
  PRIMARY KEY (`tm_id`),
  KEY `tm_tms_id` (`tm_tms_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Constraints for table `tsl_message`
--
ALTER TABLE `tsl_message`
  ADD CONSTRAINT `fk_message_sequence` FOREIGN KEY (`tm_tms_id`) REFERENCES `tsl_message_sequence` (`tms_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_message_sequence`
--

CREATE TABLE IF NOT EXISTS `tsl_message_sequence` (
  `tms_id` int(11) NOT NULL AUTO_INCREMENT,
  `tms_name` varchar(100) NOT NULL,
  `tms_user_id` int(11) NOT NULL,
  `tms_type` varchar(20) NOT NULL DEFAULT 'defined',
  `tms_rwt` varchar(5) NOT NULL,
  `tms_tsp` varchar(5) NOT NULL,
  `tms_sau` varchar(5) NOT NULL,
  PRIMARY KEY (`tms_id`),
  KEY `tms_user_id` (`tms_user_id`),
  KEY `tms_rwt` (`tms_rwt`,`tms_tsp`,`tms_sau`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_notes`
--

CREATE TABLE IF NOT EXISTS `tsl_notes` (
  `tn_id` int(11) NOT NULL AUTO_INCREMENT,
  `tn_content` text NOT NULL,
  `tn_user_id` int(11) NOT NULL,
  PRIMARY KEY (`tn_id`),
  KEY `tn_user_id` (`tn_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_organisation`
--

CREATE TABLE IF NOT EXISTS `tsl_organisation` (
  `to_id` int(11) NOT NULL AUTO_INCREMENT,
  `to_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `to_description` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`to_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_promo`
--

CREATE TABLE IF NOT EXISTS `tsl_promo` (
  `tp_id` int(11) NOT NULL AUTO_INCREMENT,
  `tp_org_name` varchar(100) NOT NULL,
  `tp_rand_name` varchar(100) NOT NULL,
  `tp_outline_id` int(11) NOT NULL,
  PRIMARY KEY (`tp_id`),
  KEY `promo_outline` (`tp_outline_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for table `tsl_promo`
--
ALTER TABLE `tsl_promo`
  ADD CONSTRAINT `fk_promo_outline_id` FOREIGN KEY (`tp_outline_id`) REFERENCES `tsl_promo_outline` (`tpo_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- --------------------------------------------------------
============================================
14-07-2018
--
-- Table structure for table `tsl_promo_background`
--

CREATE TABLE IF NOT EXISTS `tsl_promo_background` (
  `tpb_id` int(11) NOT NULL AUTO_INCREMENT,
  `tpb_org_name` varchar(100) NOT NULL,
  `tpb_rand_name` varchar(200) NOT NULL,
  `tpb_extension` varchar(20) NOT NULL,
  `tpb_user_id` int(11) NOT NULL,
  PRIMARY KEY (`tpb_id`),
  KEY `tpb_user_id` (`tpb_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-- --------------------------------------------------------

--
-- Table structure for table `tsl_promo_outline`
--

CREATE TABLE IF NOT EXISTS `tsl_promo_outline` (
  `tpo_id` int(11) NOT NULL AUTO_INCREMENT,
  `tpo_user_id` int(11) NOT NULL,
  `tpo_content` text NOT NULL,
  `tpo_version` int(4) NOT NULL,
  `tpo_client_type` varchar(10) NOT NULL,
  `tpo_promo_format` varchar(10) NOT NULL,
  `tpo_promo_focus` varchar(10) NOT NULL,
  PRIMARY KEY (`tpo_id`),
  KEY `tpo_user_id` (`tpo_user_id`),
  KEY `tpo_client_type` (`tpo_client_type`,`tpo_promo_format`,`tpo_promo_focus`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_promo_question`
--

CREATE TABLE IF NOT EXISTS `tsl_promo_question` (
  `tpq_id` int(11) NOT NULL AUTO_INCREMENT,
  `tpq_content` text NOT NULL,
  `tpq_outline_id` int(11) NOT NULL,
  PRIMARY KEY (`tpq_id`),
  KEY `tpq_outline_id` (`tpq_outline_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------
============================================
30-07-2018
--
-- Table structure for table `tsl_script`
--

CREATE TABLE IF NOT EXISTS `tsl_script` (
  `ts_id` int(11) NOT NULL AUTO_INCREMENT,
  `ts_name_id` int(11) NOT NULL,
  `ts_version` int(11) NOT NULL,
  `ts_content` text COLLATE utf8_bin NOT NULL,
  `ts_created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ts_id`),
  KEY `ts_name_id` (`ts_name_id`),
  KEY `ts_version` (`ts_version`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Constraints for table `tsl_script`
--
ALTER TABLE `tsl_script`
  ADD CONSTRAINT `fk_ts_name_id` FOREIGN KEY (`ts_name_id`) REFERENCES `tsl_script_name` (`tsn_id`) ON DELETE CASCADE ON UPDATE CASCADE;


-- --------------------------------------------------------

--
-- Table structure for table `tsl_script_client`
--

CREATE TABLE IF NOT EXISTS `tsl_script_client` (
  `tsc_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsc_client_id` int(11) NOT NULL,
  `tsc_script_id` int(11) NOT NULL,
  PRIMARY KEY (`tsc_id`),
  KEY `tsc_client_id` (`tsc_client_id`),
  KEY `tsc_script_id` (`tsc_script_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for table `tsl_script_client`
--
ALTER TABLE `tsl_script_client`
  ADD CONSTRAINT `fk_tsc_script_id` FOREIGN KEY (`tsc_script_id`) REFERENCES `tsl_script` (`ts_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_script_default`
--

CREATE TABLE IF NOT EXISTS `tsl_script_default` (
  `tsl_script_default_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsl_script_default_type` varchar(20) NOT NULL,
  `tsl_script_default_content` text NOT NULL,
  PRIMARY KEY (`tsl_script_default_id`),
  KEY `tsl_script_default_type` (`tsl_script_default_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_script_name`
--

CREATE TABLE IF NOT EXISTS `tsl_script_name` (
  `tsn_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsn_user_id` int(11) NOT NULL,
  `tsn_name` varchar(100) NOT NULL,
  `tsn_created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tsn_id`),
  KEY `tsn_user_id` (`tsn_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_script_outline`
--

CREATE TABLE IF NOT EXISTS `tsl_script_outline` (
  `tsl_script_outline_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsl_script_outline_client_type` varchar(20) NOT NULL,
  `tsl_script_outline_campaign_type` varchar(20) NOT NULL,
  `tsl_script_outline_script_type` varchar(20) NOT NULL,
  `tsl_script_outline_content` text NOT NULL,
  `tsl_script_outline_version` int(11) NOT NULL,
  `tsl_script_user_id` int(11) NOT NULL,
  PRIMARY KEY (`tsl_script_outline_id`,`tsl_script_user_id`),
  KEY `tsl_script_outline_client_type` (`tsl_script_outline_client_type`,`tsl_script_outline_campaign_type`,`tsl_script_outline_script_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_script_perf_prof`
--

CREATE TABLE IF NOT EXISTS `tsl_script_perf_prof` (
  `tspp_id` int(11) NOT NULL AUTO_INCREMENT,
  `tspp_script_client_id` int(11) NOT NULL,
  `tspp_perf1` int(2) DEFAULT NULL,
  `tspp_perf2` int(2) DEFAULT NULL,
  `tspp_perf3` int(2) DEFAULT NULL,
  `tspp_perf3a` int(2) DEFAULT NULL,
  `tspp_perf3b` int(2) DEFAULT NULL,
  `tspp_perf3c` int(2) DEFAULT NULL,
  `tspp_perf3d` int(2) DEFAULT NULL,
  `tspp_perf3e` int(2) DEFAULT NULL,
  `tspp_perf4` int(2) DEFAULT NULL,
  `tspp_perf5` int(2) DEFAULT NULL,
  `tspp_prof1` int(2) DEFAULT NULL,
  `tspp_prof2` int(2) DEFAULT NULL,
  `tspp_prof3` int(2) DEFAULT NULL,
  `tspp_prof3a` int(2) DEFAULT NULL,
  `tspp_prof3b` int(2) DEFAULT NULL,
  `tspp_prof3c` int(2) DEFAULT NULL,
  `tspp_prof3d` int(2) DEFAULT NULL,
  `tspp_prof3e` int(2) DEFAULT NULL,
  `tspp_prof4` int(2) DEFAULT NULL,
  `tspp_prof5` int(2) DEFAULT NULL,
  `tspp_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tspp_id`),
  KEY `spp_script_id` (`tspp_script_client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for table `tsl_script_perf_prof`
--
ALTER TABLE `tsl_script_perf_prof`
  ADD CONSTRAINT `fk_scores_script_id` FOREIGN KEY (`tspp_script_client_id`) REFERENCES `tsl_script_client` (`tsc_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_script_question`
--

CREATE TABLE IF NOT EXISTS `tsl_script_question` (
  `tsl_script_question_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsl_script_question_content` text NOT NULL,
  `tsl_script_question_outline_id` int(11) NOT NULL,
  PRIMARY KEY (`tsl_script_question_id`),
  KEY `tsl_script_questin_outline_id` (`tsl_script_question_outline_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------
============================================
02-08-2018
--
-- Table structure for table `tsl_survey`
--

CREATE TABLE IF NOT EXISTS `tsl_survey` (
  `tsl_survey_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsl_survey_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `tsl_survey_outline_id` int(11) NOT NULL,
  PRIMARY KEY (`tsl_survey_id`),
  KEY `tsl_survey_name_id` (`tsl_survey_name`),
  KEY `tsl_survey_outline_id` (`tsl_survey_outline_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

--
-- Constraints for table `tsl_survey`
--
ALTER TABLE `tsl_survey`
  ADD CONSTRAINT `fk_outline_survey_id` FOREIGN KEY (`tsl_survey_outline_id`) REFERENCES `tsl_survey_outline` (`tso_id`) ON DELETE CASCADE;


-- --------------------------------------------------------

--
-- Table structure for table `tsl_survey_answer`
--

CREATE TABLE IF NOT EXISTS `tsl_survey_answer` (
  `ts_ans_id` int(11) NOT NULL AUTO_INCREMENT,
  `ts_ans_question_id` int(11) NOT NULL,
  `ts_ans_client_id` int(11) NOT NULL,
  `ts_ans_survey_id` int(11) NOT NULL,
  `ts_ans_content` text NOT NULL,
  PRIMARY KEY (`ts_ans_id`),
  KEY `ts_ans_survey_id` (`ts_ans_question_id`),
  KEY `tsl_ans_client_id` (`ts_ans_client_id`),
  KEY `ts_ans_survey_id_2` (`ts_ans_survey_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for table `tsl_survey_answer`
--
ALTER TABLE `tsl_survey_answer`
  ADD CONSTRAINT `fk_answer_survey_id` FOREIGN KEY (`ts_ans_survey_id`) REFERENCES `tsl_survey` (`tsl_survey_id`) ON DELETE CASCADE;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_survey_client`
--

CREATE TABLE IF NOT EXISTS `tsl_survey_client` (
  `tsc_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsc_client_id` int(11) NOT NULL,
  `tsc_token` varchar(200) NOT NULL,
  `tsc_survey_id` int(11) NOT NULL,
  `tsc_used` int(1) NOT NULL DEFAULT '0',
  `tsc_created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tsc_id`),
  KEY `tbc_client_id` (`tsc_client_id`),
  KEY `tbc_survey_id` (`tsc_survey_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_survey_default`
--

CREATE TABLE IF NOT EXISTS `tsl_survey_default` (
  `tsd_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsd_survey_type` varchar(20) NOT NULL,
  `tsd_content` text NOT NULL,
  PRIMARY KEY (`tsd_id`),
  KEY `tsd_survey_type` (`tsd_survey_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_survey_outline`
--

CREATE TABLE IF NOT EXISTS `tsl_survey_outline` (
  `tso_id` int(11) NOT NULL AUTO_INCREMENT,
  `tso_user_id` int(11) NOT NULL,
  `tso_content` text NOT NULL,
  `tso_version` int(100) NOT NULL,
  `tso_survey_type` varchar(100) NOT NULL,
  `tso_client_type` varchar(10) NOT NULL,
  `tso_survey_class` varchar(10) NOT NULL,
  PRIMARY KEY (`tso_id`),
  KEY `tso_user_id` (`tso_user_id`),
  KEY `tso_version` (`tso_version`),
  KEY `tso_client_type` (`tso_client_type`,`tso_survey_class`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tsl_survey_question`
--

CREATE TABLE IF NOT EXISTS `tsl_survey_question` (
  `tsq_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsq_content` text NOT NULL,
  `tsq_outline_id` int(11) NOT NULL,
  PRIMARY KEY (`tsq_id`),
  KEY `tbq_survey_id` (`tsq_outline_id`),
  KEY `tsq_survey_id` (`tsq_outline_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for table `tsl_survey_question`
--
ALTER TABLE `tsl_survey_question`
  ADD CONSTRAINT `fk_outline_question_id` FOREIGN KEY (`tsq_outline_id`) REFERENCES `tsl_survey_outline` (`tso_id`) ON DELETE CASCADE;


-- --------------------------------------------------------

--
-- Table structure for table `tsl_target_audience`
--

CREATE TABLE IF NOT EXISTS `tsl_target_audience` (
  `tta_id` int(11) NOT NULL AUTO_INCREMENT,
  `tta_name` varchar(100) NOT NULL,
  `tta_user_id` int(11) NOT NULL,
  PRIMARY KEY (`tta_id`),
  KEY `tta_user_id` (`tta_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

============================================
27-08-2018

--
-- Table structure for table `cpd_team`
--

CREATE TABLE IF NOT EXISTS `cpd_team` (
  `team_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `team_type_name` varchar(100) NOT NULL,
  PRIMARY KEY (`team_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

----------------------------------------------------------------------------

--
-- Table structure for table `cpd_team-member`
--

CREATE TABLE IF NOT EXISTS `cpd_team-member` (
  `ctm_id` int(11) NOT NULL AUTO_INCREMENT,
  `ctm_name` varchar(100) NOT NULL,
  `ctm_team_id` int(11) NOT NULL,
  PRIMARY KEY (`team_member_id`),
  KEY `team_member_team_id` (`team_member_team_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Add foreign key to CPD team member
--

ALTER TABLE  `cpd_team-member` ADD CONSTRAINT  `fk_team_memeber_id` FOREIGN KEY (  `team_member_team_id` ) REFERENCES `pst_cpd`.`cpd_team` (
`cpd_team_id`
) ON DELETE CASCADE ON UPDATE CASCADE ;;

------------------------------------------------------------------------

--
-- Table structure for table `cpd_acm_deal`
--

CREATE TABLE IF NOT EXISTS `cpd_acm_deal` (
  `cad_id` int(11) NOT NULL AUTO_INCREMENT,
  `cad_deal_id` int(11) NOT NULL,
  `cad_date` date NOT NULL,
  `cad_ceo` varchar(2) NOT NULL,
  `cad_action` text NOT NULL,
  `cad_whom` varchar(100) NOT NULL,
  `cad_when` datetime NOT NULL,
  `cad_acdf` varchar(2) NOT NULL,
  `cad_tag` int(11) NOT NULL,
  `cad_cat` varchar(2) NOT NULL,
  `cad_type` varchar(10) NOT NULL,
  PRIMARY KEY (`cad_id`),
  KEY `ctd_deal_id` (`cad_deal_id`),
  KEY `ctd_type` (`cad_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


--
-- Add foreign key to CPD team deal.
--
ALTER TABLE  `cpd_acm_deal` ADD CONSTRAINT  `cpd_team_deal_id` FOREIGN KEY (  `cad_team_id` ) REFERENCES  `pst_cpd`.`cpd_team` (

`cpd_team_id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `cpd_acm_deal` ADD CONSTRAINT  `cpd_acm_deal_id` FOREIGN KEY (  `ctd_deal_id` ) REFERENCES  `pst_cpd`.`cpd` (
`cpd_id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

--------------------------------------------------------------------------
24-09-2018

Updated table structure

--
-- Table structure for table `tsl_email_default`
--

CREATE TABLE IF NOT EXISTS `tsl_email_default` (
  `ted_id` int(11) NOT NULL AUTO_INCREMENT,
  `ted_type` varchar(20) NOT NULL,
  `ted_content` text NOT NULL,
  `ted_event` varchar(50) NOT NULL,
  PRIMARY KEY (`ted_id`),
  KEY `ted_type` (`ted_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--

--
-- Table structure for table `tsl_script_default`
--

CREATE TABLE IF NOT EXISTS `tsl_script_default` (
  `tsl_script_default_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsl_script_type` varchar(50) NOT NULL,
  `tsl_script_content` text NOT NULL,
  `tsl_script_origin` varchar(10) NOT NULL,
  `tsl_script_requester` varchar(50) NOT NULL,
  PRIMARY KEY (`tsl_script_default_id`),
  KEY `tsl_script_default_type` (`tsl_script_type`),
  KEY `tsl_script_requester` (`tsl_script_requester`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--

--
-- Table structure for table `tsl_script_outline`
--

CREATE TABLE IF NOT EXISTS `tsl_script_outline` (
  `tsl_script_outline_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsl_script_outline_script_type` varchar(50) NOT NULL,
  `tsl_script_outline_content` text NOT NULL,
  `tsl_script_outline_version` int(11) NOT NULL,
  `tsl_script_user_id` int(11) NOT NULL,
  PRIMARY KEY (`tsl_script_outline_id`,`tsl_script_user_id`),
  KEY `tsl_script_outline_client_type` (`tsl_script_outline_script_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

--

--
-- Table structure for table `cpd_additional_acm`
--

CREATE TABLE IF NOT EXISTS `cpd_additional_acm` (
  `caa_id` int(11) NOT NULL AUTO_INCREMENT,
  `caa_acm_id` int(11) NOT NULL,
  `caa_action` text,
  `caa_whom` varchar(100) DEFAULT NULL,
  `caa_when` datetime DEFAULT NULL,
  `caa_cmt` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`caa_id`),
  KEY `caa_acm_id` (`caa_acm_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;


--
-- Table structure for table `cpd_docs`
--

CREATE TABLE IF NOT EXISTS `cpd_docs` (
  `cd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cd_name` varchar(100) NOT NULL,
  `cd_extension` varchar(20) NOT NULL,
  `cd_random_name` varchar(100) NOT NULL,
  `cd_cpd_deal` int(11) NOT NULL,
  `cd_user_id` int(11) NOT NULL,
  `cd_type` varchar(10) NOT NULL,
  PRIMARY KEY (`cd_id`),
  KEY `cd_cpd_deal` (`cd_cpd_deal`),
  KEY `cd_user_id` (`cd_user_id`),
  KEY `cd_type` (`cd_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--

--
-- Update table TSL Agent.
--


CREATE TABLE IF NOT EXISTS `tsl_agent` (
  `tsl_agent_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsl_agent_name` varchar(100) COLLATE utf8_bin NOT NULL,
  `tsl_agent_tsl_id` int(11) NOT NULL,
  PRIMARY KEY (`tsl_agent_id`),
  KEY `tsl_agent_tsl_id` (`tsl_agent_tsl_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;


--
-- Update table criteria category
--
CREATE TABLE IF NOT EXISTS `tsl_criteria_category` (
  `tcc_id` int(11) NOT NULL AUTO_INCREMENT,
  `tcc_tsl_id` int(11) NOT NULL,
  `tcc_name` varchar(100) NOT NULL,
  PRIMARY KEY (`tcc_id`),
  KEY `tcc_tsl_id` (`tcc_tsl_id`),
  KEY `tcc_tsl_id_2` (`tcc_tsl_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


--
-- Update table `tsl_target_audience`
--

CREATE TABLE IF NOT EXISTS `tsl_target_audience` (
  `tta_id` int(11) NOT NULL AUTO_INCREMENT,
  `tta_name` varchar(100) NOT NULL,
  `tta_tsl_id` int(11) NOT NULL,
  PRIMARY KEY (`tta_id`),
  KEY `tta_user_id` (`tta_tsl_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Update Table structure for table `tsl_caller`
--

CREATE TABLE IF NOT EXISTS `tsl_caller` (
  `tsl_caller_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsl_caller_name` varchar(20) NOT NULL,
  `tsl_caller_tsl_id` int(11) NOT NULL,
  PRIMARY KEY (`tsl_caller_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


--
-- Update Table Structure for 'tsl_assignee'
--
CREATE TABLE IF NOT EXISTS `tsl_assignee` (
  `tsl_assignee_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsl_assignee_name` varchar(100) NOT NULL,
  `tsl_assignee_tsl_id` int(11) NOT NULL,
  PRIMARY KEY (`tsl_assignee_id`),
  KEY `tsl_assignee_user_id` (`tsl_assignee_tsl_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-------------------------------------------------------------------------------------------
01-10-2018

--
-- User Quarter Objective Table
--

CREATE TABLE IF NOT EXISTS `user_quarter_objective` (
  `uqo_id` int(11) NOT NULL AUTO_INCREMENT,
  `uqo_uo_id` int(11) NOT NULL,
  `uqo_quarter` int(11) NOT NULL,
  `uqo_floor` decimal(20,0) NOT NULL DEFAULT '0',
  `uqo_target` decimal(20,0) NOT NULL DEFAULT '0',
  `uqo_game` decimal(20,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uqo_id`),
  KEY `upqo_upo_id` (`uqo_uo_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


--
-- User Objective Table
--
CREATE TABLE IF NOT EXISTS `user_objective` (
  `uo_id` int(11) NOT NULL AUTO_INCREMENT,
  `uo_cpd_id` int(11) NOT NULL,
  `uo_year` int(11) NOT NULL,
  `uo_floor` decimal(20,0) NOT NULL DEFAULT '0',
  `uo_target` decimal(20,0) NOT NULL DEFAULT '0',
  `uo_game` decimal(20,0) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uo_id`),
  UNIQUE KEY `unique_user_objective` (`uo_year`,`uo_cpd_id`),
  KEY `upo_up_id` (`uo_cpd_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


--
-- Table structure for table `tsl_script_default`
--

CREATE TABLE IF NOT EXISTS `tsl_script_default` (
  `tsl_script_default_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsl_script_type` varchar(50) NOT NULL,
  `tsl_script_content` text NOT NULL,
  `tsl_script_origin` varchar(10) NOT NULL,
  `tsl_script_requester` varchar(50) NOT NULL,
  PRIMARY KEY (`tsl_script_default_id`),
  KEY `tsl_script_default_type` (`tsl_script_type`),
  KEY `tsl_script_requester` (`tsl_script_requester`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `tsl_script_default`
--

INSERT INTO `tsl_script_default` (`tsl_script_default_id`, `tsl_script_type`, `tsl_script_content`, `tsl_script_origin`, `tsl_script_requester`) VALUES
(1, 'Initial Contact', '<div>[[<strong>Initial Contact</strong>]]<strong><br></strong>\n</div>\n<div><strong>Business Development Call Outline<br></strong>\n</div>\n<div>\n    <br>A first contact may be a call, an impromptu meeting or an encounter at a business event.\n    <br><strong>(First Announce: Who are you? What do you do? Who do you do it for? &amp; Why you are calling.)</strong>\n    <br>[[<strong>Key Practice: Evoking Rapport</strong>]]\n    <br>{{<strong>Step 1: </strong>Opening }}\n    <br>Shut down the automatic “who knows best” battle with a statement that gives both agent and client permission to “not know” by expressing “I don’t know and you don’t something.”\n    <br>{{<strong>Step 2:</strong> Permission }}\n    <br>Ask to explore their intimate situation and consider some new ideas.\n    <br>{{<strong>Step 3:</strong> Interest }}\n    <br>Declare your interest in them and helping them.\n    <br>[[Embedded Practice: Exposing Problems ]]\n    <br>((<strong>Substep 1: Generate</strong>))<strong> Show interest by asking about them and/or your knowledge about them. </strong>\n    <br>((<strong>Substep 2: Introduce</strong>))<strong> Create an Interest Generator relevant to their situation and what they said </strong>\n    <br>((<strong>Substep 3: Explore </strong>))<strong> Ask a specific Open-Ended Question(s)until a Problem is exposed. </strong>\n    <br>(Step 2 &amp; 3 are interchangeable - first create an interest generator relevant to what the client said and then ask more specific questions to expose a problem or first expose the problem by asking more specific questions and then offer an interest generator that validates the problem and relates it to other client situations.)\n    <br>((<strong>Substep 4: Listen </strong>))<strong> Expose a problem or restate the problem as a Windows of Opportunity </strong>\n    <br>((<strong>Substep 5: Convert </strong>))<strong> Turn the window of opportunity into Reason to Meet. </strong>\n    <br>{{<strong>Step 4: Detachment/Offer </strong>}}<strong> </strong>Offer to take the time to talk with them about\n    <br>• the <strong>Problem</strong> exposed by the Interest Generator,\n    <br>• their <strong>Project</strong> – where they are and where they want to be in the future with your help,\n    <br>• and how you might also set up an Advisory <strong>Process</strong> to keep them out ahead of future changes.\n    <br>{{<strong>Step 5: Questions </strong>}}&nbsp;\n    <br>When would you like to meet?</div>', 'cpdv1', 'script'),
(2, 'Extended Initial Contact', '<div><strong>[[Extended Initial Contact ]]<br><br>Business Development Call &amp; Exploratory Meeting Outline<br><br></strong>An initial contact may be a call, an impromptu meeting or an encounter at a business event.<br>Greeting and Introduction&nbsp; comes first - who you are, what you do and who you do it for. Then proceed with this outline.<br><br><strong>[[Concept: Evoking rapport]]<br></strong>Draw out their opinions and interpretations of their situation.<br><br><strong>{{Step 1: Opening}} Declare that I don\\\\\\''t know &amp; you don\\\\\\''t know.<br>{{Step 2: Permission}} Ask permission to explore new ideas.<br>{{Step 3: Interest}} Declare your interest in them. <br></strong>[[Embedded Practice: Exposing a Problem]]<strong><br><br> ((Substep 1: Generate )) Show interest by asking about them and/or your knowledge about them. <br> ((Substep 2: Introduce )) Create an Interest Generator relevant to their situation and what they said <br> ((Substep 3: Explore )) Ask a specific Open-Ended Question(s)until a Problem is exposed. <br>(Step 2 &amp; 3 are interchangeable - first create an interest generator relevant to what the client said and then ask more specific questions to expose a problem or first expose the problem by asking more specific questions and then offer an interest generator that validates the problem and relates it to other client situations.) <br>((Substep 4: Listen )) Expose a problem or restate the problem as a Windows of Opportunity <br>((Substep 5: Convert )) Turn the window of opportunity into Reason to Meet. <br>{{Step 4: Detachment/Offer }} Detach and offer to look at their project with team.<br>{{Step 5: Questions}} Ask a question that draws out their story.<br><br>[[Context: Generating Projects]]<br><br>Crack their story open with fact based questions to their get accomplishments, what\\''s needed &amp; what\\\\\\''s possible.<br><br>{{Step 1: History}} Ask why-based questions to draw out more story.<br>{{Step 2: Current facts}} Ask fact based questions to draw out accomplishments.<br>{{Step 3: Accomplished}} Acknowledge what they have accomplished so far.<br></strong>(Then you may use a second question: What Challenges are you facing and how do plan do resolve them.)<strong><br>{{Step 4: Problems/Offer}} Explore what is needed to move them forward.<br></strong>(You&nbsp; may use a third question: Where do you see yourself headed in the future with this property.)<br><strong>{{Step 5: Project}} Articulate what might become possible for them in the future with your help.<br><br>[[Construct: Strategic Analysis]]<br></strong>Draw out factors, challenge &amp; implications and offer to show options, consequences &amp; your recommendation.<br><br><strong>{{Step 1: Factors}} Identify key factors affecting their situation</strong><br><strong>{{Step 2: Challenges}} Clarify the challenges these factors present.</strong><br><strong>{{Step 3: Implications}} Consider the future implications of theses challenges.</strong><br><strong>{{Step 4: Options}} Offer to return with the options &amp; consequences.</strong><br><strong>{{Step 5: Recommendations}} Offer to return with recommendations.</strong><br><br><strong>[[Content: Client Project Focus]]</strong><br>Show them how this future possibility can be achieved&nbsp; - show them the bridge you are offering.<br><strong>{{Step 1: Platform}} review company capabilities.<br>{{Step 2: People}} Review agent capabilities.<br>{{Step 3: Problems}} Review the situational forces they are facing.<br>{{Step 4: Process/Offer}} Review the attributes of the asset.<br>{{Step 5: Project}} Review the client project including possibility.<br><br>[[Closing: Forging Partnership]]<br></strong>Establish a partnership focused of gathering, preparing and producing a strategic analysis.<br><strong>{{Step 1: Risk}} Review the risks they are facing .</strong><br><strong>{{Step 2: Responsibility}} Review the responsibility that must be taken.<br>{{Step 3: Declaration}} Declare their capability and your commitment.<br>{{Step 4: Invitation}} Invite them to prepare for your presentatiion.<br>{{Step 5: Promise}} State the results they receive if they accept.</strong><br><br><br></div>', 'cpdv1', 'script'),
(3, 'Exploratory Meeting', '<div><strong>[[Exploratory Meeting Outline]]<br></strong>Sending out confirmation email that reaffirms the reason to meet and offer to help resolve their problem, explore their project and establish an advisory relationship and includes three compelling questions for the client to consider and will help the client better prepare to maximize the value of their time with the agent and therefore make it more likely that the meeting will happen and be productive.<br>Greetings and introduction comes first - who you are, what you do and who you do it for. Then proceed with this outline.<br><strong>[[Concept: Evoking rapport]]<br></strong>Draw out their opinions and interpretations of their situation.<br><strong>{{Step 1: Opening}} Declare that I don\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\''t know &amp; you don\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\''t know.<br>{{Step 2: Permission}} Ask permission to explore new ideas.<br>{{Step 3: Interest}} Declare your interest in them. <br>{{Step 4: Detachment/Offer}} Detach and offer to look at their project with them.<br>{{Step 5: Questions}} Ask a question that draws out their story.<br></strong>(For Questions, you may use the first email question: What have you accomplished so far with your asset.)<strong><br><br>[[Concept: Generating Projects]]<br></strong>Crack their story open with fact based questions to their get accomplishment, what\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\''s needed &amp; what\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\''s possible.<strong><br><br>{{Step 1: History}} Ask why-based questions to draw out more story.<br>{{Step 2: Current facts}} Ask fact based questions to draw out accomplishment.<br>{{Step 3: Accomplished}} Acknowledge what they have accomplished so far.</strong><br>(Then you may use the second email question: What challenges are you facing and how do plan to resolve them.)<br><strong>{{Step 4: Problems/Offer}} Explore what is needed to move them forward.<br></strong>(For Client Project, you may use the second email question: Where do you see yourself headed in future with this property.)<br><strong>{{Step 5:Project}} Articulate what might become possible for them.<br><br>[[Construct: Strategic Analysis]]<br></strong>Draw out factors, challenges &amp; implications and offers to show options, consequences &amp; your recommendation.<br><br><strong>{{Step 1: Factors}} Identify key factors affecting their situation.<br>{{Step 2: Challenges}} Clarify the challenges these factors present.<br>{{Step 3: Implications}} Consider the future implications of these challenges.<br>{{Step 4: Options}} Offer to return with the options &amp; consequences.<br>{{Step 5: Recommendations}} Offer to return with recommendations.<br><br>[[Content: Client Project Focus]]<br></strong>Show them how this future possibility can be achieved - show them the bridge you are offering.<br><strong>{{Step 1: Platform}} Review company capabilities.<br>{{Step 2: People}} Review Agent&nbsp; capabilities.<br>{{Step 3: Problems}} Review Situational forces they are facing.<br>{{Step 4: Process/Offer}} Review the attributes of the asset.<br>{{Step 5: Project}} Review the client project include possibilty.<br><br>[[Closing: Forging Partnership]]<br></strong>Establish a partnership focused of gathering, preparing and producing a strategic analysis.<br><strong>{{Step 1: Risk}} Review the risks they are facing.<br>{{Step 2: Responsibility}} Review the responsibility that must be taken.<br>{{Step 3: Declaration}} Declare their capability and your commitment.<br>{{Step 4: Invitation}} Invite them to prepare for your presentation.<br>{{Step 5: Promise}} State the results they receive if they accept.</strong></div>', 'cpdv1', 'script'),
(4, 'Full Presentation', '<div><strong>[[Full Presentation]]</strong></div><div>This is a systematic way of structuring proposal presentations that will produce more consistent results. If<br>applied precisely, it will cause a quantum leap in the ability to deliver presentations that produce results.<br>From here you can reverse engineer your phone calls and meeting to set-up more power at this event.<br><strong>[[Primary Practice: Building Presentations]]</strong><br><strong>{{Concept}}</strong>– What is the Purpose and Direction of the Presentation<br>[[Embedded Practice: Evoke Rapport]]</div><div><strong>((Substep 1: Opening))<br>((Substep 2: Permission))<br>((Substep 3: Interest))<br>((Substep 4: Detachment))<br>((Substep 5: Question))</strong></div><div><br><strong>{{Context}}</strong> – Why is this Presentation Important to Them?<br>[[Embedded Practice: Generating Projects]]<br><strong>((Substep 1: History))<br>((Substep 2: Current Facts))<br>((Substep 3: Accomplishment))<br>((Substep 4: Problems))<br>((Substep 5 :Project))<br>{{Construct}}</strong> – What will be covered? (Table of Contents – List of Topics – Plus Implications – Options -<br>Consequences of Choices – and Recommended Best Course of Action)<br><strong>{{Content}}</strong> – Start, Cover, and End each section of the contents using the 5 Step Building Presentations<br>construct inside of each section - like a hologram with the macro structure reccurring within each section.<br><strong>{{Closing}}</strong> – What was covered? (Table of Contents – List of Topics)<br>Review the Concept, Context and Construct. (When you review they remember.)<br>[[Embedded Practice: Forging Partnerships]]<strong><br>((Substep 1: Risks))<br>((Substep 2: Responsibility))<br>((Substep 3: Declaration))<br>((Substep 4: Invitation))<br>((Substep 5: Promise))</strong></div>', 'cpdv1', 'script'),
(5, 'Launch Meeting', '<div><strong>[[Exploratory Meeting Outline]]</strong><br><br>Sending out confirmation email that reaffirms the purpose of the launch meeting is to streamline the process of working together so that both agent and client understand how to maximise the value to the client of the process. It is important for clients to see a clear picture of how things will roll-out so that as the unexpected obstacles or unforeseen opportunities emerge they can be fully taken advantage of. It is also essential that the client understand how the agent will be working which may be significantly different from the client\\''s previous experience with other agents. Being on the same page will&nbsp; ensure that the deal process goes smoother and that there will be greater certainty of closing. This launch applies for both a conventional transaction process and a strategic advisory relationship going forward until a transaction event&nbsp; is triggered. Then a proposal presentation will be needed and a relaunch will be needed as well.<br><br><strong>[[Concept: Evoking rapport]]</strong><br>Draw out their opinions and interpretations of their situation.<br><br><strong>{{Step 1: Opening}}</strong> Declare that I don\\''t know &amp; you don\\''t know.<br><strong>{{Step 2: Permission}}</strong> Ask permission to explore new ideas.<br><strong>{{Step 3: Interest}}</strong> Declare your interest in them. <br><strong>{{Step 4: Detachment/Offer}}</strong> Detach and offer to look at their project with them.<br><strong>{{Step 5: Questions}}</strong> Ask a question that draws out their story.<br><br><strong>[[Context: Client Project Focus]]<br></strong>Revisit how the client\\\\\\''s future possibility can be achieved with your help - show them the bridge you are offering.<br><br><strong>{{Step 1: Platform}} </strong>Review Company Capabilities.<br><strong>{{Step 2: People}}&nbsp; </strong>Review agent capabilities.<br><strong>{{Step 3: Problems}} </strong>Review Situational forces they are facing.<br><strong>{{Step 4: Process/Offer}} </strong>Review the attributes of the asset.<br><strong>{{Step 5: Project}} </strong>Review the client project including the future possibility.<br><br><strong>[[Construct: Client Project Design]]<br></strong>Draw out&nbsp; the detailed specifics of how the process of achieving the client\\\\\\\\\\\\\\''s project will unfold. <br><br><strong>{{Step 1: Mission}} </strong>The mission or client project transition.<br><strong>{{Step 2: Objectives}} </strong>The Objective that will fulfill the mission. <br><strong>{{Step 3: Conditions}} </strong>The conditions required to take on these objectives.<br><strong>{{Step 4: Metrics}} </strong>The yardsticks that will be used to measure progress. <br><strong>{{Step 5: Milestones}} </strong>The project milestones that will be reached and by when.<br><br><strong>[[Content: Rules of Engagement]]<br></strong>Set in place the rules of communication and conduct that ensure the value to the client and reaching their project.<br><strong>{{Step 1: Players}} </strong>The decision makers and who will influence that decision.<br><strong>{{Step 2: Provisions}} </strong>Determine who will provide what to whom and when. <strong><br>{{Step 3: Stipulations}} </strong>Set the rules for conduct and communications.<br><strong>{{Step 4: Considerations}} </strong>Clarify all financial rewards and market recognition.<br><strong>{{Step 5: Consequences}} </strong>Layout the consequences of failing to follow these rules.<br><br><strong>[[Closing: Forging&nbsp; Partnership]]<br></strong>Establishing a partnership focused of gathering, preparing and producing a strategic analysis.<br><strong>{{Step 1: Risk}}</strong> Review the risks they are facing. <br><strong>{{Step 2: Responsibility}} </strong>Review the responsibility that must&nbsp; be taken. <br><strong>{{Step 3: Declaration}} </strong>Declare their capability and your commitment. <br><strong>{{Step 4: Invitation}} </strong>Invite them to prepare for your presentation. <br><strong>{{Step 5: Promise}} </strong>Promise the results they will receive if they accept.<br><br><br></div>', 'cpdv1', 'script'),
(6, 'Initial Contact', '<div>[[<strong>Initial Contact</strong>]]<strong><br></strong>\n</div>\n<div><strong>Business Development Call Outline<br></strong>\n</div>\n<div>\n    <br>A first contact may be a call, an impromptu meeting or an encounter at a business event.\n    <br><strong>(First Announce: Who are you? What do you do? Who do you do it for? &amp; Why you are calling.)</strong>\n    <br>[[<strong>Key Practice: Evoking Rapport</strong>]]\n    <br>{{<strong>Step 1: </strong>Opening }}\n    <br>Shut down the automatic “who knows best” battle with a statement that gives both agent and client permission to “not know” by expressing “I don’t know and you don’t something.”\n    <br>{{<strong>Step 2:</strong> Permission }}\n    <br>Ask to explore their intimate situation and consider some new ideas.\n    <br>{{<strong>Step 3:</strong> Interest }}\n    <br>Declare your interest in them and helping them.\n    <br>[[Embedded Practice: Exposing Problems ]]\n    <br>((<strong>Substep 1: Generate</strong>))<strong> Show interest by asking about them and/or your knowledge about them. </strong>\n    <br>((<strong>Substep 2: Introduce</strong>))<strong> Create an Interest Generator relevant to their situation and what they said </strong>\n    <br>((<strong>Substep 3: Explore </strong>))<strong> Ask a specific Open-Ended Question(s)until a Problem is exposed. </strong>\n    <br>(Step 2 &amp; 3 are interchangeable - first create an interest generator relevant to what the client said and then ask more specific questions to expose a problem or first expose the problem by asking more specific questions and then offer an interest generator that validates the problem and relates it to other client situations.)\n    <br>((<strong>Substep 4: Listen </strong>))<strong> Expose a problem or restate the problem as a Windows of Opportunity </strong>\n    <br>((<strong>Substep 5: Convert </strong>))<strong> Turn the window of opportunity into Reason to Meet. </strong>\n    <br>{{<strong>Step 4: Detachment/Offer </strong>}}<strong> </strong>Offer to take the time to talk with them about\n    <br>• the <strong>Problem</strong> exposed by the Interest Generator,\n    <br>• their <strong>Project</strong> – where they are and where they want to be in the future with your help,\n    <br>• and how you might also set up an Advisory <strong>Process</strong> to keep them out ahead of future changes.\n    <br>{{<strong>Step 5: Questions </strong>}}&nbsp;\n    <br>When would you like to meet?</div>', 'tsl', 'script'),
(7, 'Extended Initial Contact', '<div><strong>[[Extended Initial Contact ]]<br><br>Business Development Call &amp; Exploratory Meeting Outline<br><br></strong>An initial contact may be a call, an impromptu meeting or an encounter at a business event.<br>Greeting and Introduction&nbsp; comes first - who you are, what you do and who you do it for. Then proceed with this outline.<br><br><strong>[[Concept: Evoking rapport]]<br></strong>Draw out their opinions and interpretations of their situation.<br><br><strong>{{Step 1: Opening}} Declare that I don\\\\\\''t know &amp; you don\\\\\\''t know.<br>{{Step 2: Permission}} Ask permission to explore new ideas.<br>{{Step 3: Interest}} Declare your interest in them. <br></strong>[[Embedded Practice: Exposing a Problem]]<strong><br><br> ((Substep 1: Generate )) Show interest by asking about them and/or your knowledge about them. <br> ((Substep 2: Introduce )) Create an Interest Generator relevant to their situation and what they said <br> ((Substep 3: Explore )) Ask a specific Open-Ended Question(s)until a Problem is exposed. <br>(Step 2 &amp; 3 are interchangeable - first create an interest generator relevant to what the client said and then ask more specific questions to expose a problem or first expose the problem by asking more specific questions and then offer an interest generator that validates the problem and relates it to other client situations.) <br>((Substep 4: Listen )) Expose a problem or restate the problem as a Windows of Opportunity <br>((Substep 5: Convert )) Turn the window of opportunity into Reason to Meet. <br>{{Step 4: Detachment/Offer }} Detach and offer to look at their project with team.<br>{{Step 5: Questions}} Ask a question that draws out their story.<br><br>[[Context: Generating Projects]]<br><br>Crack their story open with fact based questions to their get accomplishments, what\\''s needed &amp; what\\\\\\''s possible.<br><br>{{Step 1: History}} Ask why-based questions to draw out more story.<br>{{Step 2: Current facts}} Ask fact based questions to draw out accomplishments.<br>{{Step 3: Accomplished}} Acknowledge what they have accomplished so far.<br></strong>(Then you may use a second question: What Challenges are you facing and how do plan do resolve them.)<strong><br>{{Step 4: Problems/Offer}} Explore what is needed to move them forward.<br></strong>(You&nbsp; may use a third question: Where do you see yourself headed in the future with this property.)<br><strong>{{Step 5: Project}} Articulate what might become possible for them in the future with your help.<br><br>[[Construct: Strategic Analysis]]<br></strong>Draw out factors, challenge &amp; implications and offer to show options, consequences &amp; your recommendation.<br><br><strong>{{Step 1: Factors}} Identify key factors affecting their situation</strong><br><strong>{{Step 2: Challenges}} Clarify the challenges these factors present.</strong><br><strong>{{Step 3: Implications}} Consider the future implications of theses challenges.</strong><br><strong>{{Step 4: Options}} Offer to return with the options &amp; consequences.</strong><br><strong>{{Step 5: Recommendations}} Offer to return with recommendations.</strong><br><br><strong>[[Content: Client Project Focus]]</strong><br>Show them how this future possibility can be achieved&nbsp; - show them the bridge you are offering.<br><strong>{{Step 1: Platform}} review company capabilities.<br>{{Step 2: People}} Review agent capabilities.<br>{{Step 3: Problems}} Review the situational forces they are facing.<br>{{Step 4: Process/Offer}} Review the attributes of the asset.<br>{{Step 5: Project}} Review the client project including possibility.<br><br>[[Closing: Forging Partnership]]<br></strong>Establish a partnership focused of gathering, preparing and producing a strategic analysis.<br><strong>{{Step 1: Risk}} Review the risks they are facing .</strong><br><strong>{{Step 2: Responsibility}} Review the responsibility that must be taken.<br>{{Step 3: Declaration}} Declare their capability and your commitment.<br>{{Step 4: Invitation}} Invite them to prepare for your presentatiion.<br>{{Step 5: Promise}} State the results they receive if they accept.</strong><br><br><br></div>', 'tsl', 'script'),
(9, 'Evoking Rapport Worksheet', '<div>[[<strong>Evoking Rapport Worksheet</strong>]]<strong><br></strong><br></div><div>{{Client Name:}}<br>{{Project:}}<br><br></div><div>Use this worksheet template to guide questions of client. Simply replace the existing text.<br><br></div><div>{{<strong>Step 1: Opening</strong>}}</div><div>State how you will establish that you and the client don’t know something.<br><br></div><div>{{<strong>Step 2:</strong> <strong>Permission&nbsp;</strong>}}</div><div>State how you will ask for permission to learn about the client and present new ideas.<br><br>{{ <strong>Step 3: Interest </strong>}}</div><div>State how you will show your interest in the client.<br><br></div><div>{{ <strong>Step 4:</strong> <strong>Detachment </strong>}}</div><div>State how you will make a detached offer to help the client accomplish their project.<br><br></div><div>{{ <strong>Step 5: Questions </strong>}}</div><div>State the questions you will ask that will draw out the client story about their situation.<br><br></div>', 'cpdv1', 'wks'),
(10, 'Generating Project Worksheet', '<div>[[<strong>Generating Projects Worksheet</strong>]]<strong><br></strong><br></div><div>{{Client Name:}}<br>{{Product:}}<br><br></div><div>&nbsp;Use this worksheet template to guide questions of client. Simply replace the existing text.<br><br></div><div>{{<strong>History: Recount the Story</strong>}}<strong><br></strong><br></div><div>&nbsp;By asking why-based questions you can draw out more of the client’s story. Replace the existent text. Compose a number of sentences that reveals the historical interpretations that are shaping their view of their current situation and how that shapes their view of the future in a way that limits their actions in the present.<br><br></div><div>{{<strong>Current Facts: List the Facts</strong>}}<strong><br></strong><br></div><div>By asking fact based questions you can separate the client’s subjective view of their situation based on the past from the objective reality they are facing in the present. Compose a sentence or two that describes the facts of their current reality that are separate from their interpretations based on past experiences.&nbsp;<br><br></div><div>{{<strong>Accomplished: Acknowledge Accomplishments</strong>}}<strong><br></strong><br></div><div>Look into the facts to see and articulate the client’s accomplishment as the key that will open a new doorway to seeing a previously unclear future possibility. Compose a sentence or two that describes what the client has accomplished in the face of any and all of the challenges they may also be currently facing.<br><br></div><div>{{<strong>Problems: Offer What’s Needed </strong>}}<strong><br></strong><br></div><div>Make an offer to help the client put what is needed in place that they could not put into place on their own at the level that they could with your help. Compose a sentence or two that describes what is needed that you can help the client put into place so that something becomes possible because of their relationship with you.<br><br></div><div>{{<strong>Project: Create the Possibility</strong>}}<strong><br></strong><br></div><div>Work with the client to draw out greater clarity about the future possibility that will become accessible to the client as a result of your help. Compose a sentence or two that describes how this future possibility in a way that is compelling enough to change their actions in the present.<br><br></div>', 'cpdv1', 'wks'),
(11, 'Forging Partnership Outline', '<div>[[<strong>Forging Partnership Worksheet</strong>]]<strong><br></strong><br></div><div>{{Client Name: }}<br>{{Project:}}<br><br></div><div>&nbsp;Use this worksheet template to guide questions of client. Simply replace the existing text.<br><br></div><div>{{ <strong>Risks: What the Client is Facing </strong>}}<br><br></div><div>Describe the risks related to the client’s current property and financial situation.<br><br>{{ <strong>Responsibility: What is Required of the Client </strong>}}<strong><br></strong><br></div><div>Describe what responsibility the client must take to ensure your ability to help them succeed.<br><br></div><div>{{ <strong>Declaration: Declare Client Capability &amp; Your Commitment </strong>}}<strong><br></strong><br></div><div>Declare their capability of moving forward and your commitment to their success.<br><br></div><div>{{ <strong>Invitation: Make a Compelling Invitation </strong>}}<strong><br></strong><br></div><div>Invite them to take the specific actions necessary to achieve the client results.<br><br></div><div>{{&nbsp;<strong>Promise: Make a Bold Promise for Results&nbsp;</strong>}}<strong><br></strong><br></div><div>Make a promise as to the results the client can expect if they accept your invitation.<br><br></div>', 'cpdv1', 'wks'),
(12, 'Strategic Analysis Worksheet', '<div>[[ <strong>Strategic Analysis Worksheet – Manager </strong>]]<strong><br></strong><br></div><div>{{ Agent Name: }}<br>{{ Project: }}<br><br></div><div>&nbsp;Use this worksheet template to guide questions of client. Simply replace the existing text.&nbsp;<br><br></div><div>{{ <strong>Factor 1: Availability </strong>}}<strong><br></strong><br></div><div>The challenge is spending time on my MBA and working a second job. Implications are that I will never have enough focus on brokerage to make a go of it. I will miss out on the high-income opportunity. I will get stuck in job with little or no upside.<br><br></div><div>{{ <strong>Factor 2: Income </strong>}}<strong><br></strong><br></div><div>The challenge is having enough money to pay the bills and at same time having enough focus to learn brokerage and make money that way. Th implications are that my brokerage income will be limited and I will develop some bad habits that will be tough to break.<br><br></div><div>{{ <strong>Factor 3: Activity </strong>}}<strong><br></strong><br></div><div>The challenge is dedicating enough time to achieve the minimum required metrics necessary to becoming a productive full-time agent. The implications are that I will be unable to achieve the minimum necessary to make the money I want and therefore continue to struggle.</div><div><br></div><div>(( <strong>Option 1: Same Path &amp; Pace </strong>))<strong><br></strong><br></div><div>Keep on the path I am on and hope things change. The consequence is that this will be a long slow road to likely failure. Even if I get lucky, I still won’t have developed the skills needed to sustain that luck.<br><br></div><div>(( <strong>Option 2: Drop Out of Brokerage </strong>))<strong><br></strong><br></div><div>No significant income upside opportunity.<br><br></div><div>(( <strong>Option 3: Same Path – Work Harder at Brokerage </strong>))<strong><br></strong><br></div><div>Stress myself out so that everything I am doing suffers.&nbsp;<br><br></div><div>(( <strong>Option 4: Create a New Path &amp; Focus on Brokerage </strong>))<strong><br></strong><br></div><div>Be willing to allow myself to spend a minimum required time dedicated to brokerage. Set a date by when I will leave my second job. Put my MBA on hold so I can set up financially to compete that at any time.<br><br></div><div>(( <strong>Option 5: Work for a Senior </strong>))<strong><br></strong><br></div><div>Be willing to leave my second job and go to work for a senior agent with a small base and a small percentage for helping alleviate the work load on that senior.<br><br></div><div>{{ <strong>Recommendation: Option </strong>}}<br><br></div><div>Must know their project.<br><br></div>', 'cpdv1', 'wks'),
(13, 'Project Focus Worksheet', '<div>[[<strong>Project Focus Worksheet</strong>]]<strong><br></strong><br></div><div>{{ Client Name: }}<br>{{ Project: }}<br><br></div><div>Use this worksheet template to guide questions of client. Simply replace the existing text.<br><br></div><div>{{ <strong>Platform Offered </strong>}}<strong><br></strong><br></div><div>State the facts of the platform used to accomplish the clients project.<br><br>{{ <strong>People Involved </strong>}}<strong><br></strong><br></div><div>State who will be involved in making the client project happen.<br><br>{{ <strong>Problems to Solve </strong>}}<strong><br></strong><br></div><div>State the problems of the client these people will solve.<br><br>{{ <strong>Process to Follow </strong>}}<strong><br></strong><br></div><div>State the process that will be followed by the people on this platform.<br><br>{{ <strong>Project to Accomplish </strong>}}<strong><br></strong><br></div><div>State the project beginning, middle and end that will unfold through time.</div>', 'cpdv1', 'wks'),
(14, 'Building Presentation', '<div>[[ <strong>Building Presentations Worksheet </strong>]]<strong><br></strong><br></div><div>{{ Client Name: }}<br>{{ Project: }}<br><br></div><div>&nbsp;Use this worksheet template to guide questions of client. Simply replace the existing text.<br><br></div><div>{{ <strong>Concept: Where Are We Going? </strong>}}<strong><br></strong><br></div><div>State the purpose and direction of the presentation.<br><br>{{<strong>Context: Why Is This Important? </strong>}}<strong><br></strong><br></div><div>State why this presentation is important to the listeners.<br><br></div><div>{{ <strong>Construct: How Will We Get There? </strong>}}<br><br></div><div>State how you will take them on the journey.<br><br>{{ <strong>Content: What Are the Details? </strong>}}<strong><br></strong><br></div><div>State all of the details within each step of the construct.<br><br></div><div>{{ <strong>Closing: What Action to Take? </strong>}}<strong><br></strong><br></div><div>Review what has been covered and invite the listeners to take an action.<br><br></div>', 'cpdv1', 'wks'),
(15, 'Defining Challenges Worksheet', '<div>[[<strong>Defining Challenges Worksheet</strong>]]<strong><br></strong><br></div><div>{{ Client Name: }}<br>{{ Project: }}<br><br></div><div>&nbsp;Use this worksheet template to guide questions of client. Simply replace the existing text.<br><br></div><div>{{ <strong>Complaint: Reaffirm the Complaint </strong>}}<strong><br></strong><br></div><div>By asking what the client does not like about their situation you can draw out the main complaint. Compose a number of sentences that reveals what the client does not like about their situation.&nbsp;<br><br></div><div>{{ <strong>Desire: Describe the Desire </strong>}}<strong><br></strong><br></div><div>By asking about the future they want you draw out the desire which is what they want to have in the future that they do not have now. Compose a sentence or two that describes the desire in detail.&nbsp;<br><br></div><div>{{ <strong>Objection: Expose the Objection </strong>}}<strong><br></strong><br></div><div>By asking the client what they don’t want to have to do to get what they want they expose their objection. Compose a sentence or two that describes what the client does not want to have to do.&nbsp;<br><br></div><div>(( <strong>Concern 1: Concern if Things Don’t Change </strong>))<strong><br></strong><br></div><div>By asking for the concern is things don’t change you will draw out the negative outcome the client perceives will occur. Compose a sentence or two that describes this negative outcome.&nbsp;<br><br></div><div>(( <strong>Concern 2: Concern if Things Do Change </strong>))<strong><br></strong><br></div><div>By asking for the concern is things did change you will draw out the negative outcome the client perceives will occur. Compose a sentence or two that describes this negative outcome.&nbsp;<br><br></div><div>{{ <strong>Challenge: Create the Challenge </strong>}}<strong><br></strong><br></div><div>Work with the client to draw out greater clarity about how they could turn their objection into a stepping stone to the fulfillment of their desire.&nbsp;<br><br></div>', 'cpdv1', 'wks'),
(16, 'Designing Project Worksheet', '<div>[[ <strong>Designing Projects Worksheet </strong>]]<strong><br></strong>\n    <br>\n</div>\n<div>{{ Client Name: }}\n    <br>{{ Project: }}&nbsp;\n    <br>\n</div>\n<div>&nbsp;Use this worksheet template to guide questions of client. Simply replace the existing text.\n    <br>\n</div>\n<div>{{ <strong>Mission: </strong>}} Compose a sentence or two that describes the mission that will fulfill the client’s project.\n    <br>{{ <strong>Objective 1: </strong>}}<strong> </strong>Compose a sentence describing the objective and the results it will accomplish.</div>\n<div>{{ <strong>Objective 2: </strong>}}<strong> </strong>Compose a sentence describing the objective and the results it will accomplish.&nbsp;</div>\n<div>{{ <strong>Objective 3: </strong>}}<strong> </strong>Compose a sentence describing the objective and the results it will accomplish.&nbsp;</div>\n<div>{{ <strong>Objective 4: </strong>}}<strong> </strong>Compose a sentence describing the objective and the results it will accomplish.&nbsp;\n    <br>\n</div>\n<div>{{ <strong>Condition 1: </strong>}}<strong> </strong>Compose a sentence that describes the condition that must be fulfilled to take on the objective.</div>\n<div>{{ <strong>Condition 2: </strong>}}<strong> </strong>Compose a sentence that describes the condition that must be fulfilled to take on the objective.</div>\n<div>{{ <strong>Condition 3: </strong>}}<strong> </strong>Compose a sentence that describes the condition that must be fulfilled to take on the objective.</div>\n<div>{{ <strong>Condition 4: </strong>}}<strong> </strong>Compose a sentence that describes the condition that must be fulfilled to take on the objective.\n    <br>\n</div>\n<div>{{ <strong>Metric 1: </strong>}}<strong> </strong>Select a yardstick or benchmark for measuring the progress of the project.</div>\n<div>{{ <strong>Metric 2: </strong>}} Select a yardstick or benchmark for measuring the progress of the project.</div>\n<div>{{ <strong>Metric 3: </strong>}}<strong> </strong>Select a yardstick or benchmark for measuring the progress of the project.\n    <br>\n</div>\n<div>{{ <strong>Milestone 1: </strong>}}<strong> </strong>Set a milestone in time that will mark the progress of the project and what will be measured.</div>\n<div>{{ <strong>Milestone 2: </strong>}} Set a milestone in time that will mark the progress of the project and what will be measured.</div>\n<div>{{ <strong>Milestone 3: </strong>}}<strong> </strong>Set a milestone in time that will mark the progress of the project and what will be measured.</div>', 'cpdv1', 'wks'),
(17, 'Defining Rules worksheet', '<div>[[ <strong>Defining Rules Worksheet </strong>]]<strong><br></strong><br></div><div>{{ Client Name: }}<br>{{ Project: }}<br><br></div><div>&nbsp;Use this worksheet template to guide questions of client. Simply replace the existing text.<br><br></div><div>{{ <strong>Player 1: </strong>}}Name the decision maker and their role in the decision making process,</div><div>{{ <strong>Player 2: </strong>}}Name the decision maker and their role in the decision making process,&nbsp;</div><div>{{ <strong>Player 3: </strong>}}Name the decision maker and their role in the decision making process,&nbsp;<br><br></div><div>{{ <strong>Provision 1: </strong>}}Name the provision that will be provided by whom, to whom and by when.&nbsp;</div><div>{{ <strong>Provision 2: </strong>}}Name the provision that will be provided by whom, to whom and by when.&nbsp;</div><div>{{ <strong>Provision 3: </strong>}} Name the provision that will be provided by whom, to whom and by when.&nbsp;<br><br></div><div>{{ <strong>Stipulation 1: </strong>}}State the rule of conduct or communication that must be followed by whom and by when.</div><div>{{ <strong>Stipulation 2: </strong>}} State the rule of conduct or communication that must be followed by whom and by when.</div><div>{{ <strong>Stipulation 3: </strong>}}State the rule of conduct or communication that must be followed by whom and by when.<br><br></div><div>{{ <strong>Consideration 1: </strong>}}Who gets what recognition and reward and from whom by when?&nbsp;</div><div>{{ <strong>Consideration 2: </strong>}}Who gets what recognition and reward and from whom by when?</div><div>{{ <strong>Consideration 3: </strong>}}Who gets what recognition and reward and from whom by when?<br><br></div><div>{{ <strong>Consequence 1: </strong>}}What consequence will ensure if one, some or all of these rules are broken?&nbsp;</div><div>{{ <strong>Consequence 2: </strong>}}What consequence will ensure if one, some or all of these rules are broken?</div><div>{{ <strong>Consequence 3: </strong>}}<strong> </strong>What consequence will ensure if one, some or all of these rules are broken?</div>', 'cpdv1', 'wks'),
(18, 'Establishing Alignment Worksheet', '<div>[[ <strong>Establishing Alignment Worksheet </strong>]]<strong><br></strong><br></div><div>{{ Client Name: }}<br>{{ Project: }}<br><br></div><div>{{ <strong>Assessment: Clients Assertion or Opinion </strong>}}<strong><br></strong><br></div><div>State the client’s assessment of their situation or the value of their asset.<br><br></div><div>{{ <strong>Assumption 1: Name </strong>}}<strong><br></strong><br></div><div>State the primary assumption this assessment is based upon.<br><br></div><div>{{ <strong>Assumption 2: Name </strong>}}<strong><br></strong><br></div><div>State another assumption this assessment is based upon.<br><br></div><div>{{ <strong>Assumption 3: Name </strong>}}<strong><br></strong><br></div><div>State another assumption this assessment is based upon.<br><br></div><div>{{ <strong>Alignment on Accurate Assessment </strong>}}<strong><br></strong><br></div><div>State what it will take to achieve alignment on the accurate assessment.<br><br></div><div>{{ <strong>Assertion – Alignment on Best Course of Action </strong>}}<strong><br></strong><br></div><div>State your assertion of the best course of action that will serve the clients best interest.<br><br></div><div>{{ <strong>Alignment on Your Accountability </strong>}}<strong><br></strong><br></div><div>State what the you will have to be accountable for delivering.<br><br></div><div>{{ <strong>Alignment on Client Accountability </strong>}}<strong><br></strong><br></div><div>State what the client be need to be accountable for delivering?</div>', 'cpdv1', 'wks'),
(19, 'Initiating Action Worksheet', '<div>[[ <strong>Initiating Actions Worksheet </strong>]]<strong><br></strong><br></div><div>{{ Client Name: }}<br>{{ Project: }}<br><br></div><div>&nbsp;Use this worksheet template to guide questions of client. Simply replace the existing text.<br><br></div><div>{{ <strong>Intention </strong>}}</div><div>State your intention as to how the best interest of the client will be served.<br><br></div><div>{{ <strong>Specific Action </strong>}}<strong><br></strong><br></div><div>&nbsp;State the specific action you want the client to take.<br><br></div><div>{{ <strong>Date-Time </strong>}}</div><div>State the precise date and time by the action should be completed.<br><br></div><div>{{ <strong>Request of Whom </strong>}}</div><div>State the complete request and of whom it will be made.<br><br></div><div>{{ <strong>Commitment </strong>}}</div><div>State choices the client has including accept, decline, counter or defer.</div>', 'cpdv1', 'wks'),
(20, 'Reaffirming Commitments Worksheet.', '<div>[[ <strong>Reaffirming Commitments Worksheet </strong>]]<br>{{ Client Name: }}<br>{{Project: }}<strong><br></strong><br></div><div>&nbsp;Use this worksheet template to guide questions of client. Simply replace the existing text.<br><br></div><div>{{ <strong>Reflect </strong>}}<br>State the facts of the track record of promises kept.<br><br></div><div>{{ <strong>Reopen </strong>}}</div><div>State your concern about anything getting in the in the way of keeping the promise.<br><br>{{ <strong>Reconcile </strong>}}</div><div>State what actions will be taken to deal with the obstacles to keeping a promise.<br><br></div><div>{{ <strong>Remedy </strong>}}</div><div>State a remedy that will ensure the likelihood that the promise made will be kept.<br><br></div><div>{{ <strong>Reaffirm </strong>}}</div><div>State how the how the promise made is now certain to become a promise kept.</div>', 'cpdv1', 'wks');


=============================================================================

15-10-2018
--
-- Table structure for table `cpd_acm_deal`
--

CREATE TABLE IF NOT EXISTS `cpd_acm_deal` (
  `cad_id` int(11) NOT NULL AUTO_INCREMENT,
  `cad_deal_id` int(11) NOT NULL,
  `cad_date` date NOT NULL,
  `cad_ceo` varchar(2) NOT NULL,
  `cad_req` varchar(20) DEFAULT NULL,
  `cad_action` text NOT NULL,
  `cad_whom` varchar(100) DEFAULT NULL,
  `cad_when` datetime DEFAULT NULL,
  `cad_acdf` varchar(2) DEFAULT NULL,
  `cad_tag` int(11) DEFAULT NULL,
  `cad_cat` varchar(2) DEFAULT NULL,
  `cad_req_type` varchar(10) DEFAULT NULL,
  `cad_acm_type` varchar(10) DEFAULT NULL,
  `cad_user_id` int(11) NOT NULL,
  `cad_additional_row` text,
  PRIMARY KEY (`cad_id`),
  KEY `ctd_deal_id` (`cad_deal_id`),
  KEY `ctd_type` (`cad_req_type`),
  KEY `cad_user_id` (`cad_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


=============================================================================

18-10-2018

--
-- Table structure for table `cpd_sa_default`
--

CREATE TABLE IF NOT EXISTS `cpd_sa_default` (
  `csd_id` int(11) NOT NULL AUTO_INCREMENT,
  `csd_page_template` varchar(20) NOT NULL,
  `csd_template_version` varchar(20) NOT NULL,
  `csd_content` text NOT NULL,
  PRIMARY KEY (`csd_id`),
  KEY `csd_type` (`csd_page_template`,`csd_template_version`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `cpd_sa_default`
--

INSERT INTO `cpd_sa_default` (`csd_id`, `csd_page_template`, `csd_template_version`, `csd_content`) VALUES
(1, 'lp', 'fpp', '<h1>{{ Title }}</h1><div><strong>{{ Document Description }}</strong></div><blockquote><strong>{{ Client Name }}</strong><br><strong>{{ Property Name }}</strong><br><strong>{{ Property Location }}</strong><br><strong>{{ Submission Date }}</strong></blockquote><div>[[Team]]<strong><br>{{Agent 1 Name}}<br>{{Professional Speciality}}<br>{{Phone Contact}}<br><br>{{Agent 2 Name}}<br>{{Professional Speciality}}<br>{{Phone Contact}}</strong></div>'),
(2, 'wp', 'lpl', '<h1>[[WELCOME]]</h1><div>{{Dear Client Name,}}<br><br>{{Lead Paragraph}}<br><br>{{Second Paragraph}}<br><br>{{Third Paragraph}}<br><br>{{Salutation}}<br><br><br></div><div>{{Agent Name}}<br>{{Company Name}}</div>'),
(3, 'pp', '5pl2pbl', '<h1>[[PROJECT OVERVIEW]]</h1><div>[[Column 1]]</div><div>{{Historical Overview}}<br>{{Current Facts}}<br>{{Accomplishments}}<br>{{The Challenge}}<br>{{What is Possible}}<br><br>[[Column 2]]<br>{{Our Partnership}}<br>{{Our Invitation}}</div>'),
(4, 'ap', '5plBph', '<h1>[[STRATEGIC ANALYSIS]]</h1><div>[[Column 1]]</div><div><strong>[[{{Title}}]]</strong></div><div><strong>{{Factor 1: Name}}</strong><br>{{Challenge &amp; Implications Description}}<br><strong>{{Factor 2: Name}}</strong><br>{{Challenge &amp; Implications Description}}<br><strong>{{Factor 3: Name}}</strong><br>{{Challenge &amp; Implications Description}}<br><strong>{{Factor 4: Name}}</strong><br>{{Challenge &amp; Implications Description}}<br><strong>{{Factor 5: Name}}</strong><br>{{Challenge &amp; Implications Description}}<br>[[Column 2]]</div><div><strong>[[{{Title}}]]</strong></div><div><strong>{{Option 1: Name}}</strong><br>{{Description of Consequences}}<br><strong>{{Option 2: Name}}</strong><br>{{Description of Consequences}}<br><strong>{{Option 3: Name}}</strong><br>{{Description of Consequences}}<br><strong>{{Option 4: Name}}</strong><br>{{Description of Consequences}}<br><strong>{{Option 5: Name}}</strong><br>{{Description of Consequences}}</div>'),
(5, 'rp', '1ptc', '<div>[[RECOMMENDATIONS]]<br><strong>[[{{Title}}]]</strong><br><strong>{{Option #: Name}}<br></strong>{{How Achieves Project Description}}</div>'),
(6, 'amp', 'tlm', '<div>[[Top Left Message]]</div><h1>{{PROJECT OVERVIEW}}&nbsp;</h1><div>[[Center Box Program Map]]</div><blockquote>{{Header Intro Line}}</blockquote><div>{{Introduction}}</div><blockquote>{{Positioning Statement}}</blockquote><div>{{Bold Statement}}</div>');



--
-- Table structure for table `cpd_sa_data`
--

CREATE TABLE IF NOT EXISTS `cpd_sa_data` (
  `cs_data_id` int(11) NOT NULL AUTO_INCREMENT,
  `cs_data_sa_id` int(11) NOT NULL,
  `cs_data_data` text NOT NULL,
  `cs_data_template` varchar(20) NOT NULL,
  `cs_data_image` varchar(20) DEFAULT NULL,
  `cs_data_page` varchar(20) NOT NULL,
  `cs_data_sa_type` varchar(50) NOT NULL,
  PRIMARY KEY (`cs_data_id`),
  UNIQUE KEY `unique_index` (`cs_data_sa_id`,`cs_data_page`,`cs_data_sa_type`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



--
-- Table structure for table `cpd_sa`
--

CREATE TABLE IF NOT EXISTS `cpd_sa` (
  `cs_id` int(11) NOT NULL AUTO_INCREMENT,
  `cs_type` varchar(50) NOT NULL,
  `cs_name` varchar(100) NOT NULL,
  `cs_random_name` varchar(100) DEFAULT NULL,
  `cs_user_id` int(11) NOT NULL,
  `cs_client_id` int(11) NOT NULL DEFAULT '0',
  `cs_team_logo` varchar(100) DEFAULT NULL,
  `cs_company_logo` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`cs_id`),
  KEY `cs_type` (`cs_type`),
  KEY `cs_user_id` (`cs_user_id`),
  KEY `cs_client_id` (`cs_client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-----------------------------------------------------------------------

29-10-2018

--
-- Table structure for table `tsl_about_client`
--

CREATE TABLE IF NOT EXISTS `tsl_about_client` (
  `tac_id` int(11) NOT NULL AUTO_INCREMENT,
  `tac_tc_id` int(11) NOT NULL,
  `tac_valid_phone` varchar(3) NOT NULL DEFAULT 'na',
  `tac_cb` tinyint(1) DEFAULT NULL,
  `tac_cb_when` datetime DEFAULT NULL,
  `tac_cnv` tinyint(4) DEFAULT NULL,
  `tac_notes` text,
  `tac_result` varchar(11) DEFAULT NULL,
  `tac_action` int(11) DEFAULT '0',
  `tac_action_when` datetime DEFAULT NULL,
  `tac_assignee` int(11) DEFAULT NULL,
  `tac_caller` varchar(100) DEFAULT NULL,
  `tac_last_call` datetime DEFAULT NULL,
  `tac_sequence` int(11) NOT NULL DEFAULT '0',
  `tac_level` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tac_id`),
  KEY `tac_tsl_id` (`tac_tc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


-----------------------------------------------------------------------

14-11-2018


--
-- Table structure for table `universal_component`
--

CREATE TABLE IF NOT EXISTS `universal_component` (
  `uc_id` int(11) NOT NULL AUTO_INCREMENT,
  `uc_name` varchar(100) NOT NULL,
  `uc_controller` varchar(200) NOT NULL,
  `uc_icon` varchar(20) NOT NULL,
  PRIMARY KEY (`uc_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `universal_component`
--

INSERT INTO `universal_component` (`uc_id`, `uc_name`, `uc_controller`, `uc_icon`) VALUES
(1, 'Pages', 'cpdv1/page/', 'file'),
(2, 'Survey', 'cpdv1/survey/', 'book'),
(3, 'DOCS', 'cpdv1/docs/', 'book'),
(4, 'Skills', 'cpdv1/wks/', 'list-alt'),
(5, 'SAP', 'cpdv1/sa/', 'open-file'),
(6, 'WEB', 'cpdv1/web/', 'list-alt');



--
-- Table structure for table `component_sent`
--

CREATE TABLE IF NOT EXISTS `component_sent` (
  `cs_id` int(11) NOT NULL AUTO_INCREMENT,
  `cs_origin` varchar(20) NOT NULL,
  `cs_doc_type` varchar(20) NOT NULL,
  `cs_client_id` int(11) NOT NULL,
  `cs_doc_id` int(11) NOT NULL,
  PRIMARY KEY (`cs_id`),
  KEY `cs_type` (`cs_origin`),
  KEY `cs_client_id` (`cs_client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


--
-- Table structure for table `deal_lead`
--

CREATE TABLE IF NOT EXISTS `deal_lead` (
  `dl_id` int(11) NOT NULL AUTO_INCREMENT,
  `dl_cpd_id` int(11) NOT NULL,
  `dl_name` varchar(50) NOT NULL,
  `dl_type` varchar(10) NOT NULL,
  PRIMARY KEY (`dl_id`),
  KEY `dl_cpd_id` (`dl_cpd_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



----------------------------------------------------------------------------------------

23-11-2018

--
-- Table structure for table `tgd_game_type`
--

CREATE TABLE IF NOT EXISTS `tgd_game_type` (
  `tgd_gt_id` int(11) NOT NULL AUTO_INCREMENT,
  `tgd_gt_name` varchar(30) NOT NULL,
  `tgd_gt_description` text NOT NULL,
  PRIMARY KEY (`tgd_gt_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



CREATE TABLE IF NOT EXISTS `tgd_condition` (
  `tgd_c_id` int(11) NOT NULL AUTO_INCREMENT,
  `tgd_c_gt_id` int(11) NOT NULL,
  `tgd_c_name` varchar(30) NOT NULL,
  `tgd_c_description` text NOT NULL,
  `tgd_c_tooltip` text,
  `tgd_c_type` varchar(10) NOT NULL,
  `tgd_c_uom` varchar(10) NOT NULL,
  `tgd_c_prev_condition` int(11) DEFAULT NULL,
  PRIMARY KEY (`tgd_c_id`),
  KEY `tgd_c_gt_id` (`tgd_c_gt_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `tgd_condition`
--

INSERT INTO `tgd_condition` (`tgd_c_id`, `tgd_c_gt_id`, `tgd_c_name`, `tgd_c_description`, `tgd_c_tooltip`, `tgd_c_type`, `tgd_c_uom`, `tgd_c_prev_condition`) VALUES
(1, 1, 'Cumulative Income', 'Cumulative Income', 'This row shows your cumulative income milestones.', 'cond', 'n', 2),
(2, 1, 'Deals in Contract', 'Deals in Contract', 'These are your deals in contract between a seller & buyer.', 'cond', 'n', 3),
(3, 1, 'Additional Listings', 'Additional Listings', 'These are deals that have a legal selling agreement.', 'cond', 'd', 4),
(4, 1, 'Proposals in Play', 'Proposals in Play', 'These are deals proposed to sellers to take to market.\r\n', 'cond', 'n', NULL),
(47, 2, 'Proposals in Play', 'These are deals proposed to sellers to take to market.', 'These are deals that have a legal selling agreement.', 'cond', 'n', 0),
(48, 2, 'Additional Listings', 'These are deals that have a legal selling agreement.', 'These are deals that have a legal selling agreement.', 'cond', 'n', 47),
(49, 2, 'Deals in Contract', 'These are your deals in contract between a seller and buyer.', 'These are your deals in contract between a seller and a buyer.', 'cond', 'n', 48),
(50, 2, 'Cumulative Income', 'Your cumulative income from completed deals.', 'Your cumulative income from completed deals.', 'obj', 'd', 49);


--
-- Table structure for table `tgd_multiplier`
--

CREATE TABLE IF NOT EXISTS `tgd_multiplier` (
  `tgd_m_id` int(11) NOT NULL AUTO_INCREMENT,
  `tgd_m_gt_id` int(11) NOT NULL,
  `tgd_m_order` int(11) NOT NULL,
  `tgd_m_name` varchar(50) NOT NULL,
  `tgd_m_description` text NOT NULL,
  `tgd_m_type` varchar(10) NOT NULL DEFAULT 'dollar',
  `tgd_m_operation` varchar(5) NOT NULL DEFAULT 'mult',
  `tgd_m_propagate` varchar(5) NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`tgd_m_id`),
  KEY `tgd_m_gt_id` (`tgd_m_gt_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `tgd_multiplier`
--

INSERT INTO `tgd_multiplier` (`tgd_m_id`, `tgd_m_gt_id`, `tgd_m_order`, `tgd_m_name`, `tgd_m_description`, `tgd_m_type`, `tgd_m_operation`, `tgd_m_propagate`) VALUES
(1, 1, 1, 'Average Check Size', 'The average check amount per sale.', 'dollar', 'mult', 'yes'),
(2, 1, 2, 'Double End Factor', 'The percent of deals that are double end deals.', 'percent', 'mult', 'yes'),
(3, 1, 3, 'Other Income', 'Total amount received from other income including buy-side income.', 'percent', 'add', 'no'),
(13, 2, 1, 'Average Check Size', 'The average check amount per deal.', 'dollar', 'mult', 'yes'),
(14, 2, 2, 'Double End Factor', 'The percent of deals that are double end deals.', 'percent', 'mult', 'yes'),
(16, 1, 3, 'Other Income', 'Total amount received from other income including buy-side deals.', 'dollar', 'add', 'yes');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tgd_multiplier`
--
ALTER TABLE `tgd_multiplier`
  ADD CONSTRAINT `fk_mul_game_id` FOREIGN KEY (`tgd_m_gt_id`) REFERENCES `tgd_game_type` (`tgd_gt_id`);



--
-- Table structure for table `tgd_other_income_source`
--

CREATE TABLE IF NOT EXISTS `tgd_other_income_source` (
  `tois_id` int(11) NOT NULL AUTO_INCREMENT,
  `tois_value` varchar(100) NOT NULL,
  `tois_user_id` int(11) NOT NULL,
  PRIMARY KEY (`tois_id`),
  KEY `tois_user_id` (`tois_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tgd_other_income_source`
--

INSERT INTO `tgd_other_income_source` (`tois_id`, `tois_value`, `tois_user_id`) VALUES
(1, 'Consulting Fee', 0),
(2, 'Referral Fee', 0),
(3, 'Service Fee', 0);


--
-- Table structure for table `tgd_other_income`
--

CREATE TABLE IF NOT EXISTS `tgd_other_income` (
  `toi_id` int(11) NOT NULL AUTO_INCREMENT,
  `toi_source_id` int(11) NOT NULL,
  `toi_tgd_ui_id` int(11) NOT NULL,
  `toi_qtr` int(11) NOT NULL,
  `toi_amount` decimal(20,0) NOT NULL,
  `toi_name` varchar(100) NOT NULL,
  PRIMARY KEY (`toi_id`),
  KEY `tos_source_id` (`toi_source_id`,`toi_tgd_ui_id`,`toi_qtr`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

===========================================================================================

(30-11-2018)

--
-- Table structure for table `user_active_product`
--

CREATE TABLE IF NOT EXISTS `user_active_product` (
  `uap_id` int(11) NOT NULL AUTO_INCREMENT,
  `uap_user_id` int(11) NOT NULL,
  `uap_product_id` int(11) NOT NULL,
  PRIMARY KEY (`uap_id`),
  UNIQUE KEY `uap_user_id` (`uap_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;



--
-- Table structure for table `user_product`
--

CREATE TABLE IF NOT EXISTS `user_product` (
  `u_product_id` int(11) NOT NULL AUTO_INCREMENT,
  `u_product_name` varchar(100) NOT NULL,
  `u_product_user_id` int(11) NOT NULL,
  PRIMARY KEY (`u_product_id`),
  KEY `u_product_user_id` (`u_product_user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `user_product`
--

INSERT INTO `user_product` (`u_product_id`, `u_product_name`, `u_product_user_id`) VALUES
(1, 'Product1', 1),
(3, 'Product2', 1),
(4, ' Multi-Family', 0),
(5, ' Multi-Tenant Retail', 0),
(6, ' Single Tenant Retail', 0),
(7, ' Multi-Tenant Office', 0),
(8, ' Single Tenant Office', 0),
(9, ' Industrial', 0),
(10, ' Single Tenant Industrial', 0),
(11, ' Self-Storage', 0),
(12, ' Student Housing', 0),
(13, ' Senior Housing', 0),
(14, ' Assisted Living', 0),
(15, ' Hospitality', 0),
(16, ' Medical Office', 0),
(17, ' Affordable Housing', 0),
(18, ' Tax-Credit', 0),
(19, ' Re-Cap', 0),
(20, ' Mixed Use  ', 0),
(21, 'Product 3', 1);


--
-- Table structure for table `tgd_toggle_history`
--

CREATE TABLE IF NOT EXISTS `tgd_toggle_history` (
  `tph_id` int(11) NOT NULL AUTO_INCREMENT,
  `tph_prev_period` int(11) NOT NULL,
  `tph_current_period` int(11) NOT NULL,
  `tph_prev_cond_id` int(11) NOT NULL,
  `tph_current_cond_id` int(11) NOT NULL,
  `tph_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tph_tgd_ui_id` int(11) NOT NULL,
  PRIMARY KEY (`tph_id`),
  KEY `tph_tgd_ui_id` (`tph_tgd_ui_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


==========================================================================================
(13-12-2018)

--
-- Database: `pst_cpd`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_active_cpd_tgd`
--

CREATE TABLE IF NOT EXISTS `user_active_cpd_tgd` (
  `uact_id` int(11) NOT NULL AUTO_INCREMENT,
  `uact_user_id` int(11) NOT NULL,
  `uact_tgd_id` int(11) DEFAULT NULL,
  `uact_cpd_id` int(11) DEFAULT NULL,
  `uact_prod_id` int(11) NOT NULL,
  PRIMARY KEY (`uact_id`),
  UNIQUE KEY `uact_user_id` (`uact_user_id`,`uact_prod_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


--
-- Table structure for table `tgd_linked`
--

CREATE TABLE IF NOT EXISTS `tgd_linked` (
  `tl_id` int(11) NOT NULL AUTO_INCREMENT,
  `tl_tgd_id` int(11) NOT NULL,
  `tl_linked_product_id` int(11) NOT NULL,
  PRIMARY KEY (`tl_id`),
  UNIQUE KEY `tl_tgd_id` (`tl_tgd_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

=============================================================================================


(18-12-2018)

--
--  Add foreign Keys.
--

ALTER TABLE  `tgd_user_instance` ADD CONSTRAINT  `fk_ui_cpd_id` FOREIGN KEY (  `tgd_cpd_id` ) REFERENCES  `pst_cpd`.`cpd` (
`cpd_id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;

ALTER TABLE  `tgd_other_income` ADD CONSTRAINT  `fk_tois_toi_id` FOREIGN KEY (  `toi_source_id` ) REFERENCES `pst_cpd`.`tgd_other_income_source` (
`tois_id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;

ALTER TABLE  `tgd_other_income` ADD CONSTRAINT  `fk_oi_ui_id` FOREIGN KEY (  `toi_tgd_ui_id` ) REFERENCES  `pst_cpd`.`tgd_user_instance` (
`tgd_ui_id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;

ALTER TABLE  `tgd_toggle_history` ADD CONSTRAINT  `fk_history_ui_id` FOREIGN KEY (  `tph_tgd_ui_id` ) REFERENCES  `pst_cpd`.`tgd_user_instance` (
`tgd_ui_id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;

ALTER TABLE  `tgd_other_income` DROP FOREIGN KEY  `fk_tois_toi_id` ;

ALTER TABLE  `tgd_other_income` ADD CONSTRAINT  `fk_tois_toi_id` FOREIGN KEY (  `toi_source_id` ) REFERENCES `pst_cpd`.`tgd_other_income_source` (
`tois_id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `tgd_other_income` DROP FOREIGN KEY  `fk_oi_ui_id` ;

ALTER TABLE  `tgd_other_income` ADD CONSTRAINT  `fk_oi_ui_id` FOREIGN KEY (  `toi_tgd_ui_id` ) REFERENCES  `pst_cpd`.`tgd_user_instance` (
`tgd_ui_id`
) ON DELETE CASCADE ON UPDATE CASCADE ;


ALTER TABLE  `tgd_multiplier` DROP FOREIGN KEY  `fk_mul_game_id` ;

ALTER TABLE  `tgd_multiplier` ADD CONSTRAINT  `fk_mul_game_id` FOREIGN KEY (  `tgd_m_gt_id` ) REFERENCES  `pst_cpd`.`tgd_game_type` (
`tgd_gt_id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `tgd_linked` DROP FOREIGN KEY  `fk_linked_ui_id` ;

ALTER TABLE  `tgd_linked` ADD CONSTRAINT  `fk_linked_ui_id` FOREIGN KEY (  `tl_tgd_id` ) REFERENCES  `pst_cpd`.`tgd_user_instance` (
`tgd_ui_id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE  `tgd_instance_multiplier` ADD CONSTRAINT  `fk_mul_ui_id` FOREIGN KEY (  `tgd_im_ui_id` ) REFERENCES  `pst_cpd`.`tgd_user_instance` (
`tgd_ui_id`
) ON DELETE RESTRICT ON UPDATE RESTRICT ;

ALTER TABLE  `tgd_instance_multiplier` ADD CONSTRAINT  `fk_mul_m_id` FOREIGN KEY (  `tgd_im_m_id` ) REFERENCES `pst_cpd`.`profession_multiplier` (
`profession_m_id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

===============================================================================================
(24-12-2018)

--
-- Table structure for table `cpd_client_info`
--

CREATE TABLE IF NOT EXISTS `cpd_client_info` (
  `cc_id` int(11) NOT NULL AUTO_INCREMENT,
  `cc_client_id` int(11) NOT NULL,
  `cc_company` varchar(100) DEFAULT NULL,
  `cc_title` varchar(10) NOT NULL,
  `cc_fname` varchar(100) DEFAULT NULL,
  `cc_lname` varchar(100) DEFAULT NULL,
  `cc_work_phone` varchar(20) DEFAULT NULL,
  `cc_cell_phone` varchar(20) DEFAULT NULL,
  `cc_email` varchar(150) DEFAULT NULL,
  `cc_website` varchar(150) DEFAULT NULL,
  `cc_address` varchar(200) DEFAULT NULL,
  `cc_parking` varchar(100) DEFAULT NULL,
  `cc_city` varchar(100) DEFAULT NULL,
  `cc_state` varchar(100) DEFAULT NULL,
  `cc_zip` int(20) DEFAULT NULL,
  `cc_type` varchar(20) NOT NULL,
  `cc_add_email` text,
  `cc_add_phone` text,
  `cc_add_address` text,
  `cc_notes` text,
  `cc_note_date` text,
  `cc_info_type` varchar(20) DEFAULT NULL,
  `cc_ref_source` varchar(200) DEFAULT NULL,
  `cc_ref_event` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`cc_id`),
  KEY `cc_type` (`cc_type`),
  KEY `cc_client_id` (`cc_client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for table `cpd_client_info`
--
ALTER TABLE `cpd_client_info`
  ADD CONSTRAINT `fk_cc_deal_id` FOREIGN KEY (`cc_client_id`) REFERENCES `deal` (`deal_id`) ON DELETE CASCADE ON UPDATE CASCADE;

-------------------------------------------------------------------------------------------

--
-- Table structure for table `tsl_script_answer`
--

CREATE TABLE IF NOT EXISTS `tsl_script_answer` (
  `tsa_id` int(11) NOT NULL AUTO_INCREMENT,
  `tsa_question_id` int(11) NOT NULL,
  `tsa_content` text NOT NULL,
  `tsa_instance_id` int(11) NOT NULL,
  `tsa_type` varchar(20) NOT NULL,
  PRIMARY KEY (`tsa_id`),
  UNIQUE KEY `tsa_index` (`tsa_question_id`,`tsa_instance_id`),
  KEY `tsa_type` (`tsa_type`),
  KEY `tsa_instance_id` (`tsa_instance_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tsl_script_answer`
--
ALTER TABLE `tsl_script_answer`
  ADD CONSTRAINT `fk_script_ans` FOREIGN KEY (`tsa_instance_id`) REFERENCES `tsl_script` (`ts_id`) ON DELETE CASCADE ON UPDATE CASCADE;


==================================================================================================
(Dec-25-2018)


--
-- Constraints for table `cpd`
--

ALTER TABLE  `cpd` ADD CONSTRAINT  `fk_cpd_product` FOREIGN KEY (  `cpd_product_id` ) REFERENCES  `pst_cpd`.`user_product` (
`u_product_id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

--------------------------------------------------------------------------------------

--
-- Constraints for table `cpd_acm_deal`
--

ALTER TABLE  `cpd_acm_deal` ADD CONSTRAINT  `fk_cad_deal` FOREIGN KEY (  `cad_deal_id` ) REFERENCES  `pst_cpd`.`deal` (

`deal_id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

----------------------------------------------------------------------------------------

--
-- Constraints for table `cpd_client_info`
--

ALTER TABLE `pst_cpd`.`cpd_client_info`
DROP FOREIGN KEY `fk_cc_deal_id`;
ALTER TABLE `pst_cpd`.`cpd_client_info`
CHANGE COLUMN `cc_client_id` `cc_deal_id` INT(11) NOT NULL ;
ALTER TABLE `pst_cpd`.`cpd_client_info`
ADD CONSTRAINT `fk_cc_deal_id`
  FOREIGN KEY (`cc_deal_id`)
  REFERENCES `pst_cpd`.`deal` (`deal_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

----------------------------------------------------------------------------------------

--
-- Constraints for table `cpd_deal_info`
--

ALTER TABLE `cpd_client_info` RENAME TO `cpd_deal_info`;

----------------------------------------------------------------------------------------

--
-- Constraints for table `cpd_sa_data`
--

ALTER TABLE  `cpd_sa_data` ADD CONSTRAINT  `fk_sa_data` FOREIGN KEY (  `cs_data_sa_id` ) REFERENCES  `pst_cpd`.`cpd_sa` (

`cs_id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

----------------------------------------------------------------------------------------

--
-- Constraints for table `cpd_sa_data`
--

ALTER TABLE  `deal_lead` ADD CONSTRAINT  `fk_lead_cpd_id` FOREIGN KEY (  `dl_cpd_id` ) REFERENCES  `pst_cpd`.`cpd` (

`cpd_id`
) ON DELETE CASCADE ON UPDATE CASCADE ;

----------------------------------------------------------------------------------------

--
-- Table structure for table `deal_notes_action_whom`
--

CREATE TABLE IF NOT EXISTS `deal_notes_action_whom` (
  `dnaw_id` int(11) NOT NULL,
  `dnaw_cpd_id` int(11) NOT NULL,
  `dnaw_name` varchar(100) NOT NULL,
  `dnaw_type` varchar(10) NOT NULL,
  KEY `ds_cpd_id` (`dnaw_cpd_id`,`dnaw_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for table `deal_notes_action_whom`
--

ALTER TABLE `deal_notes_action_whom`
  ADD CONSTRAINT `fk_dnaw_cpd_id` FOREIGN KEY (`dnaw_cpd_id`) REFERENCES `cpd` (`cpd_id`) ON DELETE CASCADE ON UPDATE CASCADE;



--
-- Constraints for table `deal_notes_action_whom`
--


ALTER TABLE `deal` ADD CONSTRAINT `fk_deal_action` FOREIGN KEY (`deal_action_id`) REFERENCES `deal_lead`(`dl_id`) ON DELETE CASCADE ON UPDATE CASCADE; ALTER TABLE `deal` ADD CONSTRAINT `fk_deal_whom` FOREIGN KEY (`deal_whom_id`) REFERENCES `deal_lead`(`dl_id`) ON DELETE CASCADE ON UPDATE CASCADE;



-------------------------------------------------------------------------------------------

(Jan-03-2018)
ALTER TABLE `tgd_instance_cell` ADD `tgd_ic_linked` INT(11) NOT NULL DEFAULT '0' AFTER `tgd_ic_target_t`;



ALTER TABLE `pst_cpd`.`cpd_docs`
ADD COLUMN `cd_format` VARCHAR(10) NOT NULL AFTER `cd_type`,
ADD COLUMN `cd_category` VARCHAR(10) NOT NULL AFTER `cd_format`;



ALTER TABLE `pst_cpd`.`cpd_docs`
DROP INDEX `cd_type` ,
ADD INDEX `cd_type` (`cd_type` ASC, `cd_format` ASC, `cd_category` ASC);


ALTER TABLE `tgd_user_instance` CHANGE `tgd_ui_floor` `tgd_ui_floor` DECIMAL(20,2) NOT NULL DEFAULT '0.00';
ALTER TABLE `tgd_user_instance` CHANGE `tgd_ui_target` `tgd_ui_target` DECIMAL(20,2) NOT NULL DEFAULT '0.00';
ALTER TABLE `tgd_user_instance` CHANGE `tgd_ui_game` `tgd_ui_game` DECIMAL(20,2) NOT NULL DEFAULT '0.00';


-----------------------------------------------------------------------------------------------

(Jan-07-2018)


--
-- Add codes for new CPD view  tables.
--

INSERT INTO `code_type` (`ct_id`, `ct_abbrev`, `ct_name`, `ct_description`) VALUES (NULL, 'gends', 'Generation Deal', 'Generation Deal Status'), (NULL, 'comds', 'Completion Deal', 'Completion Deal Status');


--
-- Insert Prospective, Possible and potential deal codes.
--

INSERT INTO `code` (`code_id`, `code_type_id`, `code_abbrev`, `code_name`, `code_description`) VALUES (NULL, '24', 'prospective', 'Proposal Requested & Presented', 'Proposals Requested & Presented'), (NULL, '24', 'possible', 'Meetings Scheduled & Completed', 'Meetings Scheduled & Completed');

INSERT INTO `code` (`code_id`, `code_type_id`, `code_abbrev`, `code_name`, `code_description`) VALUES (NULL, '24', 'potential', 'Maybe & No to a Meeting', 'Invitations: Maybe & No to a Meeting ');


--
-- Change Deal Status char size,
--

ALTER TABLE `deal_status` CHANGE `ds_status` `ds_status` VARCHAR(11) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

--
--Add codes for new CPD complete row
--

INSERT INTO `code` (`code_id`, `code_type_id`, `code_abbrev`, `code_name`, `code_description`) VALUES (NULL, '25', 'predictable', 'Deals in Contract & Hard', 'Deals in Contract & Hard'), (NULL, '25', 'probable', 'Deals Listed & Launched', 'Deals Listed & Launched');

--
-- Add columns for new table names.
--

ALTER TABLE `cpd` ADD `cpd_predictable_name` VARCHAR(50) NOT NULL DEFAULT 'Deals in Contract & Hard'
AFTER `cpd_product_id`, ADD `cpd_probable_name` VARCHAR(50) NOT NULL DEFAULT 'Deals Listed & Launched'
AFTER `cpd_predictable_name`, ADD `cpd_potential_name` VARCHAR(50) NOT NULL DEFAULT 'Invitations: Maybe & No to a Meeting'
AFTER `cpd_probable_name`, ADD `cpd_possible_name` VARCHAR(50) NOT NULL DEFAULT 'Meetings Scheduled & Completed'
AFTER `cpd_potential_name`, ADD `cpd_prospective_name` VARCHAR(50) NOT NULL DEFAULT
'Proposals Requested & Presented' AFTER `cpd_possible_name`;

--
-- Next and Prev Year TGD instances.
--

ALTER TABLE `tgd_user_instance` ADD `tgd_ui_next_year_id` INT(11) NULL DEFAULT NULL AFTER `tgd_ui_condition`, ADD `tgd_ui_prev_year_id` INT(11) NULL DEFAULT NULL AFTER `tgd_ui_next_year_id`, ADD INDEX `fk_next_year` (`tgd_ui_next_year_id`), ADD INDEX `fk_prev_year` (`tgd_ui_prev_year_id`);


--
-- Update internal relations TGD.
--

ALTER TABLE `tgd_user_instance` DROP FOREIGN KEY `fk_ui_cpd_id`; ALTER TABLE `tgd_user_instance` ADD CONSTRAINT `fk_ui_cpd_id` FOREIGN KEY (`tgd_cpd_id`) REFERENCES `cpd`(`cpd_id`) ON DELETE CASCADE ON UPDATE CASCADE; ALTER TABLE `tgd_user_instance` DROP FOREIGN KEY `fk_ui_gt_id`; ALTER TABLE `tgd_user_instance` ADD CONSTRAINT `fk_ui_gt_id` FOREIGN KEY (`tgd_ui_gt_id`) REFERENCES `tgd_game_type`(`tgd_gt_id`) ON DELETE CASCADE ON UPDATE CASCADE; ALTER TABLE `tgd_user_instance` DROP FOREIGN KEY `fk_ui_product_id`; ALTER TABLE `tgd_user_instance` ADD CONSTRAINT `fk_ui_product_id` FOREIGN KEY (`tgd_ui_product_id`) REFERENCES `user_product`(`u_product_id`) ON DELETE CASCADE ON UPDATE CASCADE;


-----------------------------------------------------------------------------------------------

(Jan-09-2019)

--
-- Table structure for table `tsl_actions`
--

CREATE TABLE `tsl_actions` (
  `ta_id` int(11) NOT NULL,
  `ta_text` text NOT NULL,
  `ta_tsl_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `tsl_actions`
--
ALTER TABLE `tsl_actions`
  ADD PRIMARY KEY (`ta_id`),
  ADD KEY `ta_tsl_id` (`ta_tsl_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tsl_actions`
--
ALTER TABLE `tsl_actions`
  MODIFY `ta_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tsl_actions`
--
ALTER TABLE `tsl_actions`
  ADD CONSTRAINT `fk_tsl_action` FOREIGN KEY (`ta_tsl_id`) REFERENCES `tsl` (`tsl_id`) ON DELETE CASCADE;

--
-- Change name for Deposit to Hard
--

UPDATE `code` SET `code_abbrev` = 'hard' WHERE `code`.`code_id` = 33;

update deal_status set ds_status='hard' where ds_status='deposit'

------------------------------------------------------------------------------------------------------
(Jan-11-2019)

--
-- Add column for Email Purpose.
--

ALTER TABLE `pst_cpd`.`tsl_email`
ADD COLUMN `te_purpose` VARCHAR(10) NULL AFTER `te_event`;

-----------------------------------------------------------------------------------------------------------

(Jan-21-2019)

--
-- Change data type for User ID to same as in WP.
--

ALTER TABLE `tsl` CHANGE `tsl_user_id` `tsl_user_id` BIGINT(20) NOT NULL;
ALTER TABLE `tsl` CHANGE `tsl_user_id` `tsl_user_id` BIGINT(20) UNSIGNED NOT NULL;



--
-- Add foreign key constraint for TSL Table.
--

ALTER TABLE `tsl` ADD CONSTRAINT `fk_tsl_user` FOREIGN KEY (`tsl_user_id`) REFERENCES `pst_wp_cpd`.`wp_users`(`ID`) ON DELETE CASCADE ON UPDATE CASCADE;


--
--   Foreign key for Products
--

ALTER TABLE `tsl` ADD CONSTRAINT `fk_tsl_product` FOREIGN KEY (`tsl_product_id`) REFERENCES `user_product`(`u_product_id`) ON DELETE CASCADE ON UPDATE CASCADE;



--
-- Foreign Key for TSL caller.
--

ALTER TABLE `tsl_caller` ADD  CONSTRAINT `fk_tsl_caller` FOREIGN KEY (`tsl_caller_tsl_id`) REFERENCES `tsl`(`tsl_id`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Foreign Key for TSL Client
--

ALTER TABLE `tsl_client` ADD  CONSTRAINT `fk_tsl_user` FOREIGN KEY (`tc_user_id`) REFERENCES `pst_wp_cpd`.`wp_users`(`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `tsl_client` CHANGE `tc_user_id` `tc_user_id` BIGINT(20) UNSIGNED NOT NULL


--
-- Create Table Deal Notes.
--

CREATE TABLE `pst_cpd`.`deal_notes`
( `dn_id` INT(11) NOT NULL , `dn_deal_id` INT(11) NOT NULL ,
`dn_seq` INT(11) NOT NULL , `dn_name` VARCHAR(200) DEFAULT NULL ,
 `dn_note` TEXT DEFAULT NULL ,
 `dn_when` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  UNIQUE `deal_notes` (`dn_deal_id`, `dn_seq`)) ENGINE = InnoDB;

  ALTER TABLE `pst_cpd`.`deal_notes`
  CHANGE COLUMN `dn_id` `dn_id` INT(11) NOT NULL AUTO_INCREMENT ,
  ADD PRIMARY KEY (`dn_id`);


====================================================================================================
(Jan-22-2019)

--
-- Add Unique Key to TSL Notes
--

ALTER TABLE `pst_cpd`.`tsl_notes` ADD UNIQUE `tn_client_seq` (`tn_client_id`, `tn_seq`);


===================================================================================================
(Jan-22-2019)

--
-- Create table for TSL actions
--

Drop table tsl_actions

CREATE TABLE `pst_cpd`.`tsl_actions` ( `ta_id` INT(11) NOT NULL AUTO_INCREMENT , `ta_content` TEXT NOT NULL , `ta_client_id` INT(11) NOT NULL , `ta_whom` INT(11) NOT NULL , `ta_when` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `ta_seq` INT(11) NOT NULL , PRIMARY KEY (`ta_id`), INDEX `action_assignee` (`ta_whom`), UNIQUE `unique_client_seq` (`ta_client_id`, `ta_seq`)) ENGINE = InnoDB;


ALTER TABLE `tsl_actions` ADD CONSTRAINT `fk_action_assignee` FOREIGN KEY (`ta_whom`) REFERENCES `tsl_assignee`(`tsl_assignee_id`) ON DELETE CASCADE ON UPDATE CASCADE;


INSERT INTO `code_type` (`ct_id`, `ct_abbrev`, `ct_name`, `ct_description`) VALUES (NULL, 'stra_ds', 'Strategic Deal', 'Strategic Deal Status');


INSERT INTO `code` (`code_id`, `code_type_id`, `code_abbrev`, `code_name`, `code_description`) VALUES (NULL, '26', 'presented', 'Proposal Presented', 'Proposal Presented'), (NULL, '26', 'delivered', 'Meeting Delivered', 'Meeting Delivered');

=====================================================================================================
(Jan-24-2019)

--
-- Add column for presentation, invitation and meeting to CPD table.
--

ALTER TABLE `pst_cpd`.`cpd`
ADD COLUMN `cpd_present_name` VARCHAR(50) NOT NULL DEFAULT 'Presentation In-Person or By Phone' AFTER `cpd_prospective_name`,
ADD COLUMN `cpd_invitation_name` VARCHAR(50) NOT NULL DEFAULT 'Invitation In-Person or By Phone' AFTER `cpd_present_name`,
ADD COLUMN `cpd_delivered_name` VARCHAR(50) NOT NULL DEFAULT 'Meeting In-Person or By Phone' AFTER `cpd_invitation_name`;


--
-- Set Default value for TSL actions whom ID.
--

ALTER TABLE `pst_cpd`.`tsl_actions`
DROP FOREIGN KEY `fk_action_assignee`;
ALTER TABLE `pst_cpd`.`tsl_actions`
CHANGE COLUMN `ta_whom` `ta_whom` INT(11) NULL DEFAULT NULL ;
ALTER TABLE `pst_cpd`.`tsl_actions`
ADD CONSTRAINT `fk_action_assignee`
  FOREIGN KEY (`ta_whom`)
  REFERENCES `pst_cpd`.`tsl_assignee` (`tsl_assignee_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

--
-- TSL notes Table
--

Drop table pst_cpd.tsl_notes;

--
-- Table structure for table `tsl_notes`
--

CREATE TABLE `tsl_notes` (
`tn_id` int(11) NOT NULL,
`tn_content` text,
`tn_client_id` int(11) NOT NULL,
`tn_datetime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`tn_seq` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Add column for Topic In TSL call Notes.
--

ALTER TABLE `tsl_notes` ADD `tn_topic` TEXT NULL DEFAULT NULL AFTER `tn_content`;


--
-- Indexes for table `tsl_notes`
--
ALTER TABLE `tsl_notes`
  ADD PRIMARY KEY (`tn_id`),
  ADD UNIQUE KEY `tn_client_seq` (`tn_client_id`,`tn_seq`),
  ADD KEY `tn_whom` (`tn_id`);

--
-- AUTO_INCREMENT for table `tsl_notes`
--
ALTER TABLE `tsl_notes`
MODIFY `tn_id` int(11) NOT NULL AUTO_INCREMENT;


--
-- Add code type for Buyer dashboard.
--

UPDATE `pst_cpd`.`code_type` SET `ct_id`='27', `ct_abbrev`='buyer_ds', `ct_name`='Buyer Status', `ct_description`='Buyer Status' WHERE `ct_id`='29';

--
-- Add Offers dashboard Code
--

INSERT INTO `code` (`code_id`, `code_type_id`, `code_abbrev`, `code_name`, `code_description`) VALUES (NULL, '27', 'proposed', 'Offer Proposed', 'Offer Proposed');
INSERT INTO `code` (`code_id`, `code_type_id`, `code_abbrev`, `code_name`, `code_description`) VALUES (NULL, '27', 'accepted', 'Offer Accepted', 'Offer Accepted'), (NULL, '27', 'submitted', 'Offer Submitted', 'Offer Submitted');


--
-- Insert Code type for move Buyer Status
--

INSERT INTO `pst_cpd`.`code_type` (`ct_abbrev`, `ct_name`, `ct_description`) VALUES ('bds', 'Buyer Deal', 'Buyer Deal');


--
-- Insert Code for move Buyer status
--

INSERT INTO `code` (`code_id`, `code_type_id`, `code_abbrev`, `code_name`, `code_description`) VALUES (NULL, '28', 'a_person', 'Accepted In Person', 'Offer Accepted In Person'), (NULL, '28', 'a_phone', 'Accepted By Phone', 'Offer Accepted In Phone or Web'), (NULL, '28', 's_person', 'Submitted In Person', 'Offer Submitted In Phone'), (NULL, '28', 's_phone', 'Submitted By Phone', 'Offer Submitted By Phone'), (NULL, '28', 'p_person', 'Proposed In Person', 'Offer Proposed In Person'), (NULL, '28', 'p_phone', 'Proposed By Phone', 'Offer Proposed By Phone');


--
-- Add columns for Buyers Table Name to CPD.
--

ALTER TABLE `cpd` ADD `cpd_accepted_name` VARCHAR(50) NOT NULL DEFAULT 'In-Person, By Phone or Email' AFTER `cpd_delivered_name`, ADD `cpd_submitted_name` VARCHAR(50) NOT NULL DEFAULT 'In-Person, By Phone or Email' AFTER `cpd_accepted_name`, ADD `cpd_proposed_name` VARCHAR(50) NOT NULL DEFAULT 'In-Person, By Phone or Email' AFTER `cpd_submitted_name`;


=======================================================
--
-- Add foreign keys to CPD
--

ALTER TABLE `cpd` CHANGE `cpd_user_id` `cpd_user_id` INT(11) UNSIGNED NOT NULL;
ALTER TABLE `cpd` CHANGE `cpd_user_id` `cpd_user_id` BIGINT(20) UNSIGNED NOT NULL;

ALTER TABLE `cpd` ADD  CONSTRAINT `fk_cpd_user` FOREIGN KEY (`cpd_user_id`) REFERENCES `pst_wp_cpd`.`wp_users`(`ID`) ON DELETE CASCADE ON UPDATE CASCADE;



ALTER TABLE `cpd_acm_deal` CHANGE `cad_user_id` `cad_user_id` BIGINT(20) UNSIGNED NOT NULL;

ALTER TABLE `cpd_acm_deal` ADD CONSTRAINT `fk_cad_user` FOREIGN KEY (`cad_user_id`) REFERENCES `pst_wp_cpd`.`wp_users`(`ID`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `cpd_client_team` CHANGE `cct_user_id` `cct_user_id` BIGINT(20) UNSIGNED NOT NULL;

ALTER TABLE `cpd_client_team` ADD  CONSTRAINT `fk_client_user_id` FOREIGN KEY (`cct_user_id`) REFERENCES `pst_wp_cpd`.`wp_users`(`ID`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `cpd_docs` ADD CONSTRAINT `fk_cd_deal` FOREIGN KEY (`cd_cpd_deal`) REFERENCES `deal`(`deal_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `cpd_docs` CHANGE `cd_user_id` `cd_user_id` BIGINT(20) UNSIGNED NOT NULL;

ALTER TABLE `cpd_docs` ADD CONSTRAINT `fk_cd_user` FOREIGN KEY (`cd_user_id`) REFERENCES `pst_wp_cpd`.`wp_users`(`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `cpd_sa` CHANGE `cs_user_id` `cs_user_id` BIGINT(20) UNSIGNED NOT NULL;

ALTER TABLE `cpd_sa` ADD  CONSTRAINT `fk_cs_user` FOREIGN KEY (`cs_user_id`) REFERENCES `pst_wp_cpd`.`wp_users`(`ID`) ON DELETE CASCADE ON UPDATE CASCADE;


delete from cpd_sa where cs_client_id=0

ALTER TABLE `cpd_sa` ADD  CONSTRAINT `fl_cs_client` FOREIGN KEY (`cs_client_id`) REFERENCES `deal`(`deal_id`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `deal` CHANGE `deal_user_id` `deal_user_id` BIGINT(20) UNSIGNED NOT NULL;

ALTER TABLE `deal` ADD  CONSTRAINT `fk_deal_user` FOREIGN KEY (`deal_user_id`) REFERENCES `pst_wp_cpd`.`wp_users`(`ID`) ON DELETE CASCADE ON UPDATE CASCADE;


DELETE from deal where deal_lead=0

ALTER TABLE `deal` CHANGE `deal_lead` `deal_lead` INT(11) NULL DEFAULT NULL;

ALTER TABLE `deal` ADD  CONSTRAINT `fk_deal_lead` FOREIGN KEY (`deal_lead`) REFERENCES `deal_lead`(`dl_id`) ON DELETE CASCADE ON UPDATE CASCADE;



=======================================================================================================
(Feb-01-2019)

--
-- Add content column to promo table.
--

ALTER TABLE `pst_cpd`.`tsl_promo`
CHANGE COLUMN `tp_rand_name` `tp_rand_name` VARCHAR(100) NULL DEFAULT NULL ,
ADD COLUMN `tp_content` TEXT NULL DEFAULT NULL AFTER `tp_outline_id`;


===========================================================================================================

(Feb-04-2019)

--
-- Add column to survey table.
--

ALTER TABLE `tsl_survey_outline` ADD `tso_survey_phase` VARCHAR(20) NOT NULL AFTER `tso_survey_class`, ADD INDEX `tso_survey_phase` (`tso_survey_phase`);

===========================================================================================================

(Feb-05-2019)

--
-- Add columns to Universal Components.
--

ALTER TABLE `pst_cpd`.`universal_component`
ADD COLUMN `uc_table` VARCHAR(30) NULL DEFAULT NULL AFTER `uc_icon`,
ADD COLUMN `uc_col_name` VARCHAR(30) NULL DEFAULT NULL AFTER `uc_table`,
ADD COLUMN `uc_ref_primary_key` VARCHAR(20) NULL DEFAULT NULL AFTER `uc_col_name`;


--
-- Add default values for Universal component table table, column, primary key field.
--

UPDATE `pst_cpd`.`universal_component` SET `uc_table`='cpd_docs', `uc_col_name`='cd_name', `uc_ref_primary_key`='cd_id' WHERE `uc_id`='3';
UPDATE `pst_cpd`.`universal_component` SET `uc_table`='tsl_promo', `uc_col_name`='tp_org_name', `uc_ref_primary_key`='tp_id' WHERE `uc_id`='1';
UPDATE `pst_cpd`.`universal_component` SET `uc_table`='cpd_sa', `uc_col_name`='cs_name', `uc_ref_primary_key`='cs_id' WHERE `uc_id`='5';
UPDATE `pst_cpd`.`universal_component` SET `uc_table`='tsl_survey', `uc_col_name`='tsl_survey_name', `uc_ref_primary_key`='tsl_survey_id' WHERE `uc_id`='2';



===========================================================================================================

(Feb-08-2019)

--
-- Table structure for table `tsl_promo_answer`
--

CREATE TABLE `tsl_promo_answer` (
  `tpa_id` int(11) NOT NULL,
  `tpa_promo_id` int(11) NOT NULL,
  `tpa_question_id` int(11) NOT NULL,
  `tpa_text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tsl_promo_answer`
--
ALTER TABLE `tsl_promo_answer`
  ADD PRIMARY KEY (`tpa_id`),
  ADD UNIQUE KEY `tpa_unique` (`tpa_promo_id`,`tpa_question_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tsl_promo_answer`
--
ALTER TABLE `tsl_promo_answer`
  MODIFY `tpa_id` int(11) NOT NULL AUTO_INCREMENT;

  =====================================================================================================

  (Feb-11-2019)

  --
  -- Update table code for CPD codes.
  --

  UPDATE `pst_cpd`.`code` SET `code_type_id`='21' WHERE `code_id`='44';

  =======================================================================================================

  (Feb-12-2019)

  --
  -- remove Column content from message builder table.
  --

    Alter table tsl_message drop column tm_content

  --
  -- Rename Table to message version.
  --

    ALTER TABLE `pst_cpd`.`tsl_message`
    RENAME TO  `pst_cpd`.`tsl_message_version` ;

    --
    -- Create table for message content.
    --

    CREATE TABLE `pst_cpd`.`tsl_message_content`
     ( `tmc_id` INT(11) NOT NULL AUTO_INCREMENT ,
    `tmc_version_id` INT(11) NOT NULL ,
    `tmc_content` TEXT NULL DEFAULT NULL ,
     PRIMARY KEY (`tmc_id`),
     INDEX `message_version_id` (`tmc_version_id`))

     --
     -- Add foreign key to TSL message version table.
     --

     ALTER TABLE `tsl_message_content`
     ADD CONSTRAINT
    `fk_tmc_version_id`
     FOREIGN KEY (`tmc_version_id`)
     REFERENCES `tsl_message_version`
     (`tm_id`) ON DELETE CASCADE ON UPDATE CASCADE;


=========================================================================================================

    (Feb-14-2019)

    --
    -- Add foreign key to Target Audience Table.
    --

    ALTER TABLE `tsl_target_audience`
     ADD  CONSTRAINT `fk_ta_tsl_id`
     FOREIGN KEY (`tta_tsl_id`) REFERENCES `tsl`(`tsl_id`) ON DELETE CASCADE ON UPDATE CASCADE;

     --
     -- Table structure for table `tsl_promo_focus`
     --

     CREATE TABLE `tsl_promo_focus` (
       `tpf_id` int(11) NOT NULL,
       `tpf_tsl_id` int(11) NOT NULL,
       `tpf_name` varchar(200) NOT NULL
     ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

     --
     -- Indexes for dumped tables
     --

     --
     -- Indexes for table `tsl_promo_focus`
     --
     ALTER TABLE `tsl_promo_focus`
       ADD PRIMARY KEY (`tpf_id`),
       ADD KEY `tpd_tsl` (`tpf_tsl_id`);

     --
     -- AUTO_INCREMENT for dumped tables
     --

     --
     -- AUTO_INCREMENT for table `tsl_promo_focus`
     --
     ALTER TABLE `tsl_promo_focus`
       MODIFY `tpf_id` int(11) NOT NULL AUTO_INCREMENT;
     --
     -- Constraints for dumped tables
     --

     --
     -- Constraints for table `tsl_promo_focus`
     --
     ALTER TABLE `tsl_promo_focus`
       ADD CONSTRAINT `fk_tpf_tsl_id` FOREIGN KEY (`tpf_tsl_id`) REFERENCES `tsl` (`tsl_id`) ON DELETE CASCADE ON UPDATE CASCADE;


    --
    -- Add Audience and Criteria Column to Promo Table.
    --

       ALTER TABLE `tsl_promo_outline`
       ADD `tpo_criteria` INT(11) NOT NULL
       AFTER `tpo_promo_focus`, ADD `tpo_audience` INT(11) NOT NULL AFTER `tpo_criteria`;



=============================================================================================================

(Feb-15-2019)

--
-- Drop Table TSL Promo Focus.
--

DROP TABLE `pst_cpd`.`tsl_promo_focus`;

--
-- Table structure for table `tsl_promo_focus`
--

CREATE TABLE `tsl_promo_focus` (
  `tpf_id` int(11) NOT NULL,
  `tpf_tool_id` int(11) NOT NULL,
  `tpf_name` varchar(200) NOT NULL,
  `tpf_origin` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tsl_promo_focus`
--
ALTER TABLE `tsl_promo_focus`
  ADD PRIMARY KEY (`tpf_id`),
  ADD KEY `tpd_tsl` (`tpf_tool_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tsl_promo_focus`
--
ALTER TABLE `tsl_promo_focus`
  MODIFY `tpf_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Drop columns for TSL message sequence
--

ALTER TABLE `tsl_message_sequence`
  DROP `tms_user_id`,
  DROP `tms_type`,
  DROP `tms_rwt`,
  DROP `tms_tsp`,
  DROP `tms_sau`;


--
-- Add columns to TSL message Sequence
--

ALTER TABLE `tsl_message_sequence`
ADD `tms_ta` INT(11) NOT NULL AFTER `tms_name`,
ADD `tms_list_criteria` INT(11) NOT NULL AFTER `tms_ta`,
ADD `tms_focus` INT(11) NOT NULL AFTER `tms_list_criteria`,
ADD `tms_client_type` VARCHAR(3) NOT NULL AFTER `tms_focus`,
ADD `tms_event` VARCHAR(5) NOT NULL AFTER `tms_client_type`,
ADD `tms_message_type` VARCHAR(5) NOT NULL AFTER `tms_event`,
ADD INDEX `tms_ta` (`tms_ta`),
ADD INDEX `tms_list_criteria` (`tms_list_criteria`),
ADD INDEX `tms_focus` (`tms_focus`),
ADD INDEX `tms_client_type` (`tms_client_type`),
ADD INDEX `tms_event` (`tms_event`),
ADD INDEX `tms_message_type` (`tms_message_type`);

--
-- Add column for phone and email to deal.
--

ALTER TABLE `pst_cpd`.`deal`
ADD COLUMN `deal_phone` VARCHAR(20) NULL AFTER `deal_when`,
ADD COLUMN `deal_email` VARCHAR(45) NULL AFTER `deal_phone`;


--
-- Add foreign key for User to TSL Promo Outline
--

ALTER TABLE `tsl_script_outline`
CHANGE `tsl_script_user_id` `tsl_script_user_id`
BIGINT(20) NOT NULL;

ALTER TABLE `tsl_script_outline`
CHANGE `tsl_script_user_id` `tsl_script_user_id`
BIGINT(20) UNSIGNED NOT NULL;

ALTER TABLE `tsl_script_outline`
ADD  CONSTRAINT `fk_outline_user`
FOREIGN KEY (`tsl_script_user_id`)
REFERENCES `pst_wp_cpd`.`wp_users`(`ID`)
ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Drop primary key field for TSL promo outline user.
--

ALTER TABLE `pst_cpd`.`tsl_script_outline`
DROP PRIMARY KEY,
ADD PRIMARY KEY (`tsl_script_outline_id`);

--
-- Add columns to TSl script outline table.
--

ALTER TABLE `tsl_script_outline`
ADD `tsl_sript_outline_ta` INT(11) NULL DEFAULT NULL AFTER `tsl_script_user_id`,
ADD `tsl_script_outline_criteria` INT(11) NULL DEFAULT NULL AFTER `tsl_sript_outline_ta`,
ADD `tsl_script_outline_focus` INT(11) NULL DEFAULT NULL AFTER `tsl_script_outline_criteria`,
ADD `tsl_script_outline_type` INT(11) NULL DEFAULT NULL AFTER `tsl_script_outline_focus`,
ADD INDEX (`tsl_sript_outline_ta`), ADD INDEX (`tsl_script_outline_criteria`),
ADD INDEX (`tsl_script_outline_focus`), ADD INDEX (`tsl_script_outline_type`);


--
-- Add foreign key to TSL script Outline.
--

ALTER TABLE `tsl_script_outline`
ADD CONSTRAINT `fk_outline_ta`
FOREIGN KEY (`tsl_sript_outline_ta`)
REFERENCES `tsl_target_audience`(`tta_id`)
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `tsl_script_outline`
ADD CONSTRAINT `fk_outline_criteria`
FOREIGN KEY (`tsl_script_outline_criteria`)
REFERENCES `tsl_criteria`(`tsl_criteria_id`)
ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `tsl_script_outline`
ADD CONSTRAINT `fk_outline_focus`
FOREIGN KEY (`tsl_script_outline_focus`)
REFERENCES `tsl_promo_focus`(`tpf_id`)
ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Change Name for script outline name.
--

ALTER TABLE `tsl_script_outline` CHANGE `tsl_sript_outline_ta` `tsl_script_outline_ta` INT(11) NULL DEFAULT NULL;


=======================================================================================================

(Feb-19-2019)

--
-- Drop columns from tsl_email table.
--

ALTER TABLE `tsl_email`
  DROP `te_type`,
  DROP `te_event`,
  DROP `te_purpose`;


========================================================================================================

(Feb-20-2019)

--
-- Add Columns to TSL email outline.
--

ALTER TABLE `tsl_email_outline`
ADD `teo_ta` INT(11) NULL DEFAULT NULL AFTER `teo_type`,
ADD `teo_criteria` INT(11) NULL DEFAULT NULL AFTER `teo_ta`,
ADD `teo_focus` INT(11) NULL DEFAULT NULL AFTER `teo_criteria`,
ADD INDEX `tei_ta_id` (`teo_ta`),
ADD INDEX `teo_criteria_id` (`teo_criteria`),
ADD INDEX `teo_focus_id` (`teo_focus`);


--
-- Change Data type for TSL email outline User id field.
--

ALTER TABLE `tsl_email_outline` CHANGE `teo_user_id` `teo_user_id` BIGINT(20) UNSIGNED NOT NULL;



--
-- Add foreign keys to email outline table.
--

ALTER TABLE `tsl_email_outline`
ADD CONSTRAINT `fk_teo_user_id`
FOREIGN KEY (`teo_user_id`)
REFERENCES `pst_wp_cpd`.`wp_users`(`ID`)
ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `tsl_email_outline`
ADD CONSTRAINT `fk_teo_ta`
FOREIGN KEY (`teo_ta`)
 REFERENCES `tsl_target_audience`(`tta_id`)
 ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `tsl_email_outline`
ADD CONSTRAINT `fk_teo_criteria`
FOREIGN KEY (`teo_criteria`)
REFERENCES `tsl_criteria`(`tsl_criteria_id`)
ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `tsl_email_outline`
ADD CONSTRAINT `fk_teo_focus`
FOREIGN KEY (`teo_focus`)
REFERENCES `tsl_promo_focus`(`tpf_id`)
ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Add foreign keys to TSL email table.
--

ALTER TABLE `tsl_email`
ADD CONSTRAINT `fk_te_outline`
FOREIGN KEY (`te_outline_id`)
REFERENCES `tsl_email_outline`(`teo_id`)
ON DELETE CASCADE ON UPDATE CASCADE;

=============================================================================================

--
-- Hide Adminisrative Tools by changing status value for tools.
--

UPDATE `tool` SET `tool_status` = 'disabled' WHERE `tool`.`tool_id` = 1;
UPDATE `tool` SET `tool_status` = 'disabled' WHERE `tool`.`tool_id` = 8;
UPDATE `tool` SET `tool_status` = 'disabled' WHERE `tool`.`tool_id` = 12;

--
-- Change the Table structure for TSL criteria Category.
--

ALTER TABLE `tsl_criteria`
DROP FOREIGN KEY fk_criteria_category_id;

ALTER TABLE `tsl_criteria`
DROP `tsl_criteria_category_id`;

Drop table tsl_criteria_category;

ALTER TABLE `tsl_criteria`
ADD `tsl_criteria_tsl_id` INT(11) NOT NULL AFTER `tsl_criteria_name`,
ADD INDEX `tsl_criteria_tsl_id` (`tsl_criteria_tsl_id`);

ALTER TABLE `tsl_criteria`
CHANGE `tsl_criteria_tsl_id` `tsl_criteria_tsl_id`
INT(11) NULL DEFAULT NULL;

ALTER TABLE `tsl_criteria`
ADD  CONSTRAINT `fk_criteria_tsl_id`
FOREIGN KEY (`tsl_criteria_tsl_id`)
REFERENCES `tsl`(`tsl_id`)
ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `tsl_client_criteria`
ADD CONSTRAINT `fk_tccrit_criteria_id`
FOREIGN KEY (`tccrit_criteria_id`)
REFERENCES `tsl_criteria`(`tsl_criteria_id`)
ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `pst_cpd`.`tsl_email_answer`
DROP FOREIGN KEY `fk_tsl_email_answer_1`;
ALTER TABLE `pst_cpd`.`tsl_email_answer`
ADD INDEX `fk_email_ans_idx` (`tea_email_id` ASC),
DROP INDEX `tea_survey_id` ;
ALTER TABLE `pst_cpd`.`tsl_email_answer`
ADD CONSTRAINT `fk_email_ans`
FOREIGN KEY (`tea_email_id`)
REFERENCES `pst_cpd`.`tsl_email` (`te_id`)
ON DELETE CASCADE
ON UPDATE CASCADE;

=================================================================================================
(Feb-26-2019)

--
-- Constraints for table `tgd_toggle_history`
--
ALTER TABLE `tgd_toggle_history`
  ADD CONSTRAINT `fk_history_ui_id` FOREIGN KEY (`tph_tgd_ui_id`) REFERENCES `tgd_user_instance` (`tgd_ui_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Table structure for table `deal_assets`
--
CREATE TABLE `deal_assets` (
  `da_id` int(11) NOT NULL,
  `da_location` text NOT NULL,
  `da_long` varchar(200) NOT NULL,
  `da_lat` varchar(200) NOT NULL,
  `da_deal_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `deal_assets`
--
ALTER TABLE `deal_assets`
  ADD PRIMARY KEY (`da_id`),
  ADD KEY `da_deal_id` (`da_deal_id`);

--
-- Constraints for table `deal_assets`
--
ALTER TABLE `deal_assets`
ADD CONSTRAINT `fk_da_deal_id` FOREIGN KEY (`da_deal_id`) REFERENCES `deal` (`deal_id`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Table Structure for deal_contacts
--
CREATE TABLE `pst_cpd`.`deal_contacts`
( `dc_id` INT(11) NOT NULL AUTO_INCREMENT ,
`dc_phone` VARCHAR(20) NOT NULL ,
`dc_deal_id` INT(11) NOT NULL ,
PRIMARY KEY (`dc_id`),
INDEX `dc_deal_id` (`dc_deal_id`))
ENGINE = InnoDB;

--
-- Add foreign key to deal_contacts.
--
ALTER TABLE `deal_contacts`
ADD CONSTRAINT `fk_dc_deal_id`
FOREIGN KEY (`dc_deal_id`)
REFERENCES `deal`(`deal_id`) ON DELETE CASCADE ON UPDATE CASCADE;



===================================================================================================

(Feb-27-2019)

--
-- Table structure for table `homebase_client`
--

CREATE TABLE `homebase_client` (
  `hc_id` int(11) NOT NULL,
  `hc_user_id` bigint(20) UNSIGNED NOT NULL,
  `hc_name` varchar(200) DEFAULT NULL,
  `hc_main_phone` varchar(20) NOT NULL,
  `hc_mobile_phone` varchar(20) DEFAULT NULL,
  `hc_client_email` varchar(200) NOT NULL,
  `hc_website` varchar(200) DEFAULT NULL,
  `hc_social_media` varchar(400) DEFAULT NULL,
  `hc_product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `homebase_client`
--
ALTER TABLE `homebase_client`
  ADD PRIMARY KEY (`hc_id`),
  ADD KEY `hc_user_id` (`hc_user_id`),
  ADD KEY `hc_product_id` (`hc_product_id`);

--
-- AUTO_INCREMENT for table `homebase_client`
--
ALTER TABLE `homebase_client`
MODIFY `hc_id` int(11) NOT NULL AUTO_INCREMENT;


--
-- Constraints for table `homebase_client`
--
ALTER TABLE `homebase_client`
  ADD CONSTRAINT `fk_hc_product_id` FOREIGN KEY (`hc_product_id`)
  REFERENCES `user_product` (`u_product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hc_user` FOREIGN KEY (`hc_user_id`)
  REFERENCES `pst_wp_cpd`.`wp_users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Table structure for table `homebase_client_asset`
--

CREATE TABLE `homebase_client_asset` (
  `hca_id` int(11) NOT NULL,
  `hca_client_id` int(11) NOT NULL,
  `hca_name` varchar(200) DEFAULT NULL,
  `hca_location` varchar(500) DEFAULT NULL,
  `hca_address` text,
  `hca_criteria_used` int(11) NOT NULL,
  `hca_contact` varchar(100) NOT NULL,
  `hca_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Indexes for table `homebase_client_asset`
--
ALTER TABLE `homebase_client_asset`
  ADD PRIMARY KEY (`hca_id`),
  ADD KEY `hca_client_id` (`hca_client_id`),
  ADD KEY `hca_criteria` (`hca_criteria_used`),
  ADD KEY `hca_status` (`hca_status`);

--
-- AUTO_INCREMENT for table `homebase_client_asset`
--
    ALTER TABLE `homebase_client_asset`
    MODIFY `hca_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for table `homebase_client_asset`
--
ALTER TABLE `homebase_client_asset`
  ADD CONSTRAINT `fk_hca_client_id` FOREIGN KEY (`hca_client_id`) REFERENCES `homebase_client` (`hc_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_hca_criteria` FOREIGN KEY (`hca_criteria_used`) REFERENCES `tsl_criteria` (`tsl_criteria_id`) ON DELETE CASCADE ON UPDATE CASCADE;


========================================================================================
(Mar-05-2019)
--
-- Table structure for table `tsl_clients`
--

CREATE TABLE `tsl_clients` (
  `tc_id` int(11) NOT NULL,
  `tc_homebase_id` int(11) NOT NULL,
  `tc_tsl_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `tsl_clients`
--
ALTER TABLE `tsl_clients`
  ADD PRIMARY KEY (`tc_id`),
  ADD KEY `tc_homebase_id` (`tc_homebase_id`),
  ADD KEY `tc_tsl_id` (`tc_tsl_id`);

--
-- AUTO_INCREMENT for table `tsl_clients`
--
ALTER TABLE `tsl_clients`
  MODIFY `tc_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for table `tsl_clients`
--
ALTER TABLE `tsl_clients`
ADD CONSTRAINT `fk_tc_homebase_id`
FOREIGN KEY (`tc_homebase_id`)
REFERENCES `homebase_client_asset` (`hca_id`)
ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_tc_tsl_id`
FOREIGN KEY (`tc_tsl_id`)
REFERENCES `tsl` (`tsl_id`)
ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Alter foreign key for TSL about client.
--
ALTER TABLE `tsl_about_client`
DROP FOREIGN KEY `fk_about_client`;
ALTER TABLE `tsl_about_client`
ADD CONSTRAINT `fk_about_client`
FOREIGN KEY (`tac_tc_id`)
REFERENCES `tsl_client`(`tc_id`)
ON DELETE CASCADE ON UPDATE CASCADE;

================================================================================

(Mar-07-2019)

--
-- Add income columns to homebase asset table.
--
ALTER TABLE `homebase_client_asset`
 ADD `hca_price` DECIMAL(10) NOT NULL AFTER `hca_lat`,
 ADD `hca_rate` INT(3) NOT NULL AFTER `hca_price`,
 ADD `hca_gross` DECIMAL(10) NOT NULL AFTER `hca_rate`,
 ADD `hca_split` INT(3) NOT NULL AFTER `hca_gross`,
 ADD `hca_gsplit` INT(3) NOT NULL AFTER `hca_split`,
 ADD `hca_gnet` DECIMAL(10) NOT NULL AFTER `hca_gsplit`,
 ADD `hca_net` DECIMAL(10) NOT NULL AFTER `hca_gnet`;


 =======================================================================
(Marr-08-2019)
 --
 -- Add promo universal components.
 --


 INSERT INTO `universal_component` (`uc_id`, `uc_name`, `uc_controller`, `uc_icon`, `uc_table`, `uc_col_name`, `uc_ref_primary_key`, `uc_type`) VALUES (NULL, 'Vmail', '/messages/', 'envelope', NULL, NULL, '', 'promo'), (NULL, 'Script', '/script/', 'list-alt', NULL, NULL, '', 'promo'), (NULL, 'Email', '/email/', 'envelope', NULL, NULL, '', 'promo'), (NULL, 'Stories', '/stories/', 'list-alt', NULL, NULL, '', 'promo')


============================================================================================
(Mar-11-2019)

--
-- Set default for client asset table columns.
--

ALTER TABLE `pst_cpd`.`homebase_client_asset`
CHANGE COLUMN `hca_criteria_used` `hca_criteria_used` INT(11) NULL DEFAULT NULL ,
CHANGE COLUMN `hca_contact` `hca_contact` VARCHAR(100) NULL ,
CHANGE COLUMN `hca_lat` `hca_lat` VARCHAR(50) NULL DEFAULT NULL ,
CHANGE COLUMN `hca_price` `hca_price` DECIMAL(10,0) NOT NULL DEFAULT 0 ,
CHANGE COLUMN `hca_rate` `hca_rate` INT(3) NOT NULL DEFAULT 0 ,
CHANGE COLUMN `hca_gross` `hca_gross` DECIMAL(10,0) NOT NULL DEFAULT 0 ,
CHANGE COLUMN `hca_split` `hca_split` INT(3) NOT NULL DEFAULT 0 ,
CHANGE COLUMN `hca_gsplit` `hca_gsplit` INT(3) NOT NULL DEFAULT 0 ,
CHANGE COLUMN `hca_gnet` `hca_gnet` DECIMAL(10,0) NOT NULL DEFAULT 0 ,
CHANGE COLUMN `hca_net` `hca_net` DECIMAL(10,0) NOT NULL DEFAULT 0 ,
CHANGE COLUMN `hca_lng` `hca_lng` VARCHAR(50) NULL DEFAULT NULL ;


===============================================================================================
(Mar-13-2019)

--
-- Alter foreign key for ACM
--
ALTER TABLE `table_name` DROP FOREIGN KEY `id_name_fk`;

ALTER TABLE `cpd_acm_deal` ADD  CONSTRAINT `fk_cad_deal` FOREIGN KEY (`cad_deal_id`) REFERENCES `homebase_client_asset`(`hca_id`) ON DELETE CASCADE ON UPDATE CASCADE;


=============================================================================================
(Mar-14-2019)

--
-- Alter the code table for deal status order.
--

UPDATE `code` SET `code_id` = '29' WHERE `code`.`code_id` = 103;


--
-- Insert Buyer Tabs to code type.
--

INSERT INTO `code` (`code_id`, `code_type_id`, `code_abbrev`, `code_name`, `code_description`) VALUES
(99, 28, 'p_complete', 'Profile Completed', 'Buyer Profile Completed'),
(100, 28, 'p_requested', 'Profile Requested', 'Buyer Profile Requested'),
(101, 28, 'accepted_l', 'LOI Accepted', 'Buyer LOI Accepted'),
(102, 28, 'submitted_l', 'LOI Submitted', 'Buyer LOI Submitted'),
(103, 28, 'accepted_o', 'Offer Accepted', 'Offer Accepted'),
(104, 28, 'submitted_o', 'Offer Submitted', 'Offer Submitted');


=============================================================================================

(Mar-25-2019)

--
-- Add foreign key to TSL about client.
--

ALTER TABLE `pst_cpd`.`tsl_about_client`
ADD CONSTRAINT `fk_about_client`
  FOREIGN KEY (`tac_tc_id`)
  REFERENCES `pst_cpd`.`tsl_client` (`tc_id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

--
-- Add foreign key to TSL client table
--

ALTER TABLE `pst_cpd`.`tsl_client`
ADD CONSTRAINT `fk_tc_homebase_id`
  FOREIGN KEY (`tc_asset_id`)
  REFERENCES `pst_cpd`.`homebase_client_asset` (`hca_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_tc_tsl_id`
  FOREIGN KEY (`tc_tsl_id`)
  REFERENCES `pst_cpd`.`tsl` (`tsl_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

=============================================================================================

(Mar-26-2019)

--
-- Update deal table foreign key constraints.
--

ALTER TABLE `deal` DROP FOREIGN KEY `fk_deal_gen`;
ALTER TABLE `deal` ADD CONSTRAINT `fk_deal_gen`
FOREIGN KEY (`deal_gen`) REFERENCES `deal_lead`(`dl_id`)
ON DELETE SET NULL ON UPDATE SET NULL;
ALTER TABLE `deal` DROP FOREIGN KEY `fk_deal_leads`;
ALTER TABLE `deal` ADD CONSTRAINT `fk_deal_leads`
FOREIGN KEY (`deal_lead`) REFERENCES `deal_lead`(`dl_id`)
ON DELETE SET NULL ON UPDATE SET NULL;

ALTER TABLE `deal`
ADD CONSTRAINT `fk_deal_asset_id` FOREIGN KEY (`deal_asset_id`) REFERENCES `homebase_client_asset` (`hca_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_deal_cpd_id` FOREIGN KEY (`deal_cpd_id`) REFERENCES `cpd` (`cpd_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_deal_gen` FOREIGN KEY (`deal_gen`) REFERENCES `deal_lead` (`dl_id`) ON DELETE SET NULL ON UPDATE SET NULL,
ADD CONSTRAINT `fk_deal_leads` FOREIGN KEY (`deal_lead`) REFERENCES `deal_lead` (`dl_id`) ON DELETE SET NULL ON UPDATE SET NULL;

===================================================================================================

(Mar-28-2019)

--
-- Add foreign key to CPD deal info table.
--

ALTER TABLE `cpd_deal_info` ADD CONSTRAINT `fk_cc_deal_id` FOREIGN KEY (`cc_deal_id`) REFERENCES `homebase_client_asset`(`hca_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Alter foreign key for Deal Notes.
--

ALTER TABLE `deal_notes` DROP FOREIGN KEY `fk_notes_deal`;
 ALTER TABLE `deal_notes` ADD CONSTRAINT `fk_notes_deal` FOREIGN KEY (`dn_deal_id`) REFERENCES `homebase_client_asset`(`hca_id`) ON DELETE CASCADE ON UPDATE CASCADE;

 =================================================================================================

 (Apr-01-2019)

 --
 -- Add foreign key to TSL promo answer.
 --

 ALTER TABLE `tsl_promo_answer`
 ADD  CONSTRAINT `fk_promo_id`
 FOREIGN KEY (`tpa_promo_id`)
 REFERENCES `tsl_promo`(`tp_id`)
 ON DELETE CASCADE ON UPDATE CASCADE;


 ===================================================================================================

 (Apr-04-2019)

 --
 -- Add foreign key to TSL Script Name table.
 --
ALTER TABLE `tsl_script_name`
ADD CONSTRAINT `fk_tsl_version_id`
FOREIGN KEY (`tsn_version_id`)
REFERENCES `tsl_script_outline`(`tsl_script_outline_id`)
ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Add foreign key to TSL script question
--
ALTER TABLE `tsl_script_question`
ADD  CONSTRAINT `fk_question_outline_id`
FOREIGN KEY (`tsl_script_question_outline_id`)
REFERENCES `tsl_script_outline`(`tsl_script_outline_id`)
ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Add foreign keys to TSL script client.
--
ALTER TABLE `pst_cpd`.`tsl_script_client`
ADD CONSTRAINT `fk_tsc_script_id`
FOREIGN KEY (`tsc_script_id`)
REFERENCES `pst_cpd`.`tsl_script` (`ts_id`)
ON DELETE CASCADE
ON UPDATE CASCADE;'

--
-- Add foreign key to TSL script.
--
ALTER TABLE `pst_cpd`.`tsl_script`
ADD CONSTRAINT `fk_ts_name_id`
FOREIGN KEY (`ts_name_id`)
REFERENCES `pst_cpd`.`tsl_script_name` (`tsn_id`)
ON DELETE CASCADE
ON UPDATE CASCADE;

--
-- Add foreign key to TSL script Perf Prof
--
ALTER TABLE `pst_cpd`.`tsl_script_perf_prof`
ADD CONSTRAINT `fk_scores_script_id`
FOREIGN KEY (`tspp_script_client_id`)
REFERENCES `pst_cpd`.`tsl_script_client` (`tsc_id`)
ON DELETE CASCADE
ON UPDATE CASCADE;


--
-- TSL survey
--

ALTER TABLE `tsl_survey_outline`
ADD  CONSTRAINT `fk_tso_user_id`
FOREIGN KEY (`tso_user_id`)
REFERENCES `pst_wp_cpd`.`wp_users`(`ID`)
ON DELETE CASCADE ON UPDATE CASCADE;


=================================================================================
(Apr-09-2019)

--
-- Add PDF view column to Promo table.
--

ALTER TABLE `pst_cpd`.`tsl_promo`
ADD COLUMN `tp_pdf_view` VARCHAR(100) NULL DEFAULT NULL AFTER `tp_content`;


=================================================================================
(Apr-10-2019)

--
-- Change the default outline for CPD SA front page.
--

UPDATE `cpd_sa_default`
SET `csd_content` =
'<h1><!--block-->{{ Title }}</h1><div><!--block--><strong>{{ Document Description }}</strong></div><blockquote><!--block--><strong>{{ Client Name }}</strong> <br><strong>{{ Property Name }}</strong> <br><strong>{{ Property Location }}</strong> <br><strong>{{ Designated Advisor }}</strong><br><strong>{{ Submission Date }}</strong></blockquote>'
 WHERE `cpd_sa_default`.`csd_id` = 1;


--
-- Change the default outline for CPD SA welcome page.
--

UPDATE `cpd_sa_default`
SET `csd_content` = '<h1>[[WELCOME]]</h1><div>{{Dear Client Name,}}<br><br>{{Lead Paragraph}}<br><br>{{Second Paragraph}}<br><br>{{Third Paragraph}}<br><br>{{Salutation}}<br><br></div><div>{{Agent Name}}<br>{{Company Name}}</div>' WHERE `cpd_sa_default`.`csd_id` = 2;


=================================================================================
(Apr-11-2019)

--
-- Change the outline fro CPA SA Project Page.
--

UPDATE `cpd_sa_default`
SET `csd_content` = '<h1>[[PROJECT OVERVIEW]]</h1><div><br>[[Your Project Is Our Mission]]<br><br></div><div>{{Historical Overview}}<br>{{Current Facts}}<br>{{Accomplishments}}<br>{{Problems}}<br>{{Project}}</div>' WHERE `cpd_sa_default`.`csd_id` = 3;

--
-- Change outline for CPD SA Analysis page.
--

UPDATE `cpd_sa_default`
SET `csd_content` = '<h1>[[STRATEGIC ANALYSIS]]</h1><div>[[Column 1]]</div><div><strong>[[{{Title}}]]</strong></div><div><strong>{{Factor 1: }}</strong><br>{{Challenge &amp; Implications Description}}<br><strong>{{Factor 2: }}</strong><br>{{Challenge &amp; Implications Description}}<br><strong>{{Factor 3: }}</strong><br>{{Challenge &amp; Implications Description}}<br><strong>{{Factor 4: }}</strong><br>{{Challenge &amp; Implications Description}}<br><strong>{{Factor 5: }}</strong><br>{{Challenge &amp; Implications Description}}<br>[[Column 2]]</div><div><strong>[[{{Title}}]]</strong></div><div><strong>{{Option 1: }}</strong><br>{{Description of Consequences}}<br><strong>{{Option 2: }}</strong><br>{{Description of Consequences}}<br><strong>{{Option 3: }}</strong><br>{{Description of Consequences}}<br><strong>{{Option 4: }}</strong><br>{{Description of Consequences}}<br><strong>{{Option 5: }}</strong><br>{{Description of Consequences}}</div>' WHERE `cpd_sa_default`.`csd_id` = 4;


============================================================================
(Apr-12-2019)

--
-- Add updates to TSL universal component.
--

INSERT INTO `universal_component`
(`uc_id`, `uc_name`, `uc_controller`, `uc_icon`, `uc_table`, `uc_col_name`, `uc_ref_primary_key`, `uc_type`, `uc_tool`)
VALUES (NULL, 'Updates', '/updates/', 'book', 'updates', 'update_name', 'update_id', 'send', 'tsl');

--
-- Add updates to CPD universal component.
--

INSERT INTO `universal_component`
(`uc_id`, `uc_name`, `uc_controller`, `uc_icon`, `uc_table`, `uc_col_name`, `uc_ref_primary_key`, `uc_type`, `uc_tool`)
VALUES (NULL, 'Updates', '/updates/', 'book', 'updates', 'update_name', 'update_id', 'send', 'cpdv1');

 --
 -- Add Profile to TSL universal comoponent
 --
 INSERT INTO `universal_component`
(`uc_id`, `uc_name`, `uc_controller`, `uc_icon`, `uc_table`, `uc_col_name`, `uc_ref_primary_key`, `uc_type`, `uc_tool`)
VALUES (NULL, 'Profile', '/profile/', 'user', 'cpd_profile', 'cpd_profile_name', 'cpd_profile_id', 'send', 'tsl');

--
-- Add Profile to CPD universal component.
--

INSERT INTO `universal_component`
(`uc_id`, `uc_name`, `uc_controller`, `uc_icon`, `uc_table`, `uc_col_name`, `uc_ref_primary_key`, `uc_type`, `uc_tool`)
VALUES (NULL, 'Profile', '/profile/', 'user', 'cpd_profile', 'cpd_profile_name', 'cpd_profile_id', 'send', 'cpdv1');

--
-- Remove Web from universal component.
--

DELETE FROM `universal_component` WHERE `universal_component`.`uc_id` = 6;
DELETE FROM `universal_component` WHERE `universal_component`.`uc_id` = 21;

==========================================================================================================
(Apr-15-2019)

--
-- Change default outline for SAP Recommendation page.
--

UPDATE `cpd_sa_default`
SET `csd_content` = '<div>[[RECOMMENDATION]]<br><strong>[[{{Title}}]]</strong><br><strong>{{Option 5: }}</strong><br>{{Challenge &amp; Implications Description}}<br><br><strong>{{Partnership Title}}<br></strong>{{Our Partnership}}</div>'
 WHERE `cpd_sa_default`.`csd_id` = 5;

--
-- Change the SAP image column name.
--

ALTER TABLE `cpd_sa_data`
CHANGE `cs_data_image` `cs_data_image_1` VARCHAR(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

--
-- Add column for TGD cell labels.
--

ALTER TABLE `tgd_user_instance`
ADD `tgd_ui_label_1` VARCHAR(100) NOT NULL DEFAULT 'Cumulative Income' AFTER `tgd_ui_prev_year_id`,
ADD `tgd_ui_label_2` VARCHAR(100) NOT NULL DEFAULT 'Deals in Contract' AFTER `tgd_ui_label_1`,
ADD `tgd_ui_label_3` VARCHAR(100) NOT NULL DEFAULT 'Additional Listings' AFTER `tgd_ui_label_2`,
ADD `tgd_ui_label_4` VARCHAR(100) NOT NULL DEFAULT 'Proposals in Play' AFTER `tgd_ui_label_3`;

--
-- Update outline for SAP map page.
--

UPDATE `cpd_sa_default`
SET `csd_content` = '<div>{{PROJECT OVERVIEW}}&nbsp;</div><div>[[Center Box Program Map]]</div><div>{{Header Intro Line}}</div><div>{{Introduction}}</div><div>{{Positioning Statement}}</div><div>{{Bold Statement}}</div>'
WHERE `cpd_sa_default`.`csd_id` = 6;


==================================================================================
(Apr-16-2019)

--
-- Table structure for table `collab_recipient`
--

CREATE TABLE `collab_recipient` (
  `collab_recipient_id` int(11) NOT NULL,
  `collab_recipient_name` varchar(200) NOT NULL,
  `collab_recipient_designation` varchar(200) NOT NULL,
  `collab_recipient_default` int(1) NOT NULL DEFAULT '0',
  `collab_recipient_user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for table `collab_recipient`
--
ALTER TABLE `collab_recipient`
  ADD PRIMARY KEY (`collab_recipient_id`),
  ADD KEY `collab_recipient_default` (`collab_recipient_default`),
  ADD KEY `recipient_user_id` (`collab_recipient_user_id`);

--
-- AUTO_INCREMENT for table `collab_recipient`
--
ALTER TABLE `collab_recipient`
  MODIFY `collab_recipient_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for table `collab_recipient`
--
ALTER TABLE `collab_recipient`
  ADD CONSTRAINT `fk_recipient_user_id` FOREIGN KEY (`collab_recipient_user_id`) REFERENCES `pst_wp_cpd`.`wp_users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;


================================================================================
(Apr-19-2019)

--
-- Alter CPD SA default  content
--

INSERT INTO `cpd_sa_default`
(`csd_id`, `csd_page_template`, `csd_template_version`, `csd_content`) VALUES
(1, 'lp', 'fpp', '<h1>{{ Title }}</h1><div><strong>{{ Document Description }}</strong></div><blockquote><strong>{{ Client Name: }}</strong> <br><strong>{{ Property Name: }}</strong> <br><strong>{{ Property Location: }}</strong> <br><strong>{{ Designated Advisor: }}</strong><br><strong>{{ Submission Date: }}</strong></blockquote>'),
(2, 'wp', 'lpl', '<h1>[[WELCOME]]</h1><div>{{Dear Client Name,}}<br><br>{{Lead Paragraph}}<br><br>{{Second Paragraph}}<br><br>{{Third Paragraph}}<br><br>{{Salutation}}<br><br></div><div>{{Agent Name}}<br>{{Company Name}}</div>'),
(3, 'pp', '5pl2pbl', '<div>[[Your Project Is Our Mission!]]</div><div>{{History}}<br>{{Current Facts}}<br>{{Accomplishments}}<br>{{Problems}}<br>{{Project}}</div>'),
(4, 'ap', '5plBph', '<h1>[[STRATEGIC ANALYSIS]]</h1><div>[[Column 1]]</div><div><strong>[[{{Title}}]]</strong></div><div><strong>{{Factor 1: }}</strong><br>{{Challenge &amp; Implications Description}}<br><strong>{{Factor 2: }}</strong><br>{{Challenge &amp; Implications Description}}<br><strong>{{Factor 3: }}</strong><br>{{Challenge &amp; Implications Description}}<br><strong>{{Factor 4: }}</strong><br>{{Challenge &amp; Implications Description}}<br><strong>{{Factor 5: }}</strong><br>{{Challenge &amp; Implications Description}}<br>[[Column 2]]</div><div><strong>[[{{Title}}]]</strong></div><div><strong>{{Option 1: }}</strong><br>{{Description of Consequences}}<br><strong>{{Option 2: }}</strong><br>{{Description of Consequences}}<br><strong>{{Option 3: }}</strong><br>{{Description of Consequences}}<br><strong>{{Option 4: }}</strong><br>{{Description of Consequences}}<br><strong>{{Option 5: }}</strong><br>{{Description of Consequences}}</div>'),
(5, 'rp', '1ptc', '<div>[[RECOMMENDATION]]<br><strong>[[{{Title}}]]</strong><br><strong>{{Option 5: }}</strong><br>{{Challenge &amp; Implications Description}}<br><br><strong>[[{{Partnership Title}}]]<br></strong>{{Our Partnership}}</div>'),
(6, 'amp', 'tlm', '<h1>{{Header Intro Line}}</h1><div>{{Introduction}}</div><blockquote>{{Positioning Statement}}</blockquote>');


===================================================================================
(Apr-22-2019)

--
-- Drop TGD labels columns
--
ALTER TABLE `tgd_user_instance` DROP `tgd_ui_label_1`, DROP `tgd_ui_label_2`, DROP `tgd_ui_label_3`, DROP `tgd_ui_label_4`;


--
-- Insert the default for SAP promo doc.
--
INSERT INTO `pst_cpd`.`cpd_sa`
(`cs_id`, `cs_type`, `cs_name`, `cs_random_name`, `cs_user_id`, `cs_client_id`)
VALUES ('0', 'none', 'Default', 'promo1555936818', '18', '0');



--
-- CPD SA default outline table.
--

Drop table cpd_sa_default;

--
-- Table structure for table `cpd_sa_outline`
--

CREATE TABLE `cpd_sa_outline` (
  `csd_id` int(11) NOT NULL,
  `csd_page_template` varchar(20) NOT NULL,
  `csd_template_version` varchar(20) NOT NULL,
  `csd_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cpd_sa_outline`
--

INSERT INTO `cpd_sa_outline` (`csd_id`, `csd_page_template`, `csd_template_version`, `csd_content`) VALUES
(1, 'lp', 'fpp', '<h1>{{ Title }}</h1><div><strong>{{ Document Description }}</strong></div><blockquote><strong>{{ Client Name: }}</strong> <br><strong>{{ Property Name: }}</strong> <br><strong>{{ Property Location: }}</strong> <br><strong>{{ Designated Advisor: }}</strong><br><strong>{{ Submission Date: }}</strong></blockquote>'),
(2, 'wp', 'lpl', '<h1>[[WELCOME]]</h1><div>{{Dear Client Name,}}<br><br>{{Lead Paragraph}}<br><br>{{Second Paragraph}}<br><br>{{Third Paragraph}}<br><br>{{Salutation}}<br><br></div><div>{{Agent Name}}<br>{{Company Name}}</div>'),
(3, 'pp', '5pl2pbl', '<div>[[Your Project Is Our Mission!]]</div><div>{{History}}<br>{{Current Facts}}<br>{{Accomplishments}}<br>{{Problems}}<br>{{Project}}</div>'),
(4, 'ap', '5plBph', '<h1>[[STRATEGIC ANALYSIS]]</h1><div>[[Column 1]]</div><div><strong>[[{{Title}}]]</strong></div><div><strong>{{Factor 1: }}</strong><br>{{Challenge &amp; Implications Description}}<br><strong>{{Factor 2: }}</strong><br>{{Challenge &amp; Implications Description}}<br><strong>{{Factor 3: }}</strong><br>{{Challenge &amp; Implications Description}}<br><strong>{{Factor 4: }}</strong><br>{{Challenge &amp; Implications Description}}<br><strong>{{Factor 5: }}</strong><br>{{Challenge &amp; Implications Description}}<br>[[Column 2]]</div><div><strong>[[{{Title}}]]</strong></div><div><strong>{{Option 1: }}</strong><br>{{Description of Consequences}}<br><strong>{{Option 2: }}</strong><br>{{Description of Consequences}}<br><strong>{{Option 3: }}</strong><br>{{Description of Consequences}}<br><strong>{{Option 4: }}</strong><br>{{Description of Consequences}}<br><strong>{{Option 5: }}</strong><br>{{Description of Consequences}}</div>'),
(5, 'rp', '1ptc', '<div>[[RECOMMENDATION]]<br><strong>[[{{Title}}]]</strong><br><strong>{{Option 5: }}</strong><br>{{Challenge &amp; Implications Description}}<br><br><strong>[[{{Partnership Title}}]]<br></strong>{{Our Partnership}}</div>'),
(6, 'amp', 'tlm', '<h1>{{Header Intro Line}}</h1><div>{{Introduction}}</div><blockquote>{{Positioning Statement}}</blockquote>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cpd_sa_outline`
--
ALTER TABLE `cpd_sa_outline`
  ADD PRIMARY KEY (`csd_id`),
  ADD KEY `csd_type` (`csd_page_template`,`csd_template_version`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cpd_sa_outline`
--
ALTER TABLE `cpd_sa_outline`
  MODIFY `csd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


--
-- Table structure for table `cpd_sa_default`
--

CREATE TABLE `cpd_sa_default` (
  `csd_id` int(11) NOT NULL,
  `csd_template` varchar(10) NOT NULL,
  `csd_cols` int(11) NOT NULL,
  `csd_image1` varchar(100) DEFAULT NULL,
  `csd_image2` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cpd_sa_default`
--

INSERT INTO `cpd_sa_default` (`csd_id`, `csd_template`, `csd_cols`, `csd_image1`, `csd_image2`) VALUES
(1, 'lp', 7, 'Depositphotos_4539003_original.jpg', ''),
(2, 'wp', 7, 'Depositphotos_33842317_original.jpg', ''),
(3, 'pp', 5, 'Depositphotos_23237044_original.jpg', ''),
(4, 'ap', 22, '', ''),
(5, 'rp', 5, 'Depositphotos_1462985_original.jpg', 'Depositphotos_1462985_original(1).jpg'),
(6, 'amp', 3, 'Depositphotos_3793843_original.jpg', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cpd_sa_default`
--
ALTER TABLE `cpd_sa_default`
  ADD PRIMARY KEY (`csd_id`),
  ADD KEY `csd_template` (`csd_template`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cpd_sa_default`
--
ALTER TABLE `cpd_sa_default`
  MODIFY `csd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;


--
-- Table structure for table `cpd_sa_default_data`
--

CREATE TABLE `cpd_sa_default_data` (
  `csdd_id` int(11) NOT NULL,
  `csdd_csd_id` int(11) NOT NULL,
  `csdd_col` int(11) NOT NULL,
  `csdd_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cpd_sa_default_data`
--

INSERT INTO `cpd_sa_default_data` (`csdd_id`, `csdd_csd_id`, `csdd_col`, `csdd_content`) VALUES
(1, 1, 1, 'STRATEGIC ANALYSIS'),
(2, 1, 2, 'The purpose of this advisory analysis is to give your client team a clear understanding of where you currently stand with this property and of the options available to you at this time.'),
(3, 1, 3, 'Client Name'),
(4, 1, 4, 'Property Name'),
(5, 1, 5, 'Property Location'),
(6, 1, 6, 'Designated Advisor'),
(7, 1, 7, 'Submission Date'),
(8, 2, 1, 'Dear Client Name,'),
(9, 2, 2, 'We appreciate the opportunity of providing you with our in depth strategic analysis of your property and its position in this current market. There is always more to know but you have been very helpful in providing us with the information we needed. If it’s okay with you we will give you our honest no holds barred assessment of your property and overall situation so you have a better understanding of where you stand at this time.'),
(10, 2, 3, 'As the leading specialists in the ________ industry, our primary interest is in giving you a clear understanding of the driving forces in the current marketplace. This analysis will provide you a complete set of options and our recommended best course of action that we feel will serve your best interest at this time.'),
(11, 2, 4, 'Regardless of which option you choose we have given you the most accurate evaluation possible and we know that whether it be today, tomorrow or several years from now, we are available to you as your advisor and partner every step of the way to assist you in implementing the course of action you choose.'),
(12, 2, 5, 'Sincerely,'),
(13, 2, 6, 'Agent Name'),
(14, 2, 7, 'Company Name'),
(15, 3, 1, 'We take your project very seriously. As we have learned in our conversations with you the road travelled since you bought your property in 2003 has not always been good. The economic downturn of a few years ago hit everyone hard yet many like yourself have been able to rebound in fine style.'),
(16, 3, 2, 'The facts are you now are looking to exit ownership of your property and value may be as high at it will be for many years to come. There is currently great demand for your property and you are considering disposition of your property and reinvesting into a high\r\nperforming, long term security, low management asset.'),
(17, 3, 3, 'What you have accomplished is bringing your property up to top condition physically, high occupancy and vigorous cash flow. We salute you for positioning yourself to take full advantage of the current market. This strategic analysis offers you a clear picture of all of the options available to you at this time.'),
(18, 3, 4, 'Regardless of which option you choose this document will provide a good starting point for you and your management team. We will then work with you to further position your property in the best way possible.'),
(19, 3, 5, 'As a result of our work together you will get away from the burdens of management you currently endure and the pressure of looming debt. We will help you establish new situation in which you can enjoy a partial retirement and stable long term cash flow for many years to come. This will allow you to begin the process of piecing together the lifestyle you aspire to have and ensure that you establish a family legacy that will give you the peace of mind you seek'),
(20, 4, 1, 'Factors Affecting Your Situation'),
(21, 4, 2, 'Desire for Retirement'),
(22, 4, 3, 'The challenge is that you have made it eminently clear that you desire to retire as soon as possible. One implication is that you must move quickly at this time to achieve sufficient cash flow to retire in style for the long term. Another is that failure to move with this market may require your keeping the property for many years to come.'),
(23, 4, 4, 'Market Analysis'),
(24, 4, 5, 'The challenge is that the window of opportunity in the market is open for you to cash out at a high level of return. One implications is that you can still reinvest at a low interest rate for the long term to maximize cash flow. Another implication is that both market lift and interest rates are likely to significantly change within the next year or two.'),
(25, 4, 6, 'Management Challenges'),
(26, 4, 7, 'The challenge is that continued ownership will require your full time management. The implication is that you may not have the chance to retire for some time or you will have to pay for costly outside management in order to ensure the job is getting done well.'),
(27, 4, 8, 'Partners Want Out'),
(28, 4, 9, 'The challenge is that your partners are not willing to continue for the long term. The implications are that you could be left with the property to manage yourself. Another is that a great deal of work will be needed to keep afloat in the face of impending market and economic changes.'),
(29, 4, 10, 'Proximity to Family'),
(30, 4, 11, 'The challenge is that they are not local to you and in order to see them you must travel a long distance on a monthly basis. The implication is that your leaving town management of the property in the hands of your less qualified staff. Another is that you will wear yourself out with the travel and your currently high property value could deteriorate.'),
(31, 4, 12, 'Your Available Options'),
(32, 4, 13, 'Do Nothing – Wait and See'),
(33, 4, 14, 'The consequence of this option is that you will have an uncertain future retirement and property value can only diminish from this point on.'),
(34, 4, 15, 'Hold Property - Make Improvements'),
(35, 4, 16, 'The consequence of this option is that this will take money out of pocket which will take years to recoup even if you sell.'),
(36, 4, 17, 'Refinance Property - Make Improvements'),
(37, 4, 18, 'The consequence of this option is that you will be adding to your debt load and this could tie you to the property for some time to come.'),
(38, 4, 19, 'Sell Property – Do Not Reinvest'),
(39, 4, 20, 'The consequence of this option is that you will be stuck with a set amount of money for retirement after taxes which could run out.'),
(40, 4, 21, 'Sell Property – Reinvest: No Management Asset'),
(41, 4, 22, 'The consequence of this option is that you will exit the business and set yourself up for freedom to make retirement choices.'),
(42, 5, 1, 'Our Recommendation'),
(43, 5, 2, 'Sell Property – Reinvest: No Management Asset'),
(44, 5, 3, 'We recommend this option because your project is to exit this management intensive business and move toward a retirement in which you will be able to be free to choose what you want to do, where you want to go whenever you choose. Any of the other options will prolong the inevitable change you are looking to make and may put you in a more challenging market situation that could postpone that change indefinitely'),
(45, 5, 4, 'Our Partnership'),
(46, 5, 5, 'We have clarified the risks of the options available to you at this time and the consequences for which you will need to take\r\nresponsibility. By taking responsibility for keeping us informed as to the challenges affecting your situation, we will be in a better\r\nposition to ensure you make the best choices at the right time. We know you can do this and are committed to ensuring your optimal position. \r\nWe invite you to consider your options and our recommended best course of action. We promise to stand by you whatever\r\none you choose and together we will maximize your opportunity to make the transition from where you are today to where you want to be in the future in the most effective way.'),
(47, 6, 1, 'Your Strategic Advisory Program'),
(48, 6, 2, 'We offer a sequence of advisory meetings in person, on the web or by phone. In conjunction with our meetings we provide one or all of the following Documents and then regular monthly or quarterly updates as changes emerge.'),
(49, 6, 3, 'Initial Strategic Analysis\r\nMarket Analysis\r\nInvestment Analysis\r\nOperations Analysis\r\nDemand Analysis\r\nValue Analysis\r\nMarketing Proposal');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cpd_sa_default_data`
--
ALTER TABLE `cpd_sa_default_data`
  ADD PRIMARY KEY (`csdd_id`),
  ADD KEY `csdd_csd_id` (`csdd_csd_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cpd_sa_default_data`
--
ALTER TABLE `cpd_sa_default_data`
  MODIFY `csdd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cpd_sa_default_data`
--
ALTER TABLE `cpd_sa_default_data`
  ADD CONSTRAINT `fk_csd_id` FOREIGN KEY (`csdd_csd_id`) REFERENCES `cpd_sa_default` (`csd_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Update TGD Table content.
--

UPDATE `tgd_condition` SET `tgd_c_name` = 'POSSIBLE' WHERE `tgd_condition`.`tgd_c_id` = 47;

UPDATE `tgd_condition` SET `tgd_c_name` = 'PROBABLE' WHERE `tgd_condition`.`tgd_c_id` = 48;

UPDATE `tgd_condition` SET `tgd_c_name` = 'PREDICTABLE' WHERE `tgd_condition`.`tgd_c_id` = 49;

UPDATE `tgd_condition` SET `tgd_c_name` = 'ACTUAL' WHERE `tgd_condition`.`tgd_c_id` = 50;

====================================================================================
(Apr-23-2019)

--
-- Table structure for table `tsl_profile_default`
--

CREATE TABLE `tsl_profile_default` (
  `tpd_id` int(11) NOT NULL,
  `tpd_profile_type` int(1) NOT NULL,
  `tpd_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tsl_promo_default`
--

INSERT INTO `tsl_profile_default` (`tpd_id`, `tpd_profile_type`, `tpd_content`) VALUES
(1, 2, '<div><strong>Buyer Survey Questions&nbsp;<br><br>Current Status as Buyer:<br><br>&nbsp;1.{{ Briefly describe yourself as a buyer or buyer group. }}&nbsp;<br><br>2.{{ What do you own in this market? }}&nbsp;<br><br>3.{{ What do you own outside of this market? }}<br><br>&nbsp;4.{{ How is your ownership normally structured? }}<br><br>&nbsp;5.{{ How do you normally capitalize your deals? }}<br><br>&nbsp;6.{{ Is there anything else we should know about you as a buyer? }}&nbsp;<br><br>Short Term Buying Objectives:&nbsp;<br><br>1.{{ What are you looking to acquire in this market in the near term? }}&nbsp;<br><br>2.{{ What markets would you like to enter that have been challenging for you? }}&nbsp;<br><br>3.{{ What properties have you attempted to acquire recently? }}&nbsp;<br><br>4.{{ What age of property are you looking for? }}<br><br>&nbsp;5.{{ Are you looking for a value add opportunity or of stabilized acquisition? }}&nbsp;<br><br>6.{{ What other criteria do you have when looking for acquisitions? }}<br><br>&nbsp;Long Term Strategic Buying Objectives:&nbsp;<br><br>1.{{ What are you looking to build over the long term? }}&nbsp;<br><br>2.{{ What is your long-term acquisition plan? }}<br><br>&nbsp;3.{{ What dispositions will support your acquisition objectives? }}&nbsp;<br><br>4.{{ What is your long term exit strategy? }}<br><br>&nbsp;5.{{ Who else will be a beneficiary of your exit strategy? }}<br><br>&nbsp;6.{{ When do you see making such a transition? }}</strong></div>'),
(2, 1, '<div><strong>Owner Survey Questions<br><br>Current Status as Owner:<br><br>1.{{ Briefly describe yourself as an Owner or Owner group? }}<br><br>2.{{ What do you own in this market? }}<br><br>3.{{ What do you own outside of this market? }}<br><br>4.{{ How is your ownership normally structured? }}<br><br>5.{{ How do you normally capitalize your deals?}}<br><br>6.{{ Is there anything else we should know about you as an Owner? }}<br><br>Short Term Ownership Objectives:<br><br>1.{{ What are you looking to accomplish in this market in the near term? }}<br><br>2.{{ In what markets do you own property and which have been challenging for you? }}<br><br>3.{{ What is age or age range of the property you own and does that fit what you want? }}<br><br>4.{{ What properties have you acquired recently and how is that working for you? }}<br><br>5.{{ Are you looking for an opportunity to add value and stabilize your current holdings? }}<br><br>6.{{ Are their acquisition opportunities that could be secured by disposing of current holdings? }}<br><br>Long Term Strategic Ownership Objectives:<br><br>1.{{ What are you looking to build over the long term? }}<br><br>2.{{ What is your long-term plan for building your portfolio? }}<br><br>3.{{ What dispositions will support your acquisition objectives? }}<br><br>4.{{ What is your long-term exit strategy? }}<br><br>5.{{ Who else will be a beneficiary of your exit strategy? }}<br><br>6.{{ When do you see making such a transition? }}</strong></div>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tsl_profile_default`
--
ALTER TABLE `tsl_profile_default`
  ADD PRIMARY KEY (`tpd_id`);

--
-- Table structure for table `tsl_profile_outline`
--

CREATE TABLE `tsl_profile_outline` (
`tpo_id` int(11) NOT NULL,
`tpo_user_id` bigint(20) UNSIGNED NOT NULL,
`tpo_content` text NOT NULL,
`tpo_version` int(10) NOT NULL,
`tpo_client_type` varchar(10) NOT NULL,
`tpo_project_type` tinyint(1) NOT NULL,
`tpo_profile_type` tinyint(1) NOT NULL,
`tpo_profile_phase` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tsl_profile_outline`
--
ALTER TABLE `tsl_profile_outline`
ADD PRIMARY KEY (`tpo_id`),
ADD KEY `tpo_user_id` (`tpo_user_id`),
ADD KEY `tpo_project_type` (`tpo_project_type`),
ADD KEY `tpo_client_type` (`tpo_client_type`),
ADD KEY `tpo_profile_type` (`tpo_profile_type`),
ADD KEY `tpo_profile_phase` (`tpo_profile_phase`);

--
-- Add Foreign Key to Profile Outline.
--

ALTER TABLE `tsl_profile_outline`
ADD CONSTRAINT `fk_tpo_user_id`
FOREIGN KEY (`tpo_user_id`)
REFERENCES `pst_wp_cpd`.`wp_users`(`ID`)
ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Table structure for table `tsl_profile_question`
--

CREATE TABLE `tsl_profile_question` (
  `tpq_id` int(11) NOT NULL,
  `tpq_content` text NOT NULL,
  `tpq_outline_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tsl_profile_question`
--
ALTER TABLE `tsl_profile_question`
  ADD PRIMARY KEY (`tpq_id`),
  ADD KEY `tpq_outline_id` (`tpq_outline_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tsl_profile_question`
--
ALTER TABLE `tsl_profile_question`
  MODIFY `tpq_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tsl_profile_question`
--
ALTER TABLE `tsl_profile_question`
  ADD CONSTRAINT `fk_tpq_outline_id` FOREIGN KEY (`tpq_outline_id`) REFERENCES `tsl_profile_outline` (`tpo_id`) ON DELETE CASCADE ON UPDATE CASCADE;


========================================================================================================
(Apr-25-2019)

--
-- Table structure for table `tsl_profile_client`
--

CREATE TABLE `tsl_profile_client` (
  `tpc_id` int(11) NOT NULL,
  `tpc_client_id` int(11) NOT NULL,
  `tpc_token` varchar(200) NOT NULL,
  `tpc_profile_id` int(11) NOT NULL,
  `tpc_used` tinyint(1) NOT NULL,
  `tpc_created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tsl_profile_client`
--
ALTER TABLE `tsl_profile_client`
ADD PRIMARY KEY (`tpc_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tsl_profile_client`
--
ALTER TABLE `tsl_profile_client`
MODIFY `tpc_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Table structure for table `tsl_profile_answer`
--

CREATE TABLE `tsl_profile_answer` (
`tsl_profile_ans_id` int(11) NOT NULL,
`tsl_profile_ans_ques_id` int(11) NOT NULL,
`tsl_profile_ans_client_id` int(11) NOT NULL,
`tsl_profile_ans_profile_id` int(11) NOT NULL,
`tsl_profile_ans_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tsl_profile_answer`
--
ALTER TABLE `tsl_profile_answer`
ADD PRIMARY KEY (`tsl_profile_ans_id`),
ADD KEY `tsl_profile_ans_ques_id` (`tsl_profile_ans_ques_id`),
ADD KEY `tsl_profile_ans_client_id` (`tsl_profile_ans_client_id`),
ADD KEY `tsl_profile_ans_profile_id` (`tsl_profile_ans_profile_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tsl_profile_answer`
--
ALTER TABLE `tsl_profile_answer`
MODIFY `tsl_profile_ans_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tsl_profile_answer`
--
ALTER TABLE `tsl_profile_answer`
ADD CONSTRAINT `fk_profile_id` FOREIGN KEY (`tsl_profile_ans_profile_id`) REFERENCES `tsl_profile` (`tsl_profile_id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `fk_ques_id` FOREIGN KEY (`tsl_profile_ans_ques_id`) REFERENCES `tsl_profile_question` (`tpq_id`) ON DELETE CASCADE ON UPDATE CASCADE;


=====================================================================================

(May-01-2019)
--
-- Table structure for table `tsl_updates`
--

CREATE TABLE `tsl_updates` (
  `tu_id` int(11) NOT NULL,
  `tu_name` varchar(200) NOT NULL,
  `tu_outline_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tsl_updates`
--
ALTER TABLE `tsl_updates`
  ADD PRIMARY KEY (`tu_id`),
  ADD KEY `tu_outline_id` (`tu_outline_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tsl_updates`
--
ALTER TABLE `tsl_updates`
  MODIFY `tu_id` int(11) NOT NULL AUTO_INCREMENT;


  --
  -- Table structure for table `tsl_updates_answer`
  --

  CREATE TABLE `tsl_updates_answer` (
    `tua_id` int(11) NOT NULL,
    `tua_question_id` int(11) NOT NULL,
    `tua_content` text NOT NULL,
    `tua_update_id` int(11) NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  --
  -- Indexes for dumped tables
  --

  --
  -- Indexes for table `tsl_updates_answer`
  --
  ALTER TABLE `tsl_updates_answer`
    ADD PRIMARY KEY (`tua_id`),
    ADD KEY `tua_update_id` (`tua_update_id`);

  --
  -- AUTO_INCREMENT for dumped tables
  --

  --
  -- AUTO_INCREMENT for table `tsl_updates_answer`
  --
  ALTER TABLE `tsl_updates_answer`
    MODIFY `tua_id` int(11) NOT NULL AUTO_INCREMENT;

    --
    -- Table structure for table `tsl_update_default`
    --

    CREATE TABLE `tsl_update_default` (
      `tud_id` int(11) NOT NULL,
      `tud_content` text NOT NULL,
      `tud_type` tinyint(1) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

    --
    -- Dumping data for table `tsl_update_default`
    --

    INSERT INTO `tsl_update_default` (`tud_id`, `tud_content`, `tud_type`) VALUES
    (1, '<div><strong>[[Part 1 – Macro Economic Trends]] <br></strong><br>{{Step 1: What may happen to whom and by when will it happen?}}<br><br>{{Step 2: Why could this set of changes be a problem?}}<br><br>{{Step 3: How this set of events came about?}}<br><br>&nbsp;{{Step 4: What will be the short - term impact?}}<br><br>{{Step 5: What actions can be taken to mitigate these challenges?}}<br><br><strong>&nbsp;[[Part 2 – Local Economic Developments]]</strong><br><br> {{Step 1: What may happen to whom and by when will it happen?}} <br><br>{{Step 2: Why this set of changes could be a problem?}} <br><br>{{Step 3: How this set of events came about?}} <br><br>{{Step 4: What will be the short - term impact?}} <br><br>{{Step 5: What actions can be taken to mitigate these challenges?}} <br><br><strong>[[Part 3 – Emerging Challenges - Asset, People, Financial, Legal, Market]]</strong> <br><br>{{Step 1: What may happen to whom and by when will it happen?}} <br><br>{{Step 2: Why this set of changes could be a problem?}} <br><br>{{Step 3: How this set of events came about?}}. <br><br>{{Step 4: What will be the short - term impact?}} <br><br>{{Step 5: What actions can be taken to mitigate these challenges?}}.<br><br> <strong>[[Part 4 – Evolution of Available Options]]</strong> <br><br>{{Step 1: What may happen to whom and by when will it happen?}} <br><br>{{Step 2: Why this set of changes could be a problem?}} <br><br>{{Step 3: How this set of events came about?}} <br><br>{{Step 4: What will be the short - term impact?}} <br><br>{{Step 5: What actions can be taken to mitigate these challenges?}} <br><br><strong>[[Part 5 – Evolution of the Recommended Best Course of Action]]</strong>&nbsp;<br><br>{{Step 1: What may happen to whom and by when will it happen?}}&nbsp;<br><br>{{Step 2: Why this set of changes could be a problem?}}&nbsp;<br><br>{{Step 3: How this set of events came about?}}&nbsp;<br><br>{{Step 4: What will be the short - term impact?}}&nbsp;<br><br>{{Step 5: What actions can be taken to mitigate these challenges?}}</div>', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tsl_update_default`
--
ALTER TABLE `tsl_update_default`
ADD PRIMARY KEY (`tud_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tsl_update_default`
--
ALTER TABLE `tsl_update_default`
MODIFY `tud_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Table structure for table `tsl_update_outline`
--

CREATE TABLE `tsl_update_outline` (
`tuo_id` int(11) NOT NULL,
`tuo_user_id` bigint(20) UNSIGNED NOT NULL,
`tuo_content` text NOT NULL,
`tuo_version` tinyint(1) NOT NULL,
`tuo_client_type` varchar(10) NOT NULL,
`tuo_frequency` tinyint(1) NOT NULL,
`tuo_type` tinyint(1) NOT NULL,
`tuo_phase` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tsl_update_outline`
--
ALTER TABLE `tsl_update_outline`
ADD PRIMARY KEY (`tuo_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tsl_update_outline`
--
ALTER TABLE `tsl_update_outline`
MODIFY `tuo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Table structure for table `tsl_update_questions`
--

CREATE TABLE `tsl_update_questions` (
`tuq_id` int(11) NOT NULL,
`tuq_content` text NOT NULL,
`tuq_outline_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tsl_update_questions`
--
ALTER TABLE `tsl_update_questions`
ADD PRIMARY KEY (`tuq_id`),
ADD KEY `tuq_outline_id` (`tuq_outline_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tsl_update_questions`
--
ALTER TABLE `tsl_update_questions`
MODIFY `tuq_id` int(11) NOT NULL AUTO_INCREMENT;


======================================================================================
(May-04-2019)

--
-- Remove foreign key for tsl_script_name
--

ALTER TABLE tsl_script_name DROP FOREIGN KEY fk_tsl_version_id;

=====================================================================================
(May-06-2019)

--
-- Change default outline for initial contact script.
--
UPDATE `tsl_script_default` SET `tsl_script_content` = '<div><!--block-->[[<strong>Initial Contact</strong>]]<strong><br></strong><br></div><div><!--block--><strong>Business Development Call Outline<br></strong><br></div><div><!--block--><br>A first contact may be a call, an impromptu meeting or an encounter at a business event. <br><strong>{{Announcement}}</strong><br><strong>(First Announce: Who are you? What do you do? Who do you do it for? &amp; Why you are calling.)</strong> <br>[[<strong>Key Practice: Evoking Rapport</strong>]] <br>{{<strong>Step 1: </strong>Opening }} <br>Shut down the automatic “who knows best” battle with a statement that gives both agent and client permission to “not know” by expressing “I don’t know and you don’t something.” <br>{{<strong>Step 2:</strong> Permission }} <br>Ask to explore their intimate situation and consider some new ideas. <br>{{<strong>Step 3:</strong> Interest }} <br>Declare your interest in them and helping them. <br>[[Embedded Practice: Exposing Problems ]] <br>((<strong>Substep 1: Generate</strong>))<strong> Show interest by asking about them and/or your knowledge about them. </strong><br>((<strong>Substep 2: Introduce</strong>))<strong> Create an Interest Generator relevant to their situation and what they said </strong><br>((<strong>Substep 3: Explore </strong>))<strong> Ask a specific Open-Ended Question(s)until a Problem is exposed. </strong><br>(Step 2 &amp; 3 are interchangeable - first create an interest generator relevant to what the client said and then ask more specific questions to expose a problem or first expose the problem by asking more specific questions and then offer an interest generator that validates the problem and relates it to other client situations.) <br>((<strong>Substep 4: Listen </strong>))<strong> Expose a problem or restate the problem as a Windows of Opportunity </strong><br>((<strong>Substep 5: Convert </strong>))<strong> Turn the window of opportunity into Reason to Meet. </strong><br>{{<strong>Step 4: Detachment/Offer </strong>}}<strong> </strong>Offer to take the time to talk with them about <br>• the <strong>Problem</strong> exposed by the Interest Generator, <br>• their <strong>Project</strong> – where they are and where they want to be in the future with your help, <br>• and how you might also set up an Advisory <strong>Process</strong> to keep them out ahead of future changes. <br>{{<strong>Step 5: Questions </strong>}} &nbsp;<br>When would you like to meet?</div>' WHERE `tsl_script_default`.`tsl_script_default_id` = 6;

--
-- Insert Default Promo Outline.
--

INSERT INTO `tsl_promo_outline` (`tpo_id`, `tpo_user_id`, `tpo_content`, `tpo_version`, `tpo_client_type`, `tpo_promo_format`, `tpo_promo_focus`, `tpo_criteria`, `tpo_audience`, `tpo_active`, `tpo_name`) VALUES ('0', '0', '<div><!--block--><strong>&nbsp;{{ Big Headline }}</strong></div><div><!--block--><strong>&nbsp;{{ Emotional Connection }}</strong></div><div><!--block--><strong>&nbsp;{{ Inside Scoop }}</strong></div><div><!--block-->&nbsp; &nbsp; <strong>&nbsp;{{ Evidence }}</strong></div><div><!--block-->&nbsp; &nbsp; <strong>&nbsp;{{ Impact }}</strong></div><div><!--block-->&nbsp; &nbsp; <strong>&nbsp;{{ Challenge }}</strong></div><div><!--block-->&nbsp; &nbsp; <strong>&nbsp;{{ Remedy }}</strong></div><div><!--block-->&nbsp; &nbsp; <strong>&nbsp;{{ Delivery }}</strong></div><div><!--block--><strong>&nbsp;{{ Compelling Offer }}</strong></div><div><!--block--><strong>&nbsp;{{ Bold Promise }}</strong></div>', '1', 'n', 'n', 'n', '1', '1', '0', 'Default Outline');

--
-- Add user id column to tsl_email
--
ALTER TABLE `tsl_email` ADD `te_user_id` BIGINT(20) UNSIGNED NOT NULL AFTER `te_outline_id`;

=========================================================================
(May-08-2019)

--
-- Default outline answers for promo builder
--

INSERT INTO `tsl_promo_default_answer` (`tpda_id`, `tpda_answer_number`, `tpda_content`, `tpda_outline_id`) VALUES (NULL, '1', 'Our clients are finding their way to making better choices in uncertain times.', '-1'), (NULL, '2', 'Many clients we speak with have been facing an overwhelming sense of uncertainty;
they simply don’t know what options are available to them.', '-1'), (NULL, '3', 'Our advisory service is a remedy for this, it provides you with the information, strategy and
commitment you need to meet your goals', '-1'), (NULL, '4', 'In this advisory relationship, we gather the information necessary to give you a view of where you stand.', '-1'), (NULL, '5', 'This will be an in depth multi-dimensional view that will show you exactly where you are in this market and where things are headed', '-1'), (NULL, '6', 'Then we will update you as things change on a
regular basis.', '-1'), (NULL, '7', 'This way you can stay out ahead of changes which will likely occur at an ever-increasing rate.', '-1'), (NULL, '8', 'At some point, as a result of our
advisory work, you will come to see your best course of action and we will be glad to step in and facilitate your transaction if that is required.', '-1'), (NULL, '9', 'We invite you to give us a call and we will schedule a time to sit down with you and begin the process of gathering the information we need to provide you with a clear picture of where you stand in this market and what will likely be coming in your future', '-1'), (NULL, '10', 'Our promise to you is that you will, at all times, have the clarity you need to make the right choice at the right time.
You have 100 hours to contact them
before this promotional email will no longer make any difference!', '-1');

--
-- Table structure for table `tsl_promo_active_outline`
--

CREATE TABLE `tsl_promo_active_outline` (
  `tpao_id` int(11) NOT NULL,
  `tpao_user_id` bigint(20) UNSIGNED NOT NULL,
  `tpao_outline_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tsl_promo_active_outline`
--
ALTER TABLE `tsl_promo_active_outline`
  ADD PRIMARY KEY (`tpao_id`),
  ADD KEY `tpao_user_id` (`tpao_user_id`),
  ADD KEY `tpao_outline_id` (`tpao_outline_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tsl_promo_active_outline`
--
ALTER TABLE `tsl_promo_active_outline`
  MODIFY `tpao_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tsl_promo_active_outline`
--
ALTER TABLE `tsl_promo_active_outline`
  ADD CONSTRAINT `fk_tpao_outline_id` FOREIGN KEY (`tpao_outline_id`) REFERENCES `tsl_promo_outline` (`tpo_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_tpao_user_id` FOREIGN KEY (`tpao_user_id`) REFERENCES `pst_wp_cpd`.`wp_users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;


========================================================================================
(May-09-2019)

--
-- Table structure for table `tsl_message_default`
--

CREATE TABLE `tsl_message_default` (
  `tmd_id` int(11) NOT NULL,
  `tmd_seq_number` tinyint(1) NOT NULL,
  `tmd_text` text NOT NULL,
  `tmd_order` tinyint(1) NOT NULL,
  `tmd_component` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tsl_message_default`
--

INSERT INTO `tsl_message_default` (`tmd_id`, `tmd_seq_number`, `tmd_text`, `tmd_order`, `tmd_component`) VALUES
(1, 1, 'This is David from ______________. I am a Commercial Real Estate advisor. ', 1, 'vmail'),
(2, 1, 'We are just checking to see if you have received our notice on the looming market downturn and are looking to get out ahead of this inevitable event.', 2, 'vmail'),
(3, 1, 'We know how challenging it can be to break away from current success and look honestly ay how you will get prepared to ride it out. Getting out ahead of the changes in the market can ruin a good day, but we are here to help you suffer a little pain so you don’t end up in a situation from which you may not recover. Whether or not you are in a position to take any action at this time, we are here to help you think through the process so that when an action on your part is needed, you are more prepared. ', 3, 'vmail'),
(4, 1, 'Give us a call, and we will schedule an advisory session in which we will see where you stand today, where you may want to be in the future and how we might help you get there. ', 4, 'vmail'),
(5, 1, 'If we don’t hear from you, we will endeavour to reach out until we do touch base. ', 5, 'vmail'),
(6, 2, 'This is David from _______________. I am a Commercial Real Estate advisor. ', 1, 'vmail'),
(7, 2, 'We are just checking in again to see if you have thought about any challenges of the coming downturn that we might help you resolve.', 2, 'vmail'),
(8, 2, 'Our advisory service is a no-cost conversation designed to give you the information you will need and an understanding of how this information applies to your specific situation. We will help you better understand what your options are and to recommend a course of action that will serve your best get you prepared. We may also set up an ongoing advisory relationship giving you regular updates to keep you out ahead of the changes as they emerge. ', 3, 'vmail'),
(9, 2, 'Give us a call, and we will schedule an advisory session to see where you stand today, where you may want to be in the future and how we might help you get through the downturn so you achieve your objectives. ', 4, 'vmail'),
(10, 2, 'If we don’t hear from you, we will endeavor to reach out until we do touch base.', 5, 'vmail'),
(11, 3, 'This is David from _______________. I am a Commercial Real Estate advisor.', 1, 'vmail'),
(12, 3, 'We are checking in again for a third time to see if you have any current challenges, we might help you resolve and help you get out ahead of the coming market changes.', 2, 'vmail'),
(13, 3, 'Your current challenges may have successfully resolved some of your challenges without our help. If not resolved, don’t worry we will guide you through a process of getting them resolved easily and quickly so you can move on to getting prepared to ride out the coming market changes that could seriously affect the viability of your assets.', 3, 'vmail'),
(14, 3, 'Give us a call, and we will schedule an advisory session to see where you stand today, where you may want to be in the future and how we might help you get there.', 4, 'vmail'),
(15, 3, 'If we don’t hear from you, we will endeavor to reach out until we do touch base.', 5, 'vmail'),
(16, 4, 'This is David from _______________. I am a Commercial Real Estate advisor.', 1, 'vmail'),
(17, 4, 'We are just checking in again to see if you have any challenges, we might help you resolve and to work with you to take advantage of the inevitable market downturn in the next twelve to eighteen months.', 2, 'vmail'),
(18, 4, 'We know how challenging it can be to break away from a busy schedule to spend some executive time thinking about the future. But we also know that your challenges can and will be resolved more easily and completely with our help. We know the business most people do not have, and that will give you the advantage you need in dealing with the inevitable market changes like interest rates, economic downturns, and current market opportunities.', 3, 'vmail'),
(19, 4, 'Give us a call, and we will schedule an advisory session to see where you stand today, where you may want to be in the future and how we might help you get there. ', 4, 'vmail'),
(20, 4, 'If we don’t hear from you, we will endeavor to reach out until we do touch base.', 5, 'vmail'),
(21, 5, 'This is David from _______________. I am a Commercial Real Estate advisor. ', 1, 'vmail'),
(22, 5, 'We have called some times to offer you our free advisory service. ', 2, 'vmail'),
(23, 5, 'We know it takes a lot to take difficult action and we know it is our job to make that as easy for you as possible. Sooner or later you will have to deal with the emerging challenges, and the sooner you get prepared, the better. We have seen this to be the case with the many owners like you that we have helped so far. Sometimes it is true that what we don’t know won’t hurt us, but in real estate that has never shown itself to be true. ', 3, 'vmail'),
(24, 5, 'Give us a call, and we will schedule an advisory session to see where you stand today, where you may want to be in the future and how we might help you get there. ', 4, 'vmail'),
(25, 5, 'If we don’t hear from you, we will endeavor to reach out until we do touch base. ', 5, 'vmail'),
(26, 1, 'One client during the period before and after the last downturn had this happen. Before the downturn times were good, and the owner in question has a property worth 18M dollars and got an offer from an aggressive buyer at 20M', 1, 'stories'),
(27, 1, 'The owner thinking things would never turn against him wanted 21M and so turned down the offer.', 2, 'stories'),
(28, 1, 'I recommended that the client take the offer while he could as signs of a looming economic shift were already emerging. The client hungry to achieve a higher price and a bit blinded by greed chose not ti listen to our recommendation.', 3, 'stories'),
(29, 1, 'Almost twelve months to the day after the downturn hit, the client was still arguing over the price on the same property but by now he was arguing over 6.5M and 7M for the same property.', 4, 'stories'),
(30, 1, 'After putting himself through a lot of brain damage the client ended up foreclosing on te property the following year.', 5, 'stories'),
(31, 2, 'One client during the period before and after the last downturn had this happen. Before the downturn times were good, and the owner in question has a property worth 18M dollars and got an offer from an aggressive buyer at 20M', 1, 'stories'),
(32, 2, 'The owner thinking things would never turn against him wanted 21M and so turned down the offer.', 2, 'stories'),
(33, 3, 'The owner thinking things would never turn against him wanted 21M and so turned down the offer.', 2, 'stories'),
(34, 4, 'The owner thinking things would never turn against him wanted 21M and so turned down the offer.', 2, 'stories'),
(35, 5, 'The owner thinking things would never turn against him wanted 21M and so turned down the offer.', 2, 'stories'),
(36, 2, 'I recommended that the client take the offer while he could as signs of a looming economic shift were already emerging. The client hungry to achieve a higher price and a bit blinded by greed chose not ti listen to our recommendation.', 3, 'stories'),
(37, 3, 'I recommended that the client take the offer while he could as signs of a looming economic shift were already emerging. The client hungry to achieve a higher price and a bit blinded by greed chose not ti listen to our recommendation.', 3, 'stories'),
(38, 4, 'I recommended that the client take the offer while he could as signs of a looming economic shift were already emerging. The client hungry to achieve a higher price and a bit blinded by greed chose not ti listen to our recommendation.', 3, 'stories'),
(39, 5, 'I recommended that the client take the offer while he could as signs of a looming economic shift were already emerging. The client hungry to achieve a higher price and a bit blinded by greed chose not ti listen to our recommendation.', 3, 'stories'),
(40, 2, 'Almost twelve months to the day after the downturn hit, the client was still arguing over the price on the same property but by now he was arguing over 6.5M and 7M for the same property.', 4, 'stories'),
(41, 3, 'Almost twelve months to the day after the downturn hit, the client was still arguing over the price on the same property but by now he was arguing over 6.5M and 7M for the same property.', 4, 'stories'),
(42, 4, 'Almost twelve months to the day after the downturn hit, the client was still arguing over the price on the same property but by now he was arguing over 6.5M and 7M for the same property.', 4, 'stories'),
(43, 5, 'Almost twelve months to the day after the downturn hit, the client was still arguing over the price on the same property but by now he was arguing over 6.5M and 7M for the same property.', 4, 'stories'),
(44, 2, 'After putting himself through a lot of brain damage the client ended up foreclosing on te property the following year.', 5, 'stories'),
(45, 3, 'After putting himself through a lot of brain damage the client ended up foreclosing on te property the following year.', 5, 'stories'),
(46, 4, 'After putting himself through a lot of brain damage the client ended up foreclosing on te property the following year.', 5, 'stories'),
(47, 5, 'After putting himself through a lot of brain damage the client ended up foreclosing on te property the following year.', 5, 'stories'),
(48, 3, 'One client during the period before and after the last downturn had this happen. Before the downturn times were good, and the owner in question has a property worth 18M dollars and got an offer from an aggressive buyer at 20M', 1, 'stories'),
(49, 4, 'One client during the period before and after the last downturn had this happen. Before the downturn times were good, and the owner in question has a property worth 18M dollars and got an offer from an aggressive buyer at 20M', 1, 'stories'),
(50, 5, 'One client during the period before and after the last downturn had this happen. Before the downturn times were good, and the owner in question has a property worth 18M dollars and got an offer from an aggressive buyer at 20M', 1, 'stories');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tsl_message_default`
--
ALTER TABLE `tsl_message_default`
  ADD PRIMARY KEY (`tmd_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tsl_message_default`
--
ALTER TABLE `tsl_message_default`
  MODIFY `tmd_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;


========================================================================================
--
-- Add Actual FTG columns to tgd_user_instance table
--

ALTER TABLE `tgd_user_instance` ADD `tgd_proposal_actual` DECIMAL(15) NOT NULL DEFAULT '0' AFTER `tgd_proposal_g`;

ALTER TABLE `tgd_user_instance` ADD `tgd_list_actual` DECIMAL(15) NOT NULL DEFAULT '0' AFTER `tgd_list_g`;

ALTER TABLE `tgd_user_instance` ADD `tgd_contract_actual` DECIMAL(15) NOT NULL DEFAULT '0' AFTER `tgd_contract_g`;

ALTER TABLE `tgd_user_instance` ADD `tgd_cumulative_actual` DECIMAL(15) NOT NULL DEFAULT '0' AFTER `tgd_contract_g`;


==============================================================================================
(May-22-2019)

--
-- Update TSL Survey default outline.
--
UPDATE `tsl_survey_outline`
SET `tso_content` = '<div><strong>(Your offer should state: “We are offering to keep our eyes open on your behalf for opportunities that fit your profile.” Phase 1 should be the key questions on an initial buyer call. Phase two should be the focus of a tour contact. Phase three should be the focus of the post-tour acquisition strategy meeting.)<br></strong><br><strong>[[Phase 1: Current Status as Buyer: Initial Call]]</strong><br><br><strong>Step 1:{{ Briefly describe yourself as a buyer or buyer group?}}<br><br>Step 2:{{ What do you own in this market? }} </strong><br><br><strong>Step 3:{{ What do you own outside of this market?}}</strong><br><br><strong>Step 4:{{ How is your ownership normally structured?}}</strong><br><br><strong>Step 5:{{ How do you normally capitalize your deals? }}</strong><br><br><strong>[[Phase 2: Short Term Buying Objectives: Tour Contact]]</strong><br><br><strong>Step 1: {{ What are you looking to acquire in this market in the near term? }} </strong><br><br><strong>Step 2: {{ What markets would you like to enter that have been challenging for you? }} </strong><br><br><strong>Step 3:{{ What properties have you attempted to acquire recently? }} </strong><br><br><strong>Step 4:{{ What age of property are you looking for? }}</strong><br><br><strong>&nbsp;Step 5:{{ Are you looking for a value add opportunity or of stabilised acquisition? }} </strong><br><strong><br>[[Phase 3: Long Term Strategic Buying Objectives]]</strong><br><br><strong>Step 1:{{ What are you looking to build over the long term? }} </strong><br><br><strong>Step 2:{{ What is your long-term acquisition plan? }}</strong><br><br><strong>&nbsp;Step 3:{{ What dispositions will support your acquisition objectives? }} </strong><br><br><strong>Step 4:{{ What is your long term exit strategy? }}</strong><br><br><strong>Step 5:{{ When do you see making such a transition? }}</strong></div>'
 WHERE `tsl_survey_outline`.`tso_id` = 0;


 ============================================================================================
 (May-28-2019)
 --
 -- Table structure for table `script_collab`
 --

 CREATE TABLE `script_collab` (
   `sc_id` int(11) NOT NULL,
   `sc_content` text NOT NULL,
   `sc_order` int(11) NOT NULL,
   `sc_ques_id` int(11) NOT NULL,
   `sc_tool_id` int(11) NOT NULL,
   `sc_type` varchar(20) NOT NULL
 ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 --
 -- Indexes for dumped tables
 --

 --
 -- Indexes for table `script_collab`
 --
 ALTER TABLE `script_collab`
   ADD PRIMARY KEY (`sc_id`),
   ADD UNIQUE KEY `order_tool_ques` (`sc_order`,`sc_ques_id`,`sc_tool_id`,`sc_type`);

 --
 -- AUTO_INCREMENT for table `script_collab`
 --
 ALTER TABLE `script_collab`
   MODIFY `sc_id` int(11) NOT NULL AUTO_INCREMENT;

========================================================================================
(May-29-2019)
--
-- Table structure for table `script_collab_token`
--

CREATE TABLE `script_collab_token` (
 `sct_id` int(11) NOT NULL,
 `sct_token` text NOT NULL,
 `sct_client_id` int(11) NOT NULL,
 `sct_collab_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `script_collab_token`
--
ALTER TABLE `script_collab_token`
 ADD PRIMARY KEY (`sct_id`),
 ADD KEY `sct_client_id` (`sct_client_id`),
 ADD KEY `sct_collab_id` (`sct_collab_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `script_collab_token`
--
ALTER TABLE `script_collab_token`
 MODIFY `sct_id` int(11) NOT NULL AUTO_INCREMENT;


 ===================================================================================
 (June-03-2019)
 --
 -- Table structure for table `script_collab`
 --

 CREATE TABLE `script_collab` (
   `sc_id` int(11) NOT NULL,
   `sc_content` text,
   `sc_order` int(11) NOT NULL,
   `sc_ques_id` int(11) NOT NULL,
   `sc_tool_id` int(11) NOT NULL,
   `sc_type` varchar(20) NOT NULL,
   `sc_sender` varchar(150) NOT NULL,
   `sc_recipient` varchar(150) NOT NULL,
   `sc_action` varchar(200) NOT NULL,
   `sc_date` date DEFAULT NULL,
   `sc_time` time(6) DEFAULT NULL,
   `sc_category` varchar(20) NOT NULL DEFAULT 'sc',
   `sc_collaboration_id` int(11) NOT NULL DEFAULT '0'
 ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

 --
 -- Indexes for dumped tables
 --

 --
 -- Indexes for table `script_collab`
 --
 ALTER TABLE `script_collab`
   ADD PRIMARY KEY (`sc_id`),
   ADD UNIQUE KEY `order_tool_ques` (`sc_order`,`sc_ques_id`,`sc_tool_id`,`sc_type`,`sc_action`,`sc_category`,`sc_collaboration_id`) USING BTREE;

 --
 -- AUTO_INCREMENT for dumped tables
 --

 --
 -- AUTO_INCREMENT for table `script_collab`
 --
 ALTER TABLE `script_collab`
   MODIFY `sc_id` int(11) NOT NULL AUTO_INCREMENT;


--
-- Table structure for table `collab_format`
--

CREATE TABLE `collab_format` (
  `collab_format_id` int(11) NOT NULL,
  `collab_format_user_id` int(11) NOT NULL,
  `collab_format_name` varchar(200) NOT NULL,
  `collab_format_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `collab_format`
--

INSERT INTO `collab_format` (`collab_format_id`, `collab_format_user_id`, `collab_format_name`, `collab_format_default`) VALUES
(1, 0, 'Comment(s)', 1),
(2, 0, 'Question(s)', 1),
(3, 0, 'Suggestion(s)', 1),
(4, 0, 'Recommendation(s)', 1),
(5, 0, 'Request(s)', 1),
(6, 0, 'Requirement(s)', 1),
(7, 0, 'Coaching Moment(s)', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `collab_format`
--
ALTER TABLE `collab_format`
  ADD PRIMARY KEY (`collab_format_id`),
  ADD KEY `collab_format_user_id` (`collab_format_user_id`),
  ADD KEY `collab_format_default` (`collab_format_default`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `collab_format`
--
ALTER TABLE `collab_format`
  MODIFY `collab_format_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;


--
-- Table structure for table `collab`
--

CREATE TABLE `collab` (
  `collab_id` int(11) NOT NULL,
  `collab_user_id` bigint(20) UNSIGNED NOT NULL,
  `collab_sender_id` int(11) NOT NULL,
  `collab_recipient_id` int(11) NOT NULL,
  `collab_script_id` int(11) NOT NULL,
  `collab_format_id` int(11) NOT NULL,
  `collab_data` text NOT NULL,
  `collab_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `collab`
--
ALTER TABLE `collab`
  ADD PRIMARY KEY (`collab_id`),
  ADD KEY `user_id` (`collab_user_id`),
  ADD KEY `collab_sender` (`collab_sender_id`,`collab_recipient_id`,`collab_script_id`,`collab_format_id`),
  ADD KEY `fk_collab_recipient` (`collab_recipient_id`),
  ADD KEY `fk_collab_script` (`collab_script_id`),
  ADD KEY `fk_collab_format` (`collab_format_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `collab`
--
ALTER TABLE `collab`
  MODIFY `collab_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `collab`
--
ALTER TABLE `collab`
  ADD CONSTRAINT `fk_collab_format` FOREIGN KEY (`collab_format_id`) REFERENCES `collab_format` (`collab_format_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_collab_recipient` FOREIGN KEY (`collab_recipient_id`) REFERENCES `collab_recipient` (`collab_recipient_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_collab_script` FOREIGN KEY (`collab_script_id`) REFERENCES `collab_script` (`collab_script_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_collab_user` FOREIGN KEY (`collab_user_id`) REFERENCES `pst_wp_cpd`.`wp_users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Alter table tsl_script_client
--

ALTER TABLE `pst_cpd`.`tsl_script_client`
ADD COLUMN `tsc_name` VARCHAR(100) NULL DEFAULT NULL AFTER `tsc_script_id`,
ADD COLUMN `tsc_project` VARCHAR(100) NULL DEFAULT NULL AFTER `tsc_name`;


=============================================================================
(Jun-12-2019)

--
-- Update default answers fo SA Welcome Page.
--
DELETE FROM `cpd_sa_default_data` WHERE csdd_csd_id=2


INSERT INTO `cpd_sa_default_data` (`csdd_id`, `csdd_csd_id`, `csdd_col`, `csdd_content`) VALUES (NULL, '2', '1', 'Dear Client Name,'), (NULL, '2', '2', 'We appreciate the opportunity of providing you with our in depth strategic analysis of your property and its position in this current market.'), (NULL, '2', '3', 'There is always more to know but you
have been very helpful in providing us with the information we needed.'), (NULL, '2', '4', 'If it’s okay with you we will give you our honest no holds barred assessment of your property and overall situation so you have a better understanding of where you stand at this time.'), (NULL, '2', '5', 'As the leading specialists in the ________ industry, our primary interest is in giving you a clear understanding of the driving forces in the current marketplace. This analysis will provide you a complete set of options and our recommended best course of action that we feel will serve your best interest at this time.'), (NULL, '2', '6', 'Regardless of which option you choose we have given you the most accurate evaluation possible and we know that whether it be today, tomorrow or several years from now, we are available to you as your advisor and partner. We promise to stand by you every step of the way to assist you in implementing the course of action you choose.'), (NULL, '2', '7', 'Sincerely,'), (NULL, '2', '8', 'Agent Name'), (NULL, '2', '9', 'Agent Name,'), (NULL, '2', '10', 'Company Name');

UPDATE `cpd_sa_default_data` SET `csdd_content` = 'Mr,' WHERE `cpd_sa_default_data`.`csdd_id` = 57;


=============================================================================
(Jun-13-2019)
--
-- Table structure for table `cpd_sa_data`
--

CREATE TABLE `cpd_sa_data` (
  `cs_data_id` int(11) NOT NULL,
  `cs_data_sa_id` int(11) NOT NULL,
  `cs_data_data` text NOT NULL,
  `cs_data_template` varchar(20) NOT NULL,
  `cs_data_image_1` varchar(20) DEFAULT NULL,
  `cs_data_image_2` varchar(100) DEFAULT NULL,
  `cs_data_page` varchar(20) NOT NULL,
  `cs_data_sa_type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cpd_sa_data`
--
ALTER TABLE `cpd_sa_data`
  ADD PRIMARY KEY (`cs_data_id`),
  ADD UNIQUE KEY `unique_index` (`cs_data_sa_id`,`cs_data_page`,`cs_data_sa_type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cpd_sa_data`
--
ALTER TABLE `cpd_sa_data`
  MODIFY `cs_data_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cpd_sa_data`
--
ALTER TABLE `cpd_sa_data`
  ADD CONSTRAINT `fk_sa_data` FOREIGN KEY (`cs_data_sa_id`) REFERENCES `cpd_sa` (`cs_id`) ON DELETE CASCADE ON UPDATE CASCADE;


--
-- Update SA outline
--

UPDATE `pst_cpd`.`cpd_sa_outline`
SET `csd_content`='<div><strong>[[Client Name:]]<br></strong>{{Name of Client. . .}}</div><div><br><strong>[[Announcement]]<br></strong>{{We appreciate the opportunity . . .}}<br><strong>[[Step 1: Opening]]</strong><br>{{There is always more to know . . .}}<br><strong>[[Step 2: Permission]]</strong><br>{{If it’s okay with you we will . . .}}<br><strong>[[Step 3: Interest]]</strong><br>{{As the leading specialists in . . .}}<br><strong>[[Step 4: Detachment/Offer]]</strong></div><div>{{Regardless of which action you choose. . .}}<br><strong>[[Step 5: Questions]]<br></strong>{{We promise to stand by you . . .}}<br><br>{{Salutation}}<br>{{Signature}}<br>{{Company Name}}</div>'
 WHERE `csd_id`='2';

UPDATE `pst_cpd`.`cpd_sa_outline`
SET `csd_content`='<div><strong>[[Your Project is Our Mission]]</strong></div><div><strong>[[History]]<br></strong>{{We take your project very . . .}}<br><strong>[[Current Facts]]</strong><br>{{The facts are you are now . . .}}<br><strong>[[Accomplished]]</strong><br>{{What you have accomplished . . .}}<br><strong>[[Problems]]</strong><br>{{Regardless of which option you . . .}}<br><strong>[[Project]]</strong><br>{{As a result of our work together . . .}}</div>'
 WHERE `csd_id`='3';


======================================================================================
(Jun-19-2019)

ALTER TABLE `pst_cpd`.`cpd`
CHANGE COLUMN `cpd_contract_name`
`cpd_b_contract_name`
VARCHAR(50) NOT NULL
DEFAULT 'Offered and Accepted' ;


ALTER TABLE `pst_cpd`.`cpd`
CHANGE COLUMN `cpd_contract_desc`
`cpd_b_contract_desc` TEXT NOT NULL ;


=========================================================================================
(Jun-21-2019)
--
-- Drop columns from survey outline table.
--

ALTER TABLE `tsl_survey_outline`
  DROP `tso_content`,
  DROP `tso_survey_phase`;

--
-- Rename default survey outline.
--
UPDATE `tsl_survey_outline`
SET `tso_name` = 'Default Survey Outline'
WHERE `tsl_survey_outline`.`tso_id` = 0;

====================================================================================
(Jun-24-2019)
ALTER TABLE tsl_survey_question DROP FOREIGN KEY fk_outline_question_id;

======================================================================================

--
-- Table structure for table `collab_recipient`
--

CREATE TABLE `collab_recipient` (
  `collab_recipient_id` int(11) NOT NULL,
  `collab_recipient_name` varchar(200) NOT NULL,
  `collab_recipient_user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `collab_recipient`
--
ALTER TABLE `collab_recipient`
  ADD PRIMARY KEY (`collab_recipient_id`),
  ADD KEY `recipient_user_id` (`collab_recipient_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `collab_recipient`
--
ALTER TABLE `collab_recipient`
  MODIFY `collab_recipient_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `collab_recipient`
--
ALTER TABLE `collab_recipient`
  ADD CONSTRAINT `fk_recipient_user_id` FOREIGN KEY (`collab_recipient_user_id`) REFERENCES `pst_wp_cpd`.`wp_users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;




  --
  -- Dumping data for table `collab_format`
  --

  INSERT INTO `collab_format` (`collab_format_id`, `collab_format_user_id`, `collab_format_name`, `collab_format_default`) VALUES
  (1, 0, 'Comment(s)', 1),
  (2, 0, 'Example(s)', 1),
  (3, 0, 'Roleplay(s)', 1),
  (4, 0, 'Question(s)', 1),
  (5, 0, 'Recommendation(s)', 1),
  (6, 0, 'Request(s)', 1);

  /*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
  /*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
  /*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

  --
  -- Table structure for table `tsl_script_recording`
  --

  CREATE TABLE `tsl_script_recording` (
    `tsr_id` int(11) NOT NULL,
    `tsr_user_id` bigint(25) NOT NULL,
    `tsr_name` varchar(200) NOT NULL,
    `tsr_given_name` varchar(200) DEFAULT 'Recording'
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  --
  -- Indexes for dumped tables
  --

  --
  -- Indexes for table `tsl_script_recording`
  --
  ALTER TABLE `tsl_script_recording`
    ADD PRIMARY KEY (`tsr_id`),
    ADD KEY `tsr_user_id` (`tsr_user_id`);

  --
  -- AUTO_INCREMENT for dumped tables
  --

  --
  -- AUTO_INCREMENT for table `tsl_script_recording`
  --
  ALTER TABLE `tsl_script_recording`
    MODIFY `tsr_id` int(11) NOT NULL AUTO_INCREMENT;


    =============================================================================
    Jul-08-2019

    --
    -- Dumping data for table `universal_component`
    --

    INSERT INTO `universal_component` (`uc_id`, `uc_name`, `uc_controller`, `uc_icon`, `uc_table`, `uc_col_name`, `uc_ref_primary_key`, `uc_type`, `uc_tool`) VALUES
    (1, 'Survey', '/survey/', 'book', 'tsl_survey', 'tsl_survey_name', 'tsl_survey_id', 'send', 'tsl'),
    (2, 'Profile', '/profile/', 'user', 'tsl_profile', 'tsl_profile_name', 'tsl_profile_id', 'send', 'tsl'),
    (3, 'SAP', '/sa/', 'open-file', 'cpd_sa', 'cs_name', 'cs_id', 'send', 'tsl'),
    (4, 'Updates', '/updates/', 'book', 'tsl_updates', 'tu_name', 'tu_id', 'send', 'tsl'),
    (5, 'DOCS', '/docs/', 'book', 'cpd_docs', 'cd_name', 'cd_id', 'send', 'tsl'),
    (6, 'Pages', '/page/', 'file', 'tsl_promo', 'tp_org_name', 'tp_id', 'promo', 'tsl'),
    (7, 'Vmail', '/vmail/', 'envelope', 'tsl_message_version', 'tm_name', 'tm_id', 'promo', 'tsl'),
    (8, 'Script', '/script/', 'list-alt', 'tsl_script', NULL, 'ts_id', 'promo', 'tsl'),
    (9, 'Email', '/email/', 'envelope', 'tsl_email', 'te_name', 'te_id', 'promo', 'tsl'),
    (10, 'Survey', '/survey/', 'book', 'tsl_survey', 'tsl_survey_name', 'tsl_survey_id', 'send', 'cpdv1'),
    (11, 'Profile', '/profile/', 'user', 'tsl_profile', 'tsl_profile_name', 'tsl_profile_id', 'send', 'cpdv1'),
    (12, 'SAP', '/sa/', 'open-file', 'cpd_sa', 'cs_name', 'cs_id', 'send', 'cpdv1'),
    (13, 'Updates', '/updates/', 'book', 'tsl_updates', 'tu_name', 'tu_id', 'send', 'cpdv1'),
    (14, 'DOCS', '/docs/', 'book', 'cpd_docs', 'cd_name', 'cd_id', 'send', 'cpdv1'),
    (15, 'Email', '/email/', 'envelope', 'tsl_email', 'te_name', 'te_id', 'promo', 'cpdv1'),
    (16, 'Worksheet', '/wks/', 'file', 'tsl_script', NULL, 'ts_id', 'promo', 'cpdv1'),
    (17, 'Event', '/event/', 'list-alt', 'tsl_script', NULL, 'ts_id', 'promo', 'cpdv1'),
    (18, 'Review', '/review/', 'list-alt', NULL, NULL, '', 'promo', 'cpdv1'),
    (19, 'Followup', '/followup/', 'envelope', 'tsl_message_version', 'tm_name', 'tm_id', 'promo', 'cpdv1'),
    (20, 'Stories', '/story/', 'list-alt', NULL, NULL, '', 'promo', 'tsl');

    --
    -- Drop foreign key from ACM table.
    --

    ALTER TABLE `pst_cpd`.`cpd_acm_deal`
    DROP FOREIGN KEY `fk_cad_deal`;

============================================================================================

(Jul-17-2019)
--
-- Update Table structure for profile outline.
--

ALTER TABLE `tsl_profile_outline`
  DROP `tpo_content`,
  DROP `tpo_project_type`,
  DROP `tpo_profile_phase`;

ALTER TABLE `tsl_profile_outline` ADD `tpo_name` VARCHAR(300) NOT NULL AFTER `tpo_profile_type`;

ALTER TABLE `tsl_profile_outline` CHANGE `tpo_name`
`tpo_name` VARCHAR(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Custom Profile Outline';


--
-- Default phases content for Profile Builder.
--

INSERT INTO `component_phase` (`cp_id`, `cp_name`, `cp_number`, `cp_component`, `cp_content`, `cp_outline_id`) VALUES (NULL, 'Client History', '1', 'profile', '<div><!--block-->Phase 1: Client History<br><br>Question 1: {{Describe yourself as an owner or investor group.}}<br>Question 2 :{{Describe what assets do you own in this market.}}<br>Question 3 :{{Describe what you own outside of this market.}}<br>Question 4: {{Describe how is your ownership structured?}}<br>Question 5: {{Describe how do you capitalize your deals?}}</div>', '0'), (NULL, 'Client Accomplishments', '2', 'profile', '<div><!--block-->Phase 2: Client Accomplishments<br><br>Question 1: {{Describe your biggest accomplishment as owner.}}<br>Question 2 :{{Describe your other major accomplishments.}}<br>Question 3 :{{Describe your unique abilities as an owner.}}<br>Question 4: {{Describe the other members of your admin team.}}<br>Question 5: {{Describe what you have accomplished as a team.}}</div>', '0'), (NULL, 'Client Challenges', '3', 'profile', '<div><!--block-->Phase 3: Client Challenges<br><br>Question 1: {{Describe the biggest challenge you currently face.}}<br>Question 2 :{{Describe any other current major challenges.}}<br>Question 3 :{{Describe where you are least often challenged.}}<br>Question 4: {{Describe where you are most often challenged..}}<br>Question 5: {{Describe areas where you are improving your situation.}}</div>', '0'), (NULL, 'Client Possibilities', '4', 'profile', '<div><!--block-->Phase 4: Client Possibilities<br><br>Question 1: {{Describe what you most aspire to achieve.}}<br>Question 2 :{{Describe what will become possible if you do this.}}<br>Question 3 :{{Describe what value this will provide to your team.}}<br>Question 4: {{Describe how these possibilities will benefit partners.}}<br>Question 5: {{Describe how this will add value to the industry.}}</div>', '0'), (NULL, 'Client Partnership', '5', 'profile', '<div><!--block-->Phase 5: Client Partnership<br><br>Question 1: {{Describe the risks you may need to face going forward.}}<br>Question 2 :{{Describe the responsibility you will need to take.}}<br>Question 3 :{{Describe your capability and commitment.}}<br>Question 4: {{Describe the decisive actions you will need to take.}}<br>Question 5: {{Describe The promise you make to yourself and others.}}</div>', '0');

ALTER TABLE `tsl_profile_question` CHANGE `tpq_phase` `tpq_phase` INT(11) NOT NULL;

ALTER TABLE tsl_profile_question DROP FOREIGN KEY fk_tpq_outline_id;

ALTER TABLE `tsl_profile_question` DROP `tpq_outline_id;


===================================================================================
(Jul-19-2019)

--
-- Add Content Provider Admin tool to DB.
--

INSERT INTO `tool`
(`tool_id`, `tool_tg_id`, `tool_name`, `tool_description`,
`tool_controller`, `tool_wp_name`, `tool_is_admin`, `tool_status`)
VALUES
(NULL, '1', 'Content Provider',
'This tool helps the administrator to add/edit content for different components.', 'cp', '', '1', 'enabled');


===================================================================================================
(Jul-23-2019)
--
-- Database: `pst_cpd`
--

-- --------------------------------------------------------

--
-- Table structure for table `tsl_spoken_message`
--

CREATE TABLE `tsl_spoken_message` (
  `tsm_id` int(11) NOT NULL,
  `tsm_sequence_id` int(11) NOT NULL,
  `tsm_component_id` int(11) NOT NULL,
  `tsm_component_type` varchar(200) NOT NULL,
  `tsm_client_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tsl_spoken_message`
--
ALTER TABLE `tsl_spoken_message`
  ADD PRIMARY KEY (`tsm_id`),
  ADD UNIQUE KEY `tsm_client_id` (`tsm_client_id`,`tsm_component_type`) USING BTREE,
  ADD KEY `tsm_sequence_id` (`tsm_sequence_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tsl_spoken_message`
--
ALTER TABLE `tsl_spoken_message`
  MODIFY `tsm_id` int(11) NOT NULL AUTO_INCREMENT;

===============================================
(Jul-17-2019)

ALTER TABLE `tsl_promo_content` ADD `tpc_last_updated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `tpc_content`;

========================================================================================
(Jul-25-2019)
--
--Update foreign key constraintt for client asset.
--

ALTER TABLE
`homebase_client_asset`
DROP FOREIGN KEY `fk_hca_criteria`;

ALTER TABLE `homebase_client_asset`
ADD CONSTRAINT `fk_hca_criteria`
FOREIGN KEY (`hca_criteria_used`)
REFERENCES `tsl_criteria`(`tsl_criteria_id`)
ON DELETE NO ACTION ON UPDATE NO ACTION;


--
-- Update foreign key constraint for TSL criteria.
--

ALTER TABLE `tsl_criteria`
DROP FOREIGN KEY `fk_criteria_tsl_id`;

ALTER TABLE `tsl_criteria`
ADD CONSTRAINT `fk_criteria_tsl_id`
FOREIGN KEY (`tsl_criteria_tsl_id`)
REFERENCES `tsl`(`tsl_id`)
ON DELETE NO ACTION ON UPDATE NO ACTION;


===============================================================
(Jul-26-2019)
--
-- Table structure for table `user_criteria`
--

CREATE TABLE `user_criteria` (
  `uc_id` int(11) NOT NULL,
  `uc_name` varchar(300) NOT NULL,
  `uc_user_id` bigint(20) UNSIGNED NOT NULL,
  `uc_product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_criteria`
--
ALTER TABLE `user_criteria`
  ADD PRIMARY KEY (`uc_id`),
  ADD KEY `uc_user_id` (`uc_user_id`),
  ADD KEY `uc_product_id` (`uc_product_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_criteria`
--
ALTER TABLE `user_criteria`
  MODIFY `uc_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_criteria`
--
ALTER TABLE `user_criteria`
  ADD CONSTRAINT `fk_uc_user_id` FOREIGN KEY (`uc_user_id`) REFERENCES `pst_wp_cpd`.`wp_users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;



=====================================================================================
(Jul-29-2019)
--
-- Drop column tsl_criteria_selected
--

ALTER TABLE `tsl_criteria`
DROP `tsl_criteria_selected`;

ALTER TABLE `tsl_criteria`
DROP `tsl_criteria_name`


===================================================================================
(Jul-30-2019)

--
-- Table structure for table `user_location`
--

CREATE TABLE `user_location` (
  `ul_id` int(11) NOT NULL,
  `ul_name` varchar(300) NOT NULL,
  `ul_user_id` bigint(25) UNSIGNED NOT NULL,
  `ul_product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_location`
--
ALTER TABLE `user_location`
  ADD PRIMARY KEY (`ul_id`),
  ADD KEY `ul_product_id` (`ul_product_id`),
  ADD KEY `ul_user_id` (`ul_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_location`
--
ALTER TABLE `user_location`
  MODIFY `ul_id` int(11) NOT NULL AUTO_INCREMENT;

================================================================================
(Aug-07-2019)

--
-- Change homebase client asset.
--
ALTER TABLE `homebase_client_asset` CHANGE `hca_location` `hca_location` INT(11) NULL DEFAULT NULL;


======================================================================================
(Aug-09-2019)

--
-- Add repeat Name
--

ALTER TABLE `cpd` CHANGE `cpd_maybe_name` `cpd_repeat_name` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Repeat Business';


update cpd set cpd_repeat_name='Repeat Business'

===================================================================================

ALTER TABLE `tsl_message_version` CHANGE `tm_focus` `tm_focus` VARCHAR(11)
 CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL;


ALTER TABLE `tsl_email_answer` ADD `tea_default` INT(1) NOT NULL DEFAULT '0' AFTER `tea_content`, ADD INDEX (`tea_default`);

update cpd set cpd_potential_name='No to a Meeting'


=========================================================================================
(Aug-26-2019)

--
-- Insert Sales Activity manager tools.
--
INSERT INTO `tool` (`tool_id`, `tool_tg_id`, `tool_name`, `tool_description`, `tool_controller`,
`tool_wp_name`, `tool_is_admin`, `tool_status`)
 VALUES (NULL, '3', 'Sales Activity Manager', 'Coming Soon.', 'sam', '', '0', 'enabled');

============================================================================================
(Aug-30-2019)

--
-- Add foreign key to asset table.
--
ALTER TABLE `homebase_client_asset` ADD CONSTRAINT `fk_hca_product` FOREIGN KEY (`hca_product_id`)
REFERENCES `user_product`(`u_product_id`) ON DELETE CASCADE ON UPDATE CASCADE;


==========================================================================================
(Sep-13-2019)

--
-- Table structure for table `user_product_group`
--

CREATE TABLE `user_product_group` (
  `upg_id` int(11) NOT NULL,
  `upg_name` varchar(100) NOT NULL,
  `upg_short` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_product_group`
--

INSERT INTO `user_product_group` (`upg_id`, `upg_name`, `upg_short`) VALUES
(1, 'Housing & Hospitality Group', 'HHG'),
(2, 'Retail & Industrial Group', 'RIG'),
(3, 'Medical & Care Group', 'MCG'),
(21, 'Special Product Group', 'SPG');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_product_group`
--
ALTER TABLE `user_product_group`
  ADD PRIMARY KEY (`upg_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_product_group`
--
ALTER TABLE `user_product_group`
  MODIFY `upg_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

=========================================================================

(23-09-2019)

ALTER TABLE `tsl_about_client` ADD `tac_updated_at` DATETIME NULL DEFAULT NULL AFTER `tac_level`;

ALTER TABLE `tsl_about_client` ADD `tac_call_status` INT(11) NOT NULL AFTER `tac_caller`, ADD INDEX (`tac_call_status`);

ALTER TABLE `tsl_about_client` CHANGE `tac_call_status` `tac_call_status_id` INT(11) NOT NULL;

===============================================================================
(24-09-2019)
ALTER TABLE `tsl_message_version`
  DROP `tm_ta`,
  DROP `tm_criteria`,
  DROP `tm_focus`,
  DROP `tm_client_type`,
  DROP `tm_event`,
  DROP `tm_message_type`;


  ALTER TABLE `tsl_message_content`
    DROP `tmc_intro`,
    DROP `tmc_reason`,
    DROP `tmc_message`,
    DROP `tmc_invitation`,
    DROP `tmc_promise`;

ALTER TABLE `tsl_message_version` ADD  CONSTRAINT `fk_outline_id` FOREIGN KEY (`tm_outline_id`) REFERENCES `tsl_message_outline`(`tmo_id`) ON DELETE CASCADE ON UPDATE CASCADE;

    tpao
    msg_seq


============================================================================
(30-09-2019)
--
-- Table structure for table `cpd_deal_info`
--

CREATE TABLE `cpd_deal_info` (
  `cc_id` int(11) NOT NULL,
  `cc_deal_id` int(11) NOT NULL,
  `cc_entity` varchar(100) DEFAULT NULL,
  `cc_title` varchar(10) NOT NULL,
  `cc_fname` varchar(100) DEFAULT NULL,
  `cc_lname` varchar(100) DEFAULT NULL,
  `cc_work_phone` varchar(20) DEFAULT NULL,
  `cc_cell_phone` varchar(20) DEFAULT NULL,
  `cc_email` varchar(150) DEFAULT NULL,
  `cc_website` varchar(150) DEFAULT NULL,
  `cc_messaging` varchar(500) DEFAULT NULL,
  `cc_social` varchar(500) DEFAULT NULL,
  `cc_address` varchar(500) DEFAULT NULL,
  `cc_mailing` varchar(500) DEFAULT NULL,
  `cc_city` varchar(100) DEFAULT NULL,
  `cc_country` varchar(150) DEFAULT NULL,
  `cc_state` varchar(100) DEFAULT NULL,
  `cc_zip` int(20) DEFAULT NULL,
  `cc_type` varchar(20) NOT NULL,
  `cc_add_email` text,
  `cc_add_phone` text,
  `cc_add_address` text,
  `cc_add_message` text,
  `cc_notes` text,
  `cc_note_date` text,
  `cc_info_type` varchar(20) DEFAULT NULL,
  `cc_ref_source` varchar(200) DEFAULT NULL,
  `cc_ref_event` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cpd_deal_info`
--
ALTER TABLE `cpd_deal_info`
  ADD PRIMARY KEY (`cc_id`),
  ADD KEY `cc_type` (`cc_type`),
  ADD KEY `cc_client_id` (`cc_deal_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cpd_deal_info`
--
ALTER TABLE `cpd_deal_info`
  MODIFY `cc_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cpd_deal_info`
--
ALTER TABLE `cpd_deal_info`
  ADD CONSTRAINT `fk_cc_deal_id` FOREIGN KEY (`cc_deal_id`) REFERENCES `homebase_client_asset` (`hca_id`) ON DELETE CASCADE ON UPDATE CASCADE;


======================================================================================

(Oct-03-2019)

--
-- Insert Referral Code type to DB.
--

INSERT INTO `code_type`
(`ct_id`, `ct_abbrev`, `ct_name`, `ct_description`)
VALUES (NULL, 'rds', 'Referral Status', 'Referral Status');


ALTER TABLE `cpd` ADD `cpd_ref_com_name` VARCHAR(100) NOT NULL DEFAULT 'Received & Offered ' AFTER `cpd_proposed_name`, ADD `cpd_ref_acc_name` VARCHAR(100) NOT NULL DEFAULT 'Accepted & In-Play' AFTER `cpd_ref_com_name`, ADD `cpd_ref_rec_name` VARCHAR(100) NOT NULL DEFAULT 'Completed & Paid Out' AFTER `cpd_ref_acc_name`;


ALTER TABLE `tsl_sam` CHANGE `ts_end_time` `ts_end_time` VARCHAR(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;


===================================================================================
(Oct-17-2019)

--
-- Add Portfolio
--

ALTER TABLE `homebase_client_asset`
ADD `hca_portfolio_id` INT(11) NOT NULL AFTER `hca_product_id`;


ALTER TABLE `pst_cpd`.`homebase_client_asset` ADD INDEX `hca_portfolio_id` (`hca_portfolio_id`);

ALTER TABLE `homebase_client_asset` ADD CONSTRAINT `fk_hca_portfolio`
FOREIGN KEY (`hca_portfolio_id`) REFERENCES `user_portfolio`(`up_id`) ON DELETE SET NULL ON UPDATE SET NULL;


=============================================================================================

(Oct-18-2019)


--
-- Update Asset Field.
--
ALTER TABLE `homebase_client_asset`
ADD `hca_city`
VARCHAR(300) NULL DEFAULT NULL AFTER
`hca_portfolio_id`, ADD `hca_state` VARCHAR(300) NULL DEFAULT NULL AFTER `hca_city`;

ALTER TABLE `homebase_client_asset` ADD `hca_desc`
 TEXT NULL DEFAULT NULL AFTER `hca_state`,
  ADD `hca_cond` TEXT NULL DEFAULT NULL AFTER `hca_desc`;


===========================================================================================
(Nov-04-2019)

--
-- Update DB structure for homebase assets.
--
ALTER TABLE `pst_cpd`.`homebase_client_asset`
ADD COLUMN `hca_clu` VARCHAR(2) NULL DEFAULT 'C' AFTER `hca_cond`,
ADD COLUMN `hca_rwt` VARCHAR(2) NULL DEFAULT 'R' AFTER `hca_clu`,
ADD COLUMN `hca_lead` INT(11) NULL DEFAULT NULL AFTER `hca_rwt`,
ADD COLUMN `hca_gen` INT(11) NULL AFTER `hca_lead`;


CREATE TABLE `cpd_call_manager` (
  `ccm_id` int(11) NOT NULL,
  `ccm_start` varchar(50) NOT NULL,
  `ccm_end` varchar(50) DEFAULT NULL,
  `ccm_date` varchar(100) DEFAULT NULL,
  `ccm_purpose` text,
  `ccm_duration` time DEFAULT NULL,
  `ccm_deal_id` int(11) NOT NULL,
  `ccm_rating` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cpd_call_manager`
--
ALTER TABLE `cpd_call_manager`
  ADD PRIMARY KEY (`ccm_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cpd_call_manager`
--
ALTER TABLE `cpd_call_manager`
  MODIFY `ccm_id` int(11) NOT NULL AUTO_INCREMENT;


===========================================================
(Nov-05-2019)
--
-- Table structure for table `homebase_asset_info`
--

CREATE TABLE `homebase_asset_info` (
  `hai_id` int(11) NOT NULL,
  `hai_type` varchar(200) NOT NULL,
  `hai_value` text NOT NULL,
  `hai_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `homebase_asset_info`
--
ALTER TABLE `homebase_asset_info`
  ADD PRIMARY KEY (`hai_id`),
  ADD KEY `hai_user_id` (`hai_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `homebase_asset_info`
--
ALTER TABLE `homebase_asset_info`
  MODIFY `hai_id` int(11) NOT NULL AUTO_INCREMENT;



===============================================================
(Nov-12-2019)

--
-- Add columns to homebase client table.
--
ALTER TABLE `homebase_client`
ADD `hc_website` VARCHAR(500) NULL DEFAULT NULL AFTER `hc_selected_phone`,
ADD `hc_social` VARCHAR(500) NULL DEFAULT NULL AFTER `hc_website`,
ADD `hc_messaging` VARCHAR(500) NULL DEFAULT NULL AFTER `hc_social`,
ADD `hc_title` VARCHAR(200) NULL DEFAULT NULL AFTER `hc_messaging`;

--
-- Add Owner Image
--
ALTER TABLE `homebase_client` ADD `hc_avatar` VARCHAR(500) NULL DEFAULT NULL AFTER `hc_title`;

==================================================================================

--
-- Table structure for table `homebase_owner_info`
--

CREATE TABLE `homebase_owner_info` (
  `hoi_id` int(11) NOT NULL,
  `hoi_client_id` int(11) NOT NULL,
  `hoi_type` varchar(200) NOT NULL,
  `hoi_val` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `homebase_owner_info`
--
ALTER TABLE `homebase_owner_info`
  ADD PRIMARY KEY (`hoi_id`),
  ADD UNIQUE KEY `unique_key` (`hoi_client_id`,`hoi_type`),
  ADD KEY `hoi_type` (`hoi_type`),
  ADD KEY `hoi_client_id` (`hoi_client_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `homebase_owner_info`
--
ALTER TABLE `homebase_owner_info`
  MODIFY `hoi_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `homebase_owner_info`
--
ALTER TABLE `homebase_owner_info`
  ADD CONSTRAINT `fk_client_id` FOREIGN KEY (`hoi_client_id`) REFERENCES `homebase_client` (`hc_id`) ON DELETE CASCADE ON UPDATE CASCADE;


===============================================================================
(Nov-20-2019)

--
-- Drop table cpd_deal_info
--

DROP TABLE `pst_cpd`.`cpd_deal_info`


--
-- Table structure for table `cpd_deal_info`
--

CREATE TABLE `cpd_deal_info` (
  `cc_id` int(11) NOT NULL,
  `cc_deal_id` int(11) NOT NULL,
  `cc_entity` varchar(500) DEFAULT NULL,
  `cc_title` varchar(300) NOT NULL,
  `cc_name` varchar(500) DEFAULT NULL,
  `cc_office_phone` varchar(100) DEFAULT NULL,
  `cc_cell_phone` varchar(100) DEFAULT NULL,
  `cc_address` text,
  `cc_email` varchar(500) DEFAULT NULL,
  `cc_website` varchar(500) DEFAULT NULL,
  `cc_messaging` varchar(500) DEFAULT NULL,
  `cc_social` varchar(500) DEFAULT NULL,
  `cc_type` varchar(20) NOT NULL,
  `cc_add_email` text,
  `cc_add_phone` text,
  `cc_add_address` text,
  `cc_add_message` text,
  `cc_notes` text,
  `cc_note_date` text,
  `cc_info_type` varchar(20) DEFAULT NULL,
  `cc_ref_source` varchar(200) DEFAULT NULL,
  `cc_ref_event` varchar(200) DEFAULT NULL,
  `cc_image` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cpd_deal_info`
--
ALTER TABLE `cpd_deal_info`
  ADD PRIMARY KEY (`cc_id`),
  ADD KEY `cc_type` (`cc_type`),
  ADD KEY `cc_client_id` (`cc_deal_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cpd_deal_info`
--
ALTER TABLE `cpd_deal_info`
  MODIFY `cc_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cpd_deal_info`
--
ALTER TABLE `cpd_deal_info`
  ADD CONSTRAINT `fk_cc_deal_id` FOREIGN KEY (`cc_deal_id`) REFERENCES `homebase_client_asset` (`hca_id`) ON DELETE CASCADE ON UPDATE CASCADE;


=====================================================================================
(Nov-21-2019)

--
-- Table structure for table `cpd_deal_agent_info`
--

CREATE TABLE `cpd_deal_agent_info` (
  `cdai_id` int(11) NOT NULL,
  `cdai_cc_id` int(11) NOT NULL,
  `cdai_val` text NOT NULL,
  `cdai_type` varchar(50) NOT NULL,
  `cdai_form_type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cpd_deal_agent_info`
--
ALTER TABLE `cpd_deal_agent_info`
  ADD PRIMARY KEY (`cdai_id`),
  ADD UNIQUE KEY `cdai_unique` (`cdai_cc_id`,`cdai_type`),
  ADD KEY `cdai_cc_id` (`cdai_cc_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cpd_deal_agent_info`
--
ALTER TABLE `cpd_deal_agent_info`
  MODIFY `cdai_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Add columns for job title and job description.
--

ALTER TABLE `cpd_deal_info`
ADD `cc_job_title` TEXT NULL DEFAULT NULL AFTER `cc_image`,
ADD `cc_job_desc` TEXT NULL DEFAULT NULL AFTER `cc_job_title`;

===================================================

--
-- Table structure for table `homebase_asset_sa`
--

CREATE TABLE `homebase_asset_sa` (
  `has_id` int(11) NOT NULL,
  `has_asset_id` int(11) NOT NULL,
  `has_desc` text,
  `has_declaration` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `homebase_asset_sa`
--
ALTER TABLE `homebase_asset_sa`
  ADD PRIMARY KEY (`has_id`),
  ADD UNIQUE KEY `has_asset_id` (`has_asset_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `homebase_asset_sa`
--
ALTER TABLE `homebase_asset_sa`
  MODIFY `has_id` int(11) NOT NULL AUTO_INCREMENT;

==============================================

ALTER TABLE `tsl` CHANGE `tsl_description` `tsl_description` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;


=================================================================
ALTER TABLE `user_active_cpd_tgd`
  DROP `uact_prod_id`;

  ========================================
--
-- Set up defaults for CLU/RWT
--

ALTER TABLE `deal` CHANGE `deal_clu` `deal_clu` VARCHAR(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'U';

ALTER TABLE `deal` CHANGE `deal_rwt` `deal_rwt` VARCHAR(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'R';

update deal
set deal_clu='C'
where deal_clu is null

update deal
set deal_rwt='R'
where deal_rwt is null


======================================================

--
-- Add columns to homebase asset SA.
--

ALTER TABLE `pst_cpd`.`homebase_asset_sa`
ADD COLUMN `has_desc_fact` TEXT NULL DEFAULT NULL AFTER `has_declaration`,
ADD COLUMN `has_desc_possible` TEXT NULL DEFAULT NULL AFTER `has_desc_fact`,
ADD COLUMN `has_desc_needed` TEXT NULL DEFAULT NULL AFTER `has_desc_possible`,
ADD COLUMN `has_desc_accomplished` TEXT NULL DEFAULT NULL AFTER `has_desc_needed`;


===========================================================
--
-- Update homebase Asset table
--

ALTER TABLE `homebase_client_asset`
 ADD `hca_updated_at` VARCHAR(200) NOT NULL AFTER `hca_gen`,
 ADD `hca_created_at` VARCHAR(200) NOT NULL AFTER `hca_updated_at`;


ALTER TABLE `homebase_client_asset`
 ADD `hca_unit` TINYINT(1) NOT NULL DEFAULT '0' AFTER `hca_created_at`,
 ADD `hca_ft` TINYINT(1) NOT NULL DEFAULT '0' AFTER `hca_unit`;


============================================================
INSERT INTO `tgd_other_income_source` (`tois_id`, `tois_value`, `tois_user_id`) VALUES (NULL, 'Lending Fees', '0'), (NULL, 'Leasing Fees', '0'), (NULL, 'Management Fees', '0'), (NULL, 'Fund Raising Fees', '0');


=====================================================================
[Jan-06-2019]

--
-- Insert Management components.
--

INSERT INTO `universal_component` (`uc_id`, `uc_name`, `uc_controller`, `uc_icon`, `uc_table`, `uc_col_name`,
 `uc_ref_primary_key`, `uc_type`, `uc_tool`, `uc_description`) VALUES (NULL, 'DST', 'dst', 'file', NULL, NULL,
 '', 'mgt', 'cpdv1', 'Deal Status Tracker'), (NULL, 'DCB', 'file', '', NULL, NULL, '', 'mgt', 'cpdv1',
 'Deal Conversion Board'), (NULL, 'SGD', '', 'file', NULL, NULL, '', 'mgt', 'cpdv1', 'Strategic Game Designer'),
  (NULL, 'QVC', '', 'file', NULL, NULL, '', 'mgt', 'cpdv1', 'Quick View Components'),
   (NULL, 'MPA', '', 'file', NULL, NULL, '', 'mgt', 'cpdv1', 'Self –Management Model, Plan, Actual Schedule');


UPDATE `universal_component` SET `uc_controller` = '' WHERE `universal_component`.`uc_id` = 22;
UPDATE `universal_component` SET `uc_icon` = 'file' WHERE `universal_component`.`uc_id` = 22;


=================================================================================
[Jan-07-2019]

--
-- Create Objective table.
--
CREATE TABLE `user_alerts_objective` (
  `uao_id` int(11) NOT NULL,
  `uao_user_id` int(11) NOT NULL,
  `uao_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_alerts_objective`
--

INSERT INTO `user_alerts_objective` (`uao_id`, `uao_user_id`, `uao_name`) VALUES
(1, 0, 'Meeting'),
(2, 0, 'Analysis'),
(3, 0, 'Update'),
(4, 0, 'Survey'),
(5, 0, 'Profile'),
(6, 0, 'Notice'),
(7, 0, 'Promo'),
(8, 0, 'Confirm'),
(9, 0, 'One Page'),
(10, 0, 'Email'),
(11, 0, 'Create Specs'),
(12, 0, 'Send Report'),
(13, 0, 'Send Proposal'),
(14, 0, 'Send OM'),
(15, 0, 'Send Objective');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_alerts_objective`
--
ALTER TABLE `user_alerts_objective`
  ADD PRIMARY KEY (`uao_id`),
  ADD KEY `uao_user_id` (`uao_user_id`);


========================================================
[Jan-08-2019]

--
-- Add DCB table
--

--
-- Table structure for table `cpd_dcb`
--

CREATE TABLE `cpd_dcb` (
  `cd_id` int(11) NOT NULL,
  `cd_asset_id` int(11) NOT NULL,
  `cd_qtr` tinyint(1) NOT NULL,
  `cd_prob` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cpd_dcb`
--
ALTER TABLE `cpd_dcb`
  ADD PRIMARY KEY (`cd_id`),
  ADD UNIQUE KEY `cd_asset_id` (`cd_asset_id`,`cd_qtr`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cpd_dcb`
--
ALTER TABLE `cpd_dcb`
  MODIFY `cd_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cpd_dcb`
--
ALTER TABLE `cpd_dcb`
  ADD CONSTRAINT `fk_cd_asset` FOREIGN KEY (`cd_asset_id`) REFERENCES `homebase_client_asset` (`hca_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `cpd_dcb` CHANGE `cd_prob` `cd_prob` TINYINT(1) NOT NULL DEFAULT '0';



=================================================================================

--
-- Table structure for table `quick_project`
--

CREATE TABLE `quick_project` (
  `qp_id` int(11) NOT NULL,
  `qp_user_id` bigint(25) UNSIGNED NOT NULL,
  `qp_ceo` varchar(1) NOT NULL,
  `qp_project` text NOT NULL,
  `qp_who` int(11) NOT NULL,
  `qp_tag` int(11) NOT NULL,
  `qp_cat` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `quick_project`
--
ALTER TABLE `quick_project`
  ADD PRIMARY KEY (`qp_id`),
  ADD KEY `qp_user_id` (`qp_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `quick_project`
--
ALTER TABLE `quick_project`
  MODIFY `qp_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `quick_project`
--
ALTER TABLE `quick_project`
  ADD CONSTRAINT `fk_qp_user_id` FOREIGN KEY (`qp_user_id`) REFERENCES `pst_wp_cpd`.`wp_users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

  ALTER TABLE `quick_project` CHANGE `qp_id` `qp_id` INT(11) NOT NULL AUTO_INCREMENT, CHANGE `qp_ceo` `qp_ceo` VARCHAR(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'c', CHANGE `qp_project` `qp_project` TEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL, CHANGE `qp_who` `qp_who` INT(11) NULL DEFAULT NULL, CHANGE `qp_tag` `qp_tag` INT(11) NULL DEFAULT NULL, CHANGE `qp_cat` `qp_cat` VARCHAR(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'c';

ALTER TABLE `quick_project` CHANGE `qp_cat` `qp_cat` VARCHAR(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'p';




--
-- Table structure for table `quick_contact`
--

CREATE TABLE `quick_contact` (
  `qc_id` int(11) NOT NULL,
  `qc_user_id` bigint(25) UNSIGNED NOT NULL,
  `qc_ceo` varchar(1) NOT NULL DEFAULT 'c',
  `qc_contact` text,
  `qc_info` text,
  `qc_tag` int(11) DEFAULT NULL,
  `qc_cat` varchar(1) NOT NULL DEFAULT 'p'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `quick_contact`
--
ALTER TABLE `quick_contact`
  ADD PRIMARY KEY (`qc_id`),
  ADD KEY `qc_user_id` (`qc_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `quick_contact`
--
ALTER TABLE `quick_contact`
  MODIFY `qc_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `quick_contact`
--
ALTER TABLE `quick_contact`
  ADD CONSTRAINT `fk_qc_user_id` FOREIGN KEY (`qc_user_id`) REFERENCES `pst_wp_cpd`.`wp_users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;


  ALTER TABLE `quick_contact` ADD `qc_who` INT(11) NULL DEFAULT NULL AFTER `qc_cat`;



--
-- Table structure for table `quick_action`
--

CREATE TABLE `quick_action` (
  `qa_id` int(11) NOT NULL,
  `qa_user_id` bigint(25) UNSIGNED NOT NULL,
  `qa_ceo` varchar(1) NOT NULL DEFAULT 'c',
  `qa_action` text,
  `qa_who` int(11) DEFAULT NULL,
  `qa_tag` int(11) DEFAULT NULL,
  `qa_cat` varchar(1) NOT NULL DEFAULT 'p'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `quick_action`
--
ALTER TABLE `quick_action`
  ADD PRIMARY KEY (`qa_id`),
  ADD KEY `qa_user_id` (`qa_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `quick_action`
--
ALTER TABLE `quick_action`
  MODIFY `qa_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `quick_action`
--
ALTER TABLE `quick_action`
  ADD CONSTRAINT `fk_qa_user_id` FOREIGN KEY (`qa_user_id`) REFERENCES `pst_wp_cpd`.`wp_users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `cpd_acm_deal` ADD CONSTRAINT `fk_cad_deal` FOREIGN KEY (`cad_deal_id`) REFERENCES `homebase_client_asset`(`hca_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `cpd_assembler_version` ADD CONSTRAINT `fk_cav_client` FOREIGN KEY (`cav_client_id`) REFERENCES `homebase_client_asset`(`hca_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `cpd_docs` ADD CONSTRAINT `fk_cd_deal` FOREIGN KEY (`cd_cpd_deal`) REFERENCES `homebase_client_asset`(`hca_id`) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE `homebase_asset_info` CHANGE `hai_user_id` `hai_user_id` BIGINT(25) UNSIGNED NOT NULL;

ALTER TABLE `homebase_asset_info` ADD CONSTRAINT `fk_hai_user` FOREIGN KEY (`hai_user_id`) REFERENCES `pst_wp_cpd`.`wp_users`(`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `homebase_asset_profile_info` ADD CONSTRAINT `fk_hapi_asset` FOREIGN KEY (`hapi_asset_id`) REFERENCES `homebase_client_asset`(`hca_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `homebase_asset_sa` ADD CONSTRAINT `fk_has_asset` FOREIGN KEY (`has_asset_id`) REFERENCES `homebase_client_asset`(`hca_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `homebase_asset_sa_info` ADD CONSTRAINT `fk_hasi_has` FOREIGN KEY (`hasi_has_id`) REFERENCES `homebase_asset_sa`(`has_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `homebase_buyer_info` ADD CONSTRAINT `fk_hbi_asset` FOREIGN KEY (`hbi_asset_id`) REFERENCES `homebase_client_asset`(`hca_id`) ON DELETE CASCADE ON UPDATE CASCADE;


=======================================================================

--
-- Add Walk by meeting outline.
--

INSERT INTO `tsl_script_default` (`tsl_script_default_id`, `tsl_script_type`, `tsl_script_content`, `tsl_script_origin`, `tsl_script_requester`) VALUES (NULL, 'Industry Event “Walk By” Meeting', '<div><!--block--><strong>[[Embedded Practice: Exposing a Problem]]</strong><br><br>The following is a detailed preparation outline for what will amount to in-person meeting of only a few minutes at an Industry or Client event site.<br><br><strong>{{Meeting Logistics}}<br>((Walking Around the Event))<br>((Say Hello))<br>((Talk Duration 1-3 Minutes))<br>((Share Contact Information))</strong><br><strong>((Next Action: Scheduled Future Call))</strong><br><br><strong>{{Conversation Format - Promo Outline}}<br>((Big Headline)) <br>((Emotional Connection)) <br>((Short Inside Scoop)) <br>((Compelling Offer))<br>((Bold Promise)) <br></strong><br><strong>{{Discussion Topic Categories}}<br>((Economic))&nbsp;<br>((Capital))&nbsp;<br>((Demographics))&nbsp;<br>((Demand))<br>((Market))&nbsp;<br></strong><br>[...]


=============================================================================

--
-- Update Objectives
--

INSERT INTO `user_alerts_objective` (`uao_id`, `uao_user_id`, `uao_name`, `uao_category`) VALUES
(22, 0, 'Land Line', 'Call Back'),
(23, 0, 'Web Call', 'Call Back'),
(24, 0, 'Web Link', 'Call Back'),
(25, 0, 'Text Message', 'Call Back'),
(26, 0, 'Profile', 'Email'),
(27, 0, 'Survey', 'Email'),
(28, 0, 'Follow-Up', 'Email'),
(29, 0, 'Reminder', 'Email'),
(30, 0, 'Email', 'Confirm'),
(31, 0, 'Snail Main', 'Confirm'),
(32, 0, 'Phone Call', 'Confirm'),
(33, 0, 'Regular', 'Notice'),
(34, 0, 'Recurring', 'Notice'),
(35, 0, 'Random', 'Notice'),
(36, 0, 'Email', 'Update'),
(37, 0, 'Phone Call', 'Update'),
(38, 0, 'One-Page', 'Update'),
(39, 0, 'Exploratory', 'Meeting'),
(40, 0, 'Presentation', 'Meeting'),
(41, 0, 'Launch', 'Meeting'),
(42, 0, 'Negotiation', 'Meeting'),
(43, 0, 'Closing', 'Meeting'),
(44, 0, 'Post-Closing', 'Meeting'),
(45, 0, 'Acquisition', 'Meeting'),
(46, 0, 'Property Tour', 'Meeting'),
(47, 0, 'Inspection', 'Meeting'),
(48, 0, 'Market Update', 'Document'),
(49, 0, 'Strategic Analysis', 'Document'),
(50, 0, 'Detailed Analysis', 'Document'),
(51, 0, 'Marketing Proposal', 'Document'),
(52, 0, 'Listing Agreement', 'Document'),
(53, 0, 'Confidentiality Agreement', 'Document'),
(54, 0, 'Rep Agreement', 'Document'),
(55, 0, 'Offering Memorandum', 'Document'),
(56, 0, 'Purchase & Sale Agreement', 'Document');


==========================================================================================
--
-- Table structure for table `user_reminder_recurrence`
--

CREATE TABLE `user_reminder_recurrence` (
  `urr_id` int(11) NOT NULL,
  `urr_user_id` bigint(25) UNSIGNED NOT NULL,
  `urr_interval` int(200) NOT NULL,
  `urr_last_shown` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_reminder_recurrence`
--
ALTER TABLE `user_reminder_recurrence`
  ADD PRIMARY KEY (`urr_id`),
  ADD UNIQUE KEY `urr_user_id` (`urr_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_reminder_recurrence`
--
ALTER TABLE `user_reminder_recurrence`
  MODIFY `urr_id` int(11) NOT NULL AUTO_INCREMENT;


============================================================================================
[Feb-13-2020]

--
-- Table structure for table `user_sgd`
--

CREATE TABLE `user_sgd` (
  `us_id` int(11) NOT NULL,
  `us_user_id` bigint(25) UNSIGNED NOT NULL,
  `us_tab` varchar(200) NOT NULL,
  `us_type` varchar(200) NOT NULL,
  `us_seq` int(11) NOT NULL,
  `us_text` text,
  `us_by_when` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_sgd`
--
ALTER TABLE `user_sgd`
  ADD PRIMARY KEY (`us_id`),
  ADD UNIQUE KEY `us_unique` (`us_seq`,`us_tab`,`us_user_id`,`us_type`) USING BTREE,
  ADD KEY `us_user_id` (`us_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_sgd`
--
ALTER TABLE `user_sgd`
  MODIFY `us_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_sgd`
--
ALTER TABLE `user_sgd`
  ADD CONSTRAINT `fk_us_by_when` FOREIGN KEY (`us_user_id`) REFERENCES `pst_wp_cpd`.`wp_users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;


  --
  -- Table structure for table `user_sgd_month_count`
  --

  CREATE TABLE `user_sgd_month_count` (
    `usmc_id` int(11) NOT NULL,
    `usmc_user_id` bigint(25) UNSIGNED NOT NULL,
    `usmc_type` varchar(200) NOT NULL,
    `usmc_month` int(11) NOT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  --
  -- Indexes for dumped tables
  --

  --
  -- Indexes for table `user_sgd_month_count`
  --
  ALTER TABLE `user_sgd_month_count`
    ADD PRIMARY KEY (`usmc_id`),
    ADD UNIQUE KEY `usmc_unique` (`usmc_user_id`,`usmc_type`),
    ADD KEY `usmc_user_id` (`usmc_user_id`);

  --
  -- AUTO_INCREMENT for dumped tables
  --

  --
  -- AUTO_INCREMENT for table `user_sgd_month_count`
  --
  ALTER TABLE `user_sgd_month_count`
    MODIFY `usmc_id` int(11) NOT NULL AUTO_INCREMENT;
===============================================================================
[Feb-18-2019]
ALTER TABLE `tsl_actions` ADD `ta_row_color` VARCHAR(100) NULL DEFAULT NULL AFTER `ta_reason`;

ALTER TABLE `tsl_about_client` ADD `tac_cb_row_color` VARCHAR(100) NULL DEFAULT NULL AFTER `tac_updated_at`;

ALTER TABLE `user_alerts` ADD `ua_row_color` VARCHAR(100) NULL DEFAULT NULL AFTER `ua_completion_date`;


========================================================================================
[Feb-20-2019]

ALTER TABLE `homebase_client_asset` ADD `hca_avatar` VARCHAR(200) NULL DEFAULT NULL AFTER `hca_ft`;


========================================================================================
[Mar-02-2020]

ALTER TABLE `tsl_notes` ADD `tn_next_call` DATETIME NULL DEFAULT NULL AFTER `tn_client_id`;
ALTER TABLE `tsl_notes` CHANGE `tn_next_call` `tn_next_call` VARCHAR(200) NULL;
ALTER TABLE `tsl_notes` CHANGE `tn_next_call` `tn_next_call` DATETIME NULL DEFAULT NULL;
ALTER TABLE `tsl_notes` ADD `tn_action` TEXT NULL DEFAULT NULL AFTER `tn_seq`;


==============================================================================
ALTER TABLE `homebase_client_asset` CHANGE `hca_unit` `hca_unit` VARCHAR(100) NOT NULL DEFAULT '0';
ALTER TABLE `homebase_client_asset` CHANGE `hca_ft` `hca_ft` VARCHAR(100) NOT NULL DEFAULT '0';


=====================================================================================
[Mar-12-2019]
--
-- Table structure for table `agent_asset_info`
--

CREATE TABLE `agent_asset_info` (
  `aai_id` int(11) NOT NULL,
  `aai_built_year` varchar(200) DEFAULT NULL,
  `aai_units` varchar(200) DEFAULT NULL,
  `aai_price_per_unit` varchar(200) DEFAULT NULL,
  `aai_price_per_sf` varchar(200) DEFAULT NULL,
  `aai_avg_sf` varchar(200) DEFAULT NULL,
  `aai_list_price` varchar(200) DEFAULT NULL,
  `aai_hca_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agent_asset_info`
--
ALTER TABLE `agent_asset_info`
  ADD PRIMARY KEY (`aai_id`),
  ADD UNIQUE KEY `aai_hca_id` (`aai_hca_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agent_asset_info`
--
ALTER TABLE `agent_asset_info`
  MODIFY `aai_id` int(11) NOT NULL AUTO_INCREMENT;


ALTER TABLE `agent_profile` ADD `ap_image` VARCHAR(500) NULL AFTER `ap_second_phone`;


===============================
--
-- Update Buyer Tab Name.
--
UPDATE `cpd` SET cpd_tour_name='Buyer Requested & Signed CA'
UPDATE `cpd` SET cpd_offer_name='Submitted Offer & Received Counter Offer'
UPDATE `cpd` SET cpd_b_contract_name='Contingencies Removed & Contract Closed'


ALTER TABLE `cpd` CHANGE `cpd_tour_name` `cpd_tour_name` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Buyer Requested & Signed CA';

ALTER TABLE `cpd` CHANGE `cpd_offer_name` `cpd_offer_name` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Submitted Offer & Received Counter Offer';

ALTER TABLE `cpd` CHANGE `cpd_b_contract_name` `cpd_b_contract_name` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Contingencies Removed & Contract Closed';

ALTER TABLE `cpd` ADD `cpd_diligence_name` VARCHAR(50) NOT NULL DEFAULT 'Offer Accepted & Due Diligence Launched' AFTER `cpd_ref_compl_name`;

INSERT INTO `code` (`code_id`, `code_type_id`, `code_abbrev`, `code_name`, `code_description`) VALUES (NULL, '27', 'diligence', 'Diligence', 'Offer Accepted & Due Diligence Launched');


UPDATE `code` SET `code_name` = 'Buyer Requested' WHERE `code`.`code_id` = 99;
UPDATE `code` SET `code_description` = 'Buyer Requested' WHERE `code`.`code_id` = 99;
UPDATE `code` SET `code_abbrev` = 'buyer_req' WHERE `code`.`code_id` = 99;
UPDATE `code` SET `code_abbrev` = 'sign_ca' WHERE `code`.`code_id` = 100;
UPDATE `code` SET `code_name` = 'Signed CA' WHERE `code`.`code_id` = 100;
UPDATE `code` SET `code_description` = 'Signed CA' WHERE `code`.`code_id` = 100;
UPDATE `code` SET `code_abbrev` = 'count_offer' WHERE `code`.`code_id` = 102;
UPDATE `code` SET `code_name` = 'Received Counter Offer' WHERE `code`.`code_id` = 102;
UPDATE `code` SET `code_description` = 'Received Counter Offer' WHERE `code`.`code_id` = 102;
UPDATE `code` SET `code_abbrev` = 'subm_offer' WHERE `code`.`code_id` = 101;
UPDATE `code` SET `code_name` = 'Submitted Offer' WHERE `code`.`code_id` = 101;
UPDATE `code` SET `code_description` = 'Submitted Offer' WHERE `code`.`code_id` = 101;
UPDATE `code` SET `code_name` = 'Offer Accepted' WHERE `code`.`code_id` = 103;
UPDATE `code` SET `code_description` = 'Offer Accepted' WHERE `code`.`code_id` = 103;
UPDATE `code` SET `code_abbrev` = 'offer_acc' WHERE `code`.`code_id` = 103;
UPDATE `code` SET `code_abbrev` = 'diligence' WHERE `code`.`code_id` = 104;
UPDATE `code` SET `code_name` = 'Due Diligence Launched' WHERE `code`.`code_id` = 104;
UPDATE `code` SET `code_abbrev` = 'Due Diligence Launched' WHERE `code`.`code_id` = 104;

INSERT INTO `code` (`code_id`, `code_type_id`, `code_abbrev`, `code_name`, `code_description`) VALUES (NULL, '28', 'contingency', 'Contingencies Removed', 'Contingencies Removed'), (NULL, '28', 'b_contr_c', 'Contract Closed', 'Contract Closed');

====================================
--
-- Change buyer status order
--

UPDATE `code` SET `code_id` = '117' WHERE `code`.`code_id` = 99;
UPDATE `code` SET `code_id` = '99' WHERE `code`.`code_id` = 116;
UPDATE `code` SET `code_id` = '116' WHERE `code`.`code_id` = 100;
UPDATE `code` SET `code_id` = '100' WHERE `code`.`code_id` = 115;
UPDATE `code` SET `code_id` = '115' WHERE `code`.`code_id` = 101;
UPDATE `code` SET `code_id` = '101' WHERE `code`.`code_id` = 104;
UPDATE `code` SET `code_id` = '104' WHERE `code`.`code_id` = 102;


=======================================
--
-- Table structure for table `user_active_tsl`
--

CREATE TABLE `user_active_tsl` (
  `uat_id` int(11) NOT NULL,
  `uat_user_id` bigint(25) UNSIGNED NOT NULL,
  `uat_tsl_id` int(11) NOT NULL,
  `uat_to_delete` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user_active_tsl`
--
ALTER TABLE `user_active_tsl`
  ADD PRIMARY KEY (`uat_id`),
  ADD UNIQUE KEY `uat_id` (`uat_tsl_id`,`uat_user_id`) USING BTREE,
  ADD KEY `fk_uat_user_id` (`uat_user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user_active_tsl`
--
ALTER TABLE `user_active_tsl`
  MODIFY `uat_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_active_tsl`
--
ALTER TABLE `user_active_tsl`
  ADD CONSTRAINT `fk_uat_tsl_id` FOREIGN KEY (`uat_tsl_id`) REFERENCES `tsl` (`tsl_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_uat_user_id` FOREIGN KEY (`uat_user_id`) REFERENCES `pst_wp_cpd`.`wp_users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;


=================================================================
--
-- Update CPD to DMD
--
UPDATE `tool` SET `tool_description` = 'Deal Management Dashboard' WHERE `tool`.`tool_id` = 9;
UPDATE `tool` SET `tool_name` = 'Deal Management Dashboard' WHERE `tool`.`tool_id` = 9;

ALTER TABLE `collab` ADD `collab_user_email` VARCHAR(500) NOT NULL AFTER `collab_created_at`;
ALTER TABLE `collab` CHANGE `collab_user_email` `collab_user_email` VARCHAR(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;


===================================================================
--
-- Add buyer active and inactive table
--

INSERT INTO `code` (`code_id`, `code_type_id`, `code_abbrev`, `code_name`, `code_description`) VALUES (NULL, '27', 'b_active', 'b_active', 'Buyer Inactive & Active');


INSERT INTO `code` (`code_id`, `code_type_id`, `code_abbrev`, `code_name`, `code_description`) VALUES (NULL, '28', 'b_inactive', 'Buyer Inactive', 'Buyer Inactive'), (NULL, '28', 'b_active', 'Buyer Active', 'Buyer Active');

UPDATE `code` SET `code_id` = '121' WHERE `code`.`code_id` = 119;

ALTER TABLE `cpd` ADD `cpd_b_active_name` VARCHAR(200) NOT NULL DEFAULT 'Buyer Inactive & Active' AFTER `cpd_diligence_name`;


===================================================================
ALTER TABLE `homebase_buyer_asset` CHANGE `hba_market_area` `hba_market_area` INT(11) NULL DEFAULT NULL;
ALTER TABLE `homebase_buyer_asset` ADD `hba_avatar` VARCHAR(500) NULL DEFAULT NULL AFTER `hba_acquisition_strategy`;
ALTER TABLE `homebase_buyer_asset` ADD `hba_status` INT(11) NULL DEFAULT NULL AFTER `hba_avatar`;


--
-- Table structure for table `homebase_buyer`
--

CREATE TABLE `homebase_buyer` (
  `hb_id` int(11) NOT NULL,
  `hb_user_id` bigint(25) UNSIGNED NOT NULL,
  `hb_name` varchar(500) NOT NULL,
  `hb_main_phone` varchar(50) DEFAULT NULL,
  `hb_second_phone` varchar(50) DEFAULT NULL,
  `hb_mobile_phone` varchar(50) DEFAULT NULL,
  `hb_email` varchar(500) DEFAULT NULL,
  `hb_entity_name` varchar(500) DEFAULT NULL,
  `hb_rwt` varchar(1) NOT NULL DEFAULT 'r'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `homebase_buyer`
--
ALTER TABLE `homebase_buyer`
  ADD PRIMARY KEY (`hb_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `homebase_buyer`
--
ALTER TABLE `homebase_buyer`
  MODIFY `hb_id` int(11) NOT NULL AUTO_INCREMENT;


  CREATE TABLE `homebase_buyer_asset` (
    `hba_id` int(11) NOT NULL,
    `hba_hb_id` int(11) NOT NULL,
    `hba_product_type` int(11) NOT NULL,
    `hba_acquisition` varchar(500) DEFAULT NULL,
    `hba_entity_name` varchar(200) DEFAULT NULL,
    `hba_entity_address` varchar(500) DEFAULT NULL,
    `hba_city` varchar(500) DEFAULT NULL,
    `hba_state` varchar(500) DEFAULT NULL,
    `hba_zip` varchar(50) DEFAULT NULL,
    `hba_buyer_type` varchar(500) DEFAULT NULL,
    `hba_market_area` int(11) DEFAULT NULL,
    `hba_units` varchar(50) DEFAULT NULL,
    `hba_size` varchar(50) DEFAULT NULL,
    `hba_condition` int(11) DEFAULT NULL,
    `hba_criteria` int(11) DEFAULT NULL,
    `hba_price_range` varchar(200) DEFAULT NULL,
    `hba_cap_rate` varchar(500) DEFAULT NULL,
    `hba_financing` varchar(500) DEFAULT NULL,
    `hba_contingency` varchar(500) DEFAULT NULL,
    `hba_term` varchar(500) DEFAULT NULL,
    `hba_acquisition_strategy` varchar(500) DEFAULT NULL,
    `hba_avatar` varchar(500) DEFAULT NULL,
    `hba_status` int(11) DEFAULT NULL
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  --
  -- Indexes for dumped tables
  --

  --
  -- Indexes for table `homebase_buyer_asset`
  --
  ALTER TABLE `homebase_buyer_asset`
    ADD PRIMARY KEY (`hba_id`),
    ADD KEY `hba_hb_id` (`hba_hb_id`),
    ADD KEY `hba_product_type` (`hba_product_type`);

  --
  -- AUTO_INCREMENT for dumped tables
  --

  --
  -- AUTO_INCREMENT for table `homebase_buyer_asset`
  --
  ALTER TABLE `homebase_buyer_asset`
    MODIFY `hba_id` int(11) NOT NULL AUTO_INCREMENT;


====================================================

ALTER TABLE `homebase_buyer` CHANGE `hb_rwt` `hb_rwt` VARCHAR(1) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'R';
ALTER TABLE `homebase_buyer_asset` CHANGE `hba_status` `hba_status` VARCHAR(51) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE `tsl_client` ADD `tc_type` VARCHAR(50) NOT NULL DEFAULT '' AFTER `tc_temp`;


Update unique constraint.
Remove foreign key from tsl_client


=======================================================

ALTER TABLE `homebase_buyer` ADD `hb_selected_phone` VARCHAR(50) NOT NULL AFTER `hb_rwt`;
ALTER TABLE `homebase_buyer` CHANGE `hb_selected_phone` `hb_selected_phone` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'hc_mobile_phone';
ALTER TABLE `deal` ADD `deal_type` VARCHAR(50) NOT NULL DEFAULT '' AFTER `deal_updated`;
ALTER TABLE `homebase_asset_status` ADD `has_type` VARCHAR(50) NOT NULL DEFAULT '' AFTER `has_datetime`;
ALTER TABLE `tsl_notes` ADD `tn_row_color` VARCHAR(100) NULL DEFAULT NULL AFTER `tn_action`;
ALTER TABLE `tsl_notes` ADD `tn_objective` INT(11) NULL DEFAULT NULL AFTER `tn_row_color`;


Deal asset id drop
deal type with deal asset unique
drop homebase_asset_status fk
================================================================

DELETE FROM `component_phase` WHERE cp_component='profile' and cp_outline_id <> 0



INSERT INTO `component_phase` (`cp_id`, `cp_name`, `cp_number`, `cp_component`, `cp_content`, `cp_outline_id`) VALUES (NULL, 'Client Project', '1', 'profile', 'Section 1: Client Project:

Question 1: {{As an owner, what is the history and scope of your property ownership?}}
Question 2 :{{As an owner, how have the facts of your situation changed over time?}}
Question 3 :{{As an owner, what are some of your biggest accomplishments?}}
Question 4: {{As an owner, what is currently needed to move you forward?}}
Question 5: {{As an owner, what do you see as possible for you over the long term?}}', '0'), (NULL, 'Client Challenges', '2', 'profile', 'Section 2: Client Challenges:

Question 1: {{As an owner, what challenges are you facing with your property?}}
Question 2 :{{As an owner, what challenges are you facing with the people involved?}}
Question 3 :{{As an owner, what challenges are you facing financially?}}
Question 4: {{As an owner, what challenges are you facing legally?}}
Question 5: {{As an owner, what challenges are you facing in your market?}}', '0'), (NULL, 'Client Objectives', '3', 'profile', 'Section 2: Client Objectives:

Question 1: {{What are you looking to build over the long term?}}
Question 2 :{{What is your long-term acquisition plan?}}
Question 3 :{{What dispositions will support your acquisition objectives?}}
Question 4: {{What is your long-term exit strategy?}}
Question 5: {{Who else will be a beneficiary of your exit strategy?}}', '0');

INSERT INTO `tsl_profile_question` (`tpq_id`, `tpq_content`, `tpq_phase`) VALUES (NULL, 'As an owner, what is the history and scope of your property ownership?', '198'), (NULL, 'As an owner, how have the facts of your situation changed over time?', '198'), (NULL, 'As an owner, what are some of your biggest accomplishments?', '198'), (NULL, 'As an owner, what is currently needed to move you forward?', '198'), (NULL, 'As an owner, what do you see as possible for you over the long term?', '198'), (NULL, 'As an owner, what challenges are you facing with your property?', '199'), (NULL, 'As an owner, what challenges are you facing with the people involved?', '199'), (NULL, 'As an owner, what challenges are you facing financially?', '199'), (NULL, 'As an owner, what challenges are you facing legally?', '199'), (NULL, 'As an owner, what challenges are you facing in your market?', '199'), (NULL, 'What are you looking to build over the long term?', '200'), (NULL, 'What is your long-term acquisition plan?', '200'), (NULL, 'What dispositions will support your acquisition objectives?', '200'), (NULL, 'What is your long-term exit strategy?', '200'), (NULL, 'Who else will be a beneficiary of your exit strategy?', '200');


UPDATE `code` SET `code_name` = 'Buyer Considered' WHERE `code`.`code_id` = 121;
UPDATE `code` SET `code_description` = 'Buyer Considered' WHERE `code`.`code_id` = 121;
UPDATE `code` SET `code_name` = 'Buyer Contacted' WHERE `code`.`code_id` = 120;
UPDATE `code` SET `code_description` = 'Buyer Contacted' WHERE `code`.`code_id` = 120;

ALTER TABLE `cpd` CHANGE `cpd_b_active_name` `cpd_b_active_name` VARCHAR(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT 'Buyer Considered & Contacted';
update cpd set cpd_b_active_name='Buyer Considered & Contacted' where cpd_id<>0




INSERT INTO `tsl_script_default` (`tsl_script_default_id`, `tsl_script_type`, `tsl_script_content`, `tsl_script_origin`, `tsl_script_requester`) VALUES (NULL, 'Strategic Analysis Worksheet', '<div><!--block-->[[<strong>Strategic Analysis Worksheet</strong>]]<strong><br></strong><br></div><div><!--block-->{{Client Name:}}<br>{{Project:}}<br><br></div><div><!--block-->Use this worksheet template to guide questions of client. Simply replace the existing text. There are five<br>categories of potential factors including Asset, People, Financial, Legal and Market.<br><br></div><div><!--block-->{{ <strong>Factor 1: </strong>}}</div><div><!--block-->Compose a sentence or two that describes the Challenge this factor presents and the Implications of that<br>challenge on their future.<br><br></div><div><!--block-->{{ <strong>Factor 2: </strong>}}</div><div><!--block-->Compose a sentence or two that describes the Challenge this factor presents and the Implications of that<br>challenge on their future.<br><br>{{ <strong>Factor 3: </strong>}}</div><div><!--block-->Compose a sentence or two that describes the Challenge this factor presents and the Implications of that<br>challenge on their future.<br><br></div><div><!--block-->{{ <strong>Option 1: Do Nothing: </strong>}}</div><div><!--block-->Compose a sentence or two that describes the Consequences of this choice.<br><br></div><div><!--block-->{{ <strong>Option 2: </strong>}}</div><div><!--block-->Compose a sentence or two that describes the Consequences of this choice.<br><br></div><div><!--block-->{{ <strong>Option 3: Always 3 or more Options: </strong>}}</div><div><!--block-->Compose a sentence or two that describes the Consequences of this choice.<br><br></div><div><!--block-->{{ <strong>Project Description: </strong>}}</div><div><!--block-->Compose a sentence or two that describes their Project which is to help the client get from where they are to<br>where they want to be in the future.<br><br></div><div><!--block-->{{ <strong>Recommendation: Option 3: </strong>}}</div><div><!--block-->Compose a sentence or two that describes how this option best accomplishes their Project which is to help the client get from where they are to where they want to be in the best possible way.</div>', 'tsl', 'wks');


=============================
UPDATE `component_phase` SET `cp_name` = 'Project Section' WHERE `component_phase`.`cp_id` = 360;
UPDATE `component_phase` SET `cp_name` = 'Challenge Section' WHERE `component_phase`.`cp_id` = 361;
UPDATE `component_phase` SET `cp_outline_id` = '-1' WHERE `component_phase`.`cp_id` = 362;

UPDATE `component_phase` SET `cp_content` = '<div><!--block--><strong>Client Project Section<br><br>[[History Question]]</strong><br>{{Question 1}}<br>{{Question 2}}<br>{{Question 3}}<br><br><strong>[[Current Facts Questions]]</strong><br>{{Question 1}}<br>{{Question 2}}<br>{{Question 3}}<br><br><strong>[[Accomplished Questions]]</strong><br>{{Question 1}}<br>{{Question 2}}<br>{{Question 3}}<br><br><strong>[[Needed - Problems Questions]]</strong><br>{{Question 1}}<br>{{Question 2}}<br>{{Question 3}}<br><br><strong>[[Possible - Project Questions]]</strong><br>{{Question 1}}<br>{{Question 2}}<br>{{Question 3}}</div>' WHERE `component_phase`.`cp_id` = 360;

UPDATE `component_phase` SET `cp_content` = '<div><!--block--><strong>Client Challenges Section<br><br>[[Asset Challenges]]</strong><br>{{Challenge Question 1}}<br>{{Challenge Question 2}}<br>{{Challenge Question 3}}<br><br><strong>[[People Challenges]]</strong><br>{{Challenge Question 1}}<br>{{Challenge Question 2}}<br>{{Challenge Question 3}}<br><br><strong>[[Financial Challenges]]</strong><br>{{Challenge Question 1}}<br>{{Challenge Question 2}}<br>{{Challenge Question 3}}<br><br><strong>[[Legal Challenges]]</strong><br>{{Challenge Question 1}}<br>{{Challenge Question 2}}<br>{{Challenge Question 3}}<br><br><strong>[[Market Challenges]]</strong><br>{{Challenge Question 1}}<br>{{Challenge Question 2}}<br>{{Challenge Question 3}}</div>' WHERE `component_phase`.`cp_id` = 361;

delete from tsl_profile_question where tpq_id<>0
ALTER TABLE `tsl_profile_question` ADD `tpq_seq` INT(11) NOT NULL AFTER `tpq_phase`;



ALTER TABLE `tsl_profile_question` ADD `tpq_question` TEXT NULL AFTER `tpq_seq`;


=====================================================================================================
delete from tsl_script_default
where tsl_script_requester='wks';

INSERT INTO `tsl_script_default` (`tsl_script_default_id`, `tsl_script_type`, `tsl_script_content`, `tsl_script_origin`, `tsl_script_requester`) VALUES (NULL, 'Cover Page – CNC', '<div><!--block-->[[ <strong>Cover Page Outline </strong>]]<strong><br></strong>&nbsp;</div><div><!--block-->{{ Client Name }}<br>{{ Property Name}}&nbsp;</div><div><!--block-->{{ Property Location }}</div><div><!--block-->{{ Designated Advisor }}</div><div><!--block-->{{ Submission Date }}</div>', 'tsl', 'wks');


INSERT INTO `tsl_script_default` (`tsl_script_default_id`, `tsl_script_type`, `tsl_script_content`, `tsl_script_origin`, `tsl_script_requester`) VALUES (NULL, 'Welcome Page – Evoking Rapport – CTX', '<div><!--block-->[[ <strong>Welcome Page Outline </strong>]]<strong><br></strong>&nbsp;</div><div><!--block-->{{ Step 1: Opening - Lead }}<br>{{ Step 2: Permission - Ground }}&nbsp;</div><div><!--block-->{{ Step 3: Interest - Key }}</div><div><!--block-->{{ Step 4: Detachment – Offer }}</div><div><!--block-->{{ Step 5: Questions – Transition }}</div>', 'tsl', 'wks');

INSERT INTO `tsl_script_default` (`tsl_script_default_id`, `tsl_script_type`, `tsl_script_content`, `tsl_script_origin`, `tsl_script_requester`) VALUES (NULL, 'Factors Page – Strategic Analysis - CNT', '<div><!--block-->[[ <strong>Project Page Outline </strong>]]<strong><br></strong>&nbsp;</div><div><!--block-->{{ Step 1: History - Lead }}<br>{{ Step 2: Current Facts - Ground }}&nbsp;</div><div><!--block-->{{ Step 3:Accomplished- Key }}</div><div><!--block-->{{ Step 4: Needed – Offer }}</div><div><!--block-->{{ Step 5: Possible – Transition }}</div>', 'tsl', 'wks');

INSERT INTO `tsl_script_default` (`tsl_script_default_id`, `tsl_script_type`, `tsl_script_content`, `tsl_script_origin`, `tsl_script_requester`) VALUES (NULL, 'Factors Page  – Strategic Analysis - CNT', '<div><!--block-->[[ <strong>Factors Page Outline </strong>]]<strong><br></strong>&nbsp;<br>[[ <strong>Asset Challenges </strong>]]</div><div><!--block-->{{ Asset Factor 1 }}<br>{{ Asset Factor 2 }}&nbsp;</div><div><!--block-->{{ Asset Factor 3 }}<br><br>[[ <strong>People Challenges </strong>]]</div><div><!--block-->{{ People Factor 1 }}<br>{{ People Factor 2 }}&nbsp;</div><div><!--block-->{{ People Factor 3 }}<br><br>[[ <strong>Financial Challenges </strong>]]</div><div><!--block-->{{ Financial Factor 1 }}<br>{{ Financial Factor 2 }}&nbsp;</div><div><!--block-->{{ Financial Factor 3 }}<br><br>[[ <strong>Legal Challenges </strong>]]</div><div><!--block-->{{ Legal Factor 1 }}<br>{{ Legal Factor 2 }}&nbsp;</div><div><!--block-->{{ Legal Factor 3 }}<br><br>[[ M<strong>arket Challenges </strong>]]</div><div><!--block-->{{ Market Factor 1 }}<br>{{ Market Factor 2 }}&nbsp;</div><div><!--block-->{{ Market Factor 3 }}</div>', 'tsl', 'wks');

INSERT INTO `tsl_script_default` (`tsl_script_default_id`, `tsl_script_type`, `tsl_script_content`, `tsl_script_origin`, `tsl_script_requester`) VALUES (NULL, 'Options Page – Strategic Analysis - CNT', '<div><!--block-->[[ <strong>Options Page Outline </strong>]]<br>[[ <strong>Options &amp; Consequences </strong>]]<strong><br></strong><br></div><div><!--block-->{{ Option 1: Name }}<br>{{ Option 2: Name }}&nbsp;</div><div><!--block-->{{ Option 3: Name }}</div><div><!--block-->{{ Option 4: Name }}</div><div><!--block-->{{ Option 5: Name }}</div>', 'tsl', 'wks'), (NULL, 'Recommendation Page – Forging Partnership - CNT', '<div><!--block-->[[ <strong>Recommendations Page Outline </strong>]]</div><div><!--block-->{{ Recommended Option: Number &amp; Name }}<br><br>[[ <strong>Forging Partnership Practice </strong>]]</div><div><!--block-->{{ Step 1: Risks – Lead }}<br>{{ Step 2: Responsibility – Ground }}<br>{{ Step 3: Declaration – Key }}<br>{{ Step 4: Invitation - Offer }}<br>{{ Step 5: Promise - Transition }}<br><br></div>', 'tsl', 'wks');

INSERT INTO `tsl_script_default` (`tsl_script_default_id`, `tsl_script_type`, `tsl_script_content`, `tsl_script_origin`, `tsl_script_requester`) VALUES (NULL, 'Table Of Exhibits Page - CNT', '<div><!--block-->[[ <strong>Table of Exhibits Outline </strong>]]</div><div><!--block--><br></div><div><!--block-->{{ Exhibit 1: Name }}<br>{{ Exhibit 2: Name }}<br>{{ Exhibit 3: Name }}<br>{{ Exhibit 4: Name }}<br>{{ Exhibit 5: Name }}<br>{{ Exhibit 6: Name }}<br><br></div>', 'tsl', 'wks'), (NULL, 'Program Back Page - COM', '<div><!--block-->[[ <strong>Program Page Outline </strong>]]</div><div><!--block-->{{ Mission Statement<strong> }}</strong><br><br>[[ <strong>Program Sequence </strong>]]</div><div><!--block-->{{ Stage 1: Name }}<br>{{ Stage 2: Name }}<br>{{ Stage 3: Name }}<br>{{ Stage 4: Name }}<br>{{ Stage 5: Name }}<br>{{ Stage 6: Name }}<br>{{ Stage 7: Name }}<br>{{ Stage 8: Name }}</div>', 'tsl', 'wks');

==================================================================================

ALTER TABLE `cpd_deal_info` ADD `cc_is_owner` VARCHAR(20) NOT NULL DEFAULT 'false' AFTER `cc_job_desc`;
ALTER TABLE cpd_deal_info DROP FOREIGN KEY fk_cc_deal_id;

===========================================

ALTER TABLE `homebase_client_asset` ADD `hca_type` TEXT NULL DEFAULT NULL AFTER `hca_avatar`;

--
-- Table structure for table `agent_asset_info`
--

CREATE TABLE `agent_asset_info` (
  `aai_id` int(11) NOT NULL,
  `aai_built_year` varchar(200) DEFAULT NULL,
  `aai_units` varchar(200) DEFAULT NULL,
  `aai_price_per_unit` varchar(200) DEFAULT NULL,
  `aai_price_per_sf` varchar(200) DEFAULT NULL,
  `aai_avg_sf` varchar(200) DEFAULT NULL,
  `aai_list_price` varchar(200) DEFAULT NULL,
  `aai_building_size` text,
  `aai_lot_size` text,
  `aai_parking_ratio` text,
  `aai_rentable_sf` text,
  `aai_usable_sf` text,
  `aai_amenities_inside` text,
  `aai_amenities_outside` text,
  `aai_proximity` text,
  `aai_interstate_access` text,
  `aai_location_rating` text,
  `aai_hca_id` int(11) NOT NULL,
  `aai_floors` text,
  `aai_sprinkler` text,
  `aai_expansion` text,
  `aai_unit_condition` text,
  `aai_ground_condition` text,
  `aai_ownership` text,
  `aai_owner` text,
  `aai_management` text,
  `aai_attorney` text,
  `aai_accountant` text,
  `aai_investor` text,
  `aai_occupancy` text,
  `aai_lease_type` text,
  `aai_rental_rate` text,
  `aai_expenses` text,
  `aai_cash_flow` text,
  `aai_lender` text,
  `aai_debt_service` text,
  `aai_total_debt` text,
  `aai_prepayment` text,
  `aai_cap` text,
  `aai_tenant_status` text,
  `aai_easement` text,
  `aai_environmental` text,
  `aai_neighborhood` text,
  `aai_competition` text,
  `aai_demographics` text,
  `aai_crime` text,
  `aai_access` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agent_asset_info`
--
ALTER TABLE `agent_asset_info`
  ADD PRIMARY KEY (`aai_id`),
  ADD UNIQUE KEY `aai_hca_id` (`aai_hca_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agent_asset_info`
--
ALTER TABLE `agent_asset_info`
  MODIFY `aai_id` int(11) NOT NULL AUTO_INCREMENT;


  ========================================

  ALTER TABLE `cpd` ADD `cpd_deal_pros_name` VARCHAR(50) NOT NULL DEFAULT 'Offers Received & Accepted' AFTER `cpd_b_active_name`;
UPDATE `code` SET `code_name` = 'Prospective' WHERE `code`.`code_id` = 28;
UPDATE `code` SET `code_abbrev` = 'deal_pros' WHERE `code`.`code_id` = 28;
UPDATE `code` SET `code_description` = 'Deal Prospective' WHERE `code`.`code_id` = 28;

UPDATE `code` SET `code_name` = 'Offer Received' WHERE `code`.`code_id` = 28;
UPDATE `code` SET `code_id` = '123' WHERE `code`.`code_id` = 28;
INSERT INTO `code` (`code_id`, `code_type_id`, `code_abbrev`, `code_name`, `code_description`) VALUES (NULL, '11', 'deal_pros_a', 'Offer Accepted', 'Offer Accepted');


====================================
ALTER TABLE `homebase_client_asset` ADD `hca_usable_sf` DECIMAL(10) NOT NULL DEFAULT '1' AFTER `hca_type`, ADD `hca_gross_sf` DECIMAL(10) NOT NULL DEFAULT '1' AFTER `hca_usable_sf`;


ALTER TABLE `homebase_client_asset` ADD `hca_livable_sf_price` DECIMAL(10) NOT NULL DEFAULT '0' AFTER `hca_gross_sf`, ADD `hca_gross_sf_price` DECIMAL(10) NOT NULL DEFAULT '0' AFTER `hca_livable_sf_price`;

================================================================================================

--
-- Table structure for table `agent_deal_identified`
--

CREATE TABLE `agent_deal_identified` (
  `adi_id` int(11) NOT NULL,
  `adi_user_id` bigint(25) UNSIGNED NOT NULL,
  `adi_asset_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agent_deal_identified`
--
ALTER TABLE `agent_deal_identified`
  ADD PRIMARY KEY (`adi_id`),
  ADD KEY `adi_user_id` (`adi_user_id`),
  ADD KEY `adi_asset_id` (`adi_asset_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agent_deal_identified`
--
ALTER TABLE `agent_deal_identified`
  MODIFY `adi_id` int(11) NOT NULL AUTO_INCREMENT;


  CREATE TABLE `agent_comps` (
    `ac_id` int(11) NOT NULL,
    `ac_asset_id` int(11) NOT NULL,
    `ac_address` text,
    `ac_built` varchar(20) DEFAULT NULL,
    `ac_units` varchar(20) DEFAULT NULL,
    `ac_price_per_unit` varchar(20) DEFAULT NULL,
    `ac_price_per_sf` varchar(20) DEFAULT NULL,
    `ac_avg_sf` varchar(20) DEFAULT NULL,
    `ac_price` varchar(20) DEFAULT NULL,
    `ac_avatar` text
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  --
  -- Indexes for dumped tables
  --

  --
  -- Indexes for table `agent_comps`
  --
  ALTER TABLE `agent_comps`
    ADD PRIMARY KEY (`ac_id`),
    ADD KEY `ac_asset_id` (`ac_asset_id`);

  --
  -- AUTO_INCREMENT for dumped tables
  --

  --
  -- AUTO_INCREMENT for table `agent_comps`
  --
  ALTER TABLE `agent_comps`
    MODIFY `ac_id` int(11) NOT NULL AUTO_INCREMENT;

======================================================================
--
-- Table structure for table `homebase_asset_revenue`
--

CREATE TABLE `homebase_asset_revenue` (
  `har_id` int(11) NOT NULL,
  `har_asset_id` int(11) NOT NULL,
  `har_type` varchar(200) NOT NULL,
  `har_sq_ft` int(11) DEFAULT NULL,
  `har_total_sq_ft` int(11) DEFAULT NULL,
  `har_rent_per_mo` decimal(11,0) DEFAULT NULL,
  `har_total_rent_per_mo` decimal(10,0) DEFAULT NULL,
  `har_number` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `homebase_asset_revenue`
--
ALTER TABLE `homebase_asset_revenue`
  ADD PRIMARY KEY (`har_id`),
  ADD UNIQUE KEY `har_asset_id` (`har_asset_id`,`har_type`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `homebase_asset_revenue`
--
ALTER TABLE `homebase_asset_revenue`
  MODIFY `har_id` int(11) NOT NULL AUTO_INCREMENT;


  =============================================================

  ALTER TABLE `homebase_buyer_asset` ADD `hba_price` DECIMAL(10) NOT NULL DEFAULT '0' AFTER `hba_status`, ADD `hba_rate` INT(3) NOT NULL DEFAULT '0' AFTER `hba_price`, ADD `hba_gross` DECIMAL(10) NOT NULL DEFAULT '0' AFTER `hba_rate`, ADD `hba_split` INT(3) NOT NULL DEFAULT '0' AFTER `hba_gross`, ADD `hba_gsplit` INT(3) NOT NULL DEFAULT '0' AFTER `hba_split`;

  ALTER TABLE `homebase_buyer_asset` ADD `hba_gnet` DECIMAL(10) NOT NULL DEFAULT '0' AFTER `hba_gsplit`, ADD `hba_net` DECIMAL(10) NOT NULL DEFAULT '0' AFTER `hba_gnet`;

  ===================================================================
  
  --
  -- Table structure for table `buyer_preferences`
  --

  CREATE TABLE `buyer_preferences` (
    `bp_id` int(11) NOT NULL,
    `bp_buyer_id` int(11) NOT NULL,
    `bp_product` varchar(300) DEFAULT NULL,
    `bp_market` text,
    `bp_unit` varchar(200) DEFAULT NULL,
    `bp_sq_ft` varchar(200) DEFAULT NULL,
    `bp_cond` text,
    `bp_criteria` text,
    `bp_price` varchar(200) DEFAULT NULL,
    `bp_cap` varchar(300) DEFAULT NULL,
    `bp_financing` text,
    `bp_contingency` text,
    `bp_term` text,
    `bp_strategy` text,
    `bp_initial` varchar(200) DEFAULT NULL,
    `bp_counter` varchar(200) DEFAULT NULL,
    `bp_second` varchar(200) DEFAULT NULL,
    `bp_discovery` text,
    `bp_final` varchar(200) DEFAULT NULL,
    `bp_retrade` text,
    `bp_justification` text,
    `bp_extension` text,
    `bp_reason` text,
    `bp_hard` varchar(200) DEFAULT NULL,
    `bp_additional` varchar(200) DEFAULT NULL,
    `bp_accepted` varchar(200) DEFAULT NULL,
    `bp_owner` text,
    `bp_unit_renovation` text,
    `bp_external` text,
    `bp_cost` text,
    `bp_property` text,
    `bp_inspection` text,
    `bp_appraisal` text,
    `bp_environmental` text,
    `bp_title` text,
    `bp_tenant_arrangement` text,
    `bp_tenant_information` text,
    `bp_legal_procedure` text
  ) ENGINE=InnoDB DEFAULT CHARSET=latin1;

  --
  -- Indexes for dumped tables
  --

  --
  -- Indexes for table `buyer_preferences`
  --
  ALTER TABLE `buyer_preferences`
    ADD PRIMARY KEY (`bp_id`),
    ADD UNIQUE KEY `bp_buyer_id` (`bp_buyer_id`);

  --
  -- AUTO_INCREMENT for dumped tables
  --

  --
  -- AUTO_INCREMENT for table `buyer_preferences`
  --
  ALTER TABLE `buyer_preferences`
    MODIFY `bp_id` int(11) NOT NULL AUTO_INCREMENT;
