<?php

namespace Model;

class Tool
{

	/**
	 * Get the list of tool groups and tools, indicating status of each tool with relation to the user.
	 */
	public function get_tool_groups($user_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Find the list of tool groups available to the user.
		$sql = "select distinct tg_id, tg_name, tg_description, tg_is_admin"
			. " from tool_group"
			. " join user on user_is_admin=tg_is_admin or tg_is_admin=0"
			. " where user_id=:user_id";
		$groups = $PDO->select($sql, array('user_id' => $user_id));

		// Find the list of tools in each group.
		$sql = "select tool_id, tool_name, tool_description, tool_controller, tool_is_admin, ut_id, ut_eff_date, ut_term_date"
			. " from tool"
			. " join user on user_is_admin=tool_is_admin or tool_is_admin=0"
			. " left join user_tool on ut_user_id=user_id and ut_tool_id=tool_id"
			. " where tool_tg_id=:tg_id and user_id=:user_id";
		foreach ($groups as $group) {
			$group->tools = $PDO->select($sql, array('tg_id' => $group->tg_id, 'user_id' => $user_id));
		}
		return $groups;
	}

	/**
	 * Get the subscription status for the specified tool.
	 */
	public function get_subscription($tool_controller, $user_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Does the user have a subscription to the specified tool?
		$sql = "select tool_id, ut_eff_date, ut_term_date"
			. " from tool"
			. " left join user_tool on ut_tool_id=tool_id"
			. " where ut_user_id=:user_id and tool_controller=:controller";
		$subscription = $PDO->select_row($sql, array('controller' => $tool_controller, 'user_id' => $user_id));

		// If there is no subscription, create on with the "not subscribed" status.
		if (empty($subscription)) {
			$subscription = new \stdClass();
			$subscription->status = 'no';
		} // There is a subscription, check to see if it is active or not.
		else {
			if ((time() > strtotime($subscription->ut_eff_date)) &&
				($subscription->ut_term_date == null || (time() < $subscription->ut_term_date)))
				$subscription->status = 'active';
			else
				$subscription->status = 'inactive';
		}

		// Get the code name for the status.
		$sql = "select code_name"
			. " from code"
			. " join code_type on code_type_id=ct_id"
			. " where ct_abbrev='substatus' and code_abbrev=:code";
		$code = $PDO->select_row($sql, array('code' => $subscription->status));
		$subscription->status_name = $code->code_name;

		// Get the tool requirements.
		$sql = "select tool_is_admin"
			. " from tool"
			. " where tool_controller=:controller";
		$requirements = $PDO->select_row($sql, array('controller' => $tool_controller));
		if ($requirements->tool_is_admin)
			$subscription->admin_only = true;
		else
			$subscription->admin_only = false;

		// Determine if the user is eligible for the subscription.
		$sql = "select tool_id"
			. " from tool"
			. " join user on user_is_admin=tool_is_admin or tool_is_admin=0"
			. " where tool_controller=:controller and user_id=:user_id";
		$eligible = $PDO->select_row($sql, array('controller' => $tool_controller, 'user_id' => $user_id));
		if ($eligible === false)
			$subscription->eligible = false;
		else
			$subscription->eligible = true;

		return $subscription;
	}

	/**
	 * Toggle the subscription status for the specified tool.
	 */
	public function toggle_subscription($tool_controller, $user_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Get the current subscription status.
		$subscription = $this->get_subscription($tool_controller, $user_id);

		// If the user is not eligible, do nothing.
		if (!$subscription->eligible)
			return false;

		// If the subscription is not started, start it.
		if ($subscription->status == 'no') {
			$sql = "insert into user_tool (ut_user_id, ut_tool_id, ut_eff_date, ut_term_date)"
				. " values (:user_id, (select tool_id from tool where tool_controller=:tool_controller), now(), null)";
			$PDO->insert($sql, array('user_id' => $user_id, 'tool_controller' => $tool_controller));
			return true;
		}

		// If the subscription is active, terminate it.
		if ($subscription->status == 'active') {
			$sql = "update user_tool set ut_term_date=now()"
				. " where ut_user_id=:user_id and ut_tool_id=:tool_id";
			$PDO->insert($sql, array('user_id' => $user_id, 'tool_id' => $subscription->tool_id));
			return true;
		}

		// If the subscription is inactive, reactive it.
		if ($subscription->status == 'inactive') {
			$sql = "update user_tool set ut_term_date=null"
				. " where ut_user_id=:user_id and ut_tool_id=:tool_id";
			$PDO->insert($sql, array('user_id' => $user_id, 'tool_id' => $subscription->tool_id));
			return true;
		}
	}

	/**
	 * Get the specified group.
	 */
	public function get_group($id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Get the tool group
		$sql = "select tg_id, tg_name, tg_description, tg_is_admin"
			. " from tool_group"
			. " where tg_id=:id";
		$group = $PDO->select_row($sql, array('id' => $id));

		// Get the list of tools in the tool group.
		$sql = "select tool_id, tool_name, tool_description, tool_controller, tool_is_admin, tool_status"
			. " from tool"
			. " where tool_tg_id=:id";
		$group->tools = $PDO->select($sql, array('id' => $id));
		return $group;
	}

	/**
	 * Get the list of tools that meet the selection criteria.
	 */
	public function get_tools($input)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Is there a group id?
		$group_id = $input->get('group_id');

		// Get the parameters.
		$draw = $input->get('draw');
		$start = $input->get('start');
		$length = $input->get('length');

		// Is there a search.
		$search = $input->get('search', array(), 'ARRAY');
		if (!empty($search['value']))
			$search_val = $search['value'];
		//echo 'Search Val: ' . $search_val;

		// Limit search to a specific group?
		if (isset($group_id) && !empty($group_id)) {
			$sql_where = ' where tool_tg_id=' . $group_id . ' ';
			$sep_where = ' and ';
		} else {
			$sql_where = '';
			$sep_where = ' where ';
		}

		// Get the columns and the search
		$sql_columns_cte = '';
		$sql_columns = '';
		$sep = ' ';
		$columns = $input->get('columns', array(), 'ARRAY');
		foreach ($columns as $column) {
			$sql_columns .= $sep . $column['data'];
			$search_col = $column['data'];
			$sep = ', ';

			// Add to the search?
			if (isset($search_val) && $column['searchable']) {
				$sql_where .= $sep_where . "cast(" . $search_col . " as char) like '%" . $search_val . "%'";
				$sep_where = ' or ';
			}
		}

		// Determine the order information.
		$order = $input->get('order', array(), 'ARRAY');
		$sql_order = '';
		foreach ($order as $o) {
			if (strlen($sql_order) > 0)
				$sql_order .= ', ';

			// Get the column
			$col = $columns[$o['column']]['data'];
			$sql_order .= $columns[$o['column']]['data'] . ' ' . $o['dir'];
		}

		// Get the total number of rows (unfiltered).
		$sql = "select count(*) from tool";
		$count_total = $PDO->select_count($sql, array());

		// Get the total number of rows (unfiltered).
		$sql = "select count(*) from tool " . $sql_where;
		$count_filtered = $PDO->select_count($sql, array());

		// Get the filtered data.
		$sql = " select " . $sql_columns
			. " from tool"
			. " join tool_group on tool_tg_id=tg_id"
			. " join code on tool_status=code_abbrev"
			. " join code_type on code_type_id=ct_id and ct_abbrev='toolstatus'"
			. $sql_where
			. " order by " . $sql_order
			. " limit " . $start . ", " . $length;
		//echo $sql;
		$data = $PDO->select($sql, array());

		// Set the return values.
		$ret['draw'] = $draw;
		$ret['data'] = $data;
		$ret['recordsTotal'] = $count_total;
		$ret['recordsFiltered'] = $count_filtered;
		return $ret;
	}

	/**
	 * Get the list of tools useable by the specified user ID.
	 */
	public function get_user_tools($user_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');
		$PDO_wp = \Model\PDO_model::instance('WP');

		// Get the list of tool groups the user has.
		$sql = "select memberships from wp_mepr_members where user_id=:user_id";
		$memberships = $PDO_wp->select_row($sql, array('user_id' => $user_id));

		// Get the names of the tool groups.
		$sql = "select post_name from wp_posts where ID in (" . $memberships->memberships . ")";
		$tool_groups = $PDO_wp->select($sql, array());

		$sep = '';
		$membership_list = '';
		foreach ($tool_groups as $tg) {
			$membership_list .= $sep . "'" . $tg->post_name . "'";
			$sep = ', ';
		}

		// Find the list of tool groups in which the user has at least one tool.
		$sql = "select tg_id, tg_name"
			. " from tool_group"
			. " where tg_wp_name in (" . $membership_list . ")"
			. " order by tg_display_order";
		$groups = $PDO->select($sql, array());

		// Find the list of tools in each group.
		$sql = "select tool_id, tool_name, tool_controller"
			. " from tool"
			. " where tool_tg_id=:tg_id"
			. " order by tool_name";

		// Create the list of individual tools.
		$tools = array();
		foreach ($groups as $group) {
			$group_tools = $PDO->select($sql, array('tg_id' => $group->tg_id));
			foreach ($group_tools as $tool) {
				if (!in_array($tool->tool_controller, $tools))
					$tools[] = $tool;
			}
		}

		// Are there any separate tool subscriptions?
		$sql = "select tool_id, tool_name, tool_controller"
			. " from tool"
			. " where tool_wp_name in (" . $membership_list . ")"
			. " order by tool_name";
		$individual_tools = $PDO->select($sql, array());
		foreach ($individual_tools as $tool) {
			if (!in_array($tool->tool_controller, $tools))
				$tools[] = $tool;
		}
		return $tools;
	}

}