<?php
/**
 * Deal Management Dashboard Model.
 *
 * Deal Management Dashboard model for performing CRUD operations to database..
 *
 * @author Sumit K (sumitk@mindfiresolutions.com)
 *
 */

namespace Model;

use function count;
use DateTime;

/**
 * Deal Management Dashboard model class.
 *
 * @author   Sumit K (sumitk@mindfiresolutions.com)
 * @category Model
 */
class CPDV1
{

    private $PDO;
    private $common_model;

    /**
     * Initialises the PDO and model instance.
     */
    public function __construct()
    {
        $this->PDO = \Model\PDO_model::instance('Tools');
        $this->common_model = new Common;
    }

    /**
     * Perform a server side DataTables search for the CPD
     *
     * @param $user_id      Int Id of the user
     * @param $input        Mixed input from the ajax
     *
     * @return object
     */
    public function search_cpd_instances($user_id, $input)
    {
        // Get the parameters.
        $start = $input->getInt('start');
        $length = $input->getInt('length');

        // Is there a search?
        $search = $_POST['search'];

        if (!empty($search['value'])) {
            $search_val = $search['value'];
        }

        // Get the column information
        $columns = $_POST['columns'];

        // Get the columns and the search
        $sql_columns = '';
        $sep = ' ';
        $sql_where = ' where cpd_user_id=' . $user_id;
        $sep_where = ' and ';

        // Loop through each CPD column that will be used for searching and append to where clause.
        foreach ($columns as $column) {

            if (empty($column['data'])) {
                continue;
            }
            $search_col = $column['data'];
            $sql_columns .= $sep . $column['data'];
            $sep = ', ';

            // Add to the search?
            if (isset($search_val) && ('true' == $column['searchable'])) {
                $sql_where .= $sep_where . "(cast(" . $search_col . " as char(100)) like '%" . $search_val . "%')";
                $sep_where = ' or ';
            }
        }

        // Determine the order information.
        $order = $_POST['order'];

        $sql_order = '';

        // Loop through columns for order by clause.
        foreach ($order as $o) {

            if (strlen($sql_order) > 0) {
                $sql_order .= ', ';
            } else {
                $sql_order = " order by ";
            }
            // Get the column
            $columns[$o['column']]['data'];
            $sql_order .= $columns[$o['column']]['data'] . ' ' . $o['dir'];
        }

        // Get the total number of rows (unfiltered).
        $sql = "select count(*) from cpd"
            . " where cpd_user_id=:user_id";
        $count_total = $this->PDO->select_count($sql, array('user_id' => $user_id));

        // Get the total number of rows (filtered).
        $sql = "select count(*)"
            . " from cpd"
            . $sql_where;
        $count_filtered = $this->PDO->select_count($sql, array('user_id' => $user_id));

        // Selects active tools instance.
        $sql = " select " . $sql_columns . ", tgd_ui_id, uact_cpd_id, cpd_product_id from cpd"
            . " left join tgd_user_instance on cpd_id=tgd_cpd_id"
            . " left join industry_profession on cpd_profession_id=profession_id"
            . " left join industry on profession_industry_id=industry_id"
            . " left join user_active_cpd_tgd on uact_user_id=cpd_user_id"
            . $sql_where
            . $sql_order
            . " limit " . $start . ',' . $length;

        $data = $this->PDO->select($sql, array('user_id' => $user_id));

        // Set the return values.
        $ret['data'] = $data;
        $ret['recordsTotal'] = $count_total;
        $ret['recordsFiltered'] = $count_filtered;
        return $ret;
    }


    /**
     * Get the list of instances for the current user.
     *
     * @param $cpd_list Array CPD parameters.
     * @return bool
     */
    public function get_instances($cpd_list)
    {

        $sql_where = 'cpd_user_id=' . $cpd_list['user_id'];

        if (isset($cpd_list['year']) && !empty($cpd_list['year'])) {
            $sql_where .= ' and cpd_year=' . $cpd_list['year'];
        }

        if (isset($cpd_list['product_id']) && !empty($cpd_list['product_id'])) {
            $sql_where .= ' and cpd_product_id=' . $cpd_list['product_id'];
        }

        // Find the list of tool groups available to the user.
        $sql = "select cpd_id, cpd_name, cpd_year, cpd_created, cpd_updated"
            . " from cpd where " . $sql_where;

        return $this->PDO->select($sql);
    }

    /**
     * Get the specified CPD instance. We include the user ID in the search to ensure that only the owner can see
     * the TGD instance.
     *
     * @param $user_id Int    Id of user
     * @param $cpd_id  Int    Cpd id to fetch detail
     * @param $quarter Int    Quarter for which detail is required
     * @param $key     String Key for search.
     * @param $val     String Value for search.
     * @param $is_sort String Is Sorted.
     * @param $year    Int    Year for CPD.
     * @return bool $cpd mixed details od CPD with deals, and related TGD details
     *
     */
    public function get_cpd($user_id, $cpd_id, $quarter, $key, $val, $is_sort, $year)
    {
        // This is a multi step process, so use a transaction.
        try {
            $this->PDO->begin_transaction();
            $query = '';
            $order_by = '';

            if (!empty($key) && !empty($val) && empty($is_sort)) {
                $query = ' and ' . $key . '=\'' . $val . '\'';
            } else if (!empty($is_sort)) {
                $order_by = " order by field($key, '$val') desc";
            }

            // Column names to be selected.
            $columns = "cpd_id,cpd_user_id,cpd_profession_id,cpd_year,cpd_name,cpd_description, cpd_diligence_name,"
                . " cpd_complete_name,cpd_complete_desc,cpd_meetsch_name,cpd_meetsch_desc, cpd_b_active_name,"
                . " cpd_b_contract_name,cpd_b_contract_desc,cpd_deposit_name,cpd_deposit_desc,"
                . " cpd_listed_name,cpd_listed_desc,cpd_offer_name,cpd_offer_desc,cpd_requested_name,"
                . " cpd_requested_desc,cpd_presented_name,cpd_presented_desc,cpd_meetcom_name,"
                . " cpd_meetcom_desc,cpd_tour_name,cpd_tour_desc,cpd_deal_pros_name,cpd_maybe_desc,"
                . " cpd_no_name,cpd_no_desc,cpd_expl_ph_name,cpd_expl_ph_desc,cpd_expl_per_name,"
                . " cpd_expl_per_desc,cpd_pres_per_name,cpd_pres_per_desc,cpd_pres_ph_name,"
                . " cpd_pres_ph_desc,cpd_inv_per_name,cpd_inv_per_desc,cpd_inv_ph_name,cpd_inv_ph_desc,"
                . " cpd_created,cpd_updated,tgd_ui_name,tgd_ui_id,"
                . " cpd_prospective_name, cpd_possible_name, cpd_probable_name, cpd_predictable_name,"
                . " cpd_potential_name, cpd_present_name, cpd_delivered_name, cpd_invitation_name, cpd_ref_rece_name,"
                . " cpd_accepted_name, cpd_submitted_name, cpd_proposed_name, cpd_ref_compl_name, cpd_ref_acce_name";

            $sql = "select " . $columns
                . " from cpd"
                . " left join user_active_cpd_tgd"
                . " on uact_user_id=cpd_user_id "
                . " left join tgd_user_instance on tgd_ui_id=uact_tgd_id"
                . " and tgd_ui_user_id=uact_user_id"
                . " where cpd_user_id=:user_id and cpd_id=:cpd_id";

            $cpd = $this->PDO->select_row($sql, array('user_id' => $user_id, 'cpd_id' => $cpd_id));

            if (empty($cpd)) {
                throw new \UnexpectedValueException("Could not get the requested Deal Management Dashboard instance");
            }

            // Get the deals information for CPD.
            $sql = "select distinct"
                . " assetstatus.has_asset_id as deal_id,hc_id, hc_name as deal_name,"
                . " hca_portfolio_id, hca_clu as deal_clu, hca_rwt as deal_rwt, up_name,"
                . " GROUP_CONCAT(cc_name, '|' , cc_id) as cc_name, hc_main_phone as deal_phone, hca_gross_sf,"
                . " hc_second_phone, hc_mobile_phone, hca_lead as deal_lead, hca_name as deal_property,"
                . " hca_gen as deal_gen, hca_id, hca_product_id, hca_lng as deal_property_long, hca_usable_sf,"
                . " hca_lat as deal_property_lat, hc_client_email as deal_email, hca_price as deal_price,"
                . " hca_rate as deal_rate, hca_gross as deal_gross, ta_when as event_date,  hca_gsplit as deal_gsplit,"
                . " hca_gnet as deal_gnet, hca_split as deal_split, hca_net as deal_net, hc_selected_phone, "
                . " assetstatus.has_status as ds_status, assetstatus.has_md as ds_md,"
                . " assetstatus.has_datetime, ct_abbrev,"
                . " (select count(*) from deal_update where du_deal_id=deal_id) as deal_update_count, upg_name,"
                . " (select count(*) from deal_promo where dp_deal_id=deal_id) as deal_promo_count, u_product_name"
                . " from deal"
                . " left join homebase_client_asset"
                . " on hca_id=deal_asset_id"
                . " left join homebase_client on hca_client_id=hc_id"
                . " join (select has_status, hs1.has_datetime, has_id, has_asset_id, has_md"
                . " from homebase_asset_status hs1"
                . " join (select max(has_datetime) as time, has_asset_id as asset_id"
                . " from homebase_asset_status"
                . " where ((quarter(has_datetime)<=:quarter && year(has_datetime)=:year) || (year(has_datetime)<:year))"
                . " group by has_asset_id) as hs2"
                . " on hs1.has_datetime = hs2.time and hs1.has_asset_id = asset_id)"
                . " as assetstatus on hca_id=assetstatus.has_asset_id"
                . " join code on code_abbrev=assetstatus.has_status"
                . " join code_type on code_type_id=ct_id and"
                . " (ct_abbrev='gds' OR ct_abbrev='cds' OR ct_abbrev='str_ds' OR ct_abbrev='bds' OR ct_abbrev='rds'"
                . " OR ct_abbrev='bds')"
                . " left join user_portfolio"
                . " on up_id=hca_portfolio_id"
                . " left join tsl_actions"
                . " on ta_client_id=hca_id and ta_when = ("
                . " select min(ta_when) from tsl_actions"
                . " where ta_client_id=hca_id and ta_when > now())"
                . " left join user_product"
                . " on u_product_id=hca_product_id"
                . " left join user_product_group"
                . " on upg_id=hca_product_id"
                . " left join cpd_deal_info"
                . " on cc_deal_id=hca_id and cc_type='Buyer'"
                . " where deal_cpd_id=:cpd_id and ((quarter(assetstatus.has_datetime)<=:quarter"
                . " && year(has_datetime)=:year) || (year(has_datetime)<:year))" . $query
                . " group by deal_id"
                . $order_by;

            $cpd->deals = $this->PDO->select($sql, array('cpd_id' => $cpd_id,
                'quarter' => $quarter, 'quarter1' => $quarter, 'year' => $year));

            // Get the Deal Lead.
            $sql = 'select dl_name, dl_type, dl_id'
                . ' from deal_lead'
                . ' where dl_user_id=:user_id';

            $cpd->leads = $this->PDO->select($sql, array('user_id' => $user_id));

            // Get the assignee.
            $sql = 'select tsl_assignee_id, tsl_assignee_name'
                . ' from tsl_assignee'
                . ' where tsl_assignee_user_id=:user_id';

            $cpd->assignee = $this->PDO->select($sql, array('user_id' => $user_id));

            // Get User Portfolio.
            $sql = 'select up_id, up_name'
                . ' from user_portfolio'
                . ' where up_user_id=:user_id';

            $cpd->portfolio = $this->PDO->select($sql, array('user_id' => $user_id));
            // Commit the transation and return the CPD
            $this->PDO->commit_transaction();

        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return $e;
        }
        return $cpd;
    }

    public function get_cpd_buyers($id, $quarter, $year)
    {
        $sql = 'select distinct assetstatus.has_asset_id as deal_id, hb_id, null as hca_portfolio_id,'
            . ' hb_name , GROUP_CONCAT(cc_name, \'|\' , cc_id) as cc_name,'
            . ' hb_main_phone, hb_second_phone, hb_mobile_phone, hba_entity_address,'
            . ' hba_entity_name , hba_id, hba_product_type, hba_city, hba_state, hba_zip, hb_rwt, '
            . ' hb_email , hb_selected_phone, assetstatus.has_status as ds_status, hba_buyer_type,'
            . ' assetstatus.has_md as ds_md, assetstatus.has_datetime, ct_abbrev,'
            . ' hba_price, hba_rate, hba_gross, hba_split, hba_gsplit, hba_gnet, hba_net,'
            . ' (select count(*) from deal_update where du_deal_id=deal_id) as deal_update_count,'
            . ' upg_name, (select count(*) from deal_promo where dp_deal_id=deal_id) as deal_promo_count,'
            . ' u_product_name from deal left join homebase_buyer_asset on hba_id=deal_asset_id'
            . ' left join homebase_buyer on hba_hb_id=hb_id'
            . ' left join (select has_status, hs1.has_datetime, has_id, has_asset_id, has_md'
            . ' from homebase_asset_status hs1 join (select max(has_datetime) as time, has_asset_id as asset_id'
            . ' from homebase_asset_status where ((quarter(has_datetime)<=:quarter && year(has_datetime)=:year)'
            . ' || (year(has_datetime)<:year))'
            . ' group by has_asset_id) as hs2 on hs1.has_datetime = hs2.time and hs1.has_asset_id = asset_id)'
            . ' as assetstatus on hba_id=assetstatus.has_asset_id'
            . ' join code on code_abbrev=assetstatus.has_status'
            . ' join code_type on code_type_id=ct_id and (ct_abbrev=\'bds\')'
            . ' left join tsl_actions on ta_client_id=hba_id and ta_when = ('
            . ' select min(ta_when) from tsl_actions where ta_client_id=hba_id and ta_when > now())'
            . ' left join user_product on u_product_id=hba_product_type'
            . ' left join user_product_group on upg_id=hba_product_type'
            . ' left join cpd_deal_info on cc_deal_id=hba_id and cc_type=\'Buyer\''
            . ' where deal_cpd_id=:cpd_id and ((quarter(assetstatus.has_datetime)<=:quarter'
            . ' && year(has_datetime)=:year) || (year(has_datetime)<:year)) group by deal_id';

        return $this->PDO->select($sql, array('cpd_id' => $id, 'year' => $year, 'quarter' => $quarter));
    }

    /**
     * Add a new CPD
     *
     * @param int $user_id current user id
     * @param mixed $cpd details of CPD
     * @return bool $cpd_ui_id int id of the new CPD
     */
    public function add_cpd($user_id, $cpd)
    {
        // This is a multi-step process, so make sure they all work.
        try {
            $this->PDO->begin_transaction();

            // Create the instance record.
            $sql = "insert into"
                . " cpd (cpd_user_id, cpd_year, cpd_profession_id, cpd_name, cpd_description,"
                . " cpd_complete_desc, cpd_meetsch_desc, cpd_b_contract_desc, cpd_deposit_desc,"
                . " cpd_listed_desc, cpd_offer_desc, cpd_requested_desc, cpd_presented_desc,"
                . " cpd_meetcom_desc, cpd_tour_desc, cpd_maybe_desc, cpd_no_desc, cpd_expl_ph_desc,"
                . " cpd_expl_per_desc, cpd_pres_per_desc, cpd_pres_ph_desc, cpd_inv_per_desc,"
                . " cpd_inv_ph_desc, cpd_created, cpd_updated, cpd_product_id)"
                . " values (:user_id, :year, :profession_id, :name, :description,"
                . " 'Describe Title', 'Describe Title',"
                . " 'Describe Title', 'Describe Title',"
                . " 'Describe Title', 'Describe Title',"
                . " 'Describe Title', 'Describe Title',"
                . " 'Describe Title', 'Describe Title',"
                . " 'Describe Title', 'Describe Title',"
                . " 'Describe Title', 'Describe Title', 'Describe Title',"
                . " 'Describe Title', 'Describe Title', 'Describe Title', now(), now(), :product_id)";

            $params = array('user_id' => $user_id, 'year' => $cpd['cpd_year'], 'profession_id' => 0,
                'name' => $cpd['name'], 'description' => $cpd['description'], 'product_id' => $cpd['product_id']);
            $cpd_ui_id = $this->PDO->insert($sql, $params);

            // Update TGD table.
            $sql = "update tgd_user_instance "
                . " set tgd_cpd_id=:cpd_id"
                . " where tgd_ui_id=:cpd_tgd_id";
            $this->PDO->update($sql, array('cpd_id' => $cpd_ui_id, 'cpd_tgd_id' => $cpd['cpd_tgd_id']));

            // Commit the transation.
            $this->PDO->commit_transaction();

        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }

        return $cpd_ui_id;
    }

    /**
     * Delete a cpd instance
     *
     * @param  int $cpd_id id of CPD to delete
     * @return bool $result
     */
    public function delete_cpd($cpd_id)
    {
        try {
            $this->PDO->begin_transaction();

            // Delete the instance.
            $delete_sql = "delete"
                . " from cpd where cpd_id=:cpd_id";
            $this->PDO->delete($delete_sql, array('cpd_id' => $cpd_id));

            // Update TGD table.
            $sql = "update "
                . " tgd_user_instance"
                . " set tgd_cpd_id=NULL"
                . " where tgd_cpd_id=:cpd_id";
            $update = $this->PDO->update($sql, array('cpd_id' => $cpd_id));

            // Commit the transation.
            $this->PDO->commit_transaction();

        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }

        return $update;
    }

    /**
     * Copy an existing instance to a new instance
     *
     * @param int $old_ui_id original CPD id
     * @param string $save_as_name name of new CPD
     * @param int $save_as_id id of CPD to be saved as
     * @return int   $new_ui_id    id of the new CPD
     */
    public function copy_cpd($old_ui_id, $save_as_name, $save_as_id)
    {
        // This is a multi-step process, so make sure they all work.
        try {
            $this->PDO->begin_transaction();

            // If the ID is set and the name is not set, replace the existing instance.  The easiest way to do this is
            // to get the name of the existing instance, delete it and the replace it.
            if (empty($save_as_name) && !empty($save_as_id)) {
                $sql = "select cpd_ui_name, cpd_ui_created from cpd_user_instance where cpd_ui_id=:save_as_id";
                $ui = $this->PDO->select_row($sql, array('save_as_id' => $save_as_id));
                $save_as_name = $ui->cpd_ui_name;
                $created_date = $ui->cpd_ui_created;
                $sql = "delete from cpd_user_instance where cpd_ui_id=:save_as_id";
                $this->PDO->delete($sql, array('save_as_id' => $save_as_id));
            } else {
                $created_date = date('Y-m-d H:i:s');
            }

            // Create the new instance record as a copy.
            $sql = "insert into"
                . " cpd_user_instance (cpd_ui_user_id, cpd_ui_gt_id, cpd_ui_name,"
                . " cpd_ui_description, cpd_ui_year, cpd_ui_floor, cpd_ui_target, cpd_ui_game,"
                . " cpd_ui_default_check_size, cpd_ui_periods_final, cpd_ui_py_cum_floor, "
                . " cpd_ui_py_cum_target, cpd_ui_py_cum_game, cpd_ui_created, cpd_ui_updated)"
                . " select cpd_ui_user_id, cpd_ui_gt_id, :name, cpd_ui_description, cpd_ui_year,"
                . " cpd_ui_floor, cpd_ui_target, cpd_ui_game, cpd_ui_default_check_size, cpd_ui_periods_final,"
                . " cpd_ui_py_cum_floor, cpd_ui_py_cum_target, cpd_ui_py_cum_game, :created, now()"
                . " from cpd_user_instance where cpd_ui_id=:ui_id";
            $new_ui_id = $this->PDO->insert($sql, array('name' => $save_as_name, 'created' => $created_date,
                'ui_id' => $old_ui_id));

            // Create the new instance's conditions
            $sql = "insert into"
                . " cpd_instance_condition (cpd_icond_ui_id, cpd_icond_c_id, cpd_icond_conversion_rate)"
                . " select :new_ui_id, cpd_icond_c_id, cpd_icond_conversion_rate"
                . " from cpd_instance_condition where cpd_icond_ui_id=:ui_id";
            $this->PDO->insert($sql, array('new_ui_id' => $new_ui_id, 'ui_id' => $old_ui_id));

            // Create the new instance's multipliers
            $sql = "insert into"
                . " cpd_instance_multiplier (cpd_im_ui_id, cpd_im_m_id, cpd_im_period, cpd_im_multiple)"
                . " select :new_ui_id, cpd_im_m_id, cpd_im_period, cpd_im_multiple"
                . " from cpd_instance_multiplier where cpd_im_ui_id=:ui_id";
            $this->PDO->insert($sql, array('new_ui_id' => $new_ui_id, 'ui_id' => $old_ui_id));

            // Create the instance cells.
            $sql = "insert into"
                . " cpd_instance_cell (cpd_ic_ui_id, cpd_ic_c_id, cpd_ic_period, cpd_ic_floor,"
                . " cpd_ic_target, cpd_ic_game, cpd_ic_actual)"
                . " select :new_ui_id, cpd_ic_c_id, cpd_ic_period, cpd_ic_floor, cpd_ic_target, cpd_ic_game,"
                . " cpd_ic_actual"
                . " from cpd_instance_cell where cpd_ic_ui_id=:ui_id";
            $this->PDO->insert($sql, array('new_ui_id' => $new_ui_id, 'ui_id' => $old_ui_id));

            // Commit the transation and return the CPD
            $this->PDO->commit_transaction();

        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }

        return $new_ui_id;
    }

    /**
     * Add a new instance
     *
     * @param  mixed $deal details of new deal to be added
     * @return int   $deal_id  id of the new deal created
     */
    public function add_deal($deal)
    {
        // This is a multi-step process, so make sure they all work.
        try {
            $this->PDO->begin_transaction();

            // Create the instance record.
            $sql = "insert into"
                . " deal (deal_user_id, deal_cpd_id, deal_name, deal_short_name, deal_property, deal_phone,"
                . " deal_product, deal_created, deal_updated, deal_property_long, deal_property_lat, deal_email)"
                . " values (:user_id, :cpd_id, :name,"
                . " :short_name, :property, :phone, :product, now(), now(), :long, :lat, :email)";

            // Parameters array.
            $params = array('user_id' => $deal['user_id'], 'cpd_id' => $deal['cpd_id'], 'name' => $deal['name'],
                'short_name' => substr($deal['name'], 0, 10), 'property' => $deal['property'],
                'phone' => $deal['phone'], 'product' => $deal['property'], 'long' => $deal['long'],
                'lat' => $deal['lat'], 'email' => $deal['email']);

            $deal_id = $this->PDO->insert($sql, $params);

            // Update the status for deal.
            $status_sql = "insert into"
                . " deal_status (ds_deal_id, ds_status, ds_md, ds_datetime)"
                . " values (:deal_id, :status, month(now()), now())";

            $deal_status_id = $this->PDO->insert($status_sql,
                array("deal_id" => $deal_id, "status" => $deal['status']));

            // Commit the transation and return the CPD
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }
        return $deal_status_id;
    }

    /**
     * Change the status of a deal and move it to different frontend table
     *
     * @param array $deal details of status and deal to change status of deal
     * @return bool
     */
    public function move_deal($deal)
    {

        $sql = 'select hca_portfolio_id'
            . ' from homebase_client_asset'
            . ' where hca_id=:hca_id';

        $portfolio_id = $this->PDO->select($sql, array('hca_id' => $deal['id']));

        // Check if the deal is of type portfolio.
        if (isset($portfolio_id[0]->hca_portfolio_id)) {
            $sql = 'select hca_id'
                . ' from homebase_client_asset'
                . ' where hca_portfolio_id=:id';

            $assets_id = $this->PDO->select($sql, array('id' => $portfolio_id[0]->hca_portfolio_id));
            $values = '';

            // Create string for assets ID.
            foreach ($assets_id as $asset) {
                $values .= '(' . $asset->hca_id . ', ' . ':asset_status, :move_date, now()),';
            }

            $values = rtrim($values, ',');

            $sql = 'insert into homebase_asset_status'
                . ' (has_asset_id, has_status, has_md, has_datetime)'
                . ' values ' . $values;

            $ret = $this->PDO->insert($sql,
                array('asset_status' => $deal['new_status'], 'move_date' => $deal['new_move_date']));

        } else {

            // Create the instance record.
            $sql = "insert into homebase_asset_status (has_asset_id, has_status, has_md, has_datetime)"
                . " values (:asset_id, :asset_status, :move_date, now())";

            // Parameters array.
            $params = array("asset_id" => $deal['id'], "asset_status" => $deal['new_status'],
                "move_date" => $deal['new_move_date']);

            $ret = $this->PDO->insert($sql, $params);
        }

        return $ret;
    }

    /**
     * Delete a deal
     *
     * @param int $deal_id id of the deal to delete
     * @return bool
     */
    public function delete_deal($deal_id)
    {
        // Delete the instance.
        $sql = "delete from deal where deal_id=:deal_id";
        return $this->PDO->delete($sql, array('deal_id' => $deal_id));
    }

    /**
     * Updates the deal status table info
     *
     * @param string $name name of the column field to be updated
     * @param int $cpd_id id of the CPD
     * @param string $value value to be replaced with
     * @return bool
     */
    public function update_deal_table_info($name, $cpd_id, $value)
    {
        $sql = "update cpd"
            . " set $name=:value"
            . " where cpd_id=:cpd_id";

        $params = array('value' => $value, 'cpd_id' => $cpd_id);
        return $this->PDO->update($sql, $params);
    }

    /**
     * Updates the lead for simple deal
     *
     * @param int $deal_id id of the deal whose lead is to be updated
     * @param string $lead the value for the lead field
     * @return bool
     */
    public function update_lead($deal_id, $lead)
    {
        $sql = "update deal"
            . " set deal_lead=:lead"
            . " where deal_id=:deal_id";

        $params = array('lead' => $lead, 'deal_id' => $deal_id);
        return $this->PDO->update($sql, $params);
    }

    /**
     * Updates the gen for the simple deal
     *
     * @param int $deal_id id of the deal to be updated
     * @param string $gen the value of the gen
     * @return bool
     */
    public function update_gen($deal_id, $gen)
    {
        $sql = "update deal"
            . " set deal_gen=:gen"
            . " where deal_id=:deal_id";
        $params = array('gen' => $gen, 'deal_id' => $deal_id);
        return $this->PDO->update($sql, $params);
    }

    /**
     * Returns the floor, target and game value for the year.
     *
     * @param $cpd_id  Int CPD Id.
     * @param $quarter Int Quarter for CPD.
     * @param $year    Int Year for which we FTG is to be fetched
     * @return bool
     */
    public function get_ftg($cpd_id, $quarter, $year)
    {
        $sql = 'select uo_floor, uo_target, uo_game,'
            . ' uqo_floor, uqo_target, uqo_game'
            . ' from user_objective'
            . ' left join user_quarter_objective'
            . ' on uqo_uo_id=uo_id and uqo_quarter=:quarter'
            . ' where uo_cpd_id=:cpd_id and uo_year=:year';


        return $this->PDO->select($sql, array('cpd_id' => $cpd_id, 'year' => $year, 'quarter' => $quarter));
    }

    /**
     * Fetches Team accountability Manager instances.
     *
     * @param $user_id  Int     User Id.
     * @param $deal_id  Int     Deal Id.
     * @param $req_type String  Type of request.
     * @param $type     String  ACM Type
     * @return mixed
     */
    public function search_tam_instances($user_id, $deal_id, $req_type, $type)
    {
        $columns = "cad_id, cad_deal_id, DATE_FORMAT(cad_date, '%d/%m/%Y') as cad_date,"
            . " cad_ceo, cad_req, cad_action,"
            . " cad_whom, DATE_FORMAT(cad_when, '%d/%m %H:%i') as cad_when,"
            . " cad_acdf, cad_tag, cad_cat, cad_req_type, cad_acm_type, cad_additional_row";

        // Check if the request is for general ACM.
        if ($deal_id != 0) {
            $sql = 'select ' . $columns
                . ' from cpd_acm_deal'
                . ' join code'
                . ' on cad_req_type=code_abbrev'
                . ' and cad_acm_type=code_name'
                . ' where cad_user_id=:user_id'
                . ' and cad_deal_id=:deal_id'
                . ' and cad_req_type=:req_type'
                . ' and cad_acm_type=:type';

            $ret['data'] = $this->PDO->select($sql, array('user_id' => $user_id, 'deal_id' => $deal_id,
                'req_type' => $req_type, 'type' => $type));
        } else {
            $sql = 'select ' . $columns
                . ' from cpd_acm_deal'
                . ' where cad_user_id=:user_id'
                . ' and cad_req_type=:req_type'
                . ' and cad_acm_type=:type';

            $ret['data'] = $this->PDO->select($sql, array('user_id' => $user_id,
                'req_type' => $req_type, 'type' => $type));
        }

        // Select count of ACM for this user.
        $sql = 'select count(*)'
            . ' from cpd_acm_deal'
            . ' where cad_user_id=:user_id';

        $ret['recordsFiltered'] = $this->PDO->select_count($sql, array('user_id' => $user_id));

        // Select total count.
        $sql = 'select count(*)'
            . ' from cpd_acm_deal';

        $ret['recordsTotal'] = $this->PDO->select_count($sql, array('user_id' => $user_id));

        return $ret;
    }

    /**
     * Adds a new ACM instance.
     *
     * @param $data Array Input data
     * @return bool
     */
    public function add_acm($data)
    {
        $sql = 'insert into cpd_acm_deal'
            . ' (cad_deal_id, cad_date, cad_ceo, cad_action,'
            . ' cad_acm_type, cad_req_type, cad_user_id, cad_when, cad_whom)'
            . ' values(:deal_id, now(), :ceo, :action,'
            . ' :acm_type, :req_type, :user_id, :when, :whom)';

        return $this->PDO->insert($sql, array('deal_id' => ('acm' !== $data['deal_id'] ? $data['deal_id'] : 0),
            'ceo' => $data['ceo'], 'action' => $data['action'], 'acm_type' => $data['acm_type'],
            'req_type' => $data['req_type'], 'user_id' => $data['user_id'],
            'when' => ('' === $data['acm_when'] ? null : $data['acm_when']),
            'whom' => $data['acm_whom']));
    }

    /**
     * Updates the content of a ACM instance.
     *
     * @param $id               Int     ACM instance Id.
     * @param $col              String  Name of the column to be updated.
     * @param $value            Mixed   Value to be updated.
     * @return bool
     */
    public function update_acm($id, $col, $value)
    {
        $sql = 'update cpd_acm_deal'
            . ' set ' . $col . '=:val'
            . ' where cad_id=:id';

        return $this->PDO->update($sql, array('val' => ('' === $value ? null : $value), 'id' => $id));
    }

    /**
     * Adds a additional ACM  instance to a row.
     *
     * @param $id   Int ACM instance Id.
     * @return bool
     */
    public function add_additional_acm($id)
    {
        $sql = 'update cpd_acm_deal'
            . ' set cad_additional_row=cad_additional_row + 1'
            . ' where cad_id=:id';

        $this->PDO->update($sql, array('id' => $id));

        $sql = 'insert into cpd_additional_acm'
            . ' (caa_acm_id)'
            . ' values(:acm_id)';

        return $this->PDO->insert($sql, array('acm_id' => $id));

    }

    /**
     * Deletes an Accountabilty Manager Instance
     *
     * @param $acm_id   Int ACM Id.
     * @return bool
     */
    public function delete_acm($acm_id)
    {
        $sql = 'delete from cpd_acm_deal'
            . ' where cad_id=:acm_id';

        return $this->PDO->delete($sql, array('acm_id' => $acm_id));
    }

    /**
     * Fetches the additional accountability manager instances belonging to a accountability manager instance.
     *
     * @param $id   Int Accountability manager Id.
     * @return bool
     */
    public function get_additional_acm($id)
    {
        $sql = 'select caa_id, caa_acm_id, caa_cmt, caa_whom, caa_action,'
            . ' DATE_FORMAT(caa_when, "%d/%m %H:%i") as caa_when'
            . ' from cpd_additional_acm'
            . ' where caa_acm_id=:id';

        return $this->PDO->select($sql, array('id' => $id));
    }

    /**
     * Stores document info to the DB.
     *
     * @param $data Array Input data
     * @return bool
     */
    public function upload_docs($data)
    {
        $sql = 'insert into cpd_docs'
            . ' (cd_name, cd_extension, cd_random_name,'
            . ' cd_cpd_deal, cd_user_id, cd_format, cd_type, cd_category)'
            . ' values(:name, :extension, :random,'
            . ' :deal_id, :user_id, :format, :type, :category)';

        return $this->PDO->insert($sql, array('name' => $data['file_name'], 'extension' => $data['extension'],
            'random' => $data['random'], 'deal_id' => $data['client_id'], 'user_id' => $data['user_id'],
            'format' => $data['format'], 'type' => $data['type'], 'category' => $data['category']));
    }

    /**
     * Fetches all docs that belong to a client.
     *
     * @param $user_id   Int    User ID.
     * @param $format
     * @param $type      String Document Type.
     * @param $category
     * @return bool
     */
    public function get_docs($user_id, $format, $type, $category)
    {
        $sql = 'select cd_id, cd_name, cd_extension, cd_random_name'
            . ' from cpd_docs'
            . ' where cd_user_id=:user_id and cd_format=:format and cd_type=:type and cd_category=:category';

        return $this->PDO->select($sql, array('user_id' => $user_id, 'format' => $format,
            'type' => $type, 'category' => $category));
    }

    /**
     * Fetches docs info.
     *
     * @param $id   Int Document ID.
     * @return bool
     */
    public function get_doc_info($id)
    {
        $sql = 'select cd_random_name, cd_format, cd_extension'
            . ' from cpd_docs'
            . ' where cd_id=:id';

        return $this->PDO->select($sql, array('id' => $id));
    }

    /**
     * Fetches client info.
     *
     * @param $id   Int Client Id.
     * @return bool
     */
    public function get_client_info($id)
    {
        $sql = 'select cc_email'
            . ' from cpd_deal_info'
            . ' where cc_deal_id=:id';

        return $this->PDO->select($sql, array('id' => $id));
    }

    /**
     * Updates Income Amount.
     *
     * @param $id     Int    Income Id.
     * @param $val    Int    Value to be updated.
     * @param $col    String Name of column.
     * @param $table  String Table Name.
     * @param $pk     String Primary key col name for table
     * @return bool
     */
    public function update_amount($id, $val, $col, $table, $pk)
    {
        $sql = ' update ' . $table
            . ' set ' . $col . '=:val'
            . ' where ' . $pk . '=:id';
        return $this->PDO->update($sql, array('val' => $val, 'id' => $id));
    }

    /**
     * Updates Floor Target Game values for a CPD instances.
     *
     * @param $val      Int     Value.
     * @param $type     Type    Type of field(Quarter/Year).
     * @param $quarter  Int     Quarter Number.
     * @param $year     Int     Year.
     * @param $col      String  Column name(FTG)
     * @param $cpd_id   Int     CPD Id.
     * @return bool
     */
    public function update_ftg($val, $type, $quarter, $year, $col, $cpd_id)
    {

        try {
            $this->PDO->begin_transaction();

            $sql = 'select uo_id'
                . ' from user_objective'
                . ' where uo_year=:year'
                . ' and uo_cpd_id=:cpd_id';

            $year_id = $this->PDO->select($sql, array('year' => $year, 'cpd_id' => $cpd_id));

            // Check if year is empty.
            if (empty($year_id[0]->uo_id)) {
                $sql = 'insert into user_objective'
                    . ' (uo_cpd_id, uo_year)'
                    . ' values(:cpd_id, :year)';

                $year_id = $this->PDO->insert($sql, array('cpd_id' => $cpd_id, 'year' => $year));
            } else {
                $year_id = $year_id[0]->uo_id;
            }

            // Check if FTG is for quarter.
            if ('quarter' === $type) {

                $sql = ' update user_quarter_objective'
                    . ' set uqo_' . $col . '=:val'
                    . ' where uqo_uo_id=:year_id'
                    . ' and uqo_quarter=:quarter';

                $ret = $this->PDO->update($sql, array('val' => $val, 'year_id' => $year_id, 'quarter' => $quarter));

                if (empty($ret)) {
                    $sql = ' insert into user_quarter_objective'
                        . ' (uqo_uo_id, uqo_quarter, uqo_' . $col . ')'
                        . ' values(:year_id, :quarter, :val)';

                    $ret = $this->PDO->insert($sql, array('year_id' => $year_id, 'quarter' => $quarter, 'val' => $val));
                }
            } // Check if FTG type is year.
            else if ('year' === $type) {
                $sql = 'update user_objective'
                    . ' set uo_' . $col . '=:val'
                    . ' where uo_id=:year_id';

                $ret = $this->PDO->update($sql, array('val' => $val, 'year_id' => $year_id));

            }
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            $ret = $e->getMessage();
        }
        return $ret;
    }

    /**
     * Fetches the client information.
     *
     * @param $client_id Int Client Id.
     * @return bool
     */
    public function get_info($client_id)
    {
        $sql = 'select cc_additional_info'
            . ' from cpd_deal_info'
            . ' where cc_deal_id=:client_id';

        return $this->PDO->select($sql, array('client_id' => $client_id));
    }

    /**
     * Updates the client information.
     *
     * @param $client_id Int    Client ID
     * @param $data      String Data to be updated.
     * @param $col       String Column/Field name.
     * @return bool
     */
    public function update_client_info($client_id, $data, $col)
    {
        $sql = 'update cpd_deal_info'
            . ' set ' . $col . '=:val'
            . ' where cc_deal_id=:client_id';

        return $this->PDO->update($sql, array('val' => $data, 'client_id' => $client_id));
    }

    /**
     * Fetches Strategic Analyser Outlines.
     *
     * @param $page_template  String Page template.
     * @return bool
     */
    public function get_sa_outline($page_template)
    {
        // Fetch default outline for selected SAP Page template.
        $sql = ' select csd_id, csd_content'
            . ' from cpd_sa_outline'
            . ' where csd_page_template=:page_template';

        $ret['outline'] = $this->PDO->select($sql, array('page_template' => $page_template));

        // Fetch Default content for SAP compose box popup.
        $sql = 'select csd_template, csd_cols, csd_image1, csd_image2,'
            . ' csdd_col, csdd_content'
            . ' from cpd_sa_default'
            . ' join cpd_sa_default_data'
            . ' on csd_id=csdd_csd_id'
            . ' where csd_template=:template';

        $ret['default_data'] = $this->PDO->select($sql, array('template' => $page_template));

        return $ret;
    }


    /**
     * Fetches default image for SAP document.
     *
     * @param $template String Template Name.
     * @return bool
     */
    public function get_default_sa_image($template)
    {
        $sql = 'select csd_image1, csd_image2'
            . ' from cpd_sa_default'
            . ' where csd_template=:template';

        return $this->PDO->select($sql, array('template' => $template));
    }

    /**
     * Fetches the list of all SA docs.
     *
     * @param $type     String Type of Strategic Analyser.
     * @param $user_id  Int    User Id.
     * @return bool
     */
    public function get_sa_list($type, $user_id)
    {
        $sql = ' select cs_id, cs_name, cs_random_name'
            . ' from cpd_sa'
            . ' where (cs_type=:type and cs_user_id=:user_id) or cs_client_id=0'
            . ' order by cs_id';

        return $this->PDO->select($sql, array('type' => $type, 'user_id' => $user_id));
    }

    /**
     * Saves Strategic Analyser to database
     *
     * @param $sa_id     Int    Strategic Analyser Id.
     * @param $data      HTML   Strategic Analyser content.
     * @param $template  String Template used.
     * @param $image     String Background image Id.
     * @param $page      String Page type.
     * @param $sa_type   String Strategic Analyser type.
     * @return bool
     */
    public function save_sa_data($sa_id, $data, $template, $image, $page, $sa_type)
    {
        if (null !== $image) {
            $duplicate_key_query = ' ON DUPLICATE KEY UPDATE cs_data_data=:data, cs_data_image_1=:image';
        } else {
            $duplicate_key_query = ' ON DUPLICATE KEY UPDATE cs_data_data=:data';
        }
        $sql = ' insert into cpd_sa_data'
            . ' (cs_data_sa_id, cs_data_data, cs_data_template,'
            . ' cs_data_image_1, cs_data_page, cs_data_sa_type)'
            . ' values(:sa_id, :data, :template, :image, :page, :type)'
            . $duplicate_key_query;

        return $this->PDO->insert($sql, array('sa_id' => $sa_id, 'data' => $data,
            'template' => $template, 'image' => $image, 'page' => $page, 'type' => $sa_type));
    }


    /**
     * Creates a record for uploaded background image.
     *
     * @param $random String Random name for Document.
     * @param $sa_id  Int    SAP document ID.
     * @param $page   String Page Type.
     * @param $number Int    Page Number.
     * @return bool
     */
    public function upload_sa($random, $sa_id, $page, $number)
    {
        $sql = 'update cpd_sa_data'
            . ' set cs_data_image_' . $number . '=:random'
            . ' where cs_data_sa_id=:id and cs_data_page=:page';

        return $this->PDO->update($sql, array('random' => $random, 'id' => $sa_id, 'page' => $page));
    }

    /**
     * Fetches and returns Uploaded SA Image
     *
     * @param $id   Int     ID of SA document.
     * @param $page String  Type of Document.
     * @return bool
     */
    public function get_sa_uploads($id, $page)
    {
        $sql = 'select cs_data_image_1'
            . ' from cpd_sa_data'
            . ' where cs_data_sa_id=:id and cs_data_page=:page';

        return $this->PDO->select($sql, array('id' => $id, 'page' => $page));
    }


    /**
     * Fetches Strategic Analyser Data.
     *
     * @param $sa_id Int Strategic Analyser Id.
     * @return array
     */
    public function get_sa_data($sa_id)
    {
        $ret = [];
        $sql = 'select cs_data_id, cs_data_data, cs_data_template,'
            . ' cs_data_image_1, cs_data_image_2, cs_data_page'
            . ' from cpd_sa_data'
            . ' where cs_data_sa_id=:sa_id'
            . ' ORDER BY FIELD(cs_data_page, "lp", "wp", "pp", "ap", "rp", "amp")';

        $ret['data'] = $this->PDO->select($sql, array('sa_id' => $sa_id));

        // Select company and team logo.
        $sql = 'select cs_team_logo, cs_company_logo '
            . ' from cpd_sa'
            . ' where cs_id=:sa_id';
        $ret['logo'] = $this->PDO->select($sql, array('sa_id' => $sa_id));

        return $ret;
    }

    /**
     * Compile all the different composed Strategic Analyser pages into one.
     *
     * @param $random   String Random name for SA.
     * @param $sa_id    Int    Strategic Analyser Id.
     * @return bool
     */
    public function compile_sa($random, $sa_id)
    {
        $sql = 'update cpd_sa '
            . ' set cs_random_name=:random'
            . ' where cs_id=:id';

        return $this->PDO->update($sql, array('random' => $random, 'id' => $sa_id));
    }

    /**
     * Fetches Information about the Strategic Analyser document.
     *
     * @param $id   Int Strategic Analyser Id.
     * @return bool
     */
    public function get_sa_info($id)
    {
        $join_query = '';
        $select_query = '';

        if (0 !== $id) {
            $join_query = ' join cpd_sa_data on cs_data_sa_id=cs_id';
            $select_query = ', cs_data_data, cs_data_page, cs_data_image_1';
        }

        $sql = 'select cs_random_name' . $select_query
            . ' from cpd_sa'
            . $join_query
            . ' where cs_id=:id';

        return $this->PDO->select($sql, array('id' => $id));
    }

    /**
     * Creates a record for Logo uploaded.
     *
     * @param $random String  Logo Random Name.
     * @param $sa_id  Int     Strategic Analyser Id.
     * @param $type   String  Type of Logo(Company logo/Team logo)
     * @return bool
     */
    public function upload_logo($random, $sa_id, $type)
    {
        $sql = 'update cpd_sa'
            . ' set cs_' . $type . '_logo=:random'
            . ' where cs_id=:sa_id';

        return $this->PDO->update($sql, array('random' => $random, 'sa_id' => $sa_id));
    }

    /**
     * Adds a new Strategic Analyzer.
     *
     * @param $user_id      Int     User ID
     * @param $sa_type      String  Type of SA.
     * @param $client_id    Int     Client ID.
     * @return bool
     */
    public function compose_sa($user_id, $sa_type, $client_id)
    {
        $sql = 'insert'
            . ' into cpd_sa'
            . ' (cs_type, cs_name, cs_user_id, cs_client_id)'
            . ' values(:type, :name, :user_id, :client_id)';

        return $this->PDO->insert($sql,
            array('type' => $sa_type, 'name' => 'Custom SA', 'user_id' => $user_id, 'client_id' => $client_id));
    }

    /**
     * Updates a Strategic Analyzer Name.
     *
     * @param $sa_id Int    SA ID
     * @param $value String Value
     * @return bool
     */
    public function update_sa_name($sa_id, $value)
    {
        $sql = 'update cpd_sa'
            . ' set cs_name=:name'
            . ' where cs_id=:sa_id';

        return $this->PDO->update($sql, array('name' => $value, 'sa_id' => $sa_id));
    }

    /**
     * Adds a new Client Information.
     *
     * @param $data         Mixed  Array of data.
     * @param $client_id    Int    Client ID.
     * @param $type         String Type of client.
     * @param $info_type    String Type of Information.
     * @param $is_buyer     String Check if client is buyer.
     * @return bool
     */
    public function add_client_info($data, $client_id, $type, $info_type, $is_buyer, $is_owner = 'false')
    {
        try {
            // Columns to be selected.
            $columns = 'cc_deal_id, cc_entity, cc_title, cc_name, cc_office_phone, cc_cell_phone, cc_email,'
                . ' cc_website, cc_messaging, cc_social, cc_address,'
                . ' cc_type, cc_info_type, cc_ref_source, cc_ref_event, cc_is_owner';

            $values = ':client_id, :entity, :title, :name, :office_phone,'
                . ' :cell_phone, :email, :website, :messaging, :social_media, :address,'
                . ' :type, :info_type, :ref, :event, :is_owner';

            // Check if the type of info is for advisor or staff.
            if ('Advisor' === $type || 'Staff' === $type) {
                $columns .= ', cc_is_buyer';
                $values .= ', :is_buyer';
                $data['is_buyer'] = empty($is_buyer) ? NULL : $is_buyer;
            }

            $data['type'] = $type;
            $data['client_id'] = $client_id;
            $data['info_type'] = $info_type;
            $data['is_owner'] = $is_owner;

            // Insert into Client information.
            $sql = 'insert into cpd_deal_info'
                . ' (' . $columns . ')'
                . ' values(' . $values . ')';

            $ret = $this->PDO->insert($sql, $data);
        } catch (\Exception $e) {
            return $e;
        }
        return $ret;

    }

    /**
     * Fetches the Property information.
     *
     * @param $client_id  Int    Client ID
     * @param $type       String Type of Information.
     * @param $is_buyer   String Type of Buyer.
     * @return bool
     */
    public function get_prop_info($client_id, $type, $is_buyer, $is_owner = 'false')
    {
        if ($is_buyer === 'true') {

            $sql = 'select null as hc_avatar, hb_name as cc_name, hb_main_phone as cc_office_phone, hb_mobile_phone as cc_cell_phone,'
                . ' hb_email as cc_email'
                . ' from homebase_buyer'
                . ' where hb_id=:id';

            $ret = $this->PDO->select($sql, array('id' => $client_id));
        } else {
            $where = '';
            'true' === $is_owner ? $where = 'and cc_is_owner=\'true\'' : '';

            $columns = 'cc_deal_id, cc_entity, cc_title, cc_name, cc_office_phone, cc_cell_phone, cc_email,'
                . ' cc_website, cc_messaging, cc_social, cc_address, cc_job_title, cc_job_desc,'
                . ' cc_type, cc_id, cc_add_email, cc_add_message, cc_image,'
                . ' cc_add_phone, cc_add_address, cc_notes, cc_note_date, cc_ref_source, cc_ref_event';

            $sql = 'select ' . $columns
                . ' from cpd_deal_info'
                . ' where cc_deal_id=:client_id and cc_info_type=:type'
                . ' and (cc_is_buyer=:is_buyer or cc_is_buyer is null)'
                . $where;

            $ret = $this->PDO->select($sql, array('client_id' => $client_id, 'type' => $type, 'is_buyer' => $is_buyer));
        }
        return $ret;
    }

    /**
     * Updates Client Property information.
     *
     * @param $id    Int    Client ID.
     * @param $col   String Name of column
     * @param $value String Value for field.
     * @return bool
     */
    public function update_prop_info($id, $col, $value)
    {
        $sql = 'update cpd_deal_info'
            . ' set ' . $col . '=:val'
            . ' where cc_id=:id';

        return $this->PDO->update($sql, array('val' => $value, 'id' => $id));
    }

    /**
     * Updates a deal properties.
     *
     * @param $id   Int     Deal ID.
     * @param $col  String  Column/Field Name to be updated.
     * @param $val  Mixed   Value for field.
     * @return bool|string
     */
    public function update_deal($id, $col, $val)
    {
        $sql = 'update deal'
            . ' set ' . $col . '=:val'
            . ' where deal_asset_id=:id';

        $ret = $this->PDO->update($sql, array('val' => $val, 'id' => $id));
        return $ret;
    }

    /**
     * Adds a new Lead for CPD instance.
     *
     * @param $user_id  Int     Deal Id.
     * @param $type     String  Type of column (Lead/Source).
     * @param $name     String  Name of Lead.
     * @return bool
     */
    public function add_lead($user_id, $type, $name)
    {
        $sql = 'insert into deal_lead'
            . ' (dl_name, dl_type, dl_user_id)'
            . ' values(:name, :type, :user_id)';

        return $this->PDO->insert($sql, array('name' => $name, 'type' => $type, 'user_id' => $user_id));
    }

    /**
     * Fetches a list of all the sent documents for deal.
     *
     * @param $deal_id          Int     Deal ID
     * @param $type             String  Type of Document.
     * @param $component_name   String  Component Name.
     * @param $col_name         String  Name of column.
     * @param $table_name       String  Table Name.
     * @param $ref_key          String  Reference Key.
     * @return array
     */
    public function view_sent($deal_id, $type, $component_name, $col_name, $table_name, $ref_key)
    {
        $where = 'component_sent_origin=\'' . $type . '\'';

        ('homebase' === $type) ? $where = '(component_sent_origin=\'cpdv1\' || component_sent_origin=\'tsl\')' : '';

        $sql = ' select component_sent_id as id, component_sent_doc_type, component_sent_origin,'
            . ' component_sent_doc_id, ' . $col_name . ' as name, component_sent_datetime, component_sent_tool_id'
            . ' from component_sent left join ' . $table_name . ' on component_sent_doc_id=' . $ref_key
            . ' where ' . $where . ' and component_sent_client_id=:id and component_sent_doc_type=:type'
            . ' order by component_sent_datetime desc';

        return $this->PDO->select($sql, array('id' => $deal_id, 'type' => strtolower($component_name)));
    }

    /**
     * Fetches Strategic Analyzer details.
     *
     * @param $sa_id    Int SA ID.
     * @param $user_id  Int User ID.
     * @return bool
     */
    public function get_sa_details($sa_id, $user_id)
    {
        $sql = 'select cs_type'
            . ' from cpd_sa'
            . ' where cs_id=:id and cs_user_id=:user_id';

        return $this->PDO->select($sql, array('id' => $sa_id, 'user_id' => $user_id));
    }

    /**
     * Fetces Asset Update details.
     *
     * @param $update_id Int Update Id.
     * @param $user_id   Int User ID.
     * @return bool
     */
    public function get_update_details($update_id, $user_id)
    {
        $sql = 'select tu_outline_id'
            . ' from tsl_updates'
            . ' where tu_id=:id and tu_user_id=:user_id';

        return $this->PDO->select($sql, array('id' => $update_id, 'user_id' => $user_id));
    }

    /**
     * Fetches Docs Details.
     *
     * @param $doc_id   Int Dcument ID.
     * @param $user_id  Int User ID.
     * @return bool
     */
    public function get_docs_details($doc_id, $user_id)
    {
        $sql = 'select cd_type, cd_category, cd_format '
            . ' from cpd_docs'
            . ' where cd_id=:id and cd_user_id=:user_id';

        return $this->PDO->select($sql, array('id' => $doc_id, 'user_id' => $user_id));
    }

    /**
     * Deletes a document form the database
     *
     * @param $doc_id   Int Document ID.
     * @return bool
     */
    public function delete_doc($doc_id)
    {
        $sql = 'delete from cpd_docs'
            . ' where cd_id=:id';

        return $this->PDO->delete($sql, array('id' => $doc_id));
    }

    /**
     * Deletes a Lead instance.
     *
     * @param $id   Int Lead ID.
     * @return bool
     */
    public function delete_lead($id)
    {
        $sql = 'delete from deal_lead'
            . ' where dl_id=:id';

        return $this->PDO->delete($sql, array('id' => $id));
    }

    /**
     * Fetches Survey Details
     *
     * @param $survey_id  Int Survey ID.
     * @param $user_id    Int User ID.
     * @return bool
     */
    public function get_survey_details($survey_id, $user_id)
    {
        $sql = 'select tsl_survey_name, tsl_survey_outline_id,'
            . ' tso_survey_type, tso_client_type, tso_survey_class, tso_survey_phase'
            . ' from tsl_survey join tsl_survey_outline'
            . ' on tsl_survey_outline_id=tso_id'
            . ' where tso_user_id=:user_id and tsl_survey_id=:survey_id';

        return $this->PDO->select($sql, array('user_id' => $user_id, 'survey_id' => $survey_id));
    }

    /**
     * Fetches Profile Details
     *
     * @param $profile_id Int Profile Instance ID.
     * @param $user_id    Int User ID.
     * @return bool
     */
    public function get_profile_details($profile_id, $user_id)
    {
        $sql = 'select tsl_profile_name, tsl_profile_outline_id,'
            . ' tpo_profile_type, tpo_client_type, tpo_project_type, tpo_profile_phase'
            . ' from tsl_profile join tsl_profile_outline'
            . ' on tsl_profile_outline_id=tpo_id'
            . ' where tpo_user_id=:user_id and tsl_profile_id=:profile_id';

        return $this->PDO->select($sql, array('user_id' => $user_id, 'profile_id' => $profile_id));
    }

    /**
     * Updates notes content for a CPD instance.
     *
     * @param $deal_id  Int     Deal ID
     * @param $col      String  Column Name
     * @param $seq      Int     Sequence Number
     * @param $value    String  Values
     * @return bool
     */
    public function update_notes_content($deal_id, $col, $seq, $value)
    {
        $sql = ' insert into deal_notes'
            . ' (dn_deal_id, dn_seq, ' . $col . ')'
            . ' values(:deal_id, :seq, :value)'
            . ' ON DUPLICATE KEY UPDATE ' . $col . '=:value';

        return $this->PDO->insert($sql, array('deal_id' => $deal_id, 'seq' => $seq, 'value' => $value));

    }

    /**
     * Fetch Notes Content for a CPD instance.
     *
     * @param $deal_id  Int Deal ID.
     * @return bool
     */
    public function get_notes_content($deal_id)
    {

        $sql = 'select dn_deal_id, dn_seq, dn_name, dn_note, dn_when'
            . ' from deal_notes'
            . ' where dn_deal_id=:deal_id'
            . ' order by dn_seq';

        return $this->PDO->select($sql, array('deal_id' => $deal_id));
    }

    /**
     * Get all the Target Audience and Criteria Instances.
     *
     * @param $user_id
     * @return mixed
     */
    public function get_all_ta_criteria($user_id)
    {
        // Fetch the Criteria.
        $sql = 'select uc_id, uc_name'
            . ' from user_criteria'
            . ' where uc_user_id=:user_id';

        $ret['criteria'] = $this->PDO->select($sql, array('user_id' => $user_id));

        // Fetch the target Audiences.
        $sql = 'select ul_id, ul_name'
            . ' from user_location'
            . ' where ul_user_id=:user_id';

        $ret['ta'] = $this->PDO->select($sql, array('user_id' => $user_id));

        return $ret;
    }


    /**
     * Fetch CPD metrics data
     *
     * @param $cpd_id     Int    CPD ID.
     * @param $active_tab String Active tab.
     * @param $user_id    Int    User ID.
     * @param $quarter    Int    Quarter Number.
     * @return bool
     */
    public function get_cpd_metric($cpd_id, $active_tab, $user_id, $quarter)
    {
        $sql = 'select has_status as ds_status, sum(hca_net) as net, sum(hca_price) as price, count(deal_net) as count,'
            . ' sum(hca_price * hca_rate / 100) as gross'
            . ' from deal'
            . ' join homebase_client_asset'
            . ' on hca_id=deal_asset_id'
            . ' join (SELECT has_status, has1.has_datetime, has_id, has_asset_id, has_md'
            . ' FROM homebase_asset_status has1'
            . ' JOIN (SELECT MAX(has_datetime) AS time, has_asset_id as asset_id'
            . ' FROM homebase_asset_status'
            . ' WHERE quarter(has_datetime)<=:quarter'
            . ' GROUP BY has_asset_id) has2'
            . ' ON has1.has_datetime = has2.time and has1.has_asset_id = asset_id) as has_status'
            . ' on deal_asset_id=has_status.has_asset_id'
            . ' join code on code_abbrev=has_status.has_status'
            . ' join code_type on code_type_id=ct_id and (ct_abbrev=:tab)'
            . ' join cpd on cpd_id=deal_cpd_id'
            . ' where deal_cpd_id=:cpd_id and cpd_user_id=:user_id and quarter(has_status.has_datetime)<=:quarter'
            . ' group by has_status';

        return $this->PDO->select($sql,
            ['tab' => $active_tab, 'user_id' => $user_id, 'cpd_id' => $cpd_id, 'quarter' => $quarter]);
    }


    /**
     *
     * Adds a new assembler instance.
     * @param $client_id Int Client ID.
     * @return bool
     */
    public function add_assembler_instance($client_id)
    {
        $sql = 'insert'
            . ' into cpd_assembler_version'
            . ' (cav_client_id)'
            . ' values(:client_id)';

        return $this->PDO->insert($sql, array('client_id' => $client_id));

    }

    /**
     * Fetches and returns assembler instance.
     *
     * @param $client_id Int Client Id.
     * @return bool
     */
    public function get_assembler_instance($client_id)
    {
        $sql = 'select cav_id, cav_name, cav_rand_name'
            . ' from cpd_assembler_version'
            . ' where cav_client_id=:client_id';

        return $this->PDO->select($sql, array('client_id' => $client_id));
    }

    /**
     * Fetches assembler information.
     *
     * @param $id   Int Assembler Id.
     * @return bool
     */
    public function get_assember_info($id)
    {
        $sql = 'select cav_rand_name as cd_random_name'
            . ' from cpd_assembler_version'
            . ' where cav_id=:id';

        return $this->PDO->select($sql, array('id' => $id));
    }

    /**
     * Fetches Page information.
     *
     * @param $id   Int Assembler page Id.
     * @return bool
     */
    public function get_page_info($id)
    {
        $sql = 'select cap_rand_name as cd_random_name'
            . ' from cpd_assembler_page'
            . ' where cap_id=:id';

        return $this->PDO->select($sql, array('id' => $id));
    }

    /**
     * Creates a record for uploaded assembler page.
     *
     * @param $data       Array Data for assembler page.
     * @return \Exception
     */
    public function upload_page($data)
    {
        try {
            $this->PDO->begin_transaction();
            $sql = 'select max(cap_order) as `order`'
                . ' from cpd_assembler_page'
                . ' where cap_version_id=:version_id';

            $order = $this->PDO->select($sql, array('version_id' => $data['version_id']))[0]->order;

            // Check if order is null.
            (null === $order) ? $order = 1 : $order = $order + 1;

            $sql = 'insert into cpd_assembler_page'
                . ' (cap_version_id, cap_name, cap_rand_name, cap_order)'
                . ' values(:version, :name, :rand, :order)';

            $ret['page_uploaded'] = $this->PDO->insert($sql, array('version' => $data['version_id'],
                'name' => $data['file_name'], 'rand' => $data['random'], 'order' => $order));
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return $e;
        }
        return $ret;
    }

    /**
     * Fetches assembler pages data.
     *
     * @param $doc_id Int Document Id.
     * @return bool
     */
    public function get_assembler_pages($doc_id)
    {
        $sql = 'select cap_id, cap_name, cap_rand_name, cap_order'
            . ' from cpd_assembler_page'
            . ' where cap_version_id=:version_id'
            . ' order by cap_order';

        return $this->PDO->select($sql, array('version_id' => $doc_id));
    }

    /**
     * Fetches random name for assembler document.
     *
     * @param $version_id   Int Version Id.
     * @return bool
     */
    public function get_assembler_random($version_id)
    {
        $sql = 'select cav_rand_name'
            . ' from cpd_assembler_version'
            . ' where cav_id=:id';

        return $this->PDO->select($sql, array('id' => $version_id));
    }

    /**
     * Updates assembler document random name.
     *
     * @param $random       String Random name.
     * @param $version_id   Int    Version Id.
     * @return bool
     */
    public function update_assembly_doc($random, $version_id)
    {
        $sql = 'update cpd_assembler_version'
            . ' set cav_rand_name=:rand'
            . ' where cav_id=:id';

        return $this->PDO->update($sql, array('rand' => $random, 'id' => $version_id));
    }

    /**
     * Moves a page up in the assembler order.
     *
     * @param $page_id      Int Assembler page Id.
     * @param $order        Int New order for assembler page.
     * @param $version_id   Int Assembler Version Id.
     * @return bool
     */
    public function move_page_up($page_id, $order, $version_id)
    {
        try {
            $this->PDO->begin_transaction();

            // Update order for all the previous pages.
            $sql = 'update cpd_assembler_page'
                . ' set cap_order=:new_order'
                . ' where cap_version_id=:version_id and cap_order=:order';

            $ret['updated_prev_page'] = $this->PDO->update($sql,
                array('version_id' => $version_id, 'order' => $order - 1, 'new_order' => $order));

            // Update order for current page.
            $sql = ' update cpd_assembler_page'
                . ' set cap_order=:order'
                . ' where cap_id=:page_id';

            $ret['updated_current_page'] = $this->PDO->update($sql, array('order' => $order - 1, 'page_id' => $page_id));
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }
        return $ret;
    }

    /**
     * Moves a page down in assembler order.
     *
     * @param $page_id      Int Page Id.
     * @param $order        Int Order for page.
     * @param $version_id   Int Assembler version Id.
     * @return bool
     */
    public function move_page_down($page_id, $order, $version_id)
    {
        try {
            $this->PDO->begin_transaction();

            // Update Assembler page order for previous pages.
            $sql = 'update cpd_assembler_page'
                . ' set cap_order=:new_order'
                . ' where cap_version_id=:version_id and cap_order=:order';

            $ret['updated_prev_page'] = $this->PDO->update($sql,
                array('version_id' => $version_id, 'order' => $order + 1, 'new_order' => $order));

            // Update assembler page orde for current page.
            $sql = ' update cpd_assembler_page'
                . ' set cap_order=:order'
                . ' where cap_id=:page_id';

            $ret['updated_current_page'] = $this->PDO->update($sql, array('order' => $order + 1, 'page_id' => $page_id));
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }
        return $ret;
    }

    /**
     * Deletes an assembler page.
     *
     * @param $page_id  Int Assembler Page Id.
     * @return bool
     */
    public function delete_page($page_id)
    {
        $sql = 'delete'
            . ' from cpd_assembler_page'
            . ' where cap_id=:id';

        return $this->PDO->delete($sql, array('id' => $page_id));
    }

    /**
     * Deletes an assembled Document.
     *
     * @param $doc_id   Int Document Id.
     * @return bool
     */
    public function delete_assembled_doc($doc_id)
    {
        $sql = 'delete'
            . ' from cpd_assembler_version'
            . ' where cav_id=:id';

        return $this->PDO->delete($sql, array('id' => $doc_id));
    }

    /**
     * Updates Assembler page name.
     *
     * @param $id    Int    Page Id.
     * @param $value String Name for page.
     * @return bool
     */
    public function update_assembler_page_name($id, $value)
    {
        $sql = 'update cpd_assembler_page'
            . ' set cap_name=:name'
            . ' where cap_id=:id';

        return $this->PDO->update($sql, array('id' => $id, 'name' => $value));
    }

    /**
     * Updates Assembler document.
     *
     * @param $id    Int    Document Id.
     * @param $value String Name for document.
     * @return bool
     */
    public function update_assembler_name($id, $value)
    {
        $sql = 'update cpd_assembler_version'
            . ' set cav_name=:name'
            . ' where cav_id=:id';

        return $this->PDO->update($sql, array('id' => $id, 'name' => $value));
    }

    /**
     * Updates DB record for call information
     *
     * @param $deal_id  Int     Deal Id.
     * @param $type     String  Request type.
     * @param $date     Date    Date of request.
     * @param $time     Time    Time of request.
     * @param $ccm_id   Int     Call control manager Id.
     * @return bool|\Exception
     */
    public function start_end_call($deal_id, $type, $date, $time, $ccm_id)
    {
        try {
            if ('start' === $type) {
                $sql = 'insert into cpd_call_manager'
                    . ' (ccm_start, ccm_date, ccm_deal_id)'
                    . ' values(:start, :date, :deal_id)';

                $ret = $this->PDO->insert($sql, array('start' => $time, 'date' => $date, 'deal_id' => $deal_id));
            } else {
                $sql = ' update cpd_call_manager'
                    . ' set ccm_end=:time'
                    . ' where ccm_id=:ccm_id';

                $ret = $this->PDO->update($sql, array('time' => $time, 'ccm_id' => $ccm_id));
            }
        } catch (\Exception $e) {
            return $e;
        }
        return $ret;
    }

    /**
     * Fetches call information.
     *
     * @param $deal_id  Int Deal Id.
     * @return bool
     */
    public function get_call_info($deal_id)
    {
        $sql = 'select ccm_start, ccm_end, ccm_date, ccm_purpose, ccm_duration, ccm_id'
            . ' from cpd_call_manager'
            . ' where ccm_deal_id=:deal_id and ccm_end is null'
            . ' order by ccm_date, ccm_start desc'
            . ' limit 1';

        return $this->PDO->select($sql, array('deal_id' => $deal_id));
    }


    /**
     * Updates call purpose for the call information popup.
     *
     * @param $ccm  Int     Call control manager Id.
     * @param $val  String  Call Purpose notes.
     * @return bool
     */
    public function update_call_purpose($ccm, $val)
    {
        $sql = 'update cpd_call_manager'
            . ' set ccm_purpose=:purpose'
            . ' where ccm_id=:id';

        return $this->PDO->update($sql, array('purpose' => $val, 'id' => $ccm));
    }

    /**
     * Fetches call control report information.
     *
     * @param $id     Int Call control manager Id.
     * @return array
     */
    public function get_ccr_info($id)
    {
        $sql = 'select ccm_id, ccm_start, ccm_end,'
            . ' ccm_date, ccm_purpose, ccm_duration, ccm_rating'
            . ' from cpd_call_manager'
            . ' where ccm_deal_id=:id';

        $result = $this->PDO->select($sql, array('id' => $id));
        $ret = [];

        foreach ($result as $col) {
            $ret[$col->ccm_date][] = $col;
        }

        return $ret;
    }

    /**
     * Updates Call ratings.
     *
     * @param $id   Int Call control manager Id.
     * @param $val  Int Rating for call.
     * @return bool
     */
    public function update_call_rating($id, $val)
    {
        $sql = 'update cpd_call_manager'
            . ' set ccm_rating=:rating'
            . ' where ccm_id=:id';

        return $this->PDO->update($sql, array('rating' => $val, 'id' => $id));
    }

    /**
     * Fetches information popup image.
     *
     * @param $cc_id  Int Information Id.
     * @return bool
     */
    public function get_info_image($cc_id)
    {
        $sql = 'select cc_image'
            . ' from cpd_deal_info'
            . ' where cc_id=:id';

        return $this->PDO->select($sql, array('id' => $cc_id));
    }

    /**
     * Uploads an information popup Image.
     *
     * @param $cc_id   Int      Information Id.
     * @param $random  String   Name for Image.
     * @return bool
     */
    public function upload_info_image($cc_id, $random)
    {
        $sql = 'update cpd_deal_info'
            . ' set cc_image=:image'
            . ' where cc_id=:id';

        return $this->PDO->update($sql, array('image' => $random, 'id' => $cc_id));
    }

    /**
     * Fetcehs messages/scripts instances for user.
     *
     * @param $user_id  Int User Id.
     * @return mixed
     */
    public function get_message_script($user_id)
    {
        // Selects messages.
        $sql = 'select tm_id, tm_name, tms_name, tms_id '
            . ' from tsl_message_version'
            . ' join tsl_message_sequence'
            . ' on tms_version_id=tm_id'
            . ' where tm_user_id=:user_id';

        $messages = $this->PDO->select($sql, array('user_id' => $user_id));

        $ret['message'] = [];
        foreach ($messages as $message) {
            $ret['message'][$message->tm_id][] = $message;
        }

        // Select scripts.
        $sql = ' select tsn_id, ts_id, tsn_name, ts_version, tsn_type_id, tsl_script_type'
            . ' from tsl_script'
            . ' join tsl_script_name'
            . ' on ts_name_id=tsn_id'
            . ' join tsl_script_default'
            . ' on tsn_type_id=tsl_script_default_id'
            . ' where tsn_user_id=:user_id';

        $scripts = $this->PDO->select($sql, array('user_id' => $user_id));

        $ret['script'] = [];
        foreach ($scripts as $script) {
            $ret['script'][$script->tsn_type_id][] = $script;
        }

        return $ret;
    }

    /**
     * Fetches script/message content.
     *
     * @param $id       Int     Instance Id.
     * @param $type     String  Type of instance.
     * @param $deal_id  Int     Deal Id.
     * @return bool
     */
    public function get_message_script_content($id, $type, $deal_id)
    {
        // Check if the request is for message/script.
        if ('message' === $type) {
            $sql = 'select tmc_content as content'
                . ' from tsl_message_content'
                . ' where tmc_seq_id=:id'
                . ' order by tmc_index';

            $ret = $this->PDO->select($sql, array('id' => $id));

            if (0 === count($ret)) {
                $sql = 'select tmd_text as content, tmo_content as outline'
                    . ' from tsl_message_default'
                    . ' join tsl_message_sequence'
                    . ' on tms_sequence_number=tmd_seq_number'
                    . ' join tsl_message_outline'
                    . ' on tmo_id=1'
                    . ' where tms_id=:id and tmd_component=\'Message\'';

                $ret = $this->PDO->select($sql, array('id' => $id));
            }
        } else {
            $sql = 'select tsa_content as content, tsl_script_content as outline'
                . ' from tsl_script_answer'
                . ' join tsl_script_default'
                . ' on tsl_script_default_id=tsa_default'
                . ' where tsa_instance_id=:id and tsa_client_id=:client_id'
                . ' order by tsa_question_id';

            $ret = $this->PDO->select($sql, array('id' => $id, 'client_id' => $deal_id));

            // Check if there is no answer present.
            if (0 === count($ret)) {
                $sql = 'select tsda_content as content, tsl_script_content as outline'
                    . ' from tsl_script_default_answer'
                    . ' join tsl_script_default'
                    . ' on tsl_script_default_id=tsda_outline_id'
                    . ' where tsl_script_requester=:type and tsl_script_type=:script_type'
                    . ' order by tsda_answer_number';

                $ret = $this->PDO->select($sql, array('script_type' => 'Initial Contact', 'type' => $type));
            }
        }

        return $ret;
    }

    /**
     * Saves client information.
     *
     * @param $id           Int     Client Id.
     * @param $val          String  Value for field.
     * @param $type         String  Field Name.
     * @param $form_type    String  Type of form.
     * @return bool
     */
    public function save_client_popup_info($id, $val, $type, $form_type)
    {
        $sql = 'insert into cpd_deal_agent_info'
            . ' (cdai_cc_id, cdai_val, cdai_type, cdai_form_type)'
            . ' values(:cc_id, :val, :type, :form_type)'
            . ' on duplicate key update cdai_val=:val';


        return $this->PDO->insert($sql,
            array('cc_id' => $id, 'val' => $val, 'type' => $type, 'form_type' => $form_type));
    }

    /**
     * Fetches client information.
     *
     * @param $cc_id        Int     Agent Id.
     * @param $form_type    String  Type of form.
     * @return bool
     */
    public function get_client_popup_info($cc_id, $form_type)
    {
        $sql = ' select cdai_type, cdai_val'
            . ' from cpd_deal_agent_info'
            . ' where cdai_cc_id=:cc_id and cdai_form_type=:form_type';

        return $this->PDO->select($sql, array('cc_id' => $cc_id, 'form_type' => $form_type));
    }

    /**
     * Updates job field for client information.
     *
     * @param $id   Int     Client Information Id.
     * @param $val  String  Value for field.
     * @param $col  String  Field Name.
     * @return bool
     */
    public function update_job_field($id, $val, $col)
    {
        $sql = 'update cpd_deal_info'
            . ' set ' . $col . '=:val'
            . ' where cc_id=:id';

        return $this->PDO->update($sql, array('id' => $id, 'val' => $val));
    }

    /**
     * Adds a new buyer offer for the asset.
     *
     * @param $id      Int    Asset Id.
     * @param $date    Date   Date.
     * @param $seq     Int    Sequence Number.
     * @param $cc_id   Int    Client Id.
     * @param $type    String Type of Buyer.
     * @return bool
     */
    public function add_buyer_offer($id, $date, $seq, $cc_id, $type)
    {
        $sql = 'insert into homebase_buyer_info'
            . ' (hbi_asset_id,  hbi_created_on, hbi_seq, hbi_cc_id, hbi_type)'
            . ' values(:asset_id, :created_on,:seq, :cc_id, :type)';

        return $this->PDO->insert($sql, array('asset_id' => $id, 'created_on' => $date,
            'seq' => $seq, 'cc_id' => $cc_id, 'type' => $type));
    }

    /**
     * Fetches buyer offer from the database.
     *
     * @param $asset_id Int Asset Id.
     * @return bool
     */
    public function get_buyer_offer($asset_id)
    {
        $sql = 'select hbi_asset_id, hbi_offer, hbi_ca, hbi_type,'
            . ' hbi_tour_date, hbi_offer_contingencies,'
            . ' hbi_proof, hbi_financing, hbi_terms,'
            . ' hbi_fee, hbi_created_on, hbi_seq, cc_name'
            . ' from homebase_buyer_info'
            . ' join cpd_deal_info'
            . ' on cc_id=hbi_cc_id'
            . ' where hbi_asset_id=:id'
            . ' order by hbi_seq';

        return $this->PDO->select($sql, array('id' => $asset_id));
    }

    /**
     * Updates homebase buyer information.
     *
     * @param $id   Int     Buyer Info Id.
     * @param $val  String  Value for field.
     * @param $col  String  Field Name.
     * @param $seq  Int     Sequence Number.
     * @return bool
     */
    public function update_hbi_data($id, $val, $col, $seq)
    {
        $sql = 'update homebase_buyer_info'
            . ' set ' . $col . '=:val'
            . ' where hbi_asset_id=:id and hbi_seq=:seq';

        return $this->PDO->update($sql, array('val' => $val, 'id' => $id, 'seq' => $seq));
    }

    /**
     * Fetches and returns Strategic Analysis data.
     *
     * @param $asset_id Int Asset Id.
     * @return bool
     */
    public function fetch_sa($asset_id)
    {
        $sql = 'select has_id, has_desc_history,'
            . ' hasi_type, hasi_seq, hasi_text, hasi_name, has_desc_fact,'
            . ' has_desc_possible, has_desc_accomplished, has_desc_needed, has_decl_decl,'
            . ' has_decl_prom, has_decl_risk, has_decl_invitation, has_decl_responsibility'
            . ' from homebase_asset_sa'
            . ' join homebase_asset_sa_info'
            . ' on hasi_has_id=has_id'
            . ' where has_asset_id=:asset_id';

        return $this->PDO->select($sql, array('asset_id' => $asset_id));
    }

    /**
     * Saves a copy of SA.
     *
     * @param $id   Int     ID of asset.
     * @param $val  String  Value for field.
     * @param $type String  Type of SA.
     * @param $seq  Int     Sequence for SA field.
     * @return bool|string
     */
    public function save_sa($id, $val, $type, $seq)
    {
        try {
            $this->PDO->begin_transaction();
            $sql = 'insert into homebase_asset_sa'
                . ' (has_asset_id)'
                . ' values(:asset_id)'
                . ' on duplicate key'
                . ' update has_id=has_id';

            $key = $this->PDO->insert($sql, array('asset_id' => $id));

            // Check if record is already present.
            if ('0' === $key) {
                $sql = 'select has_id'
                    . ' from homebase_asset_sa'
                    . ' where has_asset_id=:id';

                $key = $this->PDO->select($sql, array('id' => $id))[0]->has_id;
            }

            // Check for the type of SA info.
            if ('factor' === $type || 'option' === $type || 'recommendation' === $type
                || 'recommendation-impact' === $type || 'recommendation-action' === $type) {
                $sql = ' insert into homebase_asset_sa_info'
                    . ' (hasi_has_id, hasi_type, hasi_seq, hasi_text)'
                    . ' values(:has_id, :type, :seq, :text)'
                    . ' on duplicate key'
                    . ' update hasi_text=:text';

                $ret = $this->PDO->insert($sql, array('has_id' => $key, 'type' => $type, 'seq' => $seq, 'text' => $val));
            } else {
                $sql = 'update homebase_asset_sa'
                    . ' set has_' . $type . '=:val'
                    . ' where has_id=:id';

                $ret = $this->PDO->insert($sql, array('val' => $val, 'id' => $key));
            }

            $this->PDO->commit_transaction();

        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return $e->getMessage();
        }
        return $ret;
    }

    /**
     * Updates Strategic Analysis information name.
     *
     * @param $id    Int    Asset Id.
     * @param $seq   Int    Sequence Number
     * @param $type  String Type of Information.
     * @param $value String Value for field.
     * @return bool
     */
    public function update_sa_info_name($id, $seq, $type, $value)
    {
        $sql = 'update homebase_asset_sa_info'
            . ' join homebase_asset_sa'
            . ' on has_id=hasi_has_id'
            . ' set hasi_name=:name'
            . ' where hasi_seq=:seq and hasi_type=:type and has_asset_id=:asset_id';

        return $this->PDO->select($sql, array('name' => $value, 'seq' => $seq, 'type' => $type, 'asset_id' => $id));
    }

    /**
     * Fetches and returns collaboration data.
     *
     * @param $user_id      Int User Id.
     * @param $cpd_id       Int CPD Id.
     * @return \Exception
     */
    public function get_collaboration_data($user_id, $cpd_id)
    {
        try {
            $this->PDO->begin_transaction();

            // Select the topic data.
            $sql = 'select collab_script_id, collab_script_name, collab_script_default'
                . ' from collab_script'
                . ' where collab_script_user_id=0 or collab_script_user_id=:user_id';

            $ret['topic'] = $this->PDO->select($sql, array('user_id' => $user_id));

            // Select recipient data.
            $sql = ' select collab_recipient_id, collab_recipient_name'
                . ' from collab_recipient'
                . ' where  collab_recipient_user_id=:user_id';

            $ret['recipient'] = $this->PDO->select($sql, array('user_id' => $user_id));

            // Select format for collaboration.
            $sql = 'select collab_format_id, collab_format_name, collab_format_default, collab_format_message'
                . ' from collab_format'
                . ' where collab_format_user_id=:user_id or collab_format_user_id=0';

            $ret['format'] = $this->PDO->select($sql, array('user_id' => $user_id));

            // Select user details.
            $sql = 'select user_nicename as collab_sender_name, ID as user_id'
                . ' from pst_wp_cpd.wp_users'
                . ' where ID=:user_id';

            $ret['sender'] = $this->PDO->select($sql, array('user_id' => $user_id));

            // Select Collaboration details.
            $sql = 'select collab_id, collab_sender_name, collab_recipient_id, collab_format_name,'
                . ' collab_script_id, collab_created_at, collab_data, collab_collab_id'
                . ' from collab  collaboration_data'
                . ' join collab_format collaboration_format'
                . ' on collaboration_data.collab_format_id=collaboration_format.collab_format_id'
                . ' where collab_cpd_id=:cpd_id';

            $ret['collab'] = $this->PDO->select($sql, array('cpd_id' => $cpd_id));

            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return $e;
        }

        return $ret;

    }

    /**
     * Adds collaboration request.
     *
     * @param $cpd_id       Int     CPD Id.
     * @param $sender       Int     Sender Id.
     * @param $recipient    Int     Recipient Id.
     * @param $topic        Int     Topic Id for collaboration.
     * @param $format       Int     Format Id for collaboration.
     * @param $user_id      Int     User Id.
     * @param $date_time    DateTime
     * @param $order        Int     Order for collaboration.
     * @return bool
     */
    public function add_collab($cpd_id, $sender, $recipient, $topic, $format, $user_id, $date_time, $order, $user_email)
    {
        $sql = 'insert into collab'
            . ' (collab_cpd_id, collab_user_id, collab_sender_name, collab_order,'
            . ' collab_recipient_id, collab_format_id, collab_script_id, collab_created_at, collab_user_email)'
            . ' values(:cpd_id, :user_id, :sender, :order,:recipient, :format, :topic, :date_time, :user_email)';

        return $this->PDO->insert($sql,
            array('cpd_id' => $cpd_id, 'user_id' => $user_id, 'sender' => $sender, 'order' => $order,
                'recipient' => $recipient, 'format' => $format, 'topic' => $topic,
                'date_time' => $date_time, 'user_email' => $user_email));
    }


    /**
     * Deletes a collaboration instance.
     *
     * @param $id   Int Collaboration Id.
     * @return bool
     */
    public function delete_collab($id)
    {
        $sql = 'delete from script_collab'
            . ' where sc_id=:id';

        return $this->PDO->delete($sql, array('id' => $id));
    }

    /**
     * Update Deal Conversion Board value.
     *
     * @param $id   Int Asset Id.
     * @param $qtr  Int Quarter.
     * @param $prob Int Probability.
     * @return bool
     */
    public function update_dcb($id, $qtr, $prob)
    {
        $sql = 'insert into cpd_dcb'
            . ' (cd_asset_id, cd_qtr, cd_prob)'
            . ' values(:asset_id, :qtr, :prob)'
            . ' on duplicate key'
            . ' update cd_prob=:prob';

        return $this->PDO->insert($sql, array('asset_id' => $id, 'qtr' => $qtr, 'prob' => $prob));

    }

    /**
     * Fetches Deal conversion board data.
     *
     * @param $id   Int Asset Id.
     * @return bool
     */
    public function fetch_dcb($id)
    {
        $sql = 'select cd_asset_id, cd_qtr, cd_prob'
            . ' from cpd_dcb'
            . ' where cd_asset_id=:id';

        return $this->PDO->select($sql, array('id' => $id));
    }

    public function delete_note($deal_id, $seq)
    {
        $sql = 'delete from deal_notes'
            . ' where dn_deal_id=:deal_id and dn_seq=:seq';

        return $this->PDO->delete($sql, array('deal_id' => $deal_id, 'seq' => $seq));
    }

    public function get_asset_info($asset_id)
    {
        $sql = 'select hca_name, hc_name'
            . ' from homebase_client_asset'
            . ' join homebase_client'
            . ' on hca_client_id=hc_id'
            . ' where hca_id=:asset_id';

        return $this->PDO->select($sql, array('asset_id' => $asset_id));
    }

    public function add_comps($asset_id)
    {
        $sql = 'insert into '
            . ' agent_comps (ac_asset_id)'
            . ' values (:asset_id)';

        return $this->PDO->insert($sql, array('asset_id' => $asset_id));
    }

    public function get_comps($asset_id)
    {
        $sql = 'select ac_id, ac_asset_id, ac_built, ac_address, ac_units,'
            . ' ac_price_per_unit, ac_price_per_sf, ac_avg_sf, ac_price, ac_avatar,'
            . ' ac_prop_type, ac_prop_name, ac_region, ac_city, ac_state, ac_zip'
            . ' from agent_comps'
            . ' where ac_asset_id=:asset_id';

        return $this->PDO->select($sql, array('asset_id' => $asset_id));
    }

    public function update_comps_data($id, $val, $col)
    {
        $sql = 'update agent_comps set '
            . $col . '=:val'
            . ' where ac_id=:id';

        return $this->PDO->insert($sql, array('val' => $val, 'id' => $id));
    }

    public function get_comps_info($id)
    {
        $sql = 'select ac_prop_type, ac_prop_name, ac_region,'
            . ' ac_address, ac_city, ac_state, ac_zip'
            . ' from agent_comps'
            . ' where ac_id=:id';

        return $this->PDO->select($sql, array('id' => $id));
    }

    public function get_revenue_data($id)
    {
        $sql = 'select har_id, har_type, har_sq_ft, har_total_sq_ft,'
            . ' har_rent_per_mo, har_total_rent_per_mo, har_number'
            . ' from homebase_asset_revenue'
            . ' where har_asset_id=:asset_id';

        return $this->PDO->select($sql, array('asset_id' => $id));
    }

    public function get_prefer_content($id)
    {
        $sql = 'select bp_id, bp_product, bp_market, bp_unit,'
            . ' bp_sq_ft, bp_cond, bp_criteria, bp_price, bp_cap, bp_financing,'
            . ' bp_contingency, bp_term, bp_strategy, bp_initial, bp_counter,'
            . ' bp_second, bp_discovery, bp_final, bp_retrade, bp_justification,'
            . ' bp_extension, bp_reason, bp_hard, bp_additional, bp_accepted, bp_owner,'
            . ' bp_unit_renovation, bp_external, bp_cost, bp_property, bp_inspection,'
            . ' bp_appraisal, bp_environmental, bp_title, bp_tenant_arrangement,'
            . ' bp_tenant_information, bp_legal_procedure'
            . ' from buyer_preferences'
            . ' where bp_buyer_id=:id';

        return $this->PDO->select($sql, array('id' => $id));
    }

    public function update_prefer_text($id, $val, $col)
    {
        $sql = ' insert into buyer_preferences'
            . '(' . $col . ', bp_buyer_id)'
            . ' values(:val, :id)'
            . ' on duplicate key'
            . ' update ' . $col . '=:val';

        return $this->PDO->insert($sql, array('val' => $val, 'id' => $id));
    }

    public function update_preference($cpd_id, $deal_id)
    {
//        $query = "[$cpd_id, ";
//        $query .= implode("] $cpd_id, ", $deal_id);
////        $query .= ']';
//        echo $query;
//        exit();

//        $sql = ''
    }
}