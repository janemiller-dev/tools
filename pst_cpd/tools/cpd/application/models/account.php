<?php

namespace Model;

class Account
{

	/**
	 * Check to see if the identifier exists.  If it does, we do not need to register the user.
	 */
	public function get_accounts($user_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Check to see if the identifier is there.
		$sql = "select *"
			. " from account"
			. " where account_user_id=:user_id";
		$params['user_id'] = $user_id;
		return $PDO->select($sql, $params);
	}
}