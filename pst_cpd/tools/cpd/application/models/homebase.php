<?php

namespace Model;

use function exif_read_data;
use function export_date_options;
use function implode;
use function is_array;
use function print_r;


/**
 * Objective model class.
 */
class HomeBase
{
    private $PDO;
    private $common_model;

    /**
     * Initialises the PDO and model instance.
     */
    public function __construct()
    {
        $this->PDO = \Model\PDO_model::instance('Tools');
        $this->common_model = new Common;
    }

    /**
     * Fetches active CPD instance.
     *
     * @param $user_id    Int User ID.
     * @param $product_id Int Active Product ID.-
     * @return bool
     */
    public function get_active_cpd($user_id, $product_id)
    {
        $sql = 'select uact_cpd_id, cpd_name'
            . ' from user_active_cpd_tgd'
            . ' join cpd'
            . ' on cpd_id=uact_cpd_id'
            . ' where uact_user_id=:user_id';

        return $this->PDO->select($sql, array('user_id' => $user_id));
    }


    /**
     * Fetches the homebase client from the database.
     *
     * @param $user_id      Int User ID.
     * @param $product_id   Int Product ID.
     * @param $query
     * @param $lower_limit
     * @param $upper_limit
     * @param $id
     * @param $criteria
     * @param $status
     * @param $sort
     * @param $order
     * @param $location
     * @param $product_type
     * @param $current_page
     * @param $offset
     * @param $clu
     * @param $rwt
     * @param $data
     * @return \Exception
     */
    public function get_homebase_client
    ($user_id, $product_id, $query, $lower_limit, $upper_limit, $id, $criteria, $status, $sort,
     $order, $location, $product_type, $current_page, $offset, $rwt, $data, $tool_id, $tool_type)
    {

        try {
            $where = '';
            $filter_where = '';
            $hca_filter = '';

            // Check if the request is for specific Homebase Client.
            (0 !== $id) ? $where = ' and hc_id = ' . $id : '';

            // Check if the criteria is set.
            if ($criteria !== '') {
                $hca_filter = ' and hca_criteria_used=\'' . $criteria . '\'';
                $filter_where = ' and hca_criteria_used=\'' . $criteria . '\'';
            }

            if ($location !== '') {
                $hca_filter = ' and hca_location=\'' . $location . '\'';
                $filter_where = ' and hca_location=\'' . $location . '\'';
            }

            // Check if the status is set.
            if ($status !== '') {
                $filter_where .= ' and has_status=\'' . $status . '\'';
            }

            if ('' !== $product_type) {
                $hca_filter .= ' and hca_product_id=' . $product_type;
                $filter_where .= ' and hca_product_id=' . $product_type;
            }

            if ($sort !== '-1') {
                $filter_where .= ' order by ' . $sort . ' ' . $order;
            }

            if ('' !== $rwt) {
                $hca_filter .= ' and hca_rwt=\'' . $rwt . '\'';
                $filter_where .= ' and hca_rwt=\'' . $rwt . '\'';
            }

            if (0 !== $tool_id) {
                'TSL' === $tool_type ? $filter_where .= " and tsl_id=$tool_id" :
                    $filter_where .= " and cpd_id=$tool_id";
            }

            foreach ($data as $index => $ind_data) {
                if ('-1' !== $ind_data) {
                    $hca_filter .= ' and hca_' . $index . '=\'' . $ind_data . '\'';
                    $filter_where .= ' and hca_' . $index . '=\'' . $ind_data . '\'';
                }
            }

            // Column Names.
            $cols = "hc_user_id, hc_name, hc_main_phone, hc_mobile_phone, hc_client_email, hc_second_phone, u_product_name, "
                . " hc_entity_name, hc_product_id, hc_entity_address, hc_entity_phone, hc_entity_contact, hca_client_id,"
                . " hca_name, hca_location, hca_address, hca_product_id, upg_name, upg_short, hca_portfolio_id,"
                . " hca_criteria_used, hca_zip, has_status as hca_status, hc_id, hc_user_id, hca_id, hca_city, hca_state,"
                . " hca_price, hca_rate, hca_gross, hca_split, hca_gsplit, hca_gnet, hca_net, hca_desc, hca_cond,"
                . " assetstatus.has_status as has_status, tsl_id, tsl_name, cpd_name, cpd_id, hca_avatar,"
                . " hca_clu, hca_rwt, hca_lead, hca_gen, ta_when, hca_updated_at, hca_created_at, hca_ft, hca_unit";

            // Select Query.
            $sql = " select " . $cols
                . " from homebase_client"
                . " left join homebase_client_asset"
                . " on hca_id="
                . " ( select distinct hca_id"
                . " from homebase_client_asset"
                . " where hca_client_id=hc_id " . $hca_filter
                . " limit " . $lower_limit . ", " . $upper_limit . " )"
                . " left join ( select hs1.has_asset_id, has_status"
                . " from homebase_asset_status hs1"
                . " join (select has_asset_id, max(has_datetime) as time"
                . " from homebase_asset_status"
                . " group by has_asset_id) as hs2"
                . " on hs1.has_datetime = hs2.time and hs1.has_asset_id = hs2.has_asset_id)"
                . " as assetstatus on hca_id=assetstatus.has_asset_id"
                . " left join tsl_client"
                . " on tc_asset_id=hca_id"
                . " left join user_product"
                . " on u_product_id=hca_product_id"
                . " left join user_product_group"
                . " on upg_id=hca_product_id or upg_id=u_product_group_id"
                . " left join tsl"
                . " on tc_tsl_id=tsl_id"
                . " left join deal"
                . " on deal_asset_id=hca_id"
                . " left join cpd"
                . " on deal_cpd_id=cpd_id"
                . " left join tsl_actions"
                . " on ta_client_id=hca_id and ta_when = ("
                . " select min(ta_when) from tsl_actions"
                . " where ta_client_id=hca_id and ta_when > now())"
                . " where hc_user_id=:user_id" . $where
                . " and (hc_name like :query or hc_entity_name like :query collate utf8_general_ci"
                . " or hca_location like :query collate utf8_general_ci)" . $filter_where
//            . " order by hc_name"
                . " limit " . ($current_page - 1) * $offset . ', ' . $offset;


            $ret['client'] = $this->PDO->select($sql,
                array('user_id' => $user_id, 'query' => "%" . $query . "%"));

            $ret['current_page'] = $current_page;

            $sql = 'select count(hc_id)'
                . ' from homebase_client'
                . ' where hc_user_id = :user_id';

            $ret['last_page'] = ceil($this->PDO->select_count($sql, array('user_id' => $user_id)) / $offset);

            $sql = 'select tsl_assignee_id, tsl_assignee_name'
                . ' from tsl_assignee'
                . ' where tsl_assignee_user_id=:user_id';

            $ret['assignee'] = $this->PDO->select($sql, array('user_id' => $user_id));

        } catch (\Exception $e) {
            return $e;
        }
        return $ret;
    }

    /**
     * Adds a new client to homebase.
     *
     * @param $data Mixed Data for homebase.
     * @return bool
     * @throws \Exception
     */
    public function add_client($data)
    {
        try {
            $sql = 'insert into homebase_client'
                . ' (hc_user_id, hc_name, hc_client_email, hc_main_phone, hc_second_phone, hc_product_id,'
                . ' hc_entity_name, hc_mobile_phone)'
                . ' values(:user_id, :client_name, :client_email, :main_phone, :second_phone, :product_id,'
                . ' :entity_name, :mobile_phone)';

            $ret = $this->PDO->insert($sql, $data);
        } catch (\Exception $e) {
            throw $e;
            return false;
        }
        return $ret;
    }

    /**
     * Add a new asset for an existing client.
     *
     * @param $data Mixed Data for Asset and client.
     * @param $id
     * @param $origin
     * @param $product_id
     * @param $user_id
     * @return \Exception
     */
    public function add_asset($data, $id, $origin, $product_id, $user_id, $i, $j)
    {
        try {
            $this->PDO->begin_transaction();

            $sql = 'insert into'
                . ' homebase_client_asset(hca_client_id, hca_name, hca_location, hca_address,'
                . ' hca_criteria_used, hca_zip, hca_status, hca_lng, hca_lat, hca_product_id,'
                . ' hca_city, hca_state, hca_unit, hca_ft, hca_cond, hca_lead, hca_gen,'
                . ' hca_created_at, hca_updated_at)'
                . ' values(:client_id, :asset_name, :asset_location, :asset_address,'
                . ' :asset_criteria, :asset_zip, :asset_status, :lng, :lat, :product_id,'
                . ' :asset_city, :asset_state, :asset_units, :asset_sq_ft, :asset_cond,'
                . ' :asset_lead, :asset_source, now(), now())';

            $data = array_merge($data, array('product_id' => $product_id));

            $asset_id = $this->PDO->insert($sql, $data);

            $sql = 'insert into'
                . ' homebase_asset_status'
                . ' (has_asset_id, has_status, has_md, has_datetime)'
                . ' values(:asset_id, :status, month(now()), now())';

            $ret['status'] = $this->PDO->insert($sql, array('asset_id' => $asset_id, 'status' => $data['asset_status']));

            // Check if the origin for request is TSL/CPD.
            if (('' !== $data['asset_status'] && null !== $data['asset_status']) && ('tsl' === $origin ||
                    ('cpdv1' === $origin && '0' !== $data['asset_status']) || ('0' !== $data['asset_status']))) {
                $ret['client_pushed'] = $this->push_client($asset_id, $data['asset_status'],
                    $id, $product_id, $user_id, '', '');
            }
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return $e;
        }
        return $ret;
    }

    /**
     * Select the clients that have same name as of existing client
     *
     * @param $client       Mixed Client List.
     * @param $user_id      Int   User Id.
     * @param $product_id   Int   Product Id.
     * @return bool
     */
    public function get_existing_client($client, $user_id, $product_id)
    {
        $sql = 'select hc_name'
            . ' from homebase_client'
            . ' where hc_user_id=:user_id and hc_product_id=:product_id'
            . ' and hc_name in (' . ("'" . implode("','", $client) . "'") . ')';

        return $this->PDO->select($sql, array('user_id' => $user_id, 'product_id' => $product_id));
    }

    /**
     * Fetch TSL instaces.
     *
     * @param $user_id      Int User ID.
     * @param $product_id   Int Product ID.
     * @return bool
     */
    public function search_tsl_instances($user_id, $product_id)
    {
        $sql = 'select tsl_id, tsl_name'
            . ' from tsl'
            . ' where tsl_user_id=:user_id and tsl_product_id=:product_id';

        return $this->PDO->select($sql, array('user_id' => $user_id, 'product_id' => $product_id));
    }

    /**
     * Update Asset table fields value.
     *
     * @param $id       Int     Asset ID
     * @param $col_name String  Column Name.
     * @param $value    String  Value
     * @return bool
     */
    public function update_asset_field($id, $col_name, $value, $lat, $long)
    {
        try {
            $this->PDO->begin_transaction();

            // Check if status column is to be updated
            if ('hca_status' === $col_name) {
                $sql = 'insert into'
                    . ' homebase_asset_status'
                    . ' (has_asset_id, has_status, has_md, has_datetime)'
                    . ' values(:asset_id, :status, month(now()), now())';

                $this->PDO->insert($sql, array('asset_id' => $id, 'status' => $value));
            } // Check if the column to be updated is address
            else if ('hca_address' === $col_name) {
                $sql = 'update homebase_client_asset'
                    . ' set hca_lat=:lat, hca_lng=:lng, hca_updated_at=now()'
                    . ' where hca_id=:id';

                $this->PDO->update($sql, array('lat' => $lat, 'lng' => $long, 'id' => $id));
            }

            $sql = 'update homebase_client_asset'
                . ' set ' . $col_name . '=:val, hca_updated_at=now()'
                . ' where hca_id=:id';

            $asset_updated = $this->PDO->update($sql, array('val' => $value, 'id' => $id));
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return $e;
        }
        return $asset_updated;
    }

    /**
     * Push a client to TSL/CPD.
     *
     * @param $asset_id Int     Homebase Asset ID.
     * @param $status   String  Deal Status (TSL/CPD Status)
     * @param $tsl_id   Int     TSL ID.
     * @param $product_id
     * @param $user_id
     * @param $type
     * @param $asset_type
     * @return bool
     * @throws \Exception
     */
    public function push_client($asset_id, $status, $tsl_id, $product_id, $user_id, $type, $asset_type)
    {
        try {

            // Check if portfolio is pushed.
            if ($type === 'portfolio') {
                $sql = 'insert into'
                    . ' homebase_asset_status'
                    . ' (has_asset_id, has_status, has_md, has_datetime)'
                    . ' values(:asset_id, :status, month(now()), now())';

                $this->PDO->insert($sql, array('asset_id' => $asset_id, 'status' => $status));
            }

            // Check if the status is 0 (TSL status)
            if ($status === '0') {

                $sql = 'insert into'
                    . ' tsl_client'
                    . ' (tc_asset_id, tc_tsl_id, tc_type)'
                    . ' values(:asset_id, :tsl_id, :type)';

                $client_id = $this->PDO->insert($sql,
                    array('asset_id' => $asset_id, 'tsl_id' => $tsl_id, 'type' => $asset_type));

                // Create a record for client, where we can store additional info.
                $sql = ' insert into'
                    . ' tsl_about_client'
                    . ' (tac_tc_id)'
                    . ' values(:client_id)';

                $this->PDO->insert($sql, array('client_id' => $client_id));

            } else {
                //Fetch active CPD Id for selected Product and User.
                $sql = 'select uact_cpd_id'
                    . ' from user_active_cpd_tgd'
                    . ' where uact_user_id=:user_id';

                $active_cpd = $this->PDO->select($sql, array('user_id' => $user_id))[0]->uact_cpd_id;

                // Insert record to Deal Table.
                $sql = 'insert into'
                    . ' deal'
                    . '(deal_cpd_id, deal_asset_id, deal_created, deal_updated, deal_type)'
                    . ' values(:cpd_id, :asset_id, now(), now(), :type)'
                    . ' on duplicate key'
                    . ' update deal_cpd_id=:cpd_id';
                $deal_id = $this->PDO->insert($sql, array('cpd_id' => $active_cpd, 'asset_id' => $asset_id, 'type' => $type));

                // Check if deal id is present.
                if ($deal_id == 0) {
                    $sql = 'select deal_id'
                        . ' from deal'
                        . ' where deal_cpd_id=:cpd_id and deal_asset_id=:asset_id and deal_type=:type';

                    $deal_id = $this->PDO->select($sql, array('cpd_id' => $active_cpd,
                        'asset_id' => $asset_id, 'type' => $type))[0]->deal_id;
                }

                // Insert Status for deal.
                $status_sql = "insert into"
                    . " deal_status (ds_deal_id, ds_status, ds_md, ds_datetime)"
                    . " values (:deal_id, :status, month(now()), now())";

                $ret = $this->PDO->insert($status_sql,
                    array("deal_id" => $deal_id, "status" => $status));
            }
        } catch (\Exception $e) {
            throw $e;
            return false;
        }
        return true;
    }

    public function update_homebase_field($id, $col, $val)
    {
        $sql = 'update homebase_client'
            . ' set ' . $col . '=:val'
            . ' where hc_id=:id';

        return $this->PDO->update($sql, array('val' => $val, 'id' => $id));
    }

    /**
     * Checks whether the asset is used by any tool.
     *
     * @param $id   Int Asset Id.
     * @return bool
     */
    public function is_asset_used($id)
    {
        is_array($id) ? $id = implode(", ", $id) : '';

        $sql = 'select deal_id, tc_id, table_name'
            . ' from ('
            . ' (select deal_id, null as tc_id, "deal" as table_name'
            . ' from deal'
            . ' where deal_asset_id in (' . $id . '))'
            . ' union all'
            . ' (select null as deal_id, tc_id, "tsl_client" as table_name'
            . ' from tsl_client'
            . ' where tc_asset_id in (' . $id . '))'
            . ' ) as asset_count';

        return $this->PDO->select($sql, array());
    }

    /**
     * Removes any previous instances of asset used.
     *
     * @param $tc_id        Int     Client Id.
     * @param $deal_id      Int     Deal Id.
     * @param $table_name   String  Table Name.
     * @return bool
     */
    public function delete_prev_instance($tc_id, $deal_id, $table_name)
    {
        if (!empty($tc_id)) {
            $where = ' where tc_id=' . $tc_id;
        } else {
            $where = ' where deal_id=' . $deal_id;
        }

        $sql = 'delete from ' . $table_name . $where;

        $this->PDO->delete($sql, array());
        return true;
    }

    /**
     * Update Amount fields for asset.
     *
     * @param $id    Int      Asset ID.
     * @param $col   String   Column Name.
     * @param $val   Int      Value.
     * @param $table String   Table Name
     * @param $pk    String   Primary Key for Table.
     * @return bool
     */
    public function update_amount($id, $col, $val, $table, $pk)
    {
        $sql = 'update ' . $table
            . ' set ' . $col . '=:val'
            . ' where ' . $pk . '=:id';

        return $this->PDO->update($sql, array('val' => $val, 'id' => $id));
    }

    /**
     * Generates a copy of asset with new status.
     *
     * @param $asset_id Int     Asset ID.
     * @param $status   String  New Status for Asset.
     * @return bool
     */
    public function copy_asset($asset_id, $status)
    {
        $col = 'hca_client_id, hca_name, hca_location, hca_address, hca_criteria_used, '
            . 'hca_contact, hca_lng, hca_lat, hca_price, hca_rate, hca_gross,'
            . ' hca_split, hca_gsplit, hca_gnet, hca_net, hca_product_id';

        $sql = 'insert into'
            . ' homebase_client_asset'
            . '(' . $col . ', hca_status, hca_created_at, hca_updated_at)'
            . ' select ' . $col . ', \'' . $status . '\', now(), now() from homebase_client_asset'
            . ' where hca_id=:id';

        $copied_asset_id = $this->PDO->insert($sql, array('id' => $asset_id));

        $sql = 'insert into'
            . ' homebase_asset_status'
            . ' (has_asset_id, has_status, has_md, has_datetime)'
            . ' values(:asset_id, :status, month(now()), now())';

        return $this->PDO->insert($sql, array('asset_id' => $copied_asset_id, 'status' => $status));
    }

    /**
     * Fetches additional rows.
     *
     * @param $user_id      Int    User ID.
     * @param $product_id   Int    Product ID.
     * @param $query        String Query to filter out results.
     * @param $lower_limit  Int    Lower Limit.
     * @param $upper_limit  Int    Upper Limit.
     * @param $id           Int    Client ID.
     * @return bool
     */
    public function fetch_more_row($user_id, $product_id, $query, $lower_limit, $upper_limit, $id)
    {
        $where = '';

        // Check if the request is for specific Homebase Client.
        if (0 !== $id) {
            $where = ' and hc_id = ' . $id;
        }

        // Column Names.
        $cols = 'hc_user_id, hc_name, hc_main_phone, hc_mobile_phone, hc_client_email, u_product_name, hca_state,'
            . ' hc_product_id,hca_client_id, hca_name, hca_location, hca_address, hca_product_id, hca_portfolio_id,'
            . ' hca_criteria_used, has_status as hca_status, hc_id, hc_user_id, hca_id,cpd_id, cpd_name, hca_city,'
            . ' hca_price, hca_rate, hca_gross, hca_split, hca_gsplit, hca_gnet, hca_net, tsl_id, tsl_name,'
            . ' hca_zip, hca_desc, hca_cond, hca_clu, hca_rwt, hca_lead, hca_gen, hca_created_at, hca_updated_at,'
            . ' hca_ft, hca_unit, hca_avatar';

        // Select Query.
        $sql = " select " . $cols
            . " from homebase_client"
            . " join homebase_client_asset"
            . " on hca_client_id=hc_id"
            . " left join user_product"
            . " on u_product_id=hca_product_id"
            . " join ( select hs1.has_asset_id, has_status"
            . " from homebase_asset_status hs1"
            . " join (select has_asset_id, max(has_datetime) as time"
            . " from homebase_asset_status"
            . " group by has_asset_id) as hs2"
            . " on hs1.has_datetime = hs2.time and hs1.has_asset_id = hs2.has_asset_id)"
            . " as assetstatus on hca_id=assetstatus.has_asset_id"
            . " left join tsl_client"
            . " on tc_asset_id=hca_id"
            . " left join tsl"
            . " on tc_tsl_id=tsl_id"
            . " left join deal"
            . " on deal_asset_id=hca_id"
            . " left join cpd"
            . " on deal_cpd_id=cpd_id"
            . " where hc_user_id=:user_id " . $where
            . " and (hc_name like :query or hca_location like :query)"
            . " order by hc_name"
            . " limit " . $lower_limit . ", " . $upper_limit;

        return $this->PDO->select($sql,
            array('user_id' => $user_id, 'product_id' => $product_id, 'query' => "%" . $query . "%"));
    }

    /**
     * Adds a new asset for the client.
     *
     * @param $client_id Int Client ID.
     * @return bool
     */
    public function add_new_asset($client_id)
    {
        try {
            $this->PDO->begin_transaction();

            // Create a new asset.
            $sql = 'insert'
                . ' into homebase_client_asset'
                . ' (hca_client_id, hca_status, hca_created_at, hca_updated_at)'
                . ' values(:client_id, 0, now(), now())';

            $asset_id = $this->PDO->insert($sql, array('client_id' => $client_id));

            // Create a record for asset status.
            $sql = 'insert'
                . ' into homebase_asset_status'
                . ' (has_asset_id, has_status, has_md, has_datetime)'
                . ' values(:asset_id, :status, month(now()), now())';

            $this->PDO->insert($sql, array('asset_id' => $asset_id, 'status' => 0));
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }
        return $asset_id;
    }

    /**
     * Deletes Asset/Client.
     *
     * @param $id   Int    Asset/Client ID.
     * @param $type String Type of Column.
     * @param $key  String Primary key for Table.
     * @return bool
     */
    public function delete_asset_client($id, $type, $key)
    {
        $sql = 'delete'
            . ' from homebase_' . $type
            . ' where ' . $key . '_id in ( ' . $id . ' )';

        return $this->PDO->delete($sql, array());
    }

    public function add_portfolio($user_id, $portfolio_name)
    {
        $sql = 'insert into user_portfolio'
            . ' (up_name, up_user_id)'
            . ' values(:name, :user_id)';

        return $this->PDO->insert($sql, array('name' => $portfolio_name, 'user_id' => $user_id));
    }

    public function get_portfolio($user_id)
    {
        $sql = 'select up_id, up_name'
            . ' from user_portfolio'
            . ' where up_user_id=:user_id';

        return $this->PDO->select($sql, array('user_id' => $user_id));
    }

    public function delete_portfolio($id)
    {
        $sql = 'delete from user_portfolio'
            . ' where up_id=:id';

        return $this->PDO->delete($sql, array('id' => $id));
    }

    public function use_portfolio($asset_id, $portfolio_id)
    {
        $sql = 'update homebase_client_asset'
            . ' set hca_portfolio_id=:portfolio_id, hca_updated_at=now()'
            . ' where hca_id=:asset_id';

        return $this->PDO->update($sql,
            array('portfolio_id' => (0 < $portfolio_id ? $portfolio_id : null), 'asset_id' => $asset_id));
    }

    public function get_portfolio_clients($user_id)
    {
        // Column Names.
        $cols = "hc_user_id, hc_name, hc_main_phone, hc_mobile_phone, hc_client_email, hc_second_phone, u_product_name, "
            . " hc_entity_name, hc_product_id, hc_entity_address, hc_entity_phone, hc_entity_contact, hca_client_id,"
            . " hca_name, hca_location, hca_address, hca_product_id, upg_name, upg_short, hca_portfolio_id,"
            . " hca_criteria_used, hca_zip, has_status as hca_status, hc_id, hc_user_id, hca_id, hca_city, hca_state,"
            . " hca_price, hca_rate, hca_gross, hca_split, hca_gsplit, hca_gnet, hca_net, hca_desc, hca_cond,"
            . " assetstatus.has_status as has_status, tsl_id, tsl_name, cpd_name, cpd_id, hca_avatar,"
            . " hca_clu, hca_rwt, hca_lead, hca_gen, hca_created_at, hca_updated_at, hca_ft, hca_unit";

        // Select Query.
        $sql = " select " . $cols
            . " from homebase_client"
            . " left join homebase_client_asset"
            . " on hca_id="
            . " ( select distinct hca_id"
            . " from homebase_client_asset"
            . " where hca_client_id=hc_id "
            . " limit 0, 1)"
            . " left join ( select hs1.has_asset_id, has_status"
            . " from homebase_asset_status hs1"
            . " join (select has_asset_id, max(has_datetime) as time"
            . " from homebase_asset_status"
            . " group by has_asset_id) as hs2"
            . " on hs1.has_datetime = hs2.time and hs1.has_asset_id = hs2.has_asset_id)"
            . " as assetstatus on hca_id=assetstatus.has_asset_id"
            . " left join tsl_client"
            . " on tc_asset_id=hca_id"
            . " left join user_product"
            . " on u_product_id=hca_product_id"
            . " left join user_product_group"
            . " on upg_id=hca_product_id or upg_id=u_product_group_id"
            . " left join tsl"
            . " on tc_tsl_id=tsl_id"
            . " left join deal"
            . " on deal_asset_id=hca_id"
            . " left join cpd"
            . " on deal_cpd_id=cpd_id"
            . " join user_portfolio"
            . " on up_id=hca_portfolio_id"
            . " where up_user_id=:user_id"
            . " order by hc_name";

        return $this->PDO->select($sql, array('user_id' => $user_id));
    }

    public function get_all_portfolio_assets($id)
    {
        $sql = ' select hca_id'
            . ' from homebase_client_asset'
            . ' where hca_portfolio_id=:id';

        return $this->PDO->select($sql, array('id' => $id));
    }

    public function get_lead_source($user_id)
    {
        $sql = 'select dl_id, dl_name'
            . ' from deal_lead'
            . ' where dl_user_id=:user_id and dl_type=\'lead\'';

        $ret['lead'] = $this->PDO->select($sql, array('user_id' => $user_id));

        $sql = 'select dl_id, dl_name'
            . ' from deal_lead'
            . ' where dl_user_id=:user_id and dl_type=\'source\'';

        $ret['source'] = $this->PDO->select($sql, array('user_id' => $user_id));

        return $ret;
    }

    public function get_asset_info($user_id)
    {
        $sql = 'select hai_id, hai_value'
            . ' from homebase_asset_info'
            . ' where hai_user_id=:user_id and hai_type=:type';

        $ret['cond'] = $this->PDO->select($sql, array('user_id' => $user_id, 'type' => 'cond'));

        $sql = 'select dl_id as hai_id, dl_name as hai_value'
            . ' from deal_lead'
            . ' where dl_user_id=:user_id and dl_type=\'lead\'';

        $ret['lead'] = $this->PDO->select($sql, array('user_id' => $user_id));

        $sql = 'select dl_id as hai_id, dl_name as hai_value'
            . ' from deal_lead'
            . ' where dl_user_id=:user_id and dl_type=\'source\'';

        $ret['source'] = $this->PDO->select($sql, array('user_id' => $user_id));

        return $ret;
    }

    public
    function get_asset_type_info($user_id, $type)
    {
        $sql = 'select hai_id, hai_value, hai_type'
            . ' from homebase_asset_info'
            . ' where hai_user_id=:user_id and hai_type=:type';

        return $this->PDO->select($sql, array('user_id' => $user_id, 'type' => $type));
    }

    public
    function add_asset_info($user_id, $type, $val)
    {
        $sql = 'insert into homebase_asset_info'
            . ' (hai_type, hai_user_id, hai_value)'
            . ' values(:type, :user_id, :val)';

        return $this->PDO->insert($sql, array('type' => $type, 'user_id' => $user_id, 'val' => $val));
    }

    public
    function update_asset_info($id, $value)
    {
        $sql = ' update homebase_asset_info'
            . ' set hai_value=:value'
            . ' where hai_id=:id';

        return $this->PDO->update($sql, array('id' => $id, 'value' => $value));
    }

    public
    function get_owner_info($owner_id)
    {
        $sql = 'select hc_id, hc_name, hc_client_email, hc_social, hc_website, hc_messaging, hc_title,'
            . ' hc_main_phone, hc_second_phone, hc_entity_name, hc_entity_address, hc_avatar'
            . ' from homebase_client'
            . ' where hc_id=:owner_id';

        return $this->PDO->select($sql, array('owner_id' => $owner_id));
    }

    public
    function save_owner_info($data)
    {
        $cols = '';

        // Prepare list of columns.
        foreach (($data->getArray($_POST)) as $key => $value) {
            $cols .= $key . '=:' . $key . ', ';
        }

        $cols = rtrim($cols, ', ');

        $sql = 'update homebase_client'
            . ' set ' . $cols
            . ' where hc_id=:hc_id';

        return $this->PDO->update($sql, $data->getArray($_POST));
    }

    public
    function get_owner_image($owner_id)
    {
        $sql = 'select hc_avatar'
            . ' from homebase_client'
            . ' where hc_id=:owner_id';

        return $this->PDO->select($sql, array('owner_id' => $owner_id));
    }

    public function upload_owner_image($owner_id, $random)
    {
        $sql = 'update homebase_client'
            . ' set hc_avatar=:avatar'
            . ' where hc_id=:hc_id';

        return $this->PDO->update($sql, array('avatar' => $random, 'hc_id' => $owner_id));
    }

    /**
     * Saves/Update owner popup info.
     *
     * @param $owner_id Int     Owner Id
     * @param $val      String  Value
     * @param $type     String  Type of column
     * @return bool
     */
    public
    function save_owner_popup_info($owner_id, $val, $type)
    {
        $sql = 'insert into homebase_owner_info'
            . ' (hoi_client_id, hoi_type, hoi_val)'
            . ' values(:client_id, :type, :val)'
            . ' on duplicate key update hoi_val=:val';

        return $this->PDO->insert($sql, array('client_id' => $owner_id, 'type' => $type, 'val' => $val));
    }

    /**
     * Fetches the owner info from DB.
     *
     * @param $owner_id Int Owner Id.
     * @return bool
     */
    public
    function get_owner_popup_info($owner_id)
    {
        $sql = 'select hoi_client_id, hoi_type, hoi_val'
            . ' from homebase_owner_info'
            . ' where hoi_client_id=:client_id';

        return $this->PDO->select($sql, array('client_id' => $owner_id));
    }

    public
    function get_asset_data($asset_id)
    {
        // Column Names.
        $cols = 'hc_user_id, hc_name, hc_main_phone, hc_mobile_phone, hc_client_email, u_product_name, hca_state,'
            . ' hc_product_id,hca_client_id, hca_name, hca_location, hca_address, hca_product_id, hca_portfolio_id,'
            . ' hca_criteria_used, has_status as hca_status, hc_id, hc_user_id, hca_id,cpd_id, cpd_name, hca_city,'
            . ' hca_price, hca_rate, hca_gross, hca_split, hca_gsplit, hca_gnet, hca_net, tsl_id, tsl_name,'
            . ' hca_zip, hca_desc, hca_cond, hca_clu, hca_rwt, hca_lead, hca_gen, hca_updated_at, hca_created_at,'
            . ' hca_ft, hca_unit';

        // Select Query.
        $sql = " select " . $cols
            . " from homebase_client"
            . " join homebase_client_asset"
            . " on hca_client_id=hc_id"
            . " left join user_product"
            . " on u_product_id=hca_product_id"
            . " join ( select hs1.has_asset_id, has_status"
            . " from homebase_asset_status hs1"
            . " join (select has_asset_id, max(has_datetime) as time"
            . " from homebase_asset_status"
            . " group by has_asset_id) as hs2"
            . " on hs1.has_datetime = hs2.time and hs1.has_asset_id = hs2.has_asset_id)"
            . " as assetstatus on hca_id=assetstatus.has_asset_id"
            . " left join tsl_client"
            . " on tc_asset_id=hca_id"
            . " left join tsl"
            . " on tc_tsl_id=tsl_id"
            . " left join deal"
            . " on deal_asset_id=hca_id"
            . " left join cpd"
            . " on deal_cpd_id=cpd_id"
            . " where hca_id=:id";

        return $this->PDO->select($sql,
            array('id' => $asset_id));
    }

    public function delete_asset_info($id)
    {
        $sql = 'delete from homebase_asset_info'
            . ' where hai_id=:id';

        return $this->PDO->delete($sql, array('id' => $id));
    }

    function get_location_info($user_id)
    {
        $ret = [];
        $location_array = ['zip', 'city', 'state', 'unit', 'ft'];

        foreach ($location_array as $item) {
            $sql = ' select distinct hca_' . $item
                . ' from homebase_client_asset'
                . ' join homebase_client'
                . ' on hc_id=hca_client_id'
                . ' WHERE hc_user_id=:user_id'
                . ' and hca_' . $item . ' is not null';

            $ret[$item] = $this->PDO->select($sql, array('user_id' => $user_id));
        }

        return $ret;
    }

    function get_homebase_tool($user_id)
    {

        $sql = 'select distinct tsl_id as tool_id, tsl_name as tool_name'
            . ' from homebase_client'
            . ' join homebase_client_asset'
            . ' on hca_client_id=hc_id'
            . ' left join tsl_client'
            . ' on tc_asset_id=hca_id'
            . ' left join tsl'
            . ' on tc_tsl_id=tsl_id'
            . ' where hc_user_id=:user_id and tsl_id is not null';

        $ret['TSL'] = $this->PDO->select($sql, array('user_id' => $user_id));

        $sql = 'select distinct cpd_id as tool_id, cpd_name as tool_name'
            . ' from homebase_client'
            . ' join homebase_client_asset'
            . ' on hca_client_id=hc_id'
            . ' left join deal'
            . ' on deal_asset_id=hca_id'
            . ' left join cpd'
            . ' on deal_cpd_id=cpd_id'
            . ' where hc_user_id=:user_id and cpd_id is not null';

        $ret['DMD'] = $this->PDO->select($sql, array('user_id' => $user_id));

        return $ret;
    }

    function upload_asset_image($asset_id, $image)
    {

        $sql = 'update homebase_client_asset'
            . ' set hca_avatar=:avatar'
            . ' where hca_id=:asset_id';

        return $this->PDO->update($sql, array('avatar' => $image, 'asset_id' => $asset_id));
    }

    function get_asset_image($asset_id)
    {
        $sql = 'select hca_avatar'
            . ' from homebase_client_asset'
            . ' where hca_id=:asset_id';

        return $this->PDO->select($sql, array('asset_id' => $asset_id));
    }

    function add_new_asset_info($val, $user_id, $type, $product_id)
    {
        if ($type === 'location') {
            $sql = 'insert into user_location'
                . ' (ul_name, ul_user_id, ul_product_id)'
                . ' values(:name, :user_id, :product_id)';
            $ret = $this->PDO->insert($sql, array('name' => $val, 'user_id' => $user_id, 'product_id' => $product_id));
        } else if ($type === 'condition') {
            $sql = 'insert into homebase_asset_info '
                . ' (hai_value, hai_type, hai_user_id)'
                . ' values(:val, :type, :user_id)';

            $ret = $this->PDO->insert($sql, array('user_id' => $user_id, 'type' => 'cond', 'val' => $val));
        } else if ($type === 'product') {
            $sql = 'insert into user_product'
                . ' (u_product_name, u_product_user_id)'
                . ' values (:product, :user_id)'
                . ' on duplicate key update u_product_name=:product';

            $ret = $this->PDO->insert($sql, array('product' => $val, 'user_id' => $user_id));
        } else {
            $sql = ' insert into user_criteria'
                . ' (uc_user_id, uc_name, uc_product_id)'
                . 'values(:user_id, :name, :product_id)';

            $ret = $this->PDO->insert($sql, array('user_id' => $user_id, 'name' => $val, 'product_id' => $product_id));
        }
        return $ret;
    }

    function add_buyer($data)
    {
        try {
            $sql = 'insert into homebase_buyer'
                . ' (hb_user_id, hb_name, hb_email, hb_main_phone, hb_second_phone,'
                . ' hb_mobile_phone, hb_entity_name)'
                . ' values(:user_id, :buyer_name, :buyer_email, :main_phone, :second_phone, :mobile_phone, '
                . ' :entity_name)';

            $ret = $this->PDO->insert($sql, $data);
        } catch (\Exception $e) {
            throw $e;
            return false;
        }
        return $ret;
    }

    public function add_buyer_asset($data, $id, $origin, $product_id, $user_id, $i, $j)
    {
        try {
            $sql = 'insert into'
                . ' homebase_buyer_asset(hba_hb_id, hba_product_type,  hba_entity_name,'
                . ' hba_entity_address, hba_city, hba_state, hba_zip, hba_market_area,'
                . ' hba_units, hba_size, hba_condition, hba_criteria)'
                . ' values(:buyer_id, :product_id, :asset_name, :asset_address, :asset_city, :asset_state, :asset_zip,'
                . ' :asset_location, :asset_units, :asset_sq_ft, :asset_cond, :asset_criteria)';

            $data = array_merge($data, array('product_id' => $product_id));
            $ret = $this->PDO->insert($sql, $data);
        } catch (\Exception $e) {
            return $e;
        }
        return $ret;
    }

    public function fetch_buyer_data($user_id)
    {
        $sql = 'select hb_id, hb_name, hb_name, hb_main_phone,'
            . ' hb_second_phone, hb_mobile_phone, hb_email, hb_entity_name, hb_rwt,'
            . ' hba_id, hba_product_type, hba_acquisition, hba_entity_name, hba_entity_address,'
            . ' hba_city, hba_state, hba_zip, hba_buyer_type, hba_market_area, hba_units, hba_size,'
            . ' hba_condition, hba_criteria, hba_price_range, hba_cap_rate, hba_financing,'
            . ' hba_contingency, hba_term, hba_acquisition_strategy, hba_avatar, hba_status'
            . ' from homebase_buyer'
            . ' join homebase_buyer_asset'
            . ' on hba_hb_id=hb_id'
            . ' where hb_user_id=:user_id';

        return $this->PDO->select($sql, array('user_id' => $user_id));
    }

    public function fetch_buyer_row($id)
    {
        $sql = 'select hba_hb_id as hb_id, hba_id, hba_product_type, hba_acquisition, hba_entity_name, hba_entity_address,'
            . ' hba_city, hba_state, hba_zip, hba_buyer_type, hba_market_area, hba_units, hba_size,'
            . ' hba_condition, hba_criteria, hba_price_range, hba_cap_rate, hba_financing,'
            . ' hba_contingency, hba_term, hba_acquisition_strategy, hba_avatar, hba_status'
            . ' from homebase_buyer_asset'
            . ' where hba_hb_id=:id';

        return $this->PDO->select($sql, array('id' => $id));
    }

    public function update_buyer_asset_data($id, $name, $val)
    {
        $sql = 'update homebase_buyer_asset'
            . ' set ' . $name . '=:val'
            . ' where hba_id=:id';

        return $this->PDO->update($sql, array('val' => $val, 'id' => $id));
    }

    public function update_buyer_data($val, $id, $col)
    {
        $sql = ' update homebase_buyer'
            . ' set ' . $col . '=:val'
            . ' where hb_id=:id';

        return $this->PDO->update($sql, array('val' => $val, 'id' => $id));
    }

    public function get_owner_asset_info($owner_id, $qtr, $year)
    {
        try {
            $sql = 'select hca_id, assetstatus.has_status as ds_status, hca_avatar, aai_list_price, hca_address,'
                . ' aai_built_year, aai_units, aai_price_per_unit, aai_price_per_sf, aai_avg_sf, hca_lat, hca_lng'
                . ' from deal join homebase_client_asset on hca_id=deal_asset_id'
                . ' join homebase_client on hca_client_id=hc_id'
                . ' join (select has_status, hs1.has_datetime, has_id, has_asset_id, has_md'
                . ' from homebase_asset_status hs1 join'
                . ' (select max(has_datetime) as time, has_asset_id as asset_id'
                . ' from homebase_asset_status where ((quarter(has_datetime)<=:quarter && year(has_datetime)=:year)'
                . ' || (year(has_datetime)<:year)) group by has_asset_id) as hs2'
                . ' on hs1.has_datetime = hs2.time and hs1.has_asset_id = asset_id) as assetstatus'
                . ' on hca_id=assetstatus.has_asset_id join code on code_abbrev=assetstatus.has_status'
                . ' join code_type on code_type_id=ct_id and (ct_abbrev=\'gds\''
                . ' OR ct_abbrev=\'cds\' OR ct_abbrev=\'str_ds\' OR ct_abbrev=\'bds\' OR ct_abbrev=\'rds\')'
                . ' left join agent_asset_info'
                . ' on aai_hca_id = hca_id'
                . ' where hc_id=:owner_id';

            $ret = $this->PDO->select($sql, array('owner_id' => $owner_id, 'year' => $year, 'quarter' => $qtr));
        } catch (\Exception $e) {
            echo $e;
            exit();
        }
        return $ret;
    }

    public function update_revenue($id, $col, $type, $val)
    {
        try {
            $sql = 'insert into homebase_asset_revenue'
                . ' (har_asset_id, har_type, ' . $col . ')'
                . ' values(:asset_id, :type, :val)'
                . ' on duplicate key'
                . ' update ' . $col . '=:val';

            return $this->PDO->insert($sql, array('asset_id' => $id, 'type' => $type, 'val' => $val));
        } catch (\Exception $e) {
            echo $e;
            exit();
        }
    }

    public function get_comp_image($comp_id)
    {
        $sql = 'select ac_avatar'
            . ' from agent_comps'
            . ' where ac_id=:id';

        return $this->PDO->select($sql, array('id' => $comp_id));
    }

    public function upload_comp_image($comp_id, $file)
    {
        $sql = 'update agent_comps'
            . ' set ac_avatar=:avatar'
            . ' where ac_id=:id';

        return $this->PDO->update($sql, array('avatar' => $file, 'id' => $comp_id));
    }
}