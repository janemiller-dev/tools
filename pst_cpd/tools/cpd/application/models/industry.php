<?php

namespace Model;

use function print_r;

class Industry
{
    private $PDO;
    private $common_model;


    /**
     * Initialises the PDO instance.
     */
    public function __construct()
    {
        $this->PDO = \Model\PDO_model::instance('Tools');
    }


    /**
     * Get the list of TGD game types
     */
    public function get_industries()
    {

        // Find the list of tool groups available to the user.
        $sql = "select industry_id, industry_name, industry_description"
            . " from industry"
            . " order by industry_name";
        return $this->PDO->select($sql, array());
    }

    /**
     * Get the specified industry
     *
     * @param $id int id of the industry
     * @return object
     * @throws \Exception
     */
    public function get_industry($id)
    {

        // Find the game types
        $sql = "select industry_id, industry_name, industry_description"
            . " from industry"
            . " where industry_id=:id";
        $gt = $this->PDO->select_row($sql, array('id' => $id));
        if (empty($gt)) {
            throw new \Exception("Industry not found");
        }
        // Get the industry professions.
        $sql = "select profession_id, profession_name, profession_description"
            . " from industry_profession"
            . " where profession_industry_id=:id"
            . " order by profession_id";
        $gt->profession = $this->PDO->select($sql, array('id' => $id));

        return $gt;
    }

    /**
     * Add a new industry.
     *
     * @param $name string name of the industry
     * @param $description string description about the industry
     * @return bool object
     */
    public function add_industry($user_id, $name, $description)
    {

        // This is a multi-step process, so make sure they all work.
        try {
            $this->PDO->begin_transaction();

            // Create the industry record.
            $sql = "insert into industry (industry_name, industry_description)"
                . " values (:name, :description)";
            $params = array('name' => $name, 'description' => $description);
            $industry_id = $this->PDO->insert($sql, $params);
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }

        // Commit the transation and return the TGD
        $this->PDO->commit_transaction();
        return $industry_id;
    }

    /**
     * Copy an existing industry to a new industry.
     *
     * @param $old_gt_id
     * @param $save_as_name
     * @param $save_as_id
     * @return bool
     * @throws \Exception
     */
    public function copy_industry($old_gt_id, $save_as_name, $save_as_id)
    {

        // This is a multi-step process, so make sure they all work.
        try {
            $this->PDO->begin_transaction();

            // If the ID is set and the name is not set, replace the existing industry.
            if (empty($save_as_name) && !empty($save_as_id)) {

                // This is only valid if the existing industry does not have any attached instances.
                $sql = "select count(*) from tgd_user_instance where tgd_ui_profession_id=:save_as_id";
                $count = $this->PDO->select_count($sql, array('save_as_id' => $save_as_id));
                if ($count !== "0") {
                    throw new \Exception("Industry type can not be overwritten because there are existing user instances of the industry type.");
                }
                // Delete the existing industry. It will be re-created below.
                $sql = "select industry_name from industry where industry_id=:save_as_id";
                $gt = $this->PDO->select_row($sql, array('save_as_id' => $save_as_id));
                $save_as_name = $gt->industry_name;
                $sql = "delete from industry where industry_id=:save_as_id";
                $this->PDO->delete($sql, array('save_as_id' => $save_as_id));
            }

            // Create the new industry record as a copy.
            $sql = "insert into industry (industry_name, industry_description)"
                . " select :name, industry_description"
                . " from industry where industry_id=:gt_id";
            $new_gt_id = $this->PDO->insert($sql, array('name' => $save_as_name, 'gt_id' => $old_gt_id));

            // Create the new industry's conditions
            $sql = "insert into profession_condition (profession_c_p_id, profession_c_name, profession_c_description, profession_c_tooltip, profession_c_type, profession_c_uom, profession_c_prev_condition)"
                . " select :new_gt_id, profession_c_name, profession_c_description, profession_c_tooltip, profession_c_type, profession_c_uom, profession_c_prev_condition"
                . " from profession_condition where profession_c_p_id=:gt_id";
            $this->PDO->insert($sql, array('new_gt_id' => $new_gt_id, 'gt_id' => $old_gt_id));

            // Create the new industry's multipliers
            $sql = "insert into profession_multiplier (profession_m_p_id, profession_m_order, profession_m_name, profession_m_description, profession_m_type, profession_m_operation, profession_m_propagate)"
                . " select :new_gt_id, profession_m_order, profession_m_name, profession_m_description, profession_m_type, profession_m_operation, profession_m_propagate"
                . " from profession_multiplier where profession_m_p_id=:gt_id";
            $this->PDO->insert($sql, array('new_gt_id' => $new_gt_id, 'gt_id' => $old_gt_id));
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            throw $e;
        }

        // Commit the transation and return the TGD
        $this->PDO->commit_transaction();
        return $new_gt_id;
    }

    /**
     * Delete an industry.
     *
     * @param $gt_id int profession id
     * @return bool object
     * @throws \Exception
     */
    public function delete_industry($gt_id)
    {

        // This is only valid if the existing industry does not have any attached instances.
        $sql = "select count(*) from tgd_user_instance where tgd_ui_profession_id=:gt_id";
        $count = $this->PDO->select_count($sql, array('gt_id' => $gt_id));

        if ($count !== "0") {
            throw new \Exception("Industry type can not be deleted because there are existing user instances of the industry type.");
        }
        // Delete the industry.
        $sql = "delete from industry where industry_id=:gt_id";
        return $this->PDO->delete($sql, array('gt_id' => $gt_id));
    }

    /**
     * Add a new condition.
     *
     * @param $profession_id int id of the profession
     * @param $values
     * @return bool
     * @throws \Exception
     */
    public function add_condition($profession_id, $values)
    {

        // Have to verify that there is not already an objective.
        try {
            $this->PDO->begin_transaction();

            // Check for the existance of an objective if a objective is being added.
            if ($values['type'] == 'obj') {
                $sql = "select profession_c_id from profession_condition"
                    . " where profession_c_type='obj' and profession_c_p_id=:profession_id";
                $objective = $this->PDO->select_row($sql, array('profession_id' => $profession_id));

                if ($objective !== false) {
                    throw new \Exception('There is already an Objective for the Industry');
                }
            }

            // Add the new condition.
            $values['profession_id'] = $profession_id;
            $sql = "insert into profession_condition (profession_c_p_id, profession_c_name, profession_c_description, profession_c_tooltip, profession_c_type, profession_c_uom)
                    values (:profession_id, :name, :description, :tooltip, :type, :uom)";

            $profession_c_id = $this->PDO->insert($sql, $values);

            // Put the new condition at the bottom of the order.
            $sql = "update profession_condition set profession_c_prev_condition=:profession_c_id
                    where profession_c_prev_condition is null and profession_c_p_id=:profession_id and profession_c_id!=:profession_c_id";
            $this->PDO->update($sql, array('profession_c_id' => $profession_c_id, 'profession_id' => $profession_id));

        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            throw $e;
        }

        // Commit the transation and return the TGD
        $this->PDO->commit_transaction();
        return $profession_c_id;
    }

    /**
     * Delete condition.
     *
     * @param $profession_c_id int id of the profession
     * @throws \Exception
     */
    public function delete_condition($profession_c_id)
    {

        // Have to verify that there is not already an objective.
        try {
            $this->PDO->begin_transaction();

            // Update the previous conditions next condition to be the deleted conditions next condition.
            $sql = "update profession_condition as a 
                    inner join profession_condition as b 
                    on a.profession_c_prev_condition = b.profession_c_id 
                    set a.profession_c_prev_condition = b.profession_c_prev_condition
                    where a.profession_c_prev_condition=:profession_c_id";
            $this->PDO->update($sql, array('profession_c_id' => $profession_c_id));

            // Do the deletion
            $sql = "delete from profession_condition where profession_c_id=:profession_c_id";
            $this->PDO->delete($sql, array('profession_c_id' => $profession_c_id));
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            throw $e;
        }

        // Commit the transation and return the TGD
        $this->PDO->commit_transaction();
    }

    /**
     * Move condition.
     *
     * @param $profession_c_id int id of the profession
     * @param $direction direction to move condition
     * @throws \Exception
     */
    public function move_condition($profession_c_id, $direction)
    {

        // Have to verify that there is not already an objective.
        try {
            $this->PDO->begin_transaction();

            if ($direction == 'up') {
                // Find which condition points to this condition and which condition this points to.
                $sql = "select profession_c_id
                        from profession_condition
                        where profession_c_prev_condition=:profession_c_id";
                $prev = $this->PDO->select_row($sql, array('profession_c_id' => $profession_c_id));

                // Have the condition that points to that condition now point to this condition.
                $sql = "update profession_condition as a
                        inner join profession_condition as b 
                        on a.profession_c_prev_condition = b.profession_c_id 
                        set a.profession_c_prev_condition = b.profession_c_prev_condition 
                        where a.profession_c_prev_condition=:prev_c_id";
                $this->PDO->update($sql, array('prev_c_id' => $prev->profession_c_id));

                // Point the previous condition to the next condition.
                $sql = "update profession_condition as a 
                        inner join profession_condition as b 
                        on a.profession_c_prev_condition = b.profession_c_id 
                        set a.profession_c_prev_condition=b.profession_c_prev_condition 
                        where a.profession_c_id=:prev_c_id";
                $this->PDO->update($sql, array('prev_c_id' => $prev->profession_c_id));

                // Point this condition to the previous condition.
                $sql = "update profession_condition 
                        set profession_c_prev_condition=:prev_c_id 
                        where profession_c_id=:profession_c_id";
                $this->PDO->update($sql, array('prev_c_id' => $prev->profession_c_id, 'profession_c_id' => $profession_c_id));
            }

        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            throw $e;
        }

        // Commit the transation and return the TGD
        $this->PDO->commit_transaction();
    }

    /**
     * Add a new multiplier.
     *
     * @param $industry_id int id of the industry
     * @param $values array of values
     * @return bool
     * @throws \Exception
     */
    public function add_multiplier($industry_id, $values)
    {
        // Have to verify that there is not already an objective.
        try {
            $this->PDO->begin_transaction();

            // Find the last order number
            $sql = "select max(profession_m_order) 'max_order' from profession_multiplier where profession_m_p_id=:industry_id";
            $last = $this->PDO->select_row($sql, array('industry_id' => $industry_id));
            if ($last === false || $last->max_order == null) {
                $order = 1;
            } else {
                $order = intval($last->max_order) + 1;
            }
            // Add the new multiplier.
            $values['industry_id'] = $industry_id;
            $values['order'] = $order;
            $sql = "insert into profession_multiplier (profession_m_p_id, profession_m_order, profession_m_name, profession_m_description, profession_m_type, profession_m_operation, profession_m_propagate) values (:industry_id, :order, :name, :description, :type, :operation, :propagate)";
            $profession_m_id = $this->PDO->insert($sql, $values);
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            throw $e;
        }

        // Commit the transation and return the TGD
        $this->PDO->commit_transaction();
        return $profession_m_id;
    }

    /**
     * Delete multiplier.
     * @param $industry_m_id int id of the industry to delete multiplier
     * @throws \Exception
     */
    public function delete_multiplier($industry_m_id)
    {

        // Have to verify that there is not already an objective.
        try {
            $this->PDO->begin_transaction();

            // Do the deletion
            $sql = "delete from profession_multiplier where profession_m_id=:industry_m_id";
            $this->PDO->delete($sql, array('industry_m_id' => $industry_m_id));
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            throw $e;
        }

        // Commit the transation and return the TGD
        $this->PDO->commit_transaction();
    }

    /**
     * Move multiplier.
     *
     * @param $industry_m_id int id of the industry for which to move multiplier
     * @param $direction direction to move multiplier
     * @throws \Exception
     */
    public function move_multiplier($industry_m_id, $direction)
    {

        // Have to verify that there is not already an objective.
        try {
            $this->PDO->begin_transaction();

            if ($direction == 'up') {
                // Get our order number.
                $sql = "select profession_m_p_id, profession_m_order from profession_multiplier where profession_m_id=:industry_m_id";
                $m = $this->PDO->select_row($sql, array('industry_m_id' => $industry_m_id));

                // Find the previous order number
                $sql = "select max(profession_m_order) 'max_prev' from profession_multiplier"
                    . " where profession_m_p_id=:profession_m_p_id"
                    . " and profession_m_order < :profession_m_order";
                $p = $this->PDO->select_row($sql, array('profession_m_p_id' => $m->profession_m_p_id, 'profession_m_order' => $m->profession_m_order));

                // Set the previous order number to be our order number.
                $sql = "update profession_multiplier set profession_m_order=:our_order"
                    . " where profession_m_order=:prev_order and profession_m_p_id=:profession_m_p_id";
                $this->PDO->update($sql, array('our_order' => $m->profession_m_order, 'prev_order' => $p->max_prev, 'profession_m_p_id' => $m->profession_m_p_id));

                // Set our number to be the previous number.
                $sql = "update profession_multiplier set profession_m_order=:prev_order where profession_m_id=:industry_m_id";
                $this->PDO->update($sql, array('prev_order' => $p->max_prev, 'industry_m_id' => $industry_m_id));

            }
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            throw $e;
        }

        // Commit the transation and return the TGD
        $this->PDO->commit_transaction();
    }

    /**
     * Get the list of professions for the specified industry.
     *
     * @param $industry_id int id of the industry
     * @param $user_id     int id of the user
     * @return bool object
     */
    public function get_professions($industry_id, $user_id)
    {

        // Find the list of tool groups available to the user.
        $sql = "select profession_id, profession_name, profession_description"
            . " from industry_profession"
            . " join user_profession"
            . " on profession_id=up_profession_id"
            . " where profession_industry_id=:industry_id"
            . " and up_user_id=:user_id"
            . " order by profession_name";

        return $this->PDO->select($sql, array('industry_id' => $industry_id, 'user_id' => $user_id));
    }

    /**
     * Get the list of professions for the specified industry that the user is not a member of.
     *
     * @param $industry_id int id of the industry
     * @param $user_id     int id of the user
     * @return bool object
     */
    public function get_unused_professions($industry_id, $user_id)
    {

        // Find the list of tool groups available to the user.
        $sql = "select profession_id, profession_name, profession_description"
            . " from industry_profession"
            . " left join ("
            . " select up_profession_id from user_profession where up_user_id=:user_id"
            . " ) up on up.up_profession_id=profession_id"
            . " where profession_industry_id=:industry_id and up_profession_id is null"
            . " order by profession_name";
        return $this->PDO->select($sql, array('industry_id' => $industry_id, 'user_id' => $user_id));
    }

    /**
     * Adds a new product type.
     *
     * @param $product  String Name of Product type.
     * @param $user_id  Int    User ID.
     * @return bool
     * @throws \Exception
     */
    public function add_new_product($product, $user_id)
    {

        try {
            $this->PDO->begin_transaction();

            $sql = 'select u_product_id'
                . ' from user_product'
                . ' where u_product_name=:product and u_product_user_id=0';

            $result = $this->PDO->select($sql, array('product' => $product));

            if (0 === count($result)) {

                $sql = 'insert into user_product'
                    . ' (u_product_name, u_product_user_id)'
                    . ' values (:product, :user_id)';

                $new_product_id = $this->PDO->insert($sql, array('product' => $product, 'user_id' => $user_id));

            } else {
                throw new \Exception("This is already a default product type.");
            }
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            throw $e;
        }
        return $new_product_id;
    }

    /**
     * Update the given user's active profession
     *
     * @param int $user_id id of the current user
     * @param int $profession_id new active profession for the current user
     * @return mixed
     */
    public function set_active_product($user_id, $product_id, $is_group)
    {

        // set all the  active profession column in user_profession table as inactive.
        $sql = "insert into user_active_product"
            . " (uap_product_id, uap_user_id, uap_is_group)"
            . " values(:product_id, :user_id, :is_group)"
            . " ON DUPLICATE KEY UPDATE uap_product_id=:product_id, uap_is_group=:is_group";

        $params['user_id'] = $user_id;
        $params['product_id'] = $product_id;
        $params['is_group'] = $is_group;

        return $this->PDO->update($sql, $params);
    }

    /**
     * Get the list of all the products that belong to a user.
     *
     * @param $user_id Int ID of User
     * @return array
     */
    public function get_my_products($user_id)
    {
        $ret = [];

        $sql = 'SELECT usp_id, u_product_name, u_product_id, usp_seq'
            . ' from user_selected_product'
            . ' join user_product'
            . ' on u_product_id=usp_product_id'
            . ' where usp_user_id=:user_id and usp_is_group=\'product\'';

        $ret['products'] = $this->PDO->select($sql, array('user_id' => $user_id));


//        $sql = 'select distinct u_product_name, u_product_group_id,'
//            . ' upg_name, upg_short, usp_is_group, usp_seq, usp_id'
//            . ' from user_product'
//            . ' join user_product_group'
//            . ' on upg_id=u_product_group_id'
//            . ' join user_selected_product'
//            . ' on usp_product_id=u_product_id or usp_product_id=upg_id'
//            . ' where usp_user_id=:user_id';


        $sql = 'SELECT usp_id, upg_name, upg_id, usp_seq'
            . ' from user_selected_product'
            . ' join user_product_group'
            . ' on upg_id=usp_product_id'
            . ' where usp_user_id=:user_id and usp_is_group=\'group\'';

        $ret['groups'] = $this->PDO->select($sql, array('user_id' => $user_id));
//        $sql = 'select distinct u_product_name, u_product_group_id, upg_name, upg_id,'
//            . ' u_product_id, upg_short as u_product_group_short,'
//            . ' usp_product_id, usp_is_group, '
//            . ' from user_product'
//            . ' join user_product_group'
//            . ' on upg_id=u_product_group_id'
//            . ' join user_selected_product'
//            . ' on usp_primary_product=u_product_id'
//            . ' or usp_secondary_product=u_product_id'
//            . ' or usp_tertiary_product=u_product_id'
//            . ' where usp_user_id=:user_id ';

//        $ret['data'] = $this->PDO->select($sql, array('user_id' => $user_id));

        $sql = 'select uap_product_id, u_product_name, upg_name, upg_id, uap_is_group'
            . ' from user_active_product'
            . ' left join user_product'
            . ' on uap_product_id=u_product_id and uap_is_group=\'undefined\''
            . ' left join user_product_group'
            . ' on upg_id=uap_product_id and uap_is_group=\'group\''
            . ' where uap_user_id=:user_id';

        $ret['active_id'] = $this->PDO->select($sql, array('user_id' => $user_id));

        return $ret;
    }


    /**
     * Get the list of all the products that belong to a user.
     *
     * @param $user_id Int ID of User
     * @return bool
     */
    public function get_all_products($user_id)
    {
        $ret = [];

        // Find the list of tool groups available to the user.
        $sql = 'select u_product_name, u_product_id, u_product_group_id,'
            . ' upg_name as u_product_group, upg_short as u_product_group_short'
            . ' from user_product'
            . ' join user_product_group'
            . ' on upg_id=u_product_group_id'
            . ' where u_product_user_id=:user_id'
            . ' or u_product_user_id=0';

        $data = $this->PDO->select($sql, array('user_id' => $user_id));

        // Create an associative array with product group.
        $ret = [];
        foreach ($data as $result) {
            $ret[$result->u_product_group][] = $result;
        }

        return $ret;
    }


    /**
     * Fetch the given user's active product
     *
     * @param int $user_id id of the current user
     * @return mixed
     */
    public function get_active_product($user_id)
    {
        //update active column in the user_profession table.
        $sql = " select uap_product_id as u_product_id"
            . " from user_active_product"
            . " where uap_user_id=:user_id";
        if (($product = $this->PDO->select_row($sql, array('user_id' => $user_id))) === false) {
            return false;
        }
        return $product->u_product_id;
    }
}