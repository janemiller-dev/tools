<?php

namespace Model;

/**
 * PDO helper.
 */

class PDO_model
{
	public $dbc;

    /**
     * Get the singleton.
     * @param $db_config
     * @return bool|mixed|PDO_model
     */
	public static function instance($db_config)
	{
		static $instances = array();
		// If the configuration is an array, then get the name.
		if (!is_array($db_config)) {
			if (isset($instances[$db_config]))
				return $instances[$db_config];
			else
				return false;
		}

		// Get the database name.
		$db_name = $db_config['name'];
		if (isset($instances[$db_name]))
			return $instances[$db_name];

		// Have we instantiated this DB yet?

		$instance = new PDO_Model();

		// Construct the DSN from the database setup.
		$dsn = sprintf('mysql:host=%s;dbname=%s', $db_config['host'], $db_config['database']);

		// Create the PDO object.
		$instance->dbc = new \PDO($dsn, $db_config['username'], $db_config['password'],
			array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
		$instances[$db_name] = $instance;

		return $instance;
	}

	/**
	 * Perform a delete operation.
	 */
	public function delete($sql, $data)
	{

		// Get the PDO
		if ($this->dbc == null)
			return false;

		// Prepare the delete.
		$query = $this->dbc->prepare($sql);

		// Attempt the delete
		if ($query->execute($data) == false) {
            throw new \Exception($query->errorInfo());
        }

        if ($query->rowCount() == 0) {
            return false;
        }
		return true;
	}

	/**
	 * Perform an insert operation.
	 */
	public function insert($sql, $data)
	{
		// Get the PDO
		if ($this->dbc == null)
			return false;

		// Prepare the insert.
		if (($query = $this->dbc->prepare($sql)) === false)
			throw new \Exception("Error preparing insert");

		// Attempt the insert
		if ($query->execute($data) == false)
			throw new \Exception(print_r($query->errorInfo(), 1));

		$id = $this->dbc->lastInsertId();
		return $id;
	}

	/**
	 * Perform an update operation.
	 */
	public function update($sql, $data)
	{
		// Get the PDO
		if ($this->dbc == null)
			return false;

		// Prepare the insert.
		$query = $this->dbc->prepare($sql);

		// Attempt the update
		if ($query->execute($data) == false)
			throw new \Exception(print_r($query->errorInfo(), 1));

		if ($query->rowCount() == 0) {
            return false;
        }
		return true;
	}

	/**
	 * Perform a select operation and return all of the selected rows.
	 */
	public function select($sql, $params = array(), $how = \PDO::FETCH_OBJ)
	{
		// Make sure we have a database object.
		if ($this->dbc == null)
			return false;

		// Prep the select.
		$q = $this->dbc->prepare($sql);
		$q->execute($params);

		// Attempt to get the row.
		$rows = $q->fetchAll($how);
		return $rows;
	}

	/**
	 * Perform a select operation with the expectation that we will return a single row.
	 */
	public function select_row($sql, $data = null, $how = \PDO::FETCH_OBJ)
	{

		// Make sure we have a database object.
		if ($this->dbc == null)
			return false;

		// Prep the select.
		$q = $this->dbc->prepare($sql);
		if ($data != null) {
			if ($q->execute($data) == false)
				throw new \Exception(print_r($q->errorInfo(), 1));
		} else {
			if ($q->execute() == false)
				throw new \Exception(print_r($q->errorInfo(), 1));
		}

		// Attempt to get the row.
		$row = $q->fetch($how);
		return $row;
	}

	/**
	 * Get the count of the specified select operation
	 */
	public function select_count($sql, $params = array(), $how = \PDO::FETCH_OBJ)
	{

		// Make sure we have a database object.
		if ($this->dbc == null)
			return false;

		// Prep the select.
		$q = $this->dbc->prepare($sql);
		$q->execute($params);

		// Attempt to get the row.
		return $q->fetchColumn();
	}

	/**
	 * Get the datatype of the specified column.
	 */
	public function getDataType($table, $column)
	{

		// Make sure we have a database object.
		if ($this->dbc == null)
			return false;

		$stmt = $this->dbc->query('select ' . $column . ' from ' . $table);
		$meta = $stmt->getColumnMeta(0);
		return $meta;
	}

	/**
	 * Transaction functions.
	 */
	public function begin_transaction()
	{
		if ($this->dbc == null)
			return false;
		return $this->dbc->beginTransaction();
	}

	public function commit_transaction()
	{
		if ($this->dbc == null)
			return false;
		return $this->dbc->commit();
	}

	public function rollback_transaction()
	{
		if ($this->dbc == null)
			return false;
		return $this->dbc->rollBack();
	}
}
