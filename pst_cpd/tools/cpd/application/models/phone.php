<?php

namespace Model;

class Phone
{

	/**
	 * Insert a new phone number
	 */
	public function add_phone($user_id, $object_name, $object_id, $phone_raw, $description)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Parse the phone into it's parts.
		$pnUtil = \libphonenumber\PhoneNumberUtil::getInstance();
		$pnObject = $pnUtil->parse($phone_raw, 'US');
		//echo print_r($pnObject, 1);

		// Create the SQL
		$sql = "insert into phone (phone_user_id, phone_object_name, phone_object_id, phone_description, phone_country_code, phone_national_number, phone_extension, phone_raw)"
			. " values(:user_id, :object_name, :object_id, :description, :county_code, :number, :extension, :raw)";
		$params = array(
			'user_id' => $user_id,
			'object_name' => $object_name,
			'object_id' => $object_id,
			'description' => $description,
			'county_code' => $pnObject->getCountryCode(),
			'number' => $pnObject->getNationalNumber(),
			'extension' => $pnObject->getExtension(),
			'raw' => $phone_raw,
		);
		return $PDO->insert($sql, $params);
	}

	/**
	 * Get the list of phones for the specified company.
	 */
	public function get_object_phones($object_name, $object_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Create the SQL
		$sql = "select phone_id, phone_description, phone_country_code, phone_national_number, phone_extension from phone"
			. " where phone_object_name=:object_name and phone_object_id=:object_id";
		$params = array(
			'object_name' => $object_name,
			'object_id' => $object_id
		);
		$phones = $PDO->select($sql, $params);

		// Format the phone
		foreach ($phones as $phone) {
			$pn = new \libphonenumber\PhoneNumber();
			$pn->setCountryCode($phone->phone_country_code);
			$pn->setNationalNumber($phone->phone_national_number);
			$pn->setExtension($phone->phone_extension);
			//echo print_r($pn, 1);

			// Format the phone number
			$pnUtil = \libphonenumber\PhoneNumberUtil::getInstance();
			//echo $pnUtil->format($pn, \libphonenumber\PhoneNumberFormat::NATIONAL);
			$phone->formatted = $pnUtil->format($pn, \libphonenumber\PhoneNumberFormat::NATIONAL);
		}
		return $phones;
	}

}