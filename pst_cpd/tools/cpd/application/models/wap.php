<?php

namespace Model;

class WAP
{

	/**
	 * Get the list of WAP game types
	 */
	public function get_types()
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Find the list of tool groups available to the user.
		$sql = "select wap_type_id, wap_type_name, wap_type_description"
			. " from wap_type"
			. " order by wap_type_name";
		return $PDO->select($sql, array());
	}

	/**
	 * Get the specified WAP instance. We include the user ID in the search to ensure that only the owner can see
	 * the TCG instance.
	 */
	public function get_wap($user_id, $wap_ui_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// This is a multistep process, so use a transaction.
		try {
			$PDO->begin_transaction();

			// Find the WAP specified.
			$sql = "select wap_ui_id, wap_ui_name, wap_ui_quarter, wap_ui_periods,"
				. " wap_type_id, wap_type_name, wap_type_description"
				. " from wap_user_instance"
				. " join wap_type on wap_ui_type_id=wap_type_id"
				. " where wap_ui_user_id=:user_id and wap_ui_id=:wap_ui_id";
			$wap = $PDO->select_row($sql, array('user_id' => $user_id, 'wap_ui_id' => $wap_ui_id));
			if (empty($wap))
				throw new \Exception("Could not get the requested Tactical Game Design instance");

			// Get the instance metrics.
			$sql = "select wap_im_id, wap_im_conversion_rate, wap_m_id, wap_m_name, wap_m_description, wap_m_prev_metric, wap_m_has_rate"
				. " from wap_instance_metric"
				. " join wap_metric on wap_im_m_id=wap_m_id"
				. " where wap_im_ui_id=:wap_ui_id"
				. " order by 0-wap_m_prev_metric desc";
			$wap->metrics = $PDO->select($sql, array('wap_ui_id' => $wap_ui_id));

			// Get the metric cells and arrange them in an easy way for the display.
			$wap->cells = array();
			for ($period = 1; $period <= $wap->wap_ui_periods; $period++) {
				$sql = "select wap_instance_cell.*"
					. " from wap_instance_cell"
					. " join wap_metric on wap_ic_m_id=wap_m_id"
					. " where wap_ic_ui_id=:wap_ui_id and wap_ic_period=:period";

				// Get the cells and put them in an array indexed by their metric ID.
				$cells = $PDO->select($sql, array('wap_ui_id' => $wap_ui_id, 'period' => $period));
				$wap->cells[$period] = array();
				foreach ($cells as $cell) {
					$wap->cells[$period][$cell->wap_ic_m_id] = $cell;
				}
			}
		} catch (Exception $e) {
			$PDO->rollback_transaction();
			return false;
		}

		// Commit the transation and return the AM
		$PDO->commit_transaction();
		return $wap;
	}

	/**
	 * Get the specified WAP model.
	 */
	public function get_model($user_id, $wap_um_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// This is a multistep process, so use a transaction.
		try {
			$PDO->begin_transaction();

			// Find the model specified.
			$sql = "select wap_um_id, wap_um_type_id, wap_um_name, wap_um_description,"
				. " wap_type_id, wap_type_name, wap_type_description"
				. " from wap_user_model"
				. " join wap_type on wap_um_type_id=wap_type_id"
				. " where wap_um_user_id=:user_id and wap_um_id=:wap_um_id";
			$model = $PDO->select_row($sql, array('user_id' => $user_id, 'wap_um_id' => $wap_um_id));
			if (empty($model))
				throw new \Exception("Could not get the requested WAP model");

			// Get the metrics.
			$sql = "select wap_m_id, wap_m_name, wap_m_description, wap_m_prev_metric, wap_model_row.*"
				. " from wap_metric"
				. " join wap_model_row on wap_model_m_id=wap_m_id and wap_model_um_id=:wap_um_id"
				. " order by 0-wap_m_prev_metric asc";
			$model->metrics = $PDO->select($sql, array('wap_um_id' => $model->wap_um_id));
		} catch (Exception $e) {
			$PDO->rollback_transaction();
			return false;
		}

		// Commit the transation and return the AM
		$PDO->commit_transaction();
		return $model;
	}

	/**
	 * Update the specified instance.
	 */
	public function update_instance($user_id, $ui_id, $name, $value)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Create the sql to do the update.
		$sql = "update wap_user_instance"
			. " set " . $name . "=:value"
			. " where wap_ui_user_id=:user_id and wap_ui_id=:ui_id";
		$params['value'] = $value;
		$params['user_id'] = $user_id;
		$params['ui_id'] = $ui_id;
		return $PDO->update($sql, $params);
	}

	/**
	 * Update the specified cell.
	 */
	public function update_cell($user_id, $ui_id, $metric_id, $period, $name, $value)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Create the sql to do the update.
		$sql = "update wap_instance_cell"
			. " set " . $name . "=:value"
			. " where wap_ic_ui_id=:ui_id and wap_ic_m_id=:metric_id and wap_ic_period=:period";
		$params['value'] = $value;
		$params['ui_id'] = $ui_id;
		$params['metric_id'] = $metric_id;
		$params['period'] = $period;
		$PDO->update($sql, $params);

		// If the update is to a metric with no previous metrics, set the value in all such metrics
		// that have a 0 value and a higher period number.
		if ($name != 'wap_ic_actual') {
			$sql = "update wap_instance_cell"
				. " join wap_metric on wap_ic_m_id=wap_m_id"
				. " set " . $name . "=:value"
				. " where wap_ic_ui_id=:ui_id and wap_ic_m_id=:metric_id and wap_ic_period>:period"
				. " and " . $name . "=0 and wap_m_prev_metric is null";
			$PDO->update($sql, $params);
		}

		// Get the number of periods in the instance and propagate the update.
		$sql = "select wap_ui_periods from wap_user_instance where wap_ui_id=:ui_id";
		$wap = $PDO->select_row($sql, array('ui_id' => $ui_id));
		for (; $period <= $wap->wap_ui_periods; $period++)
			$this->_propagate_update($ui_id, $metric_id, $period);

		return true;
	}

	/**
	 * Propogate and update.
	 */
	private function _propagate_update($ui_id, $metric_id, $period)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Update all of the subsequent cells.
		do {
			// Look for the next cell in the flow.
			$sql = "select wap_ic_id, wap_ic_m_id"
				. " from wap_instance_cell"
				. " join wap_metric on wap_ic_m_id=wap_m_id"
				. " where wap_m_prev_metric=:metric_id and wap_ic_ui_id=:ui_id"
				. " and wap_ic_period=:period";
			$params = array();
			$params['ui_id'] = $ui_id;
			$params['metric_id'] = $metric_id;
			$params['period'] = $period;
			$next_cell = $PDO->select_row($sql, $params);

			// If there is a next cell, update it.
			if ($next_cell) {

				// Get the current cell.
				$sql = "select *"
					. " from wap_instance_cell"
					. " join wap_instance_metric on wap_im_ui_id=wap_ic_ui_id and wap_im_m_id=wap_ic_m_id"
					. " where wap_ic_ui_id=:ui_id and wap_ic_m_id=:metric_id and wap_ic_period=:period";
				$current_cell = $PDO->select_row($sql, $params);

				// Update the next cell based on the current cell values.
				$sql = "update wap_instance_cell set"
					. " wap_ic_floor=:floor,"
					. " wap_ic_target=:target,"
					. " wap_ic_game=:game"
					. " where wap_ic_id=:ic_id";
				$params = array();
				$params['floor'] = floor($current_cell->wap_ic_floor * $current_cell->wap_im_conversion_rate);
				$params['target'] = floor($current_cell->wap_ic_target * $current_cell->wap_im_conversion_rate);
				$params['game'] = floor($current_cell->wap_ic_game * $current_cell->wap_im_conversion_rate);
				$params['ic_id'] = $next_cell->wap_ic_id;
				$PDO->update($sql, $params);

				// Update the metric and period.
				$metric_id = $next_cell->wap_ic_m_id;
			}

		} while ($next_cell !== false);
		return true;
	}

	/**
	 * Update the specified metric.
	 */
	public function update_metric($user_id, $ui_id, $metric_id, $name, $value)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Create the sql to do the update.
		$sql = "update wap_instance_metric"
			. " set " . $name . "=(:value / 100)"
			. " where wap_im_ui_id=:ui_id and wap_im_m_id=:metric_id";
		$params['value'] = $value;
		$params['ui_id'] = $ui_id;
		$params['metric_id'] = $metric_id;
		$PDO->update($sql, $params);

		// Get the number of periods in the instance and propagate the update.
		$sql = "select wap_ui_periods from wap_user_instance where wap_ui_id=:ui_id";
		$wap = $PDO->select_row($sql, array('ui_id' => $ui_id));
		for (; $period <= $wap->wap_ui_periods; $period++)
			$this->_propagate_update($ui_id, $metric_id, $period);

		return true;
	}

	/**
	 * Add a new instance.
	 */
	public function add_instance($user_id, $type_id, $name, $quarter)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// This is a multi-step process, so make sure they all work.
		try {
			$PDO->begin_transaction();

			// Create the instance record.
			$sql = "insert into wap_user_instance (wap_ui_user_id, wap_ui_type_id, wap_ui_name, wap_ui_quarter, wap_ui_created, wap_ui_updated)"
				. " values (:user_id, :type_id, :name, :quarter, now(), now())";
			$params = array('user_id' => $user_id, 'type_id' => $type_id, 'name' => $name, 'quarter' => $quarter);
			$wap_ui_id = $PDO->insert($sql, $params);

			// Create the instance metrics
			$sql = "insert into wap_instance_metric (wap_im_ui_id, wap_im_m_id)"
				. " select wap_ui_id, wap_m_id"
				. " from wap_user_instance"
				. " join wap_metric on wap_ui_type_id=wap_m_type_id"
				. " where wap_ui_id=:ui_id";
			$PDO->insert($sql, array('ui_id' => $wap_ui_id));

			// Create the instance cells. We need a set of cells for each period in the quarter.
			$date = date('Y-m-d', strtotime('next monday', strtotime($quarter)));
			$qnum = ceil(date('n', strtotime($date)) / 3);
			$period = 1;
			do {
				$sql = "insert into wap_instance_cell (wap_ic_ui_id, wap_ic_m_id, wap_ic_period, wap_ic_date)"
					. " select wap_ui_id, wap_m_id, :period, :date"
					. " from wap_user_instance"
					. " join wap_metric on wap_ui_type_id=wap_m_type_id"
					. " where wap_ui_id=:ui_id";
				$PDO->insert($sql, array('ui_id' => $wap_ui_id, 'period' => $period, 'date' => $date));
				$date = date('Y-m-d', strtotime('+ 1 week', strtotime($date)));
				$period += 1;
			} while (ceil(date('n', strtotime($date)) / 3) == $qnum);
			$periods = $period - 1;

			// Update the instance to indicate how many quarters there are.
			$sql = "update wap_user_instance set wap_ui_periods=:periods where wap_ui_id=:ui_id";
			$PDO->update($sql, array('ui_id' => $wap_ui_id, 'periods' => $periods));
		} catch (Exception $e) {
			$PDO->rollback_transaction();
			return false;
		}

		// Commit the transation and return the WAP
		$PDO->commit_transaction();
		return $wap_ui_id;
	}

	/**
	 * Delete an instance.
	 */
	public function delete_instance($user_id, $ui_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Delete the instance.
		$sql = "delete from wap_user_instance where wap_ui_id=:wap_ui_id";
		return $PDO->insert($sql, array('wap_ui_id' => $ui_id));
	}

	/**
	 * Get the list of instances for the current user.
	 */
	public function get_instances($user_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Find the list of tool groups available to the user.
		$sql = "select wap_ui_id, wap_ui_name, wap_ui_quarter, wap_type_id, wap_type_name, wap_type_description"
			. " from wap_user_instance"
			. " join wap_type on wap_ui_type_id=wap_type_id"
			. " where wap_ui_user_id=:user_id";
		return $PDO->select($sql, array('user_id' => $user_id));
	}

	/**
	 * Get the list of models for the current user.
	 */
	public function get_models($user_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Find the list of models.
		$sql = "select wap_um_id, wap_um_name, wap_um_description, wap_type_id, wap_type_name, wap_type_description"
			. " from wap_user_model"
			. " join wap_type on wap_um_type_id=wap_type_id"
			. " where wap_um_user_id=:user_id";
		return $PDO->select($sql, array('user_id' => $user_id));
	}

	/**
	 * Add a new model.
	 */
	public function add_model($user_id, $type_id, $name, $description)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// This is a multi-step process, so make sure they all work.
		try {
			$PDO->begin_transaction();

			// Create the instance record.
			$sql = "insert into wap_user_model (wap_um_user_id, wap_um_type_id, wap_um_name, wap_um_description)"
				. " values (:user_id, :type_id, :name, :description)";
			$params = array('user_id' => $user_id, 'type_id' => $type_id, 'name' => $name, 'description' => $description);
			$wap_um_id = $PDO->insert($sql, $params);

			/*
			// Create the instance metrics
			$sql = "insert into wap_instance_metric (wap_im_ui_id, wap_im_m_id)"
			. " select wap_ui_id, wap_m_id"
			. " from wap_user_instance"
			. " join wap_metric on wap_ui_type_id=wap_m_type_id"
			. " where wap_ui_id=:ui_id";
			$PDO->insert($sql, array('ui_id' => $wap_ui_id));

			// Create the instance cells. We need a set of cells for each period in the quarter.
			$date = date('Y-m-d', strtotime('next monday', strtotime($quarter)));
			$qnum = ceil(date('n', strtotime($date)) / 3);
			$period = 1;
			do {
				$sql = "insert into wap_instance_cell (wap_ic_ui_id, wap_ic_m_id, wap_ic_period, wap_ic_date)"
					. " select wap_ui_id, wap_m_id, :period, :date"
					. " from wap_user_instance"
					. " join wap_metric on wap_ui_type_id=wap_m_type_id"
					. " where wap_ui_id=:ui_id";
				$PDO->insert($sql, array('ui_id' => $wap_ui_id, 'period' => $period, 'date' => $date));
				$date = date('Y-m-d', strtotime('+ 1 week', strtotime($date)));
				$period += 1;
			} while (ceil(date('n', strtotime($date)) / 3) == $qnum);
			$periods = $period - 1;

			// Update the instance to indicate how many quarters there are.
			$sql = "update wap_user_instance set wap_ui_periods=:periods where wap_ui_id=:ui_id";
			$PDO->update($sql, array('ui_id' => $wap_ui_id, 'periods' => $periods));
			*/
		} catch (Exception $e) {
			$PDO->rollback_transaction();
			return false;
		}

		// Commit the transation and return the WAP
		$PDO->commit_transaction();
		return $wap_ui_id;
	}
}