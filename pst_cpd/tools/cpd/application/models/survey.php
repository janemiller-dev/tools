<?php

namespace Model;

class Survey
{

    private $PDO;
    private $common_model;

    /**
     * Initialises the PDO and model instance.
     */
    public function __construct()
    {
        $this->PDO = \Model\PDO_model::instance('Tools');
        $this->common_model = new Common;
    }

    /**
     * Validates if a token is generic or not.
     *
     * @param $token
     * @param $client_id
     * @param $survey_id
     * @return bool
     */
    public function validate_token($token, $client_id, $survey_id)
    {
        $sql = 'select tsc_id'
            . ' from tsl_survey_client'
            . ' where tsc_client_id=:client_id and tsc_token=:token'
            . ' and tsc_survey_id=:survey_id and tsc_used=:used_val';

        return $this->PDO->select($sql,
            array('client_id' => $client_id, 'token' => $token, 'survey_id' => $survey_id, 'used_val' => 0));
    }


    public function update_token_used($token, $client_id, $survey_id)
    {
        $sql = ' update tsl_survey_client'
            . ' set tsc_used=:val'
            . ' where tsc_client_id=:client_id and tsc_token=:token and tsc_survey_id=:survey_id and tsc_used=:used_val';

        return $this->PDO->update($sql, array('client_id' => $client_id, 'token' => $token, 'survey_id' => $survey_id, 'used_val' => 0, 'val' => 1));
    }


    /**
     * Fetches the list of questions for surveys.
     *
     * @param $id   Int Survey ID.
     * @return bool
     */
    public function get_ques($id)
    {
        $sql = ' select tsq_id, tsq_content, tsq_phase'
            . ' from tsl_survey_question'
            . ' where tsq_outline_id=:id'
            . ' or (tsq_outline_id=0 or tsq_outline_id=1)';

        return $this->PDO->select($sql, array('id' => $id));
    }

    /**
     * Fetches answer from the DB
     *
     * @param $question_id Int Question ID.
     * @param $client_id   Int Client ID.
     * @param $survey_id   Int Survey ID.
     * @return bool
     */
    public function get_ans($question_id, $client_id, $survey_id)
    {
        // Where clause conditions.
        $where = implode(' or ts_ans_question_id=', $question_id);
        $where = $where . ')';

        $sql = ' select ts_ans_id, ts_ans_question_id, ts_ans_content '
            . ' from tsl_survey_answer where ts_ans_survey_id=:survey_id'
            . ' and ts_ans_client_id=:client_id'
            . ' and (ts_ans_question_id=' . $where;

        return $this->PDO->select($sql, array('client_id' => $client_id, 'survey_id' => $survey_id));
    }

    public function set_ans($question_id, $client_id, $survey_id, $data)
    {
        $sql = 'select ts_ans_id'
            . ' from tsl_survey_answer'
            . ' where ts_ans_client_id=:client_id and ts_ans_question_id=:question_id and ts_ans_survey_id=:survey_id';
        $ans_id = $this->PDO->select($sql, array('client_id' => $client_id, 'question_id' => $question_id[0], 'survey_id' => $survey_id));

        // Checks if the question is being already answered by the client.
        if (empty($ans_id)) {
            // Inserts every answer in the database.
            foreach ($question_id as $id) {
                $sql = 'insert into tsl_survey_answer'
                    . ' (ts_ans_question_id, ts_ans_client_id, ts_ans_content, ts_ans_survey_id)'
                    . ' value(:question_id, :client_id, :content, :survey_id)';

                $this->PDO->insert($sql, array('question_id' => $id, 'client_id' => $client_id, 'content' => $data->get('ans' . $id, '', 'RAW'), 'survey_id' => $survey_id));
            }
        } else {
            // Updates every answer in the database.
            foreach ($question_id as $id) {
                $sql = 'update tsl_survey_answer'
                    . ' set ts_ans_content=:content'
                    . ' where ts_ans_client_id=:client_id and ts_ans_question_id=:question_id and ts_ans_survey_id=:survey_id';

                $this->PDO->update($sql, array('question_id' => $id, 'client_id' => $client_id, 'content' => $data->get('ans' . $id, '', 'RAW'), 'survey_id' => $survey_id));
            }
        }

        return true;
    }
}
