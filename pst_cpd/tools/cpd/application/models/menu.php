<?php

namespace Model;

use function print_r;
use function var_dump;

class Menu
{

    /**
     * Create the menu for the current user.
     */
    public function get_tools_menu($user_id)
    {
        if ((strpos($_SERVER['REQUEST_URI'], "tools/cpdv1/play_doc") === false)
            && (strpos($_SERVER['REQUEST_URI'], "tools/survey") === false)
            && (strpos($_SERVER['REQUEST_URI'], "tools/profile") === false)
            && (strpos($_SERVER['REQUEST_URI'], "tools/web/viewer") === false)
            && (strpos($_SERVER['REQUEST_URI'], "tools/web/view_ppt") === false)
            && (strpos($_SERVER['REQUEST_URI'], "tools/cpdv1/reading_file") === false)
            && (strpos($_SERVER['REQUEST_URI'], "tools/collab") === false)) {
            $PDO = \Model\PDO_model::instance('Tools');
            $PDO_wp = \Model\PDO_model::instance('WP');

            // Determine if the user is an administrator.
            $sql = "select user_id from wp_usermeta where user_id=:user_id"
                . " and meta_key='wp_capabilities'"
                . " and meta_value like '%administrator%'";
            $is_admin = $PDO_wp->select_row($sql, array('user_id' => $user_id));

            if (is_object($is_admin)) {
                $is_admin = 1;
            } else {
                $is_admin = 0;
            }

            // Get the list of tool groups the user has.
            $sql = "select memberships from wp_mepr_members where user_id=:user_id";
            $memberships = $PDO_wp->select_row($sql, array('user_id' => $user_id));

            // Get the names of the tool groups.
            $sql = "select post_name from wp_posts where ID in (" . $memberships->memberships . ")";
            $tool_groups = $PDO_wp->select($sql, array());
            $sep = '';
            $membership_list = '';
            foreach ($tool_groups as $tg) {
                $membership_list .= $sep . "'" . $tg->post_name . "'";
                $sep = ', ';
            }

            // If the user is an administrator, add tool-administrator to the list of groups.
            if (1 === $is_admin) {
                $membership_list .= ",'tool-administrator'";
            }

            // Find the list of tool groups in which the user has at least one tool.
            $sql = "select tg_id, tg_name"
                . " from tool_group"
                . " where tg_wp_name in (" . $membership_list . ")"
                . " order by tg_display_order asc";
            $groups = $PDO->select($sql, array());

            // Find the list of tools in each group.
            $sql = "select tool_id, tool_name, tool_controller"
                . " from tool"
                . " where tool_tg_id=:tg_id and tool_status='enabled'"
                . " order by tool_id";

            $menu['groups'] = array();
            foreach ($groups as $group) {
                $tools = $PDO->select($sql, array('tg_id' => $group->tg_id));

                if (!empty($tools)) {
                    $group->tools = $tools;
                    $menu['groups'][] = $group;
                }
            }

            // Are there any separate tool subscriptions?
            $sql = "select tool_id, tool_name, tool_controller"
                . " from tool"
                . " where tool_wp_name in (" . $membership_list . ")"
                . " and tool_status='enabled'"
                . " order by tool_name";
            $tools = $PDO->select($sql, array());
            $menu['tools'] = $tools;

            // Get the professions.
            $sql = "select up_profession_active, profession_id, profession_industry_id,"
                . " industry_name, profession_name, profession_description"
                . " from industry_profession"
                . " join user_profession on up_profession_id=profession_id"
                . " where up_user_id=:user_id"
                . " order by up_profession_active desc, profession_name asc";
            $professions = $PDO->select($sql, array('user_id' => $user_id));
            $menu['industry_profession'] = $professions;

            // Get the professions.
            $sql = 'select uc_name, uc_controller, uc_icon, uc_table,'
                . ' uc_col_name, uc_ref_primary_key, uc_type, uc_tool, uc_description'
                . ' from universal_component';

            $components = $PDO->select($sql, []);
            $menu['components'] = $components;

            return $menu;
        }
    }
}