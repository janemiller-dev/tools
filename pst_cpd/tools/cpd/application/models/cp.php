<?php
/**
 * Content Provider Dashboard Model.
 *
 * Content Provider Dashboard model that get/set data from database.
 *
 * @author Sumit K (sumitk@mindfiresolutions.com)
 *
 */

namespace Model;

use Noodlehaus\Exception;
use function print_r;

/**
 * CPD model class
 */
class CP
{
    private $PDO;
    private $common_model;

    /**
     * Initialises the PDO and model instance.
     */
    public function __construct()
    {
        $this->PDO = \Model\PDO_model::instance('Tools');
        $this->common_model = new Common;
    }

}