<?php

namespace Model;

class Email
{

	/**
	 * Insert a new email number
	 */
	public function add_email($object_name, $object_id, $email, $description, $input)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Create the SQL
		$sql = "insert into email (email_object_name, email_object_id, email_description, email_address)"
			. " values(:object_name, :object_id, :description, :email)";
		$params = array(
			'object_name' => $object_name,
			'object_id' => $object_id,
			'description' => $description,
			'email' => $email
		);
		return $PDO->insert($sql, $params);
	}

	/**
	 * Get the list of emails for the specified company.
	 */
	public function get_object_emails($object_name, $object_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Create the SQL
		$sql = "select email_object_name, email_object_id, email_description, email_address from email"
			. " where email_object_name=:object_name and email_object_id=:object_id";
		$params = array(
			'object_name' => $object_name,
			'object_id' => $object_id
		);
		return $PDO->select($sql, $params);
	}

}