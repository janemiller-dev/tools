<?php

namespace Model;

class User
{
    private $PDO;

    /**
     * Initialises the PDO and model instance.
     */
    public function __construct()
    {
        $this->PDO = \Model\PDO_model::instance('Tools');
    }

    /**
     * Get the specified user information
     */
    public function get_user($user_id)
    {
        $PDO = \Model\PDO_model::instance('Tools');

        // Get the user
        $sql = "select user_id, user_email, user_first_name, user_last_name, user_avatar"
            . " from user"
            . " where user_id=:user_id";
        $params['user_id'] = $user_id;
        if (($user = $PDO->select_row($sql, $params)) === false)
            return false;

        // Get the authenticators
        $sql = "select ua_id, ua_authenticator, ua_first_name, ua_last_name, ua_avatar, ua_data"
            . " from user_authenticator"
            . " where ua_user_id=:user_id"
            . " order by ua_authenticator";
        $user->authenticators = $PDO->select($sql, $params);
        foreach ($user->authenticators as $auth)
            $auth->ua_data = unserialize($auth->ua_data);
        return $user;
    }

    /**
     * Get the list of users that meet the selection criteria.
     */
    public function get_users($input)
    {
        $PDO = \Model\PDO_model::instance('Tools');

        // Get the parameters.
        $draw = $input->get('draw');
        $start = $input->get('start');
        $length = $input->get('length');

        // Is there a search.
        $search = $input->get('search', array(), 'ARRAY');
        if (!empty($search['value']))
            $search_val = $search['value'];
        //echo 'Search Val: ' . $search_val;

        // Get the columns and the search
        $sql_columns_cte = '';
        $sql_columns = '';
        $sep = ' ';
        $sql_where = '';
        $sep_where = ' where ';
        $columns = $input->get('columns', array(), 'ARRAY');
        foreach ($columns as $column) {
            if ($column['data'] === 'display_name') {
                $sql_columns .= $sep . "concat(user_first_name, ' ', user_last_name) as display_name";
                $search_col = "concat(user_first_name, ' ', user_last_name)";
            } else {
                $sql_columns .= $sep . $column['data'];
                $search_col = $column['data'];
            }
            $sep = ', ';

            // Add to the search?
            if (isset($search_val) && $column['searchable']) {
                $sql_where .= $sep_where . "cast(" . $search_col . " as char) like '%" . $search_val . "%'";
                $sep_where = ' or ';
            }
        }

        // Determine the order information.
        $order = $input->get('order', array(), 'ARRAY');
        $sql_order = '';
        foreach ($order as $o) {
            if (strlen($sql_order) > 0)
                $sql_order .= ', ';

            // Get the column
            $col = $columns[$o['column']]['data'];
            if ($col == 'address')
                $sql_order .= "concat(user_first_name, ' ', user_last_name)" . ' ' . $o['dir'];
            else
                $sql_order .= $columns[$o['column']]['data'] . ' ' . $o['dir'];
        }

        // Get the total number of rows (unfiltered).
        $sql = "select count(*) from user";
        $count_total = $PDO->select_count($sql, array());

        // Get the total number of rows (unfiltered).
        $sql = "select count(*) from user " . $sql_where;
        $count_filtered = $PDO->select_count($sql, array());

        // Get the filtered data.
        $sql = " select " . $sql_columns
            . " from user"
            . $sql_where
            . " order by " . $sql_order
            . " limit " . $start . ", " . $length;
        //echo $sql;
        $data = $PDO->select($sql, array());

        // Set the return values.
        $ret['draw'] = $draw;
        $ret['data'] = $data;
        $ret['recordsTotal'] = $count_total;
        $ret['recordsFiltered'] = $count_filtered;
        return $ret;
    }

    /**
     * Is the current user and administrator?
     */
    function is_administrator($user_id)
    {
        $PDO_wp = \Model\PDO_model::instance('WP');

        // Determine if the user is an administrator.
        $sql = "select user_id from wp_usermeta where user_id=:user_id"
            . " and meta_key='wp_capabilities'"
            . " and meta_value like '%administrator%'";
        $is_admin = $PDO_wp->select_row($sql, array('user_id' => $user_id));
        if (is_object($is_admin))
            return (true);

        return (false);
    }

    /**
     * Fetches the User data from the database.
     *
     * @param $user_id
     * @return bool
     */
    public function get_user_data($user_id)
    {
        $sql = 'select user_email as email, user_nicename as name'
            . ' from pst_wp_cpd.wp_users'
            . ' where ID=:user_id';

        return $this->PDO->select($sql, array('user_id' => $user_id));
    }

    /**
     * Updates User Data in the DB.
     *
     * @param $user_id  Int     User ID.
     * @param $name     String  User Name.
     * @param $email    String User Email
     * @return bool
     */
    public function update_user_data($user_id, $name, $email, $input, $product_count)
    {
        try {
            $this->PDO->begin_transaction();

            $sql = 'update pst_wp_cpd.wp_users'
                . ' set user_email=:email, user_nicename=:name'
                . ' where ID=:user_id';
            $ret['updated_email'] = $this->PDO->update($sql,
                array('name' => $name, 'email' => $email, 'user_id' => $user_id));

            for ($i = 0; $i < $product_count; $i++) {
                if ('' !== $input->get('id' . $i)) {
                    $sql = 'insert'
                        . ' into user_selected_product'
                        . ' (usp_user_id, usp_product_id, usp_is_group, usp_seq)'
                        . ' values(:user_id, :product_id, :is_group, :seq)'
                        . ' on duplicate key'
                        . ' update usp_product_id=:product_id, usp_is_group=:is_group, usp_seq=:seq';

                    $ret['updated_products'] = $this->PDO->update($sql,
                        array('user_id' => $user_id, 'product_id' => $input->get('id' . $i),
                            'is_group' => $input->get('is_group' . $i),
                            'seq' => $i));
                }
            }
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            echo $e;
        }
        return $ret;
    }
}