<?php

namespace Model;

class Code
{

	/**
	 * Add a new code type
	 */
	function add_code_type($name, $abbrev, $description)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Create the SQL.
		$sql = "insert into code_type (ct_name, ct_abbrev, ct_description)"
			. " values (:name, :abbrev, :description)";
		$params['name'] = $name;
		$params['abbrev'] = $abbrev;
		$params['description'] = $description;

		return $PDO->insert($sql, $params);
	}

	/**
	 * Delete a code type
	 */
	function delete_code_type($ct_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Create the SQL.
		$sql = "delete from code_type where ct_id=:ct_id";
		$params['ct_id'] = $ct_id;

		return $PDO->delete($sql, $params);
	}

	/**
	 * Get the list of code types and their codes.
	 */
	public function get_code_types()
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Get the code types.
		$sql = "select ct_id, ct_abbrev, ct_name, ct_description"
			. " from code_type"
			. " order by ct_name";
		$code_types = $PDO->select($sql, array());

		// Get the codes for the code types
		$sql = "select code_id, code_abbrev, code_name, code_description"
			. " from code"
			. " where code_type_id=:type_id"
			. " order by code_name";
		foreach ($code_types as $code_type) {
			$code_type->codes = $PDO->select($sql, array('type_id' => $code_type->ct_id));
		}
		return $code_types;
	}

	/**
	 * Get the list of codes given a code type.
	 */
	public function get_codes($ct_abbrev)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Get the codes for the code types
		$sql = "select code_id, code_abbrev, code_name, code_description"
			. " from code"
			. " join code_type on ct_id=code_type_id"
			. " where ct_abbrev=:ct_abbrev"
			. " order by code_id desc";
		return $PDO->select($sql, array('ct_abbrev' => $ct_abbrev));
	}

	/**
	 * Add a new code
	 */
	function add_code($ct_id, $name, $abbrev, $description)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Create the SQL.
		$sql = "insert into code (code_type_id, code_name, code_abbrev, code_description)"
			. " values (:ct_id, :name, :abbrev, :description)";
		$params['ct_id'] = $ct_id;
		$params['name'] = $name;
		$params['abbrev'] = $abbrev;
		$params['description'] = $description;

		return $PDO->insert($sql, $params);
	}

	/**
	 * Delete a code
	 */
	function delete_code($code_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Create the SQL.
		$sql = "delete from code where code_id=:code_id";
		$params['code_id'] = $code_id;

		return $PDO->delete($sql, $params);
	}

}