<?php

namespace Model;

class Setting
{

	/**
	 * Get the specified setting.
	 */
	public function get_cf_types($user_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Select the setting. The user ID is include for security.
		$sql = "select * from custom_field_type order by cft_name";
		return $PDO->select($sql, array());
	}

	/**
	 * Add a custom field definition.
	 */
	public function add_cf_definition($user_id, $cf_def, $extra)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		try {
			$PDO->begin_transaction();

			// Create custom field definition.
			$sql = "insert into custom_field_definition (cfd_user_id, cfd_object_name, cfd_name, cfd_system_name, cfd_type_id, cfd_type_extra)"
				. " values(:user_id, :object_name, :name, :sys_name, (select cft_id from custom_field_type where cft_name=:type), :type_extra)";
			$params['user_id'] = $user_id;
			$params['object_name'] = $cf_def['object_name'];
			$params['name'] = $cf_def['name'];
			$params['sys_name'] = $cf_def['sys_name'];
			$params['type'] = $cf_def['type'];
			$params['type_extra'] = serialize($extra);
			$cfd_id = $PDO->insert($sql, $params);

			// Add the custom field to each of the existing objects.
			$sql = "insert into custom_field (cf_cfd_id, cf_object_id) select :cfd_id, "
				. $params['object_name'] . "_id from "
				. $params['object_name'] . " where "
				. $params['object_name'] . "_user_id=:user_id";
			$PDO->insert($sql, array('cfd_id' => $cfd_id, 'user_id' => $user_id));
		} catch (\Exception $e) {
			$PDO->rollback_transaction();
			return false;
		}
		$PDO->commit_transaction();
		return $cfd_id;
	}

	/**
	 * Get the list of custom definitions for the specified user.
	 */
	public function get_cf_definitions($input, $user_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Get the parameters.
		$draw = $input->get('draw');
		$start = $input->get('start');
		$length = $input->get('length');

		// Is there a search.
		$search = $input->get('search', array(), 'ARRAY');
		if (!empty($search['value']))
			$search_val = $search['value'];
		//echo 'Search Val: ' . $search_val;

		// Limit search to the current user
		$sql_where = ' where cfd_user_id=' . $user_id . ' ';
		$sep_where = ' and ';

		// Get the columns and the search
		$sql_columns_cte = '';
		$sql_columns = '';
		$sep = ' ';
		$columns = $input->get('columns', array(), 'ARRAY');
		foreach ($columns as $column) {

			$sql_columns .= $sep . $column['data'];
			$search_col = $column['data'];
			$sep = ', ';

			// Add to the search?
			if (isset($search_val) && $column['searchable']) {
				$sql_where .= $sep_where . "cast(" . $search_col . " as char) like '%" . $search_val . "%'";
				$sep_where = ' or ';
			}
		}

		// Determine the order information.
		$order = $input->get('order', array(), 'ARRAY');
		$sql_order = '';
		foreach ($order as $o) {
			if (strlen($sql_order) > 0)
				$sql_order .= ', ';

			// Get the column
			$col = $columns[$o['column']]['data'];
			$sql_order .= $columns[$o['column']]['data'] . ' ' . $o['dir'];
		}

		// Get the total number of rows (unfiltered).
		$sql = "select count(*) from custom_field_definition where company_user_id=" . $user_id;
		$count_total = $PDO->select_count($sql, array());

		// Get the total number of rows (filtered).
		$sql = "select count(*) from custom_field_definition " . $sql_where;
		$count_filtered = $PDO->select_count($sql, array());

		// Get the filtered data.
		$sql = " select " . $sql_columns
			. " from custom_field_definition"
			. " join custom_field_type on cft_id=cfd_type_id"
			. $sql_where
			. " order by " . $sql_order
			. " limit " . $start . ", " . $length;
		//echo $sql;
		$data = $PDO->select($sql, array());

		// Unserialize the type extras
		foreach ($data as $cfd) {
			$cfd->type_extra = unserialize($cfd->cfd_type_extra);
		}

		// Set the return values.
		$ret['draw'] = $draw;
		$ret['data'] = $data;
		$ret['recordsTotal'] = $count_total;
		$ret['recordsFiltered'] = $count_filtered;
		return $ret;
	}
}