<?php

namespace Model;

use Noodlehaus\Exception;

/**
 * Objective model class.
 */
class Objective
{
    private $PDO;
    private $common_model;

    /**
     * Initialises the PDO and model instance.
     */
    public function __construct()
    {
        $this->PDO = \Model\PDO_model::instance('Tools');
        $this->common_model = new Common;
    }

    /**
     * Gets the list of objective.
     *
     * @param $user_id int id of the user.
     * @param $profession_id int id of the currently selected profession.
     * @return object
     */
    public function get_objective($user_id, $profession_id)
    {
        $sql = 'select upo_year as year, upo_floor as floor, upo_target as target, upo_game as game'
            . ' from user_profession_objective'
            . ' join user_profession'
            . ' on user_profession_objective.upo_up_id=user_profession.up_id'
            . ' and user_profession.up_profession_id=:profession_id'
            . ' where user_profession.up_user_id=:user_id'
            . ' order by upo_year asc';

        $params['user_id'] = $user_id;
        $params['profession_id'] = $profession_id;
        $ret['data'] = $this->PDO->select($sql, $params);

        // Get the total number of rows (filtered).
        $sql = 'select count(*) from user_profession_objective'
            . ' join user_profession'
            . ' on user_profession_objective.upo_up_id=user_profession.up_id'
            . ' and user_profession.up_profession_id=:profession_id'
            . ' where user_profession.up_user_id=:user_id';
        $count_filtered = $this->PDO->select_count($sql, $params);

        $ret['recordsFiltered'] = $count_filtered;

        // Get the total number of rows (unfiltered).
        $sql = 'select count(*) from user_profession_objective';
        $count_total = $this->PDO->select_count($sql, array('user_id' => $user_id));
        $ret['recordsTotal'] = $count_total;

        return $ret;
    }

    /**
     * Updates the objective.
     *
     * @param $user_id int id of the user.
     * @param $profession_id int id of the profession.
     * @param $name string name of the objective to be updated.
     * @param $year int year for which objective is to be updates.
     * @param $value int value to set in the updated objective.
     * @return bool
     */
    public function update_objective($user_id, $profession_id, $name, $year, $value)
    {
        $sql = 'select up_id'
            . ' from user_profession'
            . ' where up_user_id=:user_id'
            . ' and up_profession_id=:profession_id';
        $up_id = $this->PDO->select($sql, array('user_id' => $user_id, 'profession_id' => $profession_id));

        $sql = 'update user_profession_objective'
            . ' set '.$name.'=:value'
            . ' where upo_up_id=:up_id'
            . ' and upo_year=:year';
        $params['value'] = $value;
        $params['up_id'] = intval($up_id[0] -> up_id);
        $params['year'] = $year;

        return $this->PDO->update($sql, $params);
    }

    /**
     *  Gets the list of years for which 0objective is already present.
     *
     * @param $user_id       int Id of the user.
     * @param $profession_id int Id of the profession.
     * @return bool
     */
    public function get_used_years($user_id, $profession_id)
    {
        $sql = 'select upo_year'
            . ' from user_profession_objective'
            . ' join user_profession on upo_up_id=up_id'
            . ' where up_profession_id =:profession_id'
            . ' and up_user_id=:user_id';

        return $this->PDO->select($sql, array('user_id' => $user_id, 'profession_id' => $profession_id));

    }

    /**
     * Add new objective for the selected year.
     *
     * @param $data array  Values for adding objective (FTG, id of user and id of profession).
     */
    public function add_objective($data)
    {
        $sql = 'select up_id'
            . ' from user_profession'
            . ' where up_user_id=:user_id'
            . ' and up_profession_id=:profession_id';

        $up_id = $this->PDO->select($sql, array('user_id' => $data['user_id'], 'profession_id' => $data['profession_id']));

        $sql = ' insert into user_profession_objective ('
            . ' upo_up_id, upo_year, upo_floor, upo_target, upo_game )'
            . ' values (:up_id, :year, :floor, :target, :game)';

        $params['up_id'] = intval($up_id[0] -> up_id);
        $params['year'] = $data['year'];
        $params['floor'] = $data['floor'];
        $params['target'] = $data['target'];
        $params['game'] = $data['game'];
        $this->PDO->insert($sql, $params);

        return;
    }

    /**
     * Gets the list of already used quarters for a yearly objective
     *
     * @param $user_id        int Id of the user .
     * @param $profession_id  int Industry profession Id.
     * @param $year           int Year to which the quarter belong.
     * @return bool
     */
    public function get_used_quarters($user_id, $profession_id, $year)
    {
        $sql = ' select upqo_quarter from user_profession_quarter_objective'
            . ' join user_profession_objective'
            . ' on upqo_upo_id=upo_id'
            . ' join user_profession'
            . ' on upo_up_id=up_id'
            . ' where up_profession_id=:profession_id'
            . ' and up_user_id=:user_id'
            . ' and upo_year=:year';

        return $this->PDO->select($sql, array('profession_id' => $profession_id, 'user_id' => $user_id, 'year' => $year));
    }

    /**
     * Adds new Objective for a quarter.
     *
     * @param $data mixed details for the objective
     */
    public function add_quarter_objective($data)
    {
        $sql = ' select upo_id from user_profession_objective'
            . ' join user_profession'
            . ' on upo_up_id=up_id'
            . ' where up_profession_id=:profession_id'
            . ' and up_user_id=:user_id'
            . ' and upo_year=:year';

        $upo_id = $this->PDO->select($sql, array('profession_id' => $data['profession_id'], 'user_id' => $data['user_id'], 'year' => $data['year']));

        $sql = ' insert into user_profession_quarter_objective ('
            . ' upqo_upo_id, upqo_quarter, upqo_floor, upqo_target, upqo_game )'
            . ' values (:upo_id, :quarter, :floor, :target, :game) ';
        $params['upo_id'] = intval($upo_id[0] -> upo_id);
        $params['quarter'] = $data['quarter'];
        $params['floor'] = $data['floor'];
        $params['target'] = $data['target'];
        $params['game'] = $data['game'];

        $this->PDO->insert($sql, $params);
        return;
    }

    /**
     *  Returns the objectives for a quarter.
     *
     * @param $user_id       int   Id of the user.
     * @param $profession_id int   Id of the Industry Profession
     * @param $year          int   Year for which objectives will be shown.
     * @return                   mixed Objectives
     */
    public function get_quarter_objective($user_id, $profession_id, $year)
    {
        $sql = 'select upqo_id as id, upqo_quarter as quarter, upqo_floor as floor, upqo_target as target, upqo_game as game'
            . ' from user_profession_quarter_objective'
            . ' join user_profession_objective'
            . ' on user_profession_quarter_objective.upqo_upo_id=user_profession_objective.upo_id'
            . ' join user_profession'
            . ' on user_profession.up_id=user_profession_objective.upo_up_id'
            . ' where user_profession.up_profession_id=:profession_id'
            . ' and user_profession.up_user_id=:user_id'
            . ' and user_profession_objective.upo_year=:year'
            . ' order by upqo_quarter asc';

        $params['user_id'] = $user_id;
        $params['profession_id'] = $profession_id;
        $params['year'] = $year;
        $ret['data'] = $this->PDO->select($sql, $params);

        // Get the total number of rows (filtered).
        $sql = 'select count(*) from user_profession_quarter_objective'
            . ' join user_profession_objective'
            . ' on user_profession_objective.upo_id = user_profession_quarter_objective.upqo_upo_id';
        $count_filtered = $this->PDO->select_count($sql, $params);

        $ret['recordsFiltered'] = $count_filtered;

        // Get the total number of rows (unfiltered).
        $sql = 'select count(*) from user_profession_quarter_objective';
        $count_total = $this->PDO->select_count($sql, array('user_id' => $user_id));
        $ret['recordsTotal'] = $count_total;

        return $ret;
    }

    /**
     * Updates the objective for a quarter
     * @param $name     text  name of the column to be updated
     * @param $value    int   value to which the column need to be updated
     * @param $upqo_id  int   id of the quarter
     * @return bool
     */
    public function update_quarter_objective($name, $value, $upqo_id)
    {
        $sql = 'update user_profession_quarter_objective'
            . ' set '.$name.'=:value'
            . ' where upqo_id=:upqo_id';

        return $this->PDO->update($sql, array('value'=>$value, 'upqo_id'=>$upqo_id));
    }
}