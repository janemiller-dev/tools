<?php

namespace Model;

class Contact
{

	/**
	 * Insert a new phone number
	 */
	public function add_contact($user_id, $company_id, $contact_first, $contact_last)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Create the SQL
		$sql = "insert into contact (contact_user_id, contact_company_id, contact_first_name, contact_last_name)"
			. " values(:user_id, :company_id, :contact_first, :contact_last)";
		$params = array('user_id' => $user_id, 'company_id' => $company_id, 'contact_first' => $contact_first, 'contact_last' => $contact_last);
		return $PDO->insert($sql, $params);
	}

	/**
	 * Get the list of contacts for the specified company.
	 */
	public function get_company_contacts($company_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Create the SQL
		$sql = "select contact_id, contact_first_name, contact_last_name, contact_title from contact"
			. " where contact_company_id=:company_id order by contact_last_name, contact_first_name";
		$params = array('company_id' => $company_id);
		return $PDO->select($sql, $params);
	}

	/**
	 * Delete a company's contacts
	 */
	public function delete_company_contacts($company_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Get the list of contacts for the company.
		$sql = "select contact_id from contact where contact_company_id=:company_id";
		$contacts = $PDO->select($sql, array('company_id' => $company_id));

		// For each contact, delete the asscciated records.
		foreach ($contacts as $contact) {
			// Delete any attached phone numbers.
			$sql = "delete from phone where phone_object_name='contact' and phone_object_id=:contact_id";
			$PDO->delete($sql, array('contact_id' => $contact_id));

			// Delete any attached addresses.
			$sql = "delete from address where address_object_name='contact' and address_object_id=:contact_id";
			$PDO->delete($sql, array('contact_id' => $contact_id));

			// Delete any attached email addresses.
			$sql = "delete from email where email_object_name='contact' and email_object_id=:contact_id";
			$PDO->delete($sql, array('contact_id' => $contact_id));
		}

		// Delete all of the contacts.
		$sql = "delete from contact where contact_company_id=:company_id";
		$PDO->delete($sql, array('company_id' => $company_id));
	}
}