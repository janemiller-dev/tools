<?php

namespace Model;

class Profile
{

    private $PDO;
    private $common_model;

    /**
     * Initialises the PDO and model instance.
     */
    public function __construct()
    {
        $this->PDO = \Model\PDO_model::instance('Tools');
        $this->common_model = new Common;
    }

    /**
     * Validates if a token is generic or not.
     *
     * @param $token
     * @param $client_id
     * @param $profile_id
     * @return bool
     */
    public function validate_token($token, $client_id, $profile_id)
    {
        $sql = 'select tpc_id'
            . ' from tsl_profile_client'
            . ' where tpc_client_id=:client_id and tpc_token=:token'
            . ' and tpc_profile_id=:profile_id and tpc_used=:used_val';

        return $this->PDO->select($sql, array('client_id' => $client_id, 'token' => $token,
            'profile_id' => $profile_id, 'used_val' => 0));
    }


    public function update_token_used($token, $client_id, $profile_id)
    {
        $sql = ' update tsl_profile_client'
            . ' set tpc_used=:val'
            . ' where tpc_client_id=:client_id'
            . ' and tpc_token=:token and tpc_profile_id=:profile_id and tpc_used=:used_val';

        return $this->PDO->update($sql, array('client_id' => $client_id, 'token' => $token,
            'profile_id' => $profile_id, 'used_val' => 0, 'val' => 1));
    }


    /**
     * Fetches the list of questions for profiles.
     *
     * @param $id   Int Profile ID.
     * @return bool
     */
    public function get_ques($id)
    {
        $sql = ' select tpq_id, tpq_content, tpq_question'
            . ' from tsl_profile_question'
            . ' where tpq_phase=:id';

        return $this->PDO->select($sql, array('id' => $id));
    }

    public function get_ans($question_id, $client_id, $profile_id)
    {
        // Where clause conditions.
        $where = implode(' or tsl_profile_ans_ques_id=', $question_id);
        $where = $where.')';
        $sql = ' select tsl_profile_ans_id, tsl_profile_ans_ques_id, tsl_profile_ans_content '
            . ' from tsl_profile_answer where tsl_profile_ans_profile_id=:profile_id and tsl_profile_ans_client_id=:client_id'
            . ' and (tsl_profile_ans_ques_id=' . $where;
        return $this->PDO->select($sql, array('client_id' => $client_id, 'profile_id' => $profile_id));
    }

    public function set_ans($question_id, $client_id, $profile_id, $data)
    {
        $sql = 'select tsl_profile_ans_id'
            . ' from tsl_profile_answer'
            . ' where tsl_profile_ans_client_id=:client_id and tsl_profile_ans_ques_id=:question_id and tsl_profile_ans_profile_id=:profile_id';
        $ans_id = $this->PDO->select($sql, array('client_id' => $client_id, 'question_id' => $question_id[0],
            'profile_id' => $profile_id));

        // Checks if the question is being already answered by the client.
        if (empty($ans_id)) {
            // Inserts every answer in the database.
            foreach ($question_id as $id) {
                $sql = 'insert into tsl_profile_answer'
                    . ' (tsl_profile_ans_ques_id, tsl_profile_ans_client_id, tsl_profile_ans_content, tsl_profile_ans_profile_id)'
                    . ' value(:question_id, :client_id, :content, :profile_id)';

                $this->PDO->insert($sql, array('question_id' => $id, 'client_id' => $client_id,
                    'content' => $data->get('ans' . $id, '', 'RAW'), 'profile_id' => $profile_id));
            }
        } else {
            // Updates every answer in the database.
            foreach ($question_id as $id) {
                $sql = 'update tsl_profile_answer'
                    . ' set tsl_profile_ans_content=:content'
                    . ' where tsl_profile_ans_client_id=:client_id'
                    . ' and tsl_profile_ans_ques_id=:question_id and'
                    . ' tsl_profile_ans_profile_id=:profile_id';

                $this->PDO->update($sql, array('question_id' => $id, 'client_id' => $client_id,
                    'content' => $data->get('ans' . $id, '', 'RAW'), 'profile_id' => $profile_id));
            }
        }

        return true;
    }
}
