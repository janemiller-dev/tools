<?php

namespace Model;

use function array_merge;
use function implode;
use function print_r;

class Common
{
    private $PDO;

    public function __construct()
    {
        $this->PDO = \Model\PDO_model::instance('Tools');
    }

    /**
     * Update the specified table and column.
     */
    public function update_field($table, $column, $id, $value)
    {

        // Get the table's auto increment column.
        $sql = "show columns from " . $table . " where extra like '%auto_increment%'";
        $id_name = $this->PDO->select_row($sql, array());

        // If the data type is DATE, convert the value
        $column_meta = $this->PDO->getDataType($table, $column);
        if ($column_meta['native_type'] == 'DATE')
            $value = date('Y-m-d', strtotime($value));

        // Create the SQL.
        $sql = "update " . $table . " set " . $column . "=:value where " . $id_name->Field . "=:id";
        $params['id'] = $id;
        $params['value'] = $value;

        return $this->PDO->update($sql, $params);
    }

    /**
     * Updates active TSL/TGD/CPD.
     *
     * @param $instance_id      Int     TGD/CPD instance Id
     * @param $name             String  Name of column.
     * @param $user_id          Int     User Id.
     */
    public function update_active_instances($instance_id, $name, $user_id)
    {
        $sql = 'insert into user_active_cpd_tgd  (uact_user_id, uact_' . $name . ')'
            . ' values (:user_id, :instance_id)'
            . ' on DUPLICATE KEY update uact_' . $name . '=:instance_id';

        $this->PDO->update($sql, array('user_id' => $user_id, 'instance_id' => $instance_id));
    }

    /**
     * Fetches alert data.
     *
     * @param $user_id Int User Id.
     * @param $id      Int Client Id.
     * @param int $when
     * @param int $duration
     * @return mixed
     */
    public function get_alert_data($user_id, $id, $when = 0, $duration = 0)
    {
        $where = '';
        $when_query = '';

        // Prepare where clause.
        (0 !== $id) ? $where = ' and hc_id=' . $id : '';
        (0 !== $when) ? $when_query = ' and Date(tac_cb_when) = \'' . $when . '\'' : '';
        (0 !== $duration) ? $when_query = ' and tac_cb_when >= \'' . $duration . '\'' : '';

        // Call Back data.

        $sql = 'select tn_id, tn_content, tn_seq, tn_topic, tn_datetime, tn_next_call,'
            . ' tn_action, tn_row_color, hc_name, tn_objective, tac_id, tac_cb_reason'
            . ' from tsl_notes'
            . ' join homebase_client_asset'
            . ' on hca_id=tn_client_id'
            . ' join homebase_client'
            . ' on hca_client_id=hc_id'
            . ' join tsl_client'
            . ' on tc_asset_id=hca_id'
            . ' join tsl_about_client'
            . ' on tc_id=tac_tc_id'
            . ' where hc_user_id=:user_id'
            . ' order by Date(tn_datetime)=Date(now()) desc, tn_datetime asc';

//        $sql = 'SELECT tac_cb_when, tac_valid_phone, hc_name, tac_id, tac_cb_note, tac_cb_reason, tac_cb_row_color'
//            . ' FROM `tsl_about_client`'
//            . ' join tsl_client'
//            . ' on tc_id=tac_tc_id'
//            . ' join homebase_client_asset'
//            . ' on hca_id=tc_asset_id'
//            . ' join homebase_client'
//            . ' on hca_client_id=hc_id'
//            . ' where hc_user_id=:user_id and tac_valid_phone=\'cb\' ' . $where . $when_query
//            . ' order by Date(tac_cb_when)=Date(now()) desc, tac_cb_when asc';

        $ret['cb'] = $this->PDO->select($sql, array('user_id' => $user_id));

        // Prepare where clause.
        (0 !== $when) ? $when_query = ' and Date(ta_when) = \'' . $when . '\'' : '';
        (0 !== $duration) ? $when_query = ' and ta_when >= \'' . $duration . '\'' : '';

        // Next action data.
        $sql = 'select ta_content, ta_whom, ta_when, ta_row_color,'
            . ' ta_seq, hc_name, ta_id, ta_reason, ta_client_id'
            . ' from tsl_actions'
            . ' join homebase_client_asset'
            . ' on hca_id=ta_client_id'
            . ' join homebase_client'
            . ' on hca_client_id=hc_id'
            . ' where hc_user_id=:user_id' . $where . $when_query
            . ' order by Date(ta_when)=Date(now()) desc, ta_when asc';

        $ret['action'] = $this->PDO->select($sql, array('user_id' => $user_id));

        // Prepare where clause.
        (0 !== $when) ? $when_query = ' and Date(ua_datetime) = \'' . $when . '\'' : '';
        (0 !== $duration) ? $when_query = ' and ua_datetime >= \'' . $duration . '\'' : '';

        // Alerts Data.
        $sql = 'select ua_id, ua_component_type, ua_component_id, ua_completion_date,'
            . ' ua_asset_id, ua_set, ua_datetime, ua_action, ua_reason, hc_name, ua_row_color, '
            . ' deal_cpd_id '
            . ' from user_alerts'
            . ' join homebase_client_asset'
            . ' on ua_asset_id=hca_id'
            . ' join homebase_client'
            . ' on hca_client_id=hc_id'
            . ' left join deal'
            . ' on deal_asset_id=hca_id'
            . ' where ua_user_id=:user_id' . $where . $when_query
            . ' order by Date(ua_datetime)=Date(now()) desc, ua_datetime asc';

        $ret['alerts'] = $this->PDO->select($sql, array('user_id' => $user_id));

        // Alert Objective.
        $sql = 'select uao_id, uao_name, uao_category'
            . ' from user_alerts_objective'
            . ' where uao_user_id=0 or uao_user_id=:user_id';

        $objectives = $this->PDO->select($sql, array('user_id' => $user_id));

        // Loop through objectives array.
        foreach ($objectives as $objective) {
            $ret['objective'][$objective->uao_category][] = $objective;
        }

        return $ret;
    }

    public function add_to_alert($component_id, $type, $asset_id, $set, $user_id, $datetime)
    {
        $sql = ' insert into user_alerts'
            . ' (ua_component_type, ua_component_id, ua_asset_id, ua_set, ua_user_id, ua_datetime)'
            . ' values(:type, :component_id, :asset_id, :set, :user_id, :datetime)';

        return $this->PDO->insert($sql, array('type' => $type, 'component_id' => $component_id,
            'asset_id' => $asset_id, 'set' => $set, 'user_id' => $user_id, 'datetime' => $datetime));
    }

    public function update_alert_date($id, $type, $value)
    {
        if ('action' === $type) {
            $sql = 'update tsl_actions'
                . ' set ta_when=:value'
                . ' where ta_id=:id';

            $ret = $this->PDO->update($sql, array('value' => $value, 'id' => $id));
        } else if ('cb' === $type) {
            $sql = 'update tsl_about_client'
                . ' set tac_cb_when=:value'
                . ' where tac_id=:id';

            $ret = $this->PDO->update($sql, array('value' => $value, 'id' => $id));
        } else {
            $sql = 'update user_alerts'
                . ' set ua_datetime=:value'
                . ' where ua_id=:id';;

            $ret = $this->PDO->update($sql, array('value' => $value, 'id' => $id));
        }

        return $ret;
    }

    /**
     * Update alert action column.
     *
     * @param $id
     * @param $type
     * @param $value
     * @return bool
     */
    public function update_alert_action($id, $type, $value)
    {
        if ('action' === $type) {
            $sql = 'update tsl_actions'
                . ' set ta_content=:value'
                . ' where ta_id=:id';

            $ret = $this->PDO->update($sql, array('value' => $value, 'id' => $id));
        } else if ('cb' === $type) {
            $sql = 'update tsl_about_client'
                . ' set tac_cb_note=:value'
                . ' where tac_id=:id';

            $ret = $this->PDO->update($sql, array('value' => $value, 'id' => $id));
        } else {
            $sql = 'update user_alerts'
                . ' set ua_action=:value'
                . ' where ua_id=:id';;

            $ret = $this->PDO->update($sql, array('value' => $value, 'id' => $id));
        }

        return $ret;
    }

    /**
     * @param $id
     * @param $type
     * @param $value
     * @return bool
     */
    public function update_alert_objective($id, $type, $value)
    {
        if ('action' === $type) {
            $sql = 'update tsl_actions'
                . ' set ta_reason=:value'
                . ' where ta_id=:id';

            $ret = $this->PDO->update($sql, array('value' => $value, 'id' => $id));
        } else if ('cb' === $type) {
            $sql = 'update tsl_about_client'
                . ' set tac_cb_reason=:value'
                . ' where tac_id=:id';

            $ret = $this->PDO->update($sql, array('value' => $value, 'id' => $id));
        } else {
            $sql = 'update user_alerts'
                . ' set ua_reason=:value'
                . ' where ua_id=:id';

            $ret = $this->PDO->update($sql, array('value' => $value, 'id' => $id));
        }

        return $ret;
    }

    public function update_row_color($id, $value, $type)
    {
        if ('action' === $type) {
            $sql = 'update tsl_actions'
                . ' set ta_row_color=:value'
                . ' where ta_id=:id';

            $ret = $this->PDO->update($sql, array('value' => $value, 'id' => $id));
        } else if ('cb' === $type) {
            $sql = 'update tsl_about_client'
                . ' set tac_cb_row_color=:value'
                . ' where tac_id=:id';

            $ret = $this->PDO->update($sql, array('value' => $value, 'id' => $id));
        } else {
            $sql = 'update user_alerts'
                . ' set ua_row_color=:value'
                . ' where ua_id=:id';

            $ret = $this->PDO->update($sql, array('value' => $value, 'id' => $id));
        }

        return $ret;
    }

    public function add_new_objective($val, $user_id)
    {
        $sql = ' insert into user_alerts_objective'
            . ' (uao_user_id, uao_name)'
            . ' values(:user_id, :val)';

        return $this->PDO->insert($sql, array('user_id' => $user_id, 'val' => $val));
    }

    public function delete_alert($id)
    {
        $sql = 'delete from user_alerts'
            . ' where ua_id=:id';

        return $this->PDO->delete($sql, array('id' => $id));
    }

    public function update_asset_info($asset_id, $val, $seq, $type, $sub_seq, $category)
    {
        try {

            $sql = 'insert into homebase_asset_profile_info'
                . ' (hapi_type, hapi_seq, hapi_sub_seq, hapi_asset_id, hapi_value, hapi_category)'
                . ' values(:type, :seq, :sub_seq, :asset_id, :value, :category)'
                . ' on duplicate key update'
                . ' hapi_value=:value';

            $ret = $this->PDO->insert($sql, array('type' => $type, 'seq' => $seq, 'sub_seq' => (null === $sub_seq ? 0 : $sub_seq),
                'asset_id' => $asset_id, 'value' => $val, 'category' => $category));
        } catch (\Exception $e) {
            $ret = $e;
        }

        return $ret;
    }

    /**
     * Fetches asset information.
     *
     * @param $id       Int     Id for asset.
     * @param $category String  Category for information.
     * @return bool
     */
    public function fetch_asset_info($id, $category)
    {
        $sql = 'select hapi_type, hapi_seq, hapi_sub_seq, hapi_value'
            . ' from homebase_asset_profile_info'
            . ' where hapi_asset_id=:asset_id and hapi_category=:category';

        return $this->PDO->select($sql, array('asset_id' => $id, 'category' => $category));
    }

    /**
     * Update reminder data.
     *
     * @param $type     String Type of component.
     * @param $id       Int    Component Id.
     * @param $user_id  Int    User Id.
     * @return bool
     */
    public function update_reminder($type, $id, $user_id)
    {
        $sql = ' insert into user_disable_reminder'
            . ' (udr_component_type, udr_component_id, udr_user_id)'
            . ' values(:type, :id, :user_id)'
            . ' on duplicate key update udr_id=udr_id';

        return $this->PDO->insert($sql, array('type' => $type, 'id' => $id, 'user_id' => $user_id));

    }

    public function get_disable_reminder($user_id)
    {
        $sql = 'select udr_component_type, udr_component_id'
            . ' from user_disable_reminder'
            . ' where udr_user_id=:user_id';

        return $this->PDO->select($sql, array('user_id' => $user_id));
    }

    public function add_new_project($user_id, $when)
    {
        $sql = 'insert into quick_project'
            . ' (qp_user_id, qp_when)'
            . ' values(:user_id, :when)';

        return $this->PDO->select($sql, array('user_id' => $user_id, 'when' => $when));
    }

    public function get_quick_project($user_id, $when)
    {
        $sql = 'select qp_id, qp_ceo, qp_project, qp_who, qp_tag, qp_cat'
            . ' from quick_project'
            . ' where qp_user_id=:user_id and qp_when=:when';

        $ret['project_list'] = $this->PDO->select($sql, array('user_id' => $user_id, 'when' => $when));
        $ret['assignee'] = $this->get_assignee($user_id);

        return $ret;
    }

    private function get_assignee($user_id)
    {
        $sql = ' select tsl_assignee_id, tsl_assignee_name'
            . ' from tsl_assignee'
            . ' where tsl_assignee_user_id=:user_id';

        return $this->PDO->select($sql, array('user_id' => $user_id));
    }

    public function add_new_contact($user_id, $when)
    {
        $sql = 'insert into quick_contact'
            . ' (qc_user_id, qc_when)'
            . ' values(:user_id, :when)';

        return $this->PDO->insert($sql, array('user_id' => $user_id, 'when' => $when));
    }

    public function get_quick_contact($user_id, $when)
    {
        $sql = ' select qc_id, qc_ceo, qc_contact, qc_info,'
            . ' qc_tag, qc_cat, qc_who'
            . ' from quick_contact'
            . ' where qc_user_id=:user_id and qc_when=:when';

        $ret['contact_list'] = $this->PDO->select($sql, array('user_id' => $user_id, 'when' => $when));
        $ret['assignee'] = $this->get_assignee($user_id);
        return $ret;
    }

    public function add_new_action($user_id, $when)
    {
        $sql = 'insert into quick_action'
            . ' (qa_user_id, qa_when)'
            . ' values(:user_id, :when)';

        return $this->PDO->insert($sql, array('user_id' => $user_id, 'when' => $when));
    }

    public function get_quick_action($user_id, $when)
    {
        $sql = ' select qa_id, qa_ceo, qa_action, qa_who, qa_tag, qa_cat'
            . ' from quick_action'
            . ' where qa_user_id=:user_id and qa_when=:when';

        $ret['action_list'] = $this->PDO->select($sql, array('user_id' => $user_id, 'when' => $when));
        $ret['assignee'] = $this->get_assignee($user_id);
        return $ret;
    }

    public function update_quick_field($table, $id, $col, $val, $key)
    {
        try {
            $sql = 'update ' . $table
                . ' set ' . $col . '=:val'
                . ' where ' . $key . '=:id';

            $ret = $this->PDO->update($sql, array('val' => $val, 'id' => $id));
        } catch (\Exception $e) {
            throw $e;
        }
        return $ret;
    }

    public function delete_quick_tool($table, $id, $key)
    {
        $sql = 'delete from ' . $table
            . ' where ' . $key . '=:val';

        return $this->PDO->delete($sql, array('val' => $id));
    }

    public function set_reminder_recurrence($user_id, $interval)
    {
        $sql = 'insert into user_reminder_recurrence'
            . ' (urr_user_id, urr_interval)'
            . ' values(:user_id, :interval)'
            . ' on duplicate key'
            . ' update urr_interval=:interval, urr_last_shown=now()';

        return $this->PDO->insert($sql, array('user_id' => $user_id, 'interval' => $interval));
    }

    public function get_reminder_recurrence($user_id)
    {
        $sql = 'select urr_interval, urr_last_shown'
            . ' from user_reminder_recurrence'
            . ' where urr_user_id=:user_id';

        return $this->PDO->select($sql, array('user_id' => $user_id));
    }

    /**
     * Set the recurrence shown time for the reminder.
     *
     * @param $user_id Int User ID.
     * @return bool
     */
    public function set_recurrence_shown($user_id)
    {
        $sql = 'update user_reminder_recurrence'
            . ' set urr_last_shown=now()'
            . ' where urr_user_id=:user_id';

        return $this->PDO->update($sql, array('user_id' => $user_id));
    }

    public function get_sgd_info($user_id, $tab)
    {
        $sql = 'select us_text, us_id, us_by_when, us_seq, us_type'
            . ' from user_sgd'
            . ' where us_user_id=:user_id and us_tab=:tab';

        $ret['info'] = $this->PDO->select($sql, array('user_id' => $user_id, 'tab' => $tab));

        $sql = ' select usmc_month, usmc_type'
            . ' from user_sgd_month_count'
            . ' where usmc_user_id=:user_id and usmc_type=:tab';

        $ret['month_count'] = $this->PDO->select($sql, array('user_id' => $user_id, 'tab' => $tab));
        return $ret;
    }

    public function update_sgd_text($user_id, $val, $tab, $seq, $type)
    {
        $sql = 'insert into user_sgd'
            . ' (us_user_id, us_tab, us_seq, us_text, us_type)'
            . ' values(:user_id, :tab, :seq, :text, :type)'
            . ' on duplicate key'
            . ' update us_text=:text';

        return $this->PDO->update($sql, array('user_id' => $user_id, 'tab' => $tab, 'seq' => $seq,
            'text' => $val, 'type' => $type));
    }

    public function update_sgd_date($user_id, $val, $tab, $seq, $type)
    {
        $sql = 'insert into user_sgd'
            . ' (us_user_id, us_tab, us_seq, us_by_when, us_type)'
            . ' values(:user_id, :tab, :seq, :when, :type)'
            . ' on duplicate key'
            . ' update us_by_when=:when';

        return $this->PDO->insert($sql, array('user_id' => $user_id, 'tab' => $tab, 'seq' => $seq,
            'when' => $val, 'type' => $type));
    }

    public function update_sgd_month_count($user_id, $val, $type)
    {
        $sql = 'insert into user_sgd_month_count'
            . ' (usmc_user_id, usmc_type, usmc_month)'
            . ' values(:user_id, :type, :month)'
            . ' on duplicate key'
            . ' update usmc_month=:month';

        return $this->PDO->insert($sql, array('user_id' => $user_id, 'type' => $type, 'month' => $val));
    }

    public function reset_acm_color($user_id)
    {
        try {
            $sql = 'update tsl_actions'
                . ' join homebase_client_asset'
                . ' on hca_id=ta_client_id'
                . ' join homebase_client'
                . ' on hca_client_id=hc_id'
                . ' set ta_row_color=null'
                . ' where hc_user_id=:user_id';

            $ret['action_updated'] = $this->PDO->update($sql, array('user_id' => $user_id));

            $sql = 'update tsl_about_client'
                . ' join tsl_client'
                . ' on tc_id=tac_tc_id'
                . ' join tsl'
                . ' on tsl_id=tc_tsl_id'
                . ' set tac_cb_row_color=null'
                . ' where tsl_user_id=:user_id';

            $ret['cb_updated'] = $this->PDO->update($sql, array('user_id' => $user_id));

            $sql = 'update user_alerts'
                . ' set ua_row_color=null'
                . ' where ua_user_id=:user_id';

            $ret = $this->PDO->update($sql, array('user_id' => $user_id));

        } catch (\Exception $e) {
            return $e;
        }
        return $ret;
    }

    public function get_user_assets($user_id, $year, $quarter)
    {
        $sql = 'select hca_id, assetstatus.has_status as ds_status, hca_avatar, aai_list_price, hca_address,'
            . ' aai_built_year, aai_units, aai_price_per_unit, aai_price_per_sf, aai_avg_sf, hca_lat, hca_lng'
            . ' from deal join homebase_client_asset on hca_id=deal_asset_id'
            . ' join homebase_client on hca_client_id=hc_id'
            . ' join (select has_status, hs1.has_datetime, has_id, has_asset_id, has_md'
            . ' from homebase_asset_status hs1 join'
            . ' (select max(has_datetime) as time, has_asset_id as asset_id'
            . ' from homebase_asset_status where ((quarter(has_datetime)<=:quarter && year(has_datetime)=:year)'
            . ' || (year(has_datetime)<:year)) group by has_asset_id) as hs2'
            . ' on hs1.has_datetime = hs2.time and hs1.has_asset_id = asset_id) as assetstatus'
            . ' on hca_id=assetstatus.has_asset_id join code on code_abbrev=assetstatus.has_status'
            . ' join code_type on code_type_id=ct_id and (ct_abbrev=\'gds\''
            . ' OR ct_abbrev=\'cds\' OR ct_abbrev=\'str_ds\' OR ct_abbrev=\'bds\' OR ct_abbrev=\'rds\')'
            . ' left join agent_asset_info'
            . ' on aai_hca_id = hca_id'
            . ' where deal_cpd_id = (select uact_cpd_id from user_active_cpd_tgd where uact_user_id=:user_id)';

        return $this->PDO->select($sql, array('user_id' => $user_id, 'year' => $year, 'quarter' => $quarter));
    }

    public function save_agent_info($user_id, $agent_info)
    {
        $ret = '';
        // Check if the request is to add new agent.
        if ((1 === $agent_info->getInt('add_agent')) ||
            (0 === $agent_info->getInt('add_agent') && empty($agent_info->get('agent_id')))) {

            $sql = 'insert into agent_profile'
                . ' (ap_user_id, ap_name, ap_title, ap_company, ap_address,'
                . ' ap_office_phone, ap_cell_phone, ap_focus, ap_email, ap_years_experience, ap_type, ap_second_phone)'
                . ' values(:user_id, :name, :title, :company, :address, :office,'
                . ' :cell, :focus, :email, :years, :type, :second)';

            $ret = $this->PDO->insert($sql, array('user_id' => $user_id, 'name' => $agent_info->getString('agent_name'),
                'title' => $agent_info->getString('agent_title'), 'company' => $agent_info->getString('agent_company_name'),
                'address' => $agent_info->getString('agent_office_address'), 'office' => $agent_info->get('agent_office_phone'),
                'cell' => $agent_info->get('agent_cell_phone'), 'focus' => $agent_info->get('agent_focus'),
                'email' => $agent_info->get('agent_client_email'), 'years' => $agent_info->get('agent_exp', '', 'RAW'),
                'type' => $agent_info->get('agent_type'), 'second' => $agent_info->get('agent_second_phone')));
        } else if (0 === $agent_info->getInt('add_agent') && !empty($agent_info->get('agent_id'))) {
            $sql = 'update agent_profile'
                . ' set ap_user_id=:user_id, ap_name =:name, ap_title=:title,'
                . ' ap_company=:company, ap_address=:address, ap_office_phone=:office,'
                . ' ap_cell_phone=:cell, ap_focus=:focus, ap_email=:email,'
                . ' ap_years_experience=:years, ap_second_phone=:second'
                . ' where ap_id=:id';

            $ret = $this->PDO->update($sql, array('user_id' => $user_id, 'name' => $agent_info->getString('agent_name'),
                'title' => $agent_info->getString('agent_title'), 'company' => $agent_info->getString('agent_company_name'),
                'address' => $agent_info->getString('agent_office_address'), 'office' => $agent_info->get('agent_office_phone'),
                'cell' => $agent_info->get('agent_cell_phone'), 'focus' => $agent_info->get('agent_focus'),
                'email' => $agent_info->get('agent_client_email'), 'years' => $agent_info->get('agent_exp', '', 'RAW'),
                'second' => $agent_info->get('agent_second_phone'), 'id' => $agent_info->get('agent_id')));

        }

        return $ret;
    }

    public function get_agent_info($user_id, $id)
    {
        $where = '';
        !empty($id) ? $where = " and ap_id=$id" : '';

        $sql = 'select ap_name, ap_title, ap_company, ap_address, ap_office_phone, ap_id, ap_image,'
            . ' ap_cell_phone, ap_focus, ap_email, ap_years_experience, ap_type, ap_second_phone'
            . ' from agent_profile'
            . ' where ap_user_id=:user_id' . $where
            . ' order by field(ap_type, \'agent\') desc';

        return $this->PDO->select($sql, array('user_id' => $user_id));
    }

    public function update_agent_asset_info($asset_id, $col, $value)
    {
        $sql = " insert into agent_asset_info"
            . " ($col, aai_hca_id)"
            . " values(:value, :hca_id)"
            . " on duplicate key"
            . " update $col=:value";

        return $this->PDO->insert($sql, array('value' => $value, 'hca_id' => $asset_id));
    }

    public function get_agent_image($agent_id)
    {
        $sql = 'select ap_image'
            . ' from agent_profile'
            . ' where ap_id=:id';

        return $this->PDO->select($sql, array('id' => $agent_id));
    }

    public function upload_agent_image($agent_id, $random)
    {
        $sql = ' update agent_profile'
            . ' set ap_image=:image'
            . ' where ap_id=:id';

        return $this->PDO->select($sql, array('image' => $random, 'id' => $agent_id));
    }

    public function save_agent_popover_info($type, $val, $id)
    {
        $sql = 'insert into agent_info'
            . ' (ai_type, ai_agent_id, ai_value)'
            . ' values(:type, :agent_id, :value)'
            . ' on duplicate key'
            . ' update ai_value=:value';

        return $this->PDO->insert($sql, array('type' => $type, 'agent_id' => $id, 'value' => $val));
    }

    public function get_agent_popup_info($agent_id)
    {
        $sql = 'select ai_type, ai_value'
            . ' from agent_info'
            . ' where ai_agent_id=:id';

        return $this->PDO->select($sql, array('id' => $agent_id));
    }

    public function join_room($user_id)
    {
        $sql = 'select uss_message as Message, uss_id as ID'
            . ' from user_screen_share'
            . ' where uss_user_id=:user_id';

        return $this->PDO->select($sql, array('user_id' => $user_id));
    }

    public function get_asset_info($id, $type)
    {
        $sql = 'SELECT aai_id, aai_built_year, aai_units, aai_price_per_unit,'
            . ' aai_price_per_sf, aai_avg_sf, aai_list_price, aai_building_size,'
            . ' aai_lot_size, aai_parking_ratio, aai_rentable_sf, aai_usable_sf,'
            . ' aai_amenities_inside, aai_amenities_outside, aai_proximity, aai_interstate_access,'
            . ' aai_location_rating, aai_hca_id, aai_floors, aai_sprinkler, aai_expansion,'
            . ' aai_unit_condition, aai_ground_condition,'
            . ' aai_ownership, aai_owner, aai_management, aai_attorney, aai_accountant,'
            . ' aai_investor, aai_occupancy, aai_lease_type, aai_rental_rate, aai_expenses,'
            . ' aai_cash_flow, aai_lender, aai_debt_service, aai_total_debt, aai_prepayment, aai_cap,'
            . ' aai_tenant_status, aai_easement, aai_environmental, aai_neighborhood, aai_competition,'
            . ' aai_demographics, aai_crime, aai_access,'
            . ' hca_type, hca_address, hca_name,'
            . ' hca_location, hca_city, hca_zip, hca_state'
            . ' from agent_asset_info'
            . ' join homebase_client_asset'
            . ' on aai_hca_id = hca_id'
            . ' where aai_hca_id =:id';

        $ret = $this->PDO->select($sql, array('id' => $id));

        if (0 === count($ret)) {
            $sql = 'insert into agent_asset_info'
                . ' (aai_hca_id)'
                . ' values(:id)';

            $ret = $this->PDO->insert($sql, array('id' => $id));
        }

        return $ret;
    }

    public function update_agent__info($id, $table, $col, $val, $key)
    {
        try {
        $sql = 'update ' . $table
            . ' set ' . $col . ' =:val'
            . ' where ' . $key . ' =:id';

        return $this->PDO->update($sql, array('val' => $val, 'id' => $id));
        } catch (\Exception $e) {
            echo $e;
            exit();
        }
    }

    public function get_deals_identified($user_id)
    {
        $sql = 'select  hca_id,'
            . ' hca_avatar, aai_list_price, hca_address, aai_built_year, aai_units,'
            . ' aai_price_per_unit, aai_price_per_sf, aai_avg_sf, hca_lat, hca_lng'
            . ' from agent_deal_identified'
            . ' join homebase_client_asset'
            . ' on hca_id=adi_asset_id'
            . ' left join agent_asset_info'
            . ' on aai_hca_id=hca_id'
            . ' where adi_user_id=:user_id';

        return $this->PDO->select($sql, array('user_id' => $user_id));
    }

    public function update_comps_info($col, $val, $id)
    {
        $sql = ' update agent_comps'
            . ' set ' . $col . '=:val'
            . ' where ac_id=:id';

        return $this->PDO->update($sql, array('val' => $val, 'id' => $id));
    }
}