<?php

namespace Model;

use function array_column;
use Guzzle\Common\Exception\UnexpectedValueException;
use function implode;
use function print_r;
use function redirect_post;
use function sizeof;

class TGD
{

    private $PDO;

    function __construct()
    {
        $this->PDO = \Model\PDO_model::instance('Tools');
    }

    /**
     * Get the list of TGD game types
     */
    public function get_types()
    {
        // Find the list of tool groups available to the user.
        $sql = "select tgd_gt_id, tgd_gt_name, tgd_gt_description"
            . " from tgd_game_type"
            . " order by tgd_gt_name";
        return $this->PDO->select($sql, array());
    }

    /**
     * Get the specified game type
     */
    public function get_game_type($id)
    {

        // Find the game types
        $sql = "select tgd_gt_id, tgd_gt_name, tgd_gt_description"
            . " from tgd_game_type"
            . " where tgd_gt_id=:id";
        $gt = $this->PDO->select_row($sql, array('id' => $id));
        if (empty($gt)) {
            throw new Exception("Game Type not found");
        }

        // MySQL does not support recursive queries, so we have to get the conditions one at a time starting from the first.
        $conditions = array();
        $sql = "select tgd_c_id, tgd_c_name, tgd_c_description, tgd_c_tooltip, tgd_c_type, tgd_c_uom, tgd_c_prev_condition"
            . " from tgd_condition"
            . " where tgd_c_gt_id=:gt_id and tgd_c_prev_condition is null";
        $condition = $this->PDO->select_row($sql, array('gt_id' => $gt->tgd_gt_id));

        // Push the condition, then find the one that points to this one.
        do {
            array_unshift($conditions, $condition);

            if (!$condition) {
                $sql = "select tgd_c_id, tgd_c_name, tgd_c_description, tgd_c_tooltip, tgd_c_type, tgd_c_uom, tgd_c_prev_condition"
                    . " from tgd_condition"
                    . " where tgd_c_gt_id=:gt_id and tgd_c_prev_condition=:prev_tgd_c_id";
                $condition = $this->PDO->select_row($sql, array('gt_id' => $gt->tgd_gt_id, 'prev_tgd_c_id' => $condition->tgd_c_id));
            }
        } while ($condition !== false);
        $gt->conditions = $conditions;

        // Get the game type multipliers.
        $sql = "select tgd_m_id, tgd_m_order, tgd_m_name, tgd_m_description, tgd_m_type, tgd_m_operation, tgd_m_propagate"
            . " from tgd_multiplier"
            . " where tgd_m_gt_id=:gt_id"
            . " order by tgd_m_order";
        $gt->multipliers = $this->PDO->select($sql, array('gt_id' => $gt->tgd_gt_id));

        return $gt;
    }

    /**
     * Get the specified TGD instance. We include the user ID in the search to ensure that only the owner can see
     * the TCG instance.
     */
    public function get_tgd($user_id, $tgd_ui_id, $year)
    {
        // This is a multistep process, so use a transaction.
        try {
            $this->PDO->begin_transaction();

            $columns = "tgd_ui_id, tgd_ui_name, tgd_ui_mission,"
                . " tgd_ui_year, tgd_ui_floor, tgd_ui_target, tgd_ui_game,"
                . " tgd_ui_periods_final, tgd_ui_default_check_size,"
                . " uact_cpd_id as tgd_cpd_id, uact_tgd_id, cpd_name, tgd_ui_condition,"
                . " tgd_ui_py_cum_floor, tgd_ui_py_cum_target, tgd_contract_actual, tgd_cumulative_actual,"
                . " tgd_ui_py_cum_game, u_product_name, u_product_id, tgd_ui_product_id, tgd_list_actual,"
                . " tgd_ui_created, tgd_ui_updated, tgd_ui_periods_lock, tgd_proposal_actual,"
                . " tgd_ui_tgt_q1_pct, tgd_ui_tgt_q1_obj,"
                . " tgd_ui_tgt_q2_pct, tgd_ui_tgt_q2_obj,"
                . " tgd_ui_tgt_q3_pct, tgd_ui_tgt_q3_obj,"
                . " tgd_ui_tgt_q4_pct, tgd_ui_tgt_q4_obj,"
                . " tgd_ui_tgt_q5_pct, tgd_ui_tgt_q5_obj,"
                . " tgd_ui_tgt_q6_pct, tgd_ui_tgt_q6_obj,"
                . " tgd_ui_label_1, tgd_ui_label_2, tgd_ui_label_3, tgd_ui_label_4,"
                . " tgd_gt_id, tgd_gt_name, tgd_gt_description,"
                . " share_id, share_type, tgd_contract_f,"
                . " tgd_contract_t, tgd_contract_g, tgd_list_f,"
                . " tgd_list_t, tgd_list_g, tgd_proposal_f,"
                . " tgd_proposal_t, tgd_proposal_g, tgd_ui_prev_year_id, tgd_ui_next_year_id";

            // Find the TGD specified.
            $sql = "select " . $columns
                . " from tgd_user_instance"
                . " left join tgd_game_type on tgd_ui_gt_id=tgd_gt_id"
                . " left join user_active_cpd_tgd on uact_user_id=tgd_ui_user_id"
//                . " and uact_prod_id=tgd_ui_product_id"
                . " left join cpd on cpd_id=uact_cpd_id"
                . " and cpd_user_id=uact_user_id"
                . " left join share on share_object_type='tgd' and share_with_id=:user_id"
                . " and share_object_id=tgd_ui_id"
//                . " join user_active_product"
//                . " on uap_product_id=tgd_ui_product_id and tgd_ui_user_id=uap_user_id"
                . " left join user_product"
                . " on u_product_id=tgd_ui_product_id"
                . " where (tgd_ui_user_id=:user_id or share_id is not null) and tgd_ui_id=:tgd_ui_id";

            $tgd = $this->PDO->select_row($sql, array('user_id' => $user_id, 'tgd_ui_id' => $tgd_ui_id));

            if (empty($tgd)) {
                throw new \UnexpectedValueException("Could not get the requested Tactical Game Designer instance");
            }

            // Active CPD ID.
            $cpd_id = $tgd->tgd_cpd_id;

            // Current TGD ID.
            $tgd_id = $tgd->tgd_ui_id;

            // Get all of the other user's TGD instances of the same type.
            $sql = "select tgd_ui_id, tgd_ui_name"
                . " from tgd_user_instance"
                . " where tgd_ui_user_id=:user_id and tgd_ui_id != :tgd_ui_id and tgd_ui_gt_id=:gt_id"
                . " order by tgd_ui_name";
            $tgd->other_instances = $this->PDO->select($sql,
                array('user_id' => $user_id, 'tgd_ui_id' => $tgd_ui_id, 'gt_id' => $tgd->tgd_gt_id));

            // Get the instance conditions.
            $sql = "select tgd_icond_id, tgd_icond_conversion_rate, tgd_c_id, tgd_c_name, tgd_c_col_name,"
                . " tgd_c_description, tgd_c_tooltip, tgd_c_type, tgd_c_uom, tgd_c_prev_condition"
                . " from tgd_instance_condition"
                . " join tgd_condition on tgd_icond_c_id=tgd_c_id"
                . " where tgd_icond_ui_id=:tgd_ui_id"
                . " order by ISNULL(tgd_c_prev_condition), 0-tgd_c_prev_condition asc";

            $tgd->conditions = $this->PDO->select($sql, array('tgd_ui_id' => $tgd_ui_id));

            // Get the game type multipliers.
            $sql = "select tgd_m_id, tgd_m_name, tgd_m_description, tgd_m_type"
                . " from tgd_multiplier"
                . " join tgd_user_instance on tgd_ui_gt_id=tgd_m_gt_id"
                . " where tgd_ui_id=:tgd_ui_id"
                . " order by tgd_m_order asc";
            $tgd->multipliers = $this->PDO->select($sql, array('tgd_ui_id' => $tgd_ui_id));

            // Get the multiplier cells and then arrange them in an easy way for the display.
            $sql = "select tgd_im_id, tgd_im_m_id, tgd_im_period, tgd_im_multiple"
                . " from tgd_instance_multiplier"
                . " where tgd_im_ui_id=:tgd_ui_id";

            $mcells = $this->PDO->select($sql, array('tgd_ui_id' => $tgd_ui_id));
            $tgd->mcells = array();
            foreach ($mcells as $mcell) {

                // If the "row" does not exist, create it.
                if (!isset($tgd->mcells[$mcell->tgd_im_period])) {
                    $tgd->mcells[$mcell->tgd_im_period] = array();
                }

                // The "column" will never exist.
                $tgd->mcells[$mcell->tgd_im_period][$mcell->tgd_im_m_id] = $mcell;
            }

            // Get the FTG values.
            $sql = "select uo_floor, uo_target, uo_game"
                . " from user_objective"
                . " join user_active_cpd_tgd"
                . " on uact_tgd_id=:tgd_id"
                . " where uo_cpd_id=:cpd_id";

            $tgd->objective = $this->PDO->select($sql, array('cpd_id' => $cpd_id, 'tgd_id' => $tgd_ui_id));

            $sql = 'select toi_id, toi_source_id, toi_tgd_ui_id, toi_qtr, toi_amount, toi_name, tois_value'
                . ' from tgd_other_income join tgd_other_income_source'
                . ' on toi_source_id=tois_id'
                . ' where toi_tgd_ui_id=:ui_id';

            $tgd->oi = $this->PDO->select($sql, array('ui_id' => $tgd_ui_id));

            $other_income = [];

            foreach ($tgd->oi as $oi) {
                empty($other_income[$oi->toi_qtr]) ? $other_income[$oi->toi_qtr] = [] : '';
                $other_income[$oi->toi_qtr][count($other_income[$oi->toi_qtr])] = $oi;
            }


            $tgd->other_income = $other_income;
            // Initialize each period's other income.
            $tgd->oi_total = array();
            for ($period = 1; $period <= 6; $period++) {
                $tgd->oi_total[$period] = 0;
            }

            // Add the income from each type of other income.
            foreach ($tgd->other_income as $oi) {

                // Sum up all other incomes.
                foreach ($oi as $income) {
                    $tgd->oi_total[$income->toi_qtr] += $income->toi_amount;
                }
            }

            // Get the condition cells and arrange them in an easy way for the display.
            $tgd->cells = array();
            for ($period = 1; $period <= 6; $period++) {
                $sql = "select tgd_instance_cell.*"
                    . " from tgd_instance_cell"
                    . " join tgd_condition on tgd_ic_c_id=tgd_c_id"
                    . " where tgd_ic_ui_id=:tgd_ui_id and tgd_ic_period=:period";

                // Get the cells and put them in an array indexed by their condition ID.
                $cells = $this->PDO->select($sql, array('tgd_ui_id' => $tgd_ui_id, 'period' => $period));
                $tgd->cells[$period] = array();
                foreach ($cells as $cell) {
                    $tgd->cells[$period][$cell->tgd_ic_c_id] = $cell;
                }

                // If there's active CPD and active TGD instance is selected.
                if (($cpd_id !== null) && ($tgd->uact_tgd_id === $tgd_id)) {

                    $sql = "SELECT count(deal_id) as net"
                        . " FROM deal"
                        . " join homebase_client_asset"
                        . " on hca_id=deal_asset_id"
                        . " join (select has_status, hs1.has_datetime, has_id, has_asset_id, has_md"
                        . " from homebase_asset_status hs1"
                        . " join (select max(has_datetime) as time"
                        . " from homebase_asset_status"
                        . " where quarter(has_datetime)=:quarter"
                        . " group by has_asset_id) as hs2"
                        . " on hs1.has_datetime = hs2.time) as assetstatus on hca_id=assetstatus.has_asset_id"
                        . " WHERE deal_cpd_id = :cpd_id  ";

                    // Deals with Proposal Requested.
                    $actual_sql = $sql . "and (assetstatus.has_status='requested' or assetstatus.has_status='presented')";

                    $actual = $this->PDO->select($actual_sql, array('cpd_id' => $cpd_id, 'quarter' => $period - 1));
                    $tgd->actual[$cells[0]->tgd_ic_c_id][$period] = $actual;

                    // Deals Listed .
                    $actual_sql = $sql . "and (assetstatus.has_status='listed' or assetstatus.has_status='launched')";
                    $actual = $this->PDO->select($actual_sql, array('cpd_id' => $cpd_id, 'quarter' => $period - 1));
                    $tgd->actual[$cells[1]->tgd_ic_c_id][$period] = $actual;

                    $actual_sql = $sql . "and (assetstatus.has_status='contract' or assetstatus.has_status='hard')";
                    $actual = $this->PDO->select($actual_sql, array('cpd_id' => $cpd_id, 'quarter' => $period - 1));
                    $tgd->actual[$cells[2]->tgd_ic_c_id][$period] = $actual;

                    $sql = "select sum(hca_net) as net"
                        . " from homebase_client_asset"
                        . " join deal on hca_id = deal_asset_id"
                        . " join (select has_status, hs1.has_datetime, has_id, has_asset_id, has_md"
                        . " from homebase_asset_status hs1"
                        . " join (select max(has_datetime) as time"
                        . " from homebase_asset_status"
                        . " where quarter(has_datetime)=:quarter"
                        . " group by has_asset_id) as hs2"
                        . " on hs1.has_datetime = hs2.time) as assetstatus on hca_id=assetstatus.has_asset_id"
                        . " WHERE deal_cpd_id =:cpd_id  and assetstatus.has_status='complete'";

                    $actual = $this->PDO->select($sql, array('cpd_id' => $cpd_id, 'quarter' => $period - 1));
                    $tgd->actual[$cells[3]->tgd_ic_c_id][$period] = $actual;

                    $sql = "select sum(hca_net) as ref"
                        . " from homebase_client_asset"
                        . " join deal on hca_id = deal_asset_id"
                        . " join (select has_status, hs1.has_datetime, has_id, has_asset_id, has_md"
                        . " from homebase_asset_status hs1"
                        . " join (select max(has_datetime) as time"
                        . " from homebase_asset_status"
                        . " where quarter(has_datetime)=:quarter"
                        . " group by has_asset_id) as hs2"
                        . " on hs1.has_datetime = hs2.time) as assetstatus on hca_id=assetstatus.has_asset_id"
                        . " WHERE deal_cpd_id =:cpd_id  and assetstatus.has_status='ref_pa'";

                    $ref = $this->PDO->select($sql, array('cpd_id' => $cpd_id, 'quarter' => $period - 1));
                    $tgd->ref[$period] = $ref;

                    $sql = "select sum(hca_net) as buyer"
                        . " from homebase_client_asset"
                        . " join deal on hca_id = deal_asset_id"
                        . " join (select has_status, hs1.has_datetime, has_id, has_asset_id, has_md"
                        . " from homebase_asset_status hs1"
                        . " join (select max(has_datetime) as time"
                        . " from homebase_asset_status"
                        . " where quarter(has_datetime)=:quarter"
                        . " group by has_asset_id) as hs2"
                        . " on hs1.has_datetime = hs2.time) as assetstatus on hca_id=assetstatus.has_asset_id"
                        . " WHERE deal_cpd_id =:cpd_id  and assetstatus.has_status='accepted_co'";

                    $buyer = $this->PDO->select($sql, array('cpd_id' => $cpd_id, 'quarter' => $period - 1));
                    $tgd->buyer[$period] = $buyer;
                }

                $sql = 'select tl_linked_product_id, uact_tgd_id, uact_cpd_id'
                    . ' from tgd_linked join user_active_cpd_tgd'
//                    . ' on uact_prod_id=tl_linked_product_id'
                    . ' where tl_tgd_id=:tgd_id';

                $linked_data_array = $this->PDO->select($sql, array('tgd_id' => $tgd_id));

                $linked1 = 0;
                $linked2 = 0;
                $linked3 = 0;
                $linked4 = 0;


                // Iterate over each linked data for a TGD.
                foreach ($linked_data_array as $linked_data) {

                    // Check if linked data ia not null.
                    if ($linked_data != null) {
                        $linked_tgd = $linked_data->uact_tgd_id;
                        $linked_cpd = $linked_data->uact_cpd_id;
                    }

                    // Linked TGD Instances.
                    if (isset($linked_tgd) && $linked_tgd !== null) {

                        $sql = "SELECT count(deal_id) as net"
                            . " FROM deal"
                            . " left join (SELECT ds_status, ds1.ds_datetime, ds_id, ds_deal_id, ds_md"
                            . " FROM deal_status ds1"
                            . " left JOIN ( SELECT MAX(ds_datetime) AS time"
                            . " FROM deal_status"
                            . " WHERE quarter(ds_datetime)<=:quarter"
                            . " GROUP BY ds_deal_id) AS ds2"
                            . " ON ds1.ds_datetime = ds2.time) as dealstatus on deal_id=dealstatus.ds_deal_id"
                            . " WHERE deal_cpd_id = :cpd_id ";

                        // Deals with Proposal Requested.
                        $linked_sql = $sql . "and (dealstatus.ds_status='requested' or dealstatus.ds_status='presented')";

                        $linked1 += $this->PDO->select($linked_sql, array('cpd_id' => $linked_cpd, 'quarter' => $period - 1))[0]->net;

                        $tgd->linked[$cells[0]->tgd_ic_c_id][$period] = $linked1;

                        // Deals Listed .
                        $linked_sql = $sql . "and (dealstatus.ds_status='listed' or dealstatus.ds_status='launched')";
                        $linked2 += $this->PDO->select($linked_sql, array('cpd_id' => $linked_cpd, 'quarter' => $period - 1))[0]->net;
                        $tgd->linked[$cells[1]->tgd_ic_c_id][$period] = $linked2;

                        // Deals in Contract.
                        $linked_sql = $sql . "and (dealstatus.ds_status='contract' or dealstatus.ds_status='hard')";
                        $linked3 += $this->PDO->select($linked_sql, array('cpd_id' => $linked_cpd, 'quarter' => $period - 1))[0]->net;
                        $tgd->linked[$cells[2]->tgd_ic_c_id][$period] = $linked3;

                        // Linked TGD active CPD Amount Details.
                        $sql = "select sum(hca_net) as deal_net"
                            . " from homebase_client_asset"
                            . " join deal on hca_id = deal_asset_id"
                            . " join (select has_status, hs1.has_datetime, has_id, has_asset_id, has_md"
                            . " from homebase_asset_status hs1"
                            . " join (select max(has_datetime) as time"
                            . " from homebase_asset_status"
                            . " where quarter(has_datetime)=:quarter"
                            . " group by has_asset_id) as hs2"
                            . " on hs1.has_datetime = hs2.time) as assetstatus on hca_id=assetstatus.has_asset_id"
                            . " WHERE deal_cpd_id =:cpd_id  and assetstatus.has_status='complete'";

                        $linked_cpd = $this->PDO->select($sql, array('cpd_id' => $linked_cpd, 'quarter' => $period - 1));

                        // Linked TGD other income  Details.
                        $sql = 'select coalesce(sum(tgd_other_income.toi_amount), 0) as tgd_net'
                            . ' from tgd_other_income'
                            . ' where toi_tgd_ui_id=:tgd_id and toi_qtr=:quarter';
                        $linked_oi = $this->PDO->select($sql, array('tgd_id' => $linked_tgd, 'quarter' => $period));

                        $linked4 += (isset($linked_oi[0]) ? $linked_oi[0]->tgd_net : 0) + (isset($linked_cpd[0]) ? $linked_cpd[0]->deal_net : 0);
                        $tgd->linked[$cells[3]->tgd_ic_c_id][$period] = $linked4;
                    }
                }
            }

            // Get the list of shares on the TGD.
            $sql = "select user_email, share.*"
                . " from share"
                . " join pst_wp.wp_users on share_with_id=ID"
                . " where share_object_type='tgd' and share_object_id=:object_id"
                . " order by user_email";
            $tgd->shares = $this->PDO->select($sql, array('object_id' => $tgd_ui_id));
        } catch (Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }

        // Commit the transation and return the TGD
        $this->PDO->commit_transaction();
        return $tgd;
    }

    /**
     * Update the specified instance.
     */
    public function update_instance($ui_id, $name, $value)
    {
        // Create the sql to do the update.
        $sql = "update tgd_user_instance"
            . " set " . $name . "=:value, tgd_ui_updated=now()"
            . " where tgd_ui_id=:ui_id";
        $params['value'] = $value;
        $params['ui_id'] = $ui_id;
        $this->PDO->update($sql, $params);

        // If the check size is changed, change all of the quarterly check sizes.
        if ($name == 'tgd_ui_default_check_size') {
            $sql = "update tgd_instance_multiplier"
                . " join tgd_multiplier on tgd_im_m_id=tgd_m_id"
                . " set tgd_im_multiple=:value"
                . " where tgd_im_ui_id=:ui_id and tgd_m_name='Average Check Size'";
            $params = array();
            $params['value'] = $value;
            $params['ui_id'] = $ui_id;
            $this->PDO->update($sql, $params);
        }
    }

    /**
     * Update the specified cell.
     */
    public function update_cell($ui_id, $condition_id, $period, $name, $value, $direction = 'forward')
    {
        // Create the sql to do the update.
        $sql = "update tgd_instance_cell"
            . " set " . $name . "=:value"
            . " where tgd_ic_ui_id=:ui_id and tgd_ic_c_id=:condition_id and tgd_ic_period=:period";

        $params['value'] = $value;
        $params['ui_id'] = $ui_id;
        $params['condition_id'] = $condition_id;
        $params['period'] = $period;

        $this->PDO->update($sql, $params);

        // If the update is to a condition with no previous conditions, set the value in all such conditions
        // that have a 0 value and a higher period number.
        if ($name != 'tgd_ic_actual') {

            $sql = " update tgd_instance_cell"
                . " set tgd_ic_target_t=0"
                . " where tgd_ic_ui_id=:ui_id and tgd_ic_c_id=:condition_id and tgd_ic_period=:period";

            $this->PDO->update($sql, array('ui_id' => $ui_id, 'condition_id' => $condition_id, 'period' => $period));

            // Updates every TGD FTG for metric 3 if any period has value equal to zero.
//            $sql = "update tgd_instance_cell"
//                . " join tgd_condition on tgd_ic_c_id=tgd_c_id"
//                . " set " . $name . "=:value"
//                . " where tgd_ic_ui_id=:ui_id and tgd_ic_c_id=:condition_id and tgd_ic_period>:period"
//                . " and " . $name . "=0 and tgd_c_prev_condition is null";

//            $this->PDO->update($sql, $params);

            // Propagate the update.
            if ($direction == 'forward') {
                for (; $period <= 6; $period++) {
                    $this->_propagate_update($ui_id, $condition_id, $period);
                }
            } else {
                $this->_propagate_update_backward($ui_id, $condition_id, $period);
            }
        }

        // Set the updated time on the instance
        $sql = "update tgd_user_instance set tgd_ui_updated=now()"
            . " where tgd_ui_id=:ui_id";
        $this->PDO->update($sql, array('ui_id' => $ui_id));

        return true;
    }

    /**
     * Propogate an update.
     */
    private function _propagate_update($ui_id, $condition_id, $period)
    {
        // Update all of the subsequent cells.
        do {
            // Look for the next cell in the flow.
            $sql = "select tgd_ic_id, tgd_c_type, tgd_ic_c_id, tgd_ic_period"
                . " from tgd_instance_cell"
                . " join tgd_condition on tgd_ic_c_id=tgd_c_id"
                . " where tgd_c_prev_condition=:condition_id and tgd_ic_ui_id=:ui_id"
                . " and tgd_ic_period=(:period+1)";

            $params = array();
            $params['ui_id'] = $ui_id;
            $params['condition_id'] = $condition_id;
            $params['period'] = $period;
            $next_cell = $this->PDO->select_row($sql, $params);

            // If there is a next cell, update it.
            if ($next_cell) {

                // If the next cell is an objective, get the multipliers for the period.
                $multiple = 1;
                $additional = 0;
                if ($next_cell->tgd_c_type == 'obj') {
                    $sql = "select (case when tgd_m_type != 'percent' then tgd_im_multiple"
                        . " else (1+tgd_im_multiple) end) as multiple, tgd_m_operation"
                        . " from tgd_instance_multiplier"
                        . " join tgd_multiplier on tgd_im_m_id=tgd_m_id"
                        . " where tgd_im_ui_id=:ui_id and tgd_im_period=(:period+1)";
                    $multipliers = $this->PDO->select($sql, array('ui_id' => $ui_id, 'period' => $period));

                    foreach ($multipliers as $multiplier) {
                        $multiplier->tgd_m_operation == 'mult' ? $multiple *= $multiplier->multiple :
                            $additional += $multiplier->multiple;
                    }
                }

                // Get the current cell.
                $sql = "select *"
                    . " from tgd_instance_cell"
                    . " join tgd_instance_condition on tgd_icond_ui_id=tgd_ic_ui_id and tgd_icond_c_id=tgd_ic_c_id"
                    . " where tgd_ic_ui_id=:ui_id and tgd_ic_c_id=:condition_id and tgd_ic_period=:period";

                $current_cell = $this->PDO->select_row($sql, $params);

                // Check if current cell is empty.
                if (!empty($current_cell)) {

                    // Update the next cell based on the current cell values.
                    $sql = "update tgd_instance_cell set"
                        . " tgd_ic_floor=:floor,"
                        . " tgd_ic_target_p=:target,"
                        . " tgd_ic_game=:game,"
                        . " tgd_ic_actual=:actual"
                        . " where tgd_ic_id=:ic_id";

                    $params = array();
                    $params['floor'] = (floor(($current_cell->tgd_ic_floor + $current_cell->tgd_ic_target_t) *
                                $current_cell->tgd_icond_conversion_rate) * $multiple) + $additional;
                    $params['target'] = (floor(($current_cell->tgd_ic_target_p + $current_cell->tgd_ic_target_t) *
                                $current_cell->tgd_icond_conversion_rate) * $multiple) + $additional;
                    $params['game'] = (floor(($current_cell->tgd_ic_game + $current_cell->tgd_ic_target_t) *
                                $current_cell->tgd_icond_conversion_rate) * $multiple) + $additional;
                    $params['actual'] = (floor($current_cell->tgd_ic_actual * $current_cell->tgd_icond_conversion_rate) *
                            $multiple) + $additional;
                    $params['ic_id'] = $next_cell->tgd_ic_id;

                    $this->PDO->update($sql, $params);
                }
                // Update the condition and period.
                $condition_id = $next_cell->tgd_ic_c_id;
                $period = $next_cell->tgd_ic_period;
            }

        } while ($next_cell !== false);
        return true;
    }

    /**
     * Propogate an update backwards.
     */
    private function _propagate_update_backward($ui_id, $condition_id, $period)
    {
        // Update all of the subsequent cells.
        do {
            // Look for the previous cell in the flow.
            $sql = "select tgd_ic_id, tgd_c_type, tgd_ic_c_id, tgd_ic_period, tgd_icond_conversion_rate"
                . " from tgd_instance_cell"
                . " join tgd_condition on tgd_c_id=:condition_id"
                . " join tgd_instance_condition on tgd_icond_ui_id=tgd_ic_ui_id and tgd_icond_c_id=tgd_c_prev_condition"
                . " where tgd_ic_c_id=tgd_c_prev_condition and tgd_ic_ui_id=:ui_id"
                . " and tgd_ic_period=(:period-1) and tgd_icond_conversion_rate!=0.00";
            $params = array();
            $params['ui_id'] = $ui_id;
            $params['condition_id'] = $condition_id;
            $params['period'] = $period;
            $prev_cell = $this->PDO->select_row($sql, $params);

            // If there is a previous cell, update it.
            if ($prev_cell) {
                echo "Previous cell ID: $prev_cell->tgd_ic_id";

                // Get the current cell.
                $sql = "select tgd_ic_id, tgd_ic_target_p, tgd_c_type"
                    . " from tgd_instance_cell"
                    . " join tgd_condition on tgd_ic_c_id=tgd_c_id"
                    . " where tgd_ic_ui_id=:ui_id and tgd_ic_c_id=:condition_id and tgd_ic_period=:period";
                $current_cell = $this->PDO->select_row($sql, $params);
                echo "Current Cell: $current_cell->tgd_ic_id";

                // If the current cell is an objective, get the multipliers for the period.
                $multiple = 1;
                $additional = 0;
                if ($current_cell->tgd_c_type == 'obj') {
                    $sql = "select (case when tgd_m_type != 'percent' then tgd_im_multiple else (1+tgd_im_multiple) end)"
                        . " as multiple, tgd_m_operation"
                        . " from tgd_instance_multiplier"
                        . " join tgd_multiplier on tgd_im_m_id=tgd_m_id"
                        . " where tgd_im_ui_id=:ui_id and tgd_im_period=(:period+1)";
                    $multipliers = $this->PDO->select($sql, array('ui_id' => $ui_id, 'period' => $period));
                    foreach ($multipliers as $multiplier) {
                        ($multiplier->tgd_m_operation == 'mult') ? $multiple *= $multiplier->multiple :
                            $additional += $multiplier->multiple;
                    }
                }
                echo "Multiple: $multiple";
                echo "Additional: $additional";

                // Update the previous cell's target based on the current cell values.
                $sql = "update tgd_instance_cell set"
                    . " tgd_ic_target_p=:target"
                    . " where tgd_ic_id=:ic_id";
                $params = array();
                $target = $current_cell->tgd_ic_target_p - $additional;
                echo "Target 1: $target";
                $target = ceil($target / $multiple);
                echo "Target 2: $target";
                $target = ceil($target / $prev_cell->tgd_icond_conversion_rate);
                echo "Target 3: $target";
                $params['target'] = $target;
                $params['ic_id'] = $prev_cell->tgd_ic_id;
                $this->PDO->update($sql, $params);

                // Update the condition and period.
                $condition_id = $prev_cell->tgd_ic_c_id;
                $period = $prev_cell->tgd_ic_period;
            }

        } while ($prev_cell !== false);
        return true;
    }

    /**
     * Propagate the FTG values from the specified period.
     */
    public function propagate_ftg($ui_id, $period)
    {
        // Get the condition ID and the values that need to be progagated.
        $sql = "select tgd_ic_c_id,"
            . " (sum(tgd_ic_floor) + sum(tgd_ic_target_t)) as tgd_ic_floor,"
            . " (sum(tgd_ic_target_p) + sum(tgd_ic_target_t)) as tgd_ic_target_p,"
            . " (sum(tgd_ic_game) + sum(tgd_ic_target_t)) as tgd_ic_game from tgd_instance_cell"
            . " join tgd_condition on tgd_ic_c_id=tgd_c_id"
            . " where tgd_ic_ui_id=:ui_id and tgd_ic_period=:period and tgd_c_prev_condition is null"
            . " group by tgd_ic_id";

        $cell = $this->PDO->select_row($sql, array('ui_id' => $ui_id, 'period' => $period));
        $condition_id = $cell->tgd_ic_c_id;

        // Update the tgd_ic_floor, tgd_ic_target_p, and tgd_ic_game values from the initial condition.
        $sql = "update tgd_instance_cell"
            . " join tgd_condition on tgd_ic_c_id=tgd_c_id"
            . " set tgd_ic_floor=:value"
            . " where tgd_ic_ui_id=:ui_id and tgd_ic_c_id=:condition_id and tgd_ic_period>:period"
            . " and tgd_c_prev_condition is null";
        $this->PDO->update($sql, array('ui_id' => $ui_id, 'condition_id' => $condition_id, 'period' => $period,
            'value' => $cell->tgd_ic_floor));

        $sql = "update tgd_instance_cell"
            . " join tgd_condition on tgd_ic_c_id=tgd_c_id"
            . " set tgd_ic_target_p=:value"
            . " where tgd_ic_ui_id=:ui_id and tgd_ic_c_id=:condition_id and tgd_ic_period>:period"
            . " and tgd_c_prev_condition is null";

        $this->PDO->update($sql, array('ui_id' => $ui_id, 'condition_id' => $condition_id, 'period' => $period,
            'value' => $cell->tgd_ic_target_p));

        $sql = "update tgd_instance_cell"
            . " join tgd_condition on tgd_ic_c_id=tgd_c_id"
            . " set tgd_ic_game=:value"
            . " where tgd_ic_ui_id=:ui_id and tgd_ic_c_id=:condition_id and tgd_ic_period>:period"
            . " and tgd_c_prev_condition is null";
        $this->PDO->update($sql, array('ui_id' => $ui_id, 'condition_id' => $condition_id, 'period' => $period,
            'value' => $cell->tgd_ic_game));

        // Propagate the update.
        for (; $period <= 6; $period++) {
            $this->_propagate_update($ui_id, $condition_id, $period);
        }

        // Set the updated time on the instance
        $sql = "update tgd_user_instance set tgd_ui_updated=now()"
            . " where tgd_ui_id=:ui_id";
        $this->PDO->update($sql, array('ui_id' => $ui_id));

        return true;
    }

    // Undo previous toggle made by user.
    public function undo_toggle($ui_id)
    {
        $sql = ' select tph_prev_cond_id, tph_prev_period, tph_current_period, tph_current_cond_id, tph_id'
            . ' from tgd_toggle_history'
            . ' where tph_tgd_ui_id=:ui_id and tph_active=1'
            . ' order by tph_id desc limit 1';

        $params = $this->PDO->select($sql, array('ui_id' => $ui_id));

        if (empty($params)) {
            return false;
        }

        $params = $params[0];
        $sql = "update tgd_instance_cell"
            . " set tgd_ic_target_t=tgd_ic_target_t + 1"
            . " where tgd_ic_ui_id=:ui_id and tgd_ic_c_id=:condition_id and tgd_ic_period=:period";

        $this->PDO->update($sql, array('ui_id' => $ui_id, 'condition_id' => $params->tph_prev_cond_id,
            'period' => $params->tph_prev_period));

        $sql = "update tgd_instance_cell"
            . " set tgd_ic_target_t=tgd_ic_target_t - 1"
            . " where tgd_ic_ui_id=:ui_id and tgd_ic_c_id=:condition_id and tgd_ic_period=:period";

        $this->PDO->update($sql, array('ui_id' => $ui_id, 'condition_id' => $params->tph_current_cond_id,
            'period' => $params->tph_current_period));

        $sql = 'update tgd_toggle_history'
            . ' set tph_active=:active'
            . ' where tph_id=:id';

        $this->PDO->update($sql, array('active' => 0, 'id' => $params->tph_id));

        // Propagate this value.
        $this->_propagate_update($ui_id, $params->tph_prev_cond_id, $params->tph_prev_period);

        // Set the updated time on the instance
        $sql = "update tgd_user_instance"
            . " set tgd_ui_updated=now()"
            . " where tgd_ui_id=:ui_id";
        $this->PDO->update($sql, array('ui_id' => $ui_id));

        return true;
    }

    function redo_toggle($ui_id)
    {
        $sql = ' select tph_prev_cond_id, tph_prev_period, tph_current_period, tph_current_cond_id, tph_id'
            . ' from tgd_toggle_history'
            . ' where tph_tgd_ui_id=:ui_id and tph_active=0'
            . ' order by tph_id asc limit 1';

        $params = $this->PDO->select($sql, array('ui_id' => $ui_id));

        if (empty($params)) {
            return false;
        }

        $params = $params[0];
        $sql = "update tgd_instance_cell"
            . " set tgd_ic_target_t=tgd_ic_target_t - 1"
            . " where tgd_ic_ui_id=:ui_id and tgd_ic_c_id=:condition_id and tgd_ic_period=:period";

        $this->PDO->update($sql, array('ui_id' => $ui_id, 'condition_id' => $params->tph_prev_cond_id,
            'period' => $params->tph_prev_period));

        $sql = "update tgd_instance_cell"
            . " set tgd_ic_target_t=tgd_ic_target_t + 1"
            . " where tgd_ic_ui_id=:ui_id and tgd_ic_c_id=:condition_id and tgd_ic_period=:period";

        $this->PDO->update($sql, array('ui_id' => $ui_id, 'condition_id' => $params->tph_current_cond_id,
            'period' => $params->tph_current_period));

        $sql = 'update tgd_toggle_history'
            . ' set tph_active=:active'
            . ' where tph_id=:id';

        $this->PDO->update($sql, array('active' => 1, 'id' => $params->tph_id));

        // Propagate this value.
        $this->_propagate_update($ui_id, $params->tph_prev_cond_id, $params->tph_prev_period);

        // Set the updated time on the instance
        $sql = "update tgd_user_instance"
            . " set tgd_ui_updated=now()"
            . " where tgd_ui_id=:ui_id";
        $this->PDO->update($sql, array('ui_id' => $ui_id));

        return true;
    }

    /**
     * Decrement the specified cell's target value by one and increment the "destinations" target value by one.
     */
    public function adjust_cell_target($ui_id, $condition_id, $period, $direction)
    {

        // We can't go below zero, so get the current value and see if it can be decremented.
        $sql = "select tgd_ic_target_p, tgd_ic_target_t"
            . " from tgd_instance_cell"
            . " where tgd_ic_ui_id=:ui_id and tgd_ic_c_id=:condition_id and tgd_ic_period=:period";

        $params['ui_id'] = $ui_id;
        $params['condition_id'] = $condition_id;
        $params['period'] = $period;
        $tgd_ic = $this->PDO->select_row($sql, $params);

        if ((intval($tgd_ic->tgd_ic_target_p) + intval($tgd_ic->tgd_ic_target_t)) < 1) {
            return false;
        }

        // Create the sql to do the update.
        $sql = "update tgd_instance_cell"
            . " set tgd_ic_target_t=tgd_ic_target_t - 1"
            . " where tgd_ic_ui_id=:ui_id and tgd_ic_c_id=:condition_id and tgd_ic_period=:period";
        $params['ui_id'] = $ui_id;
        $params['condition_id'] = $condition_id;
        $params['period'] = $period;
        $prev_period = $period;
        $prev_condition_id = $condition_id;

        $this->PDO->update($sql, $params);

        // Propagate this value.
        $this->_propagate_update($ui_id, $condition_id, $period);

        if ($direction == 'left') {
            $period = $period - 1;
        } else if ($direction == 'right') {
            $period = $period + 1;
        } else if ($direction == 'up') {
            $sql = "select tgd_c_id from tgd_condition where tgd_c_prev_condition=:condition_id";
            $cell = $this->PDO->select_row($sql, array('condition_id' => $condition_id));
            $condition_id = $cell->tgd_c_id;
        } else if ($direction == 'down') {
            $sql = "select tgd_c_prev_condition from tgd_condition where tgd_c_id=:condition_id";
            $cell = $this->PDO->select_row($sql, array('condition_id' => $condition_id));
            $condition_id = $cell->tgd_c_prev_condition;
        }

        // Create the sql to do the update on the destination cell
        $sql = "update tgd_instance_cell"
            . " set tgd_ic_target_t=tgd_ic_target_t + 1"
            . " where tgd_ic_ui_id=:ui_id and tgd_ic_period=:period"
            . " and tgd_ic_c_id=:condition_id";

        $params['ui_id'] = $ui_id;
        $params['condition_id'] = $condition_id;
        $params['period'] = $period;
        $this->PDO->update($sql, $params);

        // Propagate this value.
        $this->_propagate_update($ui_id, $condition_id, $period);

        // Set the updated time on the instance
        $sql = "update tgd_user_instance set tgd_ui_updated=now()"
            . " where tgd_ui_id=:ui_id";
        $this->PDO->update($sql, array('ui_id' => $ui_id));

        $this->add_toggle_history($ui_id, $condition_id, $prev_condition_id, $period, $prev_period);

        return true;
    }


    /**
     * Adds toggle history to Database.
     *
     * @param $ui_id                Int TGD User Instance ID.
     * @param $current_condition_id Int Current Condition ID.
     * @param $prev_condition_id    Int Previous Condition ID.
     * @param $current_period       Int Current Period.
     * @param $prev_period          Int Previous Period.
     * @return bool
     */
    public function add_toggle_history($ui_id, $current_condition_id, $prev_condition_id, $current_period, $prev_period)
    {
        $sql = ' insert into tgd_toggle_history'
            . ' (tph_prev_period, tph_current_period, tph_prev_cond_id, tph_current_cond_id, tph_tgd_ui_id)'
            . ' values(:prev_period, :current_period, :prev_cond_id, :current_cond_id, :ui_id)';

        return $this->PDO->insert($sql, array('prev_period' => $prev_period, 'current_period' => $current_period,
            'prev_cond_id' => $prev_condition_id, 'current_cond_id' => $current_condition_id, 'ui_id' => $ui_id));

    }


    /**
     * Recalculate all instances.
     */
    public function recalc_all()
    {

        // Get all of the instances
        $sql = "select * from tgd_user_instance";
        $instances = $this->PDO->select($sql);

        // Recalulate all of them.
        foreach ($instances as $instance) {
            $this->recalculate($instance->tgd_ui_user_id, $instance->tgd_ui_id);
        }
    }

    /**
     * Recalculate all of the propagated values.
     */
    public function recalculate($ui_id, $locked_quarter)
    {
        // Zero all of the toggle moves.
        $sql = "update tgd_instance_cell set tgd_ic_target_t=0"
            . " where tgd_ic_ui_id=:ui_id and tgd_ic_period>:locked_quarter";
        $this->PDO->update($sql, array('ui_id' => $ui_id, 'locked_quarter' => $locked_quarter));

        // Get all of the conditions.
        $sql = "select tgd_icond_c_id"
            . " from tgd_instance_condition"
            . " join tgd_condition on tgd_icond_c_id=tgd_c_id"
            . " where tgd_c_type != 'obj' and tgd_icond_ui_id=:ui_id";
        $conditions = $this->PDO->select($sql, array('ui_id' => $ui_id));

        // Recalculate each condition starting with the initial period.
        foreach ($conditions as $condition) {
            $this->_propagate_update($ui_id, $condition->tgd_icond_c_id, (0 !== $locked_quarter ? $locked_quarter : 1));
        }

        // Get the initial condition.
        $sql = "select tgd_icond_c_id"
            . " from tgd_instance_condition"
            . " join tgd_condition on tgd_icond_c_id=tgd_c_id"
            . " where tgd_c_prev_condition is null and tgd_icond_ui_id=:ui_id";
        $condition = $this->PDO->select_row($sql, array('ui_id' => $ui_id));

        for ($period = (2 > $locked_quarter ? 2 : $locked_quarter); $period <= 6; $period++) {
            $this->_propagate_update($ui_id, $condition->tgd_icond_c_id, $period);
        }

        // Remove All the toggle history for this TGD instance.
        $sql = 'delete from tgd_toggle_history'
            . ' where tph_tgd_ui_id=:ui_id';
        $remove_history = $this->PDO->delete($sql, array('ui_id' => $ui_id));

        return true;
    }

    /**
     * Update the objectives
     */
    public function update_objectives($ui_id)
    {
        // Get the conditions that feed the objectives.
        $sql = "select tgd_icond_c_id"
            . " from tgd_instance_condition"
            . " join tgd_condition on tgd_icond_c_id=tgd_c_prev_condition and tgd_c_type='obj'"
            . " where tgd_icond_ui_id=:ui_id";
        $condition = $this->PDO->select_row($sql, array('ui_id' => $ui_id));
        for ($period = 1; $period <= 6; $period++) {
            $this->_propagate_update($ui_id, $condition->tgd_icond_c_id, $period);
        }

        return true;
    }

    /**
     * Update the specified multiplier.
     */
    public function update_multiplier($ui_id, $multiplier_id, $period, $name, $value)
    {

        // Create the sql to do the update.
        $sql = "update tgd_instance_multiplier"
            . " join tgd_multiplier on tgd_im_m_id=tgd_m_id"
            . " set " . $name . "=(case when tgd_m_type = 'percent' then (:value / 100) else :value end)"
            . " where tgd_im_ui_id=:ui_id and tgd_im_m_id=:multiplier_id and tgd_im_period=:period";
        $params['value'] = $value;
        $params['ui_id'] = $ui_id;
        $params['multiplier_id'] = $multiplier_id;
        $params['period'] = $period;
        $this->PDO->update($sql, $params);

        // If we are updating the first period, propogate the value to all other periods that have a 0 value.
//        if ($period == 1) {
        $sql = "update tgd_instance_multiplier"
            . " join tgd_multiplier on tgd_im_m_id=tgd_m_id"
            . " set " . $name . "=(case when tgd_m_type = 'percent' then (:value / 100) else :value end)"
            . " where tgd_im_ui_id=:ui_id and tgd_im_m_id=:multiplier_id and tgd_im_period>:period"
            . " and tgd_m_propagate='YES'";

        $params['value'] = $value;
        $params['ui_id'] = $ui_id;
        $params['multiplier_id'] = $multiplier_id;
        $params['period'] = $period;
        $this->PDO->update($sql, $params);
//        }

        // Cause a recalculation based on the new multiplier for each condition that feeds this objective.  Just to be sure, do
        // all periods.
        $sql = "select tgd_c_prev_condition from tgd_condition where tgd_c_type='obj'";
        $conditions = $this->PDO->select($sql, array());
        foreach ($conditions as $condition) {
            for (; $period <= 6; $period++) {
                $this->_propagate_update($ui_id, $condition->tgd_c_prev_condition, ($period - 1));
            }
        }

        // Set the updated time on the instance
        $sql = "update tgd_user_instance set tgd_ui_updated=now()"
            . " where tgd_ui_id=:ui_id";
        $this->PDO->update($sql, array('ui_id' => $ui_id));

        return true;
    }

    /**
     * Update the specified condition.
     */
    public function update_condition($ui_id, $condition_id, $name, $value, $locked)
    {

        // Create the sql to do the update.
        $sql = "update tgd_instance_condition"
            . " set " . $name . "=(:value / 100)"
            . " where tgd_icond_ui_id=:ui_id and tgd_icond_c_id=:condition_id";

        $params['value'] = $value;
        $params['ui_id'] = $ui_id;
        $params['condition_id'] = $condition_id;
        $this->PDO->update($sql, $params);

        // Propagate the change for all of the affected cells.
        for ($period = $locked; $period <= 6; $period++) {
            $this->_propagate_update($ui_id, $condition_id, $period);
        }

        // Set the updated time on the instance
        $sql = "update tgd_user_instance set tgd_ui_updated=now()"
            . " where tgd_ui_id=:ui_id";
        $this->PDO->update($sql, array('ui_id' => $ui_id));

        return true;
    }

    public function update_condition_text($id, $value, $year)
    {
        $sql = 'update tgd_user_instance'
            . ' set tgd_ui_condition=:value'
            . ' where tgd_ui_id=:id and tgd_ui_year=:year';

        return $this->PDO->update($sql, array('value' => $value, 'id' => $id, 'year' => $year));
    }

    /**
     * Delete an instance.
     */
    public function delete_instance($ui_id)
    {
        // Delete the instance.
        $sql = "delete from tgd_user_instance where tgd_ui_id=:tgd_ui_id";
        return $this->PDO->insert($sql, array('tgd_ui_id' => $ui_id));
    }

    /**
     * Copy an existing instance to a new instance.
     */
    public function copy_instance($old_ui_id, $save_as_name, $save_as_id)
    {
        // This is a multi-step process, so make sure they all work.
        try {
            $this->PDO->begin_transaction();

            // If the ID is set and the name is not set, replace the existing instance.  The easiest way to do this is to
            // get the name of the existing instance, delete it and the replace it.
            if (empty($save_as_name) && !empty($save_as_id)) {
                $sql = "select tgd_ui_name, tgd_ui_created from tgd_user_instance where tgd_ui_id=:save_as_id";
                $ui = $this->PDO->select_row($sql, array('save_as_id' => $save_as_id));
                $save_as_name = $ui->tgd_ui_name;
                $created_date = $ui->tgd_ui_created;
                $sql = "delete from tgd_user_instance where tgd_ui_id=:save_as_id";
                $this->PDO->delete($sql, array('save_as_id' => $save_as_id));
            } else {
                $created_date = date('Y-m-d H:i:s');
            }

            // Create the new instance record as a copy.
            $sql = "insert into tgd_user_instance (tgd_ui_user_id, tgd_ui_gt_id, tgd_ui_name, tgd_ui_mission,"
                . " tgd_ui_year, tgd_ui_floor, tgd_ui_target, tgd_ui_game, tgd_ui_default_check_size, tgd_ui_periods_final,"
                . " tgd_ui_py_cum_floor, tgd_ui_py_cum_target, tgd_ui_py_cum_game, tgd_ui_created, tgd_ui_updated,"
                . " tgd_ui_tgt_q1_pct, tgd_ui_tgt_q1_obj, tgd_ui_tgt_q2_pct, tgd_ui_tgt_q2_obj, tgd_ui_tgt_q3_pct,"
                . " tgd_ui_tgt_q3_obj, tgd_ui_tgt_q4_pct, tgd_ui_tgt_q4_obj, tgd_ui_tgt_q5_pct, tgd_ui_tgt_q5_obj,"
                . " tgd_ui_tgt_q6_pct, tgd_ui_tgt_q6_obj, tgd_ui_product_id)"
                . " select tgd_ui_user_id, tgd_ui_gt_id, :name, tgd_ui_mission, tgd_ui_year, tgd_ui_floor, tgd_ui_target,"
                . " tgd_ui_game, tgd_ui_default_check_size, tgd_ui_periods_final, tgd_ui_py_cum_floor, tgd_ui_py_cum_target,"
                . " tgd_ui_py_cum_game, :created, now(), tgd_ui_tgt_q1_pct, tgd_ui_tgt_q1_obj, tgd_ui_tgt_q2_pct, tgd_ui_tgt_q2_obj,"
                . " tgd_ui_tgt_q3_pct, tgd_ui_tgt_q3_obj, tgd_ui_tgt_q4_pct, tgd_ui_tgt_q4_obj, tgd_ui_tgt_q5_pct, tgd_ui_tgt_q5_obj,"
                . " tgd_ui_tgt_q6_pct, tgd_ui_tgt_q6_obj, tgd_ui_product_id"
                . " from tgd_user_instance where tgd_ui_id=:ui_id";
            $new_ui_id = $this->PDO->insert($sql, array('name' => $save_as_name, 'created' => $created_date, 'ui_id' => $old_ui_id));

            // Create the new instance's conditions
            $sql = "insert into tgd_instance_condition (tgd_icond_ui_id, tgd_icond_c_id, tgd_icond_conversion_rate)"
                . " select :new_ui_id, tgd_icond_c_id, tgd_icond_conversion_rate"
                . " from tgd_instance_condition where tgd_icond_ui_id=:ui_id";
            $this->PDO->insert($sql, array('new_ui_id' => $new_ui_id, 'ui_id' => $old_ui_id));

            // Create the new instance's multipliers
            $sql = "insert into tgd_instance_multiplier (tgd_im_ui_id, tgd_im_m_id, tgd_im_period, tgd_im_multiple)"
                . " select :new_ui_id, tgd_im_m_id, tgd_im_period, tgd_im_multiple"
                . " from tgd_instance_multiplier where tgd_im_ui_id=:ui_id";
            $this->PDO->insert($sql, array('new_ui_id' => $new_ui_id, 'ui_id' => $old_ui_id));

            // Create the instance cells.
            $sql = "insert into tgd_instance_cell (tgd_ic_ui_id, tgd_ic_c_id, tgd_ic_period, tgd_ic_floor,"
                . " tgd_ic_target, tgd_ic_game, tgd_ic_actual, tgd_ic_target_p, tgd_ic_target_t)"
                . " select :new_ui_id, tgd_ic_c_id, tgd_ic_period, tgd_ic_floor, tgd_ic_target, tgd_ic_game,"
                . " tgd_ic_actual, tgd_ic_target_p, tgd_ic_target_t"
                . " from tgd_instance_cell where tgd_ic_ui_id=:ui_id";
            $this->PDO->insert($sql, array('new_ui_id' => $new_ui_id, 'ui_id' => $old_ui_id));

            $sql = "insert into tgd_toggle_history"
                . " (tph_prev_period, tph_current_period, tph_prev_cond_id, tph_current_cond_id, tph_time,"
                . " tph_tgd_ui_id, tph_active)"
                . " select tph_prev_period, tph_current_period, tph_prev_cond_id, tph_current_cond_id, tph_time,"
                . " :new_ui_id, tph_active"
                . " from tgd_toggle_history"
                . " where tph_tgd_ui_id=:ui_id";

            $this->PDO->insert($sql, array('new_ui_id' => $new_ui_id, 'ui_id' => $old_ui_id));
        } catch (Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }

        // Commit the transation and return the TGD
        $this->PDO->commit_transaction();
        return $new_ui_id;
    }

    /**
     * Get the list of instances for the current user.
     */
    public function get_instances($user_id, $year, $product_id, $not_in_group = null, $tgd_id = null, $other_income = null)
    {
        // Find the list of tool groups available to the user.
        if ($not_in_group !== null) {
            $sql = "select tgd_ui_id, tgd_ui_name, tgd_ui_mission, tgd_ui_year, tgd_ui_created, tgd_ui_updated,"
                . " tgd_gt_id, tgd_gt_name, tgd_gt_description,"
                . " share_id, share_type"
                . " from tgd_user_instance"
                . " join tgd_game_type on tgd_ui_gt_id=tgd_gt_id"
                . " left join tgd_group_member on tgd_gm_member_id=tgd_ui_id and"
                . " tgd_gm_member_type='instance' and tgd_gm_g_id=:not_in_group"
                . " join tgd_group on tgd_g_year=tgd_ui_year and tgd_g_id=:not_in_group"
                . " left join share on share_object_type='tgd' and share_with_id=:user_id and share_object_id=tgd_ui_id"
                . " where (tgd_ui_user_id=:user_id or share_id is not null) and tgd_gm_id is null and"
                . " tgd_ui_product_id=:product_id and tgd_ui_year=:year"
                . " order by tgd_ui_name";
            return $this->PDO->select($sql, array('user_id' => $user_id, 'not_in_group' => $not_in_group, 'product_id' => $product_id, 'year' => $year));
        } // If for other income, make sure the TGD is not already included.
        else if ($other_income !== null) {
            $sql = "select tgd_ui_id, tgd_ui_name, tgd_ui_mission, tgd_ui_year, tgd_ui_created, tgd_ui_updated,"
                . " tgd_gt_id, tgd_gt_name, tgd_gt_description,"
                . " share_id, share_type, tgdi_oi_id, tgdi_oi_linked_tgd"
                . " from tgd_user_instance"
                . " join tgd_game_type on tgd_ui_gt_id=tgd_gt_id"
                . " left join ("
                . " select tgdi_oi_id, tgdi_oi_linked_tgd from tgdi_other_income"
                . " where tgdi_oi_tgd_id=:tgd_id and tgdi_oi_type='tgd'"
                . " ) oi on tgd_ui_id=tgdi_oi_linked_tgd"
                . " left join share on share_object_type='tgd' and share_with_id=:user_id and share_object_id=tgd_ui_id"
                . " where (tgd_ui_user_id=:user_id or share_id is not null) and oi.tgdi_oi_id is null"
                . " and tgd_ui_year=(select tgd_ui_year from tgd_user_instance where tgd_ui_id=:tgd_id)"
                . " and tgd_ui_product_id=:product_id and tgd_ui_year=:year"
                . " order by tgd_ui_name";
            return $this->PDO->select($sql, array('user_id' => $user_id, 'tgd_id' => $tgd_id, 'year' => $year, 'product_id' => $product_id));
        } else {
            $sql = "select tgd_ui_id, tgd_ui_name, tgd_ui_mission, tgd_ui_year, tgd_ui_created, tgd_ui_updated,"
                . " tgd_gt_id, tgd_gt_name, tgd_gt_description,"
                . " share_id, share_type"
                . " from tgd_user_instance"
                . " join tgd_game_type on tgd_ui_gt_id=tgd_gt_id"
                . " left join share on share_object_type='tgd' and share_with_id=:user_id and share_object_id=tgd_ui_id"
                . " where (tgd_ui_user_id=:user_id or share_id is not null)"
                . " and tgd_ui_product_id=:product_id and tgd_ui_year=:year"
                . " order by tgd_ui_name";

            return $this->PDO->select($sql, array('user_id' => $user_id, 'year' => $year, 'product_id' => $product_id));
        }
    }

    /**
     * Get the list of instances for the current user.
     */
    public function search_instances($user_id, $input, $product_id)
    {
        // Get the parameters.
        $draw = $input->getInt('draw');
        $start = $input->getInt('start');
        $length = $input->getInt('length');

        // Is there a search?
        $search = $_POST['search'];

        $search_val = '';

        if (!empty($search['value'])) {
            $search_val = $search['value'];
        }
        $ret['search_val'] = $search_val;
        $ret['search'] = $search;

        // Get the column information
        $columns = $_POST['columns'];

        // Get the columns and the search
        $sql_columns = '';
        $sep = ' ';
        $sql_where = ' where ((tgd_ui_user_id=' . $user_id . ' or share_id is not null)';
        $sep_where = ' and ';

        foreach ($columns as $column) {
            if (empty($column['data'])) {
                continue;
            }

            $search_col = $column['data'];
            $sql_columns .= $sep . $column['data'];
            $sep = ', ';

            // Add to the search?
            if (isset($search_val) && ($column['searchable'] == 'true')) {
                $sql_where .= $sep_where . "(cast(" . $search_col . " as char(100)) like '%" .
                    $search_val . "%' and (tgd_ui_user_id=" . $user_id . " or share_id is not null))";
                $sep_where = ' or ';
            }
        }

        // Determine the order information.
        $order = $_POST['order'];

        $sql_order = '';
        foreach ($order as $o) {

            (strlen($sql_order) > 0) ? $sql_order .= ', ' : $sql_order = " order by ";

            // Get the column
            $sql_order .= $columns[$o['column']]['data'] . ' ' . $o['dir'];
        }

        $sql_where .= ")";

        // Get the total number of rows (unfiltered).
        $sql = "select count(*) from tgd_user_instance"
            . " left join share on share_object_type='tgd' and share_with_id=:user_id and share_object_id=tgd_ui_id"
            . " where (tgd_ui_user_id=:user_id or share_id is not null)";
        $count_total = $this->PDO->select_count($sql, array('user_id' => $user_id));

        // Get the total number of rows (filtered).
        $sql = "select count(*)"
            . " from tgd_user_instance"
            . " join tgd_game_type on tgd_gt_id=tgd_ui_gt_id"
            . " left join share on share_object_type='tgd' and share_with_id=:user_id and share_object_id=tgd_ui_id"
            . $sql_where;

        $count_filtered = $this->PDO->select_count($sql, array('user_id' => $user_id));

        // Get the filtered data.
        $sql = " select " . $sql_columns . ", tgd_ui_product_id, group_concat(tl_linked_product_id) as linked_product_id,"
            . " uact_tgd_id from tgd_user_instance"
            . " join tgd_game_type on tgd_gt_id=tgd_ui_gt_id"
            . " join pst_wp_cpd.wp_users on tgd_ui_user_id=ID"
            . " left join tgd_linked on tl_tgd_id=tgd_ui_id"
            . " left join share on share_object_type='tgd' and share_with_id=:user_id and share_object_id=tgd_ui_id "
            . " left join user_active_cpd_tgd on uact_user_id=tgd_ui_user_id "
            . $sql_where
            . " group by tgd_ui_id" . $sql_order . " limit " . $start . ',' . $length;

        $data = $this->PDO->select($sql, array('user_id' => $user_id));

        // Set the return values.
        $ret['data'] = $data;
        $ret['draw'] = $draw;
        $ret['recordsTotal'] = $count_total;
        $ret['recordsFiltered'] = $count_filtered;
        return $ret;
    }

    /**
     * Add a new group.
     */
    public function add_group($user_id, $gt_id, $name, $year, $description)
    {
        // This is a multi-step process, so make sure they all work.
        try {
            $this->PDO->begin_transaction();

            // Create the group record.
            $sql = "insert into tgd_group (tgd_g_user_id, tgd_g_gt_id, tgd_g_name, tgd_g_year, tgd_g_description)"
                . " values (:user_id, :gt_id, :name, :year, :description)";
            $params = array('user_id' => $user_id,
                'gt_id' => $gt_id,
                'name' => $name,
                'year' => $year,
                'description' => $description);
            $tgd_g_id = $this->PDO->insert($sql, $params);
        } catch (Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }

        // Commit the transation and return the TGD
        $this->PDO->commit_transaction();
        return $tgd_g_id;
    }

    /**
     * Delete an group.
     */
    public function delete_group($g_id)
    {
        // Delete the group.
        $sql = "delete from tgd_group where tgd_g_id=:tgd_g_id";
        return $this->PDO->insert($sql, array('tgd_g_id' => $g_id));
    }

    /**
     * Add an instance to a group.
     */
    public function add_group_instance($g_id, $ui_id)
    {
        // This is a multistep process, so use a transaction.
        try {
            $this->PDO->begin_transaction();

            // Is the instance already in the group?
            $sql = "select * from tgd_group_member"
                . " where tgd_gm_g_id=:g_id and tgd_gm_member_id=:ui_id and tgd_gm_member_type='instance'";

            $gm = $this->PDO->select_row($sql, array('g_id' => $g_id, 'ui_id' => $ui_id));

            if ($gm !== false) {
                throw new Exception("The TGD Instance is already in the group.");
            }

            // Delete the group.
            $sql = "insert into tgd_group_member (tgd_gm_g_id, tgd_gm_member_type, tgd_gm_member_id)"
                . " values(:g_id, 'instance', :ui_id)";

            $this->PDO->insert($sql, array('g_id' => $g_id, 'ui_id' => $ui_id));
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            throw $e;
        }

        // Commit the transation and return the TGD
        $this->PDO->commit_transaction();
        return true;
    }

    /**
     * Delete an instance from a group.
     */
    public function delete_group_instance($g_id, $ui_id)
    {
        // Delete the group.
        $sql = "delete from tgd_group_member where tgd_gm_g_id=:g_id and tgd_gm_member_id=:ui_id and tgd_gm_member_type='instance'";
        return $this->PDO->delete($sql, array('g_id' => $g_id, 'ui_id' => $ui_id));
    }

    /**
     * Get the list of groups for the current user.
     */
    public function search_groups($user_id, $input)
    {
        // Get the parameters.
        $draw = $input->getInt('draw');
        $start = $input->getInt('start');
        $length = $input->getInt('length');

        // Is there a search?
        $search = $_POST['search'];
        $search_val = '';

        if (!empty($search['value'])) {
            $search_val = $search['value'];
        }
        $ret['search_val'] = $search_val;
        $ret['search'] = $search;

        // Get the column information
        $columns = $_POST['columns'];

        // Get the columns and the search
        $sql_columns = '';
        $sep = ' ';
        $sql_where = ' where tgd_g_user_id=' . $user_id;
        $sep_where = ' and ';

        foreach ($columns as $column) {
            if (empty($column['data'])) {
                continue;
            }

            $search_col = $column['data'];
            $sql_columns .= $sep . $column['data'];
            $sep = ', ';

            // Add to the search?
            if (isset($search_val) && ($column['searchable'] == 'true')) {
                $sql_where .= $sep_where . "cast(" . $search_col . " as char(100)) like '%" . $search_val . "%'";
                $sep_where = ' or ';
            }
        }

        // Determine the order information.

        $order = $_POST['order'];

        $sql_order = '';
        foreach ($order as $o) {
            (strlen($sql_order) > 0) ? $sql_order .= ', ' : $sql_order = " order by ";

            // Get the column
            $sql_order .= $columns[$o['column']]['data'] . ' ' . $o['dir'];
        }

        // Get the total number of rows (unfiltered).
        $sql = "select count(*) from tgd_group where tgd_g_user_id=:user_id";
        $count_total = $this->PDO->select_count($sql, array('user_id' => $user_id));

        // Get the total number of rows (filtered).
        $sql = "select count(*)"
            . " from tgd_group"
            . " join tgd_game_type on tgd_gt_id=tgd_g_gt_id"
            . $sql_where;
        $count_filtered = $this->PDO->select_count($sql, array());

        // Get the filtered data.
        $sql = " select " . $sql_columns . " from tgd_group"
            . " join tgd_game_type on tgd_gt_id=tgd_g_gt_id"
            . $sql_where
            . $sql_order
            . " limit " . $start . ',' . $length;
        $data = $this->PDO->select($sql, array());

        // Set the return values.
        $ret['sql_where'] = $sql_where;
        $ret['sql_order'] = $sql_order;
        $ret['sql_data'] = $sql;
        $ret['data'] = $data;
        $ret['draw'] = $draw;
        $ret['recordsTotal'] = $count_total;
        $ret['recordsFiltered'] = $count_filtered;
        return $ret;
    }

    /**
     * Get the specified TGD group.
     */
    public function get_group($user_id, $tgd_g_id)
    {
        // This is a multistep process, so use a transaction.
        try {
            $this->PDO->begin_transaction();

            // Find the group specified.
            $sql = "select tgd_g_id, tgd_g_name, tgd_g_description, tgd_g_year,"
                . " tgd_gt_id, tgd_gt_name, tgd_gt_description"
                . " from tgd_group"
                . " join tgd_game_type on tgd_g_gt_id=tgd_gt_id"
                . " where tgd_g_user_id=:user_id and tgd_g_id=:tgd_g_id";
            $group = $this->PDO->select_row($sql, array('user_id' => $user_id, 'tgd_g_id' => $tgd_g_id));
            if (empty($group)) {
                throw new Exception("Could not get the requested Tactical Game Design group");
            }

            // Get the instance conditions.
            $sql = "select tgd_c_id, tgd_c_name, tgd_c_description, tgd_c_tooltip, tgd_c_type, tgd_c_uom, tgd_c_prev_condition"
                . " from tgd_condition"
                . " join tgd_game_type on tgd_c_gt_id=tgd_gt_id"
                . " join tgd_group on tgd_g_gt_id=tgd_gt_id"
                . " where tgd_g_id=:tgd_g_id"
                . " order by 0-tgd_c_prev_condition desc";
            $group->conditions = $this->PDO->select($sql, array('tgd_g_id' => $tgd_g_id));

            // Get the members.
            $sql = "select tgd_ui_id, tgd_ui_user_id, tgd_ui_name"
                . " from tgd_group_member"
                . " join tgd_user_instance on tgd_gm_member_type='instance' and tgd_gm_member_id=tgd_ui_id"
                . " where tgd_gm_g_id=:tgd_g_id";
            $group->members = $this->PDO->select($sql, array('tgd_g_id' => $tgd_g_id));

            // Get the condition cells and arrange them in an easy way for the display.
            $group->cells = array();
            for ($period = 1; $period <= 6; $period++) {
                $group->cells[$period] = array();

                // Get the aggregate for each cell.
                $sql = "select tgd_ic_c_id, tgd_ic_period, sum(tgd_ic_floor) tgd_ic_floor, sum(tgd_ic_target)"
                    . " tgd_ic_target, sum(tgd_ic_game) tgd_ic_game, sum(tgd_ic_actual) tgd_ic_actual, "
                    . "sum(tgd_ic_target_p) tgd_ic_target_p, sum(tgd_ic_target_t) tgd_ic_target_t"
                    . " from tgd_group_member"
                    . " join tgd_user_instance on tgd_ui_id=tgd_gm_member_id"
                    . " join tgd_instance_cell on tgd_ic_ui_id=tgd_ui_id"
                    . " where tgd_gm_g_id=:tgd_g_id and tgd_gm_member_type='instance' and tgd_ic_period=:period"
                    . " group by tgd_ic_c_id, tgd_ic_period";

                // Get the cells and put them in an array indexed by their condition ID.
                $cells = $this->PDO->select($sql, array('tgd_g_id' => $tgd_g_id, 'period' => $period));
                foreach ($cells as $cell) {
                    $group->cells[$period][$cell->tgd_ic_c_id] = $cell;
                }
            }
        } catch (Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }

        // Commit the transation and return the TGD
        $this->PDO->commit_transaction();
        return $group;
    }

    /**
     * Update the number of periods finalized
     */
    public function finalize_period($tgd_ui_id, $num)
    {
        try {

            if ($num == 1) {
                $sql = "SELECT IF "
                    . " ((tgd_ui_periods_final = tgd_ui_periods_lock -1)"
                    . " OR (tgd_ui_periods_final = tgd_ui_periods_lock),  'Yes',  'No')"
                    . " AS tgd_result"
                    . " FROM tgd_user_instance"
                    . " WHERE tgd_ui_id=:ui_id";
                $result = $this->PDO->select($sql, array('ui_id' => $tgd_ui_id));

                if ($result[0]->tgd_result == 'No') {
                    throw new \UnexpectedValueException("Please lock Previous quarter first.");
                }
            } else if ($num == -1) {
                $sql = "SELECT IF "
                    . " ((tgd_ui_periods_final = tgd_ui_periods_lock +1)"
                    . " , 'Yes',  'No')"
                    . " AS tgd_result"
                    . " FROM tgd_user_instance"
                    . " WHERE tgd_ui_id=:ui_id";
                $result = $this->PDO->select($sql, array('ui_id' => $tgd_ui_id));

                if ($result[0]->tgd_result == 'No') {
                    throw new \UnexpectedValueException("Locked Quarters Can't be unlocked.");
                }
            }

            // Find the list of tool groups available to the user.
            $sql = "update tgd_user_instance set tgd_ui_periods_final=tgd_ui_periods_final+(:num) where tgd_ui_id=:ui_id";
        } catch (UnexpectedValueException $e) {
            return false;
        }
        return $this->PDO->select($sql, array('num' => $num, 'ui_id' => $tgd_ui_id));

    }

    /**
     * Update the number of periods Locked
     *
     * @param $tgd_ui_id INT  Tactical Game Designer ID.
     * @param $leap      Bool Whether it's the last quarter of year or not.
     * @param $year      Int  Current Year
     * @param $user_id   Int  User ID.
     * @param $num       Int  Increment Value.
     * @return array
     */
    public function lock_period($tgd_ui_id, $leap, $year, $user_id, $num)
    {
        try {
            $ret = [];

            // Find the list of tool groups available to the user.
            $sql = "update tgd_user_instance"
                . " set tgd_ui_periods_lock=tgd_ui_periods_lock+(:num)"
                . " where tgd_ui_id=:ui_id";

            $ret['quarter_locked'] = $this->PDO->update($sql, array('num' => $num, 'ui_id' => $tgd_ui_id));

            // Check if this is the last quarter of year. If yes create a new instance of TGD.
            if (true === $leap) {
                $sql = 'select tgd_ui_gt_id, tgd_ui_name, tgd_ui_mission, tgd_cpd_id, tgd_ui_product_id, tgd_ui_condition'
                    . ' from tgd_user_instance'
                    . ' where tgd_ui_id=:ui_id';

                $data = $this->PDO->select($sql, array('ui_id' => $tgd_ui_id));

                $ret['tgd_instance_id'] = $this->add_instance($user_id, $data[0]->tgd_ui_gt_id,
                    $data[0]->tgd_ui_name, ($year + 1), $data[0]->tgd_ui_mission,
                    $data[0]->tgd_cpd_id, $data[0]->tgd_ui_product_id, $data[0]->tgd_ui_condition);

                // Update Next Year ID in current TGD.
                $sql = 'update tgd_user_instance'
                    . ' set tgd_ui_next_year_id=:id'
                    . ' where tgd_ui_id=:current_id';

                $this->PDO->update($sql, array('id' => $ret['tgd_instance_id'], 'current_id' => $tgd_ui_id));

                // Update Prev Year ID in current TGD.
                $sql = 'update tgd_user_instance'
                    . ' set tgd_ui_prev_year_id=:current_id'
                    . ' where tgd_ui_id=:id';

                $this->PDO->update($sql, array('id' => $ret['tgd_instance_id'], 'current_id' => $tgd_ui_id));

                $sql = ' select tgd_ic_floor as floor, tgd_ic_target_p as target_p,'
                    . ' tgd_ic_target_t as target_t, tgd_ic_game as target_game, '
                    . ' tgd_ic_actual as tgd_actual, tgd_ic_c_id as c_id '
                    . ' from tgd_instance_cell'
                    . ' where tgd_ic_ui_id=:tgd_ic_ui_id and tgd_ic_period=5 limit 4';

                $result = $this->PDO->select($sql, array('tgd_ic_ui_id' => $tgd_ui_id));

                foreach ($result as $row) {
                    $sql = 'update tgd_instance_cell'
                        . ' set tgd_ic_floor=:floor, tgd_ic_target_p=:target_p, tgd_ic_target_t=:target_t,'
                        . ' tgd_ic_game=:target_game, tgd_ic_actual=:tgd_actual'
                        . ' where tgd_ic_ui_id=:ui_id and tgd_ic_period=1 and tgd_ic_c_id=:c_id';

                    $row_updated = $this->PDO->update($sql, array('floor' => $row->floor, 'target_p' => $row->target_p,
                        'target_t' => $row->target_t, 'target_game' => $row->target_game, 'tgd_actual' => $row->tgd_actual,
                        'c_id' => $row->c_id, 'ui_id' => $ret['tgd_instance_id']));
                }
            }

        } catch (\Exception $e) {
//            $this->PDO->rollback_transaction();
            return $e->getMessage();
        }
        return $ret;
    }

    public function leap_tgd($tgd_id, $year, $user_id)
    {

        try {
            $sql = 'select tgd_ui_gt_id, tgd_ui_name, tgd_ui_mission, tgd_cpd_id, tgd_ui_product_id, tgd_ui_condition'
                . ' from tgd_user_instance'
                . ' where tgd_ui_id=:ui_id';

            $data = $this->PDO->select($sql, array('ui_id' => $tgd_id));

            $ret['tgd_instance_id'] = $this->add_instance($user_id, $data[0]->tgd_ui_gt_id,
                $data[0]->tgd_ui_name, ($year + 1), $data[0]->tgd_ui_mission,
                $data[0]->tgd_cpd_id, $data[0]->tgd_ui_product_id, $data[0]->tgd_ui_condition);

            // Update Next Year ID in current TGD.
            $sql = 'update tgd_user_instance'
                . ' set tgd_ui_next_year_id=:id'
                . ' where tgd_ui_id=:current_id';

            $this->PDO->update($sql, array('id' => $ret['tgd_instance_id'], 'current_id' => $tgd_id));

            // Update Prev Year ID in current TGD.
            $sql = 'update tgd_user_instance'
                . ' set tgd_ui_prev_year_id=:current_id'
                . ' where tgd_ui_id=:id';

            $this->PDO->update($sql, array('id' => $ret['tgd_instance_id'], 'current_id' => $tgd_id));

            $sql = ' select tgd_ic_floor as floor, tgd_ic_target_p as target_p,'
                . ' tgd_ic_target_t as target_t, tgd_ic_game as target_game, '
                . ' tgd_ic_actual as tgd_actual, tgd_ic_c_id as c_id '
                . ' from tgd_instance_cell'
                . ' where tgd_ic_ui_id=:tgd_ic_ui_id and tgd_ic_period=5 limit 4';

            $result = $this->PDO->select($sql, array('tgd_ic_ui_id' => $tgd_id));

            foreach ($result as $row) {
                $sql = 'update tgd_instance_cell'
                    . ' set tgd_ic_floor=:floor, tgd_ic_target_p=:target_p, tgd_ic_target_t=:target_t,'
                    . ' tgd_ic_game=:target_game, tgd_ic_actual=:tgd_actual'
                    . ' where tgd_ic_ui_id=:ui_id and tgd_ic_period=1 and tgd_ic_c_id=:c_id';

                $row_updated = $this->PDO->update($sql, array('floor' => $row->floor, 'target_p' => $row->target_p,
                    'target_t' => $row->target_t, 'target_game' => $row->target_game, 'tgd_actual' => $row->tgd_actual,
                    'c_id' => $row->c_id, 'ui_id' => $ret['tgd_instance_id']));
            }
        } catch (\Exception $e) {

            echo $e;
            exit();
            $this->PDO->rollback_transaction();
        }
        return $ret;
    }


    /**
     * Add a new instance.
     */
    public function add_instance($user_id, $gt_id, $name, $year, $mission, $cpd_id, $product_id, $condition)
    {
        // This is a multi-step process, so make sure they all work.
        try {
            $this->PDO->begin_transaction();

            // Create the instance record.
            $sql = "insert"
                . " into tgd_user_instance"
                . " (tgd_ui_user_id, tgd_ui_gt_id, tgd_ui_name, tgd_ui_year,"
                . " tgd_ui_mission, tgd_ui_created, tgd_ui_updated, tgd_cpd_id, tgd_ui_product_id, tgd_ui_condition)"
                . " values (:user_id, :gt_id, :name, :year, :mission, now(), now(), :cpd_id, :product_id, :condition)";

            $params = array('user_id' => $user_id,
                'gt_id' => $gt_id,
                'name' => $name,
                'year' => $year,
                'mission' => $mission,
                'cpd_id' => $cpd_id,
                'product_id' => $product_id,
                'condition' => $condition);
            $tgd_ui_id = $this->PDO->insert($sql, $params);

            // Create the instance conditions
            $sql = "insert "
                . " into tgd_instance_condition"
                . " (tgd_icond_ui_id, tgd_icond_c_id)"
                . " select tgd_ui_id, tgd_c_id"
                . " from tgd_user_instance"
                . " join tgd_condition on tgd_ui_gt_id=tgd_c_gt_id"
                . " where tgd_ui_id=:ui_id";
            $this->PDO->insert($sql, array('ui_id' => $tgd_ui_id));

            // Create the instance multipliers
            $sql = "insert"
                . " into tgd_instance_multiplier (tgd_im_ui_id, tgd_im_m_id, tgd_im_period)"
                . " select tgd_ui_id, tgd_m_id, p.period"
                . " from tgd_user_instance"
                . " join tgd_multiplier on tgd_ui_gt_id=tgd_m_gt_id"
                . " join (select @period := @period + 1 as period"
                . " from (select 0 union all select 1 union all select 3 union all select 4 union all"
                . " select 5 union all select 6) t, (select @period:=0) r) p"
                . " where tgd_ui_id=:ui_id";
            $this->PDO->insert($sql, array('ui_id' => $tgd_ui_id));

            // Create the instance cells.
            $sql = "insert"
                . " into tgd_instance_cell (tgd_ic_ui_id, tgd_ic_c_id, tgd_ic_period)"
                . " select tgd_ui_id, tgd_c_id, p.period"
                . " from tgd_user_instance"
                . " join tgd_condition on tgd_ui_gt_id=tgd_c_gt_id"
                . " join (select @period := @period + 1 as period"
                . " from (select 0 union all select 1 union all select 3 union all select 4 union all"
                . " select 5 union all select 6) t, (select @period:=0) r) p"
                . " where tgd_ui_id=:ui_id";
            $this->PDO->insert($sql, array('ui_id' => $tgd_ui_id));

            // Commit the transation and return the TGD
            $this->PDO->commit_transaction();
        } catch (Exception $e) {
            $this->PDO->rollback_transaction();
            return $e->getMessage();
        }

        return $tgd_ui_id;
    }

    /**
     * Add a new industry.
     */
    public function add_industry($user_id, $name, $description)
    {
        // This is a multi-step process, so make sure they all work.
        try {
            $this->PDO->begin_transaction();

            // Create the industry record.
            $sql = "insert"
                . " into tgd_game_type (tgd_gt_name, tgd_gt_description)"
                . " values (:name, :description)";
            $params = array('name' => $name, 'description' => $description);
            $tgd_gt_id = $this->PDO->insert($sql, $params);
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }

        // Commit the transation and return the TGD
        $this->PDO->commit_transaction();
        return $tgd_gt_id;
    }

    /**
     * Copy an existing industry to a new industry.
     */
    public function copy_industry($user_id, $old_gt_id, $save_as_name, $save_as_id)
    {
        // This is a multi-step process, so make sure they all work.
        try {
            $this->PDO->begin_transaction();

            // If the ID is set and the name is not set, replace the existing industry.
            if (empty($save_as_name) && !empty($save_as_id)) {

                // This is only valid if the existing industry does not have any attached instances.
                $sql = "select count(*) from tgd_user_instance where tgd_ui_gt_id=:save_as_id";
                $count = $this->PDO->select_count($sql, array('save_as_id' => $save_as_id));

                if ($count !== "0") {
                    throw new Exception("Industry type can not be overwritten because there are existing user instances of the industry type.");
                }

                // Delete the existing industry. It will be re-created below.
                $sql = "select tgd_gt_name from tgd_game_type where tgd_gt_id=:save_as_id";
                $gt = $this->PDO->select_row($sql, array('save_as_id' => $save_as_id));
                $save_as_name = $gt->tgd_gt_name;
                $sql = "delete from tgd_game_type where tgd_gt_id=:save_as_id";
                $this->PDO->delete($sql, array('save_as_id' => $save_as_id));
            }

            // Create the new industry record as a copy.
            $sql = "insert into tgd_game_type (tgd_gt_name, tgd_gt_description)"
                . " select :name, tgd_gt_description"
                . " from tgd_game_type where tgd_gt_id=:gt_id";
            $new_gt_id = $this->PDO->insert($sql, array('name' => $save_as_name, 'gt_id' => $old_gt_id));

            // Create the new industry's conditions
            $sql = "insert into tgd_condition (tgd_c_gt_id, tgd_c_name, tgd_c_description, tgd_c_tooltip,"
                . " tgd_c_type, tgd_c_uom, tgd_c_prev_condition)"
                . " select :new_gt_id, tgd_c_name, tgd_c_description, tgd_c_tooltip, tgd_c_type, tgd_c_uom, tgd_c_prev_condition"
                . " from tgd_condition where tgd_c_gt_id=:gt_id";
            $this->PDO->insert($sql, array('new_gt_id' => $new_gt_id, 'gt_id' => $old_gt_id));

            // Create the new industry's multipliers
            $sql = "insert into tgd_multiplier (tgd_m_gt_id, tgd_m_order, tgd_m_name, tgd_m_description,"
                . " tgd_m_type, tgd_m_operation, tgd_m_propagate)"
                . " select :new_gt_id, tgd_m_order, tgd_m_name, tgd_m_description, tgd_m_type, tgd_m_operation, tgd_m_propagate"
                . " from tgd_multiplier where tgd_m_gt_id=:gt_id";
            $this->PDO->insert($sql, array('new_gt_id' => $new_gt_id, 'gt_id' => $old_gt_id));
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            throw $e;
        }

        // Commit the transation and return the TGD
        $this->PDO->commit_transaction();
        return $new_gt_id;
    }

    /**
     * Delete an industry.
     */
    public function delete_industry($user_id, $gt_id)
    {
        // This is only valid if the existing industry does not have any attached instances.
        $sql = "select count(*) from tgd_user_instance where tgd_ui_gt_id=:gt_id";
        $count = $this->PDO->select_count($sql, array('gt_id' => $gt_id));
        if ($count !== "0") {
            throw new Exception("Industry type can not be deleted because there are existing user instances of the industry type.");
        }

        // Delete the industry.
        $sql = "delete from tgd_game_type where tgd_gt_id=:gt_id";
        return $this->PDO->insert($sql, array('gt_id' => $gt_id));
    }

    /**
     * Add a new condition.
     */
    public function add_condition($tgdi_id, $values)
    {
        // Have to verify that there is not already an objective.
        try {
            $this->PDO->begin_transaction();

            // Check for the existance of an objective if a objective is being added.
            if ($values['type'] == 'obj') {
                $sql = "select tgd_c_id from tgd_condition"
                    . " where tgd_c_type='obj' and tgd_c_gt_id=:tgdi_id";
                $objective = $this->PDO->select_row($sql, array('tgdi_id' => $tgdi_id));
                if ($objective !== false) {
                    throw new Exception('There is already an Objective for the Industry');
                }
            }

            // Add the new condition.
            $values['tgdi_id'] = $tgdi_id;
            $sql = "insert into tgd_condition (tgd_c_gt_id, tgd_c_name, tgd_c_description, tgd_c_tooltip,"
                . " tgd_c_type, tgd_c_uom) values (:tgdi_id, :name, :description, :tooltip, :type, :uom)";
            $tgd_c_id = $this->PDO->insert($sql, $values);

            // Put the new condition at the bottom of the order.
            $sql = "update tgd_condition set tgd_c_prev_condition=:tgd_c_id where tgd_c_prev_condition is null"
                . " and tgd_c_gt_id=:tgdi_id and tgd_c_id!=:tgd_c_id";
            $this->PDO->update($sql, array('tgd_c_id' => $tgd_c_id, 'tgdi_id' => $tgdi_id));

        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            throw $e;
        }

        // Commit the transation and return the TGD
        $this->PDO->commit_transaction();
        return $tgd_c_id;
    }

    /**
     * Delete condition.
     */
    public function delete_condition($tgdi_c_id)
    {
        // Have to verify that there is not already an objective.
        try {
            $this->PDO->begin_transaction();

            // Update the previous conditions next condition to be the deleted conditions next condition.
            $sql = "update tgd_condition as a inner join tgd_condition as b on a.tgd_c_prev_condition = b.tgd_c_id "
                . "set a.tgd_c_prev_condition=b.tgd_c_prev_condition where a.tgd_c_prev_condition=:tgdi_c_id";
            $this->PDO->update($sql, array('tgdi_c_id' => $tgdi_c_id));

            // Do the deletion
            $sql = "delete from tgd_condition where tgd_c_id=:tgdi_c_id";
            $this->PDO->delete($sql, array('tgdi_c_id' => $tgdi_c_id));
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            throw $e;
        }

        // Commit the transation and return the TGD
        $this->PDO->commit_transaction();
    }

    /**
     * Move condition.
     */
    public function move_condition($tgdi_c_id, $direction)
    {
        // Have to verify that there is not already an objective.
        try {
            $this->PDO->begin_transaction();

            if ($direction == 'up') {
                // Find which condition points to this condition and which condition this points to.
                $sql = "select tgd_c_id from tgd_condition where tgd_c_prev_condition=:tgdi_c_id";
                $prev = $this->PDO->select_row($sql, array('tgdi_c_id' => $tgdi_c_id));

                // Have the condition that points to that condition now point to this condition.
                $sql = "update tgd_condition as a inner join tgd_condition as b on a.tgd_c_prev_condition = b.tgd_c_id"
                    . " set a.tgd_c_prev_condition=b.tgd_c_prev_condition where a.tgd_c_prev_condition=:prev_c_id";
                $this->PDO->update($sql, array('prev_c_id' => $prev->tgd_c_id));

                // Point the previous condition to the next condition.
                $sql = "update tgd_condition as a inner join tgd_condition as b on a.tgd_c_prev_condition = b.tgd_c_id"
                    . " set a.tgd_c_prev_condition=b.tgd_c_prev_condition where a.tgd_c_id=:prev_c_id";
                $this->PDO->update($sql, array('prev_c_id' => $prev->tgd_c_id));

                // Point this condition to the previous condition.
                $sql = "update tgd_condition set tgd_c_prev_condition=:prev_c_id where tgd_c_id=:tgdi_c_id";
                $this->PDO->update($sql, array('prev_c_id' => $prev->tgd_c_id, 'tgdi_c_id' => $tgdi_c_id));
            }

        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            throw $e;
        }

        // Commit the transation and return the TGD
        $this->PDO->commit_transaction();
    }

    /**
     * Add a new multiplier.
     */
    public function add_multiplier($tgdi_id, $values)
    {
        // Have to verify that there is not already an objective.
        try {
            $this->PDO->begin_transaction();

            // Find the last order number
            $sql = "select max(tgd_m_order) 'max_order' from tgd_multiplier where tgd_m_gt_id=:tgdi_id";
            $last = $this->PDO->select_row($sql, array('tgdi_id' => $tgdi_id));

            ($last === false || $last->max_order == null) ? $order = 1 : $order = intval($last->max_order) + 1;
            // Add the new multiplier.
            $values['tgdi_id'] = $tgdi_id;
            $values['order'] = $order;
            $sql = "insert into tgd_multiplier (tgd_m_gt_id, tgd_m_order, tgd_m_name, tgd_m_description, tgd_m_type,"
                . " tgd_m_operation, tgd_m_propagate)"
                . " values (:tgdi_id, :order, :name, :description, :type, :operation, :propagate)";

            $tgd_m_id = $this->PDO->insert($sql, $values);
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            throw $e;
        }

        // Commit the transation and return the TGD
        $this->PDO->commit_transaction();
        return $tgd_m_id;
    }

    /**
     * Delete multiplier.
     */
    public function delete_multiplier($tgdi_m_id)
    {
        // Have to verify that there is not already an objective.
        try {
            $this->PDO->begin_transaction();

            // Do the deletion
            $sql = "delete from tgd_multiplier where tgd_m_id=:tgdi_m_id";
            $this->PDO->delete($sql, array('tgdi_m_id' => $tgdi_m_id));
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            throw $e;
        }

        // Commit the transation and return the TGD
        $this->PDO->commit_transaction();
    }

    /**
     * Move multiplier.
     */
    public function move_multiplier($tgdi_m_id, $direction)
    {
        // Have to verify that there is not already an objective.
        try {
            $this->PDO->begin_transaction();

            if ($direction == 'up') {
                // Get our order number.
                $sql = "select tgd_m_gt_id, tgd_m_order from tgd_multiplier where tgd_m_id=:tgdi_m_id";
                $m = $this->PDO->select_row($sql, array('tgdi_m_id' => $tgdi_m_id));

                // Find the previous order number
                $sql = "select max(tgd_m_order) 'max_prev' from tgd_multiplier"
                    . " where tgd_m_gt_id=:tgd_m_gt_id"
                    . " and tgd_m_order < :tgd_m_order";
                $p = $this->PDO->select_row($sql, array('tgd_m_gt_id' => $m->tgd_m_gt_id, 'tgd_m_order' => $m->tgd_m_order));

                // Set the previous order number to be our order number.
                $sql = "update tgd_multiplier set tgd_m_order=:our_order"
                    . " where tgd_m_order=:prev_order and tgd_m_gt_id=:tgd_m_gt_id";
                $this->PDO->update($sql, array('our_order' => $m->tgd_m_order, 'prev_order' => $p->max_prev, 'tgd_m_gt_id' => $m->tgd_m_gt_id));

                // Set our number to be the previous number.
                $sql = "update tgd_multiplier set tgd_m_order=:prev_order where tgd_m_id=:tgdi_m_id";
                $this->PDO->update($sql, array('prev_order' => $p->max_prev, 'tgdi_m_id' => $tgdi_m_id));

            }
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            throw $e;
        }

        // Commit the transation and return the TGD
        $this->PDO->commit_transaction();
    }

    /**
     * Adds a source of other income.
     *
     * @param $user_id Int User ID.
     * @return bool
     */
    public function get_source($user_id)
    {

        $sql = 'select tois_id, tois_value'
            . ' from tgd_other_income_source'
            . ' where tois_user_id=:user_id or tois_user_id=0';

        return $this->PDO->select($sql, array('user_id' => $user_id));
    }

    public function add_income_source($val, $user_id)
    {
        try {
            $sql = 'insert into tgd_other_income_source'
                . ' (tois_user_id, tois_value)'
                . ' values(:user_id, :value)';

            $ret = $this->PDO->insert($sql, array('user_id' => $user_id, 'value' => $val));
        } catch (\Exception $e) {
            echo $e->getMessage();
            $ret = $e;
        }
        return $ret;
    }

    /**
     * Adds other income for a user.
     *
     * @param $type_id      Int     Type of Other income.
     * @param $name         String  Name of Other income.
     * @param $amount       Int     Amount.
     * @param $qtr          Int     Quarter.
     * @param $tgd_ui_id    Int     TGD user instance ID.
     * @return bool
     */
    public function add_other_income($type_id, $name, $amount, $qtr, $tgd_ui_id)
    {
        $sql = 'insert into tgd_other_income'
            . ' ( toi_source_id, toi_tgd_ui_id, toi_qtr, toi_amount, toi_name)'
            . ' values(:type_id, :ui_id, :qtr, :amount, :name)';

        return $this->PDO->insert($sql, array('type_id' => $type_id, 'ui_id' => $tgd_ui_id, 'qtr' => $qtr, 'amount' => $amount, 'name' => $name));
    }

    /**
     * Fetches other income for a user.
     *
     * @param $ui_id Int User Instance ID.
     * @param $qtr   Int Quarter.
     * @return bool
     */
    public function get_other_income($ui_id, $qtr, $user_id, $product_id)
    {
        $sql = 'select toi_id, toi_source_id, toi_amount, toi_name, tois_value'
            . ' from tgd_other_income join tgd_other_income_source'
            . ' on tois_id=toi_source_id'
            . ' where toi_tgd_ui_id=:ui_id'
            . ' and toi_qtr=:qtr';

        $ret['oi'] = $this->PDO->select($sql, array('ui_id' => $ui_id, 'qtr' => $qtr));

        $sql = 'select uact_cpd_id'
            . ' from user_active_cpd_tgd'
            . ' where uact_user_id=:user_id and uact_prod_id=:prod_id';

        $cpd_id = $this->PDO->select($sql, array('user_id' => $user_id, 'prod_id' => $product_id));
//        empty($cpd_id) ? $cpd_id = $cpd_id[0]->uact_cpd_id : $cpd_id = null;


        if (!empty($cpd_id)) {
            $cpd_id = $cpd_id[0]->uact_cpd_id;

            $sql = "select sum(hca_net) as ref"
                . " from homebase_client_asset"
                . " join deal on hca_id = deal_asset_id"
                . " join (select has_status, hs1.has_datetime, has_id, has_asset_id, has_md"
                . " from homebase_asset_status hs1"
                . " join (select max(has_datetime) as time"
                . " from homebase_asset_status"
                . " where quarter(has_datetime)=:quarter"
                . " group by has_asset_id) as hs2"
                . " on hs1.has_datetime = hs2.time) as assetstatus on hca_id=assetstatus.has_asset_id"
                . " WHERE deal_cpd_id =:cpd_id  and assetstatus.has_status='ref_pa'";

            $ret['ref'] = $this->PDO->select($sql, array('cpd_id' => $cpd_id, 'quarter' => $qtr - 1));

            $sql = "select sum(hca_net) as buyer"
                . " from homebase_client_asset"
                . " join deal on hca_id = deal_asset_id"
                . " join (select has_status, hs1.has_datetime, has_id, has_asset_id, has_md"
                . " from homebase_asset_status hs1"
                . " join (select max(has_datetime) as time"
                . " from homebase_asset_status"
                . " where quarter(has_datetime)=:quarter"
                . " group by has_asset_id) as hs2"
                . " on hs1.has_datetime = hs2.time) as assetstatus on hca_id=assetstatus.has_asset_id"
                . " WHERE deal_cpd_id =:cpd_id  and assetstatus.has_status='accepted_co'";

            $ret['buyer'] = $this->PDO->select($sql, array('cpd_id' => $cpd_id, 'quarter' => $qtr - 1));
        }

        return $ret;
    }

    /**
     * Deletes an other income instance.
     *
     * @param $oi_id Int Other Income ID
     * @return bool
     */
    public function delete_other_income($oi_id)
    {
        $sql = 'delete from tgd_other_income '
            . ' where toi_id=:oi_id';

        return $this->PDO->delete($sql, array('oi_id' => $oi_id));
    }

    /**
     * Updates Floor Target Value for TGD metrices.
     *
     * @param $col   String  Name of Column.
     * @param $id    Int     TGD UI ID
     * @param $year  Year    Year for FTG.
     * @param $value Int     FTG amount.
     * @return bool
     */
    public function update_ftg($col, $id, $year, $value)
    {
        $sql = 'update tgd_user_instance'
            . ' set tgd_' . $col . '=:val'
            . ' where tgd_ui_id=:id and tgd_ui_year=:year';

        return $this->PDO->update($sql, array('val' => $value, 'id' => $id, 'year' => $year));

    }

    /**
     * Fetches Linked TGD information.
     *
     * @param $user_id          Int User Id.
     * @param $active_product   Int Active Product Id.
     * @return array
     */
    function get_linked_tgd($user_id, $active_product)
    {
        $ret = [];

        // Find the list of products available to the user.
        $sql = 'select distinct u_product_name, u_product_id, uact_tgd_id,'
            . ' tgd_ui_name, upg_name, upg_id'
            . ' from user_active_cpd_tgd'
            . ' left join user_product'
//            . ' on uact_prod_id=u_product_id'
            . ' left join user_product_group'
            . ' on upg_id=uact_prod_id'
            . ' left join tgd_user_instance on uact_tgd_id=tgd_ui_id'
            . ' where uact_user_id=:user_id';

        $ret['product'] = $this->PDO->select($sql, array('user_id' => $user_id));

        $sql = 'select uap_product_id'
            . ' from user_active_product'
            . ' where uap_user_id=:user_id';

        $ret['active_id'] = $this->PDO->select($sql, array('user_id' => $user_id));

        return $ret;
    }

    /**
     * Updates the linked TGD record in DB
     *
     * @param $tgd_id       Int TGD Id.
     * @param $product_id   Int Product Id.
     * @return bool
     */
    function update_linked($tgd_id, $product_id)
    {

        try {
            $this->PDO->begin_transaction();

            $colNames = array('tl_tgd_id', 'tl_linked_product_id');

            // Delete any previous instance of linked product.
            $sql = " delete from"
                . " tgd_linked"
                . " where tl_tgd_id=:tgd_id";
            $this->PDO->select($sql, array('tgd_id' => $tgd_id));

            $values = '';
            $data = [];
            $data['tgd_id'] = $tgd_id;

            if (is_array($product_id)) {
                // Create data array and value query part.
                foreach ($product_id as $key => $value) {
                    $data['product_id' . $key] = $value;
                    $values .= '(:tgd_id,:product_id' . $key . ')';
                    array_key_exists(++$key, $product_id) ? $values .= ',' : '';
                }

                // Execute SQL.
                $sql = "INSERT INTO tgd_linked (" . implode(', ', $colNames) .
                    ") VALUES " . $values;

                $this->PDO->insert($sql, $data);
            }

            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }
        return true;
    }

    /**
     *  Fetches Toggle History for a TGD instance.
     *
     * @param $tgd_id Int TGD ID.
     * @return bool
     */
    function get_toggle_history($tgd_id)
    {
        $sql = 'select tph_prev_period, tph_current_period, tph_prev_cond_id, tph_current_cond_id, tph_time'
            . ' from tgd_toggle_history'
            . ' where tph_tgd_ui_id=:id and tph_active=1'
            . ' order by tph_time asc';

        return $this->PDO->select($sql, array('id' => $tgd_id));
    }


    /**
     * Fetches the Linked CPD actual Details.
     *
     * @param $cpd_id  Int    CPD ID.
     * @param $type    String Type of CPD.
     * @param $quarter Int    Quarter to fetch CPD Actual.
     * @return bool
     */
    function get_cpd_details($cpd_id, $type, $quarter)
    {
        $sql = 'select deal_name, deal_clu, deal_rwt, deal_net, deal_property, ds_status'
            . ' from deal'
            . " join (select ds_status, ds1.ds_datetime, ds_deal_id"
            . " from deal_status ds1"
            . " join (select max(ds_datetime) as time"
            . " from deal_status"
            . " where quarter(ds_datetime)<=:quarter"
            . " group by ds_deal_id) AS ds2"
            . " on ds1.ds_datetime = ds2.time) as dealstatus on deal_id=dealstatus.ds_deal_id"
            . ' where deal_cpd_id=:id and ds_status=:type';

        return $this->PDO->select($sql, array('id' => $cpd_id, 'type' => 'complete', 'quarter' => $quarter));
    }

    /**
     * Fetch all the TGD instances that are related to this TGD instance
     *
     * @param $tgd_id Int TGD ID.
     * @return bool
     */
    function get_related_tgd($tgd_id, $user_id)
    {
        $sql = 'select distinct u_product_name, u_product_id, uact_tgd_id, tgd_ui_name'
            . ' from user_product left join user_active_cpd_tgd'
            . ' on uact_user_id=:user_id'
            . ' left join tgd_user_instance on uact_tgd_id=tgd_ui_id'
            . ' join cpd on u_product_id=cpd_product_id'
            . ' left join tgd_linked on u_product_id=tl_linked_product_id'
            . ' where (u_product_user_id=:user_id or u_product_user_id=0) and tl_tgd_id=:tgd_id';


        return $this->PDO->select($sql, array('tgd_id' => $tgd_id, 'user_id' => $user_id));
    }

    /**
     * Updates TGD label.
     *
     * @param $pk       Int     Primary key
     * @param $value    String  Value for column.
     * @param $name     String  Name of column to be updated.
     * @return bool
     */
    function update_label_name($pk, $value, $name)
    {
        $sql = 'update tgd_user_instance'
            . ' set ' . $name . '=:value'
            . ' where tgd_ui_id=:id';

        return $this->PDO->update($sql, array('value' => $value, 'id' => $pk));
    }

    function propagate_other_income($id, $qtr)
    {
        for ($i = $qtr + 1; $i <= 5; $i++) {
            $sql = 'insert into tgd_other_income'
                . ' ( toi_source_id, toi_tgd_ui_id, toi_qtr, toi_amount, toi_name)'
                . ' select toi_source_id,  toi_tgd_ui_id, ' . $i . ', toi_amount, toi_name'
                . ' from tgd_other_income'
                . ' where toi_id=:id';

            $ret[$i] = $this->PDO->insert($sql, array('id' => $id));
        }
        return $ret;
    }

    function get_metrics_data($user_id)
    {
        $ret = [];
        $sql = 'select tm_dial_range, tm_broken_number, tm_contacts_achieved,'
            . ' tm_left_messages, tm_full_conversation, tm_meeting_scheduled,'
            . ' tm_meeting_completed, tm_proposal_requested, tm_proposal_presented,'
            . ' tm_deal_listed, tm_deal_launched'
            . ' from tgd_metrics'
            . ' where tm_user_id=:user_id';

        $ret['benchmark'] = $this->PDO->select($sql, array('user_id' => $user_id));

        $where = '(';

        $sql = 'select uat_tsl_id'
            . ' from user_active_tsl'
            . ' where uat_user_id=:user_id';

        $tsl_id = $this->PDO->select($sql, array('user_id' => $user_id));

        foreach ($tsl_id as $id) {
            $where .= " tc_tsl_id= $id->uat_tsl_id or";
        }
        $where = substr($where, 0, strrpos($where, ' '));

        $where .= ')';

        $sql = 'SELECT sum(seq) as val'
            . ' from ('
            . ' select count(tac_id) as seq'
            . ' from tsl_about_client'
            . ' join tsl_client'
            . ' on tac_tc_id=tc_id'
            . ' where tac_sequence=2'
            . ' and '
            . $where . ') a';

        $ret['tsl']['dials'] = $this->PDO->select($sql, array());

        $sql = 'select count(tac_id) as val'
            . ' from tsl_about_client'
            . ' join tsl_client'
            . ' on tac_tc_id=tc_id'
            . ' where tac_valid_phone="cnt"'
            . ' and ' . $where;

        $ret['tsl']['contacts'] = $this->PDO->select($sql, array());

        $sql = 'select count(tac_id) as val'
            . ' from tsl_about_client'
            . ' join tsl_client'
            . ' on tac_tc_id=tc_id'
            . ' where tac_valid_phone="cnv"'
            . ' and ' . $where;

        $ret['tsl']['conversation'] = $this->PDO->select($sql, array());

        $sql = 'select count(hc_id) as val'
            . ' from tsl_client'
            . ' join homebase_client_asset'
            . ' on tc_asset_id=hca_id'
            . ' join homebase_client'
            . ' on hc_id=hca_client_id'
            . ' where hc_main_phone=\'Number Needed\' or'
            . ' hc_mobile_phone=\'Number Needed\' or hc_second_phone=\'Number Needed\' and ' . $where;

        $ret['tsl']['broken'] = $this->PDO->select($sql, array());

        $sql = 'select count(tac_id) as val'
            . ' from tsl_about_client'
            . ' join tsl_client'
            . ' on tac_tc_id=tc_id'
            . ' where tac_result="scheduled"'
            . ' and ' . $where;

        $ret['tsl']['meetings'] = $this->PDO->select($sql, array());

        $sql = 'select count(tsm_id) as val'
            . ' from tsl_client'
            . ' join tsl_spoken_message'
            . ' on tsm_client_id=tc_asset_id'
            . ' join tsl_about_client'
            . ' on tac_tc_id=tc_id'
            . ' where ' . $where;

        $ret['tsl']['message'] = $this->PDO->select($sql, array());

        $sql = 'select uact_cpd_id'
            . ' from user_active_cpd_tgd'
            . ' where uact_user_id=:user_id';

        $cpd_id = $this->PDO->select($sql, array('user_id' => $user_id))[0]->uact_cpd_id;

        $sql = 'select has_status as ds_status, sum(hca_net) as net, sum(hca_price) as price, count(deal_net) as count,'
            . ' sum(hca_price * hca_rate / 100) as gross'
            . ' from deal'
            . ' join homebase_client_asset'
            . ' on hca_id=deal_asset_id'
            . ' join (SELECT has_status, has1.has_datetime, has_id, has_asset_id, has_md'
            . ' FROM homebase_asset_status has1'
            . ' JOIN (SELECT MAX(has_datetime) AS time, has_asset_id as asset_id'
            . ' FROM homebase_asset_status'
            . ' GROUP BY has_asset_id) has2'
            . ' ON has1.has_datetime = has2.time and has1.has_asset_id = asset_id) as has_status'
            . ' on deal_asset_id=has_status.has_asset_id'
            . ' join code on code_abbrev=has_status.has_status'
            . ' join code_type on code_type_id=ct_id'
            . ' join cpd on cpd_id=deal_cpd_id'
            . ' where deal_cpd_id=:cpd_id and cpd_user_id=:user_id'
            . ' group by has_status';

        $ret['cpd'] = $this->PDO->select($sql, array('cpd_id' => $cpd_id, 'user_id' => $user_id));

        return $ret;
    }

    /**
     * Save Metrics Benchmark.
     *
     * @param $user_id  Int     User Id.
     * @param $col      String  Column Name.
     * @param $val      Int     Value
     * @return bool
     */
    function save_metrics_benchmark($user_id, $col, $val)
    {
        $sql = 'insert into tgd_metrics'
            . ' (tm_' . $col . ', tm_user_id)'
            . ' values(:val, :user_id)'
            . ' on duplicate key'
            . ' update tm_' . $col . '=:val';

        return $this->PDO->insert($sql, array('val' => $val, 'user_id' => $user_id));
    }
}