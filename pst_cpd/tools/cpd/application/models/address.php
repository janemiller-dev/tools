<?php

namespace Model;

class Address
{

	/**
	 * Insert a new address number
	 */
	public function add_address($object_name, $object_id, $address_raw, $description, $input)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Create the SQL
		$sql = "insert into address (address_object_name, address_object_id, address_description, address_place_id, address_raw, address_street_1, address_street_2, address_city, address_state_code, address_postal_code, address_country_code, address_lat, address_long)"
			. " values(:object_name, :object_id, :description, :place_id, :address_raw, :street_1, :street_2, :city, :state_code, :postal_code, :country_code, :lat, :long)";
		$params = array(
			'object_name' => $object_name,
			'object_id' => $object_id,
			'description' => $description,
			'place_id' => $input->getText('place_id'),
			'address_raw' => $input->getText('address'),
			'street_1' => $input->getText('street_1'),
			'street_2' => $input->getText('street_2'),
			'city' => $input->getText('city'),
			'state_code' => $input->getText('state'),
			'postal_code' => $input->getText('zipcode'),
			'country_code' => $input->getText('country_code'),
			'lat' => $input->getText('lat'),
			'long' => $input->getText('lng'),
		);
		return $PDO->insert($sql, $params);
	}

	/**
	 * Get the list of addresses for the specified company.
	 */
	public function get_object_addresses($object_name, $object_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Create the SQL
		$sql = "select address_object_name, address_object_id, address_description, address_place_id, address_raw, address_street_1, address_street_2, address_city, address_state_code, address_postal_code, address_country_code, address_lat, address_long from address"
			. " where address_object_name=:object_name and address_object_id=:object_id";
		$params = array(
			'object_name' => $object_name,
			'object_id' => $object_id
		);
		return $PDO->select($sql, $params);
	}

}