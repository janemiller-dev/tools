<?php

namespace Model;

use function count;
use DateTime;
use function implode;
use function intval;
use function isEmpty;
use Joomla\Language\Text;
use function load_maintenance_page;
use function print_r;
use Prophecy\Exception\Exception;

/**
 * TSL model class
 */
class TSL
{

    private $PDO;
    private $common_model;

    /**
     * Initialises the PDO and model instance.
     */
    public function __construct()
    {
        $this->PDO = \Model\PDO_model::instance('Tools');
        $this->common_model = new Common;
    }

    /**
     * Perform a server side DataTables search for the tsl
     *
     * @param $user_id int id of the user
     * @param $input mixed input from the ajax
     *
     * @return object
     */
    public function search_tsl_instances($user_id, $product_id, $input)
    {
        // Get the parameters.
        $start = $input->getInt('start');
        $length = $input->getInt('length');

        // Is there a search?
        $search = $_POST['search'];

        if (!empty($search['value'])) {
            $search_val = $search['value'];
        }

        // Get the column information
        $columns = $_POST['columns'];

        // Get the columns and the search
        $sql_columns = '';
        $sep = ' ';
        $sql_where = ' where tsl_user_id=' . $user_id . ' and tsl_product_id=' . $product_id;
        $sep_where = ' and ';

        foreach ($columns as $column) {

            if (empty($column['data'])) {
                continue;
            }
            $search_col = $column['data'];
            $sql_columns .= $sep . $column['data'];
            $sep = ', ';

            // Add to the search?
            if (isset($search_val) && ($column['searchable'] == 'true')) {
                $sql_where .= $sep_where . "(cast(" . $search_col . " as char(100)) like '%" . $search_val . "%')";
                $sep_where = ' or ';
            }
        }

        // Determine the order information.
        $order = $_POST['order'];

        $sql_order = '';

        foreach ($order as $o) {

            if (strlen($sql_order) > 0) {
                $sql_order .= ', ';
            } else {
                $sql_order = " order by ";
            }
            // Get the column
            $columns[$o['column']]['data'];
            $sql_order .= $columns[$o['column']]['data'] . ' ' . $o['dir'];
        }

        // Get the total number of rows (unfiltered).
        $sql = "select count(*) from tsl"
            . " where tsl_user_id=:user_id";

        $count_total = $this->PDO->select_count($sql, array('user_id' => $user_id));

        // Get the total number of rows (filtered).
        $sql = "select count(*)"
            . " from tsl"
            . $sql_where;
        $count_filtered = $this->PDO->select_count($sql, array('user_id' => $user_id));

        // Fetch TSL instances.
        $sql = " select " . $sql_columns . ", cpd_id, cpd_name, uat_id from tsl"
            . " left join cpd on tsl_cpd_id=cpd_id"
            . " left join industry_profession on tsl_profession_id=profession_id"
            . " left join industry on profession_industry_id=industry_id"
            . " left join user_active_tsl"
            . " on uat_tsl_id=tsl_id"
            . $sql_where
            . $sql_order
            . " limit " . $start . ',' . $length;

        $data = $this->PDO->select($sql, array('user_id' => $user_id));

        // Fetch Target audience.
        $sql = 'select ul_id, ul_name, tta_tsl_id'
            . ' from user_location'
            . ' join tsl_target_audience'
            . ' on ul_id=tta_location_id'
            . ' join tsl on tsl_id=tta_tsl_id'
            . ' where tsl_user_id=:user_id and tsl_product_id=:product_id';

        $audience = $this->PDO->select($sql, array('user_id' => $user_id, 'product_id' => $product_id));

        foreach ($audience as $key => $value) {
            $audience_array[$value->tta_tsl_id][] = $value;
        }

        // Fetch Suspect List Criteria
        $sql = 'select uc_id, uc_name, tsl_criteria_tsl_id'
            . ' from user_criteria'
            . ' join tsl_criteria'
            . ' on tsl_criteria_criteria_id=uc_id'
            . ' join tsl on tsl_criteria_tsl_id = tsl_id'
            . ' where tsl_user_id=:user_id and tsl_product_id=:product_id';

        $criteria = $this->PDO->select($sql, array('user_id' => $user_id, 'product_id' => $product_id));

        $criteria_array = [];

        foreach ($criteria as $key => $value) {
            $criteria_array[$value->tsl_criteria_tsl_id][] = $value;
        }

        // Fetch Suspect list Prepared By.
        $sql = ' select tsl_agent_name, tsl_agent_id, tsl_agent_tsl_id, tsl_agent_selected'
            . ' from tsl_agent'
            . ' join tsl on tsl_id=tsl_agent_tsl_id'
            . ' where tsl_user_id=:user_id and tsl_product_id=:product_id';

        $prepared_by = $this->PDO->select($sql, array('user_id' => $user_id, 'product_id' => $product_id));

        $prepared_by_array = [];

        foreach ($prepared_by as $key => $value) {
            $prepared_by_array[$value->tsl_agent_tsl_id][] = $value;
        }
        // Add criteria and audience array to data array.
        foreach ($data as $key => $value) {
            isset($criteria_array[$value->tsl_id]) ? $data[$key]->criteria = $criteria_array[$value->tsl_id] : '';

            isset($audience_array[$value->tsl_id]) ? $data[$key]->audience = $audience_array[$value->tsl_id] : '';

            isset($prepared_by_array[$value->tsl_id]) ? $data[$key]->prepared_by = $prepared_by_array[$value->tsl_id] : '';
        }

        // Set the return values.
        $ret['data'] = $data;
        $ret['recordsTotal'] = $count_total;
        $ret['recordsFiltered'] = $count_filtered;

        return $ret;
    }

    /**
     * Adds a new TSL instance to the database.
     *
     * @param $data Array Data about TSL.
     * @return \Exception
     */
    public function add_tsl($data)
    {
        try {
            $this->PDO->begin_transaction();

            $columns = 'tsl_user_id, tsl_name, tsl_description,'
                . ' tsl_created, tsl_modified, tsl_cpd_id,'
                . ' tsl_product_id';

            $sql = 'insert into tsl'
                . '( ' . $columns . ' )'
                . ' values(:user_id, :name, :desc, now(),'
                . ' now(), :cpd_id, :product_id)';

            $ret['id'] = $this->PDO->insert($sql, array('user_id' => $data['user_id'], 'name' => $data['name'],
                'desc' => $data['description'], 'cpd_id' => $data['cpd_id'], 'product_id' => $data['product_id']));

            // Prepare fetch query.
            $query = '';

            // Check if any target audience is present.
            if (isset($data['audience'])) {
                $query = ' and (hca_location=';
                $query .= implode(' or hca_location= ', $data['audience']);
                $query .= ')';
                foreach ($data['audience'] as $audience) {
                    $sql = 'insert'
                        . ' into tsl_target_audience'
                        . ' (tta_tsl_id, tta_location_id)'
                        . ' values(:tsl_id, :location_id)'
                        . ' ON DUPLICATE KEY UPDATE tta_tsl_id=:tsl_id';

                    $ret['location_inserted'] = $this->PDO->insert($sql, array('tsl_id' => $ret['id'],
                        'location_id' => $audience));
                }
            }

            // Check if criteria is selected.
            if (isset($data['criteria'])) {
                $query .= ' and (hca_criteria_used=';

                $query .= implode(" or hca_criteria_used= ", $data['criteria']);
                $query .= ')';

                foreach ($data['criteria'] as $criterion) {

                    $sql = 'insert'
                        . ' into tsl_criteria'
                        . ' (tsl_criteria_tsl_id, tsl_criteria_criteria_id)'
                        . ' values(:tsl_id, :criteria_id)'
                        . ' ON DUPLICATE KEY UPDATE tsl_criteria_tsl_id=:tsl_id';

                    $ret['criteria_inserted'] = $this->PDO->insert($sql, array('tsl_id' => $ret['id'],
                        'criteria_id' => $criterion));
                }
            }

            $query .= ' and hca_product_id=' . $data['product_id'];

            $ret = $this->fetch_clients($ret['id'], $data['user_id'], $query);

            $this->PDO->commit_transaction();
        } catch (\Exception $e) {

            $this->PDO->rollback_transaction();
            return $e;
        }
        return $ret;
    }

    /**
     * Fetch clients from the homebase to TSL.
     *
     * @param $tsl_id
     * @param $user_id
     * @param $query
     * @return \Exception
     */
    public function fetch_clients($tsl_id, $user_id, $query)
    {
        try {
            $sql = 'insert into'
                . ' tsl_client'
                . ' (tc_asset_id, tc_tsl_id, tc_temp)'
                . ' select hca_id, ' . $tsl_id . ', ' . $user_id
                . ' from homebase_client_asset'
                . ' join homebase_client'
                . ' on hca_client_id=hc_id'
                . ' where hc_user_id=:user_id ' . $query
                . ' and hca_id not in'
                . ' (select hca_id '
                . ' from homebase_client_asset'
                . ' join deal'
                . ' on deal_asset_id=hca_id'
                . ' join cpd'
                . ' on cpd_id=deal_cpd_id'
                . ' where cpd_user_id=:user_id)'
                . ' on duplicate key update tc_asset_id=tc_asset_id';

            $ret['client_added'] = $this->PDO->insert($sql,
                array('user_id' => $user_id));

            // Create a record for client, where we can store additional info.
            $sql = ' insert into'
                . ' tsl_about_client'
                . ' (tac_tc_id)'
                . ' select tc_id'
                . ' from tsl_client'
                . ' where tc_temp=:random';

            $ret['asset_added'] = $this->PDO->insert($sql, array('random' => $user_id));

            $sql = 'insert into'
                . ' homebase_asset_status'
                . ' (has_asset_id, has_status, has_md, has_datetime)'
                . ' select tc_asset_id, 0 , month(now()), now()'
                . ' from tsl_client'
                . ' where tc_temp=:random';

            $ret['status_updated'] = $this->PDO->insert($sql, array('random' => $user_id));

            $sql = ' update tsl_client'
                . ' set tc_temp=:val'
                . ' where tc_temp=:random';

            $ret['temp_updated'] = $this->PDO->update($sql, array('val' => '', 'random' => $user_id));
        } catch (\Exception $e) {
            return $e;
        }
        return $ret;
    }

    /**
     * Checks whether TSL clients for a user is available or not
     *
     * @param $tsl_id   Int TSL ID.
     * @param $user_id  Int User ID.
     * @return bool
     */
    public function check_avail_tsl($tsl_id, $user_id)
    {
        $sql = 'select tsl_id from tsl'
            . ' where tsl_user_id =:user_id'
            . ' and tsl_id =:id';

        return $this->PDO->select($sql, array('user_id' => $user_id, 'id' => $tsl_id));
    }

    /**
     * Fetches the backgroung templates available
     *
     * @param $user_id  Int User ID.
     * @return bool
     */
    public function get_background_template($user_id)
    {
        $sql = ' select tpb_id, tpb_org_name, tpb_rand_name'
            . ' from tsl_promo_background'
            . ' where tpb_user_id =:user_id';

        return $this->PDO->select($sql, array('user_id' => $user_id));
    }

    /**
     * Adds the DB record for promotional document.
     *
     * @param $name    String original name of the file uploaded.
     * @param $random  String name with which the file will be stored.
     * @return bool
     */
    public function add_promo($random, $id, $is_pdf)
    {
        ($is_pdf === 1) ?
            $sql = 'update tsl_promo'
                . ' set tp_rand_name =:random'
                . ' where tp_id =:id' :
            $sql = 'update tsl_promo'
                . ' set tp_pdf_view =:random'
                . ' where tp_id =:id';

        return $this->PDO->update($sql, array('random' => $random, 'id' => $id));
    }


    /**
     * Adds the DB record for promotional document.
     *
     * @param $name    String original name of the file uploaded.
     * @param $random  String name with which the file will be stored.
     * @return bool
     */
    public function add_background_template($name, $random, $user_id, $extension)
    {
        $sql = 'insert into tsl_promo_background(tpb_rand_name, tpb_org_name, tpb_user_id, tpb_extension)'
            . ' values(:random, :name, :user_id, :extension)';

        return $this->PDO->insert($sql, array('name' => $name, 'random' => $random, 'user_id' => $user_id, 'extension' => $extension));
    }


    /**
     * Removes the record for background template from Database.
     *
     * @param $id   Int Id of the background template.
     * @return bool
     */
    public function delete_background_template($id)
    {
        $sql = ' delete from tsl_promo_background'
            . ' where tpb_id =:id';

        return $this->PDO->delete($sql, array('id' => $id));
    }

    /**
     * Returns the list of promo documents
     *
     * @return bool
     */
    public function get_promo()
    {
        $sql = "select ta_id, ta_name, ta_actual_name"
            . " from tsl_attachment where ta_type=:type";
        return $this->PDO->select($sql, ['type' => 'promo']);
    }

    /**
     *  Returns the information related to a promotional document.
     *
     * @param $id   Int  Id of the promotional document.
     * @return bool
     */
    public function get_file_info($id)
    {
        $sql = 'select tp_rand_name, tp_pdf_view'
            . ' from tsl_promo '
            . ' where tp_id =:id';
        return $this->PDO->select($sql, array('id' => $id));
    }

    /**
     * Updates the DB to show thumbnail is generated.
     *
     * @param $id   Id of the promotional document.
     * @return bool
     */
    public function add_pro_thumb($id)
    {
        $sql = ' update tsl_promo'
            . ' set pro_thumb_exist = 1'
            . ' where pro_id =:id';
        return $this->PDO->update($sql, array('id' => $id));
    }

    /**
     *  Returns the information related to a package document.
     *
     * @param $id   Int  Id of the package document.
     * @return bool
     */
    public function get_image_info($id)
    {
        $sql = 'select tpb_rand_name, tpb_extension'
            . ' from tsl_promo_background '
            . ' where tpb_id =:id';
        return $this->PDO->select($sql, array('id' => $id));
    }

    /**
     * Deletes the record for package document from the DB.
     *
     * @param $id   Int Id of the package document.
     * @return bool
     */
    public function delete_promo($id)
    {
        $sql = 'delete from tsl_promo'
            . ' where tp_id =:id';
        return $this->PDO->delete($sql, array('id' => $id));
    }

    /**
     * Deletes a promotional document outline.
     *
     * @param $outline_id Int Outline ID.
     * @return bool
     */
    public function delete_promo_outline($outline_id)
    {
        $sql = 'delete from'
            . ' tsl_promo_outline'
            . ' where tpo_id =:outline_id';

        return $this->PDO->delete($sql, array('outline_id' => $outline_id));
    }

    /**
     * Adds a Buyer's Survey to the database .
     *
     * @param $outline_id Id of the outline .
     * @return bool
     */
    public function add_survey($outline_id, $user_id)
    {
        $sql = 'insert '
            . 'into tsl_survey '
            . '(tsl_survey_name, tsl_survey_outline_id, tsl_survey_user_id) '
            . 'values(:name, :outline_id, :user_id)';

        return $this->PDO->insert($sql, array('name' => 'Survey', 'outline_id' => $outline_id, 'user_id' => $user_id));
    }

    /**
     * Fetches the defaults for survey.
     *
     * @param $survey_type String Type of Survey.
     * @return bool
     */
    function get_survey_default($survey_type)
    {
        $sql = ' select tsd_content'
            . ' from tsl_survey_default'
            . ' where tsd_survey_type=:survey_type';

        return $this->PDO->select($sql, array('survey_type' => $survey_type));
    }


    /**
     * Gets the list of Survey.
     *
     * @param $outline_id  Int Id of the outline.
     * @return bool
     */
    public function get_survey_list($outline_id, $user_id)
    {
        $sql = 'select tsl_survey_id, tsl_survey_name'
            . ' from tsl_survey'
            . ' where tsl_survey_user_id=:user_id';
        return $this->PDO->select($sql, array('user_id' => $user_id));

    }

    /**
     * Fetches the content for Buyer's Survey.
     *
     * @param $id    Int Id of the survey.
     * @return bool
     */
    public function get_bus_content($id, $version)
    {
        $sql = 'select tsl_survey_content'
            . ' from tsl_survey'
            . ' where tsl_survey_name_id=:id and tsl_survey_version=:version';

        return $this->PDO->select($sql, array('id' => $id, 'version' => $version));
    }

    /**
     * Fetches Buyer's Survey Outline
     *
     * @param $user_id      Int User ID.
     * @param $survey_type  String TYpe of survey
     * @param $survey_class String Class to which a survey belong.
     * @param $client_type  String Type of client.
     * @return bool
     */
    public function get_bus_outline($user_id, $survey_type, $survey_class, $client_type)
    {
        $sql = 'select tso_id, tso_version, tso_name'
            . ' from tsl_survey_outline'
            . ' where (tso_user_id=:user_id and tso_survey_type=:survey_type'
            . ' and tso_client_type=:client_type)'
            . ' or (tso_id=0)';

        return $this->PDO->select($sql, array('user_id' => $user_id, 'survey_type' => $survey_class,
            'client_type' => $client_type));
    }

    /**
     * Fetches the questions for Buyer's Surevy.
     *
     * @param $outline_id Int Id of the survey outline.
     * @return bool
     */
    public function get_bus_questions($outline_id, $phase, $full_screen_enabled)
    {
        // Condition to check if full screen view is enabled.
        $where = '';
//        (0 === $full_screen_enabled) ? ($where = ' and tsq_phase=' . $phase) : '';

        $sql = ' select tsq_id, tsq_content, tsq_phase_id'
            . ' from tsl_survey_question'
            . ' where tsq_phase_id=:phase_id';

        return $this->PDO->select($sql, array('phase_id' => $phase));
    }

    /**
     * Fetches and returns default answer for survey from the DB.
     *
     * @param $type String Type of Survey.
     * @return bool
     */
    public function get_survey_default_answer($type, $phase)
    {
        $sql = 'select tsda_seq, tsda_content'
            . ' from tsl_survey_default_answer'
            . ' where tsda_type=:type and tsda_phase=:phase';

        return $this->PDO->select($sql, array('type' => $type, 'phase' => $phase));
    }

    /**
     * Adds a new survey outline to the database.
     *
     * @param $client_type      String Type of client.
     * @param $survey_type      String Type of survey.
     * @return bool|\Exception
     */
    public function add_survey_outline($user_id, $survey_type, $client_type)
    {
        try {
            $this->PDO->begin_transaction();
            $sql = 'select max(tso_version) as version'
                . ' from tsl_survey_outline'
                . ' where tso_user_id=:user_id and tso_survey_type=:survey_type and'
                . ' tso_client_type=:client_type';

            $version = intval($this->PDO->select($sql, array('user_id' => $user_id, 'survey_type' => $survey_type,
                    'client_type' => $client_type))[0]->version) + 1;

            $sql = 'insert'
                . ' into tsl_survey_outline'
                . ' (tso_user_id, tso_version, tso_survey_type, tso_client_type, tso_survey_class)'
                . ' values(:user_id, :version, :survey_type, :client_type, :class)';

            $outline_id = $this->PDO->insert($sql, array('user_id' => $user_id, 'version' => $version,
                'survey_type' => $survey_type, 'client_type' => $client_type, 'class' => '1'));

            // Insert the default values for phases.
            $sql = 'insert into component_phase'
                . ' (cp_name, cp_number, cp_component, cp_content, cp_outline_id)'
                . ' select cp_name, cp_number, cp_component, cp_content, ' . $outline_id
                . ' from component_phase'
                . ' where cp_outline_id=:id and cp_component=:component';

            $phase = $this->PDO->insert($sql, array('id' => 0, 'component' => 'survey'));

            $phase_number = 1;
            for ($current_phase = $phase; $current_phase < $phase + 5; $current_phase++) {
                $sql = 'insert into tsl_survey_question'
                    . ' (tsq_content, tsq_phase_id)'
                    . ' select tsq_content, ' . $current_phase
                    . ' from tsl_survey_question'
                    . ' join component_phase'
                    . ' on cp_id = tsq_phase_id'
                    . ' where cp_outline_id=0 and cp_component=:survey and cp_number=:phase_number';

                $this->PDO->insert($sql, array('survey' => 'survey', 'phase_number' => $phase_number));
                $phase_number++;
            }

            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return $e;
        }
        return $phase . ' ' . $outline_id;
    }


    public function add_profile_outline($user_id, $profile_type, $client_type)
    {
        try {

            $this->PDO->begin_transaction();
            $sql = 'select max(tpo_version) as version'
                . ' from tsl_profile_outline'
                . ' where tpo_user_id=:user_id and tpo_profile_type=:profile_type and'
                . ' tpo_client_type=:client_type';

            $version = intval($this->PDO->select($sql, array('user_id' => $user_id, 'profile_type' => $profile_type,
                    'client_type' => $client_type))[0]->version) + 1;

            $sql = 'insert'
                . ' into tsl_profile_outline'
                . ' (tpo_user_id, tpo_version, tpo_profile_type, tpo_client_type)'
                . ' values(:user_id, :version, :profile_type, :client_type)';

            $outline_id = $this->PDO->insert($sql, array('user_id' => $user_id, 'version' => $version,
                'profile_type' => $profile_type, 'client_type' => $client_type));

            // Insert the default values for phases.
            $sql = 'insert'
                . ' into component_phase'
                . ' (cp_name, cp_number, cp_component, cp_content, cp_outline_id)'
                . ' select cp_name, cp_number, cp_component, cp_content, ' . $outline_id
                . ' from component_phase'
                . ' where cp_outline_id=:id and cp_component=:component';

            $phase = $this->PDO->insert($sql, array('id' => 0, 'component' => 'profile'));

            $phase_number = 1;
            for ($current_phase = $phase; $current_phase < $phase + 5; $current_phase++) {
                $sql = 'insert'
                    . ' into tsl_profile_question'
                    . ' (tpq_content, tpq_phase)'
                    . ' select tpq_content, ' . $current_phase
                    . ' from tsl_profile_question'
                    . ' join component_phase'
                    . ' on cp_id = tpq_phase'
                    . ' where cp_outline_id=0 and cp_component=:component and cp_number=:phase_number';

                $this->PDO->insert($sql, array('component' => 'profile', 'phase_number' => $phase_number));
                $phase_number++;
            }

            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return $e;
        }
        return $phase . ' ' . $outline_id;
    }

    /**
     * Saves the content for bus outline.
     *
     * @param $phase_id     Int    Phase ID.
     * @param $content      Text   Content of BUS.
     * @param $match        array  Questions list.
     * @param $question_id
     * @return bool
     */
    public function update_phase_content($phase_id, $content, $match, $question_id)
    {

        $this->PDO->begin_transaction();
        try {
            $sql = 'update component_phase'
                . ' set cp_content=:content'
                . ' where cp_id=:phase_id';
            $phase_id = $this->PDO->update($sql, array('content' => $content, 'phase_id' => $phase_id));
            $index = 0;

            // Loop through Survey Questions.
            foreach ($match[0] as $value) {
                $sql = 'update'
                    . ' tsl_survey_question'
                    . ' set tsq_content=:content'
                    . ' where tsq_id=:ques_id';

                $this->PDO->insert($sql, array('content' => $value, 'ques_id' => $question_id[$index]));
                $index++;
            }
            $this->PDO->commit_transaction();

        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return $e;
        }
        return $phase_id;
    }

    /**
     * Updates the phase content for a profile.
     *
     * @param $phase_id     Int   Phase ID.
     * @param $content      Text  Content for script.
     * @param $match        Array Array of Questions.
     * @param $question_id  Array Questions ID array.
     * @return bool
     */
    public function update_profile_phase_content($phase_id, $content, $match, $question_id)
    {
        $this->PDO->begin_transaction();
        try {
            $sql = 'update component_phase'
                . ' set cp_content=:content'
                . ' where cp_id=:phase_id';
            $ret['phase_updated'] = $this->PDO->update($sql, array('content' => $content, 'phase_id' => $phase_id));
            $index = 0;

            // Loop through Survey Questions.
            foreach ($match[0] as $value) {
                $sql = 'insert into tsl_profile_question'
                    . ' (tpq_content, tpq_phase, tpq_seq)'
                    . ' values(:content, :phase, :seq)'
                    . ' on duplicate key'
                    . ' update tpq_content=:content';

                $this->PDO->insert($sql, array('content' => $value, 'phase' => $phase_id, 'seq' => $index + 1));
                $index++;
            }
            $this->PDO->commit_transaction();

        } catch (\Exception $e) {
            echo $e;
            exit();
            $this->PDO->rollback_transaction();
            return $e;
        }
        return $phase_id;
    }

    /**
     * Updates name of Buyer's Survey
     *
     * @param $id   Int     Id of the BUS.
     * @param $name String  New name for BUS.
     * @return bool
     */
    public function update_bus_name($id, $name)
    {
        $sql = 'update tsl_survey'
            . ' set tsl_survey_name=:name'
            . ' where tsl_survey_id=:id';

        return $this->PDO->update($sql, array('name' => $name, 'id' => $id));
    }

    /**
     * Updates Survey Outline Name.
     *
     * @param $id
     * @param $name
     * @return bool
     */
    public function update_bus_outline_name($id, $name)
    {
        $sql = 'update tsl_survey_outline'
            . ' set tso_name=:name'
            . ' where tso_id=:id';

        return $this->PDO->update($sql, array('name' => $name, 'id' => $id));
    }

    /**
     * Deletes the Buyer's Survey.
     *
     * @param $id  Int id of the bus.
     * @return bool
     */
    public function delete_bus($id)
    {
        $sql = 'delete'
            . ' from tsl_survey'
            . ' where tsl_survey_id=:id';

        return $this->PDO->delete($sql, array('id' => $id));
    }

    /**
     * Deletes the Buyer's Survey Outline.
     *
     * @param $outline_id Int Outline ID.
     * @return bool
     */
    public function delete_bus_outline($outline_id, $requester)
    {
        $table_name = 'tsl_survey_outline';
        $key = 'tso_id';

        // Check if the request is to delete a profile Outline.
        if ('profile' === $requester) {
            $table_name = 'tsl_profile_outline';
            $key = 'tpo_id';
        }

        $sql = 'delete'
            . ' from ' . $table_name
            . ' where ' . $key . '=:outline_id';

        return $this->PDO->delete($sql, array('outline_id' => $outline_id));
    }

    /**
     * Adds a client to the TSL
     *
     * @param $user_id  Int    ID of the user
     * @param $name     String name of the client
     * @param $phone    Int    phone number of client.
     * @param $email    String Email Id of the client.
     * @param $agent    Int    Id of the agent
     * @param $criteria Int    Id of the criteria.
     * @return bool
     */
    public function add_client($user_id, $name, $phone, $email, $agent, $criteria, $tsl_id)
    {
        $sql = ' insert into'
            . ' tsl_client'
            . ' ( tc_name, tc_phone, tc_email, tc_user_id, tc_tsl_id)'
            . ' values( :name, :phone, :email, :user_id, :tsl_id)';

        $params['user_id'] = $user_id;
        $params['name'] = $name;
        $params['phone'] = $phone;
        $params['email'] = $email;
        $params['tsl_id'] = $tsl_id;

        $client_id = $this->PDO->insert($sql, $params);

        // Create a record for client, where we can store additional info.
        $sql = ' insert into'
            . ' tsl_about_client'
            . ' (tac_tc_id)'
            . ' values(:client_id)';

        $this->PDO->insert($sql, array('client_id' => $client_id));

        $sql = ' insert into'
            . ' tsl_client_criteria'
            . ' (tccrit_client_id, tccrit_criteria_id)'
            . ' values(:client_id, :criteria_id)';

        $data['client_id'] = $client_id;
        $data['criteria_id'] = $criteria;

        return $this->PDO->insert($sql, $data);
    }

    /**
     * Returns the list of client for a user.
     *
     * @param $user_id  Int ID of the user.
     * @param $tsl_id
     * @param $level
     * @param $limit
     * @param $offset
     * @param string $sort_by
     * @param string $asset_type
     * @return bool
     */
    public function get_client_list($user_id, $tsl_id, $level, $limit, $offset, $sort_by = '', $asset_type = "")
    {
        $order_by = '';

        if ('' !== $sort_by) {
            'bn' === $sort_by ?
                $order_by = ', field(hc_main_phone, \'New Number Needed\') desc'
                :
                $order_by = ', field(tac_valid_phone, \'' . $sort_by . '\') desc';
        }

        $sql = 'select tc_id, hc_id, hc_name as tc_name, hc_main_phone as tc_phone, hc_mobile_phone, hc_second_phone,'
            . ' hc_client_email as tc_email, hca_name, hca_location, hca_address, tac_call_status_id, hc_selected_phone,'
            . ' hca_criteria_used, hca_status, hca_lng, hca_lat, hca_id, hca_criteria_used, hca_product_id,'
            . ' tac_cnv, tac_cb, tac_valid_phone, tac_cb_when, tac_result, tac_action,'
            . ' tac_action_when, tac_assignee, tac_caller, tac_last_call, ta_when, tn_next_call,'
            . ' tac_sequence, tac_level, tac_id, tsm_sequence_id, tms_sequence_number, tsm_component_id,'
            . ' GROUP_CONCAT( concat(\'{{\', hcp_id, \'}}\',hcp_type, \':\', `hcp_phone`)) as additional_phone'
            . ' from tsl_client'
            . ' left join tsl_spoken_message on tsm_client_id=tc_asset_id'
            . ' left join tsl_message_sequence on tms_id=tsm_sequence_id'
            . ' join tsl_about_client on tac_tc_id=tc_id'
            . ' join homebase_client_asset on hca_id=tc_asset_id'
            . ' join homebase_client on hca_client_id=hc_id'
            . ' left join homebase_client_phone'
            . ' on hcp_hc_id=hc_id'
            . ' left join tsl_notes'
            . ' on tn_client_id=hca_id and tn_next_call = ('
            . ' select min(tn_next_call) from tsl_notes'
            . ' where tn_client_id=hca_id and tn_next_call > now())'
            . ' left join tsl_actions'
            . ' on ta_client_id=hca_id and ta_when = ('
            . ' select min(ta_when) from tsl_actions'
            . ' where ta_client_id=hca_id and ta_when > now())'
            . ' where tc_tsl_id=:tsl_id and tac_level=:level and tc_type=:asset_type'
            . ' group by hc_id'
            . ' order by'
            . ' tac_sequence > 1 asc' . $order_by
            . ' limit ' . $limit . ' offset ' . $offset;

        return $this->PDO->select($sql, array('tsl_id' => $tsl_id, 'level' => $level, 'asset_type' => $asset_type));
    }

    /**
     * Returns the list of client for a user.
     *
     * @param $user_id  Int ID of the user.
     * @param $tsl_id
     * @param $level
     * @param $limit
     * @param $offset
     * @param string $sort_by
     * @param string $asset_type
     * @return bool
     */
    public function get_buyer_list($user_id, $tsl_id, $level, $limit, $offset, $sort_by = '', $asset_type = "")
    {
        $order_by = '';

        if ('' !== $sort_by) {
            'bn' === $sort_by ?
                $order_by = ', field(hc_main_phone, \'New Number Needed\') desc'
                :
                $order_by = ', field(tac_valid_phone, \'' . $sort_by . '\') desc';
        }

        $sql = 'select tc_id, hb_id as hc_id, hb_name as tc_name, hb_main_phone as tc_phone, hb_mobile_phone as hc_mobile_phone,'
            . ' hb_second_phone as hc_second_phone, hb_email as tc_email, hba_entity_name as hca_name,'
            . ' hba_market_area as hca_location, hba_entity_address as hca_address, tac_call_status_id,'
            . ' hba_criteria as hca_criteria_used, hba_status as hca_status, hba_id as hca_id, hba_product_type as hca_product_id,'
            . ' tac_cnv, tac_cb, tac_valid_phone, tac_cb_when, tac_result, tac_action, hb_selected_phone as hc_selected_phone,'
            . ' tac_action_when, tac_assignee, tac_caller, tac_last_call, ta_when, tn_next_call,'
            . ' tac_sequence, tac_level, tac_id, tsm_sequence_id, tms_sequence_number, tsm_component_id,'
            . ' GROUP_CONCAT( concat(\'{{\', hcp_id, \'}}\',hcp_type, \':\', `hcp_phone`)) as additional_phone'
            . ' from tsl_client'
            . ' left join tsl_spoken_message on tsm_client_id=tc_asset_id'
            . ' left join tsl_message_sequence on tms_id=tsm_sequence_id'
            . ' join tsl_about_client on tac_tc_id=tc_id'
            . ' join homebase_buyer_asset on hba_id=tc_asset_id'
            . ' join homebase_buyer on hba_hb_id=hb_id'
            . ' left join homebase_client_phone'
            . ' on hcp_hc_id=hb_id'
            . ' left join tsl_notes'
            . ' on tn_client_id=hba_id and tn_next_call = ('
            . ' select min(tn_next_call) from tsl_notes'
            . ' where tn_client_id=hba_id and tn_next_call > now())'
            . ' left join tsl_actions'
            . ' on ta_client_id=hba_id and ta_when = ('
            . ' select min(ta_when) from tsl_actions'
            . ' where ta_client_id=hba_id and ta_when > now())'
            . ' where tc_tsl_id=:tsl_id and tac_level=:level and tc_type=:asset_type'
            . ' group by hb_id'
            . ' order by'
            . ' tac_sequence > 1 asc' . $order_by
            . ' limit ' . $limit . ' offset ' . $offset;


        return $this->PDO->select($sql, array('tsl_id' => $tsl_id, 'level' => $level, 'asset_type' => $asset_type));
    }

    /**
     * Returns the level to which a TSL belongs.
     *
     * @param $tsl_id            Int    TSL ID
     * @param $user_id           Int    User ID
     * @param string $asset_type String Type of Asset: Buyer/Client
     * @return bool
     */
    public function get_tsl_level($tsl_id, $user_id, $asset_type = "")
    {
        $sql = ' select tsl_id'
            . ' from tsl'
            . ' join user_active_product'
            . ' on tsl_product_id=uap_product_id'
            . ' and uap_user_id=tsl_user_id'
            . ' where tsl_user_id=:user_id'
            . ' and tsl_id=:tsl_id';

        $tsl = $this->PDO->select($sql, array('user_id' => $user_id, 'tsl_id' => $tsl_id));

        if (empty($tsl)) {
            throw new \UnexpectedValueException("Could not get the requested TSL instance");
        }

        $sql = 'select distinct tac_level'
            . ' from tsl_about_client'
            . ' join tsl_client'
            . ' on tc_id=tac_tc_id'
            . ' where tc_tsl_id=:tsl_id and tc_type=:asset_type';

        return $this->PDO->select($sql, array('tsl_id' => $tsl_id, 'asset_type' => $asset_type));
    }


    /**
     * Adds the agent for TSL
     *
     * @param $agent_name  String  Name of the agent.
     * @param $tsl_id      Int     TSL instance Id.
     * @return bool
     */
    public function add_agent($agent_name, $tsl_id)
    {
        try {
            $this->PDO->begin_transaction();
            $sql = 'update tsl_agent'
                . ' set tsl_agent_selected=0'
                . ' where tsl_agent_tsl_id=:tsl_id';

            $this->PDO->update($sql, array('tsl_id' => $tsl_id));

            $sql = 'insert'
                . ' into tsl_agent'
                . '(tsl_agent_name, tsl_agent_tsl_id, tsl_agent_selected)'
                . ' values(:name, :tsl_id, 1)';

            $this->PDO->insert($sql, array('name' => $agent_name, 'tsl_id' => $tsl_id));
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }
        return true;
    }

    /**
     * Deletes the agent (prepared by name)
     *
     * @param $agent_id  Int Id of the agent.
     * @return bool
     */
    public function delete_agent($agent_id)
    {
        $sql = 'delete from tsl_agent'
            . ' where tsl_agent_id=:agent_id';

        return $this->PDO->delete($sql, array('agent_id' => $agent_id));
    }


    /**
     * Returns the List of criteria category.
     *
     * @param $tsl_id Int TSL ID
     * @return bool
     */
    public function get_category($tsl_id)
    {
        $sql = ' select tsl_criteria_category.tcc_name, tcc_id'
            . ' from tsl_criteria_category'
            . ' where tcc_tsl_id=:tsl_id';

        return $this->PDO->select($sql, array('tsl_id' => $tsl_id));
    }

    /**
     * Adds a criteria to the list.
     *
     * @param $criteria
     * @param $tsl_id
     * @param $user_id
     * @param $product_id
     * @return bool
     */
    public function add_criteria($criteria, $user_id, $product_id)
    {
        try {
            $this->PDO->begin_transaction();

            $sql = 'insert'
                . ' into user_criteria(uc_user_id, uc_name, uc_product_id)'
                . ' values(:user_id, :criteria, :product)';

            $this->PDO->insert($sql,
                array('user_id' => $user_id, 'criteria' => $criteria, 'product' => $product_id));
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }
        return true;
    }

//    /**
//     * Add Criteria category for a profession.
//     *
//     * @param $tsl_id         Id      TSL Id.
//     * @param $category_name  String  Name for the category
//     * @return bool
//     */
//    public function add_criteria_category($tsl_id, $category_name)
//    {
//
//        $sql = ' insert into tsl_criteria_category'
//            . ' (tcc_tsl_id, tcc_name)'
//            . ' values(:tsl_id, :category_name)';
//
//        return $this->PDO->insert($sql, array('tsl_id' => $tsl_id, 'category_name' => $category_name));
//    }


    /**
     * Deletes a  criteria.
     *
     * @param $criteria_id Int ID of the criteria.
     * @return bool
     */
    public function delete_criteria($criteria_id)
    {
        $sql = 'delete'
            . ' from user_criteria'
            . ' where uc_id=:id';

        return $this->PDO->delete($sql, array('id' => $criteria_id));
    }

    /**
     * Returns the list of script belonging to a user.
     *
     * @param $user_id Int Id of the user.
     * @return bool
     */
    public function get_script_list($user_id, $type_id, $version_id)
    {
        $sql = ' select tsn_id, ts_id, tsn_name, ts_version'
            . ' from tsl_script'
            . ' join tsl_script_name'
            . ' on ts_name_id=tsn_id'
            . ' where tsn_user_id=:user_id and tsn_type_id=:type_id and tsn_version_id=:version_id'
            . ' order by tsn_id, ts_version desc';

        return $this->PDO->select($sql, array('user_id' => $user_id, 'type_id' => $type_id, 'version_id' => $version_id));
    }

    /**
     * Saves script outline
     *
     * @param $user_id  Int ID of user.
     * @param $content  Text Content for script.
     * @param $match    Array Array with questions.
     * @param $type     String Type of script outline.
     * @return bool
     */
    public function save_script_outline
    ($user_id, $content, $match, $type, $requester, $client_type, $focus, $criteria, $ta)
    {

        // Prepare Where Clause.
        $where = ' tsl_script_user_id=:user_id and tsl_script_outline_script_type=:script_type';

        $params['user_id'] = $user_id;
        $params['script_type'] = $type;

        // Check if target audience is empty,
        if (!empty($ta)) {

            $where .= ' and tsl_script_outline_ta=:ta and tsl_script_outline_criteria=:criteria'
                . ' and tsl_script_outline_focus=:focus and tsl_script_outline_type=:client_type';

            $params['ta'] = $ta;
            $params['criteria'] = $criteria;
            $params['focus'] = $focus;
            $params['client_type'] = $client_type;

        } else {
            $where .= ' and tsl_script_outline_ta is null and tsl_script_outline_criteria is null'
                . ' and tsl_script_outline_focus is null and tsl_script_outline_type is null';
        }

        // Prepare SQL query.
        $sql = 'select max(tsl_script_outline_version) as version'
            . ' from tsl_script_outline'
            . ' where ' . $where;

        $version = intval($this->PDO->select($sql, $params)[0]->version) + 1;

        $sql = 'insert into'
            . ' tsl_script_outline'
            . ' (tsl_script_outline_content, tsl_script_user_id, tsl_script_outline_version,'
            . ' tsl_script_outline_script_type, tsl_script_outline_ta, tsl_script_outline_criteria,'
            . ' tsl_script_outline_focus, tsl_script_outline_type)'
            . ' values(:content, :user_id, :version, :script_type, :ta, :criteria, :focus, :client_type)';

        $outline_id = $this->PDO->insert($sql, array('user_id' => $user_id, 'version' => $version,
            'content' => $content, 'script_type' => $type, 'ta' => $ta, 'criteria' => $criteria, 'focus' => $focus,
            'client_type' => $client_type));

        foreach ($match[0] as $value) {
            $sql = ' insert into'
                . ' tsl_script_question'
                . ' (tsl_script_question_content, tsl_script_question_outline_id)'
                . ' values(:content, :outline_id)';
            $this->PDO->insert($sql, array('content' => $value, 'outline_id' => $outline_id));
        }
        return $outline_id;
    }

    /**
     * Fetches types of script available.
     *
     * @param $origin   String Requset URL.
     * @param $req_type
     * @return bool
     */
    public function get_script_type($origin, $req_type)
    {
        $sql = 'select tsl_script_default_id,'
            . ' tsl_script_type, tsl_script_content'
            . ' from tsl_script_default'
            . ' where tsl_script_requester=:req';

        return $this->PDO->select($sql, array('req' => $req_type));
    }


    /**
     * Fetches the Script outline
     *
     * @param $user_id      Int     User ID
     * @param $script_type  String  Script Type
     * @return bool
     */
    public function get_script_outline($user_id, $script_type, $ta, $criteria, $focus, $client_type)
    {

        $where = ' tsl_script_user_id=:user_id and tsl_script_outline_script_type=:script_type';

        $params['user_id'] = $user_id;
        $params['script_type'] = $script_type;

        if (!empty($ta)) {

            $where .= ' and tsl_script_outline_ta=:ta and tsl_script_outline_criteria=:criteria'
                . ' and tsl_script_outline_focus=:focus and tsl_script_outline_type=:client_type';

            $params['ta'] = $ta;
            $params['criteria'] = $criteria;
            $params['focus'] = $focus;
            $params['client_type'] = $client_type;

        } else {
            $where .= ' and tsl_script_outline_ta is null and tsl_script_outline_criteria is null'
                . ' and tsl_script_outline_focus is null and tsl_script_outline_type is null';
        }

        $sql = 'select tsl_script_outline_content, tsl_script_outline_version, tsl_script_outline_id'
            . ' from tsl_script_outline'
            . ' where ' . $where;

        return $this->PDO->select($sql, $params);
    }

    /**
     * Updates the name of the script.
     *
     * @param $script_id   Int     ID of the script.
     * @param $script_name String  New name for the script.
     * @return bool
     */
    public function update_script_name($script_id, $script_name)
    {
        $sql = ' update tsl_script_name'
            . ' set tsn_name=:script_name'
            . ' where tsn_id=:script_id';
        return $this->PDO->update($sql, array('script_name' => $script_name, 'script_id' => $script_id));
    }

    /**
     * Updates the content of script and adds it as new version.
     *
     * @param $data             Array Array of data.
     * @param $type
     * @param $count
     * @param $instance_id
     * @param $client_id
     * @param $outline_version
     * @return bool
     */
    public function update_script($data, $type, $count, $instance_id, $client_id, $outline_version)
    {
        try {

            $this->PDO->begin_transaction();

            // Set the Defaults field to zero.
            $sql = 'update tsl_script_answer'
                . ' set tsa_default=0'
                . ' where tsa_type=:type and tsa_instance_id=:instance_id';
            $this->PDO->update($sql, array('type' => $type, 'instance_id' => $instance_id));

            $sql = 'select tsa_id'
                . ' from tsl_script_answer'
                . ' where tsa_instance_id=:instance_id'
                . ' and tsa_type=:type and tsa_client_id=:client_id limit 1';

            $ans_id = $this->PDO->select($sql,
                array('instance_id' => $instance_id, 'type' => $type, 'client_id' => $client_id));

            // Checks if the question is being already answered by the client.
            if (empty($ans_id)) {
                $i = 1;
                // Inserts every answer in the database.
                while ($i <= $count) {
                    $sql = 'insert into'
                        . ' tsl_script_answer'
                        . ' (tsa_question_id, tsa_instance_id, tsa_content, tsa_type, tsa_client_id, tsa_default)'
                        . ' value(:question_id, :instance_id, :content, :type, :client_id, :outline_version)';

                    $ret['script_inserted'] = $this->PDO->insert($sql,
                        array('question_id' => $i, 'instance_id' => $instance_id, 'client_id' => $client_id,
                            'content' => $data->get('ans' . $i, '', 'RAW'), 'type' => $type,
                            'outline_version' => $outline_version));
                    $i++;
                }
            } else {
                $i = 1;
                // Updates every answer in the database.
                while ($i <= $count) {
                    $sql = 'update tsl_script_answer'
                        . ' set tsa_content=:content, tsa_default=:outline_version'
                        . ' where tsa_instance_id=:instance_id and tsa_client_id=:client_id'
                        . ' and tsa_type=:type and tsa_question_id=:question_id';

                    $ret['script_updated'] = $this->PDO->update($sql,
                        array('question_id' => $i, 'instance_id' => $instance_id, 'client_id' => $client_id,
                            'content' => $data->get('ans' . $i, '', 'RAW'), 'type' => $type,
                            'outline_version' => $outline_version));
                    $i++;
                }
            }
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $ret['err'] = $e;
            $this->PDO->rollback_transaction();
        }
        return $ret;
    }

    /**
     * Fetches Script answers
     *
     * @param $instance_id  INT Script Instance ID.
     * @param $type         String Type of script.
     * @return bool
     */
    public function get_script_ans($instance_id, $type, $client_id)
    {
        try {
            $this->PDO->begin_transaction();
            $sql = 'select tsa_id, tsa_content as answer, tsa_question_id as question'
                . ' from tsl_script_answer'
                . ' where tsa_instance_id=:instance_id'
                . ' and tsa_type=:type and tsa_client_id=:client_id';

            $ret = $this->PDO->select($sql,
                array('instance_id' => $instance_id, 'type' => $type, 'client_id' => $client_id));

            // Check if the result set is zero and return default content.
            if (0 === count($ret)) {
                $sql = 'select tsa_id, tsa_content, tsa_question_id'
                    . ' from tsl_script_answer'
                    . ' where tsa_instance_id=:instance_id'
                    . ' and tsa_type=:type and tsa_default=1';

                $ret = $this->PDO->select($sql, array('instance_id' => $instance_id, 'type' => $type));
            }

            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $ret = $e;
            $this->PDO->rollback_transaction();
        }
        return $ret;
    }

    /**
     * Adds a new script with default content for a user.
     *
     * @param $user_id  Int Id of the user.
     * @return bool
     */
    public function add_script($user_id, $type_id, $version_id, $component)
    {
        $name = 'Custom Script';

        // Check for the component name and set Default version name.
        if ('wks' === $component) {
            $name = 'Custom Skills';
        } else if ('event' === $component) {
            $name = 'Custom Event';
        }

        $sql = 'insert into'
            . ' tsl_script_name'
            . ' (tsn_user_id, tsn_name, tsn_type_id, tsn_version_id)'
            . ' values(:user_id,:name, :type_id, :version_id)';

        $name_id = $this->PDO->insert($sql,
            array('user_id' => $user_id, 'name' => $name, 'type_id' => $type_id,
                'version_id' => $version_id));

        $sql = 'insert into'
            . ' tsl_script'
            . ' (ts_name_id, ts_version)'
            . ' values(:name_id, :version)';

        return $this->PDO->insert($sql, array('name_id' => $name_id, 'version' => 1));
    }


    /**
     * Deletes a script
     *
     * @param $script_id Int Id of the script.
     * @return bool
     */
    public function delete_script($script_id)
    {
        $sql = 'delete from'
            . ' tsl_script'
            . ' where ts_id=:script_id';

        return $this->PDO->delete($sql, array('script_id' => $script_id));
    }

    /**
     * Fetches default answer for Scripts.
     *
     * @param $outline_id Int Outline ID.
     * @return bool
     */
    public function get_script_default_answers($outline_id)
    {
        $sql = 'select tsda_id, tsda_content, tsda_answer_number'
            . ' from tsl_script_default_answer'
            . ' where tsda_outline_id=:outline_id';

        return $this->PDO->select($sql, array('outline_id' => $outline_id));
    }

    /**
     * Returns the criteria belonging to a client.
     *
     * @param $asset_id Int Asset ID.
     * @return bool
     */
    public function get_client_criteria($asset_id)
    {
        $sql = 'select uc_name'
            . ' from user_criteria'
            . ' join homebase_client_asset'
            . ' on hca_criteria_used=uc_id'
            . ' where hca_id=:asset_id';

        return $this->PDO->select($sql, array('asset_id' => $asset_id));
    }

    /**
     * Returns the list of already used criteria for a client.
     *
     * @param $client_id  Int ID of the client.
     * @return bool
     */
    public function get_used_criteria($client_id)
    {
        $sql = ' select tccrit_criteria_id'
            . ' from tsl_client_criteria'
            . ' where tccrit_client_id=:client_id';

        return $this->PDO->select($sql, array('client_id' => $client_id));
    }

    /**
     * Uses a new criteria for a client.
     *
     * @param $client_id    Int ID of the client.
     * @param $criteria_id  Int ID of the criteria.
     * @return bool
     */
    public function use_new_criteria($client_id, $criteria_id)
    {
        $sql = 'insert into'
            . ' tsl_client_criteria'
            . ' (tccrit_client_id, tccrit_criteria_id)'
            . 'values(:client_id, :criteria_id)';

        return $this->PDO->select($sql, array('client_id' => $client_id, 'criteria_id' => $criteria_id));
    }

    /**
     * Deletes a criteria.
     *
     * @param $criteria_id Int of the criteria.
     * @return bool
     */
    public function delete_client_criteria($criteria_id)
    {
        $sql = ' delete from'
            . ' tsl_client_criteria'
            . ' where tccrit_id=:criteria_id';

        return $this->PDO->delete($sql, array('criteria_id' => $criteria_id));
    }

    /**
     * Updates the script performance/proficiency score.
     *
     * @param $script_id  Int    Id of the script.
     * @param $client_id  Int    Id of the client.
     * @param $cols_name  String Name of the column whose value is updated.
     * @param $values     Int    Value of each column.
     * @return bool
     */
    public function update_script_perf_prof($script_id, $client_id, $cols_name, $values)
    {
        $sql = 'select tsc_id'
            . ' from tsl_script_client'
            . ' where tsc_client_id=:client_id and tsc_script_id=:script_id';

        $result = $this->PDO->select($sql, array('client_id' => $client_id, 'script_id' => $script_id));

        $tsc_id = $result[0]->tsc_id;

        $val_name = '';
        foreach ($values as $key => $value) {
            $val_name .= ':' . $key . ',';
        }

        $cols_name = rtrim($cols_name, ',');
        $val_name = rtrim($val_name, ',');

        $sql = ' insert into'
            . ' tsl_script_perf_prof'
            . ' (tspp_script_client_id,' . $cols_name . ')'
            . ' values(:id,' . $val_name . ')';
        $values['id'] = $tsc_id;

        return $this->PDO->insert($sql, $values);
    }

    /**
     * Gets the performance / proficiency latest score.
     *
     * @param $script_id  Int ID of the script.
     * @param $client_id  Int Id of the client.
     * @return bool
     */
    public function get_perf_prof($script_id, $client_id)
    {

        $ret = [];

        $sql = 'select tsc_id'
            . ' from tsl_script_client'
            . ' where tsc_client_id=:client_id and tsc_script_id=:script_id';
        $result = $this->PDO->select($sql, array('client_id' => $client_id, 'script_id' => $script_id));

        if (empty($result)) {
            $sql = 'insert into'
                . ' tsl_script_client'
                . ' (tsc_client_id, tsc_script_id)'
                . ' values(:client_id, :script_id)';
            $tsc_id = $this->PDO->insert($sql, array('client_id' => $client_id, 'script_id' => $script_id));
        } else {
            $tsc_id = $result[0]->tsc_id;
        }

        $sql = 'select tsc_name, tsc_project'
            . ' from tsl_script_client'
            . ' where tsc_id=:id';

        $ret['name-project'] = $this->PDO->select($sql, array('id' => $tsc_id));

        // Name of columns to be fetched.
        $cols = 'tspp_perf1, tspp_perf2, tspp_perf3, tspp_perf3a, tspp_perf3b, tspp_perf3c, tspp_perf3d, tspp_perf3e,'
            . ' tspp_perf4, tspp_perf5, tspp_prof1, tspp_prof2, tspp_prof3, tspp_prof3a, tspp_prof3b, tspp_prof3c,'
            . ' tspp_prof3d, tspp_prof3e, tspp_prof4, tspp_prof5';

        $sql = 'select ' . $cols . ' from tsl_script_perf_prof'
            . ' where tspp_script_client_id=:tsc_id'
            . ' order by tspp_datetime desc'
            . ' limit 1';

        $ret['perf-prof'] = $this->PDO->select($sql, array('tsc_id' => $tsc_id));

        return $ret;
    }

    /**
     * Returns the email Id of the client.
     *
     * @param $client_id  Int Id of the client.
     * @return bool
     */
    public function get_client_email($client_id)
    {
        $sql = 'select tc_email, tc_name'
            . ' from tsl_client'
            . ' where tc_id=:id';

        return $this->PDO->select($sql, array('id' => $client_id));
    }

    /**
     * Returns the performance / proficiency score for a particular client and script between specified dates.
     *
     * @param $client_id   Int  Id of the client.
     * @param $script_id   Int  Id of the script.
     * @param $to_date     Date start date.
     * @param $from_date   Date end date.
     * @return bool
     */
    public function get_perf_score($client_id, $script_id, $to_date, $from_date)
    {
        // Column name to be fetched.
        $cols = 'tspp_datetime, IFNULL(tspp_perf1, 0) as tspp_perf1, IFNULL(tspp_perf2, 0) as tspp_perf2,'
            . ' IFNULL(tspp_perf3, 0) as tspp_perf3, IFNULL(tspp_perf3a, 0) as tspp_perf3a, IFNULL(tspp_perf3b, 0)'
            . ' as tspp_perf3b, IFNULL(tspp_perf3c, 0) as tspp_perf3c, IFNULL(tspp_perf3d, 0) as tspp_perf3d,'
            . ' IFNULL(tspp_perf3e, 0) as tspp_perf3e, IFNULL(tspp_perf4, 0) as tspp_perf4, IFNULL(tspp_perf5, 0)'
            . ' as tspp_perf5, IFNULL(tspp_prof1, 0) as tspp_prof1, IFNULL(tspp_prof2, 0) as tspp_prof2,'
            . ' IFNULL(tspp_prof3, 0) as tspp_prof3, IFNULL(tspp_prof3a, 0) as tspp_prof3a, IFNULL(tspp_prof3b, 0)'
            . ' as tspp_prof3b, IFNULL(tspp_prof3c, 0) as tspp_prof3c, IFNULL(tspp_prof3d, 0) as tspp_prof3d,'
            . ' IFNULL(tspp_prof3e, 0) as tspp_prof3e, IFNULL(tspp_prof4, 0) as tspp_prof4, IFNULL(tspp_prof5, 0) as tspp_prof5';

        $where = '';
        if ($to_date != null && $from_date != null) {
            $where = 'and (tspp_datetime BETWEEN \'' . $from_date . ' 00:00:00\'' . ' and \'' . $to_date . ' 23:59:59\')';
        }

        $sql = 'select ' . $cols
            . ' from tsl_script_perf_prof'
            . ' join tsl_script_client'
            . ' on tsc_id=tspp_script_client_id'
            . ' where tsc_client_id=:client_id and tsc_script_id=:script_id ' . $where;

        return $this->PDO->select($sql, array('client_id' => $client_id, 'script_id' => $script_id));
    }


    /**
     * Fetches Messages Outline
     *
     * @param $user_id  Int User Id.
     * @param $component
     * @return \Exception
     */
    public function get_message_outline($user_id, $component)
    {
        try {

            $sql = 'select tmo_id, tmo_content, tmo_name'
                . ' from tsl_message_outline'
                . ' where (tmo_user_id=:user_id or tmo_user_id=0) and tmo_type=:component';

            $ret['outline'] = $this->PDO->select($sql, array('user_id' => $user_id, 'component' => $component));

            $sql = 'select tpao_outline_id'
                . ' from tsl_promo_active_outline'
                . ' where tpao_user_id=:user_id and tpao_component=:component';

            $ret['active_id'] = $this->PDO->select($sql, array('user_id' => $user_id, 'component' => $component));
        } catch (\Exception $e) {
            return $e;
        }
        return $ret;
    }

    public function delete_msg_outline($outline_id)
    {
        try {
            $sql = 'delete from tsl_message_outline'
                . ' where tmo_id=:id';

            $ret = $this->PDO->delete($sql, array('id' => $outline_id));
        } catch (\Exception $e) {
            return $e;
        }
        return $ret;
    }

    /**
     * Creates a new message set.
     *
     * @param $user_id
     * @return bool
     */
    public function create_message_sequence($id, $component_name)
    {
        $sql = 'select max(tms_sequence_number) as version'
            . ' from tsl_message_sequence'
            . ' where tms_version_id=:version_id';

        $version_number = $this->PDO->select($sql, array('version_id' => $id))[0]->version;

        $sql = 'insert into'
            . ' tsl_message_sequence'
            . ' (tms_name, tms_version_id, tms_sequence_number)'
            . ' values(:name, :version_id, :sequence_number)';

        $name = 'Message ';

        ('stories' === $component_name) ? $name = 'Story ' : '';

        $params['name'] = $name . (intval($version_number) + 1);
        $params['version_id'] = $id;
        $params['sequence_number'] = intval($version_number) + 1;

        return $this->PDO->insert($sql, $params);
    }

    /**
     * Fetches Message Sequence.
     *
     * @param $id
     * @return bool
     */
    public function get_message_seq($id)
    {
        $sql = 'select tms_id, tms_name, tms_sequence_number'
            . ' from tsl_message_sequence'
            . ' where tms_version_id=:version_id';

        return $this->PDO->select($sql, array('version_id' => $id));
    }

    /**
     * Creates a new message sequence.
     *
     * @param $tool_id
     * @param $message_name
     * @param $client_id
     * @param $component_name
     * @param $user_id
     * @param $outline_id
     * @return bool
     */
    public function create_message($tool_id, $message_name, $client_id, $component_name, $user_id, $outline_id)
    {

        try {
            $this->PDO->begin_transaction();

            $sql = 'insert into'
                . ' tsl_message_version(tm_name, tm_component_name, tm_user_id, tm_tool_id, tm_client_id, tm_outline_id)'
                . ' values(:name, :component_name, :user_id, :tool_id, :client_id, :outline_id)';

            $version_id = $this->PDO->insert($sql, array('name' => $message_name, 'component_name' => $component_name,
                'user_id' => $user_id, 'tool_id' => $tool_id, 'client_id' => $client_id, 'outline_id' => $outline_id
            ));

            $ret = $this->create_msg_seq($component_name, $version_id);

            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            echo $e;
            $this->PDO->rollback_transaction();
            return false;
        }

        return $ret;
    }

    public function create_msg_seq($component_name, $version_id)
    {

        $count = 1;
        $sql = 'insert into'
            . ' tsl_message_sequence'
            . ' (tms_name, tms_version_id, tms_sequence_number)'
            . ' values (:name1, :version, :seq_num1),'
            . ' (:name2, :version, :seq_num2),'
            . ' (:name3, :version, :seq_num3),'
            . ' (:name4, :version, :seq_num4),'
            . ' (:name5, :version, :seq_num5)';

        $name = 'Message ';

        // Set the default name for Sequences.
        if ('stories' === $component_name) {
            $name = 'Story ';
        } else if ('Follow-Up' === $component_name) {
            $name = 'Follow-Up ';
        } else if ('Debriefing' === $component_name) {
            $name = 'Debriefing ';
        }

        $ret = $this->PDO->insert($sql, array(
            'seq_num1' => $count,
            'name1' => $name . ' Sequence ' . $count++,
            'seq_num2' => $count,
            'name2' => $name . ' Sequence ' . $count++,
            'seq_num3' => $count,
            'name3' => $name . ' Sequence ' . $count++,
            'seq_num4' => $count,
            'name4' => $name . ' Sequence ' . $count++,
            'seq_num5' => $count,
            'name5' => $name . ' Sequence ' . $count++,
            'version' => $version_id
        ));
    }

    /**
     * Fetches Voice Messages.
     *
     * @param $seq_id Int Sequence ID.
     * @return bool
     */
    public function get_message($tool_id, $client_id, $component_name, $user_id, $outline_id)
    {
        $sql = 'select tm_id, tm_name'
            . ' from tsl_message_version'
            . ' where tm_component_name=:component_name  and tm_outline_id=:outline_id'
            . ' and tm_user_id=:user_id and tm_tool_id=:tool_id';

        return $this->PDO->select($sql, array('component_name' => $component_name,
            'user_id' => $user_id, 'tool_id' => $tool_id, 'outline_id' => $outline_id));
    }

    /**
     * Fetches Message Content
     *
     * @param $seq_id Int Message Sequence ID.
     * @return bool
     */
    public function get_message_content($seq_id)
    {
        $sql = 'select tmc_id, tmc_index, tmc_content'
            . ' from tsl_message_content'
            . ' where tmc_seq_id=:seq_id';

        return $this->PDO->select($sql, array('seq_id' => $seq_id));
    }

    /**
     * Updates the content of message sequence data.
     *
     * @param $input
     * @param $seq_id
     * @param $count
     * @return bool
     */
    public function update_message_content($input, $seq_id, $count)
    {
        try {
            $this->PDO->begin_transaction();

            for ($i = 0; $i < $count; $i++) {
                $sql = 'insert'
                    . ' into tsl_message_content'
                    . ' (tmc_seq_id, tmc_index, tmc_content)'
                    . ' values(:seq_id, :index, :content)'
                    . ' on duplicate key update tmc_content=:content';

                $this->PDO->update($sql, array(
                    'seq_id' => $seq_id, 'index' => $i, 'content' => $input->get('ans' . $i, '', 'RAW')));
            }

            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            echo $e;
            $this->PDO->rollback_transaction();
            return false;
        }
        return true;
    }

    /**
     * Fetches default Sequence from the DB.
     *
     * @param $seq        Int    Sequence Number.
     * @param $component  String Component Name.
     * @return bool
     */
    public function get_default_seq($seq, $component)
    {
        $sql = 'select tmd_seq_number, tmd_text, tmd_order'
            . ' from tsl_message_default'
            . ' where tmd_seq_number=:seq and tmd_component=:component'
            . ' order by tmd_order';

        return $this->PDO->select($sql, array('seq' => $seq, 'component' => $component));
    }

    /**
     * Updates message set name.
     *
     * @param $name  String  new name for message set.
     * @param $id    Int     Id of the message set.
     * @return bool
     */
    public function update_msg_seq_name($name, $id)
    {
        $sql = ' update tsl_message_sequence'
            . ' set tms_name=:name'
            . ' where tms_id=:id';

        return $this->PDO->update($sql, array('name' => $name, 'id' => $id));
    }

    /**
     * Updates the name of message sequence.
     *
     * @param $name  String New Name foe message.
     * @param $id    Int    Id of the message.
     * @return bool
     */
    public function update_msg_name($name, $id)
    {
        $sql = ' update tsl_message_version'
            . ' set tm_name=:name'
            . ' where tm_id=:id';

        return $this->PDO->update($sql, array('name' => $name, 'id' => $id));
    }

    /**
     * Deletes message instance.
     *
     * @param $msg_id  Int Message Id
     * @return bool
     */
    public function delete_msg($msg_id)
    {
        $sql = ' delete from'
            . ' tsl_message_version'
            . ' where tm_id=:msg_id';

        return $this->PDO->delete($sql, array('msg_id' => $msg_id));
    }

    /**
     * Deletes message sequence instance.
     *
     * @param $seq_id  Int Message Sequence ID.
     * @return bool
     */
    public function delete_seq($seq_id)
    {
        $sql = ' delete from'
            . ' tsl_message_sequence'
            . ' where tms_id=:seq_id';

        return $this->PDO->delete($sql, array('seq_id' => $seq_id));
    }


    /**
     * Fetches TSL data.
     *
     * @param $tsl_id Int TSL id.
     * @return array
     */
    public function get_tsl_data($tsl_id, $user_id)
    {
        $ret = [];
        $sql = ' select tsl_assignee_id, tsl_assignee_name'
            . ' from tsl_assignee'
            . ' where tsl_assignee_user_id=:user_id';

        $ret['assignee'] = $this->PDO->select($sql, array('user_id' => $user_id));

        $sql = ' select tsl_caller_id, tsl_caller_name'
            . ' from tsl_caller'
            . ' where tsl_caller_tsl_id=:tsl_id';

        $ret['caller'] = $this->PDO->select($sql, array('tsl_id' => $tsl_id));

        $sql = ' select ul_id, ul_name'
            . ' from user_location'
            . ' where ul_user_id=:user_id';

        $ret['audience'] = $this->PDO->select($sql, array('user_id' => $user_id));

        $sql = 'select uc_id, uc_name'
            . ' from user_criteria'
            . ' where uc_user_id=:user_id';

        $ret['criteria'] = $this->PDO->select($sql, array('user_id' => $user_id));

        $sql = 'select tsl_agent_id, tsl_agent_name, tsl_agent_selected'
            . ' from tsl_agent'
            . ' where tsl_agent_tsl_id=:tsl_id';

        $ret['agent'] = $this->PDO->select($sql, array('tsl_id' => $tsl_id));

        $sql = 'select tsl_name, tsl_modified, uact_cpd_id, cpd_name, tsl_description, ts_id, ts_duration'
            . ' from tsl '
            . ' left join user_active_cpd_tgd on uact_user_id=tsl_user_id'
            . ' left join cpd'
            . ' on uact_cpd_id=cpd_id'
            . ' left join tsl_sam'
            . ' on ts_tsl_id=tsl_id and ts_active=1'
            . ' where tsl_id=:id';

        $ret['tsl'] = $this->PDO->select($sql, array('id' => $tsl_id));

        // Get User Portfolio.
        $sql = 'select up_id, up_name'
            . ' from user_portfolio'
            . ' where up_user_id=:user_id';

        $ret['portfolio'] = $this->PDO->select($sql, array('user_id' => $user_id));

        return $ret;
    }

    public function get_metric_data($tsl_id, $period)
    {

        $where = '';

        ('' !== $period) ? $where = ' and tac_updated_at > ' . $period : '';

        $sql = 'SELECT sum(seq) as dial'
            . ' from ('
            . ' select count(tac_id) as seq'
            . ' from tsl_about_client'
            . ' join tsl_client'
            . ' on tac_tc_id=tc_id'
            . ' where tac_sequence=2'
            . ' and tc_tsl_id=:id'
            . $where . ')';

        $ret['dial'] = $this->PDO->select($sql, array('id' => $tsl_id));

        $sql = 'select count(tac_id) as contacts'
            . ' from tsl_about_client'
            . ' join tsl_client'
            . ' on tac_tc_id=tc_id'
            . ' where tac_valid_phone="cnt"'
            . ' and tc_tsl_id=:id' . $where;

        $ret['cnt'] = $this->PDO->select($sql, array('id' => $tsl_id));

        $sql = 'select count(tac_id) as conversation'
            . ' from tsl_about_client'
            . ' join tsl_client'
            . ' on tac_tc_id=tc_id'
            . ' where tac_valid_phone="cnv"'
            . ' and tc_tsl_id=:id' . $where;

        $ret['cnv'] = $this->PDO->select($sql, array('id' => $tsl_id));

        $sql = 'select count(tac_id) as cb'
            . ' from tsl_about_client'
            . ' join tsl_client'
            . ' on tac_tc_id=tc_id'
            . ' where tac_valid_phone="cb"'
            . ' and tc_tsl_id=:id' . $where;

        $ret['cb'] = $this->PDO->select($sql, array('id' => $tsl_id));

        $sql = 'select count(tac_id) as na'
            . ' from tsl_about_client'
            . ' join tsl_client'
            . ' on tac_tc_id=tc_id'
            . ' where tac_valid_phone="na"'
            . ' and tc_tsl_id=:id' . $where;

        $ret['na'] = $this->PDO->select($sql, array('id' => $tsl_id));

        $sql = 'select count(tsm_id) as lm'
            . ' from tsl_client'
            . ' join tsl_spoken_message'
            . ' on tsm_client_id=tc_asset_id'
            . ' join tsl_about_client'
            . ' on tac_tc_id=tc_id'
            . ' where tc_tsl_id=:id' . $where;

        $ret['lm'] = $this->PDO->select($sql, array('id' => $tsl_id));

        $sql = 'select count(tac_id) as scheduled'
            . ' from tsl_about_client'
            . ' join tsl_client'
            . ' on tac_tc_id=tc_id'
            . ' where tac_result="scheduled"'
            . ' and tc_tsl_id=:id' . $where;

        $ret['sch'] = $this->PDO->select($sql, array('id' => $tsl_id));

        return $ret;
    }


    /**
     * Deletes a caller for a user.
     *
     * @param $caller_id  Int Id of the user.
     * @return bool
     */
    public function delete_caller($caller_id)
    {
        $sql = ' delete from'
            . ' tsl_caller'
            . ' where tsl_caller_id=:id';

        return $this->PDO->delete($sql, array('id' => $caller_id));
    }

    /**
     * Deletes a caller for a user.
     *
     * @param $caller_id  Int Id of the user.
     * @return bool
     */
    public function delete_action($action_id)
    {
        $sql = ' delete from'
            . ' tsl_actions'
            . ' where ta_id=:id';

        return $this->PDO->delete($sql, array('id' => $action_id));
    }

    /**
     * Deletes a assignee for a user.
     *
     * @param $assignee_id Int Agent Id to whom next action is assigned.
     * @return bool
     */
    public function delete_assignee($assignee_id)
    {
        $sql = ' delete from'
            . ' tsl_assignee'
            . ' where tsl_assignee_id=:id';

        return $this->PDO->delete($sql, array('id' => $assignee_id));
    }

    /**
     * Adds a new caller to the list.
     *
     * @param $name     String Caller Name.
     * @param $tsl_id   Int    TSL id
     * @return bool
     */
    public function add_caller($name, $tsl_id)
    {
        $sql = ' insert into'
            . ' tsl_caller'
            . ' (tsl_caller_name, tsl_caller_tsl_id)'
            . ' values (:name, :tsl_id)';

        return $this->PDO->insert($sql, array('name' => $name, 'tsl_id' => $tsl_id));
    }

    /**
     * Adds a new Action to the list.
     *
     * @param $text   String  Action Text.
     * @param $tsl_id Int    TSL id.
     * @return bool
     */
    public function add_action($text, $tsl_id)
    {
        $sql = ' insert into'
            . ' tsl_actions'
            . ' (ta_text, ta_tsl_id)'
            . ' values (:text, :tsl_id)';

        return $this->PDO->insert($sql, array('text' => $text, 'tsl_id' => $tsl_id));
    }

    /**
     * Adds a new Target audience instance for a user.
     *
     * @param $target_audience String Target Audience Name.
     * @param $user_id         Int    User ID.
     * @param $product_id      Int    Product ID.
     * @return bool
     */
    public function add_target_audience($target_audience, $user_id, $product_id)
    {
        try {
            $this->PDO->begin_transaction();

            $sql = 'insert into'
                . ' user_location'
                . ' (ul_name, ul_user_id, ul_product_id)'
                . ' values (:name, :user_id, :product_id)';

            $this->PDO->insert($sql,
                array('name' => $target_audience, 'user_id' => $user_id, 'product_id' => $product_id));

            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }
        return true;
    }

    /**
     * Deletes an instance of Target Audience.
     *
     * @param $target_audience_id Int Tragtet Audience ID.
     * @return bool
     */
    public function delete_target_audience($target_audience_id)
    {
        $sql = 'delete'
            . ' from user_location'
            . ' where ul_id=:id';

        return $this->PDO->delete($sql, array('id' => $target_audience_id));
    }

    /**
     * Adds a new caller to the list.
     *
     * @param $name     String Assignee Name.
     * @param $user_id
     * @return bool
     */
    public function add_assignee($name, $user_id)
    {
        $sql = ' insert into'
            . ' tsl_assignee'
            . ' (tsl_assignee_name, tsl_assignee_user_id)'
            . ' values (:name, :user_id)';

        return $this->PDO->insert($sql, array('name' => $name, 'user_id' => $user_id));
    }

    /**
     * Returns the survey.
     *
     * @param $name_id  Int Name Id.
     * @param $version  Int Version.
     * @return bool
     */
    public function get_survey_id($name_id, $version)
    {

        $sql = 'select tsl_survey_id'
            . ' from tsl_survey'
            . ' where tsl_survey_name_id=:id and tsl_survey_version=:version';

        return $this->PDO->select($sql, array('id' => $name_id, 'version' => $version));
    }

    /**
     * Adds a new token for survey sent.
     *
     * @param $token     Mixed Random generated token.
     * @param $client_id Int   Client ID.
     * @param $survey_id Int   Survey ID,
     * @return bool
     */
    public function add_token($token, $client_id, $survey_id)
    {
        $sql = ' insert into'
            . ' tsl_survey_client'
            . ' (tsc_client_id, tsc_token, tsc_survey_id)'
            . ' values(:client_id, :token, :survey_id)';

        return $this->PDO->insert($sql, array('client_id' => $client_id, 'token' => $token, 'survey_id' => $survey_id));
    }

    /**
     * Fetches different promo outline versions.
     *
     * @param $user_id       Int  User ID.
     * @param $promo_format  Char Format of promo document.
     * @param $promo_focus   Char Event for promo.
     * @param $client_type   Char Size of client.
     * @return bool
     */
    public function get_promo_outline($user_id, $promo_format, $promo_focus, $client_type, $criteria, $audience)
    {
        $sql = 'select tpo_id, tpo_content, tpo_version, tpo_name'
            . ' from tsl_promo_outline'
            . ' where (tpo_user_id=:user_id)'
            . ' or tpo_id=0 or tpo_id=-1';

        $this->ret['outline'] = $this->PDO->select($sql, array('user_id' => $user_id));

        $sql = 'select tpao_outline_id'
            . ' from tsl_promo_active_outline'
            . ' where tpao_user_id=:user_id and tpao_component=:component';

        $this->ret['active_id'] = $this->PDO->select($sql, array('user_id' => $user_id, 'component' => 'promo'));

        return $this->ret;
    }

    /**
     * Saves promo outline and extract questions from it.
     *
     * @param $user_id      Int   User ID.
     * @param $promo_focus  Char  Event or Occurrence.
     * @param $promo_format Char  Format for document.
     * @param $client_type  Char  Size of client.
     * @param $outline      Text  Outline Content.
     * @param $match        Array Questions extracted from outline.
     * @return bool
     */
    public function save_promo_outline($user_id, $promo_focus, $promo_format, $client_type,
                                       $outline, $criteria, $target_audience)
    {
        // Get max available version of outline.
        $sql = 'select max(tpo_version) as version'
            . ' from tsl_promo_outline'
            . ' where tpo_user_id=:user_id and tpo_promo_focus=:promo_focus and tpo_audience=:audience'
            . ' and tpo_client_type=:client_type and tpo_promo_format=:promo_format and tpo_criteria=:criteria';

        $version = intval($this->PDO->select($sql, array('user_id' => $user_id, 'promo_focus' => $promo_focus,
                'client_type' => $client_type, 'promo_format' => $promo_format, 'audience' => $target_audience,
                'criteria' => $criteria))[0]->version) + 1;

        // Insert outline content into DB.
        $sql = 'insert into '
            . ' tsl_promo_outline'
            . ' (tpo_content, tpo_user_id, tpo_version, tpo_promo_focus, tpo_client_type, tpo_promo_format,'
            . ' tpo_audience, tpo_criteria)'
            . ' values(:content, :user_id, :version, :promo_focus, :client_type, :promo_format, :audience, :criteria)';


        return $this->PDO->insert($sql, array('user_id' => $user_id, 'version' => $version, 'content' => $outline,
            'promo_focus' => $promo_focus, 'promo_format' => $promo_format, 'client_type' => $client_type,
            'audience' => $target_audience, 'criteria' => $criteria));

    }

    /**
     * Updates promo name in DB.
     *
     * @param $id    Int    Promo ID.
     * @param $value String Name
     * @return bool
     */
    public function update_promo_name($id, $value)
    {
        $sql = 'update tsl_promo'
            . ' set tp_org_name=:name'
            . ' where tp_id=:id';

        return $this->PDO->update($sql, array('name' => $value, 'id' => $id));
    }

    /**
     * Fetch default promo answers.
     *
     * @param $outline_id   Int Outline ID.
     * @return bool
     */
    public function get_default_promo_answers($outline_id)
    {
        $sql = 'select tpda_answer_number, tpda_content'
            . ' from tsl_promo_default_answer'
            . ' where tpda_outline_id=:outline_id';

        return $this->PDO->select($sql, array('outline_id' => $outline_id));
    }

    /**
     * Fetches the List of Promos for a user.
     *
     * @param $outline_id Int Outline ID.
     * @param $user_id
     * @param $client_id
     * @return array
     */
    public function get_promo_list($outline_id, $user_id, $client_id)
    {

        $sql = ' select tp_id, tp_org_name, tp_rand_name, tp_content, coalesce(tpc_content, (select tpc_content'
            . ' from tsl_promo_content where tpc_promo_id=tp_id)) as tpc_content, tpc_id'
            . ' from tsl_promo'
            . ' left join tsl_promo_content'
            . ' on tpc_promo_id=tp_id and tpc_client_id=' . $client_id
            . ' where tp_outline_id=:outline_id and tp_content is not null and tp_user_id=:user_id'
            . ' order by tp_id';

        // Array to return.
        $ret = [];
        $ret['promo'] = $this->PDO->select($sql, array('outline_id' => $outline_id, 'user_id' => $user_id));

        $sql = ' select tp_id, tp_org_name, tp_rand_name, tp_content'
            . ' from tsl_promo'
            . ' where tp_outline_id=:outline_id and tp_rand_name is not null and tp_user_id=:user_id'
            . ' order by tp_id';

        $ret['pdf'] = $this->PDO->select($sql, array('outline_id' => $outline_id, 'user_id' => $user_id));

        return $ret;
    }

    /**
     * Fetches Email Outline.
     *
     * @param $email_event String Event Name.
     * @param $email_type  String Type of email.
     * @param $user_id     Int    User ID.
     * @return bool
     */
    public function get_email_outline($email_event, $email_type, $user_id, $email_purpose, $ta, $criteria, $focus)
    {
        // Prepare SQL query.
        $sql = 'select teo_id, teo_content, teo_version'
            . ' from tsl_email_outline';

        $where = ' where teo_event=:event and teo_type=:type and teo_user_id=:user_id and teo_purpose=:purpose';
        $params['event'] = $email_event;
        $params['type'] = $email_type;
        $params['user_id'] = $user_id;
        $params['purpose'] = $email_purpose;

        if (!empty($ta)) {
            $where .= ' and teo_ta=:ta and teo_criteria=:criteria and teo_focus=:focus';

            $params['ta'] = $ta;
            $params['criteria'] = $criteria;
            $params['focus'] = $focus;
        } else {
            $where .= ' and teo_ta is null and teo_criteria is null and teo_focus is null';
        }

        $sql = $sql . $where;

        return $this->PDO->select($sql, $params);
    }

    /**
     * Fetches Default email content.
     *
     * @param $email_event  String Event Name.
     * @return bool
     */
    public function get_email_default($email_event)
    {
        $sql = ' select ted_content, ted_type, ted_id'
            . ' from tsl_email_default'
            . ' where ted_event=:event';

        return $this->PDO->select($sql, array('event' => $email_event));
    }

    /**
     * Fetches and returns default answers for Email from DB.
     *
     * @param $id   Int Default Outline ID for Email.
     * @return bool
     */
    public function get_email_default_answer($id)
    {
        $sql = 'select teda_question_number, teda_content'
            . ' from tsl_email_default_answer'
            . ' where teda_default_id=:outline_id';

        return $this->PDO->select($sql, array('outline_id' => $id));

    }

    /**
     * Saves an instance of Email Outline.
     *
     * @param $user_id       Int     User ID.
     * @param $content       Text    Email Outline Content.
     * @param $match         Array   Different Email Questions.
     * @param $email_type    String  Type of Email.
     * @param $email_purpose String  Purpose of Email.
     * @param $email_event   String  Event for email.
     * @return bool
     */
    public function save_email_outline
    ($user_id, $content, $match, $email_type, $email_purpose, $email_event, $ta, $criteria, $focus)
    {

        // Prepare SQL query.
        $sql = 'select max(teo_version) as version'
            . ' from tsl_email_outline';

        $where = ' where teo_event=:event and teo_type=:type and teo_user_id=:user_id and teo_purpose=:purpose';
        $params['event'] = $email_event;
        $params['type'] = $email_type;
        $params['user_id'] = $user_id;
        $params['purpose'] = $email_purpose;

        if (!empty($ta)) {
            $where .= ' and teo_ta=:ta and teo_criteria=:criteria and teo_focus=:focus';

            $params['ta'] = $ta;
            $params['criteria'] = $criteria;
            $params['focus'] = $focus;
        } else {
            $where .= ' and teo_ta is null and teo_criteria is null and teo_focus is null';
        }

        $sql = $sql . $where;

        $version = intval($this->PDO->select($sql, $params)[0]->version) + 1;

        $sql = 'insert into'
            . ' tsl_email_outline'
            . ' (teo_content, teo_user_id, teo_version, teo_type, teo_event,'
            . ' teo_purpose, teo_ta, teo_criteria, teo_focus)'
            . ' values(:content, :user_id, :version, :type, :event, :purpose, :ta, :criteria, :focus)';

        $outline_id = $this->PDO->insert($sql, array('user_id' => $user_id, 'version' => $version,
            'content' => $content, 'type' => $email_type, 'purpose' => $email_purpose, 'event' => $email_event,
            'ta' => ($ta !== '' ? $ta : NULL), 'criteria' => ($criteria !== '' ? $criteria : NULL),
            'focus' => ($focus !== '' ? $focus : NULL)));

        return $outline_id;
    }

    /**
     * Fetches the list of emails.
     *
     * @param $outline_id   Int    Outline ID.
     * @param $email_type   String Type of email.
     * @param $email_event  String Event for email.
     * @return bool
     */
    public function get_email_list($outline_id, $user_id)
    {
        $sql = ' select te_id, te_name'
            . ' from tsl_email'
            . ' where te_outline_id=:outline_id and te_user_id=:user_id';

        return $this->PDO->select($sql, array('outline_id' => $outline_id, 'user_id' => $user_id));
    }

    /**
     * Adds a new email instance.
     *
     * @param $email_type   String  Type of email.
     * @param $outline_id   Int     Outline ID.
     * @param $email_event  String  Event for email.
     * @return bool
     */
    public function add_email_instance($outline_id, $user_id)
    {
        $sql = ' insert into'
            . ' tsl_email'
            . ' (te_outline_id, te_name, te_user_id)'
            . ' values(:outline_id, :name, :user_id)';

        return $this->PDO->insert($sql, array('outline_id' => $outline_id, 'name' => 'Custom Confirm',
            'user_id' => $user_id));
    }

    /**
     * Fetches email content.
     *
     * @param $client_id Int Client ID
     * @param $email_id  Int Email ID
     * @return bool
     */
    public function get_email_content($client_id, $email_id)
    {
        // Where clause conditions.
        $sql = ' select tea_id, tea_question_number, tea_content '
            . ' from tsl_email_answer where tea_email_id=:email_id and tea_client_id=:client_id';

        return $this->PDO->select($sql, array('client_id' => $client_id, 'email_id' => $email_id));
    }

    /**
     * Updates Email content.
     *
     * @param $count      Int   Question Number.
     * @param $client_id  Int   ID of client.
     * @param $email_id   Int   ID of email instance.
     * @param $data       Array Array of answers.
     * @return bool
     */
    public function set_email_content($count, $client_id, $email_id, $data)
    {
        try {
            $this->PDO->begin_transaction();
            $sql = 'select tea_id'
                . ' from tsl_email_answer'
                . ' where tea_client_id=:client_id and tea_email_id=:email_id limit 1';
            $ans_id = $this->PDO->select($sql, array('client_id' => $client_id, 'email_id' => $email_id));

            // Checks if the question is being already answered by the client.
            if (empty($ans_id)) {


//            $sql = "DELIMITER $$"
//                . " DROP PROCEDURE IF EXISTS test $$"
//                . " CREATE PROCEDURE test ()"
//                . " BEGIN"
//                . " DECLARE i INT DEFAULT 1;"
//                . " WHILE i <= :count DO"
//                . " insert into tsl_email_answer"
//                . " (tea_question_number, tea_client_id, tea_content, tea_email_id)"
//                . " value(i, :client_id, " . $data->get('ans' . " i "  , '', 'RAW') . ", :email_id)"
//                . " SET i = i + 1;"
//                . " END WHILE;"
//                . " END $$"
//                . " DELIMITER ;";

                $i = 1;

                // Inserts every answer in the database.
                while ($i <= $count) {
                    $sql = 'insert into tsl_email_answer'
                        . ' (tea_question_number, tea_client_id, tea_content, tea_email_id)'
                        . ' value(:question_no, :client_id, :content, :email_id)';

                    $this->PDO->insert($sql, array('question_no' => $i, 'client_id' => $client_id,
                        'content' => $data->get('ans' . $i, '', 'RAW'), 'email_id' => $email_id));
                    $i++;
                }
            } else {
                $i = 1;

                // Updates every answer in the database.
                while ($i <= $count) {
                    $sql = 'update tsl_email_answer'
                        . ' set tea_content=:content'
                        . ' where tea_client_id=:client_id and tea_question_number=:question_no and tea_email_id=:email_id';

                    $this->PDO->update($sql, array('question_no' => $i, 'client_id' => $client_id,
                        'content' => $data->get('ans' . $i, '', 'RAW'), 'email_id' => $email_id));
                    $i++;
                }
            }
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }
        return true;
    }

    /**
     * Delete an email outine.
     *
     * @param $outline_id Int Outline ID
     * @return bool
     */
    public function delete_email_outline($outline_id)
    {
        $sql = 'delete from tsl_email_outline'
            . ' where teo_id=:outline_id';
        return $this->PDO->delete($sql, array('outline_id' => $outline_id));
    }

    /**
     * Deletes an email.
     *
     * @param $id   Int ID of email.
     * @return bool
     */
    public function delete_email($id)
    {
        $sql = 'delete from tsl_email'
            . ' where te_id=:id';
        return $this->PDO->delete($sql, array('id' => $id));
    }

    /**
     * Updates Email name.
     *
     * @param $id   Int     Id of email.
     * @param $name String  New Name for email.
     * @return bool
     */
    public function update_email_name($id, $name)
    {
        $sql = ' update tsl_email'
            . ' set te_name=:name'
            . ' where te_id=:id';

        return $this->PDO->update($sql, array('name' => $name, 'id' => $id));

    }

    /**
     * Update client information.
     *
     * @param $id    Int     ID of client.
     * @param $col   String  Field to be updated
     * @param $val   Mixed   Value To be updated.
     * @param $table String  Table Name.
     * @return bool
     */
    public function update_client_info($id, $col, $val, $table)
    {

        try {
            $this->PDO->begin_transaction();

            // Set Primary column name.
            if ($table == 'tsl_about_client') {
                $pk = 'tac_id';

                $sql = 'update tsl_about_client'
                    . ' set tac_updated_at=now()'
                    . ' where tac_id=:id';

                $ret['date_updated'] = $this->PDO->update($sql, array('id' => $id));
            } else if ($table == 'tsl_about_buyer') {
                $pk = 'tab_id';

                $sql = 'update tsl_about_buyer'
                    . ' set tab_updated_at=now()'
                    . ' where tab_id=:id';

                $ret['date_updated'] = $this->PDO->update($sql, array('id' => $id));
            } else if ($table === 'homebase_client_asset') {
                $pk = 'hca_id';
            } else if ($table === 'homebase_client') {
                $pk = 'hc_id';
            } else if ($table === 'homebase_client_phone') {
                $pk = 'hcp_id';
            } else if ($table === 'homebase_buyer_asset') {
                $pk = 'hba_id';
            } else if ($table === 'homebase_buyer') {
                $pk = 'hb_id';
            } else {
                $pk = 'tc_id';
            }

            $sql = 'update ' . $table
                . ' set ' . $col . '=:val'
                . ' where ' . $pk . '=:id';

            $ret['updated'] = $this->PDO->update($sql, array('val' => $val, 'id' => $id));
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return $e->getMessage();
        }
        return $ret;

    }

    /**
     * Adds user defined Notes,
     *
     * @param $val      String Value for note.
     * @param $tsl_id   Int     ID of TSL instance.
     * @return bool
     */
    public function add_user_notes($val, $tsl_id)
    {

        $sql = ' insert into tsl_notes'
            . '(tn_content, tn_tsl_id )'
            . ' values(:val, :tsl_id)';

        return $this->PDO->insert($sql, array('val' => $val, 'tsl_id' => $tsl_id));
    }

    /**
     * Deletes a script outline.
     *
     * @param $outline_id Int Outline ID
     * @return bool
     */
    public function delete_script_outline($outline_id)
    {
        $sql = 'delete from tsl_script_outline'
            . ' where tsl_script_outline_id=:outline_id';

        return $this->PDO->delete($sql, array('outline_id' => $outline_id));
    }

    /**
     * Deletes the record for package document from the DB.
     *
     * @param $id   Int Id of the package document.
     * @return bool
     */
    public function delete_sa($id)
    {
        $sql = 'delete from cpd_sa'
            . ' where cs_id=:id';
        return $this->PDO->delete($sql, array('id' => $id));
    }

    /**
     * Pushes a client to next TSL level.
     *
     * @param $tac_id       Int Client Info Id
     * @param $sequence     Int Sequence in current level.
     * @param $client_id    Int Client ID.
     * @param $tac_level    Int Current Level.
     * @param $caller       Int Id of caller.
     * @return array
     */
    public function push_client($tac_id, $sequence, $client_id, $tac_level, $caller)
    {

        try {
            $this->PDO->begin_transaction();

            // Check if this row need to be pushed to next level.
            if ($sequence == 1) {
                $ret = [];

                $sql = 'insert into tsl_about_client'
                    . ' (tac_tc_id, tac_level)'
                    . ' values(:client_id, :tac_level)';

                $ret['new_level'] = $this->PDO->insert($sql,
                    array('client_id' => $client_id, 'tac_level' => ($tac_level + 1)));

                $sql = 'update tsl_about_client'
                    . ' set tac_sequence=:sequence'
                    . ' where tac_id=:tac_id';

                $ret['sequence_updated'] = $this->PDO->update($sql,
                    array('sequence' => ($sequence + 1), 'tac_id' => $tac_id));
            } else {

                $sql = 'update tsl_about_client'
                    . ' set tac_sequence=:sequence, tac_last_call=NOW(), tac_caller=:caller'
                    . ' where tac_id=:tac_id';

                $ret['sequence_updated'] = $this->PDO->update($sql,
                    array('sequence' => ($sequence + 1), 'tac_id' => $tac_id, 'caller' => $caller));

            }
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {

            echo $e;
            exit();
            $this->PDO->rollback_transaction();
            return false;
        }
        return $ret;
    }

    /**
     * Moves a deal and related data to CPD.
     *
     * @param $tac_id   Int Client Info ID.
     * @param $user_id  Int Current User Id.
     * @param $tc_id    Int Client Id.
     * @return array
     */
    public function move_deal($tac_id, $user_id, $tc_id)
    {
        try {
            $this->PDO->begin_transaction();
            $ret = [];
            $sql = 'select tac_result, tc_asset_id, tc_type'
                . ' from tsl_client join tsl_about_client'
                . ' on tc_id=tac_tc_id'
                . ' where tac_id=:tac_id';

            $tac_info = $this->PDO->select($sql, array('tac_id' => $tac_id));

            //Fetch active CPD Id for selected Product and User.
            $sql = 'select uact_cpd_id'
                . ' from user_active_cpd_tgd'
                . ' where uact_user_id=:user_id';
            $active_cpd =
                $this->PDO->select($sql, array('user_id' => $user_id))[0]->uact_cpd_id;

            $sql = 'insert'
                . ' into deal'
                . ' (deal_cpd_id,'
                . ' deal_created, deal_updated, deal_asset_id, deal_type)'
                . ' values( :cpd_id, NOW(), NOW(), :asset_id, :type)';

            $deal_id = $this->PDO->insert($sql, array('cpd_id' => $active_cpd,
                'asset_id' => ($tac_info[0]->tc_asset_id), 'type' => $tac_info[0]->tc_type));

            $sql = 'insert'
                . ' into homebase_asset_status'
                . ' (has_status, has_md, has_asset_id, has_datetime, has_type)'
                . ' values (:status, month(now()), :asset_id, now(), :type)';

            $ret['deal_moved'] = $this->PDO->insert($sql, array('status' => $tac_info[0]->tac_result,
                'asset_id' => $tac_info[0]->tc_asset_id, 'type' => $tac_info[0]->tc_type));

            $sql = 'delete'
                . ' from tsl_client'
                . ' where tc_id= :tc_id';

            $ret['instance_removed'] = $this->PDO->delete($sql, array('tc_id' => $tc_id));
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {

            echo $e;
            exit();
            return false;
            $this->PDO->rollback_transaction();
        }
        return $ret;
    }

    /**
     * Adds a new record for the component sent.
     *
     * @param $origin      String Requester.
     * @param $type        String Type of component.
     * @param $client_id   Int    Client Id.
     * @param $doc_id      Int    Document Id.
     * @param $tool_id     Int    Tool Id.
     * @param $content_id  Int    Content Id.
     * @return bool
     */
    public function add_component_sent($origin, $type, $client_id, $doc_id, $tool_id, $content_id)
    {
        $sql = 'insert into component_sent'
            . ' (component_sent_origin, component_sent_doc_type,'
            . ' component_sent_client_id, component_sent_doc_id,'
            . ' component_sent_tool_id, component_sent_content_id)'
            . ' values (:origin, :type, :client_id, :doc_id, :tool_id, :content_id)';

        return $this->PDO->insert($sql, array(
            'origin' => $origin,
            'type' => $type,
            'client_id' => $client_id,
            'doc_id' => $doc_id,
            'tool_id' => $tool_id,
            'content_id' => $content_id
        ));
    }

    /**
     * Fetches Promotional Doc details.
     *
     * @param $id       Int Id of Doc.
     * @param $user_id  Int User Id.
     * @return bool
     */
    public function get_promo_details($id, $user_id)
    {
        $sql = 'select tpo_client_type, tpo_promo_format,'
            . ' tpo_promo_focus, tp_outline_id, tpf_tool_id, tpf_origin, tpf_name'
            . ' from tsl_promo join tsl_promo_outline'
            . ' on tpo_id=tp_outline_id'
            . ' join tsl_promo_focus'
            . ' on tpo_promo_focus=tpf_id'
            . ' where tp_id=:id and tpo_user_id=:user_id';

        return $this->PDO->select($sql, array('id' => $id, 'user_id' => $user_id));
    }

    /**
     * Updates Worksheet Information.
     *
     * @param $script_id Int    Script ID.
     * @param $client_id Int    Client ID.
     * @param $name      String Name of worksheet.
     * @param $project   String Project for worksheet.
     * @return bool
     */
    public function update_wks_info($script_id, $client_id, $name, $project)
    {
        $sql = ' update tsl_script_client'
            . ' set tsc_name=:name, tsc_project=:project'
            . ' where tsc_client_id=:client_id and tsc_script_id=:script_id';

        return $this->PDO->select($sql, array('script_id' => $script_id, 'client_id' => $client_id, 'name' => $name, 'project' => $project));
    }

    /**
     * Delete a TSL instance.
     *
     * @param $id
     * @return bool
     */
    public function delete_tsl($id)
    {
        try {
            $this->PDO->begin_transaction();

            $sql = 'delete'
                . ' from tsl'
                . ' where tsl_id=:id';

            $ret['tsl_deleted'] = $this->PDO->delete($sql, array('id' => $id));

            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return $e;
        }
        return $ret;
    }

    /**
     * Fetch Notes content.
     *
     * @param $id
     * @return bool
     */
    public function get_action_content($id)
    {
        $sql = 'select ta_content, ta_whom, ta_when, ta_seq, ta_date'
            . ' from tsl_actions'
            . ' where ta_client_id=:id'
            . ' order by ta_seq';

        return $this->PDO->select($sql, array('id' => $id));
    }

    /**
     * Update Notes content for a client
     *
     * @param $client_id  Int     Client ID.
     * @param $col        String  Column Name.
     * @param $val        String  Value.
     * @param $seq        Int     Sequence.
     * @return bool
     */
    public function update_action_content($client_id, $col, $val, $seq)
    {
        $sql = ' insert into tsl_actions'
            . ' (ta_client_id, ta_seq, ' . $col . ')'
            . ' values(:client_id, :seq, :value)'
            . ' ON DUPLICATE KEY UPDATE ' . $col . '=:value';

        return $this->PDO->insert($sql, array('client_id' => $client_id, 'seq' => $seq, 'value' => $val));
    }

    /**
     * Fetches Notes content.
     *
     * @param $client_id Int Client ID
     * @return bool
     */
    public function get_notes_content($client_id)
    {
        $sql = 'select tn_id, tn_content, tn_seq, tn_topic, tn_datetime, tn_next_call, tn_action'
            . ' from tsl_notes'
            . ' where tn_client_id=:client_id';

        return $this->PDO->select($sql, array('client_id' => $client_id));
    }

    /**
     * Update Notes content.
     *
     * @param $client_id Int    Client ID.
     * @param $col       String Column name.
     * @param $val       Text   value for field.
     * @param $seq       Int    Sequence
     * @return bool
     */
    public function update_notes_content($client_id, $col, $val, $seq)
    {
        $sql = ' insert into tsl_notes'
            . ' (tn_client_id, tn_seq, ' . $col . ')'
            . ' values(:client_id, :seq, :value)'
            . ' ON duplicate key update ' . $col . '=:value';

        return $this->PDO->insert($sql, array('client_id' => $client_id, 'seq' => $seq, 'value' => $val));
    }

    /**
     *Adds a new promo instance.
     *
     * @param $outline_id Int Promo outline ID.
     * @param $user_id    Int User ID.
     * @return bool
     */
    public function add_promo_instance($outline_id, $user_id, $name)
    {
        $sql = 'insert'
            . ' into tsl_promo'
            . ' (tp_org_name, tp_outline_id, tp_content, tp_user_id)'
            . ' values(:name, :outline_id, :content, :user_id)';

        return $this->PDO->insert($sql,
            array('name' => $name, 'outline_id' => $outline_id, 'content' => '',
                'user_id' => $user_id));
    }

    /**
     * Updates Promo Instance content.
     *
     * @param $content
     * @param $form_data
     * @param $active_promo_id
     * @return bool
     */
    public function update_promo_instance($content, $form_data, $active_promo_id, $questions_length)
    {
        try {
            $this->PDO->begin_transaction();

            $sql = 'update tsl_promo'
                . ' set tp_content=:content'
                . ' where tp_id=:id';

            $this->PDO->update($sql, array('id' => $active_promo_id, 'content' => $content));

            //  Loop over every Answer.
            for ($i = 0; $i < $questions_length; $i++) {

                $sql = 'insert into tsl_promo_answer'
                    . ' (tpa_promo_id, tpa_question_id, tpa_text)'
                    . ' values(:promo,:ques,:text)'
                    . ' on duplicate key'
                    . ' update tpa_text =:text';

                $this->PDO->insert($sql, array('promo' => $active_promo_id, 'ques' => $i,
                    'text' => $form_data['ans' . $i]));
            }

            $this->PDO->commit_transaction();

        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }
        return true;
    }

    /**
     * Propagates Promo Content.
     *
     * @param $client_id    Int  Client Id.
     * @param $user_id      Int  User Id.
     * @param $promo_id     Int  Promo version Id.
     * @param $content      Text Promo version content
     * @return bool
     */
    public function propagate_promo($client_id, $user_id, $promo_id, $content)
    {
        try {
            $this->PDO->begin_transaction();

            $sql = 'update tsl_promo_content'
                . ' left join component_sent'
                . ' on component_sent_doc_id=tpc_promo_id'
                . ' and component_sent_client_id=tpc_client_id'
                . ' set tpc_content=:content '
                . ' where tpc_promo_id=:promo_id and tpc_client_id!=:client_id'
                . ' and tpc_id not in'
                . ' (select component_sent_content_id '
                . ' from component_sent'
                . ' where component_sent_doc_type=:type'
                . ' and component_sent_doc_id=:promo_id'
                . ' and component_sent_client_id=:client_id)';

            $ret['content_updated'] = $this->PDO->update($sql,
                array('promo_id' => $promo_id, 'client_id' => $client_id, 'content' => $content, 'type' => 'pages'));

            // Updates Deafult for promo content.
            $sql = 'update tsl_promo_content'
                . ' set tpc_default=0'
                . ' where tpc_promo_id=:promo_id';

            $this->PDO->update($sql, array('promo_id' => $promo_id));

            $sql = 'update tsl_promo_content'
                . ' set tpc_default=1'
                . ' where tpc_client_id=:client_id and tpc_promo_id=:promo_id';

            $ret['default_updated'] = $this->PDO->update($sql,
                array('client_id' => $client_id, 'promo_id' => $promo_id));
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            $ret['err'] = $e;
        }

        return $ret;
    }

    /**
     * Updates Promo Content
     *
     * @param $id
     * @param $content
     * @return bool
     */
    public function save_promo_content($id, $content, $client_id)
    {
        $sql = 'update tsl_promo'
            . ' set tp_content=:content'
            . ' where tp_id=:id';

        return $this->PDO->update($sql, array('content' => $content, 'id' => $id));

//        $sql = 'insert into tsl_promo_content'
//            . ' (tpc_client_id, tpc_promo_id, tpc_content)'
//            . ' values(:client_id, :promo_id, :content)'
//            . ' on duplicate key'
//            . ' update  tpc_content=:content';
//
//        return $this->PDO->insert($sql, array('content' => $content, 'promo_id' => $id, 'client_id' => $client_id));
    }

    public function upload_promo($org_name, $random, $outline_id, $user_id)
    {
        $sql = 'insert into tsl_promo'
            . ' (tp_org_name, tp_rand_name, tp_outline_id, tp_user_id)'
            . ' values(:org_name, :random, :outline, :user_id)';

        return $this->PDO->insert($sql, array('org_name' => $org_name, 'random' => $random,
            'outline' => $outline_id, 'user_id' => $user_id));
    }

    /**
     * Fetcch Promo Details
     *
     * @param $id   Int ID
     * @return bool
     */
    public function get_promo_data($id)
    {
        $sql = 'select tpa_text as answer, tpa_question_id as question'
            . ' from tsl_promo_answer'
            . ' where tpa_promo_id =:promo_id';

        return $this->PDO->select($sql, array('promo_id' => $id));
    }

    /**
     * Fetch the complete list of Criteria/Target Audience/Prepared By Name for a user.
     *
     * @param $user_id
     * @return array|bool
     */
    public function get_user_data($user_id, $product_id)
    {
        try {
            $this->PDO->begin_transaction();

            $ret = [];

            $sql = 'select uc_id, uc_name'
                . ' from user_criteria'
                . ' where uc_user_id=:user_id';

            $ret['criteria'] = $this->PDO->select($sql, array('user_id' => $user_id));

            // Fetch the list of Target Audience.
            $sql = ' select ul_id, ul_name'
                . ' from user_location'
                . ' where ul_user_id=:user_id';

            $ret['target_audience'] = $this->PDO->select($sql, array('user_id' => $user_id));

            // Fetch the list of Prepared By.
            $sql = 'select tsl_agent_id, tsl_agent_name'
                . ' from tsl_agent'
                . ' join tsl'
                . ' on tsl_agent_tsl_id=tsl_id'
                . ' where tsl_user_id=:user_id';

            $ret['prepared_by'] = $this->PDO->select($sql, array('user_id' => $user_id));

            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }
        return $ret;
    }

    /**
     * Fetches data for individual TSL.
     *
     * @param $tsl_id   Int TSL ID.
     * @param $user_id  Int User ID.
     * @return bool
     */
    public function get_user_tsl_data($tsl_id, $user_id, $product_id)
    {
        $sql = "select uc_id, uc_name"
            . " from user_criteria"
            . " where uc_user_id=:user_id and uc_product_id=:product_id and"
            . " uc_id not in ("
            . " select tsl_criteria_criteria_id"
            . " from tsl_criteria"
            . " where tsl_criteria_tsl_id=:tsl_id"
            . ")";

        $ret['criteria'] = $this->PDO->select($sql, array(
            'tsl_id' => $tsl_id,
            'user_id' => $user_id,
            'product_id' => $product_id
        ));

        $sql = "select ul_id, ul_name"
            . " from user_location"
            . " where ul_user_id=:user_id and ul_product_id=:product_id and"
            . " ul_id not in ("
            . " select tta_location_id"
            . " from tsl_target_audience"
            . " where tta_tsl_id=:tsl_id"
            . " )";

        $ret['location'] = $this->PDO->select($sql,
            array(
                'user_id' => $user_id,
                'product_id' => $product_id,
                'tsl_id' => $tsl_id
            ));

        return $ret;
    }

    /**
     * Add a new copy of message for the particular message version.
     *
     * @param $id   Int Message Version ID
     * @return bool
     */
    public function add_copy_message($id)
    {
        $sql = 'insert into'
            . ' tsl_message_content'
            . ' (tmc_version_id, tmc_content)'
            . ' values(:version_id, :content)';

        return $this->PDO->insert($sql, array('version_id' => $id, 'content' => ''));
    }

    /**
     * Deletes a copy of message.
     *
     * @param $id   Int Message ID.
     * @return bool
     */
    public function delete_message_copy($id)
    {
        $sql = 'delete from'
            . ' tsl_message_content'
            . ' where tmc_id=:id';

        return $this->PDO->delete($sql, array('id' => $id));
    }

    /**
     * Fetches the criteria categories.
     *
     * @param $id
     * @return bool
     */
    public function get_criteria_category($id)
    {
        $sql = 'select tcc_id, tcc_name'
            . ' from tsl_criteria_category'
            . ' where tcc_tsl_id=:tsl_id';

        return $this->PDO->select($sql, array('tsl_id' => $id));
    }

    public function save_as_new_msg($msg_id)
    {
        try {
            $sql = 'insert into tsl_message_version'
                . ' (tm_name, tm_component_name, tm_user_id,'
                . ' tm_tool_id, tm_client_id, tm_outline_id)'
                . ' select tm_name, tm_component_name,'
                . ' tm_user_id, tm_tool_id, tm_client_id, tm_outline_id'
                . ' from tsl_message_version where tm_id=:msg_id';

            $version_id = $this->PDO->insert($sql, array('msg_id' => $msg_id));
            $component_name = 'Message';

            $ret = $this->create_msg_seq($component_name, $version_id);
        } catch (\Exception $e) {
            echo $e;
            return $e;
        }
        return $ret;
    }

    /**
     * Fetches Criteria for this client.
     *
     * @param $category_id Int Category ID.
     * @return bool
     */
    public function get_criteria($category_id)
    {
        $sql = 'select tsl_criteria_id, tsl_criteria_name'
            . ' from tsl_criteria'
            . ' where tsl_criteria_category_id=:id';

        return $this->PDO->select($sql, array('id' => $category_id));
    }

    /**
     * Fetch Target audience and Criteria for a particular TSL.
     *
     * @param $tsl_id Int TSL ID.
     * @return mixed
     */
    public function get_tsl_ta_criteria($tsl_id, $user_id)
    {

        // Fetch Target Audience for this TSL.
        $sql = ' select ul_id, ul_name'
            . ' from user_location'
            . ' join tsl_target_audience'
            . ' on tta_location_id=ul_id'
            . ' where ul_user_id=:user_id and tta_tsl_id=:tsl_id';

        $ret['ta'] = $this->PDO->select($sql, array('user_id' => $user_id, 'tsl_id' => $tsl_id));

        // Fetch the list of TSL criteria.
        $sql = 'select uc_id, uc_name'
            . ' from user_criteria'
            . ' join tsl_criteria'
            . ' on tsl_criteria_criteria_id=uc_id'
            . ' where uc_user_id=:user_id and tsl_criteria_tsl_id=:tsl_id';

        $ret['criteria'] = $this->PDO->select($sql, array('user_id' => $user_id, 'tsl_id' => $tsl_id));
        return $ret;
    }

    /**
     * Updates TSL data for the user.
     *
     * @param $tsl_id           Int     TSL Id.
     * @param $component_id     Int     Component ID.
     * @param $type             String  Type of Component.
     * @return bool|\Exception
     */
    public function update_tsl_data($tsl_id, $component_id, $type, $user_id)
    {
        try {
//            $this->PDO->begin_transaction();
            if ('location' !== $type) {
                $sql = 'insert'
                    . ' into tsl_criteria'
                    . ' (tsl_criteria_tsl_id, tsl_criteria_criteria_id)'
                    . ' values(:tsl_id, :criteria_id)'
                    . ' ON DUPLICATE KEY UPDATE tsl_criteria_tsl_id=:tsl_id';

                $ret['criteria_inserted'] = $this->PDO->insert($sql, array('tsl_id' => $tsl_id,
                    'criteria_id' => $component_id));

                // Fetch and insert assets with selected criteria and move to TSL instance.
                $sql = 'insert into'
                    . ' tsl_client'
                    . ' (tc_asset_id, tc_tsl_id, tc_temp)'
                    . ' select hca_id, ' . $tsl_id . ', ' . $user_id
                    . ' from homebase_client_asset'
                    . ' join homebase_client'
                    . ' on hca_client_id=hc_id'
                    . ' where hc_user_id=:user_id and hca_criteria_used=:criteria_id'
                    . ' on duplicate key update tc_asset_id=tc_asset_id';

                $ret['client_added'] = $this->PDO->insert($sql,
                    array('user_id' => $user_id, 'criteria_id' => $component_id));

                // Create a record for client, where we can store additional info.
                $sql = ' insert into'
                    . ' tsl_about_client'
                    . ' (tac_tc_id)'
                    . ' select tc_id'
                    . ' from tsl_client'
                    . ' where tc_temp=:random';

                $ret['asset_added'] = $this->PDO->insert($sql, array('random' => $user_id));

                $sql = ' update tsl_client'
                    . ' set tc_temp=:val'
                    . ' where tc_temp=:random';

                $ret['temp_updated'] = $this->PDO->update($sql, array('val' => '', 'random' => $user_id));
            } else {
                $sql = 'insert'
                    . ' into tsl_target_audience'
                    . ' (tta_tsl_id, tta_location_id)'
                    . ' values(:tsl_id, :location_id)'
                    . ' ON DUPLICATE KEY UPDATE tta_tsl_id=:tsl_id';

                $ret['location_inserted'] = $this->PDO->insert($sql, array('tsl_id' => $tsl_id,
                    'location_id' => $component_id));

                // Fetch and insert assets with selected location and move to TSL instance.
                $sql = 'insert into'
                    . ' tsl_client'
                    . ' (tc_asset_id, tc_tsl_id, tc_temp)'
                    . ' select hca_id, ' . $tsl_id . ', ' . $user_id
                    . ' from homebase_client_asset'
                    . ' join homebase_client'
                    . ' on hca_client_id=hc_id'
                    . ' where hc_user_id=:user_id and hca_location=:location_id'
                    . ' on duplicate key update tc_asset_id=tc_asset_id';

                $ret['client_added'] = $this->PDO->insert($sql,
                    array('user_id' => $user_id, 'location_id' => $component_id));

                // Create a record for client, where we can store additional info.
                $sql = ' insert into'
                    . ' tsl_about_client'
                    . ' (tac_tc_id)'
                    . ' select tc_id'
                    . ' from tsl_client'
                    . ' where tc_temp=:random';

                $ret['asset_added'] = $this->PDO->insert($sql, array('random' => $user_id));

                $sql = ' update tsl_client'
                    . ' set tc_temp=:val'
                    . ' where tc_temp=:random';

                $ret['temp_updated'] = $this->PDO->update($sql, array('val' => '', 'random' => $user_id));
            }
//            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
//            $this->PDO->rollback_transaction();
            return $e;
        }
        return $ret;
    }

    /**
     * Fetch List of promo focus for a TSL instance.
     *
     * @param $tool_id Int Tool Id.
     * @return bool
     */
    public function get_promo_focus($tool_id, $origin)
    {
        $sql = 'select tpf_name, tpf_id, tpf_tool_id'
            . ' from tsl_promo_focus'
            . ' where (tpf_tool_id=:tool_id or tpf_tool_id=0) and tpf_origin=:origin';


        return $this->PDO->select($sql, array('tool_id' => $tool_id, 'origin' => $origin));
    }

    /**
     * Adds a new promo focus for the TSL.
     *
     * @param $tool_id           Int     Tool ID.
     * @param $promo_focus_name String  Promo Focus Name.s
     * @return bool
     */
    public function add_promo_focus($tool_id, $promo_focus_name, $origin)
    {
        $sql = 'insert into'
            . ' tsl_promo_focus'
            . ' (tpf_tool_id, tpf_name, tpf_origin)'
            . ' values(:tsl_id, :name, :origin)';

        return $this->PDO->insert($sql, array('tsl_id' => $tool_id, 'name' => $promo_focus_name, 'origin' => $origin));
    }

    /**
     * Deletes a Promotional Focus Instance.
     *
     * @param $promo_focus_id Int Promo Focus Id.
     * @return bool
     */
    public function delete_promo_focus($promo_focus_id)
    {
        $sql = 'delete from'
            . ' tsl_promo_focus'
            . ' where tpf_id=:id';

        return $this->PDO->delete($sql, array('id' => $promo_focus_id));
    }

    /**
     * Updates the content of promo component.
     *
     * @param $id               Int     ID of Promo component.
     * @param $type             String  Check whether the request is to update or to add new a new promo.
     * @param $content          Text    Content
     * @param $outline_id       Int     Outline ID for component.
     * @return bool|\Exception
     */
    public function update_promo_content($id, $type, $content, $outline_id, $user_id)
    {
        try {

            $this->PDO->begin_transaction();

            // Check if the type of request to update content.
            if ('recompose' === $type) {
                $sql = 'update tsl_promo'
                    . ' set tp_content=:content'
                    . ' where tp_id=:id';

                $this->PDO->update($sql, array('content' => $content, 'id' => $id));
            } // Check if the request is to add a new promo instance.
            else {
                $sql = 'insert'
                    . ' into tsl_promo'
                    . ' (tp_org_name, tp_outline_id, tp_content, tp_user_id)'
                    . ' values(:name, :outline_id, :content, :user_id)';

                $promo_id = $this->PDO->insert($sql,
                    array('name' => 'Custom Doc', 'outline_id' => $outline_id,
                        'content' => $content, 'user_id' => $user_id));

                $sql = 'insert'
                    . ' into tsl_promo_answer'
                    . ' (tpa_promo_id, tpa_question_id, tpa_text)'
                    . ' select ' . $promo_id . ', tpa_question_id, tpa_text'
                    . ' from tsl_promo_answer'
                    . ' where tpa_promo_id=:id';

                $this->PDO->insert($sql, array('id' => $id));
            }
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            return $e;
            $this->PDO->rollback_transaction();
        }
        return true;
    }

    /**
     * Changes Active Outline for Promo component.
     *
     * @param $id           Int     Promo component ID.
     * @param $component    String  Component Name
     * @param $promo_focus  Int     Promo Focus ID.
     * @param $promo_format String  Promo Format.
     * @param $client_type  String  Type of client.
     * @param $criteria     Int     Criteria used for promo.
     * @param $location     Int     Location ID.
     * @param $origin       String  Origin of request.
     * @param $user_id      Int     User ID.
     * @return bool
     */
    public function change_active_outline
    ($id, $component, $promo_focus, $promo_format, $client_type, $criteria, $location, $origin, $user_id)
    {
        try {
            $this->PDO->begin_transaction();

            $sql = 'insert into tsl_promo_active_outline'
                . ' (tpao_user_id, tpao_outline_id, tpao_component)'
                . ' values(:user_id, :outline_id, :component)'
                . ' on duplicate key'
                . ' update  tpao_outline_id=:outline_id';

            $this->PDO->insert($sql, array('user_id' => $user_id, 'outline_id' => $id, 'component' => $component));
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return $e;
        }
        return true;
    }

    /**
     * Updates the name for promo outline.
     *
     * @param $id       Int     Promo Outline ID
     * @param $value    String  New name for promo outline.
     * @return bool
     */
    public function update_promo_outline_name($id, $value)
    {
        $sql = 'update tsl_promo_outline'
            . ' set tpo_name=:name'
            . ' where tpo_id=:id';

        return $this->PDO->update($sql, array('name' => $value, 'id' => $id));
    }

    /**
     * Fetch and return all the promo that belongs to a particular tsl/cpd instance.
     *
     * @param $tool_id  Int     Tool ID.
     * @param $origin   String  Request Originator.
     * @param $user_id  Int     User ID.
     * @param $input    Array   Array of inputs by user.
     * @return bool
     */
    public function get_promo_archive($tool_id, $origin, $user_id, $input, $component_name)
    {

        // Get the parameters.
        $start = $input->getInt('start');
        $length = $input->getInt('length');

        // Is there a search?
        $search = $_POST['search'];

        if (!empty($search['value'])) {
            $search_val = $search['value'];
        }

        // Get the column information
        $columns = $_POST['columns'];

        // Get the columns and the search
        $sql_columns = '';
        $sep = ' ';
        $sql_where = ' where tpf_origin=:origin and tpf_tool_id=:id and tpo_user_id=:user_id';
        $sep_where = ' and ';

        // Loop through each CPD column that will be used for searching and append to where clause.
        foreach ($columns as $column) {

            if (empty($column['data'])) {
                continue;
            }
            $search_col = $column['data'];
            $sql_columns .= $sep . $column['data'];
            $sep = ', ';

            // Add to the search?
            if (isset($search_val) && ($column['searchable'] == 'true')) {
                $sql_where .= $sep_where . "(cast(" . $search_col . " as char(100)) like '%" . $search_val . "%')";
                $sep_where = ' or ';
            }
        }

        if ('vmail' === $component_name) {
            // Get all the record of message for the tool instance.

            $sql = 'select tms_id as tp_id, tms_name as tp_org_name, ca_id,'
                . ' GROUP_CONCAT(DISTINCT tmc_content order by tmc_index SEPARATOR \' \') as tp_content, '
                . ' hca_name, hca_location, hc_name, ca_datetime, ca_response, ca_topic'
                . ' from tsl_message_sequence'
                . ' join component_archive'
                . ' on ca_tool_id=tms_id'
                . ' join homebase_client_asset'
                . ' on hca_id=ca_asset_id'
                . ' join homebase_client'
                . ' on hca_client_id=hc_id'
                . ' join tsl_message_content'
                . ' on tmc_seq_id=tms_id'
                . ' where ca_user_id=:user_id and ca_component=:component'
                . ' group by tms_id';

            $ret['data'] = $this->PDO->select($sql, array('user_id' => $user_id, 'component' => $component_name));

        } else {

            // Get all the record of promos for the tool instance.
            $sql = 'select tp_id, tp_org_name, tp_rand_name, tp_content, ca_id,'
                . ' hca_name, hca_location, hc_name, ca_datetime, ca_response, ca_topic'
                . ' from tsl_promo'
                . ' join component_archive'
                . ' on ca_tool_id=tp_id'
                . ' join homebase_client_asset'
                . ' on hca_id=ca_asset_id'
                . ' join homebase_client'
                . ' on hca_client_id=hc_id'
                . ' where ca_user_id=:user_id and ca_component=:component';

            $ret['data'] = $this->PDO->select($sql, array('user_id' => $user_id, 'component' => $component_name));
        }
        // Get count of all the record of promos for the tool instance.
        $sql = 'select count(tp_id)'
            . ' from tsl_promo'
            . ' join tsl_promo_outline'
            . ' on tpo_id=tp_outline_id'
            . ' join tsl_promo_focus'
            . ' on tpf_id=tpo_promo_focus'
            . $sql_where;

        $ret['recordsFiltered'] =
            $this->PDO->select_count($sql, array('id' => $tool_id, 'origin' => $origin, 'user_id' => $user_id));

        // Get count of all the record of promos for the user.
        $sql = 'select count(tp_id)'
            . ' from tsl_promo'
            . ' join tsl_promo_outline'
            . ' on tpo_id=tp_outline_id'
            . ' where tpo_user_id=:user_id';

        $ret['recordsTotal'] = $this->PDO->select_count($sql, array('user_id' => $user_id));
        return $ret;
    }

    /*
    * Fetch and return all the promo that belongs to a particular tsl/cpd instance.
    *
    * @param $tool_id  Int     Tool ID.
    * @param $origin   String  Request Originator.
    * @param $user_id  Int     User ID.
    * @param $input    Array   Array of inputs by user.
    * @return bool
    */
    public function get_script_archive($tool_id, $origin, $user_id, $input, $component_name)
    {

        // Get the parameters.
        $start = $input->getInt('start');
        $length = $input->getInt('length');

        // Is there a search?
        $search = $_POST['search'];

        if (!empty($search['value'])) {
            $search_val = $search['value'];
        }

        // Get the column information
        $columns = $_POST['columns'];

        // Get the columns and the search
        $sql_columns = '';
        $sep = ' ';
        $sql_where = '';
        $sql_where = ' where tsn_user_id=:user_id';
        $sep_where = ' and ';

        // Loop through each CPD column that will be used for searching and append to where clause.
        foreach ($columns as $column) {

            if (empty($column['data'])) {
                continue;
            }
            $search_col = $column['data'];
            $sql_columns .= $sep . $column['data'];
            $sep = ', ';

            // Add to the search?
            if (isset($search_val) && ($column['searchable'] == 'true')) {
                $sql_where .= $sep_where . "(cast(" . $search_col . " as char(100)) like '%" . $search_val . "%')";
                $sep_where = ' or ';
            }
        }

        // Determine the order information.
        $order = $_POST['order'];

        $sql_order = '';

        // Loop through columns for order by clause.
        foreach ($order as $o) {

            if (strlen($sql_order) > 0) {
                $sql_order .= ', ';
            } else {
                $sql_order = " order by ";
            }

            // Get the column
            $columns[$o['column']]['data'];
            $sql_order .= $columns[$o['column']]['data'] . ' ' . $o['dir'];
        }

        // Get all the record of promos for the tool instance.
        $sql = 'select ts_id, tsn_name, hca_name, hca_location, hc_name'
            . ' from tsl_script'
            . ' join tsl_script_name'
            . ' on ts_name_id=tsn_id'
            . ' join component_archive'
            . ' on ca_tool_id=ts_id'
            . ' join homebase_client_asset'
            . ' on hca_id=ca_asset_id'
            . ' join homebase_client'
            . ' on hca_client_id=hc_id'
            . ' where ca_user_id=:user_id and ca_component=:component';

        $ret['data'] = $this->PDO->select($sql, array('user_id' => $user_id, 'component' => 'script'));

        // Get count of all the record of promos for the tool instance.
        $sql = 'select count(ts_id)'
            . ' from tsl_script'
            . ' join tsl_script_name'
            . ' on tsn_id=ts_name_id'
            . $sql_where;

        $ret['recordsFiltered'] =
            $this->PDO->select_count($sql, array('user_id' => $user_id));

        // Get count of all the record of promos for the user.
        $sql = 'select count(ts_id)'
            . ' from tsl_script'
            . ' join tsl_script_name'
            . ' on tsn_id=ts_name_id'
            . $sql_where;

        $ret['recordsTotal'] = $this->PDO->select_count($sql, array('user_id' => $user_id));
        return $ret;
    }

    /**
     * Fetches and returns email archive list from the database.
     *
     * @param $tool_id          Int     Tool ID.
     * @param $origin           String  Request Originator
     * @param $user_id          Int     User ID.
     * @param $input            Array   Array of input.
     * @param $component_name   String  Component Name.
     * @return mixed
     */
    public function get_email_archive($tool_id, $origin, $user_id, $input, $component_name)
    {
        // Get the parameters.
        $start = $input->getInt('start');
        $length = $input->getInt('length');

        // Is there a search?
        $search = $_POST['search'];

        if (!empty($search['value'])) {
            $search_val = $search['value'];
        }

        // Get the column information
        $columns = $_POST['columns'];

        // Get the columns and the search
        $sql_columns = '';
        $sep = ' ';
        $sql_where = '';
        $sql_where = ' where tsn_user_id=:user_id';
        $sep_where = ' and ';

        // Loop through each CPD column that will be used for searching and append to where clause.
        foreach ($columns as $column) {

            if (empty($column['data'])) {
                continue;
            }
            $search_col = $column['data'];
            $sql_columns .= $sep . $column['data'];
            $sep = ', ';

            // Add to the search?
            if (isset($search_val) && ($column['searchable'] == 'true')) {
                $sql_where .= $sep_where . "(cast(" . $search_col . " as char(100)) like '%" . $search_val . "%')";
                $sep_where = ' or ';
            }
        }

        // Determine the order information.
        $order = $_POST['order'];

        $sql_order = '';

        // Loop through columns for order by clause.
        foreach ($order as $o) {

            if (strlen($sql_order) > 0) {
                $sql_order .= ', ';
            } else {
                $sql_order = " order by ";
            }

            // Get the column
            $columns[$o['column']]['data'];
            $sql_order .= $columns[$o['column']]['data'] . ' ' . $o['dir'];
        }

        // Get all the record of promos for the tool instance.
        $sql = 'select te_id, te_name, hca_name, hca_location, hc_name, ca_asset_id'
            . ' from tsl_email'
            . ' join component_archive'
            . ' on ca_tool_id=te_id'
            . ' join homebase_client_asset'
            . ' on hca_id=ca_asset_id'
            . ' join homebase_client'
            . ' on hca_client_id=hc_id'
            . ' where ca_user_id=:user_id and ca_component=:component';

        $ret['data'] = $this->PDO->select($sql, array('user_id' => $user_id, 'component' => 'email'));

        // Get count of all the record of promos for the tool instance.
        $sql = 'select count(te_id)'
            . ' from tsl_email'
            . $sql_where;

        $ret['recordsFiltered'] =
            $this->PDO->select_count($sql, array('user_id' => $user_id));

        // Get count of all the record of promos for the user.
        $sql = 'select count(te_id)'
            . ' from tsl_email'
            . $sql_where;

        $ret['recordsTotal'] = $this->PDO->select_count($sql, array('user_id' => $user_id));
        return $ret;
    }


    /**
     * Adds a component to the archive table.
     *
     * @param $promo_id     Int     Promo ID.
     * @param $user_id      Int     User ID.
     * @param $type         String  Type of Component.
     * @param $asset_id     Int     Asset ID.
     * @param $datetime     DateTime Datetime for archive
     * @param $topic        String   Topic for
     * @return bool
     */
    public function add_archive($promo_id, $user_id, $type, $asset_id, $datetime, $topic)
    {
        $sql = 'insert'
            . ' into component_archive'
            . ' (ca_component, ca_tool_id, ca_user_id, ca_asset_id, ca_datetime, ca_topic)'
            . ' values(:component, :tool_id, :user_id, :asset_id, :datetime, :topic)'
            . ' on duplicate key update ca_id=ca_id, ca_datetime=:datetime, ca_topic=:topic';

        return $this->PDO->insert($sql, array
        ('component' => $type, 'user_id' => $user_id, 'tool_id' => $promo_id, 'asset_id' => $asset_id,
            'datetime' => $datetime, 'topic' => $topic));
    }

    /**
     * Fetches outline for profile.
     *
     * @param $project_type  Int    Type of Project
     * @param $profile_phase Int    Phase for Profile.
     * @param $profile_type  Int    Type of Profile.
     * @param $client_type   String Type of client (Radish, Wheat, Tree)
     * @param $user_id       Int    User ID
     * @return bool
     */
    public function get_profile_outline($project_type, $profile_phase, $profile_type, $client_type, $user_id)
    {

        $sql = 'select tpo_id, tpo_name, tpo_version '
            . ' from tsl_profile_outline'
            . ' where (tpo_user_id=:user_id and tpo_client_type=:client_type'
            . ' and tpo_profile_type=:profile_type) or tpo_id=0';


        return $this->PDO->select($sql, array('user_id' => $user_id, 'client_type' => $client_type,
            'profile_type' => $profile_type));
    }


    /**
     * Fetches and returns profile Default outline.
     *
     * @param $profile_type Int Profile type
     * @return bool
     */
    public function get_profile_default($profile_type)
    {
        $sql = 'select tpd_id, tpd_content'
            . ' from tsl_profile_default'
            . ' where tpd_profile_type=:profile_type';

        return $this->PDO->select($sql, array('profile_type' => $profile_type));
    }

    /**
     * Fetches and returns Profile Questions
     *
     * @param $outline_id Int Outline ID.
     * @return bool
     */
    public function get_profile_questions($outline_id, $full_screen_enabled, $phase)
    {

        $where = '';
        (0 === $full_screen_enabled) ? ($where = ' and tpq_phase=' . $phase) : '';

        $sql = 'select tpq_id, tpq_content, tpq_question'
            . ' from tsl_profile_question'
            . ' where tpq_phase=:phase' . $where;

        return $this->PDO->select($sql, array('phase' => $phase));
    }

    /**
     * Gets the list of Profile.
     *
     * @param $outline_id  Int Id of the outline.
     * @param $user_id     Int User ID
     * @return bool
     */
    public function get_profile_list($outline_id, $user_id)
    {
        $sql = 'select tsl_profile_id, tsl_profile_name'
            . ' from tsl_profile'
            . ' where tsl_profile_user_id=:user_id';

        return $this->PDO->select($sql, array('user_id' => $user_id));

    }

    /**
     * Saves a new profile outline instance.
     *
     * @param $user_id      Int     User ID.
     * @param $project_type String  Type of Project/
     * @param $phase        String  Phase used.
     * @param $content      Text    Content for outline.
     * @param $type         Int     Type of profile
     * @param $client_type  String  Client Type.
     * @param $match        Array   Questions generated from outline.
     * @return bool
     */
    public function save_profile_outline($user_id, $project_type, $phase, $content, $type, $client_type, $match)
    {
        try {
            $this->PDO->begin_transaction();

            // Select the max of version for profile.
            $sql = 'select max(tpo_version) as version'
                . ' from tsl_profile_outline'
                . ' where tpo_user_id=:user_id and tpo_profile_type=:profile_type and'
                . ' tpo_client_type=:client_type and tpo_project_type=:project_type and tpo_profile_phase=:profile_phase';

            $version = intval($this->PDO->select($sql, array('user_id' => $user_id, 'profile_type' => $type,
                    'client_type' => $client_type, 'project_type' => $project_type,
                    'profile_phase' => $phase))[0]->version) + 1;

            // Insert to profile outline.
            $sql = 'insert'
                . ' into tsl_profile_outline'
                . ' (tpo_user_id, tpo_content, tpo_version,'
                . ' tpo_client_type, tpo_project_type, tpo_profile_type, tpo_profile_phase)'
                . ' values(:user_id, :content, :version, :client, :project, :profile_type, :profile_phase)';

            echo $sql;
            exit();

            $outline_id = $this->PDO->insert($sql, array('user_id' => $user_id, 'content' => $content, 'version' => $version,
                'client' => $client_type, 'project' => $project_type, 'profile_type' => $type, 'profile_phase' => $phase));

            // Insert question generated from outline.
            foreach ($match[0] as $value) {
                $sql = 'insert'
                    . ' into tsl_profile_question'
                    . ' (tpq_content, tpq_outline_id)'
                    . ' values(:content, :outline_id)';
                $this->PDO->insert($sql, array('content' => $value, 'outline_id' => $outline_id));
            }
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->commit_transaction();
            return false;
        }
        return true;
    }


    /**
     * Adds a new profile instance.
     *
     * @param $outline_id Int Outline ID.
     * @param $user_id    Int User ID.
     * @return bool
     */
    public function add_profile($outline_id, $user_id)
    {
        $sql = 'insert into tsl_profile(tsl_profile_name, tsl_profile_outline_id, tsl_profile_user_id)'
            . ' values(:name, :outline_id, :user_id)';

        return $this->PDO->insert($sql, array('name' => 'Profile', 'outline_id' => $outline_id, 'user_id' => $user_id));
    }

    /**
     * Deletes a profile Outline from DB.
     *
     * @param $outline_id   Int Outline ID.
     * @return bool
     */
    public function delete_profile_outline($outline_id)
    {
        $sql = 'delete from tsl_profile_outline'
            . ' where tpo_id=:outline_id';

        return $this->PDO->delete($sql, array('outline_id' => $outline_id));
    }

    /**
     * Updates Profile instance Name in the DB.
     *
     * @param $value String Name.
     * @param $id    Int    Profile ID.
     * @return bool
     */
    public function update_profile_name($value, $id)
    {
        $sql = 'update tsl_profile'
            . ' set tsl_profile_name=:name'
            . ' where tsl_profile_id=:id';

        return $this->PDO->update($sql, array('name' => $value, 'id' => $id));
    }

    /**
     * Updates Profile Outline Name.
     *
     * @param $id   Int     Outline ID.
     * @param $name String  Outline Name
     * @return bool
     */
    public function update_profile_outline_name($id, $name)
    {
        $sql = 'update tsl_profile_outline'
            . ' set tpo_name=:name'
            . ' where tpo_id=:id';

        return $this->PDO->update($sql, array('name' => $name, 'id' => $id));
    }

    /**
     * Adds a new profile token that will be sent with the email to client.
     *
     * @param $token        String Token value
     * @param $client_id    Int    Client ID
     * @param $profile_id   Int    Profile ID.
     * @return bool
     */
    public function add_profile_token($token, $client_id, $profile_id)
    {
        $sql = ' insert into tsl_profile_client'
            . ' (tpc_client_id, tpc_token, tpc_profile_id, tpc_used)'
            . ' values(:client_id, :token, :profile_id, 0)';

        return $this->PDO->insert($sql,
            array('client_id' => $client_id, 'token' => $token, 'profile_id' => $profile_id));
    }


    /**
     * Deletes a profile instance.
     *
     * @param $id   Int Profile ID.
     * @return bool
     */
    public function delete_profile($id)
    {
        $sql = 'delete from tsl_profile'
            . ' where tsl_profile_id=:id';

        return $this->PDO->delete($sql, array('id' => $id));
    }

    /**
     * Fetches Update Builder from the Database.
     *
     * @param $update_type      Int  Type of update.
     * @param $update_phase     Int  Phase ID.
     * @param $update_frequency Int  Frequency for updates.
     * @param $client_type      Char Type of client.
     * @param $user_id          Int  User ID.
     * @return bool
     */
    public function get_update_outline($update_type, $update_phase, $update_frequency, $client_type, $user_id)
    {
        $sql = 'select tuo_id, tuo_content, tuo_version '
            . ' from tsl_update_outline'
            . ' where tuo_user_id=:user_id and tuo_client_type=:client_type and tuo_frequency=:frequency '
            . ' and tuo_type=:type and tuo_phase=:phase';

        return $this->PDO->select($sql, array('user_id' => $user_id, 'client_type' => $client_type,
            'frequency' => $update_frequency, 'phase' => $update_phase, 'type' => $update_type));
    }

    /**
     * Fetches the default for updates from the DB.
     *
     * @param $update_type  Int Type of Update.
     * @return bool
     */
    public function get_update_default($update_type, $update_phase)
    {
        $sql = 'select tud_content'
            . ' from tsl_update_default'
            . ' where tud_type=:type and tud_phase=:phase';

        return $this->PDO->select($sql, array('type' => $update_type, 'phase' => $update_phase));
    }

    /**
     * Fetches updates question from the database.
     *
     * @param $outline_id Int Outline Id.
     * @return bool
     */
    public function get_update_questions($outline_id)
    {
        $sql = 'select tuq_content, tuq_id'
            . ' from tsl_update_questions'
            . ' where tuq_outline_id=:outline_id';

        return $this->PDO->select($sql, array('outline_id' => $outline_id));
    }

    /**
     * Fetches default update answer from the database.
     *
     * @return bool
     */
    public function get_default_update_answer()
    {
        $sql = 'select tuda_seq, tuda_content'
            . ' from tsl_update_default_answer';

        return $this->PDO->select($sql, array());
    }

    /**
     * Fetches the list of updates from the database.
     *
     * @param $outline_id Int Outline ID.
     * @return bool
     */
    public function get_update_list($outline_id, $user_id)
    {
        $sql = 'select tu_name, tu_id'
            . ' from tsl_updates'
            . ' where tu_outline_id=:id and tu_user_id=:user_id';

        return $this->PDO->select($sql, array('id' => $outline_id, 'user_id' => $user_id));
    }

    /**
     * Updates Update builder name.
     *
     * @param $id   Int     Update instance ID.
     * @param $name String  Name of the instance.
     * @return bool
     */
    public function update_update_name($id, $name)
    {
        $sql = 'update tsl_updates'
            . ' set tu_name=:name'
            . ' where tu_id=:id';

        return $this->PDO->update($sql, array('name' => $name, 'id' => $id));
    }

    /**
     * Saves a Update Builder Outline
     *
     * @param $user_id      Int     User Id.
     * @param $frequency    Int     Update Frequency.
     * @param $phase        Int     Update Phase.
     * @param $content      Text    Outline Content.
     * @param $type         Int     Update Type
     * @param $client_type  Char    Type of Client
     * @param $match        Array   Array Containing text extracted
     * @return bool
     */
    public function save_update_outline($user_id, $frequency, $phase, $content, $type, $client_type, $match)
    {
        try {
            $this->PDO->begin_transaction();

            // Get max available version of outline.
            $sql = 'select max(tuo_version) as version'
                . ' from tsl_update_outline'
                . ' where tuo_user_id=:user_id and tuo_client_type=:client_type and tuo_frequency=:frequency'
                . ' and tuo_type=:type and tuo_phase=:phase';

            $version = intval($this->PDO->select($sql, array('user_id' => $user_id, 'client_type' => $client_type,
                    'frequency' => $frequency, 'type' => $type, 'phase' => $phase))[0]->version) + 1;

            // Insert outline content into DB.
            $sql = 'insert into '
                . ' tsl_update_outline'
                . ' (tuo_content, tuo_user_id, tuo_version, tuo_frequency,'
                . ' tuo_client_type, tuo_type, tuo_phase)'
                . ' values(:content, :user_id, :version, :frequency, :client_type, :type, :phase)';

            $outline_id = $this->PDO->insert($sql, array('user_id' => $user_id, 'version' => $version, 'content' => $content,
                'frequency' => $frequency, 'client_type' => $client_type, 'type' => $type, 'phase' => $phase));

            foreach ($match[0] as $value) {
                $sql = 'insert'
                    . ' into tsl_update_questions'
                    . ' (tuq_content, tuq_outline_id)'
                    . ' values(:content, :outline_id)';

                $this->PDO->insert($sql, array('content' => $value, 'outline_id' => $outline_id));
            }
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }
        return true;
    }

    /**
     * Adds a new update instance to the DB.
     *
     * @param $outline_id   Int Update Outline ID.
     * @return bool
     */
    public function add_update($outline_id, $user_id)
    {
        $sql = 'insert'
            . ' into tsl_updates'
            . ' (tu_name, tu_outline_id, tu_user_id)'
            . ' values(:name, :outline_id, :user_id)';

        return $this->PDO->insert($sql,
            array('name' => 'Custom Update', 'outline_id' => $outline_id, 'user_id' => $user_id));
    }

    /**
     * Fetches updates answer from the database.
     *
     * @param $update_id Int Update Id.
     * @return bool
     */
    public function get_update_ans($update_id)
    {
        $sql = 'select tua_id, tua_question_id, tua_content'
            . ' from tsl_updates_answer'
            . ' where tua_update_id=:update_id';

        return $this->PDO->select($sql, array('update_id' => $update_id));
    }

    /**
     * Updates the answer for update builder,
     *
     * @param $data         Text Content to be updated.
     * @param $count        Int  Number of fields to be updated.
     * @param $instance_id  Int Update Instance ID.
     * @return bool
     */
    public function update_update($data, $count, $instance_id)
    {
        $sql = 'select tsa_id'
            . ' from tsl_updates_answer'
            . ' where tua_update_id=:instance_id limit 1';
        $ans_id = $this->PDO->select($sql, array('instance_id' => $instance_id));

        // Checks if the question is being already answered by the client.
        if (empty($ans_id)) {
            $i = 1;
            // Inserts every answer in the database.
            while ($i <= $count) {
                $sql = 'insert into'
                    . ' tsl_updates_answer'
                    . ' (tua_question_id, tua_update_id, tua_content)'
                    . ' value(:question_id, :instance_id, :content)';

                $this->PDO->insert($sql, array('question_id' => $i, 'instance_id' => $instance_id,
                    'content' => $data->get('ans' . $i, '', 'RAW')));
                $i++;
            }
        } else {
            $i = 1;
            // Updates every answer in the database.
            while ($i <= $count) {
                $sql = 'update tsl_updates_answer'
                    . ' set tua_content=:content'
                    . ' where tua_update_id=:instance_id and tua_question_id=:question_id';

                $this->PDO->update($sql, array('question_id' => $i, 'instance_id' => $instance_id,
                    'content' => $data->get('ans' . $i, '', 'RAW')));
                $i++;
            }
        }

        return true;
    }

    /**
     * Deletes an update outline.
     *
     * @param $outline_id   Int Outline ID.
     * @return bool
     */
    public function delete_update_outline($outline_id)
    {
        $sql = 'delete from tsl_update_outline'
            . ' where tuo_id=:id';

        return $this->PDO->delete($sql, array('id' => $outline_id));
    }

    /**
     * Deletes an update instance from the database.
     *
     * @param $update_id Int Update Instance ID.
     * @return bool
     */
    public function delete_update($update_id)
    {
        $sql = 'delete'
            . ' from tsl_updates'
            . ' where tu_id=:update_id';

        return $this->PDO->delete($sql, array('update_id' => $update_id));
    }

    public function update_dashboard_dropdown($name, $key, $id, $val, $table)
    {
        try {
            $this->PDO->begin_transaction();

            $sql = 'update ' . $table
                . ' set ' . $key . '_selected=0'
                . ' where ' . $key . '_tsl_id=:id';

            $this->PDO->update($sql, array('id' => $id));

            $sql = 'update ' . $table
                . ' set ' . $key . '_selected=1'
                . ' where ' . $key . '_id=:id';

            $this->PDO->update($sql, array('id' => $val));

            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }
        return true;
    }

    /**
     * Fetches Phases for a component.
     *
     * @param $outline_id Int    Outline ID.
     * @param $type       String Type of component.
     * @return bool
     */
    public function get_phase($outline_id, $type)
    {
        $sql = 'select cp_id, cp_content, cp_name, cp_number'
            . ' from component_phase'
            . ' where cp_component=:component'
            . ' and cp_outline_id=:outline_id';

        return $this->PDO->select($sql, array('component' => $type, 'outline_id' => $outline_id));
    }

    /**
     * Creates a record for roleplay in the database.
     *
     * @param $name     String Name of Roleplay
     * @param $user_id  Int User ID.
     * @return bool
     */
    public function add_recording($name, $user_id)
    {
        $sql = ' insert into tsl_script_recording'
            . ' (tsr_user_id, tsr_name)'
            . ' values(:user_id, :name)';

        return $this->PDO->insert($sql, array('user_id' => $user_id, 'name' => $name));
    }

    /**
     * Fetches roleplay for a user.
     *
     * @param $user_id
     * @return bool
     */
    public function get_recording($user_id)
    {
        $sql = 'select tsr_id, tsr_name, tsr_given_name'
            . ' from tsl_script_recording'
            . ' where tsr_user_id=:user_id';

        return $this->PDO->select($sql, array('user_id' => $user_id));
    }

    public function delete_recording($id)
    {
        $sql = 'delete'
            . ' from tsl_script_recording'
            . ' where tsr_id=:id';

        return $this->PDO->delete($sql, array('id' => $id));

    }

    /**
     * Updates Recording names.
     *
     * @param $id
     * @param $name
     * @return bool
     */
    public function update_recording_name($id, $name)
    {
        $sql = 'update tsl_script_recording'
            . ' set tsr_given_name=:name'
            . ' where tsr_id=:id';

        return $this->PDO->update($sql, array('id' => $id, 'name' => $name));
    }

    /**
     *Saves a message sequence as spoken
     *
     * @param $client_id        Int Client ID.
     * @param $seq_id           Int Component Sequence ID.
     * @param $component_id     Int Component ID.
     * @param $component_type   String Component Type.
     * @return bool
     */
    public function save_spoken_message($client_id, $seq_id, $component_id, $component_type)
    {
        $sql = 'insert'
            . ' into tsl_spoken_message'
            . ' (tsm_sequence_id, tsm_component_id, tsm_component_type, tsm_client_id)'
            . ' values(:seq_id, :component_id, :component_type, :client_id)'
            . ' on duplicate key update tsm_sequence_id=:seq_id, tsm_component_id=:component_id';

        return $this->PDO->insert($sql, array('seq_id' => $seq_id,
            'component_id' => $component_id, 'component_type' => $component_type, 'client_id' => $client_id));

    }

    /**
     * @param $tsl_id Int TSL ID.
     * @return \Exception
     */
    public function reload_tsl($tsl_id, $user_id, $data, $info, $product_id)
    {
        try {
            $this->PDO->begin_transaction();

            $query = '';
            foreach ($info as $key => $ind_info) {
                if (!empty($ind_info)) {
                    if ($key === 'location') {
                        $query .= ' and (hca_city=';
                        $query .= implode(' or hca_city= ', $ind_info);

                        $query .= ' or hca_state=';
                        $query .= implode(' or hca_state= ', $ind_info);

                        $query .= ' or hca_zip=';
                        $query .= implode(' or hca_zip= ', $ind_info);
                        $query .= ')';
                    } else {
                        $query .= " and (hca_$key=";
                        $query .= implode(" or hca_$key= ", $ind_info);
                        $query .= ')';
                    }
                }
            }


            // Check if criteria is set.
            if (!empty($data['criteria'])) {
                $query .= ' and (hca_criteria_used=';
                $query .= implode(" or hca_criteria_used= ", $data['criteria']);
                $query .= ')';
            }

            // Check if location object is empty.
            if (!empty($data['area'])) {
                $query .= ' and (hca_location=';
                $query .= implode(' or hca_location= ', $data['area']);
                $query .= ')';
            }

            $query .= ' and hca_product_id=' . $product_id;

            $ret['client_fetched'] = $this->fetch_clients($tsl_id, $user_id, $query);

            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $ret = $e;
            $this->PDO->rollback_transaction();
        }
        return $ret;
    }

    /**
     * Fetch list of assets already in use.
     *
     * @param $audience
     * @param $criteria
     * @param $desc
     * @param $cond
     * @param $user_id
     * @param $product_id
     * @return bool
     */
    function fetch_assets_used($audience, $criteria, $desc, $cond, $user_id, $product_id)
    {

        $query = '';

        // Check if audience is set.
        if (isset($audience)) {
            $query = ' and (hca_location=';
            $query .= implode(' or hca_location= ', $audience);
            $query .= ')';
        }

        // Check if criteria is set.
        if (isset($criteria)) {
            $query .= ' and (hca_criteria_used=';
            $query .= implode(" or hca_criteria_used= ", $criteria);
            $query .= ')';
        }

        if (isset($desc)) {
            $query .= ' and (hca_desc=';
            $query .= implode(" or hca_desc= ", $desc);
            $query .= ')';
        }

        if (isset($cond)) {
            $query .= ' and (hca_cond=';
            $query .= implode(" or hca_cond= ", $cond);
            $query .= ')';
        }


        $sql = 'select hca_id, hca_name, hc_name, tsl_name, tc_id'
            . ' from homebase_client_asset'
            . ' join homebase_client'
            . ' on hca_client_id=hc_id'
            . ' join tsl_client'
            . ' on tc_asset_id=hca_id'
            . ' join tsl'
            . ' on tc_tsl_id=tsl_id'
            . ' where tsl_user_id=:user_id and hca_product_id=:product_id' . $query;

        return $this->PDO->select($sql, array('user_id' => $user_id, 'product_id' => $product_id));
    }

    function update_promo_focus($id, $val)
    {
        $sql = 'update tsl'
            . ' set tsl_description=:val'
            . ' where tsl_id=:id';

        return $this->PDO->update($sql, array('val' => $val, 'id' => $id));
    }


    /**
     * Fetches email content.
     *
     * @param $client_id Int Client ID
     * @param $email_id  Int Email ID
     * @return bool
     */
    public function propagate_email_content($client_id, $email_id)
    {

        try {
            $this->PDO->begin_transaction();
            $sql = ' select tea_content, tea_question_number'
                . ' from tsl_email_answer'
                . ' where tea_email_id=:email_id and tea_client_id=:client_id'
                . ' order by tea_question_number';

            $contents = $this->PDO->select($sql, array('email_id' => $email_id, 'client_id' => $client_id));

            // Propagate Content.
            foreach ($contents as $content) {
                $sql = 'update tsl_email_answer'
                    . ' set tea_content=:content'
                    . ' where tea_email_id=:email_id and tea_question_number=:ques_no';

                $updated_content = $this->PDO->update($sql, array('email_id' => $email_id,
                    'content' => $content->tea_content, 'ques_no' => $content->tea_question_number));
            }

            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return $e;
        }
        return true;
    }

    public function get_notices_outline($user_id)
    {
        $sql = 'select tno_content, tno_id'
            . ' from tsl_notice_outline'
            . ' where tno_user_id=:user_id or tno_user_id=0';

        return $this->PDO->select($sql, array('user_id' => $user_id));
    }

    public function add_notice($client_id, $user_id, $name)
    {
        $sql = 'insert into tsl_notice'
            . '(tn_name, tn_client_id, tn_user_id)'
            . ' values(:name, :client_id, :user_id)';

        return $this->PDO->insert($sql, array('name' => $name, 'client_id' => $client_id, 'user_id' => $user_id));
    }

    public function get_notice_instance($client_id, $user_id)
    {
        $sql = 'select tn_id, tn_name'
            . ' from tsl_notice'
            . ' where tn_client_id=:client_id';

        return $this->PDO->select($sql, array('client_id' => $client_id));
    }

    public function save_notice_content($notice_id, $data, $count, $client_id)
    {
        try {
            $this->PDO->begin_transaction();

            $sql = 'select tna_id'
                . ' from tsl_notice_answer'
                . ' where tna_client_id=:client_id and tna_notice_id=:notice_id';

            $ans_id = $this->PDO->select($sql, array('client_id' => $client_id, 'notice_id' => $notice_id));

            // Checks if the question is being already answered by the client.
            if (empty($ans_id)) {
                $i = 1;

                // Inserts every answer in the database.
                while ($i <= $count) {
                    $sql = 'insert'
                        . ' into tsl_notice_answer'
                        . ' (tna_question_number, tna_client_id, tna_content, tna_notice_id)'
                        . ' value(:question_no, :client_id, :content, :notice_id)';

                    $this->PDO->insert($sql, array('question_no' => $i, 'client_id' => $client_id,
                        'content' => $data->get('ans' . $i, '', 'RAW'), 'notice_id' => $notice_id));
                    $i++;
                }
            } else {
                $i = 1;

                // Updates every answer in the database.
                while ($i <= $count) {
                    $sql = 'update tsl_notice_answer'
                        . ' set tna_content=:content'
                        . ' where tna_client_id=:client_id'
                        . ' and tna_question_number=:question_no and tna_notice_id=:notice_id';

                    $this->PDO->update($sql, array('question_no' => $i, 'client_id' => $client_id,
                        'content' => $data->get('ans' . $i, '', 'RAW'), 'notice_id' => $notice_id));
                    $i++;
                }
            }
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }
        return true;
    }

    public function get_notice_answer($client_id, $notice_id)
    {
        $sql = 'select tna_id, tna_content, tna_question_number'
            . ' from tsl_notice_answer'
            . ' where tna_client_id=:client_id and tna_notice_id=:notice_id';

        return $this->PDO->select($sql, array('client_id' => $client_id, 'notice_id' => $notice_id));
    }

    /**
     * Fetches call status from DB.
     *
     * @param $user_id
     * @return bool
     */
    public function get_call_status($user_id)
    {
        $sql = 'select tcs_id, tcs_name, tcs_user_id'
            . ' from tsl_call_status'
            . ' where tcs_user_id=0 or tcs_user_id=:user_id';

        return $this->PDO->select($sql, array('user_id' => $user_id));
    }

    public function save_call_status($user_id, $call_status)
    {
        $sql = 'insert into tsl_call_status'
            . ' (tcs_user_id, tcs_name)'
            . ' values(:user_id, :name)';

        return $this->PDO->insert($sql, array('user_id' => $user_id, 'name' => $call_status));
    }

    public function update_call_status($id, $value)
    {
        $sql = 'update tsl_call_status'
            . ' set tcs_name=:name'
            . ' where tcs_id=:id';

        return $this->PDO->update($sql, array('name' => $value, 'id' => $id));
    }

    public function delete_call_status($id)
    {
        $sql = 'delete from tsl_call_status'
            . ' where tcs_id=:id';

        return $this->PDO->delete($sql, array('id' => $id));
    }

    public function save_message_outline($component, $content, $user_id)
    {
        $sql = 'insert into tsl_message_outline'
            . ' (tmo_user_id, tmo_content, tmo_name)'
            . ' values(:user_id, :content, :name)';

        return $this->PDO->insert($sql, array('user_id' => $user_id, 'content' => $content, 'name' => 'Custom Outline'));
    }

    public function get_client_phone($client_id)
    {
        try {

            $sql = 'select hcp_id, hcp_type, hcp_hc_id, hcp_phone'
                . ' from homebase_client_phone'
                . ' where hcp_hc_id=:client_id';

            $ret['additional'] = $this->PDO->select($sql, array('client_id' => $client_id));

            $sql = 'select hc_main_phone'
                . ' from homebase_client'
                . ' where hc_id=:client_id';

            $ret['main_phone'] = $this->PDO->select($sql, array('client_id' => $client_id));

            $sql = 'select hc_second_phone'
                . ' from homebase_client'
                . ' where hc_id=:client_id';

            $ret['second_phone'] = $this->PDO->select($sql, array('client_id' => $client_id));

            $sql = 'select hc_mobile_phone'
                . ' from homebase_client'
                . ' where hc_id=:client_id';

            $ret['mobile_phone'] = $this->PDO->select($sql, array('client_id' => $client_id));
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return $e;
        }

        return $ret;
    }

    public function save_client_phone($client_id, $type, $number)
    {
        $sql = 'insert into homebase_client_phone'
            . ' (hcp_hc_id, hcp_phone, hcp_type)'
            . ' values(:hc_id, :phone, :type)';

        return $this->PDO->insert($sql, array('hc_id' => $client_id, 'phone' => $number, 'type' => $type));
    }

    function update_client_phone($col, $val, $id)
    {
        $sql = 'update homebase_client'
            . ' set ' . $col . '=:val'
            . ' where hc_id=:id';

        return $this->PDO->update($sql, array('val' => $val, 'id' => $id));
    }

    function update_additional_phone($col, $val, $id)
    {
        $sql = 'update homebase_client_phone'
            . ' set ' . $col . '=:val'
            . ' where hcp_id=:id';

        return $this->PDO->update($sql, array('val' => $val, 'id' => $id));
    }

    function delete_additional_phone($id)
    {
        $sql = 'delete from homebase_client_phone'
            . ' where hcp_id=:id';

        return $this->PDO->delete($sql, array('id' => $id));
    }

    function begin_call_session($tsl_id, $day, $day_num, $time, $date, $month, $seq, $duration)
    {
        $sql = 'insert into tsl_sam'
            . ' (ts_tsl_id, ts_day, ts_day_num, ts_start_time, ts_date, ts_month, ts_sequence, ts_active, ts_duration)'
            . ' values(:tsl_id, :day, :day_num, :time, :date, :month, :seq, :active, :duration)';

        return $this->PDO->insert($sql, array('tsl_id' => $tsl_id, 'day' => $day, 'day_num' => $day_num,
            'time' => $time, 'date' => $date, 'month' => $month, 'seq' => $seq, 'active' => 1, 'duration' => $duration));
    }

    function end_call_session($session_id, $time)
    {
        $sql = 'update tsl_sam'
            . ' set ts_active=0, ts_end_time=:time'
            . ' where ts_id=:id';

        return $this->PDO->update($sql, array('time' => $time, 'id' => $session_id));
    }

    function update_call_session($sam_id, $col_name)
    {
        $sql = ' update tsl_sam'
            . ' set ' . $col_name . '=' . $col_name . ' + 1'
            . ' where ts_id=:id';

        return $this->PDO->update($sql, array('id' => $sam_id));
    }

    function pause_restart_call_session($session_id, $val)
    {
        $sql = 'update tsl_sam'
            . ' set ts_state=:state'
            . ' where ts_id=:id';

        return $this->PDO->update($sql, array('state' => $val, 'id' => $session_id));
    }

    function get_active_sam($tsl_id)
    {
        $sql = 'select ts_id, ts_start_time, ts_end_time, ts_duration, ts_state'
            . ' from tsl_sam'
            . ' where ts_tsl_id=:tsl_id and ts_active=1';

        return $this->PDO->select($sql, array('tsl_id' => $tsl_id));
    }

    /**
     * Fetch SAM data.
     *
     * @param $tsl_id Int TSL Id.
     * @return array
     */
    function get_sam_data($tsl_id)
    {
        $sql = 'select ts_day, ts_day_num, ts_start_time, ts_date, ts_month,'
            . ' ts_dials, ts_contact, ts_conv, ts_meet, ts_sequence, ts_end_time'
            . ' from tsl_sam'
            . ' where ts_tsl_id=:id and ts_date_created > DATE_SUB(NOW(), INTERVAL 1 WEEK)';

        $ts_data = $this->PDO->select($sql, array('id' => $tsl_id));
        $ret = [];
        foreach ($ts_data as $data) {
            $ret[$data->ts_day][] = $data;
        }

        return $ret;
    }

    function get_tsl_dropdown($user_id, $active_product)
    {
        $sql = 'select ul_name, ul_id'
            . ' from user_location'
            . ' where ul_user_id=:user_id and ul_product_id=:product';

        $ret['location'] = $this->PDO->select($sql, array('user_id' => $user_id, 'product' => $active_product));
    }

    function get_asset_criteria($asset_id)
    {
        $this->PDO->begin_transaction();

        try {

            $sql = 'select ul_name, uc_name'
                . ' from homebase_client_asset'
                . ' join user_location'
                . ' on ul_id=hca_location'
                . ' join user_criteria'
                . ' on uc_id=hca_criteria_used'
                . ' where hca_id=:id';

            $ret['location_criteria'] = $this->PDO->select($sql, array('id' => $asset_id));

            $sql = 'select hai_value'
                . ' from homebase_asset_info'
                . ' join homebase_client_asset'
                . ' on hca_desc=hai_id'
                . ' where hai_type=\'desc\' and hca_id=:id';

            $ret['desc'] = $this->PDO->select($sql, array('id' => $asset_id));

            $sql = 'select hai_value'
                . ' from homebase_asset_info'
                . ' join homebase_client_asset'
                . ' on hca_cond=hai_id'
                . ' where hai_type=\'cond\' and hca_id=:id';

            $ret['cond'] = $this->PDO->select($sql, array('id' => $asset_id));

            $sql = 'select hai_value'
                . ' from homebase_client_asset'
                . ' join homebase_asset_info'
                . ' on hca_city=hai_id'
                . ' where hai_type=\'city\' and hca_id=:id';

            $ret['city'] = $this->PDO->select($sql, array('id' => $asset_id));

            $sql = 'select hai_value'
                . ' from homebase_client_asset'
                . ' join homebase_asset_info'
                . ' on hca_state=hai_id'
                . ' where hai_type=\'state\' and hca_id=:id';

            $ret['state'] = $this->PDO->select($sql, array('id' => $asset_id));

            $sql = 'select hai_value'
                . ' from homebase_client_asset'
                . ' join homebase_asset_info'
                . ' on hca_zip=hai_id'
                . ' where hai_type=\'zip\' and hca_id=:id';

            $ret['zip'] = $this->PDO->select($sql, array('id' => $asset_id));

            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return $e;
        }

        return $ret;
    }

    function update_archive_select($id, $val)
    {
        $sql = 'update component_archive'
            . ' set ca_response=:response'
            . ' where ca_id=:id';

        return $this->PDO->update($sql, array('response' => $val, 'id' => $id));
    }

    function delete_archive($id)
    {
        $sql = 'delete from component_archive'
            . ' where ca_id=:id';

        return $this->PDO->delete($sql, array('id' => $id));
    }

    function delete_client_action($id, $seq)
    {
        $sql = 'delete from tsl_actions'
            . ' where ta_client_id=:id and ta_seq=:seq';

        return $this->PDO->delete($sql, array('id' => $id, 'seq' => $seq));
    }

    /**
     * Removes a client from TSL  instance.
     *
     * @param $tc_id        Int TSL client ID.
     * @param $asset_id     Int Asset Id.
     * @return mixed
     */
    function remove_tsl_client($tc_id, $asset_id)
    {
        $sql = 'delete from tsl_client'
            . ' where tc_id=:tc_id';

        $ret['client_removed'] = $this->PDO->delete($sql, array('tc_id' => $tc_id));

        $sql = 'insert into'
            . ' homebase_asset_status'
            . ' (has_asset_id, has_status, has_md, has_datetime)'
            . ' values(:asset_id, :status, month(now()), now())';

        $ret['asset_status_updated'] = $this->PDO->insert($sql, array('asset_id' => $asset_id, 'status' => ''));
        return $ret;
    }

    /**
     * Delete Past calls from TSl instance.
     *
     * @param $tsl_id   Int TSL instance ID.
     * @return bool
     */
    function delete_past_call($tsl_id)
    {
        try {
            $sql = 'delete tsl_about_client'
                . ' from tsl_about_client'
                . ' join tsl_client'
                . ' on tc_id=tac_tc_id'
                . ' where tc_tsl_id=:tsl_id and tac_sequence=2';

            $ret = $this->PDO->delete($sql, array('tsl_id' => $tsl_id));
        } catch (\Exception $e) {
            return $e;
        }
        return $ret;
    }

    /**
     * Update scoreboard data.
     *
     * @param $user_id  Int    User Id.
     * @param $type     String Type fo score.
     * @return bool|\Exception
     */
    function update_scoreboard($user_id, $type)
    {
        try {
            $sql = 'insert into user_call_scoreboard'
                . ' (ucs_type, ucs_user_id, ucs_when)'
                . ' values(:type, :user_id, now())';

            $ret = $this->PDO->insert($sql, array('type' => $type, 'user_id' => $user_id));

        } catch (\Exception $e) {
            return $e;
        }
        return $ret;
    }


    /**
     * Fetch Call scoreboard data
     *
     * @param $user_id  Int  User Id.
     * @param $when     Date Date time for scoreboard data.
     * @return bool
     */
    public function get_call_scoreboard($user_id, $when)
    {
        $sql = 'select ucs_type'
            . ' from user_call_scoreboard'
            . ' where ucs_user_id=:user_id and DATE(ucs_when)=DATE(:when)';

        return $this->PDO->select($sql, array('user_id' => $user_id, 'when' => $when));
    }

    public function get_confirm_data($client_id)
    {
        $sql = 'select tsl_confirm_id, tsl_confirm_name, tsl_confirm_site,'
            . ' tsl_confirm_note, tsl_confirm_topic, tsl_confirm_when, tsl_confirm_seq'
            . ' from tsl_confirm'
            . ' where tsl_confirm_client=:client_id';

        return $this->PDO->select($sql, array('client_id' => $client_id));
    }

    public function update_confirm_data($client_id, $col, $val, $seq)
    {
        $sql = ' insert into tsl_confirm'
            . ' (tsl_confirm_client, tsl_confirm_seq, ' . $col . ')'
            . ' value(:client, :seq, :val)'
            . ' on duplicate key'
            . ' update ' . $col . '=:val';

        return $this->PDO->insert($sql, array('client' => $client_id, 'seq' => $seq, 'val' => $val));
    }

    public function delete_client_confirm($client_id, $seq)
    {
        $sql = 'delete from tsl_confirm'
            . ' where tsl_confirm_client=:client and tsl_confirm_seq=:seq';

        return $this->PDO->delete($sql, array('client' => $client_id, 'seq' => $seq));
    }

    /**
     * Fetches the max sequence for notes, action and confirms.
     *
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public function get_max_seq($id)
    {
        try {
            $sql = 'select max(ta_seq) as seq'
                . ' from tsl_actions'
                . ' where ta_client_id=:client';

            $ret['action'] = $this->PDO->select($sql, array('client' => $id));

            $sql = 'select max(tn_seq) as seq'
                . ' from tsl_notes'
                . ' where tn_client_id=:client';
            $ret['notes'] = $this->PDO->select($sql, array('client' => $id));

            $sql = 'select max(tsl_confirm_seq) as seq'
                . ' from tsl_confirm'
                . ' where tsl_confirm_client=:client';

            $ret['confirm'] = $this->PDO->select($sql, array('client' => $id));
        } catch (\Exception $e) {
            throw $e;
        }
        return $ret;
    }

    /**
     * Update Active TSL.
     *
     * @param $id       Int TSL Id.
     * @param $user_id  Int User Id.
     * @return mixed
     */
    public function update_active_tsl($id, $user_id)
    {
        try {
            $sql = 'insert into user_active_tsl'
                . '(uat_tsl_id, uat_user_id)'
                . ' values(:tsl_id, :user_id)'
                . ' on duplicate key'
                . ' update uat_to_delete=1';

            $ret['active_tsl_updated'] = $this->PDO->insert($sql, array('tsl_id' => $id, 'user_id' => $user_id));

            $sql = 'delete from user_active_tsl'
                . ' where uat_to_delete=1';

            $ret['prev_active_deleted'] = $this->PDO->delete($sql, array());
        } catch (\Exception $e) {
            echo $e;
        }
        return $ret;
    }

    public function update_profile_questions($question_id, $input)
    {
        try {
            foreach ($question_id as $id) {
                $sql = 'update tsl_profile_question'
                    . ' set tpq_question=:question'
                    . ' where tpq_id=:id';

                $this->PDO->update($sql, array('id' => $id, 'question' => $input->get('ques_' . $id, '', 'RAW')));
            }
        } catch (\Exception $e) {
            echo $e;
        }
        return true;
    }

    public function get_profile_data($id, $section)
    {
        $sql = 'select tsl_profile_ans_content as answer, tsl_profile_ans_ques_id as ques, tpq_content'
            . ' from tsl_profile_answer'
            . ' join tsl_profile_question'
            . ' on tsl_profile_ans_ques_id=tpq_id'
            . ' where tsl_profile_ans_profile_id=:id and tpq_phase=:phase';

        return $this->PDO->select($sql, array('id' => $id, 'phase' => $section));
    }

    public function move_to_identified($tac_id, $tc_id, $user_id)
    {
        try {
            $this->PDO->begin_transaction();

            $ret = [];
            $sql = 'select tac_result, tc_asset_id, tc_type'
                . ' from tsl_client join tsl_about_client'
                . ' on tc_id=tac_tc_id'
                . ' where tac_id=:tac_id';

            $tac_info = $this->PDO->select($sql, array('tac_id' => $tac_id));

            $sql = 'insert'
                . ' into agent_deal_identified'
                . ' (adi_user_id, adi_asset_id)'
                . ' values(:user_id, :asset_id)';

            $ret['adi_id'] = $this->PDO->insert($sql, array('user_id' => $user_id, 'asset_id' => ($tac_info[0]->tc_asset_id)));

            $sql = 'insert'
                . ' into homebase_asset_status'
                . ' (has_status, has_md, has_asset_id, has_datetime, has_type)'
                . ' values (:status, month(now()), :asset_id, now(), :type)';

            $ret['deal_moved'] = $this->PDO->insert($sql, array('status' => 'identified',
                'asset_id' => $tac_info[0]->tc_asset_id, 'type' => $tac_info[0]->tc_type));

            $sql = 'delete'
                . ' from tsl_client'
                . ' where tc_id= :tc_id';

            $ret['instance_removed'] = $this->PDO->delete($sql, array('tc_id' => $tc_id));
            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return $e;
        }
        return $ret;
    }
}