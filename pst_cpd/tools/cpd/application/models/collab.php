<?php
/**
 * Collaboration Model.
 *
 * General Collaboration model that get/set data from database.
 *
 * @author Sumit K (sumitk@mindfiresolutions.com)
 *
 */

namespace Model;

use function isEmpty;
use function print_r;
use function wp_get_current_user;

/**
 * CPD model class
 */
class Collab
{

    private $PDO;
    private $common_model;

    /**
     * Initialises the PDO and model instance.
     */
    public function __construct()
    {
        $this->PDO = \Model\PDO_model::instance('Tools');
        $this->common_model = new Common;
    }

    public function get_collab_details($user_id, $id)
    {
        try {

            $this->PDO->begin_transaction();

            if (!empty($id)) {
                $sql = 'select sc_sender, collab_format_name'
                    . ' from script_collab'
                    . ' join collab_format'
                    . ' on collab_format_id=sc_action'
                    . ' where sc_id=:id';

                $data['collab_data'] = $this->PDO->select($sql, array('id' => $id));
            }

            // Fetch Collaboration Details
            $sql = 'select collab_id, collab_sender_id, collab_recipient_name, collab_script_name,'
                . ' collab_data, collab_created_at, collab_format_name'
                . ' from collab'
                . ' join collab_format on collab_format.collab_format_id=collab.collab_format_id'
                . ' join collab_script on collab.collab_script_id=collab_script.collab_script_id'
                . ' join collab_recipient on collab.collab_recipient_id=collab_recipient.collab_recipient_id'
                . ' where collab_user_id=:user_id';

            $data['collab_details'] = $this->PDO->select($sql, array('user_id' => $user_id));

            // Fetch Collaboration Format details.
            $sql = 'select collab_format_name, collab_format_id'
                . ' from collab_format'
                . ' where collab_format_user_id=:user_id or collab_format_default=1';

            $data['format'] = $this->PDO->select($sql, array('user_id' => $user_id));

            // Fetch Collaboration Script details.
            $sql = 'select collab_script_name, collab_script_id'
                . ' from collab_script'
                . ' where collab_script_user_id=:user_id or collab_script_default=1';

            $data['script'] = $this->PDO->select($sql, array('user_id' => $user_id));

            // Fetch Collaboration Recipient details.
            $sql = 'select collab_recipient_name, collab_recipient_id'
                . ' from collab_recipient'
                . ' where collab_recipient_user_id=:user_id';

            $data['recipient'] = $this->PDO->select($sql, array('user_id' => $user_id));

            0 !== $user_id ? $data['user'] = wp_get_current_user()->data->user_nicename : '';

            $this->PDO->commit_transaction();
        } catch (\Exception $e) {
            $this->PDO->rollback_transaction();
            return false;
        }
        return $data;
    }

    /**
     * Saves Collaboration data,
     *
     * @param $instance_id Int    Tool Instance ID.
     * @param $ques_id     Int    Ques number.
     * @param $type        String Type of component.
     * @param $order       Int    Order for Collaboration
     * @param $content     String Content for collaboration box.
     * @return bool
     */
    public function save_collab_data($instance_id, $ques_id, $type, $order, $content, $sender,
                                     $recipient, $action, $date, $time, $category, $collaboration_id)
    {
        $sql = ' insert'
            . ' into script_collab'
            . ' (sc_tool_id, sc_ques_id, sc_order, sc_type,'
            . ' sc_content, sc_sender, sc_recipient, sc_action, sc_time, sc_date, sc_category, sc_collaboration_id)'
            . ' values(:tool_id, :ques_id, :order, :type, :content, :sender, :recipient, :action, :time,'
            . ' :date, :category, :collaboration_id)';

        return $this->PDO->insert($sql, array('tool_id' => $instance_id, 'ques_id' => $ques_id, 'order' => $order,
            'type' => $type, 'content' => $content, 'sender' => $sender, 'recipient' => $recipient, 'action' => $action,
            'time' => $time, 'date' => $date, 'category' => $category, 'collaboration_id' => $collaboration_id));
    }

    /**
     * Fetches and returns collaboration data for a particular component in a tool.
     *
     * @param $id   Int     Tool ID.
     * @param $type String  Type of component
     * @return bool
     */
    public function get_collab_data($id, $type, $category, $recipient, $action)
    {
        $order_by = ' sc_order desc';
        ('scr' === $category) ? $order_by = ' sc_date, sc_time desc ' : '';

        $where = '';
        ('' != $recipient && '0' !== $recipient && 'scr' !== $category) ?
            $where = ' and sc_recipient=' . $recipient : '';

        ('' != $action && '0' !== $action) ? $where .= ' and sc_action=' . $action : '';

        $sql = 'select sc_id, sc_content, sc_order, sc_ques_id, sc_type, sc_collaboration_id, '
            . ' sc_sender, collab_recipient_name as sc_recipient, collab_format_message as message, '
            . ' collab_format_name as sc_action, sc_date, sc_time, sc_tool_id'
            . ' from script_collab'
            . ' left join collab_recipient'
            . ' on collab_recipient_id=script_collab.sc_recipient'
            . ' left join collab_format'
            . ' on collab_format_id=script_collab.sc_action'
            . ' where sc_type=:type and sc_tool_id=:tool_id and sc_category=:category' . $where
            . ' order by sc_order desc';

        return $this->PDO->select($sql, array('type' => $type, 'tool_id' => $id, 'category' => $category));
    }

    /**
     * Updates Data for Collaboration table.
     *
     * @param $id       Int  Collaboration ID.
     * @param $content  Text Content to be updated.
     * @return bool
     */
    public function update_collab_data($id, $content)
    {
        $sql = 'update script_collab'
            . ' set sc_content=:content'
            . ' where sc_id=:id';

        return $this->PDO->update($sql, array('id' => $id, 'content' => $content));
    }

    /**
     * @param $token     Int Token for collaboration
     * @param $tool_id   Int Tool Id
     * @param $collab_id Int Collaboration ID.
     * @return bool
     */
    public function validate_token($token, $tool_id, $collab_id, $outline_id, $recipient)
    {
        $sql = 'select sct_id'
            . ' from script_collab_token'
            . ' where sct_tool_id=:tool_id and sct_token=:token and sct_outline_id=:outline_id'
            . ' and sct_collab_id=:collab_id and sct_used=:used_val and sct_recipient=:recipient';

        return $this->PDO->select($sql,
            array('tool_id' => $tool_id, 'token' => $token, 'collab_id' => $collab_id,
                'used_val' => 0, 'outline_id' => $outline_id, 'recipient' => $recipient));
    }

    public function add_collab_token($id, $tool_id, $token, $outline_id, $recipient, $type, $user_email)
    {
        $sql = 'insert into script_collab_token'
            . ' (sct_token, sct_tool_id, sct_collab_id, sct_outline_id, sct_recipient, sct_user_email)'
            . ' values(:token, :tool_id, :collab_id, :outline_id, :recipient, :user_email)';

        return $this->PDO->insert($sql, array('token' => $token, 'tool_id' => $tool_id, 'collab_id' => $id,
            'outline_id' => $outline_id, 'recipient' => $recipient, 'user_email' => $user_email));
    }

    /**
     * Fetches Script outline from database.
     *
     * @param $id   Int Script Outline ID.
     * @return bool
     */
    public function get_script_outline($id)
    {
        $sql = 'select tsl_script_content'
            . ' from tsl_script_default'
            . ' where tsl_script_default_id=:id';

        return $this->PDO->select($sql, array('id' => $id));
    }

    public function get_page_outline($id)
    {
        $sql = 'select tpo_content as tsl_script_content'
            . ' from tsl_promo_outline'
            . ' where tpo_id=:id';

        return $this->PDO->select($sql, array('id' => $id));
    }
    public function get_message_outline($id)
    {
        $sql = 'select tpo_content as tsl_script_content'
            . ' from tsl_promo_outline'
            . ' where tpo_id=:id';

        $sql = 'select tmo_content as tsl_script_content'
            . ' from tsl_message_outline'
            . ' where tmo_id=:id';

        return $this->PDO->select($sql, array('id' => $id));
    }

    public function get_collaborator_details($collab_id, $user_id)
    {
        $sql = 'select collab_format_name, collab_format_id'
            . ' from collab_format'
            . ' where collab_format_user_id=:user_id or collab_format_default=1';

        $data['format'] = $this->PDO->select($sql, array('user_id' => $user_id));

        $data['user'] = wp_get_current_user()->data->user_nicename;

        $sql = 'select collab_recipient_name as sc_recipient'
            . ' from script_collab'
            . ' join collab_recipient'
            . ' on collab_recipient_id=script_collab.sc_recipient'
            . ' where sc_id=:id';

        $data['recipient'] = $this->PDO->select($sql, array('id' => $collab_id));

        return $data;
    }

    /**
     * Fetches Script answers
     *
     * @param $instance_id  INT Script Instance ID.
     * @param $type         String Type of script.
     * @return bool
     */
    public function get_script_ans($instance_id, $type)
    {
        $sql = 'select tsa_id, tsa_content as answer, tsa_question_id as question'
            . ' from tsl_script_answer'
            . ' where tsa_instance_id=:instance_id'
            . ' and tsa_type=:type';

        return $this->PDO->select($sql, array('instance_id' => $instance_id, 'type' => $type));
    }

    /**
     * Adds a new recipient.
     *
     * @param $name     String Recipient Name.
     * @param $user_id  Int    User ID.
     * @return bool
     */
    public function save_recipient($name, $user_id)
    {
        $sql = 'insert'
            . ' into collab_recipient'
            . ' (collab_recipient_name, collab_recipient_user_id)'
            . ' values(:name, :user_id)';

        return $this->PDO->insert($sql, array('name' => $name, 'user_id' => $user_id));
    }

    public function get_component_info($collab_id)
    {
        $sql = 'select sc_type'
            . ' from script_collab'
            . ' where sc_id=:collab_id';

        return $this->PDO->select($sql, array('collab_id' => $collab_id));
    }

    function get_collab_info($id, $recipient)
    {
        $sql = 'select collab_id, collab_sender_name, collab_cpd_id, collab_recipient_id,'
            . ' collab_script_id, collab_data, collab_type, collab_collab_id,'
            . ' collab_order, collab_created_at, collab_format_name, collab_user_email'
            . ' from collab co'
            . ' join collab_format cf'
            . ' on cf.collab_format_id=co.collab_format_id'
            . ' where collab_user_id=:user_id and collab_recipient_id=:recipient';


        return $this->PDO->select($sql, array('user_id' => $id, 'recipient' => $recipient));

    }

    function get_univ_collab_details($id)
    {
        $sql = 'select cr.collab_recipient_name'
            . ' from collab_recipient cr'
            . ' join collab co'
            . ' on cr.collab_recipient_id=co.collab_recipient_id'
            . ' where collab_id=:id';

        return $this->PDO->select($sql, array('id' => $id));
    }

    function add_collab_reply($id, $format, $sender, $date_time)
    {
        try {
            $sql = "insert into collab"
                . " (collab_cpd_id, collab_user_id, collab_sender_name,"
                . " collab_recipient_id, collab_script_id, collab_format_id,"
                . " collab_type, collab_collab_id, collab_order, collab_created_at, collab_user_email)"
                . " select collab_cpd_id, collab_user_id, '$sender', collab_recipient_id,"
                . " collab_script_id, '$format', collab_type, '$id', 1, '$date_time', collab_user_email"
                . " from collab where collab_id=:id";

            $ret = $this->PDO->insert($sql, array('id' => $id));
        } catch (\Exception $e) {
            throw $e;
        }
        return $ret;
    }

    function delete_collab($id)
    {
        $sql = 'delete from collab'
            . ' where collab_id=:id or collab_collab_id=:id';

        return $this->PDO->delete($sql, array('id' => $id));
    }

    function get_collab_response_data($id)
    {
        $sql = 'select sc_sender'
            . ' from script_collab'
            . ' where sc_id=:id';

        $ret['sender'] = $this->PDO->select($sql, array('id' => $id))[0]->sc_sender;

        $sql = 'select collab_format_name, collab_format_id'
            . ' from collab_format'
            . ' where collab_format_default=1';

        $ret['format'] = $this->PDO->select($sql, array());

        return $ret;
    }

    function get_default_seq($seq)
    {
        $sql = 'select tmd_seq_number, tmd_text as tpda_content, tmd_order as tpda_answer_number'
            . ' from tsl_message_default'
            . ' where tmd_seq_number=:seq and tmd_component=\'Message\''
            . ' order by tmd_order';

        return $this->PDO->select($sql, array('seq' => $seq));
    }

    /**
     * Saves Collaboration text.
     *
     * @param $id   Int     Collaboration Id.
     * @param $val  String  Collaboration Data.
     * @return bool
     */
    public function save_collab_text($id, $val)
    {
        $sql = 'update collab'
            . ' set collab_data=:val'
            . ' where collab_id=:id';

        return $this->PDO->update($sql, array('val' => $val, 'id' => $id));
    }
}