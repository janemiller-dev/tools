<?php

namespace Model;

/**
 * WEB model class
 */
class WEB
{

    private $PDO;
    private $common_model;

    /**
     * Initialises the PDO and model instance.
     */
    public function __construct()
    {
        $this->PDO = \Model\PDO_model::instance('Tools');
        $this->common_model = new Common;
    }

    public function upload_ppt($random, $extension, $file_name, $user_id)
    {
        $sql = 'insert into user_ppt'
            . ' (up_user_id, up_random, up_name, up_extension)'
            . ' values(:user_id, :random, :name, :extension)';

        return $this->PDO->insert($sql, array('user_id' => $user_id, 'random' => $random . '.' . $extension,
            'name' => $file_name, 'extension' => $extension));
    }

    public function get_ppt_list($user_id)
    {
        $sql = 'select up_random, up_extension, up_id, up_name'
            . ' from user_ppt'
            . ' where up_user_id=:user_id';

        return $this->PDO->select($sql, array('user_id' => $user_id));
    }

    public function delete_presentation($id)
    {
        $sql = 'delete from user_ppt'
            . ' where up_id=:id';

        return $this->PDO->delete($sql, array('id' =>  $id));
    }

}