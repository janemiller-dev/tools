<?php

namespace Model;

class Company
{

	/**
	 * Add a new company.
	 */
	public function add_company($user_id, $name, $domain, $description)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Create the company record.
		$sql = "insert into company (company_user_id, company_name, company_domain, company_description)"
			. " values (:user_id, :name, :domain, :description)";
		$params = array('user_id' => $user_id,
			'name' => $name,
			'domain' => $domain,
			'description' => $description);
		return $PDO->insert($sql, $params);
	}

	/**
	 * Get the list of companies that meet the selection criteria.
	 */
	public function get_companies($input, $user_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Get the parameters.
		$draw = $input->get('draw');
		$start = $input->get('start');
		$length = $input->get('length');

		// Is there a search.
		$search = $input->get('search', array(), 'ARRAY');
		if (!empty($search['value']))
			$search_val = $search['value'];
		//echo 'Search Val: ' . $search_val;

		// Limit search to the current user
		$sql_where = ' where company_user_id=' . $user_id . ' ';
		$sep_where = ' and ';

		// Get the columns and the search
		$sql_columns_cte = '';
		$sql_columns = '';
		$sep = ' ';
		$columns = $input->get('columns', array(), 'ARRAY');
		foreach ($columns as $column) {

			// Skip the combined columns
			if ($column['data'] == 'contacts' || $column['data'] == 'phones' || $column['data'] == 'addresses' || $column['data'] == 'emails' || $column['data'] == null)
				continue;

			$sql_columns .= $sep . $column['data'];
			$search_col = $column['data'];
			$sep = ', ';

			// Add to the search?
			if (isset($search_val) && $column['searchable']) {
				$sql_where .= $sep_where . "cast(" . $search_col . " as char) like '%" . $search_val . "%'";
				$sep_where = ' or ';
			}
		}

		// Determine the order information.
		$order = $input->get('order', array(), 'ARRAY');
		$sql_order = '';
		foreach ($order as $o) {
			if (strlen($sql_order) > 0)
				$sql_order .= ', ';

			// Get the column
			$col = $columns[$o['column']]['data'];
			$sql_order .= $columns[$o['column']]['data'] . ' ' . $o['dir'];
		}

		// Get the total number of rows (unfiltered).
		$sql = "select count(*) from company where company_user_id=" . $user_id;
		$count_total = $PDO->select_count($sql, array());

		// Get the total number of rows (filtered).
		$sql = "select count(*) from company " . $sql_where;
		$count_filtered = $PDO->select_count($sql, array());

		// Get the filtered data.
		$sql = " select " . $sql_columns
			. " from company"
			. $sql_where
			. " order by " . $sql_order
			. " limit " . $start . ", " . $length;
		//echo $sql;
		$data = $PDO->select($sql, array());

		// Set the return values.
		$ret['draw'] = $draw;
		$ret['data'] = $data;
		$ret['recordsTotal'] = $count_total;
		$ret['recordsFiltered'] = $count_filtered;
		return $ret;
	}

	/**
	 * Get the specified company.
	 */
	public function get_company($company_id, $user_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Select the company. The user ID is include for security.
		$sql = "select * from company where company_id=:company_id and company_user_id=:user_id";
		return $PDO->select_row($sql, array('company_id' => $company_id, 'user_id' => $user_id));
	}

	/**
	 * Delete a company.
	 */
	public function delete_company($company_id)
	{
		$PDO = \Model\PDO_model::instance('Tools');

		// Create the company record.
		$sql = "delete from company where company_id=:company_id";
		$PDO->delete($sql, array('company_id' => $company_id));

		// Delete any attached phone numbers.
		$sql = "delete from phone where phone_object_name='company' and phone_object_id=:company_id";
		$PDO->delete($sql, array('company_id' => $company_id));

		// Delete any attached addresses.
		$sql = "delete from address where address_object_name='company' and address_object_id=:company_id";
		$PDO->delete($sql, array('company_id' => $company_id));

		// Delete any attached email addresses.
		$sql = "delete from email where email_object_name='company' and email_object_id=:company_id";
		$PDO->delete($sql, array('company_id' => $company_id));
	}

}