<?php

namespace Controller;

use function explode;
use function json_encode;
use function print_r;

class Survey extends Controller
{
    private $survey_model;
    private $ret;

    function __construct($app)
    {
        parent::__construct($app);
        $this->survey_model = new \Model\Survey;
        $this->ret['status'] = 'success';
    }

    /**
     * Checks if the credentials are valid then show Survey Form
     */
    function index()
    {
        $client_id =  $this->input->getInt('id');
        $token = $this->input->get('token', '', 'RAW');
        $survey_id = $this->input->getInt('survey_id');

        // Checks if any field is empty and validates token.
        if (empty($client_id) || empty($token) || empty($survey_id)
            || empty($this->survey_model->validate_token($token, $client_id, $survey_id))) {
            echo nl2br("Invalid Request!! \n Please Try again later!!");
            exit();
        }
        $ret['render'] = 'views/survey/bus.php';
        $ret['data'] = $this->view_data;
        return $ret;
    }

    /**
     * Fetches the list of questions for a survey.
     *
     */
    function get_ques()
    {
        $this->check_ajax();
        try {
            $outline_id = $this->input->getInt('outline');
            $this->ret['questions'] = $this->survey_model->get_ques($outline_id);
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Saves the client response to the database.
     */
    function set_ans()
    {
        $this->check_ajax();
        try {
            // Get the parameter.
            $question_id = $this->input->get('question_id', '', 'RAW');
            $array = explode(',', $question_id);
            $survey_id = $this->input->get('survey_id');
            $client_id = $this->input->getInt('client_id');
            $data = $this->input;

            $this->ret['answer'] = $this->survey_model->set_ans($array, $client_id, $survey_id, $data);

        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Gets the Buyer's survey questions.
     */
    function get_bus_questions()
    {
        $this->check_ajax();
        try {
            $survey_id = $this->input->getInt('id');
            $this->ret['questions'] = $this->survey_model->get_ques($survey_id);
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Gets the list of already answered questions.
     */
    public function get_ans()
    {
        $this->check_ajax();
        try {
            $question_id = $this->input->get('question_id', '', 'RAW');
            $client_id = $this->input->getInt('client_id');
            $survey_id = $this->input->getInt('survey_id');
            $this->ret['answers'] = $this->survey_model->get_ans($question_id, $client_id, $survey_id);
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }
}