<?php
/**
 * Targeted Suspect List controller.
 */

namespace Controller;

use const ENT_QUOTES;
use const PDF_MARGIN_LEFT;
use const PDF_MARGIN_RIGHT;
use function print_r;


class Tsl extends Controller
{
    private $tsl_model;
    private $ret;

    function __construct($app)
    {
        parent::__construct($app);
        $this->tsl_model = new \Model\TSL;
        $this->ret['status'] = $this->app->get_status('success');
    }

    /**
     * Default method.
     *
     */
    function index()
    {
        $this->ret['render'] = 'views/tsl/dashboard.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    function tsl()
    {
        $tsl_id = \explode('/', $_SERVER['REQUEST_URI'])[4];
        $user_id = $this->get_user_id();

        if (\sizeof($this->tsl_model->check_avail_tsl($tsl_id, $user_id)) == 0) {
            $this->input_missing_exception();
        }

        $ret['render'] = 'views/tsl/tsl.php';
        $ret['data'] = $this->view_data;
        return $ret;
    }


    /**
     * Search for the specified user's CPD instances.
     *
     * @return void $this->ret list of CPD instances
     */
    function search_tsl_instances()
    {
        $this->check_ajax();
        try {
            // Get the CPD instances
            $user_id = $this->get_user_id();
            $product_id = $this->input->getInt('product_id');
            $this->ret = $this->tsl_model->search_tsl_instances($user_id, $product_id, $this->input);
        } catch (\Exception $e) {
            $this->ret['error'] = $e->getMessage();
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Adds a new TSL instance.
     */
    function add_tsl()
    {
        $this->check_ajax();
        try {

            $data['year'] = $this->input->get('tsl_year');
            $data['user_id'] = $this->get_user_id();
            $data['industry_id'] = $this->input->getInt('tsl_industry_id');
            $data['product_id'] = $this->input->getInt('tsl_product_id');
            $data['cpd_id'] = $this->input->getInt('tsl_cpd_id');
            $data['prepared_by'] = $this->input->getInt('prepared_by');
            $data['audience'] = $this->input->get('audience');
            $data['criteria'] = $this->input->getInt('criteria');
            $data['name'] = $this->input->getString('tsl_ui_name');
            $data['description'] = $this->input->getString('tsl_ui_description');

            $this->ret['tsl_added'] = $this->tsl_model->add_tsl($data);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Fetches the list of background templates.
     *
     */
    function get_background_template()
    {
        $this->check_ajax();
        try {
            $user_id = $this->get_user_id();
            $this->ret['background_template'] = $this->tsl_model->get_background_template($user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);

    }

    /**
     * Converts the promotion form to PDF documents, creates a thumbnail and adds the record to DB.
     *
     */
    function compose_promo()
    {
        try {

            // Get the inputs.
            $content = \stripslashes($this->input->get('data', '', 'RAW'));
            $id = $this->input->getInt('id');
            $template_id = $this->input->getInt('template_id');
            $file_info = $this->tsl_model->get_image_info($template_id);
            $prev_pdf = $this->tsl_model->get_file_info($id);
            $is_pdf = $this->input->getInt('is_pdf');

            //  Check if there is an existing PDF for this version. Remove if exist.
            if ($is_pdf === 1) {
                !empty($prev_pdf[0]->tp_rand_name) ?
                    \unlink(__dir__ . '/../../upload/' . $prev_pdf[0]->tp_rand_name . '.pdf') : '';
            } else {
                !empty($prev_pdf[0]->tp_pdf_view) ?
                    \unlink(__dir__ . '/../../upload/' . $prev_pdf[0]->tp_pdf_view . '.pdf') : '';
            }

            // create new PDF document
            $pdf = new PST_PDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Roth Methods');
            $pdf->SetSubject('Promotional Document');
            $pdf->SetKeywords('document, PDF, promotion, roth, advisory');

            // set auto page breaks
            $pdf->SetAutoPageBreak(TRUE, 0);

            // set image scale factor
            $pdf->setImageScale(1);

            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

            // Check if background image is included.
            (!empty($file_info)) ? $pdf->setImagePath(__dir__ . '/../../thumbnails/' .
                $file_info[0]->tpb_rand_name . '.' . $file_info[0]->tpb_extension, 'promo') :
                $pdf->setImagePath('', 'promo');
            $pdf->setPrintFooter(false);

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            $pdf->AddPage('L', 'A4');
            $pdf->setPageUnit('px');

            // Set the default font
            $pdf->SetFont('centurygothic', '', 16);

            // Array for PDF vertical spacing.
            $tagvs = array(
                'p' => array(0 => array('h' => 1, 'n' => 8), 1 => array('h' => 1, 'n' => 1)),
                'h1' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
                'h2' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
                'h3' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
                'h4' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
                'h5' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
                'h6' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
                'div' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
                'blockquote' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
                'span' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
                'strong' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0))
            );

            $pdf->setHtmlVSpace($tagvs);
            $pdf->writeHTML($content);
            $random = "promo" . time();

            // Print the file out
            $pdf->Output(__dir__ . '/../../upload/' . $random . '.pdf', 'F');
            $this->tsl_model->add_promo($random, $id, $is_pdf);
            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Uploads a background image for Promotion Document and stores the record in DB.
     *
     */
    function upload_background_template()
    {
        $this->check_ajax();
        foreach ($_FILES as $file) {
            try {
                $file_name = pathinfo($file['name'][0]);

                if (!($file_name['extension'] == 'png' || $file_name['extension'] == 'jpg' ||
                    $file_name['extension'] == 'jpeg')) {
                    $this->invalid_pdf_exception();
                }

                $random = "thumb" . time();
                move_uploaded_file($file['tmp_name'][0],
                    __dir__ . '/../../thumbnails/' . $random . '.' . $file_name['extension']);
                $user_id = $this->get_user_id();

                // Creating a record in the DB.
                $this->ret['promo_id'] =
                    $this->tsl_model->
                    add_background_template($file['name'][0], $random, $user_id, $file_name['extension']);
                $this->ret['status'] = $this->app->get_status('success');
            } catch (\Exception $e) {
                $this->ret['status'] = $this->app->get_status('fail',
                    'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
                $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
                $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
            }
            echo \json_encode($this->ret);
        }
    }

    /**
     * Deletes background template file from the server and removes record from the DB.
     *
     */
    function delete_background_template()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('background_id');
            $file_info = $this->tsl_model->get_image_info($id);
            if (\unlink(__dir__ . '/../../thumbnails/' . $file_info[0]->tpb_rand_name . '.' . $file_info[0]->tpb_extension)) {
                $this->ret['background_deleted'] = $this->tsl_model->delete_background_template($id);
                $this->ret['status'] = $this->app->get_status('success');
            } else {
                $this->ret['status'] = $this->app->get_status('fail',
                    'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
                $this->ret['status'] = $this->app->get_status('fail',
                    'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . "Unable to delete file/file doesn't exist");
                $this->ret['message'] = "Unable to delete file/file doesn't exist";
            }
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Uploads Promo document to the server.
     *
     */
    function upload_promo()
    {
        $this->check_ajax();

        foreach ($_FILES as $file) {
            try {
                $file_name = pathinfo($file['name'][0]);

                if ($file_name['extension'] != 'pdf') {
                    $this->invalid_pdf_exception();
                }

                $user_id = $this->get_user_id();
                $outline_id = $this->input->getInt('outline_id');
                $random = "promo" . time();
                move_uploaded_file($file['tmp_name'][0], __dir__ . '/../../upload/' . $random . '.' . $file_name['extension']);

                // Creating a record in the DB.
                $this->ret['promo_id'] = $this->tsl_model->upload_promo($file['name'][0], $random, $outline_id, $user_id);
                $this->ret['status'] = $this->app->get_status('success');
            } catch (\Exception $e) {
                $this->ret['status'] = $this->app->get_status('fail',
                    'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
                $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
                $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
            }
            echo \json_encode($this->ret);
        }
    }

    /**
     * Returns the list of all the promotion documents available.
     *
     */
    function get_promos()
    {
        $this->check_ajax();

        try {
            $this->ret['promos'] = $this->tsl_model->get_promo();
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }


    /**
     * Display a preview for the promotion doc.
     *
     */
    function view()
    {
        $ret['render'] = 'views/tsl/dialogs/view.php';
        $ret['data'] = $this->view_data;
        return $ret;
    }

    /**
     * Preview for promotion doc.
     *
     */
    function view_promo()
    {
        try {
            // Get the inputs
            $id = $this->input->getInt('id');
            $is_pdf = $this->input->getInt('is_pdf');
            $file_info = $this->tsl_model->get_file_info($id);
            header('Content-Type: application/pdf');

            ($is_pdf === 0) ?
                \readfile(__dir__ . '/../../upload/' . $file_info[0]->tp_pdf_view . '.pdf') :
                \readfile(__dir__ . '/../../upload/' . $file_info[0]->tp_rand_name . '.pdf');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
    }

    /**
     * Preview for the thumbnail.
     *
     */
    function view_thumbnail()
    {
        try {

            $id = $this->input->getInt('id');
            $file_info = $this->tsl_model->get_image_info($id);

            header('Content-Type: image/png');

            \readfile(__dir__ . '/../../thumbnails/' . $file_info[0]->tpb_rand_name .
                '.' . $file_info[0]->tpb_extension);

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
    }


    /**
     *  Deletes a promo from the server and removes DB record.
     *
     */
    function delete_promo()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('promo_id');
            $type = $this->input->get('type');

            // If delete request is for SA doc
            if ($type == 'sa') {
                $random_name = $this->input->get('random_name');
                if (\unlink(__dir__ . '/../../upload/' . $random_name . '.pdf')) {
                    $this->ret['deal_id'] = $this->tsl_model->delete_sa($id);
                    $this->ret['status'] = $this->app->get_status('success');
                } else {
                    $this->ret['status'] = $this->app->get_status('fail',
                        'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' .
                        "Unable to delete file/file doesn't exist");
                    $this->ret['message'] = "Unable to delete file/file doesn't exist";
                }

            } else {

                $file_info = $this->tsl_model->get_file_info($id);

                // Check if pdf exist.
                if (!empty($file_info[0]->tp_rand_name)) {
                    if (\unlink(__dir__ . '/../../upload/' . $file_info[0]->tp_rand_name . '.pdf')) {
                        $this->ret['deal_id'] = $this->tsl_model->delete_promo($id);
                        $this->ret['status'] = $this->app->get_status('success');
                    } else {
                        $this->ret['status'] = $this->app->get_status('fail',
                            'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' .
                            "Unable to delete file/file doesn't exist");
                        $this->ret['message'] = "Unable to delete file/file doesn't exist";
                    }
                } else {
                    $this->ret['deal_id'] = $this->tsl_model->delete_promo($id);
                    $this->ret['status'] = $this->app->get_status('success');
                }
            }

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function delete_promo_outline()
    {
        $this->check_ajax();
        try {
            $outline_id = $this->input->getInt('promo_outline_id');
            $this->ret['outline_id'] = $this->tsl_model->delete_promo_outline($outline_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }


    /**
     * Renders the email/attachment page.
     *
     * @return mixed
     */
    function email()
    {
        $this->ret['render'] = 'views/tsl/email.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     *  Gets the content of the email and sends the email.
     *
     */
    function send_email()
    {
        $this->check_ajax();
        try {
            $subject = $this->input->getString('subject');
            $email_content = \stripslashes($this->input->get('content', '', 'RAW'));
            $current_user = wp_get_current_user();
            $user_name = $current_user->data->user_nicename;
            $client_email = $this->input->getString('client_email', '', 'RAW');
            $origin = $this->input->get('origin');
            $type = $this->input->get('type');
            $id = $this->input->getInt('id');
            $client_id = $this->input->getInt('client_id');
            $tool_id = $this->input->getInt('tool_id');

            foreach ($client_email as $email) {
                $email_array[$email] = 'client_name';
            }

            // Fetch the message body.
            $msg_body = $this->get_email_body($email_content, $user_name);

            $transporter = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
                ->setUsername($this->app->get_email_credentials('username'))
                ->setPassword($this->app->get_email_credentials('password'));

            $mailer = \Swift_Mailer::newInstance($transporter);

            // Construct the Yes message.
            $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setContentType('text/html')
                ->setTo($email_array)
                ->setFrom(array($current_user->data->user_email => $user_name))
                ->setBody($msg_body, 'text/html');

            $mailer->send($message);
            $this->tsl_model->add_component_sent($origin, $type, $client_id, $id, $tool_id);

            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Returns Email Body.
     *
     * @param $email_content Content to be sent
     * @param $user_name     Name of current User.
     * @return string
     */
    function get_email_body($email_content, $user_name)
    {
        header('Content-Type: text/plain');
        $new = \htmlspecialchars(\htmlspecialchars($email_content, ENT_QUOTES));

        $html = \shell_exec('php ' . __dir__ . '/../views/tsl/email_body.php \'' . $new . '\' ' . $user_name);
        return $html;
    }

    /**
     *  Sends the email with an attachment.
     *
     */
    function send_attachment()
    {
        $this->check_ajax();
        try {

            // Get input parameter.
            $id = $this->input->getInt('id');
            $subject = $this->input->getString('subject');
            $client_id = $this->input->getInt('client_id');
            $email_content = \stripslashes($this->input->get('text', '', 'RAW'));
            $type = $this->input->get('type');
            $origin = $this->input->get('origin');
            $tool_id = $this->input->getInt('tool_id');
            $current_user = wp_get_current_user();
            $user_name = $current_user->data->user_nicename;
            $pdf_id = $this->input->getInt('pdf_id');
            $msg_body = $this->get_email_body($email_content, $user_name);
            $client_email = $this->input->get('client_email', '', 'RAW');
            $random = $this->input->get('random');
            $content_id = $this->input->getInt('content_id');

            foreach ($client_email as $email) {
                $email_array[$email] = 'client_name';
            }

            $transporter = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
                ->setUsername($this->app->get_email_credentials('username'))
                ->setPassword($this->app->get_email_credentials('password'));

            $mailer = \Swift_Mailer::newInstance($transporter);
            $logger = new \Swift_Plugins_Loggers_ArrayLogger;
            $mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($logger));

            // Check for type of document.
            if ('sa' === $type) {

                // Construct the message.
                $message = \Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setContentType('text/html')
                    ->setTo($email_array)
                    ->setFrom(array($current_user->data->user_email => $user_name))
                    ->setReplyTo(array($current_user->data->user_email => $user_name))
                    ->attach(\Swift_Attachment::fromPath(__dir__ . '/../../upload/' . $random . '.pdf')
                        ->setFilename('Document.pdf'))
                    ->setBody($msg_body, 'text/html');

                $type = 'SAP';

            } else {
                $file_info = $this->tsl_model->get_file_info($pdf_id);

                // Construct the message.
                $message = \Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setContentType('text/html')
                    ->setTo($email_array)
                    ->setFrom(array($current_user->data->user_email => $user_name))
                    ->setReplyTo(array($current_user->data->user_email => $user_name))
                    ->setBody($msg_body, 'text/html');

                (0 !== $pdf_id) ? (
                $message
                    ->attach(\Swift_Attachment::fromPath(__dir__ . '/../../upload/' . $file_info[0]->tp_rand_name . '.pdf')
                        ->setFilename('Document.pdf'))) : '';
            }

            // Set the header for read receipt.
            $headers = $message->getHeaders();
            $headers->addIdHeader("X-Confirm-Reading-To", $current_user->data->user_email);
            $headers->addIdHeader("Return-receipt-to", $current_user->data->user_email);
            $headers->addIdHeader('Disposition-Notification-To: $current_user->data->user_email');
            $message->setReadReceiptTo($current_user->data->user_email);
            $sent = $mailer->send($message);

            // Check if the type variable is set or not
            (null !== $type) ?
                $this->tsl_model->add_component_sent($origin, $type, $client_id, $id, $tool_id, $content_id) : '';

            $this->ret['sent'] = $sent;
            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    // Renders TSl survey view.
    function survey()
    {
        $this->ret['render'] = 'views/tsl/survey.php';
        $this->ret['data'] = $this->view_data;
        echo $this->input->get('');
        return $this->ret;
    }

    /**
     * Adds a Buyer's Survey to the database.
     *
     */
    function add_survey()
    {
        $this->check_ajax();

        try {
            $outline_id = $this->input->getInt('outline_id');
            $user_id = $this->get_user_id();

            $this->ret['survey_id'] = $this->tsl_model->add_survey($outline_id, $user_id);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }

        echo \json_encode($this->ret);
    }

    /**
     * Fetches and returns the default for surveys.
     *
     */
    function get_survey_default()
    {
        $this->check_ajax();
        try {
            $survey_type = $this->input->get('survey_type');
            $this->ret['content'] = $this->tsl_model->get_survey_default($survey_type);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }

        echo \json_encode($this->ret);
    }

    /**
     * Returns the list of Buyer's Survey
     *
     */
    function get_survey_list()
    {
        $this->check_ajax();
        try {
            $outline_id = $this->input->get('outline_id');
            $user_id = $this->get_user_id();

            $this->ret['survey'] = $this->tsl_model->get_survey_list($outline_id, $user_id);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Fetches the content of Buyer's Survey for preview.
     *
     */
    function get_bus_content()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('bus_id');
            $version = $this->input->getInt('version');
            $this->ret['content'] = \stripslashes($this->tsl_model->get_bus_content($id, $version)[0]->tsl_survey_content);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Fetches Buyer's survey outlines.
     *
     */
    function get_bus_outline()
    {
        $this->check_ajax();
        try {
            $user_id = $this->get_user_id();
            $survey_type = $this->input->get('survey_type');
            $client_type = $this->input->get('client_type');
            $survey_class = $this->input->get('survey_class');

            $this->ret['bus'] = $this->tsl_model->get_bus_outline($user_id, $survey_type, $survey_class, $client_type);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Fetches the Buyer's Survey Questions.
     *
     */
    function get_bus_questions()
    {
        $this->check_ajax();
        try {
            $outline_id = $this->input->getInt('outline_id');
            $phase = $this->input->getInt('phase');
            $full_screen_enabled = $this->input->getInt('full_screen_enabled');

            $this->ret['content'] = $this->tsl_model->get_bus_questions($outline_id, $phase, $full_screen_enabled);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Fetches default survey answer from the database.
     */
    function get_survey_default_answer()
    {
        $this->check_ajax();

        try {
            $type = $this->input->getString('type');
            $phase = $this->input->getInt('phase');

            $this->ret['default_answers'] = $this->tsl_model->get_survey_default_answer($type, $phase);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Adds a new survey outline instance.
     */
    function add_survey_outline()
    {
        $this->check_ajax();

        try {
            $client_type = $this->input->get('client_type');
            $survey_type = $this->input->get('survey_type');
            $user_id = $this->get_user_id();

            $this->ret['survey_outline_added'] =
                $this->tsl_model->add_survey_outline($user_id, $survey_type, $client_type);
            $this->ret['status'] = 'success';

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Adds a new profile outline instance.
     */
    function add_profile_outline()
    {
        $this->check_ajax();

        try {
            $client_type = $this->input->get('client_type');
            $profile_type = $this->input->get('profile');
            $user_id = $this->get_user_id();

            $this->ret['survey_outline_added'] =
                $this->tsl_model->add_profile_outline($user_id, $profile_type, $client_type);
            $this->ret['status'] = 'success';

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Updates Profile Outline Name.
     */
    function update_profile_outline_name()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('pk');
            $value = $this->input->getString('value');

            $this->ret['name_updated'] = $this->tsl_model->update_profile_outline_name($id, $value);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }


    /**
     *
     * Saves the new outline and extracts question from outline.
     */
    function save_outline()
    {
        $this->check_ajax();
        try {
            $user_id = $this->get_user_id();
            $type = $this->input->get('type');
            $content = $this->input->get('outline_content', '', 'RAW');
            $requester = $this->input->get('requester');
            $phase = $this->input->getString('phase');
            $client_type = $this->input->get('client_type');
            $focus = $this->input->get('focus');
            $criteria = $this->input->get('criteria');
            $ta = $this->input->get('ta');
            $project_type = $this->input->getInt('project_type');
            $frequency = $this->input->getInt('frequency');
            $outline_id = $this->input->getInt('outline_id');
            $name = $this->input->getString('phase_name');
            $number = $this->input->getString('phase_number');

            // Extracting questions from outline.
            preg_match_all('{\{\{[^\}]*\}\}}', $content, $match);

            // Check if the request is for survey or script doc.
            if ($requester == 'survey') {
                $class = $this->input->get('class');
                $client = $this->input->get('client');
                $this->ret['result'] =
                    $this->tsl_model->save_bus_outline($name, $number, $content, $outline_id, $user_id, $match);
            } else if ($requester == 'script') {
                $this->ret['result'] =
                    $this->tsl_model->save_script_outline
                    ($user_id, $content, $match, $type, $requester, $client_type, $focus, $criteria, $ta);
            } else if ($requester == 'profile') {
                $this->ret['result'] = $this->tsl_model->save_profile_outline
                ($user_id, $project_type, $phase, $content, $type, $client_type, $match);
            } else if ($requester === 'update') {
                $this->ret['result'] = $this->tsl_model->save_update_outline
                ($user_id, $frequency, $phase, $content, $type, $client_type, $match);
            }
            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function update_phase_content()
    {
        $this->check_ajax();

        try {
            $phase_id = $this->input->getInt('phase');
            $content = $this->input->get('content', '', 'RAW');
            $question_id = \explode(",", $this->input->get('ques_id', '', 'RAW'));
            preg_match_all('{\{\{[^\}]*\}\}}', $content, $match);
            $requester = $this->input->getString('requester');

            if ($requester === 'profile')
                $this->ret['content_updated'] =
                    $this->tsl_model->update_profile_phase_content($phase_id, $content, $match, $question_id);
            else
                $this->ret['content_updated'] =
                    $this->tsl_model->update_phase_content($phase_id, $content, $match, $question_id);

            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Updates the name of Buyer's Survey.
     *
     */
    function update_bus_name()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('pk');
            $value = $this->input->getString('value');
            $this->ret['update'] = $this->tsl_model->update_bus_name($id, $value);

            // Type of the request so that appropriate method to refresh the page can be called.
            $this->ret['type'] = 'bus';
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Updates Survey Outline Name.
     */
    function update_bus_outline_name()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('pk');
            $value = $this->input->getString('value');

            $this->ret['name_updated'] = $this->tsl_model->update_bus_outline_name($id, $value);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Delete Buyer Survey instance.
     *
     */
    function delete_bus()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('bus_id');
            $this->ret['yes_id'] = $this->tsl_model->delete_bus($id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Deletes the buyer's survey outline.
     */
    function delete_bus_outline()
    {
        $this->check_ajax();
        try {
            $outline_id = $this->input->getInt('outline_id');
            $requester = $this->input->getString('type');

            $this->ret['outline_id'] = $this->tsl_model->delete_bus_outline($outline_id, $requester);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *  Adds a new client.
     *
     */
    function add_client()
    {
        $this->check_ajax();
        try {

            $tsl_id = $this->input->getInt('tsl_id');
            $count = $this->input->getInt('client_count');
            $user_id = $this->get_user_id();

            for ($i = 1; $i <= $count; $i++) {

                // Prepare an array of inputs.
                $phone = $this->input->get('client_phone' . $i, '', 'RAW');
                $email = $this->input->getString('client_email' . $i);
                $name = $this->input->getString('client_name' . $i);
                $agent = $this->input->getInt('client_agent' . $i);
                $criteria = $this->input->getInt('client_criteria' . $i);

                $this->ret['client_id'] = $this->tsl_model->add_client
                ($user_id, $name, $phone, $email, $agent, $criteria, $tsl_id);

            }
            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Fetches the list of clients for a user.
     *
     */
    function get_client_list()
    {
        $this->check_ajax();
        try {
            $user_id = $this->get_user_id();
            $tsl_id = $this->input->getInt('tsl_id');
            $sort_by = $this->input->get('sort_by');
            $type = $this->input->get('buyer_type');

            // Get TSL levels for current TSL instance.
            $tsl_levels = $this->tsl_model->get_tsl_level($tsl_id, $user_id, $type);

            foreach ($tsl_levels as $tsl_level) {

                $level = $tsl_level->tac_level;

                $this->ret['client'][$level] =
                    'buyer-' !== $type ?
                        $this->tsl_model->get_client_list($user_id, $tsl_id, $level, 5, 0, $sort_by, $type) :
                        $this->tsl_model->get_buyer_list($user_id, $tsl_id, $level, 5, 0, $sort_by, $type);
            }

            $this->ret['status'] = $this->app->get_status('success');

        } catch (\UnexpectedValueException $e) {
            $this->ret['status'] = $this->app->get_status('redirect');
            $this->ret['redirect'] = '/tools/tsl';

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function get_sam_data()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $tsl_id = $this->input->getInt('tsl_id');

            $this->ret['sam_data'] = $this->tsl_model->get_sam_data($tsl_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Fetch Additional rows for TGD.
     *
     */
    function get_rows()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $tsl_id = $this->input->getInt('id');
            $level = $this->input->getInt('level');
            $sort_by = $this->input->get('sort_by');

            $this->ret['rows'] = $this->tsl_model->get_client_list($user_id, $tsl_id, $level, 100000, 5, $sort_by);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Adds a new agent.
     *
     */
    function add_agent()
    {
        $this->check_ajax();
        try {
            $agent_name = $this->input->getString('agent_name');
            $tsl_id = $this->input->getInt('tsl_id');

            $this->ret['agent'] = $this->tsl_model->add_agent($agent_name, $tsl_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }


    /**
     * Deletes the agent.
     *
     */
    function delete_agent()
    {
        $this->check_ajax();
        try {
            $agent_id = $this->input->getInt('agent_id');
            $this->ret['agent_id'] = $this->tsl_model->delete_agent($agent_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Returns the List of criteria category.
     *
     */
    function get_category()
    {
        $this->check_ajax();
        try {
            //Get the user id and active product.
            $tsl_id = $this->input->getInt('tsl_id');

            $this->ret['category'] = $this->tsl_model->get_category($tsl_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Add new criteria.
     *
     */
    function add_criteria()
    {
        $this->check_ajax();
        try {
            $criteria = $this->input->getString('criteria_name');
            $user_id = $this->get_user_id();
            $product_id = $this->app->get_product();

            $this->ret['criteria_id'] = $this->tsl_model->add_criteria($criteria, $user_id, $product_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Add criteria category for a product.
     *
     */
//    function add_criteria_category()
//    {
//        $this->check_ajax();
//        try {
//            $category_name = $this->input->getString('criteria_category_name');
//            $tsl_id = $this->input->getInt('tsl_id');
//
//            $this->ret['category_id'] = $this->tsl_model->add_criteria_category($tsl_id, $category_name);
//            $this->ret['status'] = $this->app->get_status('success');
//        } catch (\Exception $e) {
//$this->ret['status'] = $this->app->get_status('fail',//            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
//            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
//        }
//        echo \json_encode($this->ret);
//    }

    /**
     * Deletes criteria.
     *
     */
    function delete_criteria()
    {
        $this->check_ajax();
        try {
            $criteria_id = $this->input->getInt('criteria_id');
            $this->ret['criteria_id'] = $this->tsl_model->delete_criteria($criteria_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Renders the script page.
     *
     * @return mixed
     */
    function script()
    {
        $this->ret['render'] = 'views/tsl/script.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     * Returns the script type that belongs to a tool.
     */
    function get_script_type()
    {
        $this->check_ajax();
        try {
            $origin = $this->input->get('origin');
            $req_type = $this->input->get('req_type');
            $this->ret['type'] = $this->tsl_model->get_script_type($origin, $req_type);
            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }

        echo \json_encode($this->ret);
    }

    /**
     * Fetches script outlines.
     */
    function get_script_outline()
    {
        $this->check_ajax();
        try {
            // Get the Inputs.
            $script_type = $this->input->get('script_type');
            $user_id = $this->get_user_id();
            $ta = $this->input->get('ta');
            $criteria = $this->input->get('criteria');
            $focus = $this->input->get('focus');
            $client_type = $this->input->get('client_type');

            // Fetch Outline Instances.
            $this->ret['outline'] =
                $this->tsl_model->get_script_outline($user_id, $script_type, $ta, $criteria, $focus, $client_type);

            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Fetches script answers.
     */
    function get_script_ans()
    {
        $this->check_ajax();
        try {
            $instance_id = $this->input->getInt('script_id');
            $type = $this->input->get('type');
            $client_id = $this->input->getInt('client_id');

            $this->ret['answer'] = $this->tsl_model->get_script_ans($instance_id, $type, $client_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('success');
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);

    }

    /**
     * Returns the List of script belonging to a particular user.
     */
    function get_script_list()
    {
        $this->check_ajax();
        try {
            $user_id = $this->get_user_id();
            $type_id = $this->input->getInt('type_id');
            $version_id = $this->input->getInt('version_id');

            $this->ret['scripts'] = $this->tsl_model->get_script_list($user_id, $type_id, $version_id);
            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }


    /**
     * Updates the name of a script.
     */
    function update_script_name()
    {
        $this->check_ajax();
        try {
            $script_id = $this->input->getInt('pk');
            $script_name = $this->input->getString('value');
            $this->ret['update'] = $this->tsl_model->update_script_name($script_id, $script_name);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Updates the content of a script.
     */
    function update_script()
    {
        $this->check_ajax();
        try {
            $type = $this->input->getString('type');
            $count = $this->input->getInt('count');
            $instance_id = $this->input->getInt('active_id');
            $client_id = $this->input->getInt('client_id');
            $outline_version = $this->input->getInt('outline_version');

            $data = $this->input;
            $this->ret['update'] = $this->tsl_model->update_script($data, $type, $count, $instance_id,
                $client_id, $outline_version);

            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Adds a new script.
     */
    function add_script()
    {
        $this->check_ajax();
        try {
            $user_id = $this->get_user_id();
            $type_id = $this->input->getInt('type_id');
            $version_id = $this->input->getInt('version_id');
            $component = $this->input->getString('component');

            $this->ret['script_id'] = $this->tsl_model->add_script($user_id, $type_id, $version_id, $component);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Deletes a script.
     */
    function delete_script()
    {
        $this->check_ajax();
        try {
            $script_id = $this->input->getInt('script_id');
            $this->ret['script_deleted'] = $this->tsl_model->delete_script($script_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);

    }

    /**
     *
     * Fetches and returns default answers for script from DB.
     */
    function get_script_default_answers()
    {
        $this->check_ajax();
        try {
            $outline_id = $this->input->getInt('outline_id');

            $this->ret['default_answers'] = $this->tsl_model->get_script_default_answers($outline_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Returns the criteria belonging to a client.
     */
    function get_client_criteria()
    {
        $this->check_ajax();
        try {
            //Get the user id and active product.
            $asset_id = $this->input->getInt('asset_id');
            $this->ret['criteria'] = $this->tsl_model->get_client_criteria($asset_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Returns the id of already used criteria.
     */
    function get_used_criteria()
    {
        $this->check_ajax();
        try {
            $client_id = $this->input->getInt('client_id');
            $this->ret['used_criteria'] = $this->tsl_model->get_used_criteria($client_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Uses a new criteria for a client.
     */
    function use_new_criteria()
    {
        $this->check_ajax();
        try {
            $criteria_id = $this->input->getInt('use_new_criteria_id');
            $client_id = $this->input->getInt('client_id');
            $this->ret['criteria_id'] = $this->tsl_model->use_new_criteria($client_id, $criteria_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Deletes criteria for a client.
     */
    function delete_client_criteria()
    {
        $this->check_ajax();
        try {
            $criteria_id = $this->input->getInt('criteria_id');
            $this->ret['criteria_id'] = $this->tsl_model->delete_client_criteria($criteria_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Updates  the script performance / proficiency.
     */
    function update_script_perf_prof()
    {
        $this->check_ajax();
        try {
            $client_id = $this->input->getInt('client-id');
            $script_id = $this->input->getInt('script-id');
            $cols_name = trim($this->input->getString('cols-name'));
            $cols = \explode(" ", $cols_name);
            $data = [];
            foreach ($cols as $col) {
                $data['tspp_' . $col] = $this->input->getInt($col);
            }


            $name = '';
            foreach ($data as $key => $value) {
                $name .= $key . ',';
            }

            $this->ret['script_perf_prof'] = $this->tsl_model->update_script_perf_prof($script_id, $client_id, $name, $data);

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Returns the latest performance / proficiency scores.
     */
    function get_perf_prof()
    {
        $this->check_ajax();
        try {
            $script_id = $this->input->getInt('script_id');
            $client_id = $this->input->getInt('client_id');
            $this->ret['cols'] = $this->tsl_model->get_perf_prof($script_id, $client_id);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Returns the client email ID.
     */
    function get_client_email()
    {
        $this->check_ajax();
        try {
            $client_id = $this->input->getInt('client_id');
            $this->ret['client'] = $this->tsl_model->get_client_email($client_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Sends the link for buyer's survey form
     */
    function send_bus()
    {
        $this->check_ajax();
        try {

            // Get Inputs
            $client_id = $this->input->getInt('client_id');
            $client_email = $this->input->getString('client_email');
            $subject = $this->input->getString('subject');
            $token = base64_encode(hash('sha256', time()));
            $survey_id = $this->input->getInt('survey_id');
            $profile_id = $this->input->getInt('profile_id');
            $outline_id = $this->input->getInt('outline_id');
            $update_id = $this->input->getInt('update_id');
            $type = $this->input->get('type');
            $origin = $this->input->get('origin');
            $text = \stripslashes($this->input->get('text', '', 'RAW'));
            $current_user = wp_get_current_user();
            $user_name = $current_user->data->user_nicename;
            $email_text = '';
            $tool_id = $this->input->getInt('tool_id');
            $phase = $this->input->getInt('phase');

            if ('survey' === $type) {
                // Link to Survey.
                $link = 'http://' . $_SERVER['SERVER_NAME'] . '/tools/survey?token=' . $token .
                    '&id=' . $client_id . '&survey_id=' . $survey_id . '&outline_id=' . $outline_id;
                $email_text = 'GO TO SURVEY';
            } else {
                $link = 'http://' . $_SERVER['SERVER_NAME'] . '/tools/profile?token=' . $token .
                    '&id=' . $client_id . '&profile_id=' . $profile_id . '&outline_id=' . $phase;
                $email_text = 'GO TO PROFILE';
            }

            $transporter = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
                ->setUsername($this->app->get_email_credentials('username'))
                ->setPassword($this->app->get_email_credentials('password'));

            $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setContentType('text/html')
                ->setTo(array($client_email => 'client_name'))
                ->setFrom(array($current_user->data->user_email => $user_name));

            $newHTML = $text;
            if (!empty($text)) {
                $doc = new \DOMDocument;
                $doc->loadHTML($text);

                // Fetch all the images.
                $anchors = $doc->getElementsByTagName('img');
                $len = $anchors->length;

                // Loop through images instances and chage source from base64 to image.
                for ($i = 0; $i < $len; $i++) {
                    $data = \explode(',', $anchors->item($i)->getAttribute('src'));
                    $cid = $message->embed(new \Swift_Image(\base64_decode($data[1]), 'image.jpg', 'image/jpeg'));
                    $anchors->item($i)->setAttribute('src', $cid);
                }

                $newHTML = $doc->saveHTML();
            }

            $msg_body = '<!DOCTYPE html><html><head><title>Page Title</title></head><body>' . $newHTML .
                '<div style="text-align: center">' .
                '<a href=' . $link . ' target=\'_blank\'>' . $email_text . '</a></div></body></html>';

            // Log Mailer instance.
            $this->app->log->debug("getting mailer");
            $mailer = \Swift_Mailer::newInstance($transporter);

            // Construct the message for Buyer's survey.
            if ('updates' === $type) {
                $message->setBody($newHTML, 'text/html');
            } else {
                $message->setBody($msg_body, 'text/html');
            }
            $mailer->send($message);

            // Create a database record with client id and token.
            if ('survey' === $type) {
                $this->ret['id'] = $this->tsl_model->add_token($token, $client_id, $survey_id);
                $this->tsl_model->add_component_sent($origin, $type, $client_id, $survey_id, $tool_id);
            } else if ('profile' === $type) {
                $this->ret['id'] = $this->tsl_model->add_profile_token($token, $client_id, $profile_id);
                $this->tsl_model->add_component_sent($origin, $type, $client_id, $profile_id, $tool_id);
            } else {
                $this->ret['id'] = $this->tsl_model->add_component_sent($origin, $type, $client_id, $update_id, $tool_id);
            }

            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Returns the performance / proficiency score for a particular client and script between specified dates.
     */
    function get_perf_score()
    {
        $this->check_ajax();
        try {
            // Get Inputs.
            $client_id = $this->input->getInt('client_id');
            $script_id = $this->input->getInt('script_id');
            $to_date = $this->input->get('to_date');
            $from_date = $this->input->get('from_date');

            $this->ret['score'] = $this->tsl_model->get_perf_score($client_id, $script_id, $to_date, $from_date);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Loads the ajax loader image.
     */
    function img()
    {
        try {
            // Reads the gif.
            header('Content-Type: image');
            \readfile(__dir__ . '/../../thumbnails/ajax-loader.gif');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
    }

    /**
     * Renders and return the message page.
     *
     * @return mixed
     */
    function vmail()
    {
        $this->ret['render'] = 'views/tsl/message.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    function get_message_outline()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $component = $this->input->getString('component');

            $this->ret['outlines'] = $this->tsl_model->get_message_outline($user_id, $component);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }

        echo \json_encode($this->ret);
    }

    function delete_msg_outline()
    {
        $this->check_ajax();

        try {
            $outline_id = $this->input->getInt('msg_outline_id');

            $this->ret['outline_deleted'] = $this->tsl_model->delete_msg_outline($outline_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }

        echo \json_encode($this->ret);
    }


    /**
     * Creates a new message set.
     */
    function create_message_sequence()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('id');
            $component_name = $this->input->getString('component_name');

            $this->ret['seq_id'] = $this->tsl_model->create_message_sequence($id, $component_name);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Returns the list of message set.
     */
    function get_message_seq()
    {
        $this->check_ajax();
        try {
            // Get Inputs.
            $id = $this->input->getInt('version_id');

            $this->ret['msg_seq'] = $this->tsl_model->get_message_seq($id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }


    /**
     * Creates a new message for a message sequence.
     */
    function create_message()
    {
        $this->check_ajax();
        try {
            $tool_id = $this->input->getInt('tool_id');
            $message_name = $this->input->getString('message_name');
            $component_name = $this->input->getString('component_name');
            $user_id = $this->get_user_id();
            $client_id = $this->input->getInt('client_id');
            $outline_id = $this->input->getInt('outline_id');

            $this->ret['msg_id'] =
                $this->tsl_model->create_message($tool_id, $message_name, $client_id, $component_name, $user_id, $outline_id);

            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Returns the list of message sequence for a message set.
     */
    function get_message()
    {
        $this->check_ajax();
        try {
            // Get Inputs.
            $tool_id = $this->input->getInt('tool_id');
            $client_id = $this->input->getInt('client_id');
            $component_name = $this->input->get('component_name');
            $user_id = $this->get_user_id();
            $outline_id = $this->input->getInt('outline_id');

            $this->ret['message'] = $this->tsl_model->get_message($tool_id, $client_id, $component_name, $user_id, $outline_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Returns the content for a  message sequence.
     */
    function get_message_content()
    {
        $this->check_ajax();
        try {
            $seq_id = $this->input->getInt('id');

            $this->ret['data'] = $this->tsl_model->get_message_content($seq_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Updates the content of a message sequence,
     */
    function update_message_content()
    {
        $this->check_ajax();
        try {
            $seq_id = $this->input->getInt('seq_id');
            $count = $this->input->getInt('seq_count');

            $this->ret['content'] = $this->tsl_model->update_message_content($this->input, $seq_id, $count);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Fetches the list of default Sequence available
     */
    function get_default_seq()
    {
        $this->check_ajax();
        try {
            $seq = $this->input->getInt('seq');
            $component = $this->input->getString('component');

            $this->ret['default_seq'] = $this->tsl_model->get_default_seq($seq, $component);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Updates the message set name.
     */
    function update_msg_seq_name()
    {
        $this->check_ajax();
        try {
            $name = $this->input->getString('value');
            $id = $this->input->getInt('pk');

            $this->ret['name'] = $this->tsl_model->update_msg_seq_name($name, $id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Updates the name of message sequence.
     */
    function update_msg_name()
    {
        $this->check_ajax();
        try {
            $name = $this->input->getString('value');
            $id = $this->input->getInt('pk');

            $this->ret['name'] = $this->tsl_model->update_msg_name($name, $id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Delete a Message instance.
     */
    function delete_msg()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('msg_id');

            $this->ret['name'] = $this->tsl_model->delete_msg($id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Deletes a Message Sequence.
     */
    function delete_seq()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('seq_id');

            $this->ret['name'] = $this->tsl_model->delete_seq($id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Returns List of TSL data for a user.
     */
    function get_tsl_data()
    {
        $this->check_ajax();
        try {
            $tsl_id = $this->input->getInt('tsl_id');
            $user_id = $this->get_user_id();

            $this->ret['tsl_data'] = $this->tsl_model->get_tsl_data($tsl_id, $user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }


    /**
     * Deletes a caller,
     */
    function delete_caller()
    {
        $this->check_ajax();
        try {
            $caller_id = $this->input->getInt('caller_id');
            $this->ret['caller'] = $this->tsl_model->delete_caller($caller_id);

            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Deletes an action instance.
     */
    function delete_action()
    {
        $this->check_ajax();
        try {
            $action_id = $this->input->getInt('action_id');
            $this->ret['action_deleted'] = $this->tsl_model->delete_action($action_id);

            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Deletes an assignee.
     */
    function delete_assignee()
    {
        $this->check_ajax();
        try {
            $assignee_id = $this->input->getInt('assignee_id');

            $this->ret['assignee'] = $this->tsl_model->delete_assignee($assignee_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Adds a new caller to the list,
     */
    function add_caller()
    {
        $this->check_ajax();
        try {
            $tsl_id = $this->input->getInt('tsl_id');

            $caller_name = $this->input->getString('caller_name');
            $this->ret['caller'] = $this->tsl_model->add_caller($caller_name, $tsl_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Adds a new Action to the list,
     */
    function add_action()
    {
        $this->check_ajax();
        try {
            $tsl_id = $this->input->getInt('tsl_id');
            $action_text = $this->input->getString('action_text');

            $this->ret['caller'] = $this->tsl_model->add_action($action_text, $tsl_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }


    /**
     * Adds a new target audience instance.
     */
    function add_target_audience()
    {
        $this->check_ajax();
        try {
//            $tsl_id = $this->input->getInt('tsl_id');
            $target_audience = $this->input->getString('target_audience_name');
            $user_id = $this->get_user_id();
            $product_id = $this->app->get_product();

            $this->ret['caller'] = $this->tsl_model->add_target_audience($target_audience, $user_id, $product_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Deletes a target audience instance.
     */
    function delete_target_audience()
    {
        $this->check_ajax();
        try {
            $target_audience_id = $this->input->getInt('target_audience_id');

            $this->ret['target_audience_deleted'] = $this->tsl_model->delete_target_audience($target_audience_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Adds a new name to the assignee list.
     */
    function add_assignee()
    {
        $this->check_ajax();
        try {
            $user_id = $this->get_user_id();
            $assignee_name = $this->input->getString('assignee_name');

            $this->ret['caller'] = $this->tsl_model->add_assignee($assignee_name, $user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Renders the package page.
     *
     * @return mixed
     */
    function page()
    {
        $this->ret['render'] = 'views/tsl/page.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }


    /**
     * Fetches promotion document outlines.
     */
    function get_promo_outline()
    {
        $this->check_ajax();
        try {
            $user_id = $this->get_user_id();
            $promo_focus = $this->input->get('promo_focus');
            $promo_format = $this->input->get('promo_format');
            $client_type = $this->input->get('client_type');
            $criteria = $this->input->getInt('criteria');
            $audience = $this->input->getInt('audience');

            $this->ret['promo'] = $this->tsl_model->get_promo_outline
            ($user_id, $promo_format, $promo_focus, $client_type, $criteria, $audience);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Fetches questions related to a promotion document outline.
     */
    function get_promo_questions()
    {
        $this->check_ajax();
        try {
            $outline_id = $this->input->getInt('outline_id');

            $this->ret['promo'] = $this->tsl_model->get_promo_questions($outline_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Returns list of promotion document.
     */
    function get_promo_list()
    {
        $this->check_ajax();
        try {
            $outline_id = $this->input->getInt('outline_id');
            $user_id = $this->get_user_id();
            $client_id = $this->input->getInt('client_id');


            $this->ret['promo'] = $this->tsl_model->get_promo_list($outline_id, $user_id, $client_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Save promo outline and extracts questions out of it.
     */
    function save_promo_outline()
    {
        $this->check_ajax();
        try {
            // Get Inputs.
            $promo_focus = $this->input->get('promo_focus');
            $promo_format = $this->input->get('promo_format');
            $client_type = $this->input->get('client_type');
            $outline = $this->input->get('outline_content', '', 'RAW');
            $user_id = $this->get_user_id();
            $criteria = $this->input->getInt('criteria');
            $target_audience = $this->input->getInt('target_audience');

            // Extracting questions from outlines.
            $this->ret['promo_id'] =
                $this->tsl_model->save_promo_outline
                ($user_id, $promo_focus, $promo_format, $client_type, $outline, $criteria, $target_audience);

            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Updates the name of Promotional Document.
     *
     */
    function update_promo_name()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('pk');
            $value = $this->input->getString('value');
            $this->ret['update'] = $this->tsl_model->update_promo_name($id, $value);

            // Type of the request so that appropriate method to refresh the page can be called.
//            $this->ret['type'] = 'bus';
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Fetches and returns default promo answers.
     */
    function get_default_promo_answers()
    {
        $this->check_ajax();

        try {
            $outline_id = $this->input->getInt('outline_id');

            $this->ret['default_answers'] = $this->tsl_model->get_default_promo_answers($outline_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Fetches outline for emails.
     */
    function get_email_outline()
    {
        $this->check_ajax();
        try {
            // Get Inputs.
            $email_type = $this->input->get('email_type');
            $email_event = $this->input->get('email_event');
            $user_id = $this->get_user_id();
            $email_purpose = $this->input->get('email_purpose');
            $ta = $this->input->get('email_ta');
            $criteria = $this->input->get('email_criteria');
            $focus = $this->input->get('email_focus');

            $this->ret['email_outline'] = $this->tsl_model->get_email_outline
            ($email_event, $email_type, $user_id, $email_purpose, $ta, $criteria, $focus);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);

    }

    /**
     * Fetches default email outline.
     */
    function get_email_default()
    {
        $this->check_ajax();
        try {
            $email_event = $this->input->get('email_event');

            $this->ret['default_outline'] = $this->tsl_model->get_email_default($email_event);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function get_email_default_answer()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $this->ret['default_answers'] = $this->tsl_model->get_email_default_answer($id);

            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Save email outline and list it as a new version.
     */
    function save_email_outline()
    {
        $this->check_ajax();
        try {
            // Get Inputs.
            $user_id = $this->get_user_id();
            $email_type = $this->input->get('email_type');
            $email_purpose = $this->input->get('email_purpose');
            $email_event = $this->input->get('email_event');
            $content = $this->input->get('outline_content', '', 'RAW');
            $ta = $this->input->get('ta');
            $criteria = $this->input->get('criteria');
            $focus = $this->input->get('focus');

            // Extracting questions from outline.
            preg_match_all('{\{\{[^\}]*\}\}}', $content, $match);
            $this->ret['result'] = $this->tsl_model->save_email_outline
            ($user_id, $content, $match, $email_type, $email_purpose, $email_event, $ta, $criteria, $focus);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Fetches list of email versions available.
     */
    function get_email_list()
    {
        $this->check_ajax();
        try {
            $outline_id = $this->input->getInt('outline_id');
            $user_id = $this->get_user_id();

            $this->ret['content'] = $this->tsl_model->get_email_list($outline_id, $user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Adds a new instance of email.
     */
    function add_email_instance()
    {
        $this->check_ajax();
        try {
            // Get inputs.
            $outline_id = $this->input->getInt('outline_id');
            $user_id = $this->get_user_id();

            $this->ret['email_id'] = $this->tsl_model->add_email_instance($outline_id, $user_id);
            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Fetches email content already answered.
     */
    function get_email_content()
    {
        $this->check_ajax();
        try {
            $client_id = $this->input->getInt('client_id');
            $email_id = $this->input->getInt('email_id');

            $this->ret['answers'] = $this->tsl_model->get_email_content($client_id, $email_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Saves the email content.
     */
    function set_email_content()
    {
        $this->check_ajax();
        try {
            // Get the parameter.
            $count = $this->input->getInt('count');
            $email_id = $this->input->get('email_id');
            $client_id = $this->input->getInt('client_id');
            $data = $this->input;

            $this->ret['content_updated'] = $this->tsl_model->set_email_content($count, $client_id, $email_id, $data);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Deletes email outlines.
     */
    function delete_email_outline()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('email_outline_id');
            $this->ret['outline_deleted'] = $this->tsl_model->delete_email_outline($id);

            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Delete an email instance.
     */
    function delete_email()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('id');

            $this->tsl_model->delete_email($id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('success');
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Update Email Name.
     */
    function update_email_name()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('pk');
            $name = $this->input->getString('value');

            $this->ret['name_updated'] = $this->tsl_model->update_email_name($id, $name);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->check_ajax();
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Update Client Info.
     */
    function update_client_info()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('id');
            $col = $this->input->get('col');
            $val = $this->input->get('val', '', 'RAW');
            $table = $this->input->get('table');

            (('hc_selected_phone' === $col) && ('-1' === $val)) ?
                '' : $this->ret['info_updated'] = $this->tsl_model->update_client_info($id, $col, $val, $table);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->check_ajax();
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function get_metric_data()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('tsl_id');
            $period = $this->input->get('period');

            $this->ret['tsl_metric'] = $this->tsl_model->get_metric_data($id, $period);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->check_ajax();
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }


    /**
     * Add notes for user.
     */
    function add_user_notes()
    {
        $this->check_ajax();
        try {
            $value = $this->input->get('tsl_note', '', 'RAW');
            $tsl_id = $this->input->getInt('tsl_id');

            $this->ret['note_added'] = $this->tsl_model->add_user_notes($value, $tsl_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Uploads a roleplay session to S3 bucket.
     */
    function upload_roleplay()
    {
        $this->check_ajax();
        try {

            foreach ($_FILES as $file) {

                $file_name = pathinfo($file['name'][0]);
                if (!$file_name['extension'] == 'mp3') {
                    $this->invalid_pdf_exception();
                }

                move_uploaded_file($file['tmp_name'][0], $file['tmp_name'][0] . '.mp3');

                $bucket = 'astroleplay';
                $s3 = $this->get_s3();
                $result = $s3->putObject([
                    'Bucket' => $bucket,
                    'Key' => $file['name'][0],
                    'SourceFile' => $file['tmp_name'][0] . '.mp3',
                    'Content-Type' => 'audio/mpeg',
                    'ACL' => 'public-read'
                ]);

                $this->ret['result'] = $result;
                $this->ret['status'] = $this->app->get_status('success');
            }
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Deletes a script outline.
     */
    function delete_script_outline()
    {
        $this->check_ajax();
        try {
            $outline_id = $this->input->getInt('outline_id');

            $this->ret['outline_deleted'] = $this->tsl_model->delete_script_outline($outline_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }


    /**
     * Push a client to next level and move to the last of current level.
     */
    function push_client()
    {
        $this->check_ajax();

        try {
            $tac_id = $this->input->getInt('tac_id');
            $sequence = $this->input->getInt('sequence');
            $client_id = $this->input->getInt('client_id');
            $tac_level = $this->input->getInt('tac_level');
            $caller = $this->input->getString('caller');

            $this->ret['client_pushed'] = $this->tsl_model->push_client
            ($tac_id, $sequence, $client_id, $tac_level, $caller);

            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Moves a deal to CPD.
     */
    function move_deal()
    {
        $this->check_ajax();

        try {
            $tac_id = $this->input->getInt('tac_id');
            $user_id = $this->get_user_id();
            $tc_id = $this->input->getInt('tc_id');

            $this->ret['deal_moved'] = $this->tsl_model->move_deal($tac_id, $user_id, $tc_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }

        echo \json_encode($this->ret);
    }


    /**
     * Get details of promo doc.
     */
    function get_promo_details()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('promo_id');
            $user_id = $this->get_user_id();

            $this->ret['promo_details'] = $this->tsl_model->get_promo_details($id, $user_id);
            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);

    }

    /**
     * Update Worksheet information.
     */
    function update_wks_info()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('wks_id');
            $name = $this->input->get('client_name');
            $project = $this->input->get('client_project');
            $client_id = $this->input->getInt('client_id');

            $this->ret['wks_info_updated'] = $this->tsl_model->update_wks_info($id, $client_id, $name, $project);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Render Documents page.
     *
     * @return mixed
     */
    function docs()
    {
        $ret['render'] = 'views/cpdv1/docs.php';
        $ret['data'] = $this->view_data;
        return $ret;
    }

    /**
     * Returns Worksheet view.
     *
     * @return mixed
     */
    function wks()
    {
        $this->ret['render'] = 'views/tsl/script.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }


    /**
     * Render Strategic analyzer.
     *
     * @return mixed
     */
    function sa()
    {
        $this->ret['render'] = 'views/cpdv1/sa.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     * Renders Web page.
     *
     * @return mixed
     */
    function web()
    {
        $this->ret['render'] = 'views/cpdv1/web.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    function retrieve_call_record()
    {
        $sid = "AC0365e535f6cb0c85389cf0c49f0af1fa";
        $token = "5e639adfa4a3057b06120676232ba9c4";
        $twilio = new Client($sid, $token);

        $calls = $twilio->calls->read(array("status" => "completed", "direction" => "outbound-api"));

        foreach ($calls as $record) {
            echo \json_encode('DateTime: ' . $record->dateCreated->format('Y-m-d H:i:s') .
                ' --- FROM:' . $record->from . ' --- TO:' . $record->to . ' --- Cost:' .
                $record->price . ' --- Duration:' . $record->duration . 'Sec');
        }
    }

    /**
     * Deletes a TSL instance.
     *
     */
    function delete_tsl()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('tsl_id');

            $this->ret['tsl_deleted'] = $this->tsl_model->delete_tsl($id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }

        echo \json_encode($this->ret);
    }

    /**
     *  Fetch Notes content
     *
     */
    function get_action_content()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');

            $this->ret['action_content'] = $this->tsl_model->get_action_content($id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Update Action content.
     */
    function update_action_content()
    {
        $this->check_ajax();
        try {
            $client_id = $this->input->getInt('id');
            $col = $this->input->get('col');
            $val = $this->input->getString('val');
            $seq = $this->input->getInt('seq');

            $this->ret['action_content_updated'] =
                $this->tsl_model->update_action_content($client_id, $col, $val, $seq);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Fetch the notes content
     */
    function get_notes_content()
    {
        $this->check_ajax();

        try {
            $client_id = $this->input->getInt('id');

            $this->ret['notes_content'] = $this->tsl_model->get_notes_content($client_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }

        echo \json_encode($this->ret);
    }

    function update_notes_content()
    {
        $this->check_ajax();

        try {
            $col = $this->input->get('col');
            $val = $this->input->getString('val');
            $seq = $this->input->getInt('seq');
            $client_id = $this->input->getInt('id');

            $this->ret['notes_content_updated'] = $this->tsl_model->update_notes_content($client_id, $col, $val, $seq);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception  $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }

        echo \json_encode($this->ret);
    }

    function add_promo_instance()
    {
        $this->check_ajax();

        try {
            $outline_id = $this->input->getInt('outline_id');
            $name = $this->input->getString('name');
            $user_id = $this->get_user_id();

            $this->ret['promo_instance_added'] = $this->tsl_model->add_promo_instance($outline_id, $user_id, $name);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *Adds a new promo instance.
     *
     */
    function update_promo()
    {
        $this->check_ajax();

        try {
            $content = $this->input->get('content', '', 'RAW');
            $form_data = $this->input->get('form_data', '', 'RAW');
            $active_promo_id = $this->input->getInt('promo_id');
            $questions_length = $this->input->getInt('questions_length');

            $this->ret['promo_added'] = $this->tsl_model->update_promo_instance
            ($content, $form_data, $active_promo_id, $questions_length);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Propagates Promo Instance.
     */
    function propagate_promo()
    {
        $this->check_ajax();

        try {
            $client_id = $this->input->getInt('client_id');
            $user_id = $this->get_user_id();
            $promo_id = $this->input->getInt('promo_id');
            $content = $this->input->get('content', '', 'RAW');

            $this->ret['promo_propagated'] =
                $this->tsl_model->propagate_promo($client_id, $user_id, $promo_id, $content);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Saves Promo content as html.
     */
    function save_promo_content()
    {
        $this->check_ajax();

        try {

            $id = $this->input->getInt('id');
            $client_id = $this->input->getInt('client_id');
            $content = $this->input->get('content', '', 'RAW');

            $this->ret['content_saved'] = $this->tsl_model->save_promo_content($id, $content, $client_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Fetch Promo Data.
     */
    function get_promo_data()
    {
        $this->check_ajax();
        try {

            $id = $this->input->getInt('id');

            $this->ret['data'] = $this->tsl_model->get_promo_data($id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }


    /**
     *
     * Fetch complete data for TSL.
     */
    function get_user_data()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $product_id = $this->app->get_product();

            $this->ret['info'] = $this->tsl_model->get_user_data($user_id, $product_id);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }

        echo \json_encode($this->ret);

    }

    /**
     * Adds a new copy of message for the particular message version.
     *
     */
    function add_copy_message()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');

            $this->ret['message_copied'] = $this->tsl_model->add_copy_message($id);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Deletes a copy of message.
     *
     */
    function delete_message_copy()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');

            $this->ret['copy_deleted'] = $this->tsl_model->delete_message_copy($id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function save_as_new_msg()
    {
        $this->check_ajax();

        try {
            $msg_id = $this->input->getInt('msg_id');

            $this->ret['msg_saved'] = $this->tsl_model->save_as_new_msg($msg_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);

    }

    /**
     *
     * Fetch Criteria categories that belong to this TSL instance.
     */
    function get_criteria_category()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');

            $this->ret['criteria_category'] = $this->tsl_model->get_criteria_category($id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Fetches Criteria for a particular category.
     */
    function get_criteria()
    {
        $this->check_ajax();

        try {
            $category_id = $this->input->getInt('id');

            $this->ret['criteria'] = $this->tsl_model->get_criteria($category_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Fetches criteria and TA that belongs to this TSL instance.
     */
    function get_tsl_ta_criteria()
    {
        $this->check_ajax();

        try {
            $tsl_id = $this->input->getInt('tsl_id');
            $user_id = $this->get_user_id();

            $this->ret['data'] = $this->tsl_model->get_tsl_ta_criteria($tsl_id, $user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function get_user_tsl_data()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $tsl_id = $this->input->getInt('tsl_id');
            $product_id = $this->app->get_product();

            $this->ret['tsl_data'] = $this->tsl_model->get_user_tsl_data($tsl_id, $user_id, $product_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }


    function update_tsl_data()
    {
        $this->check_ajax();

        try {
            $tsl_id = $this->input->getInt('id');
            $component_id = $this->input->getInt('val');
            $type = $this->input->getString('type');
            $user_id = $this->get_user_id();

            $this->ret['data_updated'] =
                $this->tsl_model->update_tsl_data($tsl_id, $component_id, $type, $user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Fetch Promotional Document Focus List.
     */
    function get_promo_focus()
    {
        $this->check_ajax();

        try {
            $tool_id = $this->input->getInt('tool_id');
            $origin = $this->input->get('origin');

            $this->ret['data'] = $this->tsl_model->get_promo_focus($tool_id, $origin);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Adds a new Promotional focus for the TSL.
     */
    function add_promo_focus()
    {
        $this->check_ajax();

        try {
            $tool_id = $this->input->getInt('tool_id');
            $promo_focus_name = $this->input->getString('promo_focus_name');
            $origin = $this->input->get('origin');

            $this->ret['promo_focus_added'] = $this->tsl_model->add_promo_focus($tool_id, $promo_focus_name, $origin);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Deletes a Promotional focus.
     */
    function delete_promo_focus()
    {
        $this->check_ajax();

        try {
            $promo_focus_id = $this->input->getInt('promo_focus_id');

            $this->ret['promo_focus_deleted'] = $this->tsl_model->delete_promo_focus($promo_focus_id);
            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Fetches and displays Story Builder.
     */
    function story()
    {
        $ret['render'] = 'views/tsl/message.php';
        $ret['data'] = $this->view_data;
        return $ret;
    }

    /**
     *
     * Updates the content of Promo component.
     */
    function update_promo_content()
    {
        $this->check_ajax();
        try {
            $content = $this->input->get('content', '', 'RAW');
            $id = $this->input->getInt('id');
            $type = $this->input->get('type');
            $outline_id = $this->input->getInt('outline_id');
            $user_id = $this->get_user_id();

            $this->ret['content_updated'] =
                $this->tsl_model->update_promo_content($id, $type, $content, $outline_id, $user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Change Active Outline for Promo component.
     */
    function change_active_outline()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('id');
            $component = $this->input->getString('component');
            $promo_focus = $this->input->getString('promo_focus');
            $promo_format = $this->input->getString('promo_format');
            $client_type = $this->input->getString('client_type');
            $criteria = $this->input->getString('criteria');
            $audience = $this->input->getString('audience');
            $origin = $this->input->getString('origin');
            $user_id = $this->get_user_id();

            $this->ret['outline_updated'] = $this->tsl_model->change_active_outline
            ($id, $component, $promo_focus, $promo_format, $client_type, $criteria, $audience, $origin, $user_id);

            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Updates Promo outlines Name.
     */
    function update_promo_outline_name()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('pk');
            $value = $this->input->getString('value');

            $this->ret['name_updated'] = $this->tsl_model->update_promo_outline_name($id, $value);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Fetches and returns promo archive for an active product type and user
     */
    function get_promo_archive()
    {
        $this->check_ajax();
        try {
            $tool_id = $this->input->getInt('id');
            $origin = $this->input->get('origin');
            $user_id = $this->get_user_id();
            $component_name = $this->input->get('component');

            $this->ret = $this->tsl_model->get_promo_archive($tool_id, $origin, $user_id, $this->input, $component_name);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Fetches and returns Archive Script Component.
     */
    function get_script_archive()
    {
        $this->check_ajax();

        try {

            $tool_id = $this->input->getInt('id');
            $origin = $this->input->get('origin');
            $user_id = $this->get_user_id();
            $component_name = $this->input->get('component');

            $this->ret = $this->tsl_model->get_script_archive($tool_id, $origin, $user_id, $this->input, $component_name);
            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Adds a new promo archive instance.
     */
    function add_archive()
    {
        $this->check_ajax();

        try {
            $promo_id = $this->input->get('promo_id');
            $user_id = $this->get_user_id();
            $type = $this->input->get('type');
            $asset_id = $this->input->getInt('asset_id');
            $datetime = $this->input->getString('datetime');
            $topic = $this->input->getString('topic');

            $this->ret['promo_archive_added'] =
                $this->tsl_model->add_archive($promo_id, $user_id, $type, $asset_id, $datetime, $topic);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Fetches and returns Archive Script Component.
     */
    function get_email_archive()
    {
        $this->check_ajax();

        try {

            $tool_id = $this->input->getInt('id');
            $origin = $this->input->get('origin');
            $user_id = $this->get_user_id();
            $component_name = $this->input->get('component');

            $this->ret = $this->tsl_model->get_email_archive($tool_id, $origin, $user_id, $this->input, $component_name);
            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /*
     * Returns Profile Builder View.
     */
    function profile()
    {
        $this->ret['render'] = 'views/tsl/profile.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     *
     * Fetches and returns Profile outline.
     */
    function get_profile_outline()
    {
        $this->check_ajax();

        try {
            // Get the inputs.
            $profile_phase = $this->input->getInt('profile_phase');
            $profile_type = $this->input->getInt('profile_type');
            $project_type = $this->input->getInt('project_type');
            $client_type = $this->input->getString('client_type');
            $user_id = $this->get_user_id();

            $this->ret['profile_outline'] = $this->tsl_model->get_profile_outline
            ($project_type, $profile_phase, $profile_type, $client_type, $user_id);

            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Fetches and returns profile default outline.
     */
    function get_profile_default()
    {
        try {
            $profile_type = $this->input->getInt('profile_type');

            $this->ret['default_outline'] = $this->tsl_model->get_profile_default($profile_type);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Fetches Questions from DB.
     */
    function get_profile_questions()
    {
        $this->check_ajax();

        try {
            $outline_id = $this->input->getInt('outline_id');
            $full_screen_enabled = $this->input->getInt('full_screen_enabled');
            $phase = $this->input->getInt('phase');

            $this->ret['content'] = $this->tsl_model->get_profile_questions($outline_id, $full_screen_enabled, $phase);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     * Returns the list of Buyer's Survey
     *
     */
    function get_profile_list()
    {
        $this->check_ajax();
        try {
            $outline_id = $this->input->get('outline_id');
            $user_id = $this->get_user_id();

            $this->ret['profile'] = $this->tsl_model->get_profile_list($outline_id, $user_id);
            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }


    /**
     * Adds a Profile to the database.
     *
     */
    function add_profile()
    {
        $this->check_ajax();

        try {
            $outline_id = $this->input->getInt('outline_id');
            $user_id = $this->get_user_id();

            $this->ret['survey_id'] = $this->tsl_model->add_profile($outline_id, $user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }

        echo \json_encode($this->ret);
    }

    /**
     *
     * Deletes a profile outline version.
     */
    function delete_profile_outline()
    {
        $this->check_ajax();

        try {
            $outline_id = $this->input->getInt('outline_id');
            $this->ret['outline_deleted'] = $this->tsl_model->delete_profile_outline($outline_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }

        echo \json_encode($this->ret);
    }

    /**
     *
     * Updates Profile version Name.
     */
    function update_profile_name()
    {
        $this->check_ajax();

        try {
            $value = $this->input->getString('value');
            $id = $this->input->getInt('pk');

            $this->ret['name_updated'] = $this->tsl_model->update_profile_name($value, $id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Deletes profile version instance.
     */
    function delete_profile()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('profile_id');

            $this->ret['profile_deleted'] = $this->tsl_model->delete_profile($id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /*
     * Returns Profile Builder View.
     */
    function updates()
    {
        $this->ret['render'] = 'views/tsl/updates.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     *
     * Fetches and returns Update outline.
     */
    function get_update_outline()
    {
        $this->check_ajax();

        try {
            // Get the inputs.
            $update_phase = $this->input->getInt('update_phase');
            $update_type = $this->input->getInt('update_type');
            $update_frequency = $this->input->getInt('update_frequency');
            $client_type = $this->input->getString('client_type');
            $user_id = $this->get_user_id();

            $this->ret['update_outline'] = $this->tsl_model->get_update_outline
            ($update_type, $update_phase, $update_frequency, $client_type, $user_id);

            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Fetches update default.
     */
    function get_update_default()
    {
        $this->check_ajax();

        try {
            // Get the inputs.
            $update_type = $this->input->getInt('update_type');
            $update_phase = $this->input->getInt('update_phase');
            $this->ret['update_outline'] = $this->tsl_model->get_update_default($update_type, $update_phase);

            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }


    /**
     *
     * Fetches updates question from the database.
     */
    function get_update_questions()
    {
        $this->check_ajax();

        try {
            $outline_id = $this->input->getInt('outline_id');

            $this->ret['content'] = $this->tsl_model->get_update_questions($outline_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Fetches default update answer from the DB.
     */
    function get_default_update_answer()
    {
        $this->check_ajax();

        try {
            $this->ret['default_answers'] = $this->tsl_model->get_default_update_answer();
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Fetches Update list from the DB.
     */
    function get_update_list()
    {
        $this->check_ajax();

        try {
            $outline_id = $this->input->getInt('outline_id');
            $user_id = $this->get_user_id();

            $this->ret['update_list'] = $this->tsl_model->get_update_list($outline_id, $user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Updates Update builder instance name.
     */
    function update_update_name()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('pk');
            $name = $this->input->getString('value');

            $this->ret['name_updated'] = $this->tsl_model->update_update_name($id, $name);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Adds a new instance of update version.
     */
    function add_update()
    {
        $this->check_ajax();
        try {
            $outline_id = $this->input->getInt('outline_id');
            $user_id = $this->get_user_id();

            $this->ret['update_added'] = $this->tsl_model->add_update($outline_id, $user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Fetches Updates Answer from the DB.
     */
    function get_update_ans()
    {
        $this->check_ajax();

        try {
            $update_id = $this->input->getInt('update_id');

            $this->ret['answer'] = $this->tsl_model->get_update_ans($update_id);
            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Updates Update builder content.
     */
    function update_update()
    {
        $this->check_ajax();

        try {
            $count = $this->input->getInt('count');
            $instance_id = $this->input->getInt('update_id');

            $data = $this->input;
            $this->ret['update'] = $this->tsl_model->update_update($data, $count, $instance_id);

            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }


    /**
     *
     * Deletes Update builder outline.
     */
    function delete_update_outline()
    {
        $this->check_ajax();

        try {
            $outline_id = $this->input->getInt('outline_id');

            $this->ret['outline_deleted'] = $this->tsl_model->delete_update_outline($outline_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Deletes update instance from the database.
     */
    function delete_update()
    {
        $this->check_ajax();

        try {
            $update_id = $this->input->getInt('update_id');

            $this->ret['update_deleted'] = $this->tsl_model->delete_update($update_id);
            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function update_dashboard_dropdown()
    {
        $this->check_ajax();

        try {
            $name = $this->input->getString('name');
            $key = $this->input->getString('key');
            $id = $this->input->getInt('id');
            $val = $this->input->getString('val');
            $table = $this->input->getString('table');

            $this->ret['dropdown_updated'] = $this->tsl_model->update_dashboard_dropdown($name, $key, $id, $val, $table);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Fetches Phases for a particular component and outline.
     */
    function get_phase()
    {
        $this->check_ajax();

        try {
            $outline_id = $this->input->getInt('id');
            $type = $this->input->getString('type');

            $this->ret['phases'] = $this->tsl_model->get_phase($outline_id, $type);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Uploads an audio file to the server.
     */
    function upload_audio()
    {
        $this->check_ajax();

        try {
            $name = $this->input->getString('name');
            $user_id = $this->get_user_id();

            // Upload recorded file to the server.
            $this->ret['upload_file'] =
                move_uploaded_file($_FILES['file']['tmp_name'], __dir__ . '/../../upload/' . $name);

            $this->ret['record_created'] = $this->tsl_model->add_recording($name, $user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Fetches the roleplay instances from the DB.
     */
    function get_recording()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();

            $this->ret['recording'] = $this->tsl_model->get_recording($user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Reads recording files from the server,
     */
    function play_recording()
    {
        try {
            // Get the inputs.
            $name = $this->input->get('name', '', 'RAW');
            header('Content-Type: audio/mp3');

            \readfile(__dir__ . '/../../upload/' . $name);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Deletes a recording.
     */
    function delete_recording()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('id');
            $name = $this->input->getString('name');

            \unlink(__dir__ . '/../../upload/' . $name);

            $this->ret['recording_deleted'] = $this->tsl_model->delete_recording($id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function update_recording_name()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('pk');
            $name = $this->input->getString('value');

            $this->ret['name_updated'] = $this->tsl_model->update_recording_name($id, $name);
            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Puts the message sequence into spoken message list.
     */
    function save_spoken_message()
    {
        $this->check_ajax();

        try {
            $client_id = $this->input->getInt('client_id');
            $seq_id = $this->input->getInt('seq_id');
            $component_id = $this->input->getInt('component_id');
            $component_type = $this->input->getString('component_type');

            $this->ret['saved_spoken_message'] =
                $this->tsl_model->save_spoken_message($client_id, $seq_id, $component_id, $component_type);
            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Reloads TSL asset list.
     */
    function reload_tsl()
    {
        $this->check_ajax();

        try {
            $tsl_id = $this->input->getInt('tsl_id');
            $user_id = $this->get_user_id();
            $info['location'] = $this->input->get('location', '', 'RAW');
            $info['desc'] = $this->input->get('desc', '', 'RAW');
            $info['cond'] = $this->input->get('cond', '', 'RAW');
            $data['area'] = $this->input->get('area', '', 'RAW');
            $data['criteria'] = $this->input->get('criteria', '', 'RAW');
            $product_id = $this->app->get_product();

            $this->ret['tsl_reloaded'] = $this->tsl_model->reload_tsl($tsl_id, $user_id, $data, $info, $product_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function fetch_assets_used()
    {
        $this->check_ajax();

        try {
            $audience = $this->input->get('audience');
            $criteria = $this->input->get('criteria');
            $desc = $this->input->get('desc');
            $cond = $this->input->get('cond');
            $user_id = $this->get_user_id();
            $product_id = $this->input->getInt('tsl_product_id');

            $this->ret['tsl_assets'] = $this->tsl_model->
            fetch_assets_used($audience, $criteria, $desc, $cond, $user_id, $product_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Updates promo focus content for a TSL.
     */
    function update_promo_focus()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $val = $this->input->getInt('val');

            $this->ret['updated_promo_focus'] = $this->tsl_model->update_promo_focus($id, $val);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    /**
     *
     * Propagates Email Content.
     */
    function propagate_email()
    {
        $this->check_ajax();

        try {
            $client_id = $this->input->getInt('client_id');
            $email_id = $this->input->getInt('email_id');

            $this->ret['email_propagated'] = $this->tsl_model->propagate_email_content($client_id, $email_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    // Renders TSl Notice view.
    function notice()
    {
        $this->ret['render'] = 'views/tsl/notice.php';
        $this->ret['data'] = $this->view_data;
        echo $this->input->get('');
        return $this->ret;
    }

    function get_notices_outline()
    {
        $this->check_ajax();

        try {
            $client_id = $this->input->getInt('client_id');
            $user_id = $this->get_user_id();

            $this->ret['outlines'] = $this->tsl_model->get_notices_outline($user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function add_notice()
    {
        $this->check_ajax();

        try {
            $client_id = $this->input->getInt('client_id');
            $user_id = $this->get_user_id();
            $name = $this->input->get('name', '', 'RAW');

            $this->ret['notice_added'] = $this->tsl_model->add_notice($client_id, $user_id, $name);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function get_notice_instance()
    {
        $this->check_ajax();

        try {
            $client_id = $this->input->getInt('client_id');
            $user_id = $this->get_user_id();

            $this->ret['notices'] = $this->tsl_model->get_notice_instance($client_id, $user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function save_notice_content()
    {
        $this->check_ajax();

        try {
            $notice_id = $this->input->getInt('notice_id');
            $data = $this->input;
            $client_id = $this->input->getInt('client_id');
            $count = $this->input->getInt('count');

            $this->ret['notice_saved'] = $this->tsl_model->save_notice_content($notice_id, $data, $count, $client_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function get_notice_answer()
    {
        $this->check_ajax();

        try {
            $client_id = $this->input->getInt('client_id');
            $notice_id = $this->input->getInt('notice_id');

            $this->ret['answer'] = $this->tsl_model->get_notice_answer($client_id, $notice_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function get_call_status()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();

            $this->ret['call_status'] = $this->tsl_model->get_call_status($user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function save_call_status()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $call_status = $this->input->getString('call_status');

            $this->ret['call_status_saved'] = $this->tsl_model->save_call_status($user_id, $call_status);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function update_call_status()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('pk');
            $value = $this->input->getString('value');

            $this->ret['call_status_updated'] = $this->tsl_model->update_call_status($id, $value);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function delete_call_status()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');

            $this->ret['call_status_deleted'] = $this->tsl_model->delete_call_status($id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function save_message_outline()
    {
        $this->check_ajax();

        try {
            $content = $this->input->get('content', '', 'RAW');
            $component = $this->input->getString('component');
            $user_id = $this->get_user_id();

            $this->ret['outline_saved'] = $this->tsl_model->save_message_outline($component, $content, $user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function get_client_phone()
    {
        $this->check_ajax();

        try {
            $client_id = $this->input->getInt('client_id');
            $this->ret['phones'] = $this->tsl_model->get_client_phone($client_id);

            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    public function save_client_phone()
    {
        $this->check_ajax();

        try {
            $client_id = $this->input->getInt('id');
            $type = $this->input->getString('type');
            $number = $this->input->get('number');

            $this->ret['save_phone'] = $this->tsl_model->save_client_phone($client_id, $type, $number);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    public function update_client_phone()
    {
        $this->check_ajax();

        try {
            $col = $this->input->getString('name');
            $val = $this->input->getString('value');
            $id = $this->input->getInt('pk');

            $this->ret['update_phone'] = $this->tsl_model->update_client_phone($col, $val, $id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    public function update_additional_phone()
    {
        $this->check_ajax();

        try {
            $value = $this->input->getString('value');
            $id = $this->input->getInt('pk');
            $col = $this->input->getString('name');

            $this->ret['update_phone'] = $this->tsl_model->update_additional_phone($col, $value, $id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    public function delete_additional_phone()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');

            $this->ret['phone_deleted'] = $this->tsl_model->delete_additional_phone($id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    public function begin_call_session()
    {
        $this->check_ajax();

        try {
            $tsl_id = $this->input->getInt('tsl_id');
            $day = $this->input->getString('day');
            $day_num = $this->input->getInt('day_numeric');
            $time = $this->input->get('time', '', 'RAW');
            $date = $this->input->getString('date');
            $month = $this->input->getInt('month');
            $seq = $this->input->getInt('seq');
            $duration = $this->input->getInt('duration');

            $this->ret['begin_call_session'] =
                $this->tsl_model->begin_call_session($tsl_id, $day, $day_num, $time, $date, $month, $seq, $duration);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    public function end_call_session()
    {
        $this->check_ajax();

        try {
            $session_id = $this->input->getInt('session_id');
            $time = $this->input->getString('time');

            $this->ret['session_ended'] = $this->tsl_model->end_call_session($session_id, $time);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    public function update_call_session()
    {
        $this->check_ajax();

        try {

            $sam_id = $this->input->getInt('session_id');
            $col_name = $this->input->getString('col_name');

            $this->ret['call_session_updated'] = $this->tsl_model->update_call_session($sam_id, $col_name);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    public function pause_restart_call_session()
    {
        $this->check_ajax();

        try {
            $session_id = $this->input->getInt('session_id');
            $val = $this->input->getString('val');

            $this->ret['call_session_paused_started'] = $this->tsl_model->pause_restart_call_session($session_id, $val);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    public function get_active_sam()
    {
        $this->check_ajax();

        try {
            $tsl_id = $this->input->getInt('tsl_id');

            $this->ret['active_sam'] = $this->tsl_model->get_active_sam($tsl_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    public function get_tsl_drop_down()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $active_product = $this->app->get_product();

            $this->ret['drop_down'] = $this->tsl_model->get_tsl_dropdown($user_id, $active_product);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    public function get_asset_criteria()
    {
        $this->check_ajax();
        try {
            $asset_id = $this->input->getInt('asset_id');

            $this->ret['asset_criteria'] = $this->tsl_model->get_asset_criteria($asset_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    public function update_archive_select()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $val = $this->input->getString('val');

            $this->ret['archive_selector_updated'] = $this->tsl_model->update_archive_select($id, $val);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    public function delete_archive()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');

            $this->ret['archive_deleted'] = $this->tsl_model->delete_archive($id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function delete_client_action()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $seq = $this->input->getInt('seq');

            $this->ret['action_deleted'] = $this->tsl_model->delete_client_action($id, $seq);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function remove_tsl_client()
    {

        $this->check_ajax();

        try {
            $tc_id = $this->input->getInt('tc_id');
            $asset_id = $this->input->getInt('asset_id');

            $this->ret['tsl_client_removed'] = $this->tsl_model->remove_tsl_client($tc_id, $asset_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }


    function delete_past_call()
    {
        $this->check_ajax();

        try {
            $tsl_id = $this->input->getInt('id');

            $this->ret['past_calls_deleted'] = $this->tsl_model->delete_past_call($tsl_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function update_scoreboard()
    {
        $this->check_ajax();

        try {
            $type = $this->input->getString('type');
            $user_id = $this->get_user_id();

            $this->ret['scoreboard_updated'] = $this->tsl_model->update_scoreboard($user_id, $type);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function get_call_scoreboard()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $when = $this->input->getString('when');

            $this->ret['call_scoreboard'] = $this->tsl_model->get_call_scoreboard($user_id, $when);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function get_confirm_data()
    {
        $this->check_ajax();
        try {
            $client_id = $this->input->getInt('client_id');

            $this->ret['confirm_data'] = $this->tsl_model->get_confirm_data($client_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function update_confirm_data()
    {
        $this->check_ajax();

        try {
            $client_id = $this->input->getInt('id');
            $col = $this->input->getString('col');
            $val = $this->input->get('val', '', 'RAW');
            $seq = $this->input->getInt('seq');

            $this->ret['confirm_data_updated'] = $this->tsl_model->update_confirm_data($client_id, $col, $val, $seq);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function delete_client_confirm()
    {
        $this->check_ajax();
        try {
            $client_id = $this->input->getInt('id');
            $seq = $this->input->getInt('seq');

            $this->ret['confirm_deleted'] = $this->tsl_model->delete_client_confirm($client_id, $seq);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function get_max_seq()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('id');

            $this->ret['max_seq'] = $this->tsl_model->get_max_seq($id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function update_active_tsl()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $user_id = $this->get_user_id();

            $this->ret['active_tsl_updated'] = $this->tsl_model->update_active_tsl($id, $user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function update_profile_questions()
    {
        $this->check_ajax();

        try {
            $question_id= explode(",", $this->input->get('question_id', '', 'RAW'));

            $this->ret['profile_question_updated'] =
                $this->tsl_model->update_profile_questions($question_id, $this->input);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function get_profile_data() {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $section = $this->input->getInt('section');

            $this->ret['profile_data'] = $this->tsl_model->get_profile_data($id, $section);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }

    function move_to_identified() {
        $this->check_ajax();

        try {
            $tac_id = $this->input->getInt('tac_id');
            $tc_id = $this->input->getInt('tc_id');
            $user_id = $this->get_user_id();

            $this->ret['deal_identified'] = $this->tsl_model->move_to_identified($tac_id, $tc_id, $user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo \json_encode($this->ret);
    }
}