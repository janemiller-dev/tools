<?php
/**
 * Tactical Game Designer controller.
 */

namespace Controller;

class Wap extends Controller
{

	/**
	 * Ensure that the user has access to this tool.
	 */
	function __construct($app)
	{

		parent::__construct($app);
	}

	/**
	 * Default method.
	 */
	function index()
	{
		$ret['render'] = 'views/wap/dashboard.php';
		$ret['data'] = $this->view_data;
		return $ret;
	}

	/**
	 * Show the specified model.
	 */
	function model()
	{
		$ret['render'] = 'views/wap/model.php';
		$ret['data'] = $this->view_data;
		return $ret;
	}

	/**
	 * Get the specified model.
	 */
	function get_model()
	{
		$this->check_ajax();
		try {

			// Get the inputs
			$id = $this->input->getInt('id');
			if (empty($id))
				throw new Exception("Invalid request. One or more inputs are missing.");

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Add the instance
			$wap_model = new \Model\WAP;
			$ret['model'] = $wap_model->get_model($user_id, $id);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Show the specified WAP.
	 */
	function wap()
	{
		$ret['render'] = 'views/wap/wap.php';
		$ret['data'] = $this->view_data;
		return $ret;
	}

	/**
	 * Show the specified WAP.
	 */
	function get_wap()
	{
		$this->check_ajax();
		try {

			// Get the inputs
			$id = $this->input->getInt('id');
			if (empty($id))
				throw new Exception("Invalid request. One or more inputs are missing.");

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Add the instance
			$wap_model = new \Model\WAP;
			$ret['wap'] = $wap_model->get_wap($user_id, $id);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Update the specified user instance.
	 */
	function update_instance()
	{
		$this->check_ajax();
		try {

			// Get the parameters.
			$id = $this->input->getRaw('pk');
			$name = $this->input->getText('name');
			$value = $this->input->getRaw('value');

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Update the cell.
			$wap_model = new \Model\WAP;
			$wap_model->update_instance($user_id, $id, $name, $value);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Update the specified cell.
	 */
	function update_cell()
	{
		$this->check_ajax();
		try {

			// Get the parameters.
			$id = $this->input->getRaw('pk');
			$name = $this->input->getText('name');
			$value = $this->input->getRaw('value');

			// Split the name into table name and column.
			list($ui_id, $metric_id, $period) = explode('-', $id);

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Update the cell.
			$wap_model = new \Model\WAP;
			$wap_model->update_cell($user_id, $ui_id, $metric_id, $period, $name, $value);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Update the specified muliplier.
	 */
	function update_multiplier()
	{
		$this->check_ajax();
		try {

			// Get the parameters.
			$id = $this->input->getRaw('pk');
			$name = $this->input->getText('name');
			$value = $this->input->getRaw('value');

			// Split the name into table name and column.
			list($ui_id, $multiplier_id, $period) = explode('-', $id);

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Update the cell.
			$wap_model = new \Model\WAP;
			$wap_model->update_multiplier($user_id, $ui_id, $multiplier_id, $period, $name, $value);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Update the specified metric.
	 */
	function update_metric()
	{
		$this->check_ajax();
		try {

			// Get the parwapeters.
			$id = $this->input->getRaw('pk');
			$name = $this->input->getText('name');
			$value = $this->input->getRaw('value');

			// Split the name into table name and column.
			list($ui_id, $metric_id) = explode('-', $id);

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Update the cell.
			$wap_model = new \Model\WAP;
			$wap_model->update_metric($user_id, $ui_id, $metric_id, $name, $value);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Add a new WAP model
	 */
	function add_model()
	{
		$this->check_ajax();
		try {

			// Get the inputs
			$type_id = $this->input->getInt('wap_type_id');
			$name = $this->input->getText('name');
			$description = $this->input->getText('description');
			if (empty($type_id) || empty($description) || empty($name))
				throw new Exception("Invalid request. One or more inputs are missing.");

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Add the model
			$wap_model = new \Model\WAP;
			$ret['model_id'] = $wap_model->add_model($user_id, $type_id, $name, $description);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Add a new WAP instance
	 */
	function add_instance()
	{
		$this->check_ajax();
		try {

			// Get the inputs
			$type_id = $this->input->getInt('wap_type_id');
			$name = $this->input->getText('name');
			$quarter = $this->input->getText('quarter');
			if (empty($type_id) || empty($quarter) || empty($name))
				throw new Exception("Invalid request. One or more inputs are missing.");

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Add the instance
			$wap_model = new \Model\WAP;
			$ret['instance_id'] = $wap_model->add_instance($user_id, $type_id, $name, $quarter);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Delete an TGD instance
	 */
	function delete_instance()
	{
		$this->check_ajax();
		try {

			// Get the inputs
			$ui_id = $this->input->getInt('wap_ui_id');
			if (empty($ui_id))
				throw new Exception("Invalid request. One or more inputs are missing.");

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Delete the instance
			$wap_model = new \Model\WAP;
			$ret['instance_id'] = $wap_model->delete_instance($user_id, $ui_id);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Get the user's WAP instances.
	 */
	function get_instances()
	{
		$this->check_ajax();
		try {
			// Get the current subscription status.
			$user_id = $this->get_user_id();
			$wap_model = new \Model\WAP;
			$ret['instances'] = $wap_model->get_instances($user_id);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Get the user's WAP models.
	 */
	function get_models()
	{
		$this->check_ajax();
		try {
			// Get the current subscription status.
			$user_id = $this->get_user_id();
			$wap_model = new \Model\WAP;
			$ret['models'] = $wap_model->get_models($user_id);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Get the WAP game types.
	 */
	function get_types()
	{
		$this->check_ajax();
		try {
			// Get the current subscription status.
			$wap_model = new \Model\WAP;
			$ret['types'] = $wap_model->get_types();
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

}