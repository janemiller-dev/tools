<?php

namespace Controller;

use function json_encode;
use function print_r;

class Code extends Controller
{

	/**
	 * Ensure that the user has access to this code.
	 */
	function __construct($app)
	{
		parent::__construct($app);
	}

	/**
	 * Default method.
	 */
	function index()
	{
		$ret['render'] = 'views/code/codes.php';
		$ret['data'] = $this->view_data;
		return $ret;
	}

	/**
	 * Add a new code type.
	 */
	public function add_code_type()
	{

		// Must be an ajax request.
		$this->check_ajax();

		try {

			// Get the parameters and check the requirements
			if ((($name = $this->input->getText('name')) === false) || empty($name))
				throw new \Exception('Invalid Request: Code Type name is not specified');

			if ((($abbrev = $this->input->getText('abbrev')) === false) || empty($abbrev))
				throw new \Exception('Invalid Request: Code Type abbreviation is not specified');

			if ((($description = $this->input->getText('description')) === false) || empty($description))
				throw new \Exception('Invalid Request: Code Type description is not specified');

			// Insert the code type
			$code_model = new \Model\Code;
			$ct_id = $code_model->add_code_type($name, $abbrev, $description);
			$ret['ct_id'] = $ct_id;
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Delete a code type
	 */
	public function delete_code_type()
	{

		// Must be an ajax request.
		$this->check_ajax();

		try {

			// Get the parameters and check the requirements
			if ((($ct_id = $this->input->getInt('ct_id', true)) === false) || empty($ct_id))
				throw new \Exception('Invalid Request: Code Type ID is not specified');

			$code_model = new \Model\Code;
			$code_model->delete_code_type($ct_id);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Get the code types along with their corresponding codes.
	 */
	public function get_code_types()
	{
		$this->check_ajax();

		try {
			// Get the list of codes from the code model.
			$code_model = new \Model\Code;
			$ret['code_types'] = $code_model->get_code_types();
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Add a new code.
	 */
	public function add_code()
	{

		// Must be an ajax request.
		$this->check_ajax();

		try {

			// Get the parameters and check the requirements
			if ((($name = $this->input->getText('name')) === false) || empty($name))
				throw new \Exception('Invalid Request: Code name is not specified');

			if ((($abbrev = $this->input->getText('abbrev')) === false) || empty($abbrev))
				throw new \Exception('Invalid Request: Code abbreviation is not specified');

			if ((($description = $this->input->getText('description')) === false) || empty($description))
				throw new \Exception('Invalid Request: Code description is not specified');

			if ((($ct_id = $this->input->getInt('ct_id')) === false) || empty($ct_id))
				throw new \Exception('Invalid Request: Code Type ID  is not specified');

			// Insert the code type
			$code_model = new \Model\Code;
			$code_id = $code_model->add_code($ct_id, $name, $abbrev, $description);
			$ret['code_id'] = $code_id;
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Delete a code
	 */
	public function delete_code()
	{

		// Must be an ajax request.
		$this->check_ajax();

		try {

			// Get the parameters and check the requirements
			if ((($code_id = $this->input->getInt('code_id', true)) === false) || empty($code_id))
				throw new \Exception('Invalid Request: Code ID is not specified');

			$code_model = new \Model\Code;
			$code_model->delete_code($code_id);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Update the code type or code field.
	 */
	public function update_field()
	{

		// Must be an ajax request.
		$this->check_ajax();

		try {

			// Get the parameters and check the requirements
			$id = $this->input->getRaw('pk');
			$name = $this->input->getText('name');
			$value = $this->input->getRaw('value');

			// Base on the name, determine the table name (code type or code)
			if (strpos($name, 'ct_') === 0)
				$table = 'code_type';
			else
				$table = 'code';

			// Update the field
			$common_model = new \Model\Common;
			if ($common_model->update_field($table, $name, $id, $value) === false)
				throw new \Exception('Error updating the field: ' . $name);

			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Get the specified codes from the codes table.
	 */
	public function get_codes($code_type)
	{
		// Must be an ajax request.
		$this->check_ajax();

		try {

			// We need the code_type.
			if (empty($code_type))
				throw new Exception('Invalid request: Code type not specified.');

			// Update the request.
			$code_model = new \Model\Code;
			$ret = array();

			if ('dummy' === $code_type) {

				$code_types = $this->input->get('codes');
				foreach ($code_types as $c_type => $abbr) {
					$codes = $code_model->get_codes($abbr);

					if ($codes === false)
						throw new Exception('Database Error: Could not get the codes.');

					// Need to format in the way expected by the editable select.
					foreach ($codes as $code)
						$ret['codes'][$abbr][] = $code;
				}
			}
			else {
				if (($codes = $code_model->get_codes($code_type)) === false)
					throw new Exception('Database Error: Could not get the codes.');
				// Need to format in the way expected by the editable select.
				foreach ($codes as $code)
					$ret[$code->code_abbrev] = $code->code_name;
			}
			$ret['status'] = 'success';
		} catch (Exception $e) {
			$ret['status'] = 'failure';
		}
		echo json_encode($ret);
	}

	public function get_components()
    {
        $this->check_ajax();
        try {
            $ret['components'] = $this->get_component();
            $ret['status'] = 'success';
        } catch (\Exception $e) {
            $ret['status'] = 'failure';
            $ret['message'] = $e->getMessage();
        }
        echo json_encode($ret);
    }
}