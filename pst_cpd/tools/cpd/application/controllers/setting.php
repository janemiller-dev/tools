<?php
/**
 * Setting controller.
 */

namespace Controller;

class Setting extends Controller
{

	var $app = null;

	/**
	 * Ensure that the user has access to this tool.
	 */
	function __construct($app)
	{
		$this->app = $app;
		parent::__construct($app);
	}

	/**
	 * Default method is to show the list of companies.
	 */
	function index()
	{
		$this->app->redirect('/setting/dashboard');
	}

	/**
	 * Show the custom fields screen.
	 */
	function custom_fields()
	{
		$ret['render'] = 'views/setting/custom_fields.php';
		$ret['data'] = $this->view_data;
		return $ret;
	}

	/**
	 * Get the list of custom field types.
	 */
	public function get_cf_types()
	{
		$this->check_ajax();

		try {
			// Get the list of companies from the company model.
			$setting_model = new \Model\Setting;
			$ret['cf_types'] = $setting_model->get_cf_types();
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['message'] = $e->getMessage();
			$ret['status'] = 'failure';
		}
		echo json_encode($ret);
	}

	/**
	 * Get the list of custom field definitions.
	 */
	public function get_cf_definitions()
	{
		$this->check_ajax();

		try {
			// Limit the search to the user's companies.
			$user_id = $this->get_user_id();

			// Get the list of custom fields from the setting model
			$setting_model = new \Model\Setting;
			$ret = $setting_model->get_cf_definitions($this->input, $user_id);

		} catch (\Exception $e) {
			$ret['error'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Add a new custom field type.
	 */
	public function add_cf_definition($user_id, $cf_def, $extra)
	{
		$this->check_ajax();

		try {
			// Get the inputs
			$cf_def['object_name'] = $this->input->getText('object_name');
			$cf_def['name'] = $this->input->getText('name');
			$cf_def['sys_name'] = $this->input->getText('sys_name');
			$cf_def['type'] = $this->input->getText('type');

			// Check that the required arguments have been provided.
			if (empty($cf_def['object_name']) || empty($cf_def['name']) || empty($cf_def['sys_name']) || empty($cf_def['type']))
				throw new Exception("Invalid request. One or more inputs are missing.");

			// Get the type specific extras.
			$extra = array();
			if ($cf_def['type'] == 'number') {
				$extra['number_dec'] = $this->input->getText('number_dec');
				$extra['number_min'] = $this->input->getText('number_min');
				$extra['number_max'] = $this->input->getText('number_max');
			}

			// Add the new setting.
			$setting_model = new \Model\Setting;
			$ret['cfd_id'] = $setting_model->add_cf_definition($this->get_user_id(), $cf_def, $extra);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['message'] = $e->getMessage();
			$ret['status'] = 'failure';
		}
		echo json_encode($ret);
	}
}
