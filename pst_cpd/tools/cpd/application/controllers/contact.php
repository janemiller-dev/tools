<?php
/**
 * Contact controller.
 */

namespace Controller;

class Contact extends Controller
{

	var $app = null;

	/**
	 * Ensure that the user has access to this tool.
	 */
	function __construct($app)
	{
		parent::__construct($app);
	}

	/**
	 * Default method.
	 */
	function index()
	{
		$ret['render'] = 'views/contact/dashboard.php';
		$ret['data'] = $this->view_data;
		return $ret;
	}

	/**
	 * Show the specified CONTACT.
	 */
	function contact()
	{
		$ret['render'] = 'views/contact/contact.php';
		$ret['data'] = $this->view_data;
		return $ret;
	}

	/**
	 * Show the specified CONTACT.
	 */
	function get_contact()
	{
		$this->check_ajax();
		try {

			// Get the inputs
			$id = $this->input->getInt('id');
			if (empty($id))
				throw new Exception("Invalid request. One or more inputs are missing.");

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Add the instance
			$contact_model = new \Model\CONTACT;
			$ret['contact'] = $contact_model->get_contact($user_id, $id);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Update the specified user instance.
	 */
	function update_instance()
	{
		$this->check_ajax();
		try {

			// Get the parameters.
			$id = $this->input->getRaw('pk');
			$name = $this->input->getText('name');
			$value = $this->input->getRaw('value');

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Update the cell.
			$contact_model = new \Model\CONTACT;
			$contact_model->update_instance($user_id, $id, $name, $value);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Update the specified cell.
	 */
	function update_cell()
	{
		$this->check_ajax();
		try {

			// Get the parameters.
			$id = $this->input->getRaw('pk');
			$name = $this->input->getText('name');
			$value = $this->input->getRaw('value');

			// Split the name into table name and column.
			list($ui_id, $condition_id, $period) = explode('-', $id);

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Update the cell.
			$contact_model = new \Model\CONTACT;
			$contact_model->update_cell($user_id, $ui_id, $condition_id, $period, $name, $value);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Update the target value.
	 */
	function adjust_target()
	{
		$this->check_ajax();
		try {

			// Get the parameters.
			$ui_id = $this->input->getRaw('ui');
			$period = $this->input->getText('period');
			$condition_id = $this->input->getRaw('condition_id');
			$direction = $this->input->getRaw('direction');

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Update the cell.
			$contact_model = new \Model\CONTACT;
			$contact_model->adjust_cell_target($user_id, $ui_id, $condition_id, $period, $direction);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Recalculate based on Q4 values, initial conditions and conversion rates
	 */
	function recalculate()
	{
		$this->check_ajax();
		try {

			// Get the parameters.
			$ui_id = $this->input->getRaw('ui_id');

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Update the cell.
			$contact_model = new \Model\CONTACT;
			$contact_model->recalculate($user_id, $ui_id);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Update the specified muliplier.
	 */
	function update_multiplier()
	{
		$this->check_ajax();
		try {

			// Get the parameters.
			$id = $this->input->getRaw('pk');
			$name = $this->input->getText('name');
			$value = $this->input->getRaw('value');

			// Split the name into table name and column.
			list($ui_id, $multiplier_id, $period) = explode('-', $id);

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Update the cell.
			$contact_model = new \Model\CONTACT;
			$contact_model->update_multiplier($user_id, $ui_id, $multiplier_id, $period, $name, $value);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Update the specified condition.
	 */
	function update_condition()
	{
		$this->check_ajax();
		try {

			// Get the parameters.
			$id = $this->input->getRaw('pk');
			$name = $this->input->getText('name');
			$value = $this->input->getRaw('value');

			// Split the name into table name and column.
			list($ui_id, $condition_id) = explode('-', $id);

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Update the cell.
			$contact_model = new \Model\CONTACT;
			$contact_model->update_condition($user_id, $ui_id, $condition_id, $name, $value);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Add a new CONTACT instance
	 */
	function add_instance()
	{
		$this->check_ajax();
		try {

			// Get the inputs
			$gt_id = $this->input->getInt('contact_gt_id');
			$name = $this->input->getText('name');
			$year = $this->input->getText('year');
			$mission = $this->input->getText('mission');
			if (empty($gt_id) || empty($year) || empty($name) || empty($mission))
				throw new Exception("Invalid request. One or more inputs are missing.");

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Add the instance
			$contact_model = new \Model\CONTACT;
			$ret['instance_id'] = $contact_model->add_instance($user_id, $gt_id, $name, $year, $mission);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Delete an CONTACT instance
	 */
	function delete_instance()
	{
		$this->check_ajax();
		try {

			// Get the inputs
			$ui_id = $this->input->getInt('contact_ui_id');
			if (empty($ui_id))
				throw new Exception("Invalid request. One or more inputs are missing.");

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Delete the instance
			$contact_model = new \Model\CONTACT;
			$ret['instance_id'] = $contact_model->delete_instance($user_id, $ui_id);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Copy a CONTACT instance to another instance.
	 */
	function copy_instance()
	{
		$this->check_ajax();
		try {

			// Get the inputs
			$contact_ui_id = $this->input->getText('contact_ui_id');
			$save_as_name = $this->input->getText('save_as_name');
			if ($save_as_name == '')
				$save_as_name = null;
			$save_as_id = $this->input->getText('save_as_id');
			if ($save_as_id == '0')
				$save_as_id = null;

			if (empty($contact_ui_id) && (empty($save_as_name) || empty($save_as_id)))
				throw new Exception("Invalid request. One or more inputs are missing.");

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Add the instance
			$contact_model = new \Model\CONTACT;
			$ret['instance_id'] = $contact_model->copy_instance($user_id, $contact_ui_id, $save_as_name, $save_as_id);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Update the number of periods finalized.
	 */
	function finalize_period()
	{
		$this->check_ajax();
		try {

			// Get the inputs
			$contact_ui_id = $this->input->getInt('id');
			if (empty($contact_ui_id))
				throw new Exception("Invalid request. One or more inputs are missing.");

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Add the instance
			$contact_model = new \Model\CONTACT;
			$contact_model->finalize_period($contact_ui_id, 1);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	function allow_period()
	{
		$this->check_ajax();
		try {

			// Get the inputs
			$contact_ui_id = $this->input->getInt('id');
			if (empty($contact_ui_id))
				throw new Exception("Invalid request. One or more inputs are missing.");

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Add the instance
			$contact_model = new \Model\CONTACT;
			$contact_model->finalize_period($contact_ui_id, -1);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Get the user's CONTACT instances.
	 */
	function get_instances()
	{
		$this->check_ajax();
		try {
			// Get the current subscription status.
			$user_id = $this->get_user_id();
			$contact_model = new \Model\CONTACT;
			$ret['instances'] = $contact_model->get_instances($user_id);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Get the CONTACT game types.
	 */
	function get_types()
	{
		$this->check_ajax();
		try {
			// Get the current subscription status.
			$contact_model = new \Model\CONTACT;
			$ret['game_types'] = $contact_model->get_types();
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Print a PDF version of the CONTACT.
	 */
	public function print_pdf($id)
	{

		// Get the inputs
		if (empty($id))
			$this->app->redirect('/contact');

		// Get the current subscription status.
		$user_id = $this->get_user_id();

		// Try to get the instance
		try {
			$contact_model = new \Model\CONTACT;
			$contact = $contact_model->get_contact($user_id, $id);
		} catch (\Exception $e) {
			$this->app->redirect('/contact');
		}

		// Create a new PDF document
		$pdf = new PST_PDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// Set the document information.
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Advisory Selling Tools');
		$pdf->SetTitle('Tactical Game Designer');
		$pdf->SetSubject('Tactical Game Designer User Instance');
		$pdf->SetKeywords('Roth Methods, Tactical Game Designer');

		$pdf->AddPage();

		// Set the default font
		$pdf->SetFont('helvetica', '', 12);

		// Show the instance name
		$pdf->SetXY(10, 30);
		$pdf->Cell(45, 10, 'Instance Name:', 0, 0, 'R', 0, '', 0, false, 'C', 'C');
		$pdf->Cell(0, 10, $contact->contact_ui_name, 0, 0, 'L', 0, '', 0, false, 'C', 'C');

		// Show the instance mission
		$pdf->SetXY(10, 35);
		$pdf->Cell(45, 10, 'Mission:', 0, 0, 'R', 0, '', 0, false, 'C', 'C');
		$pdf->Cell(0, 10, $contact->contact_ui_mission, 0, 0, 'L', 0, '', 0, false, 'C', 'C');

		// Show the instance objectives
		$pdf->SetXY(10, 40);
		$pdf->Cell(45, 10, 'Objectives:', 0, 0, 'R', 0, '', 0, false, 'C', 'C');
		$pdf->Cell(0, 10, 'Floor: $' . number_format($contact->contact_ui_floor, 0) . ' Target: $' . number_format($contact->contact_ui_target, 0) . ' Game: $' . number_format($contact->contact_ui_game, 0), 0, 0, 'L', 0, '', 0, false, 'C', 'C');

		$pdf->SetXY(10, 45);
		$pdf->Cell(45, 10, 'Check Size:', 0, 0, 'R', 0, '', 0, false, 'C', 'C');
		$pdf->Cell(0, 10, '$' . number_format($contact->contact_ui_default_check_size, 2), 0, 0, 'L', 0, '', 0, false, 'C', 'C');

		// Set the table font to 11pts
		$pdf->SetFont('helvetica', '', 11);

		// Draw the CONTACT Table.
		$tbl = '<table cellspacing="0" cellpadding="0" border="0">';

		// Draw the table header.
		$tbl .= '<thead><tr>';

		// Need a blank cell.
		$tbl .= '<th> </th>';

		// Draw the period date headers.
		$num_periods = count($contact->cells);
		$date = new \DateTime($contact->contact_ui_year . '-01-01');
		$date = $date->modify('-1 day');
		for ($period = 1; $period <= $num_periods; $period++) {

			// Determine the message based on the period.
			$q_msg = 'Set-Up Q1 to Q3';
			if ($period == 2)
				$q_msg = 'Strong Q1 Launch';
			else if ($period == 3)
				$q_msg = 'Make Your Floor by Q2';
			else if ($period == 4)
				$q_msg = 'Make Your Target by Q3';
			else if ($period == 5)
				$q_msg = 'Set-Up a Strong Q1';
			else if ($period == 6)
				$q_msg = 'Match Floor or Target in Q1';

			// Create the column header.
			$col_header = '<span style="font-size: .8em; font-style: italic;">' . $q_msg . '</span><br />';
			$col_header .= 'Q' . (round($date->format('n') / 4, 0) + 1) . ' ' . $date->format('Y') . '<br />';
			$col_header .= $date->format('M j, Y');

			// Get the multiplier name (if any);
			foreach ($contact->multipliers as $multiplier) {
				$col_header .= '<br /><span style="font-size: .7em;">' . $multiplier->contact_m_name . ': ';
				if ($multiplier->contact_m_type == 'dollar')
					$value = '$' . number_format($contact->mcells[$period][$multiplier->contact_m_id]->contact_im_multiple, 0);
				else if ($multiplier->contact_m_type == 'percent')
					$value = number_format(($contact->mcells[$period][$multiplier->contact_m_id]->contact_im_multiple * 100), 2) . '%';

				$col_header .= $value . '</span>';
			};

			$tbl .= '<th style="text-align: center;">' . $col_header . '</th>';

			// Increment to the next quarter
			$date = $date->modify('first day of this month');
			$date = $date->modify('+3 months');
			$date = $date->modify('last day of this month');
		}

		// Close the table header.
		$tbl .= '</tr></thead>';

		// Add the table body.
		$tbl .= '<tbody>';

		// Calculate and store all of the needed values.
		$row = 1;
		foreach ($contact->conditions as $condition) {
			$total_projected = 0;
			for ($period = 1; $period <= 6; $period++) {
				$needed = 0;
				if ($row == 1 && $period == 1)
					$needed = $contact->contact_ui_py_cum_target - $contact->cells[$period][$condition->contact_c_id]->contact_ic_actual;
				else if ($row == 1 && $period == 2) {
					$total_projected += $contact->cells[$period][$condition->contact_c_id]->contact_ic_actual;
					$needed = ($contact->contact_ui_floor / 2) - $total_projected;
				} else if ($row == 1 && $period == 3) {
					$total_projected += $contact->cells[$period][$condition->contact_c_id]->contact_ic_actual;
					$needed = $contact->contact_ui_floor - $total_projected;
				} else if ($row == 1 && $period == 4) {
					$total_projected += $contact->cells[$period][$condition->contact_c_id]->contact_ic_actual;
					$needed = $contact->contact_ui_target - $total_projected;
				} else if ($row == 1 && $period == 5) {
					$total_projected += $contact->cells[$period][$condition->contact_c_id]->contact_ic_actual;
					$needed = $contact->contact_ui_game - $total_projected;
				} else if ($row == 1 && $period == 6) {
					$total_projected = $contact->cells[$period][$condition->contact_c_id]->contact_ic_actual;
					$needed = $contact->contact_ui_floor - $total_projected;
				} else {
					$val = $contact->cells[$period + 1][$last_cond_id]->needed;
					$cr = $condition->contact_icond_conversion_rate;
					$cs = $contact->contact_ui_default_check_size;
					if ($cs == 0)
						$needed = 'N/A';
					else if ($row == 2)
						$val = $val / $cs;
					if ($cr == 0)
						$needed = 'N/A';
					else
						$needed = ceil($val / $cr);
				}

				$contact->cells[$period][$condition->contact_c_id]->needed = $needed;
			}
			$last_cond_id = $condition->contact_c_id;
			$row++;
		}

		// Create the rows and columns for each condition.
		$row = 1;
		$cond_num = 1;
		foreach ($contact->conditions as $condition) {
			$cond_value = '';

			// Indicate whether or this this is an objective or a condition.
			if ($condition->contact_c_type == 'obj')
				$cond_value .= '<span style="font-size: .8em">Milestones</span><br />';
			else {
				$cond_value .= '<span style="font-size: .8em">Condition ' . $cond_num . '</span><br />';
				$cond_num += 1;
			}

			// Each condition label contains the condition name and the converstion rate.
			$cond_value .= '<span style="font-size: .9em">' . $condition->contact_c_name . '</span><br />';

			// Show the conversion rate for conditions (not objectives).
			if ($condition->contact_c_type != 'obj') {
				$cond_value .= '<span style="font-size: .8em">Conversion Rate: ' . number_format(($condition->contact_icond_conversion_rate * 100), 0) . '%</span>';
			}

			// Create the data row and the condition cell.
			$tbl .= '<tr><td style="text-align: center">' . $cond_value . '</td>';

			// Create a cell for each condition's period and display the cell contents.
			for ($period = 1; $period <= 6; $period++) {
				$tbl .= '<td>' . $this->_displayCell($contact, $period, $condition, $row) . '</td>';
			}

			// Close the row.
			$tbl .= '</tr>';
			$row += 1;
		};

		// Close table body.
		$tbl .= '</tbody>';

		// Close the table.
		$tbl .= '</table>';

		// Show the table
		//$pdf->SetXY(10, 300);
		//$pdf->MultiCell(0, 10, $tbl, 0, '', 0, 1, '', '', true);

		// Write the table.
		$pdf->SetXY(10, 55);
		$pdf->writeHTML($tbl, true, false, false, false, '');

		// Print the file out
		$pdf->Output('contact.pdf', 'D');
	}

	/**
	 * Create the HTML for a cell of the CONTACT PDF.
	 */
	private function _displayCell($contact, $period, $condition, $row)
	{
		$cells = $contact->cells;
		$condition_id = $condition->contact_c_id;
		$data_cell = $cells[$period][$condition_id];

		// Set the label to be either Actual or Projected based on the number of periods finalized.
		$actual_label = 'A';
		if (($contact->contact_ui_periods_final + 1) < $period)
			$actual_label = 'P';

		// Create the cell contents.
		$cell = '';

		// Set the border color
		$color = 'black';
		$bg_color = '#ffffff';
		if (($period == 1 && $row == 4) || ($period == 2 && $row == 3) || ($period == 3 && $row == 2) || ($period == 4 && $row == 1) || ($period == 3 && $row == 4) || ($period == 4 && $row == 3) || ($period == 5 && $row == 2) || ($period == 6 && $row == 1)) {
			$color = 'black';
			$bg_color = '#e6e6ff';
		}

		// Create the table.
		$cell = '<table style="font-size: .6em; border-right: 1px solid ' . $color . '; border-left: 1px solid ' . $color . ';" >';

		// Objective cells have a total and different headings.
		if ($condition->contact_c_type == 'obj') {

			// Create the table header
			$cell .= '<thead style="border-top: 1px solid ' . $color . ';">';
			$cell .= '<tr style="background-color: #ededed">';
			$cell .= '<th style="text-align: center; border-top: 1px solid ' . $color . ';" colspan="4">FTG</th>';
			$cell .= '<th style="text-align: center; border-top: 1px solid ' . $color . ';" colspan="4">Amount</th>';
			$cell .= '<th style="text-align: center; border-top: 1px solid ' . $color . ';" colspan="4">Total</th>';
			$cell .= '</tr>';
			$cell .= '</thead>';

			// Calculate the cumulative values.
			$floor_total = $data_cell->contact_ic_floor;
			$target_total = $data_cell->contact_ic_target;
			$game_total = $data_cell->contact_ic_game;
			if ($period == 1) {
				$floor_total = $contact->contact_ui_py_cum_floor;
				$target_total = $contact->contact_ui_py_cum_target;
				$game_total = $contact->contact_ui_py_cum_game;
			} else {
				if ($period >= 3 && $period <= 5) {
					$floor_total = 0;
					$target_total = 0;
					$game_total = 0;
					for ($i = 2; $i <= $period; $i++) {
						$tmp_cell = $cells[$i][$condition_id];
						$floor_total += $tmp_cell->contact_ic_floor;
						$target_total += $tmp_cell->contact_ic_target;
						$game_total += $tmp_cell->contact_ic_game;
					}
				}
			}

			// Create the table body
			$cell .= '<tbody>';
			$cell .= '<tr style="background-color: ' . $bg_color . '">';
			$cell .= '<td style="text-align: center; border: none;" colspan="4">F</td>';
			$cell .= '<td style="text-align: center; border: none;" colspan="4">$' . number_format($data_cell->contact_ic_floor) . '</td>';
			$cell .= '<td style="text-align: center; border: none;" colspan="4">$' . number_format($floor_total) . '</td>';
			$cell .= '</tr>';

			$cell .= '<tr style="background-color: ' . $bg_color . '">';
			$cell .= '<td style="text-align: center; border: none;" colspan="4">T</td>';
			$cell .= '<td style="text-align: center; border: none;" colspan="4">$' . number_format($data_cell->contact_ic_target) . '</td>';
			$cell .= '<td style="text-align: center; border: none;" colspan="4">$' . number_format($target_total) . '</td>';
			$cell .= '</tr>';

			$cell .= '<tr style="background-color: ' . $bg_color . '">';
			$cell .= '<td style="text-align: center; border: none;" colspan="4">G</td>';
			$cell .= '<td style="text-align: center; border: none;" colspan="4">$' . number_format($data_cell->contact_ic_game) . '</td>';
			$cell .= '<td style="text-align: center; border: none;" colspan="4">$' . number_format($game_total) . '</td>';
			$cell .= '</tr>';
			$cell .= '</tbody>';

			// Create the table footer
			$cell .= '<tfoot>';
			$cell .= '<tr style="background-color: #ededed">';
			$cell .= '<td style="text-align: center; border-bottom: 1px solid ' . $color . ';" colspan="3">' . $actual_label . '</td>';
			$cell .= '<td style="text-align: center; border-bottom: 1px solid ' . $color . ';" colspan="3">$' . number_format($data_cell->contact_ic_actual) . '</td>';
			$cell .= '<td style="text-align: center; border-bottom: 1px solid ' . $color . ';" colspan="3">N</td>';
			$cell .= '<td style="text-align: center; border-bottom: 1px solid ' . $color . ';" colspan="3">$' . number_format($data_cell->needed) . '</td>';
			$cell .= '</tr>';
			$cell .= '</tfoot>';
		} else {

			// Create the table header
			$cell .= '<thead style="border-top: 1px solid ' . $color . ';">';
			$cell .= '<tr style="background-color: #ededed">';
			$cell .= '<th style="text-align: center; border-top: 1px solid ' . $color . ';" colspan="2">FTG</th>';
			$cell .= '<th style="text-align: center; border-top: 1px solid ' . $color . ';" colspan="2">Count</th>';
			$cell .= '</tr>';
			$cell .= '</thead>';

			// Create the table body
			$cell .= '<tbody>';
			$cell .= '<tr style="background-color: ' . $bg_color . '">';
			$cell .= '<td style="text-align: center; border: none;" colspan="2">F</td>';
			$cell .= '<td style="text-align: center; border: none;" colspan="2">' . number_format($data_cell->contact_ic_floor) . '</td>';
			$cell .= '</tr>';

			$cell .= '<tr style="background-color: ' . $bg_color . '">';
			$cell .= '<td style="text-align: center; border: none;" colspan="2">T</td>';
			$cell .= '<td style="text-align: center; border: none;" colspan="2">' . number_format($data_cell->contact_ic_target) . '</td>';
			$cell .= '</tr>';

			$cell .= '<tr style="background-color: ' . $bg_color . '">';
			$cell .= '<td style="text-align: center; border: none;" colspan="2">G</td>';
			$cell .= '<td style="text-align: center; border: none;" colspan="2">' . number_format($data_cell->contact_ic_game) . '</td>';
			$cell .= '</tr>';
			$cell .= '</tbody>';

			// Create the table footer
			$cell .= '<tfoot>';
			$cell .= '<tr style="background-color: #ededed">';
			$cell .= '<td style="text-align: center; border-bottom: 1px solid ' . $color . ';">' . $actual_label . '</td>';
			$cell .= '<td style="text-align: center; border-bottom: 1px solid ' . $color . ';">' . number_format($data_cell->contact_ic_actual) . '</td>';
			$cell .= '<td style="text-align: center; border-bottom: 1px solid ' . $color . ';">N</td>';
			$cell .= '<td style="text-align: center; border-bottom: 1px solid ' . $color . ';">' . number_format($data_cell->needed) . '</td>';
			$cell .= '</tr>';
			$cell .= '</tfoot>';

		}

		// Close the cell table
		$cell .= '</table>';
		return $cell;
	}
}
