<?php
/**
 * Collaboration Controller.
 *
 * General Collaboration controller that controls all the Collaboration requests
 *
 * @author Sumit K (sumitk@mindfiresolutions.com)
 *
 */

namespace Controller;

use function json_encode;
use function print_r;

class Collab extends Controller
{
    private $ret, $success_string, $status_string, $message_string, $render_string;

    /**
     * Ensure that the user has access to this tool.
     * @param $app
     */
    function __construct($app)
    {
        parent::__construct($app);
        $this->collab_model = new \Model\Collab;
        $this->success_string = 'success';
        $this->status_string = 'status';
        $this->message_string = 'message';
        $this->render_string = 'render';

        $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
    }

    /**
     * Default method.
     */
    function index()
    {
        $client_id = $this->input->getInt('id');
        $token = $this->input->get('token', '', 'RAW');
        $collab_id = $this->input->getInt('collab_id');
        $outline_id = $this->input->getInt('outline');
        $recipient = $this->input->getInt('r');

        // Checks if any field is empty and validates token.
        if (empty($token) || empty($collab_id) || !isset($outline_id)
            || empty($this->collab_model->validate_token($token, $client_id, $collab_id, $outline_id, $recipient))) {
            echo nl2br("Invalid Request!! \n Please Try again later!!");
            exit();
        }
        $ret['render'] = 'views/collab/collab.php';
        $ret['data'] = $this->view_data;
//        $this->send_receipt();
        return $ret;
    }

    function univ()
    {
        $id = $this->input->getInt('id');
        $token = $this->input->get('token', '', 'RAW');
        $recipient = $this->input->getInt('r');

        // Checks if any field is empty and validates token.
//        if (empty($client_id) || empty($token) || empty($collab_id) || !isset($outline_id)
//            || empty($this->collab_model->validate_token($token, $client_id, $collab_id, $outline_id, $recipient))) {
//            echo nl2br("Invalid Request!! \n Please Try again later!!");
//            exit();
//        }
        $ret['render'] = 'views/collab/univ_collab.php';
        $ret['data'] = $this->view_data;
//        $this->send_receipt();
        return $ret;
    }

    /**
     *
     * Sends Collaboration receipt to User.
     */
    function send_receipt()
    {
        // Send Email to user.
        $current_user = wp_get_current_user();
        $user_name = $current_user->data->user_nicename;
        $user_email = $current_user->data->user_email;
//        header('Content-Type: text/plain');
        $new = htmlspecialchars(htmlspecialchars(
            '<p style="text-align: center">This is a Return Reciept for Collaboration.!!'
            . ' Please Visit Collaboration Page.<br/>'
            . ' <h4 style="text-align: center">Note: This return reciept only'
            . ' acknowledges collaborator visited collaboration page.</h4></p>', ENT_QUOTES));
        $msg_body = shell_exec('php ' . __dir__ . '/../views/tsl/email_body.php \'' . $new . '\' ' . $user_name);

        $transporter = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
            ->setUsername($this->app->get_email_credentials('username'))
            ->setPassword($this->app->get_email_credentials('password'));

        $mailer = \Swift_Mailer::newInstance($transporter);

        // Construct the Yes message.
        $message = \Swift_Message::newInstance()
            ->setSubject("Collaboration Complete.")
            ->setContentType('text/html')
            ->setTo($user_email)
            ->setFrom(array($current_user->data->user_email => $user_name))
            ->setBody($msg_body, 'text/html');

        $mailer->send($message);
    }


    // Fetches Collaboration Details for the User.
    function get_collab_details()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $id = $this->input->getInt('id');

            $this->ret['collab_details'] = $this->collab_model->get_collab_details($user_id, $id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail', $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Saves Collaboration data.
     */
    function save_collab_data()
    {
        $this->check_ajax();

        try {
            $type = $this->input->getString('type');
            $instance_id = $this->input->getInt('instance_id');
            $ques_id = $this->input->getInt('ques_id');
            $order = $this->input->getInt('order');
            $content = $this->input->get('content', '', 'RAW');
            $sender = $this->input->getString('sender');
            $recipient = $this->input->getString('recipient');
            $action = $this->input->getString('action');
            $date = $this->input->get('date', '', 'RAW');
            $time = $this->input->get('time', '', 'RAW');
            $category = $this->input->getString('category');
            $collaboration_id = $this->input->getInt('collaboration_id');

            $this->ret['collab_data_saved'] = $this->collab_model->save_collab_data($instance_id, $ques_id, $type,
                $order, $content, $sender, $recipient, $action, $date, $time, $category, $collaboration_id);

            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail', $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Returns a list of collaboration for a tool.
     */
    function get_collab_data()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $type = $this->input->getString('type');
            $category = $this->input->getString('category');
            $recipient = $this->input->getString('filter_recipient');
            $action = $this->input->getString('filter_action');

            $this->ret['collab_data'] =
                $this->collab_model->get_collab_data($id, $type, $category, $recipient, $action);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail', $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }

        echo json_encode($this->ret);
    }

    /**
     *
     * Updates Content for Collaboration
     */
    function update_collab_data()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $content = $this->input->get('content', '', 'RAW');

            $this->ret['collab_data_updated'] = $this->collab_model->update_collab_data($id, $content);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail', $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    function send_collab()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $tool_id = $this->input->getInt('tool_id');
            $outline_id = $this->input->getInt('outline_id');
            $recipient = $this->input->getInt('recipient_id');
            $type = $this->input->getString('type');
            $user_email = $this->input->get('user_email', '', 'RAW');
            $token = $this->input->get('token', '', 'RAW');

            if ($type !== 'universal')
                $this->ret['token_added'] = $this->collab_model->add_collab_token
                ($id, $tool_id, $token, $outline_id, $recipient, $type, $user_email);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail', $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    function get_script_outline()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('outline_id');

            $this->ret['script_outline'] = $this->collab_model->get_script_outline($id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail', $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    function get_collaborator_details()
    {
        $this->check_ajax();

        try {
            $collab_id = $this->input->getInt('collab_id');
            $user_id = $this->get_user_id();

            $this->ret['collaborator_details'] = $this->collab_model->get_collaborator_details($collab_id, $user_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);

        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail', $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     * Fetches script answers.
     */
    function get_script_ans()
    {
        $this->check_ajax();
        try {
            $instance_id = $this->input->getInt('script_id');
            $type = $this->input->get('type');

            $this->ret['answer'] = $this->collab_model->get_script_ans($instance_id, $type);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail', $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);

    }

    function save_recipient()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $name = $this->input->getString('name');

            $this->ret['recipient_saved'] = $this->collab_model->save_recipient($name, $user_id);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail', $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function get_component_info()
    {
        $this->check_ajax();

        try {
            $collab_id = $this->input->getInt('collab_id');

            $this->ret['component_info'] = $this->collab_model->get_component_info($collab_id);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail', $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function get_page_outline()
    {
        $this->check_ajax();

        try {
            $outline_id = $this->input->getInt('outline_id');

            $this->ret['script_outline'] = $this->collab_model->get_page_outline($outline_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail', $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);

    }

    function get_message_outline()
    {
        $this->check_ajax();

        try {
            $outline_id = $this->input->getInt('outline_id');

            $this->ret['script_outline'] = $this->collab_model->get_message_outline($outline_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail', $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);

    }

    function get_collab_info()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $recipient = $this->input->getInt('recipient');

            $this->ret['collab_info'] = $this->collab_model->get_collab_info($id, $recipient);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail', $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function get_univ_collab_details()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $user_id = $this->get_user_id();

            $this->ret['collab_data'] = $this->collab_model->get_collab_details($user_id, $id);
            $this->ret['univ_collab'] = $this->collab_model->get_univ_collab_details($id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail', $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function add_collab_reply()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $format = $this->input->getInt('format');
            $sender = $this->input->getString('sender');
            $date_time = $this->input->get('date_time', '', 'RAW');
            $email = $this->input->get('email', '', 'RAW');

            $email_text = $sender . ' has responded to you collaboration request.'
                . ' Please login to Advisory Selling Tools. To check response';

            $transporter = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
                ->setUsername($this->app->get_email_credentials('username'))
                ->setPassword($this->app->get_email_credentials('password'));

            $message = \Swift_Message::newInstance()
                ->setSubject('Collaboration Response')
                ->setContentType('text/html')
                ->setTo($email)
                ->setFrom(array($email => $sender));

            $msg_body = '<!DOCTYPE html><html><head><title>Page Title</title></head><body>'
                . '<div style="text-align: center">' . $email_text . '</div></body></html>';

            // Log Mailer instance.
            $this->app->log->debug("getting mailer");
            $mailer = \Swift_Mailer::newInstance($transporter);

            // Construct the message.
            $message->setBody($msg_body, 'text/html');
            $mailer->send($message);

            $this->ret['reply_added'] = $this->collab_model->add_collab_reply($id, $format, $sender, $date_time, $email);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail', $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function delete_collab()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('id');

            $this->ret['collab_deleted'] = $this->collab_model->delete_collab($id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail', $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function get_collab_response_data()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('collab_id');

            $this->ret['response'] = $this->collab_model->get_collab_response_data($id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail', $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function send_collab_response()
    {
        $this->check_ajax();

        try {
            $sender = $this->input->getString('sender');
            $recipient = $this->input->getString('recipient_id');
            $email_array = $this->input->get('client_email', '', 'RAW');
            $subject = $this->input->getString('subject');
            $email_text = $this->input->getString('text');


//            $email_text = 'Click to Join Collaboration';

            $transporter = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
                ->setUsername($this->app->get_email_credentials('username'))
                ->setPassword($this->app->get_email_credentials('password'));

            $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setContentType('text/html')
                ->setTo($email_array)
                ->setFrom(array('email@advisorysellingtools.com' => $sender));

            $msg_body = '<!DOCTYPE html><html><head><title>Page Title</title></head><body>' .
                '<div style="text-align: center">' .
                "This Email is a response to your collaboration request sent to $sender <br>" .
                $email_text . '</div></body></html>';

            // Log Mailer instance.
            $this->app->log->debug("getting mailer");
            $mailer = \Swift_Mailer::newInstance($transporter);

            // Construct the message.
            $message->setBody($msg_body, 'text/html');
            $mailer->send($message);

            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail', $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Fetches the list of default Sequence available
     */
    function get_default_seq()
    {
        $this->check_ajax();
        try {
            $seq = $this->input->getInt('seq');

            $this->ret['default_answers'] = $this->collab_model->get_default_seq($seq);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Saves Collaboration Text.
     */
    function save_collab_text()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $val = $this->input->get('val', '', 'RAW');

            $this->ret['collab_text_saved'] = $this->collab_model->save_collab_text($id, $val);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

}
