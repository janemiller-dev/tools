<?php
/**
 * Tactical Game Designer controller.
 */

namespace Controller;

use function array_fill;
use function count;
use function gettype;
use function json_encode;
use function print_r;

class Tgd extends Controller
{

    var $app = null;
    private $tgd_model;
    private $ret;


    /**
     * Ensure that the user has access to this tool.
     */
    function __construct($app)
    {
        parent::__construct($app);
        $this->tgd_model = new \Model\TGD;
        $this->ret['status'] = $this->app->get_status('success');

    }

    /**
     * Default method.
     */
    function index()
    {
        $this->ret['render'] = 'views/tgd/dashboard.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     * Show the specified TGD.
     */
    function tgd()
    {
        $this->ret['render'] = 'views/tgd/tgd.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     * Show the specified group.
     */
    function group()
    {
        $this->ret['render'] = 'views/tgd/group.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     * Show the specified TGD.
     */
    function get_tgd()
    {
        $this->check_ajax();

        try {

            // Get the inputs
            $id = $this->input->getInt('id');
            $year = $this->input->getInt('year');

            if (empty($id)) {
                throw new \Exception("Invalid request. One or more inputs are missing.");
            }

            // Get the current subscription status.
            $user_id = $this->get_user_id();

            // Add the instance
            $this->ret['tgd'] = $this->tgd_model->get_tgd($user_id, $id, $year);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\UnexpectedValueException $e) {
            echo $e->getMessage();
            exit();
            $this->app->log->debug($e->getMessage());
            $this->ret['redirect'] = '/tools/tgd';
            $this->ret['status'] = $this->app->get_status('redirect');
        } catch (\Exception $e) {
            $this->app->log->debug($e->getMessage());
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'An error Occured While Loading the page, Please try again Later';
        }
        echo json_encode($this->ret);
    }

    /**
     * Show the specified Group.
     */
    function get_group()
    {
        $this->check_ajax();

        try {

            // Get the inputs
            $id = $this->input->getInt('id');
            if (empty($id)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }

            // Get the current subscription status.
            $user_id = $this->get_user_id();

            // Add the instance
            $this->ret['group'] = $this->tgd_model->get_group($user_id, $id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Show the specified Game Type
     */
    function game_type()
    {
        $this->ret['render'] = 'views/tgd/game_type.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     * Show the specified game type.
     */
    function get_game_type()
    {
        $this->check_ajax();

        try {

            // Get the inputs
            $id = $this->input->getInt('id');
            if (empty($id)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }

            // Add the instance

            $this->ret['game_type'] = $this->tgd_model->get_game_type($id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Update the specified user instance.
     */
    function update_instance()
    {
        $this->check_ajax();
        try {

            // Get the parameters.
            $id = $this->input->getRaw('pk');
            $name = $this->input->getText('name');
            $value = $this->input->getRaw('value');

            // Update the cell.
            $this->tgd_model->update_instance($id, $name, $value);

            // Perform a recalculation of the objectives if the main objectives or check size has changed.
            ($name == 'tgd_ui_floor' || $name == 'tgd_ui_target' || $name == 'tgd_ui_game' || $name == 'tgd_ui_default_check_size') ?
                $this->tgd_model->update_objectives($id) : '';

            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Update the specified cell.
     */
    function update_cell()
    {
        $this->check_ajax();
        try {
            // Get the parameters.
            $id = $this->input->getRaw('pk');
            $name = $this->input->getText('name');
            $value = $this->input->getRaw('value');

            // Split the name into table name and column.
            list($ui_id, $condition_id, $period) = explode('-', $id);

            // Update the cell.
            $this->tgd_model->update_cell($ui_id, $condition_id, $period, $name, $value);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Update the target value.
     */
    function adjust_target()
    {
        $this->check_ajax();
        try {

            // Get the parameters.
            $ui_id = $this->input->getRaw('ui');
            $period = $this->input->getText('period');
            $condition_id = $this->input->getRaw('condition_id');
            $direction = $this->input->getRaw('direction');

            // Update the cell.
            $this->tgd_model->adjust_cell_target($ui_id, $condition_id, $period, $direction);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }


    /**
     * Undo last made toggle.
     *
     */
    function undo_toggle()
    {
        $this->check_ajax();

        try {

            $ui_id = $this->input->getInt('ui');
            $this->ret['undo_toggle'] = $this->tgd_model->undo_toggle($ui_id);

            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    function redo_toggle()
    {
        $this->check_ajax();

        try {
            $ui_id = $this->input->getInt('ui');
            $this->ret['redo_toggle'] = $this->tgd_model->redo_toggle($ui_id);

            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Recalculate based on Q4 values, initial conditions and conversion rates
     */
    function recalculate()
    {
        $this->check_ajax();
        try {

            // Get the parameters.
            $ui_id = $this->input->getRaw('ui_id');
            $locked_quarter = $this->input->getInt('locked_quarter');

            // Is there a save?
            $save_as_name = $this->input->getText('save_as_name');

            ($save_as_name == '') ? $save_as_name = null : '';

            $save_as_id = $this->input->getText('save_as_id');

            ($save_as_id == '0') ? $save_as_id = null : '';

            // Get the current subscription status.
            $user_id = $this->get_user_id();

            // Save the current version?
            (!empty($ui_id) && (!empty($save_as_name) || !empty($save_as_id))) ?
                $this->ret['instance_id'] = $this->tgd_model->copy_instance($ui_id, $save_as_name, $save_as_id) : '';

            // Update the cell.
            $this->tgd_model->recalculate($ui_id, $locked_quarter);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    function recalc_all()
    {
        // perform a recalculate on all instances.
        $this->tgd_model->recalc_all();
        echo "Done!";
    }

    /**
     * Update the specified muliplier.
     */
    function update_multiplier()
    {
        $this->check_ajax();
        try {

            // Get the parameters.
            $id = $this->input->getRaw('pk');
            $name = $this->input->getText('name');
            $value = $this->input->getRaw('value');

            // Split the name into table name and column.
            list($ui_id, $multiplier_id, $period) = explode('-', $id);

            // Update the cell.
            $this->tgd_model->update_multiplier($ui_id, $multiplier_id, $period, $name, $value);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Update the specified condition.
     */
    function update_condition()
    {
        $this->check_ajax();
        try {
            // Get the parameters.
            $id = $this->input->getRaw('pk');
            $name = $this->input->getText('name');
            $value = $this->input->getRaw('value');
            $locked = $this->input->getInt('locked');

            // Split the name into table name and column.
            list($ui_id, $condition_id) = explode('-', $id);

            // Update the cell.
            $this->tgd_model->update_condition($ui_id, $condition_id, $name, $value, $locked);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    function update_condition_text()
    {
        $this->check_ajax();

        try {
            //Get Inputs.
            $id = $this->input->getInt('id');
            $value = $this->input->getString('value');
            $year = $this->input->getInt('year');

            $this->ret['condition_text_updated'] = $this->tgd_model->update_condition_text($id, $value, $year);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Add a new TGD instance
     */
    function add_instance()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $gt_id = $this->input->getInt('tgd_gt_id');
            $name = $this->input->getText('name');
            $year = $this->input->getText('year');
            $mission = $this->input->getText('mission');
            $condition = $this->input->getText('condition');
            $product_id = $this->app->get_product();

            // Check if CPD if is zero set it to  null.
            $cpd_id = (0 === $this->input->getInt('cpd') ? null : $this->input->getInt('cpd'));

            if (empty($gt_id) || empty($year) || empty($name) || empty($mission) || empty($condition)) {
                throw new \Exception("Invalid request. One or more inputs are missing.");
            }

            // Get the current subscription status.
            $user_id = $this->get_user_id();

            // Add the instance
            $this->ret['instance_id'] = $this->tgd_model->add_instance($user_id, $gt_id, $name, $year,
                $mission, $cpd_id, $product_id, $condition);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Delete an TGD instance
     */
    function delete_instance()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $ui_id = $this->input->getInt('tgd_ui_id');
            if (empty($ui_id)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }

            // Delete the instance
            $this->ret['instance_id'] = $this->tgd_model->delete_instance($ui_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Copy a TGD instance to another instance.
     */
    function copy_instance()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $tgd_ui_id = $this->input->getText('tgd_ui_id');
            $save_as_name = $this->input->getText('save_as_name');

            ($save_as_name == '') ? $save_as_name = null : '';
            $save_as_id = $this->input->getText('save_as_id');

            ($save_as_id == '0') ? $save_as_id = null : '';

            if (empty($tgd_ui_id) && (empty($save_as_name) || empty($save_as_id))) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }

            // Get the current subscription status.
            $user_id = $this->get_user_id();

            // Add the instance
            $this->ret['instance_id'] = $this->tgd_model->copy_instance($tgd_ui_id, $save_as_name, $save_as_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Update the number of periods finalized.
     */
    function finalize_period()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $tgd_ui_id = $this->input->getInt('id');
            if (empty($tgd_ui_id)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }

            // Add the instance
            $this->tgd_model->finalize_period($tgd_ui_id, 1);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\UnexpectedValueException $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    function lock_period()
    {
        $this->check_ajax();

        try {
            $tgd_ui_id = $this->input->getInt('id');
            $leap = $this->input->get('leap') === 'true' ? true : false;

            $year = $this->input->getInt('year');
            $user_id = $this->get_user_id();

            if (empty($tgd_ui_id)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }

            $this->ret['tgd_data'] = $this->tgd_model->lock_period($tgd_ui_id, $leap, $year, $user_id, 1);
            $this->ret['status'] = $this->app->get_status('success');

        } catch (Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->debug($e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    function leap_tgd()
    {
        $this->check_ajax();

        try {
            $tgd_id = $this->input->getInt('id');
            $year = $this->input->getInt('year');
            $user_id = $this->get_user_id();

            $this->ret['data'] = $this->tgd_model->leap_tgd($tgd_id, $year, $user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->debug($e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }


    /**
     * Allow a pervious period to be re-opened.
     */
    function allow_period()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $tgd_ui_id = $this->input->getInt('id');
            if (empty($tgd_ui_id)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }

            // Add the instance
            $this->tgd_model->finalize_period($tgd_ui_id, -1);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Propagate an instances initial conditions FTG values.
     */
    function propagate_ftg()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $tgd_ui_id = $this->input->getInt('id');
            $period = $this->input->getInt('period');
            if (empty($tgd_ui_id) || empty($period)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }

            // Add the instance
            $this->tgd_model->propagate_ftg($tgd_ui_id, $period);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Get the user's TGD instances.
     */
    function get_instances()
    {
        $this->check_ajax();
        try {
            // Is there a "not in" argument
            $not_in_group = $this->input->getInt('not_in');
            $tgd_id = $this->input->getInt('tgd_id');
            $other_income = $this->input->getInt('other_income');

            // Get inputs.
            $year = $this->input->getInt('year');
            $product_id = $this->input->get('product_id');

            // Get the instances
            $user_id = $this->get_user_id();
            $this->ret['instances'] = $this->tgd_model->get_instances($user_id, $year, $product_id, $not_in_group, $tgd_id, $other_income);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Get the user's TGD instances.
     */
    function search_instances()
    {
        $this->check_ajax();
        try {
            // Get the current subscription status.
            $user_id = $this->get_user_id();
            $product_id = $this->input->getInt('product_id');

            $this->ret = $this->tgd_model->search_instances($user_id, $this->input, $product_id);
        } catch (\Exception $e) {
            $this->ret['error'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Get the user's TGD groups.
     */
    function search_groups()
    {
        $this->check_ajax();
        try {
            // Get the current subscription status.
            $user_id = $this->get_user_id();
            $this->ret = $this->tgd_model->search_groups($user_id, $this->input);
        } catch (\Exception $e) {
            $this->ret['error'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Add a new TGD group
     */
    function add_group()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $gt_id = $this->input->getInt('tgd_gt_id');
            $name = $this->input->getText('name');
            $year = $this->input->getText('year');
            $description = $this->input->getText('description');
            if (empty($gt_id) || empty($year) || empty($name) || empty($description)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }

            // Get the current subscription status.
            $user_id = $this->get_user_id();

            // Add the group
            $this->ret['group_id'] = $this->tgd_model->add_group($user_id, $gt_id, $name, $year, $description);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Delete an TGD group
     */
    function delete_group()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $g_id = $this->input->getInt('tgd_g_id');
            if (empty($g_id)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }

            // Delete the group
            $this->ret['group_id'] = $this->tgd_model->delete_group($g_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Add an instance from to group
     */
    function add_group_instance()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $g_id = $this->input->getInt('tgd_g_id');
            $ui_id = $this->input->getInt('tgd_ui_id');
            if (empty($ui_id) || empty($g_id)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }

            // Delete the group
            $this->tgd_model->add_group_instance($g_id, $ui_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Remove an instance from a group
     */
    function remove_group_instance()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $g_id = $this->input->getInt('tgd_g_id');
            $ui_id = $this->input->getInt('tgd_ui_id');
            if (empty($ui_id)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }

            // Delete the group
            $this->tgd_model->delete_group_instance($g_id, $ui_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Get the TGD game types.
     */
    function get_types()
    {
        $this->check_ajax();
        try {
            // Get the current subscription status.
            $this->ret['game_types'] = $this->tgd_model->get_types();
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Get list of Sources for other income.
     */
    function get_source()
    {
        $this->check_ajax();

        try {

            // Get the current subscription status.
            $user_id = $this->get_user_id();

            // Fetch the list pof sources.
            $this->ret['sources'] = $this->tgd_model->get_source($user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    function add_income_source()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $val = $this->input->getString('name');

            // Add a new other income.
            $this->ret['source_added'] = $this->tgd_model->add_income_source($val, $user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }


    /**
     * Add new other income for a TGD instance quarter.
     */
    function add_other_income()
    {
        $this->check_ajax();

        try {
            // Get inputs
            $type = $this->input->get('oi_type');
            $name = $this->input->getString('name');
            $amount = $this->input->get('amount');
            $qtr = $this->input->getInt('qtr');
            $tgd_id = $this->input->getInt('tgd_id');

            // Add a new other income.
            $this->ret['income_added'] = $this->tgd_model->add_other_income($type, $name, $amount, $qtr, $tgd_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Get the list of other income for a particular quarter.
     */
    function get_other_income()
    {
        $this->check_ajax();

        try {

            // Get the inputs
            $ui_id = $this->input->getInt('ui_id');
            $qtr = $this->input->get('qtr');
            $user_id = $this->get_user_id();
            $product_id = $this->app->get_product();

            $this->ret['other_income'] = $this->tgd_model->get_other_income($ui_id, $qtr, $user_id, $product_id);
            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    function delete_other_income()
    {
        $this->check_ajax();
        try {
            $oi_id = $this->input->getInt('oi_id');

            $this->ret['other_income_deleted'] = $this->tgd_model->delete_other_income($oi_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Print a PDF version of the TGD.
     */
    public function print_pdf($id)
    {

        // Get the inputs
        if (empty($id)) {
            $this->app->redirect('/tgd');
        }

        // Get the current subscription status.
        $user_id = $this->get_user_id();

        // Try to get the instance
        try {
            $tgd = $this->tgd_model->get_tgd($user_id, $id);
        } catch (\Exception $e) {
            $this->app->redirect('/tgd');
        }

        // Create a new PDF document
        $pdf = new PST_PDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // Set the document information.
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Power Selling Tools');
        $pdf->SetTitle('Tactical Game Designer');
        $pdf->SetSubject('Tactical Game Designer User Instance');
        $pdf->SetKeywords('Roth Methods, Tactical Game Designer');

        $pdf->AddPage();

        // Set the default font
        $pdf->SetFont('helvetica', '', 12);

        // Show the instance name
        $pdf->SetXY(10, 30);
        $pdf->Cell(45, 10, 'Instance Name:', 0, 0, 'R', 0, '', 0, false, 'C', 'C');
        $pdf->Cell(0, 10, $tgd->tgd_ui_name, 0, 0, 'L', 0, '', 0, false, 'C', 'C');

        // Show the instance mission
        $pdf->SetXY(10, 35);
        $pdf->Cell(45, 10, 'Mission:', 0, 0, 'R', 0, '', 0, false, 'C', 'C');
        $pdf->Cell(0, 10, $tgd->tgd_ui_mission, 0, 0, 'L', 0, '', 0, false, 'C', 'C');

        // Show the instance objectives
        $pdf->SetXY(10, 40);
        $pdf->Cell(45, 10, 'Objectives:', 0, 0, 'R', 0, '', 0, false, 'C', 'C');
        $pdf->Cell(0, 10, 'Floor: $' . number_format($tgd->tgd_ui_floor, 0) . ' Target: $' . number_format($tgd->tgd_ui_target, 0) . ' Game: $' . number_format($tgd->tgd_ui_game, 0), 0, 0, 'L', 0, '', 0, false, 'C', 'C');

        $pdf->SetXY(10, 45);
        $pdf->Cell(45, 10, 'Check Size:', 0, 0, 'R', 0, '', 0, false, 'C', 'C');
        $pdf->Cell(0, 10, '$' . number_format($tgd->tgd_ui_default_check_size, 2), 0, 0, 'L', 0, '', 0, false, 'C', 'C');

        // Set the table font to 11pts
        $pdf->SetFont('helvetica', '', 11);

        // Draw the TGD Table.
        $tbl = '<table cellspacing="0" cellpadding="0" border="0">';

        // Draw the table header.
        $tbl .= '<thead><tr>';

        // Need a blank cell.
        $tbl .= '<th> </th>';

        // Draw the period date headers.
        $num_periods = count($tgd->cells);
        $date = new \DateTime($tgd->tgd_ui_year . '-01-01');
        $date = $date->modify('-1 day');
        for ($period = 1; $period <= $num_periods; $period++) {

            // Determine the message based on the period.
            $percent = $tgd->tgd_ui_tgt_q1_pct;
            $objective = $tgd->tgd_ui_tgt_q1_obj;
            if ($period == 2) {
                $percent = $tgd->tgd_ui_tgt_q2_pct;
                $objective = $tgd->tgd_ui_tgt_q2_obj;
            } else if ($period == 3) {
                $percent = $tgd->tgd_ui_tgt_q3_pct;
                $objective = $tgd->tgd_ui_tgt_q3_obj;
            } else if ($period == 4) {
                $percent = $tgd->tgd_ui_tgt_q4_pct;
                $objective = $tgd->tgd_ui_tgt_q4_obj;
            } else if ($period == 5) {
                $percent = $tgd->tgd_ui_tgt_q5_pct;
                $objective = $tgd->tgd_ui_tgt_q5_obj;
            } else if ($period == 6) {
                $percent = $tgd->tgd_ui_tgt_q6_pct;
                $objective = $tgd->tgd_ui_tgt_q6_obj;
            }

            // Create the column header.
            $col_header = '<span style="font-size: .8em; font-style: italic;">Make ' . $percent . '% of ' . ucfirst($objective) . '</span><br />';
            $col_header .= 'Q' . (round($date->format('n') / 4, 0) + 1) . ' ' . $date->format('Y') . '<br />';
            $col_header .= $date->format('M j, Y');

            // Get the multiplier name (if any)
            foreach ($tgd->multipliers as $multiplier) {
                $col_header .= '<br /><span style="font-size: .7em;">' . $multiplier->tgd_m_name . ': ';
                if ($multiplier->tgd_m_type == 'dollar') {
                    $value = '$' . number_format($tgd->mcells[$period][$multiplier->tgd_m_id]->tgd_im_multiple, 0);
                } else if ($multiplier->tgd_m_type == 'percent') {
                    $value = number_format(($tgd->mcells[$period][$multiplier->tgd_m_id]->tgd_im_multiple * 100), 2) . '%';
                }

                $col_header .= $value . '</span>';
            }

            $tbl .= '<th style="text-align: center;">' . $col_header . '</th>';

            // Increment to the next quarter
            $date = $date->modify('first day of this month');
            $date = $date->modify('+3 months');
            $date = $date->modify('last day of this month');
        }

        // Close the table header.
        $tbl .= '</tr></thead>';

        // Add the table body.
        $tbl .= '<tbody>';

        // Calculate and store all of the needed values.
        $row = 1;
        foreach ($tgd->conditions as $condition) {
            for ($period = 1; $period <= 6; $period++) {
                $needed = 0;
                if ($row == 1) {
                    if ($period == 1 || $period == 2 || $period == 6) {
                        $needed = $tgd->cells[$period][$condition->tgd_c_id]->tgd_ic_target_p
                            - $tgd->cells[$period][$condition->tgd_c_id]->tgd_ic_actual;
                    } else {
                        $needed = $tgd->cells[$period][$condition->tgd_c_id]->tgd_ic_target_p
                            - $tgd->cells[$period][$condition->tgd_c_id]->tgd_ic_actual
                            + $tgd->cells[$period - 1][$condition->tgd_c_id]->needed;
                    }
                } else {
                    $needed = $tgd->cells[$period][$condition->tgd_c_id]->tgd_ic_target_p
                        + $tgd->cells[$period][$condition->tgd_c_id]->tgd_ic_target_t
                        - $tgd->cells[$period][$condition->tgd_c_id]->tgd_ic_actual;
                }

                $tgd->cells[$period][$condition->tgd_c_id]->needed = $needed;
            }
            $row++;
        }

        // Create the rows and columns for each condition.
        $row = 1;
        $cond_num = 1;
        foreach ($tgd->conditions as $condition) {
            $cond_value = '';

            // Indicate whether or this this is an objective or a condition.
            if ($condition->tgd_c_type == 'obj') {
                $cond_value .= '<span style="font-size: .8em">Milestones</span><br />';
            } else {
                $cond_value .= '<span style="font-size: .8em">Condition ' . $cond_num . '</span><br />';
                $cond_num += 1;
            }

            // Each condition label contains the condition name and the converstion rate.
            $cond_value .= '<span style="font-size: .9em">' . $condition->tgd_c_name . '</span><br />';

            // Show the conversion rate for conditions (not objectives).
            if ($condition->tgd_c_type != 'obj') {
                $cond_value .= '<span style="font-size: .8em">Conversion Rate: ' . number_format(($condition->tgd_icond_conversion_rate * 100), 0) . '%</span>';
            }

            // Create the data row and the condition cell.
            $tbl .= '<tr><td style="text-align: center">' . $cond_value . '</td>';

            // Create a cell for each condition's period and display the cell contents.
            for ($period = 1; $period <= 6; $period++) {
                $tbl .= '<td>' . $this->_displayCell($tgd, $period, $condition, $row) . '</td>';
            }

            // Close the row.
            $tbl .= '</tr>';
            $row += 1;
        }

        // Close table body.
        $tbl .= '</tbody>';

        // Close the table.
        $tbl .= '</table>';

        // Write the table.
        $pdf->SetXY(10, 55);
        $pdf->writeHTML($tbl, true, false, false, false, '');

        // Print the file out
        $pdf->Output('tgd.pdf', 'D');
    }

    /**
     * Create the HTML for a cell of the TGD PDF.
     */
    private function _displayCell($tgd, $period, $condition, $row)
    {
        $cells = $tgd->cells;
        $condition_id = $condition->tgd_c_id;
        $data_cell = $cells[$period][$condition_id];

        // Set the label to be either Actual or Projected based on the number of periods finalized.
        $actual_label = 'A';
        if (($tgd->tgd_ui_periods_final + 1) < $period) {
            $actual_label = 'P';
        }

        // Create the cell contents.
        $cell = '';

        // Set the border color
        $color = 'black';
        $bg_color = '#ffffff';
        if (($period == 1 && $row == 4) || ($period == 2 && $row == 3) || ($period == 3 && $row == 2) || ($period == 4 && $row == 1) || ($period == 3 && $row == 4) || ($period == 4 && $row == 3) || ($period == 5 && $row == 2) || ($period == 6 && $row == 1)) {
            $color = 'black';
            $bg_color = '#e6e6ff';
        }

        // Create the table.
        $cell = '<table style="font-size: .6em; border-right: 1px solid ' . $color . '; border-left: 1px solid ' . $color . ';" >';

        // Objective cells have a total and different headings.
        if ($condition->tgd_c_type == 'obj') {

            // Create the table header
            $cell .= '<thead style="border-top: 1px solid ' . $color . ';">';
            $cell .= '<tr style="background-color: #ededed">';
            $cell .= '<th style="text-align: center; border-top: 1px solid ' . $color . ';" colspan="4">FTG</th>';
            $cell .= '<th style="text-align: center; border-top: 1px solid ' . $color . ';" colspan="4">Amount</th>';
            $cell .= '<th style="text-align: center; border-top: 1px solid ' . $color . ';" colspan="4">Cumulative</th>';
            $cell .= '</tr>';
            $cell .= '</thead>';

            // Calculate the cumulative values.
            $floor_total = $data_cell->tgd_ic_floor;
            $target_total = $data_cell->tgd_ic_target;
            $game_total = $data_cell->tgd_ic_game;
            if ($period == 1) {
                $floor_total = $tgd->tgd_ui_py_cum_floor;
                $target_total = $tgd->tgd_ui_py_cum_target;
                $game_total = $tgd->tgd_ui_py_cum_game;
            } else {
                if ($period >= 3 && $period <= 5) {
                    $floor_total = 0;
                    $target_total = 0;
                    $game_total = 0;
                    $actual_total = 0;
                    for ($i = 2; $i <= $period; $i++) {
                        $tmp_cell = $cells[$i][$condition_id];
                        $floor_total += $tmp_cell->tgd_ic_floor;
                        $target_total += $tmp_cell->tgd_ic_target;
                        $game_total += $tmp_cell->tgd_ic_game;
                        $actual_total += $tmp_cell->tgd_ic_actual;
                    }
                }
            }

            // Create the table body
            $cell .= '<tbody>';
            $cell .= '<tr style="background-color: ' . $bg_color . '">';
            $cell .= '<td style="text-align: center; border: none;" colspan="4">F</td>';
            $cell .= '<td style="text-align: center; border: none;" colspan="4">$' . number_format($data_cell->tgd_ic_floor) . '</td>';
            $cell .= '<td style="text-align: center; border: none;" colspan="4">$' . number_format($floor_total) . '</td>';
            $cell .= '</tr>';

            $cell .= '<tr style="background-color: ' . $bg_color . '">';
            $cell .= '<td style="text-align: center; border: none;" colspan="4">T</td>';
            $cell .= '<td style="text-align: center; border: none;" colspan="4">$' . number_format($data_cell->tgd_ic_target) . '</td>';
            $cell .= '<td style="text-align: center; border: none;" colspan="4">$' . number_format($target_total) . '</td>';
            $cell .= '</tr>';

            $cell .= '<tr style="background-color: ' . $bg_color . '">';
            $cell .= '<td style="text-align: center; border: none;" colspan="4">G</td>';
            $cell .= '<td style="text-align: center; border: none;" colspan="4">$' . number_format($data_cell->tgd_ic_game) . '</td>';
            $cell .= '<td style="text-align: center; border: none;" colspan="4">$' . number_format($game_total) . '</td>';
            $cell .= '</tr>';
            $cell .= '</tbody>';

            // Create the table footer
            $cell .= '<tfoot>';
            $cell .= '<tr style="background-color: #ededed">';
            $cell .= '<td style="text-align: center; border-bottom: 1px solid ' . $color . ';" colspan="3">' . $actual_label . '</td>';
            $cell .= '<td style="text-align: center; border-bottom: 1px solid ' . $color . ';" colspan="3">$' . number_format($actual_total) . '</td>';
            $cell .= '<td style="text-align: center; border-bottom: 1px solid ' . $color . ';" colspan="3">N</td>';
            $cell .= '<td style="text-align: center; border-bottom: 1px solid ' . $color . ';" colspan="3">$' . number_format($data_cell->needed) . '</td>';
            $cell .= '</tr>';
            $cell .= '</tfoot>';
        } else {


            if (intval($data_cell->tgd_ic_target_t) !== 0) {
                $target_msg = number_format(intval($data_cell->tgd_ic_target_p) + intval($data_cell->tgd_ic_target_t)) . ' ['
                    . number_format(intval($data_cell->tgd_ic_target_p))
                    . ((intval($data_cell->tgd_ic_target_t) > 0) ? '+' : '')
                    . number_format(intval($data_cell->tgd_ic_target_t))
                    . ']';
            } else {
                $target_msg = number_format(intval($data_cell->tgd_ic_target_p));
            }

            // Create the table header
            $cell .= '<thead style="border-top: 1px solid ' . $color . ';">';
            $cell .= '<tr style="background-color: #ededed">';
            $cell .= '<th style="text-align: center; border-top: 1px solid ' . $color . ';" colspan="2">FTG</th>';
            $cell .= '<th style="text-align: center; border-top: 1px solid ' . $color . ';" colspan="2">Count</th>';
            $cell .= '</tr>';
            $cell .= '</thead>';

            // Create the table body
            $cell .= '<tbody>';
            $cell .= '<tr style="background-color: ' . $bg_color . '">';
            $cell .= '<td style="text-align: center; border: none;" colspan="2">F</td>';
            $cell .= '<td style="text-align: center; border: none;" colspan="2">' . number_format($data_cell->tgd_ic_floor) . '</td>';
            $cell .= '</tr>';

            $cell .= '<tr style="background-color: ' . $bg_color . '">';
            $cell .= '<td style="text-align: center; border: none;" colspan="2">T</td>';
            $cell .= '<td style="text-align: center; border: none;" colspan="2">' . $target_msg . '</td>';
            $cell .= '</tr>';

            $cell .= '<tr style="background-color: ' . $bg_color . '">';
            $cell .= '<td style="text-align: center; border: none;" colspan="2">G</td>';
            $cell .= '<td style="text-align: center; border: none;" colspan="2">' . number_format($data_cell->tgd_ic_game) . '</td>';
            $cell .= '</tr>';
            $cell .= '</tbody>';

            // Create the table footer
            $cell .= '<tfoot>';
            $cell .= '<tr style="background-color: #ededed">';
            $cell .= '<td style="text-align: center; border-bottom: 1px solid ' . $color . ';">' . $actual_label . '</td>';
            $cell .= '<td style="text-align: center; border-bottom: 1px solid ' . $color . ';">' . number_format($data_cell->tgd_ic_actual) . '</td>';
            $cell .= '<td style="text-align: center; border-bottom: 1px solid ' . $color . ';">N</td>';
            $cell .= '<td style="text-align: center; border-bottom: 1px solid ' . $color . ';">' . number_format($data_cell->needed) . '</td>';
            $cell .= '</tr>';
            $cell .= '</tfoot>';

        }

        // Close the cell table
        $cell .= '</table>';
        return $cell;
    }

    /**
     * Updates FTG values for metrices and milestones.
     *
     */
    function update_ftg()
    {
        $this->check_ajax();

        try {
            $col = $this->input->get('col');
            $id = $this->input->getInt('id');
            $year = $this->input->getInt('year');
            $value = $this->input->get('value');

            $this->ret['ftg_updated'] = $this->tgd_model->update_ftg($col, $id, $year, $value);
            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Fetches the linked TGD data.
     */
    public function get_linked_tgd()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $active_product = $this->app->get_product();

            $this->ret['products'] = $this->tgd_model->get_linked_tgd($user_id, $active_product);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }


    /**
     * Updates Linked TGD information
     */
    public function update_linked()
    {
        $this->check_ajax();

        try {
            $tgd_id = $this->input->getInt('tgd_id');
            $product_id = $this->input->getInt('product_id');

            $this->ret['linked_updated'] = $this->tgd_model->update_linked($tgd_id, $product_id);
            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Fetches the history of all the toggles made.
     */
    function get_toggle_history()
    {
        $this->check_ajax();

        try {
            $tgd_id = $this->input->getInt('tgd_id');

            $this->ret['history'] = $this->tgd_model->get_toggle_history($tgd_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Fetches the details for CPD.
     */
    function get_cpd_details()
    {
        $this->check_ajax();

        try {

            // Get the parameters.
            $cpd_id = $this->input->getInt('id');
            $type = $this->input->getString('type');
            $quarter = $this->input->getInt('quarter');

            $this->ret['cpd_details'] = $this->tgd_model->get_cpd_details($cpd_id, $type, $quarter);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Fetch all the TGD that are linked to this TGD instance.
     */
    function get_related_tgd()
    {
        $this->check_ajax();

        try {

            // Get the Parameters.
            $tgd_id = $this->input->getInt('id');
            $user_id = $this->get_user_id();

            $this->ret['related_tgd'] = $this->tgd_model->get_related_tgd($tgd_id, $user_id);
            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Updates the TGD cell label.
     *
     */
    function update_label_name()
    {
        $this->check_ajax();

        try {
            $pk = $this->input->getInt('pk');
            $value = $this->input->getString('value');
            $name = $this->input->getString('name');

            $this->ret['tgd_label_updated'] = $this->tgd_model->update_label_name($pk, $value, $name);
            $this->ret['status'] = $this->app->get_status('success');

        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    function propagate_other_income()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('oi_id');
            $qtr = $this->input->getInt('qtr');

            $this->ret['other_income_propagated'] = $this->tgd_model->propagate_other_income($id, $qtr);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    // Fetch Metrics Data.
    function get_metrics_data() {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();

            $this->ret['metrics_data'] = $this->tgd_model->get_metrics_data($user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    function save_metrics_benchmark() {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $col = $this->input->getString('col');
            $val = $this->input->getInt('val');

            $this->ret['metrics_saved'] = $this->tgd_model->save_metrics_benchmark($user_id, $col, $val);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }
}
