<?php

namespace Controller;

// Common requirements for controllers.
use Joomla\Input;
use Noodlehaus\Exception;


class Controller
{

    public $input;
    public $route;
    public $app;
    public $view_data;

    function __construct($app)
    {

        $this->app = $app;
        $this->input = new Input\Input;

        // Construct the menus if this is not an AJAX request.
        if (!$this->app->is_ajax()) {
            $this->view_data['main_menu'] = $this->_create_main_menu();
            $this->view_data['tool_menu'] = $this->_create_tool_menu();
        }
    }

    public function get_user_id()
    {
        return $_SESSION['user_id'];
    }

    public function route($request, $response, $service)
    {
        // Split the path. Any components after the slash are arguments.
        if (strlen($request->rest) == 0) {
            $args = array();
            $method = 'index';
        } else {
            $args = explode('/', $request->rest);
            $method = array_shift($args);
        }

        // If the method exists, call it.
        if (!method_exists($this, $method)) {
            echo "Method: " . $method . " does not exist";
            die();
        }

        // Call the method
        $ret = call_user_func_array(array($this, $method), $args);

        // If there is data provided, set it
        if (isset($ret['data'])) {
            foreach ($ret['data'] as $key => $val)
                $service->$key = $val;
        }

        // If there is a layout provided, set it
        if (isset($ret['layout'])) {
            $service->layout($ret['layout']);
        }

        // Render the view.
        if (isset($ret['render'])) {
            $service->render($ret['render']);
        }
    }

    /**
     * Create the main menu items.
     */
    private function _create_main_menu()
    {
        return array();
    }

    /**
     * Create the tool menu items.
     */
    private function _create_tool_menu()
    {

        // Anyone logged in? If not, there is no tool menu.
        $user_id = $_SESSION['user_id'];

        // Find out which tools the user has.
        $menu_model = new \Model\Menu;
        return $menu_model->get_tools_menu($user_id);
    }

    /**
     * Check if a request is from AJAX.
     */
    public function check_ajax()
    {
        if (!$this->app->is_ajax()) {
            $ret['status'] = 'failure';
            $ret['message'] = 'Request must be AJAX';
            echo json_encode($ret);
            die();
        }
    }

    /**
     * Check if a request is from an administrator
     */
    public function is_administrator()
    {
        $user_id = $_SESSION['user_id'];

        // Check the database
        $user_model = new \Model\User;
        return $user_model->is_administrator($user_id);
    }

    /*
     * Throws Exception in case of Missing Input
     * */
    public function input_missing_exception()
    {
        throw new Exception("Invalid request. One or more inputs are missing.");
    }

    public function invalid_pdf_exception()
    {
        throw new Exception("Invalid file type.");
    }

    public function get_s3()
    {
        return $this->app->get_s3();
    }

    /**
     * @return Universal Components
     */
    public function get_component() {
        if (isset($_SESSION['components']))
            return $_SESSION['components'];
    }

}
