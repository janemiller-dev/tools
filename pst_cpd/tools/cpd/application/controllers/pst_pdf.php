<?php
/**
 * PST PDF controller.
 */

namespace Controller;
require_once __DIR__ . '/../../vendor/tecnickcom/tcpdf/tcpdf.php';
require_once __DIR__ . '/../../vendor/setasign/fpdi/src/Tcpdf/Fpdi.php';

class PST_PDF extends \TCPDF
{

    public $title, $image_path, $header_text, $company_logo_path, $team_logo_path,
        $footer_text, $company_logo, $team_logo, $requester;

    public function SetTitle($title)
    {
        $this->title = $title;
    }

    public function SetImagePath($image_name, $requester)
    {
        $this->image_path = $image_name;
        $this->requester = $requester;
    }

    public function setHeaderText($text)
    {
        $this->header_text = $text;
    }

    // Set Footer (Team Logo and Company logo)
    public function setFooterContent($company_logo, $team_logo, $text)
    {
        $this->company_logo_path = __dir__ . '/../../thumbnails/' . $company_logo;
        $this->team_logo_path = __dir__ . '/../../thumbnails/' . $team_logo;
        $this->footer_text = $text;

        $this->company_logo = $company_logo;
        $this->team_logo = $team_logo;
        $this->Footer($company_logo, $team_logo);
    }

    public function Header()
    {

        if ($this->image_path !== '') {
            // get the current page break margin
            $bMargin = $this->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $this->AutoPageBreak;
            // disable auto-page-break
            $this->SetAutoPageBreak(false, 0);
            $this->setPageUnit('px');
            // set background image
            $this->SetAlpha(0.1);
            $img_file = $this->image_path;
            $this->Image($img_file, 0, 0, 850, 700, '', '', '', true,
                300, '', false, false, 0);

            // restore auto-page-break status
            $this->SetAutoPageBreak($auto_page_break, $bMargin);

            // set the starting point for the page content
            $this->setPageMark();
            $this->SetAlpha(1);
        }

        // Check if the request is from promo.
        if ('promo' === $this->requester)
            return;

        $this->setPageUnit('px');
        // Set the title
        $this->SetFont('centurygothic', '', 30);

        $this->SetTextColor(19, 25, 77);
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, $bMargin);
        $this->setAbsXY(100, 10);
        $this->Write(10, $this->header_text, '', 0, 'R', true,
            0, false, false, 0, 0, 5);
        $this->SetLineStyle(
            array('width' => 2, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => array(19, 25, 77))
        );
        $this->Line(0, 48, 850, 48);
    }

    public function Footer()
    {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        $this->Rect(0, 540, 2000, 60, 'F', array(), array(0, 0, 0));

        $this->setPageUnit('px');

        if (empty($this->company_logo)) {
            $this->SetFont('centurygothic', '', 15);
            $this->SetTextColor(255, 255, 255);
            $this->setAbsXY(70, 555);
            $this->Write(-105, 'Company Logo', '', 0, 'L', true,
                0, false, false, 0, $wadj = 0, $margin = -15);

        } else {
            $this->Image($this->company_logo_path, 40, 530, 100, 80, '', '', 'M', true,
                300, '', false, false, 0, false, false, false);
        }

        if (empty($this->team_logo)) {
            $this->SetFont('centurygothic', '', 15);
            $this->SetTextColor(255, 255, 255);
            $this->setAbsXY(680, 555);
            $this->Write(-105, 'Team Logo', '', 0, 'L', true,
                0, false, false, 0, $wadj = 0, $margin = -15);

        } else {
            $this->Image($this->team_logo_path, 700, 530, 100, 80, '', '', 'M', false,
                300, '', false, false, 0, false, false, false);
        }
    }

}
