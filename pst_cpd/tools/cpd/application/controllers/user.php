<?php

namespace Controller;

use function ini_get;
use function print_r;

require "controller.php";

class User extends Controller
{
    private $ret, $success_string, $status_string, $message_string, $render_string, $user_model;

    /**
     * Ensure that the user has access to this tool.
     * @param $app
     */
    function __construct($app)
    {
        parent::__construct($app);
        $this->user_model = new \Model\User;
        $this->success_string = 'success';
        $this->status_string = 'status';
        $this->message_string = 'message';
        $this->render_string = 'render';

        $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
    }

    // Show the user profile.
    public function profile()
    {
        // CHANGE THIS TO USE THE WP Profile
        $ret['render'] = 'views/user/profile.php';
        $ret['data'] = $this->view_data;
        return $ret;
    }

    // Show the user's tools.
    public function tools()
    {
        $ret['render'] = 'views/user/tools.php';
        $ret['data'] = $this->view_data;
        return $ret;
    }

    /**
     *
     * Fetches User Data.
     */
    public function get_user_data()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();

            $this->ret['user_data'] = $this->user_model->get_user_data($user_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail');
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Updates User data.
     */
    public function update_user_data()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $name = $this->input->getString('name');
            $email = $this->input->getString('email');
            $product_count = $this->input->getInt('product_count');

            $this->ret['user_data_updated'] = $this->user_model->update_user_data($user_id, $name, $email, $this->input, $product_count);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail');
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }
}