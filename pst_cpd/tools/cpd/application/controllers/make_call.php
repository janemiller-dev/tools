<?php
use Twilio\Rest\Client;

// Your Account SID and Auth Token from twilio.com/console
$account_sid = 'ACc572afeb31b63111c4f425aa2e1b70c6';
$auth_token = $_ENV['1ef3f610b8476095609e5a34a6f37f8d'];
// In production, these should be environment variables. E.g.:
// $auth_token = ["TWILIO_ACCOUNT_SID"]

// A Twilio number you own with Voice capabilities
$twilio_number = "+17194964096";

// Where to make a voice call (your cell phone?)
$to_number = "+919411199458";

$client = new Client($account_sid, $auth_token);
$client->account->calls->create(
    $to_number,
    $twilio_number,
    array(
        "url" => "http://demo.twilio.com/docs/voice.xml"
    )
);