<?php

namespace Controller;
require __DIR__ . '/../../vendor/autoload.php';

use function print_r;
use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;
use Twilio\Twiml;

class Call extends Controller
{

    var $app = null, $response;
    var $account_id, $auth_token, $app_sid;
    private $ret, $twilio;

    /**
     * Ensure that the user has access to this tool.
     */
    function __construct($app)
    {
        parent::__construct($app);

        // Your Account SID and Auth Token from twilio.com/console
        $this->twilio =  $this->app->getTwilio();
        $this->app_sid = $this->twilio['app_sid'];
        $this->account_sid = $this->twilio['account_sid'];
        $this->auth_token = $this->twilio['auth_token'];

    }

    /**
     * Default Method
     *
     * @return mixed
     */
    public function index()
    {
        $this->ret['render'] = 'views/call.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     * Answering machine.
     */
    function answer()
    {
        $response = new Twiml;
        $response->say('Hello');
        $response->play('https://api.twilio.com/cowbell.mp3', array("loop" => 5));
        print $response;
    }

    /**
     * Fetches Credentials.
     */
    function get_capablity_token()
    {
        $this->check_ajax();
        try {

            $capability = new ClientToken($this->account_sid, $this->auth_token);
            $capability->allowClientOutgoing($this->app_sid);
            $capability->allowClientIncoming('Paul');

            $this->ret['token'] = $capability->generateToken();
            $this->ret['status'] = 'success';

        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }

        echo json_encode($this->ret);
    }

    /**
     * VOIP functionality
     */
    function voice()
    {
        try {
        $response = new Twiml;

        // get the phone number from the page request parameters, if given
        if (isset($_REQUEST['To']) && strlen($_REQUEST['To']) > 0) {
            $number = htmlspecialchars($_REQUEST['To']);
            $dial = $response->dial(array('callerId' => +919411199458));

            // wrap the phone number or client name in the appropriate TwiML verb
            // by checking if the number given has only digits and format symbols
            if (preg_match("/^[\d\+\-\(\) ]+$/", $number)) {
                $dial->number($number);
            } else {
                $dial->client($number);
            }
        } else {
            $response->say("Thanks for calling!");
        }

        header('Content-Type: text/xml');
        echo $response;
        } catch (\Exception $e) {

        }
    }
}