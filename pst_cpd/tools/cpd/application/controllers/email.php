<?php
/**
 * Email controller.
 */

namespace Controller;

class Email extends Controller
{

    /**
     * Ensure that the user has access to this tool.
     */
    function __construct($app)
    {
        parent::__construct($app);
    }

    /**
     * Send an email support message.
     */
    public function send_support_message()
    {
        $this->check_ajax();

        try {

            // Check the recaptcha code.
            $response = $this->input->getText('g-recaptcha-response');
            $recaptcha = new \ReCaptcha\ReCaptcha('6LdCKi8UAAAAAJa3zF7EVehXEoBXEBK7cF6oUUt7');
            $remoteip = $this->app->get_remote_ip();
            $resp = $recaptcha->verify($response, $remoteip);

            if ($resp->isSuccess()) {
            } else {
                $errors = $resp->getErrorCodes();

                foreach ($errors as $error) {

                    if ($error == 'missing-input-response')
                        throw new \Exception("Please click the reCAPTCHA button");
                }
                throw new \Exception("Please confirm that you are not a robot.");
            }

            // Get the parameters
            $topic_id = $this->input->getInt('topic');
            $subject = $this->input->getRaw('subject');
            $question = $this->input->getRaw('question');

            // Create the transporter
            $this->app->log->debug("getting transport");
            $transporter = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
                ->setUsername($this->app->get_email_credentials('username'))
                ->setPassword($this->app->get_email_credentials('password'));;

            $this->app->log->debug("getting mailer");
            $mailer = \Swift_Mailer::newInstance($transporter);

            //Get the user's email information.
            $current_user = wp_get_current_user();

            // Create the body of the message.
            $msg_body = "Subject: " . $topic_id;
            $msg_body .= "\nQuestion: " . $question;

            // Construct the message to the support team
            $message = \Swift_Message::newInstance()
                ->setSubject('[Support Request] : ' . $subject)
                ->setTo(array('support@advisorysellingtools.com' => 'Advisory Selling Tools Support', 'sumitk@mindfiresolutions.com'))
                ->setFrom(array($current_user->data->user_email => $current_user->data->user_nicename))
                ->setBody($msg_body)
                ->addPart(nl2br($msg_body), 'text/html');
            $mailer->send($message);

            // Update the message to be the one sent to the requester
            $msg_body = "Thank you for your question. You will receive an email confirmation that it was received by our support team from advisorysellingtools.com. If this is your first support question, the confirmation will give you instructions on how to log in to the support site so that you can follow the progress of the resolution of our question. If you have any further questions, please contact us at support@advisorysellingtools.com.\nYour support question is shown below:\n" . $msg_body;

            // Construct the message to the requester
            $message = \Swift_Message::newInstance()
                ->setSubject('[Advisory Selling Tools Support] : ' . $subject)
                ->setTo(array($current_user->data->user_email => $current_user->data->user_nicename))
                ->setFrom(array('suppport@advisorysellingtools.com' => 'Advisory Selling Tools Support'))
                ->setBody($msg_body)
                ->addPart(nl2br($msg_body), 'text/html');
            $mailer->send($message);

            $ret['status'] = 'success';
        } catch (\Exception $e) {
            $ret['status'] = 'failure';
            $ret['message'] = $e->getMessage();
        }
        echo json_encode($ret);
    }

}