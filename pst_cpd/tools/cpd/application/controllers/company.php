<?php
/**
 * Company controller.
 */

namespace Controller;

class Company extends Controller
{

	var $app = null;

	/**
	 * Ensure that the user has access to this tool.
	 */
	function __construct($app)
	{
		$this->app = $app;
		parent::__construct($app);
	}

	/**
	 * Default method is to show the list of companies.
	 */
	function index()
	{
		$this->app->redirect('/company/companies');
	}

	/**
	 * Default method is to show the list of companies.
	 */
	function companies()
	{
		$ret['render'] = 'views/company/companies.php';
		$ret['data'] = $this->view_data;
		return $ret;
	}

	/**
	 * Show the list of companies.
	 */
	function get_companies()
	{
		$this->check_ajax();

		try {
			// Limit the search to the user's companies.
			$user_id = $this->get_user_id();

			// Get the list of companies from the company model.
			$company_model = new \Model\Company;
			$ret = $company_model->get_companies($this->input, $user_id);

			// For each company, get the contacts, address, phones, and emails.
			foreach ($ret['data'] as $company) {
				// Get the contacts
				$contact_model = new \Model\Contact;
				$company->contacts = $contact_model->get_company_contacts($company->company_id);

				// Get the phone numbers
				$phone_model = new \Model\Phone;
				$company->phones = $phone_model->get_object_phones('company', $company->company_id);

				// Get the addresses
				$address_model = new \Model\Address;
				$company->addresses = $address_model->get_object_addresses('company', $company->company_id);

				// Get the email addresses
				$email_model = new \Model\Email;
				$company->emails = $email_model->get_object_emails('company', $company->company_id);
			}
		} catch (\Exception $e) {
			$ret['error'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Show the specified COMPANY.
	 */
	function company()
	{
		$ret['render'] = 'views/company/company.php';
		$ret['data'] = $this->view_data;
		return $ret;
	}

	/**
	 * Show the specified COMPANY.
	 */
	function get_company()
	{
		$this->check_ajax();
		try {

			// Get the inputs
			$id = $this->input->getInt('id');
			if (empty($id))
				throw new Exception("Invalid request. One or more inputs are missing.");

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Add the instance
			$company_model = new \Model\Company;
			$ret['company'] = $company_model->get_company($id, $user_id);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Add a new company.
	 */
	function add_company()
	{
		$this->check_ajax();
		try {

			// Get the inputs
			$name = $this->input->getText('name');
			$domain = $this->input->getText('domain');
			$description = $this->input->getText('description');
			$contact_first = $this->input->getText('contact_first');
			$contact_last = $this->input->getText('contact_last');
			$address = $this->input->getText('address');
			$phone = $this->input->getText('phone');
			$email = $this->input->getText('email');
			if (empty($name))
				throw new Exception("Invalid request. One or more inputs are missing.");

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Add the instance
			$company_model = new \Model\Company;
			$company_id = $company_model->add_company($user_id, $name, $domain, $description);

			// If there is a contact name, add it.
			$contact_id = null;
			if (!empty($contact_first) || !empty($contact_last)) {
				$contact_model = new \Model\Contact;
				$contact_id = $contact_model->add_contact($user_id, $company_id, $contact_first, $contact_last);
			}

			// If there is a phone, add it.
			if (!empty($phone)) {
				$phone_model = new \Model\Phone;
				$phone_model->add_phone($user_id, 'company', $company_id, $phone, 'Main');

				// Associate the phone with the contact as well.
				if ($contact_id != null)
					$phone_model->add_phone($user_id, 'contact', $contact_id, $phone, 'Office');
			}

			// If there is an address, add it.
			if (!empty($address)) {
				$address_model = new \Model\Address;
				$address_model->add_address('company', $company_id, $address, 'Main', $this->input);

				// Associate the address with the contact as well.
				if ($contact_id != null)
					$address_model->add_address('contact', $contact_id, $address, 'Office');
			}
			// If there is an email, add it.
			if (!empty($email)) {
				$email_model = new \Model\Email;
				$email_model->add_email('company', $company_id, $email, 'Main');

				// Associate the email with the contact as well.
				//if ($contact_id != null)
				//$email_model->add_email($user_id, 'contact', $contact_id, $email, 'Business');
			}

			$ret['company_id'] = $company_id;
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

	/**
	 * Delete a company
	 */
	function delete_company()
	{
		$this->check_ajax();
		try {

			// Get the inputs
			$company_id = $this->input->getInt('company_id');
			if (empty($company_id))
				throw new Exception("Invalid request. One or more inputs are missing.");

			// Get the current subscription status.
			$user_id = $this->get_user_id();

			// Delete any attached contacts.
			$contact_model = new \Model\Contact;
			$contact_model->delete_company_contacts($company_id);

			// Delete the company
			$company_model = new \Model\Company;
			$ret['company_id'] = $company_model->delete_company($company_id);

			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}
}
