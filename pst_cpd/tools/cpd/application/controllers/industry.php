<?php
/**
 * Industry controller.
 */

namespace Controller;

use function basename;
use Noodlehaus\Exception;
use function pathinfo;
use function var_dump;

class Industry extends Controller
{

    var $app = null;
    private $industry_model;
    private $ret;
    /**
     * Ensure that the user has access to this tool.
     */
    function __construct($app)
    {
        parent::__construct($app);
        $this->industry_model = new \Model\Industry();
        $this->ret['status'] = 'success';

        // If not an administrator, nothing in this controller is accessible.
//        if (!$this->is_administrator()) {
//            $this->app->redirect('dashboard');
//        }
    }

    /**
     * Default method.
     */
    function index()
    {
        $this->ret['render'] = 'views/industry/dashboard.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     * Show the specified Industry
     *
     * @return void $this->ret view for the industry.
     */
    function industry()
    {
        $this->ret['render'] = 'views/industry/industry.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     * Get the Industries.
     *
     * @return void $this->ret list of all industries
     */
    function get_industries()
    {
        $this->check_ajax();
        try {
            // Get the current subscription status.
            $this->ret['industries'] = $this->industry_model->get_industries();
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Get the Professions for the specified Industry.
     *
     * @return void $this->ret result of profession for a industry
     */
    function get_professions()
    {
        $this->check_ajax();
        try {
            $user_id = $this->get_user_id();
            // Need the industry ID.
            $industry_id = $this->input->getInt('industry_id');

            if (empty($industry_id)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }
            // Get the current subscription status.
            $this->ret['professions'] = $this->industry_model->get_professions($industry_id, $user_id);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Get the Professions for the specified industry that the user is not a member of.
     *
     * @return void $this->ret result of usused profession
     */
    function get_unused_professions()
    {
        $this->check_ajax();
        try {
            $user_id = $this->get_user_id();
            // Need the industry ID.
            $industry_id = $this->input->getInt('industry_id');

            if (empty($industry_id)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }
            // Get the current subscription status.
            $this->ret['professions'] = $this->industry_model->get_unused_professions($industry_id, $user_id);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }


    /**
     * Show the specified industry.
     *
     * @return void $this->ret result of select industry
     */
    function get_industry()
    {
        $this->check_ajax();
        try {
            // Get the inputs
            $id = $this->input->getInt('id');
            if (empty($id)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }
            // Add the instance
            $this->ret['game_type'] = $this->industry_model->get_industry($id);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }

        echo json_encode($this->ret);
    }

    /**
     * Add a new industry
     *
     * @return void $this->ret id of new industry
     */
    function add_industry()
    {
        $this->check_ajax();
        try {
            // Get the inputs
            $name = $this->input->getText('name');
            $description = $this->input->getText('description');

            if (empty($name) || empty($description)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }
            // Get the current subscription status.
            $user_id = $this->get_user_id();

            // Add the industry
            $tgd_model = new \Model\TGD;
            $ret['industry_id'] = $tgd_model->add_industry($user_id, $name, $description);

            $this->ret['status'] = 'success';


        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Copy the specified INDUSTRY to a new one (or overwrite an existing one).
     */
    function copy_industry()
    {
        // Perform checks
        $this->check_ajax();
        // Try to clone the industy
        try {
            // Get the inputs
            $industry_id = $this->input->getText('industry_id');
            $save_as_name = $this->input->getText('save_as_name');

            if ($save_as_name == '') {
                $save_as_name = null;
            }
            $save_as_id = $this->input->getText('save_as_id');

            if ($save_as_id == '0') {
                $save_as_id = null;
            }
            if (empty($industry_id) && (empty($save_as_name) || empty($save_as_id))) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }
            // Get the current subscription status.
            $user_id = $this->get_user_id();

            // Add the industry
            $this->ret['industry_id'] = $this->industry_model->copy_industry($user_id, $industry_id, $save_as_name, $save_as_id);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Delete an industry
     *
     * @return void $this->ret result of profession update
     */
    function delete_industry()
    {
        $this->check_ajax();
        try {
            // Get the inputs
            $industry_id = $this->input->getText('industry_id');

            if (empty($industry_id)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }
            // Get the current subscription status.
            $user_id = $this->get_user_id();

            // Delete the industry
            $this->ret['industry_id'] = $this->industry_model->delete_industry($user_id, $industry_id);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Add a new Profession condition
     *
     * @return void $this->ret result of add profession
     */
    function add_condition()
    {
        $this->check_ajax();
        try {
            // Get the inputs
            $profession_id = $this->input->getInt('profession_id');
            $type = $this->input->getText('type');
            $name = $this->input->getText('name');
            $description = $this->input->getText('description');
            $tooltip = $this->input->getText('tooltip');
            $uom = $this->input->getText('uom');

            if (empty($profession_id) || empty($type) || empty($name) || empty($description) || empty($tooltip) || empty($uom)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }
            // Put the values in an array.
            $values['type'] = $type;
            $values['name'] = $name;
            $values['description'] = $description;
            $values['tooltip'] = $tooltip;
            $values['uom'] = $uom;

            // Add the condition
            $this->ret['condition_id'] = $this->industry_model->add_condition($profession_id, $values);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Delete a condition
     *
     * @return void $this->ret result of delete
     */
    function delete_condition()
    {
        $this->check_ajax();
        try {
            // Get the inputs
            $profession_c_id = $this->input->getInt('profession_c_id');

            if (empty($profession_c_id)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }
            // Add the condition
            $this->industry_model->delete_condition($profession_c_id);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Move a condition
     *
     * @return void $this->ret result of move
     *
     */
    function move_condition($direction)
    {
        $this->check_ajax();
        try {
            // Get the inputs
            $industry_c_id = $this->input->getInt('id');

            if (empty($industry_c_id)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }
            // Add the condition
            $this->industry_model->move_condition($industry_c_id, $direction);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Add a new multiplier
     *
     * @return void $this->ret id of new multiplier
     */
    function add_multiplier()
    {
        $this->check_ajax();
        try {
            // Get the inputs
            $industry_id = $this->input->getInt('industry_id');
            $name = $this->input->getText('name');
            $description = $this->input->getText('description');
            $operation = $this->input->getText('operation');
            $type = $this->input->getText('type');
            $propagate = $this->input->getText('propagate');

            if (empty($industry_id) || empty($name) || empty($description) || empty($operation) || empty($type) || empty($propagate)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }
            // Get the current subscription status.
            $user_id = $this->get_user_id();

            // Put the values in an array.
            $values['name'] = $name;
            $values['description'] = $description;
            $values['operation'] = $operation;
            $values['type'] = $type;
            $values['propagate'] = $propagate;

            // Add the multiplier
            $this->industry_model = new \Model\Industry;
            $this->ret['multiplier_id'] = $this->industry_model->add_multiplier($industry_id, $values);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Delete a multiplier
     *
     * @return void
     */
    function delete_multiplier()
    {
        $this->check_ajax();
        try {
            // Get the inputs
            $industry_m_id = $this->input->getInt('industry_m_id');

            if (empty($industry_m_id)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }
            // Get the current subscription status.
            $user_id = $this->get_user_id();

            // Add the multiplier
            $this->industry_model->delete_multiplier($industry_m_id);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Move a multiplier
     * @return void $this->ret none
     */
    function move_multiplier($direction)
    {
        $this->check_ajax();
        try {
            // Get the inputs
            $industry_m_id = $this->input->getInt('id');

            if (empty($industry_m_id)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }
            // Add the multiplier
            $this->industry_model->move_multiplier($industry_m_id, $direction);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Show the specified Industry Profession
     *
     * @return void $this->ret view data
     */
    function profession()
    {
        $this->ret['render'] = 'views/industry/profession.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     * Show the specified profession.
     *
     * @return void $this->ret result of profession update
     */
    function get_profession()
    {
        $this->check_ajax();
        try {
            // Get the inputs
            $id = $this->input->getInt('id');

            if (empty($id)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }
            // Add the instance
            $this->ret['game_type'] = $this->industry_model->get_profession($id);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Add a new profession
     *
     * @return void $this->ret returns the profession
     */
    function add_profession()
    {
        $this->check_ajax();
        try {
            // Get the inputs
            $industry_id = $this->input->getInt('industry_id');
            $name = $this->input->getText('name');
            $description = $this->input->getText('description');

            if (empty($name) || empty($description)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }
            // Get the current subscription status.
            $user_id = $this->get_user_id();

            // Add the profession
            $this->ret['profession_id'] = $this->industry_model->add_profession($industry_id, $name, $description);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Copy the specified profession
     *
     * @return void $this->ret id of new profession
     */
    function copy_profession()
    {

        // Perform checks
        $this->check_ajax();

        // Try to clone the industy
        try {
            // Get the inputs
            $profession_id = $this->input->getText('profession_id');
            $save_as_name = $this->input->getText('save_as_name');

            if ($save_as_name == '') {
                $save_as_name = null;
            }
            $save_as_id = $this->input->getText('save_as_id');

            if ($save_as_id == '0') {
                $save_as_id = null;
            }
            if (empty($profession_id) && (empty($save_as_name) || empty($save_as_id))) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }
            // Get the current subscription status.
            $user_id = $this->get_user_id();

            // Add the industry
            $this->ret['industry_id'] = $this->industry_model->copy_profession($profession_id, $save_as_name, $save_as_id);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Delete an profession
     *
     * @return void $this->ret id of deleted profession
     */
    function delete_profession()
    {
        $this->check_ajax();
        try {
            // Get the inputs
            $profession_id = $this->input->getInt('profession_id');

            if (empty($profession_id)) {
                throw new Exception("Invalid request. One or more inputs are missing.");
            }
            // Delete the profession
            $this->industry_model->delete_profession($profession_id);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Adds new product type.
     *
     */
    function add_new_product()
    {
        $this->check_ajax();

        try {
            $product = $this->input->getString('product');
            $user_id = $this->get_user_id();

            $this->ret['product_added'] = $this->industry_model->add_new_product($product, $user_id);
            $this->ret['status'] = 'success';

        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = 'This Product Type Already Exists.';

            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' .  $e->getMessage());
        }
        echo json_encode($this->ret);
    }

    /**
     *  Fetches the list of all the product types that have CPD attached to it.
     */
    function get_my_products()
    {
        $this->check_ajax();
        try {
            // Get the current subscription status.
            $user_id = $this->get_user_id();
            $this->ret['products'] = $this->industry_model->get_my_products($user_id);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Fetches the list of all the products that belongs to a user/default products.
     */
    function get_all_products()
    {
        $this->check_ajax();
        try {
            // Get the current subscription status.
            $user_id = $this->get_user_id();
            $this->ret['products'] = $this->industry_model->get_all_products($user_id);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }


    /**
     * Update the active product of the user.
     *
     * @return void $this->ret result of product update
     */
    public function set_active_product()
    {
        $this->check_ajax();
        try {
            // Get the parameters.
            $product_id = $this->input->getInt('id');
            $is_group = $this->input->getString('is_group');

            // Perform the update.
            $this->industry_model->set_active_product($this->get_user_id(), $product_id, $is_group);
            $this->ret['product_id'] = $this->input->getInt('id');
            $this->app->set_product($product_id);
            $this->ret['status'] = 'success';
        } catch (Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }
}
