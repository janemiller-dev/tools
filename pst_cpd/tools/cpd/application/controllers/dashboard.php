<?php

namespace Controller;

class Dashboard extends Controller
{
    public function index()
    {
        $ret['render'] = 'views/dashboard.php';
        $ret['data'] = $this->view_data;
        return $ret;
    }

    public function getting_started()
    {
        $ret['render'] = 'views/getting_started.php';
        $ret['data'] = $this->view_data;
        return $ret;
    }

    public function welcome()
    {
        $ret['render'] = 'views/welcome.php';
        $ret['data'] = $this->view_data;
        return $ret;
    }
}
