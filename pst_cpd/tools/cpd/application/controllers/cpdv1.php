<?php
/**
 * Client Project Dashboard Controller.
 *
 * Client Project Dashboard controller for request to CPD tool.
 *
 * @author   Sumit K (sumitk@mindfiresolutions.com)
 * @category Controller
 *
 */

namespace Controller;

use function print_r;
use setasign\Fpdi\Tcpdf\Fpdi;


/**
 * Client Project Dashboard class.
 *
 * @author   Sumit K (sumitk@mindfiresolutions.com)
 * @category Controller.
 */
class Cpdv1 extends Controller
{

    private $cpd_model;
    private $ret, $success_string, $status_string, $message_string, $render_string;

    /**
     * Ensure that the user has access to this tool.
     * @param $app
     */
    function __construct($app)
    {
        parent::__construct($app);
        $this->cpd_model = new \Model\CPDV1;
        $this->success_string = 'success';
        $this->status_string = 'status';
        $this->message_string = 'message';
        $this->render_string = 'render';

        $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
    }

    /**
     * Default method.
     */
    function index()
    {
        $this->ret[$this->render_string] = 'views/cpdv1/dashboard.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     * Show the specified CPD.
     */
    function cpdv1()
    {
        $this->ret[$this->render_string] = 'views/cpdv1/cpdv1.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     * Show the specified CPD.
     */
    function cpd()
    {
        $this->ret[$this->render_string] = 'views/cpdv1/cpd.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     * Get the user's CPD instances.
     */
    function get_instances()
    {
        $this->check_ajax();
        try {
            // Get the current subscription status.
            $cpd_list['user_id'] = $this->get_user_id();

            $cpd_list['year'] = $this->input->getText('year') ?: '';
            $cpd_list['product_id'] = $this->input->get('product_id');

            $this->ret['instances'] = $this->cpd_model->get_instances($cpd_list);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     * Add a new CPD instance
     *
     * @return void $this->ret result for add_cpd
     */
    function add_cpd()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $cpd['cpd_year'] = $this->input->getText('cpd_year');
            $cpd['cpd_tgd_id'] = $this->input->getText('cpd_tgd_id');
            $cpd['name'] = $this->input->getText('name');
            $cpd['description'] = $this->input->getText('description');
            $cpd['product_id'] = $this->app->get_product();

            // Check if any input is missing.
            if (empty($cpd['cpd_year']) || empty($cpd['name']) ||
                empty($cpd['description']) || empty($cpd['product_id'])) {
                $this->input_missing_exception();
            }

            // Get the current subscription status.
            $user_id = $this->get_user_id();

            // Add the instance
            $this->ret['instance_id'] = $this->cpd_model->add_cpd($user_id, $cpd);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     * Delete an CPD instance
     *
     * @return void $this->ret result for delete_cpd
     */
    function delete_cpd()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $cpd_id = $this->input->getInt('id');

            // Check if CPD ID is not set. If not throw error.
            if (empty($cpd_id)) {
                $this->input_missing_exception();
            }
            // Delete the instance
            $this->ret['instance_id'] = $this->cpd_model->delete_cpd($cpd_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     * Copy a CPD instance to another instance
     *
     * @return void $this->ret result for copy_cpd
     */
    function copy_cpd()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $cpd_ui_id = $this->input->getText('cpd_ui_id');
            $save_as_name = $this->input->getText('save_as_name');

            // If name is not set, set it to null.
            if ('' === $save_as_name) {
                $save_as_name = null;
            }
            $save_as_id = $this->input->getText('save_as_id');

            // If save as ID is not set, set it to null.
            if ('0' === $save_as_id) {
                $save_as_id = null;
            }

            // If any input is missing throw an error.
            if (empty($cpd_ui_id) && (empty($save_as_name) || empty($save_as_id))) {
                $this->input_missing_exception();
            }

            // Get the current subscription status.
            $user_id = $this->get_user_id();

            // Add the instance
            $this->ret['instance_id'] = $this->cpd_model->copy_cpd($user_id, $cpd_ui_id, $save_as_name, $save_as_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     * Get the specified cpd
     *
     * @return void $this->ret details for the specified CPD
     */
    function get_cpd()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $id = $this->input->getInt('id');
            $quarter = $this->input->getInt('current_quarter');
            $user_id = $this->get_user_id();
            $year = $this->input->getInt('year');
            $key = $this->input->getString('key');
            $val = $this->input->getString('val');
            $is_sort = $this->input->getString('is_sort');

            // Check if any of the input is missing.
            if (empty($id) || (empty($quarter) || 0 === $quarter || $quarter > 4)) {
                $this->input_missing_exception();
            }

            // Add the instance
            $cpd_details = $this->cpd_model->get_cpd($user_id, $id, $quarter, $key, $val, $is_sort, $year);
            $cpd_deals = $cpd_details->deals;

            unset($cpd_details->deals);
            $this->ret['cpd'] = $cpd_details;
            $deals_ret = [];

            // Loop through each existing CPD deals.
            foreach ($cpd_deals as $deal) {
                $deals_ret[$deal->ds_status][] = $deal;
            }

            $cpd_buyers = $this->cpd_model->get_cpd_buyers($id, $quarter, $year);
            // Loop through each existing CPD deals.
            foreach ($cpd_buyers as $deal) {
                $deals_ret[$deal->ds_status][] = $deal;
            }

            // Fetches CPD deals details.
            $this->ret['cpd_deals'] = $deals_ret;
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\UnexpectedValueException $e) {
            $product_id = $this->app->get_product();

            // Create DB record
            $homebase_model = new \Model\HomeBase;
            $cpd_id = $homebase_model->get_active_cpd($this->get_user_id(), $product_id);

            (0 !== sizeof($cpd_id)) ?
                $url = 'https://' . $_SERVER['SERVER_NAME'] . '/tools/cpdv1/cpdv1/' . $cpd_id[0]->uact_cpd_id :
                $url = 'https://' . $_SERVER['SERVER_NAME'] . '/tools/cpdv1';

            $this->ret[$this->status_string] = $this->app->get_status('redirect');
            $this->ret['redirect'] = $url;
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     * Search for the specified user's CPD instances .
     *
     * @return void $this->ret list of CPD instances
     */
    function search_cpd_instances()
    {
        $this->check_ajax();
        try {
            // Get the CPD instances
            $user_id = $this->get_user_id();

            $this->ret = $this->cpd_model->search_cpd_instances($user_id, $this->input);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     * Add a new deal for CPD
     *
     * @return void $this->ret result for add_deal
     */
    function add_deal()
    {
        $this->check_ajax();
        try {
            // Get all the inputs and store in an array.
            $deal['name'] = $this->input->getText('deal_name');
            $deal['property'] = $this->input->getText('property_name');
            $deal['cpd_id'] = $this->input->getText('add_deal_cpd_id');
            $deal['email'] = $this->input->getText('client_email');
            $deal['phone'] = $this->input->get('client_phone');
            $deal['user_id'] = $this->get_user_id();
            $deal[$this->status_string] = $this->input->getText('deal_status');
            $deal['long'] = $this->input->get('add_deal_long');
            $deal['lat'] = $this->input->get('add_deal_lat');

            if (count(array_filter($deal)) != count($deal)) {
                $this->input_missing_exception();
            }

            // Add the instance
            $this->ret['deal_id'] = $this->cpd_model->add_deal($deal);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     * Change the Status of the deal and move it to other table
     *
     * @return void $this->ret result for move_deal
     */
    function move_deal()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $deal['id'] = $this->input->getText('new_status_deal_id');
            $deal['current_status'] = $this->input->getText('deal_current_status');
            $deal['new_status'] = $this->input->getText('deal_new_status');
            $deal['new_move_date'] = $this->input->getText('deal_new_move_date');

            // Check if any input is missing.
            if (empty($deal['id']) || empty($deal['current_status']) ||
                empty($deal['new_status']) || empty($deal['new_move_date'])) {
                $this->input_missing_exception();
            }

            // Set new status for deal.
            $this->ret['deal_status_id'] = $this->cpd_model->move_deal($deal);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     * Delete a deal
     *
     * @return void $this->ret result for delete_deal
     */
    function delete_deal()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $deal_id = $this->input->getInt('deal_id');
            if (empty($deal_id)) {
                $this->input_missing_exception();
            }

            // Delete the instance
            $this->ret['deal_id'] = $this->cpd_model->delete_deal($deal_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     * Get the promos for specified deal
     *
     * @return void $this->ret list of promos for the specified deal
     */
    function get_promos()
    {
        $this->check_ajax();
        try {
            // Get the inputs
            $id = $this->input->getInt('deal_id');

            if (empty($id)) {
                $this->input_missing_exception();
            }
            // Add the instance
            $this->ret['promos'] = $this->cpd_model->get_promos($id);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     * Add a new promo for deal
     *
     * @return void $this->ret result for adding promotions
     */
    function add_promo()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $promo['name'] = $this->input->getText('promo_name');
            $promo['datetime'] = $this->input->getText('promo_date');
            $promo['promo_deal_id'] = $this->input->getText('add_promo_deal_id');

            if (empty($promo['name']) || empty($promo['promo_deal_id'])) {
                $this->input_missing_exception();
            }
            // Add the instance
            $this->ret['promo_id'] = $this->cpd_model->add_promo($promo);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     * Delete a promo
     *
     * @return void $this->ret result for deleting promotions
     */
    function delete_promo()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $promo_id = $this->input->getText('promo_id');
            if (empty($promo_id)) {
                $this->input_missing_exception();
            }
            // Delete the instance
            $this->ret['promo_id'] = $this->cpd_model->delete_promo($promo_id);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     * Updates the field name and description
     *
     * @return void $this->ret result for updating the detail for the deal tables in CPD
     */
    function update_deal_table_info()
    {
        $this->check_ajax();
        try {

            // Get the parameters
            $name = $this->input->get('name', '', 'RAW');
            $id = $this->input->getInt('pk');
            $value = $this->input->get('value', '', 'RAW');

            if (empty($value)) {
                $this->input_missing_exception();
            }
            // Update the value
            $this->ret[$this->message_string] = $this->cpd_model->update_deal_table_info($name, $id, $value);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     * Updates the lead for simple deal
     *
     * @return void $this->ret result for updating the lead
     */
    function update_lead()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('pk');
            $lead = $this->input->get('value');

            // Check if input is missing.
            if (empty($lead) || empty($id)) {
                $this->input_missing_exception();
            }
            $this->ret[$this->message_string] = $this->cpd_model->update_lead($id, $lead);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);

        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *Updates the agent who generated the deal
     *
     * @return void $this->ret result for updating the gen
     */
    function update_gen()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('pk');
            $gen = $this->input->get('value');

            // Check if any input is missing.
            if (empty($gen) || empty($id)) {
                $this->input_missing_exception();
            }
            $this->ret[$this->message_string] = $this->cpd_model->update_gen($id, $gen);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);

        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     * Gets the FTG values for a year.
     *
     * @return void $this->ret result of FTG for the profession.
     */
    function get_ftg()
    {
        $this->check_ajax();
        try {
            $year = $this->input->getInt('year');

            if (empty($year)) {
                $this->input_missing_exception();
            }
            $year = $this->input->getInt('year');
            $cpd_id = $this->input->getInt('cpd_id');
            $quarter = $this->input->getInt('quarter');
            $this->ret['data'] = $this->cpd_model->get_ftg($cpd_id, $quarter, $year);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);

        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     * Fetches and displays Accountability Manager window.
     *
     * @return mixed Layout for Accountability Manager.
     */
    function acm()
    {
        $ret[$this->render_string] = 'views/cpdv1/acm.php';
        $ret['data'] = $this->view_data;
        return $ret;
    }

    /**
     *
     * Fetch Team Accountability Manager Instances.
     */
    function search_tam_instances()
    {
        $this->check_ajax();
        try {

            // Get the CPD instances
            $user_id = $this->get_user_id();
            $deal_id = $this->input->getInt('deal_id');
            $req_type = $this->input->get('req_type');
            $type = $this->input->get('type');

            $this->ret = $this->cpd_model->search_tam_instances($user_id, $deal_id, $req_type, $type);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);

        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Adds a new instance of accountability manager.
     */
    function add_acm()
    {
        $this->check_ajax();
        try {

            $data['user_id'] = $this->get_user_id();
            $data['deal_id'] = $this->input->get('deal_id');
            $data['ceo'] = $this->input->get('acm_ceo_id');
            $data['action'] = $this->input->get('acm_action', '', 'RAW');
            $data['acm_type'] = $this->input->get('acm_type');
            $data['req_type'] = $this->input->get('req_prom');
            $data['acm_when'] = $this->input->get('acm_when', '', 'RAW');
            $data['acm_whom'] = $this->input->getString('acm_whom');

            $this->ret['add_acm'] = $this->cpd_model->add_acm($data);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);

        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Update an existing instance of ACM.
     */
    function update_acm()
    {
        $this->check_ajax();
        try {
            $additional_row = $this->input->get('additional-row');
            $col = $this->input->get('col');
            $value = $this->input->get('value', '', 'RAW');
            $id = $this->input->get('id');

            $this->ret['acm_updated'] = $this->cpd_model->update_acm($id, $col, $value, $additional_row);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }

        echo json_encode($this->ret);
    }

    /**
     *
     * Adds additional ACM rows to ACM.
     */
    function add_additional_acm()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('id');

            $this->ret['aac_id'] = $this->cpd_model->add_additional_acm($id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Fetches additional ACM row.
     */
    function get_additional_row()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('id');

            $this->ret['additional_data'] = $this->cpd_model->get_additional_acm($id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Deletes ACM instance.
     */
    function delete_acm()
    {
        $this->check_ajax();
        try {
            $acm_id = $this->input->getInt('id');

            $this->ret['acm_deleted'] = $this->cpd_model->delete_acm($acm_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     * Fetches and returns docs available.
     *
     * @return mixed
     */
    function docs()
    {
        $ret[$this->render_string] = 'views/cpdv1/docs.php';
        $ret['data'] = $this->view_data;
        return $ret;
    }

    /**
     *
     * Uploads a new Document.
     */
    function upload_docs()
    {
        $this->check_ajax();

        foreach ($_FILES as $file) {
            try {
                // Get inputs.
                $data['client_id'] = $this->input->getInt('client_id');
                $data['type'] = $this->input->get('type');
                $data['category'] = $this->input->get('category');

                // Fetch file info.
                $data['file_info'] = pathinfo($file['name'][0]);
                $data['file_name'] = $data['file_info']['filename'];
                $data['extension'] = $data['file_info']['extension'];
                $data['user_id'] = $this->get_user_id();
                $data['format'] = $this->input->get('format');
                $data['random'] = "doc" . time();

                // Moves a file to a upload directory.
                move_uploaded_file($file['tmp_name'][0], __dir__ . '/../../upload/' .
                    $data['random'] . '.' . $data['extension']);

                // Creating a record in the DB.
                $this->ret['doc_id'] = $this->cpd_model->upload_docs($data);
                $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
            } catch (\Exception $e) {
                $this->ret[$this->status_string] = $this->app->get_status('fail',
                    'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
                $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
                $this->ret[$this->message_string] = $this->app->get_message('fail');
            }
            echo json_encode($this->ret);
        }
    }

    /**
     *
     * Gets the list of docs available for a category.
     */
    function get_docs()
    {
        $this->check_ajax();
        try {
            $user_id = $this->get_user_id();
            $format = $this->input->get('format');
            $type = $this->input->get('type');
            $category = $this->input->get('category');

            $this->ret['docs'] = $this->cpd_model->get_docs($user_id, $format, $type, $category);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Play/Preview Doc in application.
     */
    function play_doc()
    {
        try {
            // Get the inputs.
            $id = $this->input->getInt('id');
            $file_info = $this->cpd_model->get_doc_info($id);
            $format = $file_info[0]->cd_format;

            $this->read_file($format, $file_info);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
    }

    /**
     *
     * Reads a file.
     * @param $format       String Format for file.
     * @param $file_info    String File Information
     */
    function read_file($format, $file_info)
    {
        // Read file.
        if ('video' === $format) {
            header('Content-Type: video/mp4');
            readfile(__dir__ . '/../../upload/' . $file_info[0]->cd_random_name . '.mp4');
        } else if ('audio' === $format) {
            header('Content-Type: audio/mp3');
            readfile(__dir__ . '/../../upload/' . $file_info[0]->cd_random_name . '.mp3');
        } else if ('pdf' === $format) {
            header('Content-Type: application/pdf');
            readfile(__dir__ . '/../../upload/' . $file_info[0]->cd_random_name . '.pdf');
        }
    }

    /**
     * Fetches the Client Information.
     */
    function get_client_info()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('client_id');

            $this->ret['info'] = $this->cpd_model->get_client_info($id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);

    }

    /**
     * Returns email body HTML.
     *
     * @param $link     String Link to be shared in email.
     * @param $id       Int    Id to append.
     * @return string
     */
    function get_email_body($link, $id)
    {
        return shell_exec('php ' . __dir__ . '/../views/cpdv1/email_body.php ' . $link . ' ' . $id);
    }

    /**
     *
     * Updates the content of buyer.
     */
    function update_amount()
    {
        $this->check_ajax();
        try {

            // Get inputs.
            $cb_id = $this->input->getInt('id');
            $col = $this->input->get('col');
            $val = $this->input->get('val', '', 'RAW');
            $table = $this->input->get('table');
            $pk = $this->input->get('pk');

            $this->ret['cpd_updated'] = $this->cpd_model->update_amount($cb_id, $val, $col, $table, $pk);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     * Renders the script page.
     *
     * @return mixed
     */
    function script()
    {
        $this->ret[$this->render_string] = 'views/tsl/script.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     *
     *  Updates Floor Target Game Values.
     */
    function update_ftg()
    {
        $this->check_ajax();
        try {
            // Fetch inputs
            $val = $this->input->getInt('val');
            $type = $this->input->get('type');
            $quarter = $this->input->getInt('quarter');
            $year = $this->input->getInt('year');
            $col = $this->input->get('col');
            $cpd_id = $this->input->getInt('cpd_id');

            $this->ret['ftg_updated'] = $this->cpd_model->update_ftg($val, $type, $quarter, $year, $col, $cpd_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Fetches and display infomation.
     */
    function get_info()
    {
        $this->check_ajax();
        try {
            $client_id = $this->input->getInt('client_id');

            $this->ret['info'] = $this->cpd_model->get_info($client_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Updates client information in the database.
     */
    function update_client_info()
    {
        $this->check_ajax();
        try {
            // Get inputs.
            $client_id = $this->input->getInt('client_id');
            $data = $this->input->get('info', '', 'RAW');
            $col = $this->input->get('col');

            $this->ret['data_updated'] = $this->cpd_model->update_client_info($client_id, $data, $col);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }

        echo json_encode($this->ret);
    }

    /**
     * Renders the Package page.
     *
     * @return mixed
     */
    function page()
    {
        $this->ret[$this->render_string] = 'views/tsl/page.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     * Renders Assembly Page.
     *
     * @return mixed
     */
    function assembly()
    {
        $this->ret[$this->render_string] = 'views/cpdv1/assembler.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     * Renders the email/attachment page.
     *
     * @return mixed
     */
    function email()
    {
        $this->ret[$this->render_string] = 'views/tsl/email.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     * Renders and return the message page.
     *
     * @return mixed
     */
    public function followup()
    {
        $this->ret[$this->render_string] = 'views/tsl/message.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     * Renders Strategic Advisory Page.
     *
     * @return mixed
     */
    function offering()
    {
        $this->ret[$this->render_string] = 'views/cpdv1/sa.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     *
     * Fetches Strategic Advisory instance outlines.
     */
    function get_sa_outline()
    {
        $this->check_ajax();
        try {
            // Get Inputs
            $page_template = $this->input->get('page_template');
            $this->ret['sa_outline'] = $this->cpd_model->get_sa_outline($page_template);

            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }


    /**
     *
     * Fetches Strategic advisory List.
     */
    function get_sa_list()
    {
        $this->check_ajax();
        try {
            // Fetch inputs.
            $sa_type = $this->input->get('sa_type');
            $user_id = $this->get_user_id();

            $this->ret['list'] = $this->cpd_model->get_sa_list($sa_type, $user_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Uploads Strategic advisor background image.
     */
    function upload_sa()
    {
        $this->check_ajax();
        $id = $this->input->getInt('id');
        $page = $this->input->get('page');
        $number = $this->input->get('number');

        $uploaded_image = $this->cpd_model->get_sa_uploads($id, $page);

        // Check if the image is already present. If so remove it.
        !empty($uploaded_image[0]->cs_data_image) ?
            unlink(__dir__ . '/../../thumbnails/' . $uploaded_image[0]->cs_data_image) : '';

        foreach ($_FILES as $file) {
            try {
                $file_name = pathinfo($file['name'][0]);

                // Check If file extension is png or jpf.
                if (!('png' === $file_name['extension'] || 'jpg' === $file_name['extension']
                    || 'jpeg' === $file_name['extension'])) {
                    $this->invalid_pdf_exception();
                }
                $random = "sa" . time();
                move_uploaded_file($file['tmp_name'][0], __dir__ . '/../../thumbnails/' .
                    $random . '.' . $file_name['extension']);
                $random = $random . '.' . $file_name['extension'];

                $this->ret['sa_uploaded'] = $this->cpd_model->upload_sa($random, $id, $page, $number);
                $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
            } catch (\Exception $e) {
                $this->ret[$this->status_string] = $this->app->get_status('fail',
                    'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
                $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
                $this->ret[$this->message_string] = $this->app->get_message('fail');
            }
            echo json_encode($this->ret);
        }
    }

    /**
     *
     * Upload company and team logo.
     */
    function upload_logo()
    {
        $this->check_ajax();
        foreach ($_FILES as $file) {
            try {
                $sa_id = $this->input->get('sa_id');
                $type = $this->input->get('type');

                // Get file name.
                $file_name = pathinfo($file['name'][0]);
                (!('png' === $file_name['extension'] || 'jpg' === $file_name['extension']
                    || 'jpeg' === $file_name['extension'])) ?
                    $this->invalid_pdf_exception() : '';

                // Set the random name for file to be uploaded.
                $random = "sa" . time();
                move_uploaded_file($file['tmp_name'][0], __dir__ . '/../../thumbnails/' .
                    $random . '.' . $file_name['extension']);
                $this->ret['name'] = $random . '.' . $file_name['extension'];

                // Creating a record in the DB.
                $this->ret['logo_uploaded'] = $this->cpd_model->upload_logo($random . '.' .
                    $file_name['extension'], $sa_id, $type);
                $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
            } catch (\Exception $e) {
                $this->ret[$this->status_string] = $this->app->get_status('fail',
                    'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
                $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
                $this->ret[$this->message_string] = $this->app->get_message('fail');
            }
        }
        echo json_encode($this->ret);
    }


    /**
     *
     * Save Strategic Advisory Data.
     */
    function save_sa_data()
    {
        $this->check_ajax();
        try {

            // Get inputs.
            $data = $this->input->get('data', '', 'RAW');

            $sa_type = $this->input->get('sa');
            $page = $this->input->get('page');
            $template = $this->input->get('template');
            $image = $this->input->get('bg_image');
            $sa_id = $this->input->getInt('sa_id');

            $this->ret['saved_sa'] = $this->cpd_model->save_sa_data($sa_id, $data, $template, $image, $page, $sa_type);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Compile a SA document.
     */
    function compile_sa()
    {
        $this->check_ajax();
        try {
            // Get inputs.
            $sa_id = $this->input->get('sa_id');
            $sa_type = $this->input->get('sa_type', '', 'RAW');
            $sa_data_logo = $this->cpd_model->get_sa_data($sa_id);
            $sa_data = $sa_data_logo['data'];
            $sa_logo = $sa_data_logo['logo'][0];

            $sa_info = $this->cpd_model->get_sa_info($sa_id, $this->get_user_id());

            // Check if the PDF for this SA already exist if so remove it.
            !empty($sa_info[0]->cs_random_name) ?
                unlink(__dir__ . '/../../upload/' . $sa_info[0]->cs_random_name . '.pdf') : '';

            // Create a new PDF document.
            $pdf = new PST_PDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->setFooterContent($sa_logo->cs_company_logo, $sa_logo->cs_team_logo, $sa_type);

            // Set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Roth Methods');

            // Set PDF properties.
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Promotional Document');
            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->SetKeywords('Roth Methods, Promotional document');
            $pdf->setTitle('Promotional Document');

            // Name for PDF.
            $random = "promo" . time();

            // Loop through each Strategic Analysis page data.
            foreach ($sa_data as $sa_info) {
                $page = $sa_info->cs_data_page;
                $data = $sa_info->cs_data_data;
                $image = $sa_info->cs_data_image_1;

                // Check if image is present.
                if (null === $image) {
                    $image_info = $this->cpd_model->get_default_sa_image($page);
                    $image = $image_info[0]->csd_image1;
                }
                $random = "promo" . time();

                // If page type is lead page.
                if ('lp' === $page) {
                    $pdf = $this->compose_lead_page($pdf, $data, $image);
                } // Check if page type is Welcome page.
                else if ('wp' === $page) {
                    $pdf = $this->compose_welcome_page($pdf, $data, $image);
                    // -- Long Pic Left- Text Right - One column -- //
                } // Check if page type is project page.
                else if ('pp' === $page) {
                    $pdf = $this->compose_project_page($pdf, $data, $image);
                } // Check if page type is Analysis page.
                else if ('ap' === $page) {
                    $pdf = $this->compose_analysis_page($pdf, $data);

                } // Check for recommendation Page.
                else if ('rp' === $page) {
                    $pdf = $this->compose_recommendation_page($pdf, $data, $sa_info, $image);

                } // Check for advisory map page
                else if ('amp' === $page) {
                    $pdf = $this->compose_advisory_page($pdf, $data, $image);
                }
            }

            // Set the Header for PDF.
            $pdf->setHeaderData('', 0, '', '<table cellspacing="0" cellpadding="1" border="1">'
                . '<tr><td rowspan="3">test</td><td>test</td></tr></table>', array(0, 0, 0), $lc = array(0, 0, 0));

            // Output PDF document.
            $pdf->Output(__dir__ . '/../../upload/' . $random . '.pdf', 'F');
            $this->ret['composed_sa'] = $this->cpd_model->compile_sa($random, $sa_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     * Compose Advisory map page for PDF.
     *
     * @param $pdf      mixed Pdf instance.
     * @param $data     Array Data for PDF.
     * @param $image    String Image Path.
     * @return mixed
     */
    function compose_advisory_page($pdf, $data, $image)
    {
        $pdf->resetHeaderTemplate();

        // Top Left Message – Center Box Program Map //
        $pdf->AddPage('L', 'A4');
        $pdf->SetFont('centurygothic', '', 25, '', 'false');

        $pdf->setPageUnit('pt');
        $pdf->Image(__dir__ . '/../../thumbnails/' . $image, 0, 0, 950, 600, '', '', '', false, 300, '', false,
            false, 0, false, false, false, false, array());

        // set alpha to semi-transparency
        $pdf->SetAlpha(0.7);

        // draw green square
        $pdf->SetFillColor(84, 110, 132);
        $pdf->SetDrawColor(84, 110, 132);
        $pdf->Rect(0, 80, 450, 50, 'DF');
        $pdf->setAlpha(1);

        // Fetch Page Header html.
        $html = shell_exec('php ' . __dir__ . '/../views/pdf_body/get_advisory_page_header.php');

        $pdf->SetAbsXY(0, -5);
        $pdf->writeHTML($html);

        // set alpha to semi-transparency
        $pdf->SetAlpha(0.7);

        $pdf->SetFont('centurygothic', '', 12, '', 'false');
        // draw green square
        $pdf->SetFillColor(84, 110, 132);
        $pdf->SetDrawColor(84, 110, 132);
        $pdf->Rect(0, 200, 450, 280, 'DF');
        $pdf->setAlpha(1);

        // Fetch Page header footer.
        $data = str_replace("\'", "'", $data);
        $data = preg_replace('/\n/', ' ', $data);
        $html = shell_exec("php " . __dir__ . "/../views/pdf_body/get_advisory_page_footer.php \"" . $data . "\"");

        $pdf->SetAbsXY(0, 200);
        $pdf->writeHTML($html);
        $pdf->setPrintHeader(true);

        return $pdf;
    }

    /**
     * Compose Recommendation page for PDF,
     *
     * @param $pdf      mixed  PDF instance.
     * @param $data     Array  Data for Pdf.
     * @param $sa_info  Array  Data for image to be inserted.
     * @param $image    String Image path
     * @return mixed
     */
    function compose_recommendation_page($pdf, $data, $sa_info, $image)
    {
        $pdf->SetHeaderMargin(0);
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setHeaderText('RECOMMENDATION');

        // Long Pic Left- Text Right - One column //
        $pdf->AddPage('L', 'A4');
        $pdf->SetFont('centurygothic', '', 11, '', 'false');
        $pdf->SetTextColor(0, 0, 0);
        $image2 = $sa_info->cs_data_image_2;

        // Check if the second image for recommendation page is null, if so set it to default image.
        if ($image2 === null) {
            $image_info = $this->cpd_model->get_default_sa_image($page);
            $image2 = $image_info[0]->csd_image2;
        }

        $pdf->setPageUnit('px');

        // Set Left Image
        $pdf->Image(__dir__ . '/../../thumbnails/' . $image, 20, 70, 200, 450, '', '', '', true, 300, '', false, false,
            0, false, false, false, false, array());

        // HTML for page.
        $html = shell_exec("php " . __dir__ . "/../views/pdf_body/get_recommendation_page.php \"" . $data . "\"");

        // Right Image.
        $pdf->Image(__dir__ . '/../../thumbnails/' . $image2, 620, 70, 200, 450, '', '', '', true, 300, '', false,
            false, 0, false, false, false, false, array());

        $pdf->SetAbsXY(205, 60);
        $html = stripslashes($html);
        $pdf->writeHTML($html);

        return $pdf;
    }

    /**
     * Compose Lead Page for PDF.
     *
     * @param $pdf      mixed PDF instance
     * @param $data     Array Data to be inserted.
     * @param $image    string Image path.
     * @return mixed
     */
    function compose_lead_page($pdf, $data, $image)
    {
        $pdf->setHeaderTemplateAutoreset(true);

        // Full Page Pic - Title Box left - Team-Box-Right //
        $pdf->AddPage('L', 'A4');

        $pdf->setPageUnit('px');
        $pdf->Image(__dir__ . '/../../thumbnails/' . $image, 0, 0, 950, 600, '', '', '', false, 300, '', false, false,
            0, false, false, false, false, array());

        // set alpha to semi-transparency
        $pdf->SetAlpha(0.8);

        // draw green square
        $pdf->SetFillColor(84, 110, 132);
        $pdf->SetDrawColor(84, 110, 132);
        $pdf->Rect(0, 140, 450, 220, 'DF');
        $pdf->setAlpha(1);
        $data = stripslashes($data);

        // Set the body for Lead page.
        $pdf->SetFont('centurygothic', '', 12, '', 'false');
        $data = str_replace('"', '\'', $data);

        // Get the html for lead page.
        $html = shell_exec("php " . __dir__ . "/../views/pdf_body/get_lead_page.php \"" . $data . "\"");

        $pdf->SetAbsXY(0, 140);
        $pdf->writeHTML($html);

        return $pdf;
    }

    /**
     * Compose welcome page for PDF.
     *
     * @param $pdf      mixed  PDF instance.
     * @param $data     Array  Data for PDF.
     * @param $image    String Image path.
     * @return mixed
     */
    function compose_welcome_page($pdf, $data, $image)
    {
        $pdf->SetHeaderMargin(0);
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setHeaderText('WELCOME');

        // Long Pic Left- Text Right - One column //
        $pdf->AddPage('L', 'A4');
        $pdf->SetFont('centurygothic', '', 11, '', 'false');
        $pdf->SetTextColor(0, 0, 0);

        $pdf->setPageUnit('px');
        $pdf->Image(__dir__ . '/../../thumbnails/' . $image, 37, 83, 290, 420, '', '', '', true, 300, '', false,
            false, 0, false, false, false,
            $alt = false, $altimgs = array());

        $html = shell_exec("php " . __dir__ . "/../views/pdf_body/get_welcome_page.php \""
            . $data . "\"");

        $pdf->SetAbsXY(300, 20);
        $pdf->writeHTML($html);
        $pdf->setPrintHeader(true);

        return $pdf;
    }

    /**
     * Compose Analysis page for PDF.
     *
     * @param $pdf      mixed PDF instance
     * @param $data     Array Data for PDF to be inserted.
     * @return mixed
     */
    function compose_analysis_page($pdf, $data)
    {
        $pdf->SetHeaderMargin(0);
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setHeaderText('STRATEGIC ANALYSIS');

        // Long Pic Left- Text Right - One column //
        $pdf->AddPage('L', 'A4');
        $pdf->SetFont('centurygothic', '', 10.5, '', 'false');
        $pdf->SetTextColor(0, 0, 0);
        $pdf->setPageUnit('px');

        $array = explode('<h1>Column 2</h1>', str_replace('<h1><br></h1>', '',
            str_replace('<div><br></div>', '',
                str_replace('<h1>STRATEGIC ANALYSIS</h1>', '', $data))));

        $html = shell_exec("php " . __dir__ . "/../views/pdf_body/get_analysis_page_header.php \""
            . $array[0] . "\"");

        $html = stripslashes($html);

        $pdf->SetAbsXY(20, 28);
        $pdf->writeHTML($html);

        $html = shell_exec("php " . __dir__ . "/../views/pdf_body/get_analysis_page_footer.php \""
            . $array[1] . "\"");

        $html = stripslashes($html);
        $pdf->SetAbsXY(425, 15);
        $pdf->writeHTML($html);
        return $pdf;
    }

    /**
     * Compose Project Page for PDF.
     *
     * @param $pdf   mixed PDF instance.
     * @param $data  Array Data for PDF.
     * @param $image String Image path.
     * @return mixed
     */
    function compose_project_page($pdf, $data, $image)
    {
        $pdf->SetHeaderMargin(0);
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setHeaderText('PROJECT OVERVIEW');

        // Long Pic Left- Text Right - One column //
        $pdf->AddPage('L', 'A4');
        $pdf->SetTextColor(0, 0, 0);

        $pdf->setPageUnit('px');
        $pdf->Image(__dir__ . '/../../thumbnails/' . $image, 525, 80, 280, 430, '', '', '', true, 300, '', false, false,
            0, false, false, false, false, array());

        $tagvs = array('h1' => array(0 => array('h' => 1, 'n' => 3), 1 => array('h' => 1, 'n' => 2)),
            'h2' => array(0 => array('h' => 1, 'n' => 2), 1 => array('h' => 1, 'n' => 1)));
        $pdf->setHtmlVSpace($tagvs);

        $data = stripslashes($data);
        $data = str_replace("\"", "'", $data);

        $pdf->SetFont('centurygothic', 'B', 10.5, '', 'false');

        $html = shell_exec("php " . __dir__ . "/../views/pdf_body/get_project_page.php \""
            . $data . "\"");

        $pdf->SetAbsXY(20, 50);
        $pdf->writeHTML($html);

        return $pdf;
    }

    /**
     *
     * Generates preview of SA doc.
     */
    function view_sa()
    {
        try {
            // Get the inputs
            $id = $this->input->getInt('id');
            $file_info = $this->cpd_model->get_sa_info($id);
            header('Content-Type: application/pdf');

            readfile(__dir__ . '/../../upload/' . $file_info[0]->cs_random_name . '.pdf');
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }

        echo json_encode($this->ret);
    }


    /**
     *
     * Compose a new Strategic Advisory Doc.
     */
    function compose_sa()
    {
        $this->check_ajax();
        try {
            // Get inputs.
            $client_id = $this->input->getInt('client_id');
            $user_id = $this->get_user_id();
            $sa_type = $this->input->get('sa_type');

            $this->ret['new_sa'] = $this->cpd_model->compose_sa($user_id, $sa_type, $client_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Updates Strategic advisory doc name.
     */
    function update_sa_name()
    {
        $this->check_ajax();
        try {
            // Get inputs.
            $sa_id = $this->input->getInt('pk');
            $value = $this->input->get('value', '', 'RAW');

            $this->ret['name_updated'] = $this->cpd_model->update_sa_name($sa_id, $value);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Adds client info.
     */
    function add_client_info()
    {
        $this->check_ajax();
        try {
            // Get Inputs
            $input = $this->input;
            $data['entity'] = $input->getString('entity_name');
            $data['title'] = $input->getString('title');
            $data['name'] = $input->getString('name');
            $data['office_phone'] = $input->getString('office_phone');
            $data['cell_phone'] = $input->getString('cell_phone');
            $data['email'] = $input->getString('email');
            $data['website'] = $input->getString('website');
            $data['messaging'] = $input->getString('messaging');
            $data['social_media'] = $input->getString('social_media');
            $data['address'] = $input->getString('address');
            $data['ref'] = $input->getString('client_ref');
            $data['event'] = $input->getString('client_event');
            $is_buyer = $input->getString('is_buyer');
            $is_owner = $this->input->getString('is_owner');

            $info_type = $this->input->get('info_type');
            $client_id = $this->input->getInt('client_id');
            $type = $this->input->getString('relation_type');

            $this->ret['client_info_added'] =
                $this->cpd_model->add_client_info($data, $client_id, $type, $info_type, $is_buyer, $is_owner);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Fetches Property Info.
     */
    function get_prop_info()
    {
        $this->check_ajax();
        try {
            // Get inputs.
            $info_type = $this->input->get('type');
            $client_id = $this->input->getInt('client_id');
            $is_buyer = $this->input->get('is_buyer');
            $is_owner = $this->input->get('is_owner');

            $this->ret['prop_info'] = $this->cpd_model->get_prop_info($client_id, $info_type, $is_buyer, $is_owner);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     * Returns Worksheet view.
     *
     * @return mixed
     */
    function wks()
    {
        $this->ret['render'] = 'views/tsl/script.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     *
     * Updates Property Info.
     */
    function update_prop_info()
    {
        $this->check_ajax();
        try {
            // Get inputs.
            $id = $this->input->getInt('id');
            $col = $this->input->get('col');
            $value = $this->input->get('value', '', 'RAW');

            $this->ret['prop_info_updated'] = $this->cpd_model->update_prop_info($id, $col, $value);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     * Renders Web page.
     *
     * @return mixed
     */
    function web()
    {
        $this->ret[$this->render_string] = 'views/cpdv1/web.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     *
     * Generates preview of SA doc
     */
    function view_ppt()
    {
        try {
            // Get the inputs
            header("Content-type: application/octet-stream");
            header('Content-disposition: attachment; filename=' .
                __dir__ . '/../../upload_ppt/1Presentation.pptx');
            readfile(__dir__ . '/../../upload_ppt/1Presentation.pptx');

        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }

        echo json_encode($this->ret);
    }

    /**
     *
     * Updates a deal content.
     */
    function update_deal()
    {
        $this->check_ajax();
        try {
            // Get Inputs.
            $id = $this->input->getInt('id');
            $col = $this->input->get('col');
            $val = $this->input->get('val', '', 'RAW');

            $this->ret['deal_updated'] = $this->cpd_model->update_deal($id, $col, $val);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Adds a new lead.
     */
    function add_lead()
    {
        $this->check_ajax();
        try {
            // Get inputs.
            $name = $this->input->getString('lead_name');
            $type = $this->input->get('lead_type');
            $user_id = $this->get_user_id();

            $this->ret['lead_added'] = $this->cpd_model->add_lead($user_id, $type, $name);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Fetches the list of sent components for a deal.
     */
    function view_sent()
    {
        $this->check_ajax();
        try {
            // Get inputs.
            $deal_id = $this->input->getInt('deal_id');
            $type = $this->input->get('type');
            $component_name = $this->input->getString('component');

            $table_name = $this->input->getString('table');
            $col_name = $this->input->getString('col');
            $ref_key = $this->input->getString('ref_key');

            $this->ret['sent_items'] = $this->cpd_model->view_sent
            ($deal_id, $type, $component_name, $col_name, $table_name, $ref_key);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);

        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Fetches Strategic Advisory details.
     */
    function get_sa_details()
    {
        $this->check_ajax();
        try {
            // Get inputs.
            $sa_id = $this->input->getInt('sa_id');
            $user_id = $this->get_user_id();

            $this->ret['sa_details'] = $this->cpd_model->get_sa_details($sa_id, $user_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);

    }

    /**
     *
     * Fetches Update Builder details.
     */
    function get_update_details()
    {
        $this->check_ajax();
        try {
            // Get inputs.
            $update_id = $this->input->getInt('update_id');
            $user_id = $this->get_user_id();

            $this->ret['update_details'] = $this->cpd_model->get_update_details($update_id, $user_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);

    }

    /**
     *
     * Fetches Docs Details.
     */
    function get_docs_details()
    {
        $this->check_ajax();
        try {
            // Get inputs.
            $doc_id = $this->input->getInt('doc_id');
            $user_id = $this->get_user_id();

            $this->ret['doc_details'] = $this->cpd_model->get_docs_details($doc_id, $user_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Deletes a document instance from the database.
     */
    function delete_doc()
    {
        $this->check_ajax();
        try {
            $doc_id = $this->input->getInt('doc_id');

            $this->ret['doc_deleted'] = $this->cpd_model->delete_doc($doc_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Deletes a lead instance.
     */
    function delete_lead()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('lead_id');

            $this->ret['lead_deleted'] = $this->cpd_model->delete_lead($id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);

        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }

        echo json_encode($this->ret);
    }

    /**
     *
     * Fetches Survey Details.
     */
    function get_survey_details()
    {
        $this->check_ajax();
        try {
            // Get the inputs.
            $survey_id = $this->input->getInt('survey_id');
            $user_id = $this->get_user_id();

            $this->ret['survey_details'] = $this->cpd_model->get_survey_details($survey_id, $user_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }

        echo json_encode($this->ret);
    }

    /**
     *
     * Fetches Profile Details.
     */
    function get_profile_details()
    {
        $this->check_ajax();
        try {
            // Get the inputs.
            $profile_id = $this->input->getInt('profile_id');
            $user_id = $this->get_user_id();

            $this->ret['profile_details'] = $this->cpd_model->get_profile_details($profile_id, $user_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }

        echo json_encode($this->ret);
    }

    /**
     *
     * Updates Notes Content.
     */
    function update_notes_content()
    {
        $this->check_ajax();
        try {
            // Get the inputs.
            $deal_id = $this->input->getInt('id');
            $col = $this->input->get('col');
            $seq = $this->input->getInt('seq');
            $value = $this->input->getString('val');

            // Check if all the inputs are present.
//            if (empty($deal_id) || empty($col) || empty($value)) {
//                $this->input_missing_exception();
//            }

            $this->ret['note_updated'] = $this->cpd_model->update_notes_content($deal_id, $col, $seq, $value);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }

        echo json_encode($this->ret);
    }

    /**
     *
     * Fetches Notes content for the CPD.
     */
    function get_notes_content()
    {
        $this->check_ajax();
        try {
            $deal_id = $this->input->getInt('id');

            $this->ret['notes_content'] = $this->cpd_model->get_notes_content($deal_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }

        echo json_encode($this->ret);
    }

    /**
     *
     * Fetch all the criteria and Target Audience that belong to a user.
     */
    function get_all_ta_criteria()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $product_id = $this->app->get_product();

            $this->ret['ta_criteria'] = $this->cpd_model->get_all_ta_criteria($user_id, $product_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }

        echo json_encode($this->ret);
    }

    /**
     *
     * Fetch all the CPD Metric data(Deal Count, Deal Price, Deal Amount).
     */
    function get_cpd_metric()
    {
        $this->check_ajax();

        try {
            // Get the inputs.
            $id = $this->input->getInt('id');
            $active_tab = $this->input->get('tab');
            $user_id = $this->get_user_id();
            $quarter = $this->input->getInt('quarter');

            $this->ret['metric'] = $this->cpd_model->get_cpd_metric($id, $active_tab, $user_id, $quarter);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Fetches and displays Story Builder.
     */
    function review()
    {
        $ret[$this->render_string] = 'views/tsl/message.php';
        $ret['data'] = $this->view_data;
        return $ret;
    }

    /*
     * Returns Proposal Builder View.
     */
    function proposal()
    {
        $this->ret[$this->render_string] = 'views/tsl/profile.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     * Returns Agreement Page.
     *
     * @return mixed
     */
    function agreement()
    {
        $this->ret[$this->render_string] = 'views/tsl/updates.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    /**
     *
     * Uploads an assembler page.
     */
    function upload_page()
    {
        $this->check_ajax();

        foreach ($_FILES as $file) {
            try {
                // Get inputs.
                $data['version_id'] = $this->input->getInt('version_id');
                $data['client_id'] = $this->input->getInt('client_id');
                $data['client_type'] = $this->input->get('client_type');
                $data['doc_del'] = $this->input->get('doc_del');
                $data['doc_format'] = $this->input->get('doc_format');
                $data['doc_type'] = $this->input->get('doc_type');

                // Fetch file info.
                $data['file_info'] = pathinfo($file['name'][0]);
                $data['file_name'] = $data['file_info']['filename'];
                $data['extension'] = $data['file_info']['extension'];
                $data['user_id'] = $this->get_user_id();
                $data['random'] = $data['doc_type'] . time();

                // Moves a file to a upload directory.
                move_uploaded_file($file['tmp_name'][0], __dir__ . '/../../upload/' .
                    $data['random'] . '.' . $data['extension']);

                // Creating a record in the DB.
                $this->ret['page_id'] = $this->cpd_model->upload_page($data);
                $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
            } catch (\Exception $e) {
                $this->ret[$this->status_string] = $this->app->get_status('fail',
                    'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
                $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
                $this->ret[$this->message_string] = $this->app->get_message('fail');
            }
            echo json_encode($this->ret);
        }
    }


    /**
     *
     * Adds a new assembler instance.
     */
    function add_assembler_instance()
    {
        $this->check_ajax();
        try {
            $client_id = $this->input->getInt('client_id');
            $this->ret['instance_added'] = $this->cpd_model->add_assembler_instance($client_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);

    }

    /**
     *
     * Fetched ans returns assembler instance.
     */
    function get_assembler_instance()
    {
        $this->check_ajax();

        try {
            $client_id = $this->input->getInt('client_id');
            $this->ret['instances'] = $this->cpd_model->get_assembler_instance($client_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Reads PDF file for assembler.
     */
    function preview_assembler()
    {
        // Get the inputs.
        $id = $this->input->getInt('id');
        $file_info = $this->cpd_model->get_assember_info($id);

        $this->read_file('pdf', $file_info);
    }

    /**
     *
     * Preview Single Page.
     */
    function preview_page()
    {
        // Get the inputs.
        $id = $this->input->getInt('id');
        $file_info = $this->cpd_model->get_page_info($id);

        $this->read_file('pdf', $file_info);
    }

    /**
     *
     * Fetches and returns list of assembler pages.
     */
    function fetch_assembler_pages()
    {
        $this->check_ajax();

        try {
            $doc_id = $this->input->getInt('doc_id');

            $this->ret['pages'] = $this->cpd_model->get_assembler_pages($doc_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Merge Pages and create a doc out of it.
     */
    function merge_pages()
    {
        $this->check_ajax();
        try {
            $version_id = $this->input->getInt('version_id');
            $pages = $this->cpd_model->get_assembler_pages($version_id);

            $pdf = new Fpdi();

            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(false);

            // iterate through the files
            foreach ($pages as $page) {
                // get the page count
                $pageCount = $pdf->setSourceFile(__dir__ . '/../../upload/' . $page->cap_rand_name . '.pdf');
                // iterate through all pages
                for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                    // import a page
                    $templateId = $pdf->importPage($pageNo);
                    // get the size of the imported page
                    $size = $pdf->getTemplateSize($templateId);

                    // add a page with the same orientation and size
                    $pdf->AddPage($size['orientation'], $size);

                    // use the imported page
                    $pdf->useTemplate($templateId);

                    $pdf->SetFont('Helvetica');
                    $pdf->SetXY(5, 5);
                }
            }

            // Setup the random name for new PDF file.
            $random = 'assemble' . time();
            $filename = __dir__ . '/../../upload/' . $random . '.pdf';

            // Check if the image is already present. If so remove it.
            $prev_rand_name = $this->cpd_model->get_assembler_random($version_id);
            !empty($prev_rand_name[0]->cav_rand_name) ?
                unlink(__dir__ . '/../../upload/' . $prev_rand_name[0]->cav_rand_name . '.pdf') : '';

            // Output the new PDF
            $pdf->Output($filename, 'F');
            $this->ret['doc_updated'] = $this->cpd_model->update_assembly_doc($random, $version_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Move a page up.
     */
    function move_page_up()
    {
        $this->check_ajax();
        try {
            $page_id = $this->input->getInt('page_id');
            $order = $this->input->getInt('order');
            $version = $this->input->getInt('version');

            $this->ret['page_moved'] = $this->cpd_model->move_page_up($page_id, $order, $version);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Moves a page down.
     */
    function move_page_down()
    {
        $this->check_ajax();
        try {
            $page_id = $this->input->getInt('page_id');
            $order = $this->input->getInt('order');
            $version = $this->input->getInt('version');

            $this->ret['page_moved'] = $this->cpd_model->move_page_down($page_id, $order, $version);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Deletes a Page from Document.
     */
    function delete_page()
    {
        $this->check_ajax();
        try {
            $page_id = $this->input->getInt('id');
            $file_name = $this->cpd_model->get_page_info($page_id)[0]->cd_random_name;

            // Check if the page PDF is present. If so remove it.
            !empty($file_name) ?
                unlink(__dir__ . '/../../upload/' . $file_name . '.pdf') : '';

            $this->ret['page_removed'] = $this->cpd_model->delete_page($page_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Deletes complete assembled document.
     */
    function delete_assembled_doc()
    {
        $this->check_ajax();
        try {
            $doc_id = $this->input->getInt('id');
            $file_name = $this->cpd_model->get_assember_info($doc_id)[0]->cd_random_name;

            // Check if the page PDF is present. If so remove it.
            !empty($file_name) ?
                unlink(__dir__ . '/../../upload/' . $file_name . '.pdf') : '';

            $this->ret['doc_removed'] = $this->cpd_model->delete_assembled_doc($doc_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Updates assembler page name.
     */
    function update_assembler_page_name()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('pk');
            $value = $this->input->getString('value');

            $this->ret['page_name_updated'] = $this->cpd_model->update_assembler_page_name($id, $value);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Updates assembler document name.
     */
    function update_assembler_name()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('pk');
            $value = $this->input->getString('value');


            $this->ret['assembler_name_updated'] = $this->cpd_model->update_assembler_name($id, $value);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Starts/ends call for CPD call control manager.
     */
    function start_end_call()
    {
        $this->check_ajax();

        try {
            $deal_id = $this->input->getInt('deal_id');
            $type = $this->input->getString('type');
            $date = $this->input->getString('date', '', 'RAW');
            $time = $this->input->getString('time', '', 'RAW');
            $ccm_id = $this->input->getInt('ccm');

            $this->ret['call_started_ended'] = $this->cpd_model->start_end_call($deal_id, $type, $date, $time, $ccm_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Fetches and returns call information.
     */
    function get_call_info()
    {
        $this->check_ajax();

        try {
            $deal_id = $this->input->getInt('id');
            $user_id = $this->get_user_id();

            $this->ret['call_info'] = $this->cpd_model->get_call_info($deal_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Updates call purpose for call control manager instance.
     */
    function update_call_purpose()
    {
        $this->check_ajax();

        try {
            $ccm = $this->input->getInt('ccm');
            $val = $this->input->getString('val');

            $this->ret['call_purpose_updated'] = $this->cpd_model->update_call_purpose($ccm, $val);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Fetches and returns call control record information.
     */
    function get_ccr_info()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');

            $this->ret['ccr_info'] = $this->cpd_model->get_ccr_info($id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Updates Call rating.
     */
    function update_call_rating()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $val = $this->input->getInt('val');

            $this->ret['call_rating_updated'] = $this->cpd_model->update_call_rating($id, $val);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Upload information popup image.
     */
    function upload_info_image()
    {
        $this->check_ajax();
        try {
            foreach ($_FILES as $file) {
                $cc_id = $this->input->getInt('cc_id');
                $random = "u_img" . time();

                $prev_info_image = $this->cpd_model->get_info_image($cc_id);

                // Check if the image is already present. If so remove it.
                !empty($prev_info_image[0]->cc_image) ?
                    unlink(__dir__ . '/../../upload/' . $prev_info_image[0]->cc_image) : '';

                // Fetch file info.
                $data['file_info'] = pathinfo($file['name'][0]);
                $data['file_name'] = $data['file_info']['filename'];
                $data['extension'] = $data['file_info']['extension'];

                // Moves file to a upload directory.
                move_uploaded_file($file['tmp_name'][0], __dir__ . '/../../upload/' .
                    $random . '.' . $data['extension']);

                // Creating a record in the DB.
                $this->ret['doc_id'] = $this->cpd_model->upload_info_image($cc_id, $random . '.' . $data['extension']);
                $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
            }
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Fetch and return information popup image.
     */
    function get_info_image()
    {
        $img = $this->input->get('img', '', 'RAW');

        header('Content-Type: image/jpeg+png+jpg');
        readfile(__dir__ . '/../../upload/' . $img);
    }

    /**
     *
     * Fetches and returns messages/scripts instance for a user.
     */
    function get_message_script()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();

            $this->ret['message_script'] = $this->cpd_model->get_message_script($user_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Fetches and return message script content.
     */
    function get_message_script_content()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $type = $this->input->getString('type');
            $deal_id = $this->input->getInt('deal_id');

            $this->ret['content'] = $this->cpd_model->get_message_script_content($id, $type, $deal_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Saves client information.
     */
    function save_client_popup_info()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $val = $this->input->getString('val');
            $type = $this->input->getString('type');
            $form_type = $this->input->getString('form-type');

            $this->ret['client_info_saved'] = $this->cpd_model->save_client_popup_info($id, $val, $type, $form_type);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Returns client information.
     */
    function get_client_popup_info()
    {
        $this->check_ajax();

        try {
            $cc_id = $this->input->getInt('id');
            $form_type = $this->input->getString('form-type');


            $this->ret['info'] = $this->cpd_model->get_client_popup_info($cc_id, $form_type);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Updates job field for client information.
     */
    function update_job_field()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $val = $this->input->getString('val');
            $col = $this->input->getString('col');

            $this->ret['field_updated'] = $this->cpd_model->update_job_field($id, $val, $col);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Adds new offer for a buyer.
     */
    function add_buyer_offer()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $date = $this->input->get('date', '', 'RAW');
            $seq = $this->input->getInt('seq');
            $cc_id = $this->input->getInt('cc_id');
            $type = $this->input->get('type');

            $this->ret['add_buyer_offer'] = $this->cpd_model->add_buyer_offer($id, $date, $seq, $cc_id, $type);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Fetches and returns offers made by a buyer.
     */
    function get_buyer_offer()
    {
        $this->check_ajax();

        try {
            $asset_id = $this->input->getInt('asset_id');

            $this->ret['buyer_offer'] = $this->cpd_model->get_buyer_offer($asset_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Updates homebase buyer information data.
     */
    function update_hbi_data()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $val = $this->input->getString('val');
            $col = $this->input->getString('col');
            $seq = $this->input->getInt('seq');

            $this->ret['data_udated'] = $this->cpd_model->update_hbi_data($id, $val, $col, $seq);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Fetches and returns Strategic Analysis.
     */
    function fetch_sa()
    {
        $this->check_ajax();

        try {
            $asset_id = $this->input->getInt('id');

            $this->ret['sa'] = $this->cpd_model->fetch_sa($asset_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Saves Strategic analysis data,
     */
    function save_sa()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('id');
            $val = $this->input->get('val', '', 'RAW');
            $type = $this->input->getString('type');
            $seq = $this->input->getInt('seq');

            $this->ret['sa_saved'] = $this->cpd_model->save_sa($id, $val, $type, $seq);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Updates Strategic Analysis Name.
     */
    function update_sa_info_name()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('pk');
            $seq = $this->input->getInt('seq');
            $type = $this->input->getString('type');
            $value = $this->input->getString('value');

            $this->ret['sa_info_name_updated'] = $this->cpd_model->update_sa_info_name($id, $seq, $type, $value);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     * Fetches and returns Survey View.
     *
     * @return mixed
     */
    function survey()
    {
        $this->ret[$this->render_string] = 'views/tsl/survey.php';
        $this->ret['data'] = $this->view_data;
        echo $this->input->get('');
        return $this->ret;
    }

    /**
     *
     * Fetches Collaboration data for the user.
     */
    function get_collaboration_data()
    {
        try {
            $user_id = $this->get_user_id();
            $cpd_id = $this->input->getInt('cpd_id');

            $this->ret['collab_data'] = $this->cpd_model->get_collaboration_data($user_id, $cpd_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Adds a new collaboration request.
     */
    function add_collab()
    {
        $this->check_ajax();

        try {
            $cpd_id = $this->input->getInt('id');
            $sender = $this->input->getString('sender');
            $recipient = $this->input->getInt('recipient');
            $topic = $this->input->getInt('topic');
            $format = $this->input->getInt('format');
            $user_id = $this->get_user_id();
            $date_time = $this->input->get('date_time', '', 'RAW');
            $order = $this->input->getInt('order');
            $user_email = $this->input->get('user_email', '', 'RAW');

            $this->ret['collab_added'] =
                $this->cpd_model->add_collab($cpd_id, $sender, $recipient,
                    $topic, $format, $user_id, $date_time, $order, $user_email);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Delete component collaboration.
     */
    function delete_collab()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');

            $this->ret['collab_deleted'] = $this->cpd_model->delete_collab($id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Updates Deal Conversion Board data
     */
    function update_dcb()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $qtr = $this->input->getInt('qtr');
            $prob = $this->input->getInt('probability');

            $this->ret['dcb_updated'] = $this->cpd_model->update_dcb($id, $qtr, $prob);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Fetches and return deal conversion board data.
     */
    function fetch_dcb()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');

            $this->ret['dcb'] = $this->cpd_model->fetch_dcb($id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function delete_note()
    {
        $this->check_ajax();

        try {
            $deal_id = $this->input->getInt('id');
            $seq = $this->input->getInt('seq');

            $this->ret['note_deleted'] = $this->cpd_model->delete_note($deal_id, $seq);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!! Some Error Occurred. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function generate_sa_pdf()
    {
        $this->check_ajax();

        try {
            $content = \stripslashes($this->input->get('content', '', 'RAW'));
            $asset_id = $this->input->getInt('asset_id');
            $asset_info = $this->cpd_model->get_asset_info($asset_id);

            // create new PDF document
            $pdf = new PST_PDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Roth Methods');
            $pdf->SetSubject('Strategic Analysis Document');
            $pdf->SetKeywords('document, PDF, promotion, roth, advisory');

            // set auto page breaks
            $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            // set image scale factor
            $pdf->setImageScale(1);

            $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $pdf->setPrintFooter(false);
            $pdf->setPrintHeader(false);

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            $pdf->AddPage('P', 'A5');
            $pdf->setPageUnit('px');
            $pdf->SetFont('helvetica', '', 9);

            // Array for PDF vertical spacing.
            $tagvs = array(
                'p' => array(0 => array('h' => 1, 'n' => 1), 1 => array('h' => 1, 'n' => 1)),
                'h1' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
                'h2' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
                'h3' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
                'h5' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
                'h6' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
                'div' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
                'blockquote' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
                'span' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0)),
                'strong' => array(0 => array('h' => 0, 'n' => 0), 1 => array('h' => 0, 'n' => 0))
            );

            $pdf->setHtmlVSpace($tagvs);
            $pdf->writeHTML('<h3 style="text-align: center">Strategic Analysis</h3><p></p>' .
                '<table><tr><td align="left"><h5> Client Name: ' . $asset_info[0]->hc_name . '</h5></td>' .
                '<td align = "center"><h5> Asset Name: ' . $asset_info[0]->hca_name . '</h5></td>' .
                '<td align = "right"><h5> Date:' . date("Y/m/d") . '</h5></td ></tr>' . $content . '</table>');

//            $pdf->SetMargins('50', '0', '30');

//            $pdf->writeHTML($content);
            $random = "strategic_analysis";

            // Print the file out
            $pdf->Output(__dir__ . ' /../../upload/' . $random . '.pdf', 'F');
//            $this->tsl_model->add_promo($random, $id, $is_pdf);

            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!!Some Error Occurred . We are working on getting this Fixed . ';
        }
        echo json_encode($this->ret);
    }

    function add_comps()
    {
        $this->check_ajax();

        try {
            $asset_id = $this->input->getInt('asset_id');

            $this->ret['comps_added'] = $this->cpd_model->add_comps($asset_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!!Some Error Occurred . We are working on getting this Fixed . ';
        }
        echo json_encode($this->ret);
    }

    function get_comps()
    {
        $this->check_ajax();
        try {
            $asset_id = $this->input->getInt('asset_id');

            $this->ret['comps_data'] = $this->cpd_model->get_comps($asset_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!!Some Error Occurred . We are working on getting this Fixed . ';
        }
        echo json_encode($this->ret);
    }

    function update_comps_data()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $val = $this->input->getString('val');
            $col = $this->input->getString('col');

            $this->ret['data_updated'] = $this->cpd_model->update_comps_data($id, $val, $col);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!!Some Error Occurred . We are working on getting this Fixed . ';
        }
        echo json_encode($this->ret);
    }

    function get_comps_info()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');

            $this->ret['comps_info'] = $this->cpd_model->get_comps_info($id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!!Some Error Occurred . We are working on getting this Fixed . ';
        }
        echo json_encode($this->ret);
    }

    function get_revenue_data() {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');

            $this->ret['revenue_data'] = $this->cpd_model->get_revenue_data($id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!!Some Error Occurred . We are working on getting this Fixed . ';
        }
        echo json_encode($this->ret);
    }

    function get_prefer_content()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');

            $this->ret['prefer_content'] = $this->cpd_model->get_prefer_content($id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!!Some Error Occurred . We are working on getting this Fixed . ';
        }
        echo json_encode($this->ret);
    }

    function update_prefer_text()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $val = $this->input->getString('val');
            $col = $this->input->getString('col');

            $this->ret['prefer_text_updated'] = $this->cpd_model->update_prefer_text($id, $val, $col);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!!Some Error Occurred . We are working on getting this Fixed . ';
        }
        echo json_encode($this->ret);
    }

    function update_preference()
    {
        $this->check_ajax();

        try {
            $cpd_id = $this->input->getInt('cpd_id');
            $deal_id = $this->input->get('deal_id', '', 'RAW');

            $this->ret['preferences_updated'] = $this->cpd_model->update_preference($cpd_id, $deal_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = 'Oops!!Some Error Occurred . We are working on getting this Fixed . ';
        }
        echo json_encode($this->ret);
    }
}