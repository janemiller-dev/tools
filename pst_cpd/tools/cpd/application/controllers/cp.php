<?php
/**
 * Content Provider Controller.
 *
 * Content Provider controller that controls all the CP requests
 *
 * @author Sumit K (sumitk@mindfiresolutions.com)
 *
 */

namespace Controller;

class Cp extends Controller
{

    private $cpd_model;
    private $ret, $success_string, $status_string, $message_string, $render_string;

    /**
     * Ensure that the user has access to this tool.
     * @param $app
     */
    function __construct($app)
    {
        parent::__construct($app);
        $this->cp_model = new \Model\CP;
        $this->success_string = 'success';
        $this->status_string = 'status';
        $this->message_string = 'message';
        $this->render_string = 'render';

        $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
    }

    /**
     * Default method.
     */
    function index()
    {
        $this->ret[$this->render_string] = 'views/cp/dashboard.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    function page()
    {
        $this->ret[$this->render_string] = 'views/tsl/page.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

}