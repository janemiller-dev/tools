<?php

namespace Controller;

class Account extends Controller
{

	/**
	 * The user does not have a current account selected. They may need to create one or just select an
	 * existing one.
	 */
	public function index()
	{
		$ret['render'] = 'views/account.php';
		$ret['data'] = $this->view_data;
		return $ret;
	}

	/**
	 * Return the existing accounts for the current user.
	 */
	public function get_accounts()
	{
		$this->check_ajax();

		try {

			// Get the current user ID.
			$user_id = $this->get_user_id();
			if (is_null($user_id))
				throw new Exception("User is not logged in");

			// Find the user's accounts
			$account_model = new \Model\Account;
			$accounts = $account_model->get_accounts($user_id);

			$ret['accounts'] = $accounts;
			$ret['status'] = 'success';
		} catch (Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}

}