<?php
/**
 * Clearslide controller.
 */

namespace Controller;

use function json_encode;

class Clearslide extends Controller
{
    /**
     * Default method.
     */
    function index()
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://oauth.platform.clearslide.com/oauth/authorize?client_id=HTNJPP88AW7Q5LNB78BT&redirect_uri=http%3A%2F%2Flocal-pst%2F&response_type=code&state=test12223",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "postman-token: 1261ba40-635f-8293-07d8-f555da4edc99"
            ),
        ));

        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_HEADER, TRUE);


        $response = curl_exec($curl);
        $last_url = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
        $err = curl_error($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }


    }


}