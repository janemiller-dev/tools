<?php
/**
 * PST PDF controller.
 */

namespace Controller;
require_once __DIR__ . '/../../vendor/setasign/fpdi/src/Tcpdf/Fpdi.php';

class PST_MERGE extends \FPDI
{
    /**
     * "Remembers" the template id of the imported page
     */
    protected $tplId;

    /**
     * Draw an imported PDF logo on every page
     */
    function Header()
    {
    }

    function Footer()
    {
        // emtpy method body
    }

}