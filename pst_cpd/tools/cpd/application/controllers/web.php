<?php
/**
 * Web Presentation Controller
 *
 * Web presentation Controller.
 *
 * @author   Sumit K (sumitk@mindfiresolutions.com)
 * @category Controller
 *
 */

namespace Controller;

use function readfile;

/**
 * Web Presentation class.
 *
 * @author   Sumit K (sumitk@mindfiresolutions.com)
 * @category Controller.
 */
class Web extends Controller
{

    private $ret, $success_string, $status_string, $render_string, $web_model;

    /**
     * Ensure that the user has access to this tool.
     * @param $app
     */
    function __construct($app)
    {
        parent::__construct($app);
        $this->web_model = new \Model\WEB;
        $this->success_string = 'success';
        $this->status_string = 'status';
        $this->message_string = 'message';
        $this->render_string = 'render';

        $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
    }

    /**
     * Default method.
     */
    function index()
    {
        $this->ret[$this->render_string] = 'views/web/web.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    function viewer()
    {
        $this->ret[$this->render_string] = 'views/web/viewer.php';
        $this->ret['data'] = $this->view_data;
        return $this->ret;
    }

    function upload_ppt()
    {
        $this->check_ajax();
        try {
            foreach ($_FILES as $file) {

                // Fetch file info.
                $file_info = pathinfo($file['name'][0]);
                $file_name = $file_info['filename'];
                $extension = $file_info['extension'];
                $user_id = $this->get_user_id();
                $random = 'web-presentation' . $user_id . time();

                // Moves a file to a upload directory.
                move_uploaded_file($file['tmp_name'][0],
                    __dir__ . '/../../../../wordpress_cpd/wp-content/uploads/' .
                    $random . '.' . $extension);

                // Creating a record in the DB.
                $this->ret['ppt_uploaded'] = $this->web_model->upload_ppt($random, $extension, $file_name, $user_id);
                $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
            }
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }

        echo json_encode($this->ret);
    }

    function get_ppt_list()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();

            // Creating a record in the DB.
            $this->ret['ppt_list'] = $this->web_model->get_ppt_list($user_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }

        echo json_encode($this->ret);
    }

    function delete_presentation()
    {
        try {
            $id = $this->input->getInt('id');
            $rand = $this->input->get('rand', '', 'RAW');

            unlink(__dir__ . '/../../../../wordpress_cpd/wp-content/uploads/' . $rand);

            $this->ret['presentation_deleted'] = $this->web_model->delete_presentation($id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }

        echo json_encode($this->ret);
    }

    function view_ppt()
    {
        $file = $this->input->get('u', '', 'RAW');
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize(__dir__ . '/../../upload/' . $file));
        header('Content-Type: application/vnd.openxmlformats-officedocument.presentationml.presentation');
        readfile(__dir__ . '/../../upload/' . $file);
    }
}

