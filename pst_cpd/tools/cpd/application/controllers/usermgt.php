<?php

namespace Controller;

class Usermgt extends Controller
{

	/**
	 * Ensure that the user has access to this tool.
	 */
	function __construct($app)
	{
		parent::__construct($app);
	}

	/**
	 * Default method.
	 */
	function index()
	{
		$ret['render'] = 'views/usermgt/users.php';
		$ret['data'] = $this->view_data;
		return $ret;
	}

	/**
	 * Get the user list as constrained by the specified window an search.
	 */
	public function get_users()
	{
		$this->check_ajax();

		try {
			// Get the list of users from thr user model.
			$user_model = new \Model\User;
			$ret = $user_model->get_users($this->input);
		} catch (\Exception $e) {
			$ret['error'] = $e->getMessage();
		}
		echo json_encode($ret);
	}
}