<?php

/**
 * HomeBase controller.
 */

namespace Controller;

use function is_array;
use function is_integer;
use function json_encode;
use function PasswordCompat\binary\check;
use function print_r;

class Homebase extends Controller
{
    private $homebase_model;
    private $ret, $success_string, $status_string, $message_string, $render_string;

    /**
     * Homebase constructor.
     * @param $app
     */
    function __construct($app)
    {
        parent::__construct($app);
        $this->homebase_model = new \Model\HomeBase;
        $this->ret['status'] = 'success';
        $this->success_string = 'success';
        $this->status_string = 'status';
        $this->message_string = 'message';
        $this->render_string = 'render';
    }

    /**
     * Default method.
     */
    function index()
    {
        $ret['render'] = 'views/homebase/homebase.php';
        $ret['data'] = $this->view_data;
        return $ret;
    }


    /**
     *
     * Fetch the Homebase Clients.
     */
    function get_homebase_client()
    {
        $this->check_ajax();
        try {
            $user_id = $this->get_user_id();
            $product_id = $this->app->get_product();
            $query = $this->input->getString('query');
            $lower_limit = $this->input->getInt('lower_limit');
            $upper_limit = $this->input->getInt('upper_limit');
            $criteria = $this->input->get('criteria');
            $status = $this->input->getString('status');
            $location = $this->input->get('location');
            $sort = $this->input->getString('sort');
            $rwt = $this->input->getString('rwt');
            $order = $this->input->getString('order');
            $product_type = $this->input->getString('product_type');
            $current_page = $this->input->getInt('current_page');
            $offset = $this->input->getInt('offset');
            $data['cond'] = $this->input->get('cond');
            $data['zip'] = $this->input->get('zip');
            $data['state'] = $this->input->get('state');
            $data['city'] = $this->input->get('city');
            $data['unit'] = $this->input->get('unit');
            $data['ft'] = $this->input->get('ft');
            $tool_id = $this->input->getInt('tool_id');
            $tool_type = $this->input->get('tool_type');

            $id = 0;

            if (0 !== $lower_limit) {
                $id = $this->input->getInt('id');
            }

            $this->ret['active_cpd'] = $this->homebase_model->get_active_cpd($user_id, $product_id);
            $this->ret['homebase_criteria'] = $criteria;
            $this->ret['homebase_status'] = $status;
            $this->ret['homebase_location'] = $location;
            $this->ret['product_type'] = $product_type;
            $this->ret['portfolio'] = $this->homebase_model->get_portfolio($user_id);
            $this->ret['portfolio_client'] = $this->homebase_model->get_portfolio_clients($user_id);

            $this->ret['client'] = $this->homebase_model->get_homebase_client
            ($user_id, $product_id, $query, $lower_limit, $upper_limit, $id, $criteria,
                $status, $sort, $order, $location, $product_type, $current_page, $offset, $rwt, $data, $tool_id, $tool_type);

            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Adds new clients and their assets to Homebase.
     */
    function add_client()
    {
        $this->check_ajax();

        try {
            $client_count = $this->input->getInt('client_count');
            $id = $this->input->getInt('id');
            $origin = $this->input->getString('origin');
            $user_id = $this->get_user_id();

            // loop through all the clients and fetch the parameters.
            for ($i = 1; $i <= $client_count; $i++) {
                // Check if client name is set.
                if (!empty($this->input->getString('client_name' . $i))) {
                    $client_args['client_name'] = $this->input->getString('client_name' . $i);
                    $client_args['main_phone'] = $this->input->getString('main_phone' . $i);
                    $client_args['mobile_phone'] = $this->input->getString('mobile_phone' . $i);
                    $client_args['client_email'] = $this->input->getString('client_email' . $i);
                    $client_args['entity_name'] = $this->input->getString('entity_name' . $i);
                    $client_args['second_phone'] = $this->input->getString('second_phone' . $i);
                    $client_args['product_id'] = $this->app->get_product();
                    $client_args['user_id'] = $this->get_user_id();

                    // Insert into homebase client and store the ID.
                    $client_id = $this->homebase_model->add_client($client_args);

                    // Get the count of assets for this client.
                    $asset_count = $this->input->getInt('asset-count' . $i);

                    // loop through asset count.
                    for ($j = 1; $j <= $asset_count; $j++) {
                        $product_id = $this->input->get('asset_product' . $i . '-' . $j, '', 'RAW');
                        $asset_args['asset_name'] = $this->input->getString('asset_name' . $i . '-' . $j);
                        $location = $this->input->get('asset_location' . $i . '-' . $j, '', 'RAW');
                        $asset_args['asset_location'] = ('' === $location) ? null : $location;
                        $asset_args['asset_address'] = $this->input->getString('asset_address' . $i . '-' . $j);
                        $asset_args['asset_criteria'] = ('' !== $this->input->getString('asset_criteria' . $i . '-' . $j))
                            ? $this->input->getString('asset_criteria' . $i . '-' . $j) : null;
                        $asset_args['asset_zip'] = $this->input->getString('asset_zip' . $i . '-' . $j);
                        $asset_args['asset_status'] = $this->input->getString('asset_status' . $i . '-' . $j);
                        $asset_args['lng'] = $this->input->getString('lng' . $i . '-' . $j);
                        $asset_args['lat'] = $this->input->getString('lat' . $i . '-' . $j);
                        $asset_args['client_id'] = $client_id;

                        $asset_args['asset_city'] = $this->input->get('asset_city' . $i . '-' . $j, '', 'RAW');
                        $asset_args['asset_state'] = $this->input->get('asset_state' . $i . '-' . $j, '', 'RAW');
                        $asset_args['asset_units'] = $this->input->get('asset_unit' . $i . '-' . $j, '', 'RAW');
                        $asset_args['asset_sq_ft'] = $this->input->get('asset_sq_ft' . $i . '-' . $j, '', 'RAW');
                        $asset_args['asset_cond'] = $this->input->get('asset_cond' . $i . '-' . $j, '', 'RAW');
                        $asset_args['asset_lead'] = $this->input->get('asset_lead' . $i . '-' . $j, '', 'RAW');
                        $asset_args['asset_source'] = $this->input->get('asset_source' . $i . '-' . $j, '', 'RAW');

                        // Check if Product type doesn't exist in the database
                        // and if not create one.
                        (!is_numeric($asset_args['asset_cond'])
                            && null !== $asset_args['asset_cond']) ?
                            $asset_args['asset_cond'] =
                                $this->homebase_model->add_new_asset_info(
                                    $asset_args['asset_cond'], $user_id, 'condition', $product_id) : '';

                        // Check if Condition doesn't exist in the database
                        // and if not create one.
                        (!is_numeric($product_id)
                            && null !== $product_id) ?
                            $product_id =
                                $this->homebase_model->add_new_asset_info(
                                    $product_id, $user_id, 'product', $product_id) : '';

                        // Check if location doesn't exist in the database
                        // and if not create one.
                        (!is_numeric($asset_args['asset_location'])
                            && null !== $asset_args['asset_location']) ?
                            $asset_args['asset_location'] =
                                $this->homebase_model->add_new_asset_info(
                                    $asset_args['asset_location'], $user_id, 'location', $product_id) : '';

                        // Check if Criteria doesn't exist in the database
                        // and if not create one.
                        (!is_numeric($asset_args['asset_criteria'])
                            && null !== $asset_args['asset_criteria']) ?
                            $asset_args['asset_criteria'] =
                                $this->homebase_model->add_new_asset_info(
                                    $asset_args['asset_criteria'], $user_id, 'criteria', $product_id) : '';

                        // Add Asset.
                        $this->homebase_model->add_asset($asset_args, $id, $origin, $product_id, $user_id, $i, $j);
                    }
                }
            }

            $this->ret['client_added'] = true;
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Check whether any client with this name already exist.
     */
    function get_existing_clients()
    {
        $this->check_ajax();

        try {
            $client = $this->input->get('clients', '', 'RAW');
            $user_id = $this->get_user_id();
            $product_id = $this->app->get_product();

            $this->ret['existing_clients'] =
                $this->homebase_model->get_existing_client($client, $user_id, $product_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Fetch TSL instances.
     */
    public function search_tsl_instances()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $product_id = $this->app->get_product();
            $this->ret['tsl_instances'] = $this->homebase_model->search_tsl_instances($user_id, $product_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);

        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }

        echo json_encode($this->ret);
    }

    /**
     *
     * Update a particular asset table field.
     */
    public function update_asset_field()
    {
        $this->check_ajax();

        try {
            $id = $this->input->get('id');
            $col_name = $this->input->getString('col_name');
            $value = $this->input->getString('val');
            $lat = $this->input->get('lat', '', 'RAW');
            $long = $this->input->get('long', '', 'RAW');

            $this->ret['asset_updated'] = $this->homebase_model->update_asset_field($id, $col_name, $value, $lat, $long);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Push a client to TSL/CPD.
     */
    public function push_client()
    {
        $this->check_ajax();

        try {
            $ids = $this->input->get('id', '', 'RAW');
            $status = $this->input->getString('status');
            $tsl_id = $this->input->get('tsl');
            $product_id = $this->app->get_product();
            $user_id = $this->get_user_id();
            $type = $this->input->get('type');
            $asset_type = $this->input->get('asset_type');

            if (is_array($ids)) {
                foreach ($ids as $id) {
                    $this->ret['push_client'] =
                        $this->homebase_model->push_client($id, $status, $tsl_id, $product_id, $user_id, $type, $asset_type);
                }
            }

            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    public function update_homebase_field()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $col = $this->input->getString('col');
            $val = $this->input->get('val', '', 'RAW');

            $this->ret['homebase_field_updated'] = $this->homebase_model->update_homebase_field($id, $col, $val);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    public function is_asset_used()
    {
        $this->check_ajax();

        try {
            $id = $this->input->get('id', '', 'RAW');

            $this->ret['asset_used'] = $this->homebase_model->is_asset_used($id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);

    }

    public function delete_prev_instance()
    {
        $this->check_ajax();

        try {
            $tc_id = $this->input->getInt('tc_id');
            $deal_id = $this->input->getInt('deal_id');
            $table_name = $this->input->getString('table_name');

            $this->ret['instance_deleted'] =
                $this->homebase_model->delete_prev_instance($tc_id, $deal_id, $table_name);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);

        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Updates Homebase Amount Details.
     */
    public function update_amount()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $col = $this->input->getString('col');
            $val = $this->input->get('val', '', 'RAW');
            $table = $this->input->getString('table');
            $pk = $this->input->get('pk');

            $this->ret['amount_updated'] = $this->homebase_model->update_amount($id, $col, $val, $table, $pk);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    public function copy_asset()
    {
        $this->check_ajax();

        try {
            $asset_id = $this->input->getInt('asset_id');
            $status = $this->input->get('status');

            $this->ret['asset_copied'] = $this->homebase_model->copy_asset($asset_id, $status);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    public function fetch_more_row()
    {
        $this->check_ajax();
        try {
            $user_id = $this->get_user_id();
            $product_id = $this->app->get_product();
            $query = $this->input->getString('query');
            $lower_limit = $this->input->getInt('lower_limit');
            $upper_limit = $this->input->getInt('upper_limit');
            $id = $this->input->getInt('id');

            if (0 !== $lower_limit) {
                $id = $this->input->getInt('id');
            }

            $this->ret['client'] = $this->homebase_model->fetch_more_row
            ($user_id, $product_id, $query, $lower_limit, $upper_limit, $id);

            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    public function add_new_asset()
    {
        $this->check_ajax();

        try {
            $client_id = $this->input->getInt('id');

            $this->ret['asset_added'] = $this->homebase_model->add_new_asset($client_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    public function delete_asset_client()
    {
        $this->check_ajax();

        try {
            $id = $this->input->get('id', '', 'RAW');
            $type = $this->input->get('type');
            $key = $this->input->get('key');

            // Check if multiple items delete request.
            if (is_array($id)) {
                $id = implode(", ", array_column($id, 'value'));
            }

            $this->ret['asset_client_deleted'] = $this->homebase_model->delete_asset_client($id, $type, $key);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    public function add_portfolio()
    {
        $this->check_ajax();

        try {
            $portfolio_name = $this->input->getString('portfolio_name');
            $user_id = $this->get_user_id();

            $this->ret['portfolio_added'] = $this->homebase_model->add_portfolio($user_id, $portfolio_name);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    function delete_portfolio()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('portfolio_id');
            $this->ret['portfolio_deleted'] = $this->homebase_model->delete_portfolio($id);

            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }


    public function use_portfolio()
    {
        $this->check_ajax();

        try {
            $asset_id = $this->input->getInt('asset_id');
            $portfolio_id = $this->input->getInt('portfolio_id');

            $this->ret['portfolio_used'] = $this->homebase_model->use_portfolio($asset_id, $portfolio_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    public function get_all_portfolio_assets()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');

            $this->ret['portfolio_assets'] = $this->homebase_model->get_all_portfolio_assets($id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    public function get_lead_source()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();

            $this->ret['lead_source'] = $this->homebase_model->get_lead_source($user_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    public function get_asset_info()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();

            $this->ret['asset_info'] = $this->homebase_model->get_asset_info($user_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    public function get_asset_type_info()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $type = $this->input->getString('type');

            $this->ret['asset_type_info'] = $this->homebase_model->get_asset_type_info($user_id, $type);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    public function add_asset_info()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $type = $this->input->getString('info_type');
            $val = $this->input->getString('asset_info_name');

            $this->ret['asset_info_added'] = $this->homebase_model->add_asset_info($user_id, $type, $val);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    public function update_asset_info()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('pk');
            $val = $this->input->getString('value');

            $this->ret['asset_info_updated'] = $this->homebase_model->update_asset_info($id, $val);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    public function get_owner_info()
    {
        $this->check_ajax();

        try {
            $owner_id = $this->input->getInt('owner_id');

            $this->ret['owner_info'] = $this->homebase_model->get_owner_info($owner_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    public function save_owner_info()
    {
        $this->check_ajax();

        try {
            $data = $this->input;

            $this->ret['info_saved'] = $this->homebase_model->save_owner_info($data);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    public function upload_owner_image()
    {
        $this->check_ajax();
        try {
            foreach ($_FILES as $file) {
                $owner_id = $this->input->getInt('owner_id');
                $random = "owner_img" . time();

                $prev_owner_image = $this->homebase_model->get_owner_image($owner_id);

                // Check if the image is already present. If so remove it.
                !empty($prev_owner_image[0]->hc_avatar) ?
                    unlink(__dir__ . '/../../upload/' . $prev_owner_image[0]->hc_avatar) : '';

                // Fetch file info.
                $data['file_info'] = pathinfo($file['name'][0]);
                $data['file_name'] = $data['file_info']['filename'];
                $data['extension'] = $data['file_info']['extension'];

                // Moves file to a upload directory.
                move_uploaded_file($file['tmp_name'][0], __dir__ . '/../../upload/' .
                    $random . '.' . $data['extension']);

                // Creating a record in the DB.
                $this->ret['image_uploaded'] = $this->homebase_model->upload_owner_image($owner_id,
                    $random . '.' . $data['extension']);
                $this->ret['status'] = $this->app->get_status('success');
            }
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Fetch Owner Image.
     */
    public function get_owner_image()
    {
        $img = $this->input->get('img', '', 'RAW');

        header('Content-Type: image/jpeg');
        readfile(__dir__ . '/../../upload/' . $img);
    }

    /**
     *
     * Saves owner popup info.
     */
    public function save_owner_popup_info()
    {
        $this->check_ajax();

        try {
            $owner_id = $this->input->getInt('owner_id');
            $val = $this->input->getString('val');
            $type = $this->input->getString('type');

            $this->ret['owner_info_saved'] = $this->homebase_model->save_owner_popup_info($owner_id, $val, $type);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Fetch owner profile info.
     */
    public function get_owner_popup_info()
    {
        $this->check_ajax();
        try {
            $owner_id = $this->input->getInt('owner_id');

            $this->ret['owner_info'] = $this->homebase_model->get_owner_popup_info($owner_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    // Fetches info for an data.
    public function get_asset_data()
    {
        $this->check_ajax();
        try {
            $asset_id = $this->input->getInt('asset_id');
            $user_id = $this->get_user_id();

            $this->ret['portfolio'] = $this->homebase_model->get_portfolio($user_id);
            $this->ret['client'] = $this->homebase_model->get_asset_data($asset_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Delete Asset Info from DB.
     */
    function delete_asset_info()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');

            $this->ret['asset_info_deleted'] = $this->homebase_model->delete_asset_info($id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    function get_location_info()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();

            $this->ret['location_info'] = $this->homebase_model->get_location_info($user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    function get_homebase_tool()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();

            $this->ret['tool_info'] = $this->homebase_model->get_homebase_tool($user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret[$this->status_string] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret[$this->message_string] = $this->app->get_message('fail');
        }
        echo json_encode($this->ret);
    }

    function upload_asset_image()
    {
        $this->check_ajax();
        try {
            foreach ($_FILES as $file) {
                $asset_id = $this->input->getInt('asset_id');
                $random = "asset_img" . $asset_id . time();

                $prev_asset_image = $this->homebase_model->get_asset_image($asset_id);

                // Check if the image is already present. If so remove it.
                !empty($prev_asset_image[0]->hca_avatar) ?
                    unlink(__dir__ . '/../../upload/' . $prev_asset_image[0]->hca_avatar) : '';

                // Fetch file info.
                $data['file_info'] = pathinfo($file['name'][0]);
                $data['file_name'] = $data['file_info']['filename'];
                $data['extension'] = $data['file_info']['extension'];

                // Moves file to a upload directory.
                move_uploaded_file($file['tmp_name'][0], __dir__ . '/../../upload/' .
                    $random . '.' . $data['extension']);

                // Creating a record in the DB.
                $this->ret['image_uploaded'] = $this->homebase_model->upload_asset_image($asset_id,
                    $random . '.' . $data['extension']);
                $this->ret['status'] = $this->app->get_status('success');
            }
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }


//    function get_add_client_info()
//    {
//        $this->check_ajax();
//
//        try {
//            $user_id = $this->get_user_id();
//
//            $this->ret['add_client_info'] = $this->homebase_model->get_add_client_info($user_id);
//            $this->ret['status'] = $this->app->get_status('success');
//        } catch (\Exception $e) {
//            $this->ret[$this->status_string] = $this->app->get_status('fail',
//                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
//            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
//            $this->ret[$this->message_string] = $this->app->get_message('fail');
//        }
//        echo json_encode($this->ret);
//    }


    /**
     * Adds new buyer to the list.
     *
     */
    function add_buyer()
    {
        $this->check_ajax();

        try {

            $buyer_count = $this->input->getInt('buyer_count');
            $id = $this->input->getInt('id');
            $origin = $this->input->getString('origin');
            $user_id = $this->get_user_id();

            // loop through all the buyers and fetch the parameters.
            for ($i = 1; $i <= $buyer_count; $i++) {
                // Check if buyer name is set.
                if (!empty($this->input->getString('buyer_name' . $i))) {
                    $buyer_args['buyer_name'] = $this->input->getString('buyer_name' . $i);
                    $buyer_args['main_phone'] = $this->input->getString('main_phone' . $i);
                    $buyer_args['mobile_phone'] = $this->input->getString('mobile_phone' . $i);
                    $buyer_args['buyer_email'] = $this->input->getString('buyer_email' . $i);
                    $buyer_args['entity_name'] = $this->input->getString('entity_name' . $i);
                    $buyer_args['second_phone'] = $this->input->getString('second_phone' . $i);
                    $buyer_args['user_id'] = $user_id;

                    // Insert into homebase buyer and store the ID.
                    $buyer_id = $this->homebase_model->add_buyer($buyer_args);

                    // Get the count of assets for this buyer.
                    $asset_count = $this->input->getInt('buyer-asset-count' . $i);

                    // loop through asset count.
                    for ($j = 1; $j <= $asset_count; $j++) {
                        $product_id = $this->input->get('entity_product' . $i . '-' . $j);
                        $asset_args['asset_name'] = $this->input->getString('entity_name' . $i . '-' . $j);
                        $location = $this->input->get('entity_location' . $i . '-' . $j, '', 'RAW');
                        $asset_args['asset_location'] = ('' === $location) ? null : $location;
                        $asset_args['asset_address'] = $this->input->getString('entity_address' . $i . '-' . $j);
                        $asset_args['asset_criteria'] = ('' !== $this->input->getString('entity_criteria' . $i . '-' . $j))
                            ? $this->input->getString('entity_criteria' . $i . '-' . $j) : null;
                        $asset_args['asset_zip'] = $this->input->getString('entity_zip' . $i . '-' . $j);
//                        $asset_args['asset_status'] = $this->input->getString('asset_status' . $i . '-' . $j);
//                        $asset_args['lng'] = $this->input->getString('lng' . $i . '-' . $j);
//                        $asset_args['lat'] = $this->input->getString('lat' . $i . '-' . $j);
                        $asset_args['buyer_id'] = $buyer_id;

                        $asset_args['asset_city'] = $this->input->get('entity_city' . $i . '-' . $j, '', 'RAW');
                        $asset_args['asset_state'] = $this->input->get('entity_state' . $i . '-' . $j, '', 'RAW');
                        $asset_args['asset_units'] = $this->input->get('entity_unit' . $i . '-' . $j, '', 'RAW');
                        $asset_args['asset_sq_ft'] = $this->input->get('entity_sq_ft' . $i . '-' . $j, '', 'RAW');
                        $asset_args['asset_cond'] = $this->input->get('entity_cond' . $i . '-' . $j, '', 'RAW');
//                        $asset_args['asset_lead'] = $this->input->get('asset_lead' . $i . '-' . $j, '', 'RAW');
//                        $asset_args['asset_source'] = $this->input->get('asset_source' . $i . '-' . $j, '', 'RAW');

                        // Check if Condition doesn't exist in the database
                        // and if not create one.
                        (!is_numeric($asset_args['asset_cond'])
                            && null !== $asset_args['asset_cond']) ?
                            $asset_args['asset_cond'] =
                                $this->homebase_model->add_new_asset_info(
                                    $asset_args['asset_cond'], $user_id, 'condition', $product_id) : '';

                        // Check if location doesn't exist in the database
                        // and if not create one.
                        (!is_numeric($asset_args['asset_location'])
                            && null !== $asset_args['asset_location']) ?
                            $asset_args['asset_location'] =
                                $this->homebase_model->add_new_asset_info(
                                    $asset_args['asset_location'], $user_id, 'location', $product_id) : '';

                        // Check if Criteria doesn't exist in the database
                        // and if not create one.
                        (!is_numeric($asset_args['asset_criteria'])
                            && null !== $asset_args['asset_criteria']) ?
                            $asset_args['asset_criteria'] =
                                $this->homebase_model->add_new_asset_info(
                                    $asset_args['asset_criteria'], $user_id, 'criteria', $product_id) : '';

                        // Add Asset.
                        $this->homebase_model->add_buyer_asset($asset_args, $id, $origin, $product_id, $user_id, $i, $j);
                    }
                }
            }

            $this->ret['buyer_added'] = true;
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function fetch_buyer_data()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();

            $this->ret['buyer_data'] = $this->homebase_model->fetch_buyer_data($user_id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function fetch_buyer_row()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');

            $this->ret['buyer_asset'] = $this->homebase_model->fetch_buyer_row($id);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function update_buyer_asset_data()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $name = $this->input->getString('name');
            $val = $this->input->get('val', '', 'RAW');

            $this->ret['buyer_data_updated'] = $this->homebase_model->update_buyer_asset_data($id, $name, $val);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function update_buyer_data()
    {
        $this->check_ajax();

        try {
            $val = $this->input->get('val', '', 'RAW');
            $id = $this->input->getInt('id');
            $col = $this->input->getString('col');

            $this->ret['buyer_updated'] = $this->homebase_model->update_buyer_data($val, $id, $col);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function get_owner_asset_info()
    {
        $this->check_ajax();

        try {
            $owner_id = $this->input->getInt('owner_id');
            $qtr = $this->input->getInt('qtr');
            $year = $this->input->getInt('year');

            $deals_ret = [];
            $cpd_deals = $this->homebase_model->get_owner_asset_info($owner_id, $qtr, $year);
            // Loop through each existing CPD deals.
            foreach ($cpd_deals as $deal) {
                $deals_ret[$deal->ds_status][] = $deal;
            }

            $this->ret['owner_asset_info'] = $deals_ret;
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function update_revenue()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $col = $this->input->getString('col');
            $type = $this->input->getString('type');
            $val = $this->input->getString('val');

            $this->ret['revenue_updated'] = $this->homebase_model->update_revenue($id, $col, $type, $val);
            $this->ret[$this->status_string] = $this->app->get_status($this->success_string);
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function upload_comp_image()
    {
        $this->check_ajax();
        try {
            foreach ($_FILES as $file) {
                $comp_id = $this->input->getInt('comp_id');
                $random = "comp_img" . time();

                $prev_comp_image = $this->homebase_model->get_comp_image($comp_id);

                // Check if the image is already present. If so remove it.
                !empty($prev_comp_image[0]->ac_avatar) ?
                    unlink(__dir__ . '/../../upload/' . $prev_comp_image[0]->ac_avatar) : '';

                // Fetch file info.
                $data['file_info'] = pathinfo($file['name'][0]);
                $data['file_name'] = $data['file_info']['filename'];
                $data['extension'] = $data['file_info']['extension'];

                // Moves file to a upload directory.
                move_uploaded_file($file['tmp_name'][0], __dir__ . '/../../upload/' .
                    $random . '.' . $data['extension']);

                // Creating a record in the DB.
                $this->ret['image_uploaded'] = $this->homebase_model->upload_comp_image($comp_id,
                    $random . '.' . $data['extension']);
            }
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }
}