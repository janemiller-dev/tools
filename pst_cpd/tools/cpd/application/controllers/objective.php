<?php

/**
 * Objectives controller.
 */

namespace Controller;

use PHPUnit\Exception;

class Objective extends Controller
{
    private $objective_model;
    private $ret;

    function __construct($app)
    {
        parent::__construct($app);
        $this->objective_model = new \Model\Objective;
        $this->ret['status'] = 'success';
    }

    /**
     * Default method.
     */
    function index()
    {
        $ret['render'] = 'views/objective/dashboard.php';
        $ret['data'] = $this->view_data;
        return $ret;
    }

    /**
     * View objectives for a quarter.
     *
     */
    function objective()
    {
        $ret['render'] = 'views/objective/objective.php';
        $ret['data'] = $this->view_data;
        return $ret;
    }

    /*
     * Gets the objective for a profession related to user.
     *
     * @return void $this->ret objectives for a profession
     */
    function get_objective()
    {
        $this->check_ajax();
        try {
            //Get the Profession Objectives
            $user_id = $this->get_user_id();
            $profession_id = $this->app->get_profession();
            $this->ret = $this->objective_model->get_objective($user_id, $profession_id);
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['error'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Updates the objective values.
     *
     * @return void $this->ret whether objective updated or not
     */
    function update_objective()
    {
        $this->check_ajax();
        try {
            //Get the data
            $user_id = $this->get_user_id();
            $profession_id = $this->app->get_profession();
            $name = 'upo_'.$this->input->get('name');
            $year = $this->input->getInt('pk');
            $value = $this->input->get('value');
            $this->ret = $this->objective_model->update_objective($user_id, $profession_id, $name, $year, $value);

        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['error'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Returns the list of years for which user already have objectives.
     *
     *  @return void $this->ret   List of years for which objective is already present.
     */
    function get_used_years()
    {
        $this->check_ajax();
        try {
            $user_id = $this->get_user_id();
            $profession_id = $this->app->get_profession();
            $this->ret['years'] = $this->objective_model->get_used_years($user_id, $profession_id);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['error'] = $e->getMessage();
        }
        echo json_encode($this->ret);

    }

    /**
     * Adds new objective for a year.
     *
     * @return void $this->ret
     */
    function add_objective()
    {
        $this->check_ajax();
        try {
            // data for objective
            $data['user_id'] = $this->get_user_id();
            $data['profession_id'] = $this->app->get_profession();
            $data['year'] = $this->input->getInt('year');
            $data['floor'] = $this->input->getInt('floor');
            $data['target'] = $this->input->getInt('target');
            $data['game'] = $this->input->getInt('game');

            foreach ($data as $val) {
                if (empty($val)) {
                    $this->input_missing_exception();
                }
            }
            $this->ret = $this->objective_model->add_objective($data);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['error'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     *  Returns the list of already used quarters
     *
     *  @return void $this->ret   List of quarters for which objective is already present.
     */
    function get_used_quarters()
    {
        $this->check_ajax();
        try {
            $year = $this->input->getInt('year');
            $user_id = $this->get_user_id();
            $profession_id = $this->app->get_profession();
            $this->ret['quarters'] = $this->objective_model->get_used_quarters($user_id, $profession_id, $year);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['error'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Adds new objective for a quarter.
     *
     *  @return void $this->ret.
     */
    function add_quarter_objective()
    {
        $this->check_ajax();
        try {

            // Data for quarterly objective
            $data['user_id'] = $this->get_user_id();
            $data['profession_id'] = $this->app->get_profession();
            $data['floor'] = $this->input->getInt('floor');
            $data['game'] = $this->input->getInt('game');
            $data['target'] = $this->input->getInt('target');
            $data['quarter'] = $this->input->getInt('quarter');
            $data['year'] = $this->input->getInt('quarter-year');

            $this->ret = $this->objective_model->add_quarter_objective($data);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['error'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    function get_quarter_objective()
    {
        $this->check_ajax();
        try {

            // Get the Profession Quaterly Objectives
            $user_id = $this->get_user_id();
            $profession_id = $this->app->get_profession();
            $year = $this->input->getInt('year');
            $this->ret = $this->objective_model->get_quarter_objective($user_id, $profession_id, $year);
        }catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['error'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    function update_quarter_objective()
    {
        $this->check_ajax();
        try {

            // Get the column name, value and id.
            $name = 'upqo_'.$this->input->get('name');
            $value = $this->input->getInt('value');
            $upqo_id = $this->input->getInt('pk');
            $this->ret = $this->objective_model->update_quarter_objective($name, $value, $upqo_id);
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['error'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }
}