<?php

namespace Controller;

class Tool extends Controller
{

	/**
	 * Ensure that the user has access to this tool.
	 */
	function __construct($app)
	{
		parent::__construct($app);
	}

	/**
	 * Default method.
	 */
	function index()
	{
		$ret['render'] = 'views/tool/tools.php';
		$ret['data'] = $this->view_data;
		return $ret;
	}

	/**
	 * Tool Group.
	 */
	function group()
	{
		$ret['render'] = 'views/tool/group.php';
		$ret['data'] = $this->view_data;
		return $ret;
	}

	/**
	 * Get the tool list as constrained by the specified window an search.
	 */
	public function get_tools()
	{
		$this->check_ajax();

		try {
			// Get the list of tools from the tool model.
			$tool_model = new \Model\Tool;
			$ret = $tool_model->get_tools($this->input);
		} catch (\Exception $e) {
			$ret['error'] = $e->getMessage();
		}

		echo json_encode($ret);
	}

	/**
	 * Get the list of tools accessible to the user.
	 */
	public function get_user_tools()
	{
		$this->check_ajax();

		try {
			// Get the list of tools from the tool model.
			$user_id = $this->get_user_id();
			$tool_model = new \Model\Tool;
			$ret['tools'] = $tool_model->get_user_tools($user_id);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['error'] = $e->getMessage();
			$ret['status'] = 'failure';
		}
		echo json_encode($ret);
	}

	/**
	 * Get the specified tool group.
	 */
	public function get_group()
	{
		$this->check_ajax();
		try {

			// Get the inputs
			$id = $this->input->getInt('id');
			if (empty($id))
				throw new Exception("Invalid request. One or more inputs are missing.");

			// Add the instance
			$tool_model = new \Model\Tool;
			$ret['group'] = $tool_model->get_group($id);
			$ret['status'] = 'success';
		} catch (\Exception $e) {
			$ret['status'] = 'failure';
			$ret['message'] = $e->getMessage();
		}
		echo json_encode($ret);
	}
}