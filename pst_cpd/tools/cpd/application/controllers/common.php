<?php

namespace Controller;

use function print_r;

require "controller.php";

class Common extends Controller
{

    private $common_model;
    private $ret;

    /**
     * Ensure that the user has access to this tool.
     * @param $app
     */
    function __construct($app)
    {
        parent::__construct($app);
        $this->common_model = new \Model\Common();
        $this->ret['status'] = 'success';
    }

    function update_db_field()
    {
        $this->check_ajax();

        try {
            // Get the parameters.
            $id = $this->input->getInt('pk');
            $name = $this->input->getText('name');
            $value = $this->input->getRaw('value');

            // Split the name into table name and column.
            list($table, $column) = explode('.', $name);

            // Perform the update.
            $this->common_model->update_field($table, $column, $id, $value);

            $this->ret['status'] = 'success';
        } catch (Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Updates active CPD/TGD.
     */
    function update_active_instances()
    {
        try {
            $id = $this->input->get('id');
            $name = $this->input->get('name');
            $user_id = $this->get_user_id();
//            $product = $this->input->getInt('product');

            $this->common_model->update_active_instances($id, $name, $user_id);
            $this->ret['status'] = 'success';
        } catch (Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Return Alert data;
     *
     */
    function get_alert_data()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $client_id = $this->input->getInt('id');
            $duration = $this->input->get('duration', '', 'RAW');

            $this->ret['data'] = $this->common_model->get_alert_data($user_id, $client_id, 0, $duration);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function add_to_alert()
    {
        $this->check_ajax();
        try {
            $component_id = $this->input->getInt('id');
            $type = $this->input->getString('type');
            $asset_id = $this->input->getInt('client_id');
            $set = $this->input->getString('set');
            $user_id = $this->get_user_id();
            $datetime = $this->input->getString('datetime');

            $this->ret['alert_added'] =
                $this->common_model->add_to_alert($component_id, $type, $asset_id, $set, $user_id, $datetime);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    // Update Alert Date.
    function update_alert_date()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('pk');
            $type = $this->input->getString('name');
            $value = $this->input->get('value', '', 'RAW');

            $this->ret['date_updated'] = $this->common_model->update_alert_date($id, $type, $value);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function update_alert_action()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('pk');
            $type = $this->input->getString('name');
            $val = $this->input->get('value', '', 'RAW');

            $this->ret['action_updated'] = $this->common_model->update_alert_action($id, $type, $val);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function update_alert_objective()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $val = $this->input->getInt('val');
            $type = $this->input->getString('type');

            $this->ret['objective_updated'] = $this->common_model->update_alert_objective($id, $type, $val);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function update_row_color()
    {
        $this->check_ajax();

        try {
            $val = $this->input->get('val', '', 'RAW');
            $id = $this->input->getInt('id');
            $type = $this->input->getString('type');

            $this->ret['row_color_updated'] = $this->common_model->update_row_color($id, $val, $type);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function add_new_objective()
    {
        $this->check_ajax();

        try {
            $val = $this->input->getString('val');
            $user_id = $this->get_user_id();

            $this->ret['objective_added'] = $this->common_model->add_new_objective($val, $user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }


    function delete_alert()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');

            $this->ret['alert_deleted'] = $this->common_model->delete_alert($id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function update_asset_info()
    {
        $this->check_ajax();

        try {
            $asset_id = $this->input->getInt('id');
            $val = $this->input->get('val', '', 'RAW');
            $seq = $this->input->getInt('seq');
            $type = $this->input->getString('type');
            $sub_seq = $this->input->getInt('sub-seq');
            $category = $this->input->getString('category');

            $this->ret['asset_info_updated'] =
                $this->common_model->update_asset_info($asset_id, $val, $seq, $type, $sub_seq, $category);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function fetch_asset_info()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $category = $this->input->getString('category');

            $this->ret['asset_info'] = $this->common_model->fetch_asset_info($id, $category);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Shows alert notification.
     */
//    function show_alerts()
//    {
//        $this->ret['render'] = 'views/dialogs/reminder.php';
//        $this->ret['data'] = $this->view_data;
//        return $this->ret;
//    }

    function fetch_alerts()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $date = $this->input->get('date', '', 'RAW');

            $this->ret['alerts_fetched'] = $this->common_model->get_alert_data($user_id, 0, $date, 0);
            $this->ret['disabled_reminder'] = $this->common_model->get_disable_reminder($user_id);
            $this->ret['recurrence'] = $this->common_model->get_reminder_recurrence($user_id);
            $this->ret['set_recurrence_shown'] = $this->common_model->set_recurrence_shown($user_id);

            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function update_reminder()
    {
        $this->check_ajax();

        try {
            $type = $this->input->get('type');
            $id = $this->input->get('id');
            $user_id = $this->get_user_id();

            $this->ret['reminder_updated'] = $this->common_model->update_reminder($type, $id, $user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);

    }

    function add_new_project()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $when = $this->input->getString('when');

            $this->ret['project_added'] = $this->common_model->add_new_project($user_id, $when);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function get_quick_project()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $when = $this->input->getString('when');

            $this->ret['quick_project'] = $this->common_model->get_quick_project($user_id, $when);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function add_new_contact()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $when = $this->input->getString('when');

            $this->ret['contact_added'] = $this->common_model->add_new_contact($user_id, $when);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function get_quick_contact()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $when = $this->input->getString('when');

            $this->ret['quick_contact'] = $this->common_model->get_quick_contact($user_id, $when);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function get_quick_action()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $when = $this->input->getString('when');

            $this->ret['quick_action'] = $this->common_model->get_quick_action($user_id, $when);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function add_new_action()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $when = $this->input->getString('when');

            $this->ret['contact_added'] = $this->common_model->add_new_action($user_id, $when);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function update_quick_field()
    {
        $this->check_ajax();

        try {
            $table = $this->input->getString('table');
            $id = $this->input->getInt('id');
            $col = $this->input->getString('col');
            $val = $this->input->get('val', '', 'RAW');
            $key = $this->input->getString('key');

            $this->ret['quick_field_updated'] = $this->common_model->update_quick_field($table, $id, $col, $val, $key);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function delete_quick_tool()
    {
        $this->check_ajax();

        try {
            $table = $this->input->getString('table');
            $id = $this->input->getInt('id');
            $key = $this->input->getString('key');

            $this->ret['quick_tool_deleted'] = $this->common_model->delete_quick_tool($table, $id, $key);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function set_reminder_recurrence()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $interval = $this->input->getInt('interval');

            $this->ret['recurrence_set'] = $this->common_model->set_reminder_recurrence($user_id, $interval);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function get_sgd_info()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $tab = $this->input->getString('tab');

            $this->ret['sgd_info'] = $this->common_model->get_sgd_info($user_id, $tab);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function update_sgd_text()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $val = $this->input->get('val', '', 'RAW');
            $tab = $this->input->getString('tab');
            $seq = $this->input->getInt('seq');
            $type = $this->input->getString('type');

            $this->ret['sgd_text_updated'] = $this->common_model->update_sgd_text($user_id, $val, $tab, $seq, $type);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function update_sgd_date()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $val = $this->input->get('val', '', 'RAW');
            $tab = $this->input->getString('tab');
            $seq = $this->input->getInt('seq');
            $type = str_replace('-date', '', $this->input->getString('type'));

            $this->ret['sgd_date_updated'] = $this->common_model->update_sgd_date($user_id, $val, $tab, $seq, $type);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function update_sgd_month_count()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $val = $this->input->getInt('val');
            $type = $this->input->getString('type');

            $this->ret['month_count_updated'] = $this->common_model->update_sgd_month_count($user_id, $val, $type);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function reset_acm_color()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();

            $this->ret['acm_color_reset'] = $this->common_model->reset_acm_color($user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function get_user_assets()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();
            $qtr = $this->input->getInt('qtr');
            $year = $this->input->getInt('year');
            $cpd_deals = $this->common_model->get_user_assets($user_id, $year, $qtr);
            $deals_ret = [];

            // Loop through each existing CPD deals.
            foreach ($cpd_deals as $deal) {
                $deals_ret[$deal->ds_status][] = $deal;
            }

            $deals_ret['identified'] = $this->common_model->get_deals_identified($user_id);
            $this->ret['agent_info'] = $deals_ret;
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!!Some Error Occurred . We are working on getting this Fixed . ';
        }
        echo json_encode($this->ret);
    }

    function save_agent_info()
    {
        $this->check_ajax();
        try {
            $user_id = $this->get_user_id();
            $agent_info = $this->input;

            $this->ret['agent_info_saved'] = $this->common_model->save_agent_info($user_id, $agent_info);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!!Some Error Occurred . We are working on getting this Fixed . ';
        }
        echo json_encode($this->ret);
    }

    function get_agent_info()
    {
        $this->check_ajax();
        try {
            $user_id = $this->get_user_id();
            $id = $this->input->getInt('id');

            $this->ret['agent_info'] = $this->common_model->get_agent_info($user_id, $id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!!Some Error Occurred . We are working on getting this Fixed . ';
        }
        echo json_encode($this->ret);
    }

    function update_agent_asset_info()
    {
        $this->check_ajax();
        try {
            $asset_id = $this->input->getInt('pk');
            $col = $this->input->getString('name');
            $value = $this->input->getString('value');

            $this->ret['agent_asset_updated'] = $this->common_model->update_agent_asset_info($asset_id, $col, $value);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!!Some Error Occurred . We are working on getting this Fixed . ';
        }
        echo json_encode($this->ret);
    }

    function upload_agent_image()
    {
        $this->check_ajax();
        try {
            foreach ($_FILES as $file) {
                $agent_id = $this->input->getInt('agent_id');
                $random = "agent_img" . time();

                $prev_agent_image = $this->common_model->get_agent_image($agent_id);

                // Check if the image is already present. If so remove it.
                !empty($prev_agent_image[0]->ap_image) ?
                    unlink(__dir__ . '/../../upload/' . $prev_agent_image[0]->ap_image) : '';

                // Fetch file info.
                $data['file_info'] = pathinfo($file['name'][0]);
                $data['file_name'] = $data['file_info']['filename'];
                $data['extension'] = $data['file_info']['extension'];

                // Moves file to a upload directory.
                move_uploaded_file($file['tmp_name'][0], __dir__ . '/../../upload/' .
                    $random . '.' . $data['extension']);

                // Creating a record in the DB.
                $this->ret['image_uploaded'] = $this->common_model->upload_agent_image($agent_id,
                    $random . '.' . $data['extension']);
            }
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function save_agent_popover_info()
    {
        $this->check_ajax();

        try {
            $type = $this->input->getString('type');
            $val = $this->input->getString('val');
            $id = $this->input->getInt('id');

            $this->ret['popover_info_saved'] = $this->common_model->save_agent_popover_info($type, $val, $id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function get_agent_popup_info()
    {
        $this->check_ajax();

        try {
            $agent_id = $this->input->getInt('agent_id');

            $this->ret['agent_info'] = $this->common_model->get_agent_popup_info($agent_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function join_room()
    {
//        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();

            $this->ret['data'] = $this->common_model->join_room($user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function create_room()
    {

    }

    function get_asset_info()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $type = $this->input->getString('type');

            $this->ret['asset_info'] = $this->common_model->get_asset_info($id, $type);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function update_agent_info()
    {
        $this->check_ajax();

        try {
            $id = $this->input->getInt('id');
            $table = $this->input->getString('table');
            $col = $this->input->getString('col');
            $val = $this->input->get('val', '', 'RAW');
            $key = $this->input->getString('key');

            $this->ret['agent_info_updated'] = $this->common_model->update_agent__info($id, $table, $col, $val, $key);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function get_deals_identified()
    {
        $this->check_ajax();

        try {
            $user_id = $this->get_user_id();

            $this->ret['identified_deals'] = $this->common_model->get_deals_identified($user_id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

    function update_comps_info()
    {
        $this->check_ajax();

        try {
            $col = $this->input->getString('name');
            $val = $this->input->getString('value');
            $id = $this->input->getString('pk');

            $this->ret['comps_updated'] = $this->common_model->update_comps_info($col, $val, $id);
            $this->ret['status'] = $this->app->get_status('success');
        } catch (\Exception $e) {
            $this->ret['status'] = $this->app->get_status('fail',
                'File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->app->log->error('File:' . __FILE__ . ' Line:' . __LINE__ . ' Error:' . $e->getMessage());
            $this->ret['message'] = 'Oops!! Some Error Occured. We are working on getting this Fixed.';
        }
        echo json_encode($this->ret);
    }

}