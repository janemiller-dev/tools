<?php
/**
 * Client Project Dashboard controller.
 */

namespace Controller;

class Cpd extends Controller
{

    private $cpd_model;
    private $ret;

    /**
     * Ensure that the user has access to this tool.
     * @param $app
     */
    function __construct($app)
    {
        parent::__construct($app);
        $this->cpd_model = new \Model\CPD;
        $this->ret['status'] = 'success';
    }

    /**
     * Default method.
     */
    function index()
    {
        $ret['render'] = 'views/cpd/dashboard.php';
        $ret['data'] = $this->view_data;
        return $ret;
    }

    /**
     * Show the specified CPD.
     */
    function cpd()
    {
        $ret['render'] = 'views/cpd/cpd.php';
        $ret['data'] = $this->view_data;
        return $ret;
    }

    /**
     * Add a new CPD instance
     *
     * @return void $this->ret result for add_cpd
     */
    function add_cpd()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $cpd['cpd_year'] = $this->input->getText('cpd_year');
            $cpd['cpd_industry_id'] = $this->input->getText('cpd_industry_id');
            $cpd['cpd_profession_id'] = $this->input->getText('cpd_profession_id');
            $cpd['cpd_tgd_id'] = $this->input->getText('cpd_tgd_id');
            $cpd['name'] = $this->input->getText('name');
            $cpd['description'] = $this->input->getText('description');

            if (empty($cpd['cpd_year']) || empty($cpd['name']) || empty($cpd['description'])) {
                $this->input_missing_exception();
            }

            // Get the current subscription status.
            $user_id = $this->get_user_id();

            // Add the instance
            $this->ret['instance_id'] = $this->cpd_model->add_cpd($user_id, $cpd);
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Delete an CPD instance
     *
     * @return void $this->ret result for delete_cpd
     */
    function delete_cpd()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $cpd_id = $this->input->getInt('cpd_id');
            if (empty($cpd_id)) {
                $this->input_missing_exception();
            }
            // Delete the instance
            $this->ret['instance_id'] = $this->cpd_model->delete_cpd($cpd_id);
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Copy a CPD instance to another instance
     *
     * @return void $this->ret result for copy_cpd
     */
    function copy_cpd()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $cpd_ui_id = $this->input->getText('cpd_ui_id');
            $save_as_name = $this->input->getText('save_as_name');
            if ($save_as_name == '') {
                $save_as_name = null;
            }
            $save_as_id = $this->input->getText('save_as_id');
            if ($save_as_id == '0') {
                $save_as_id = null;
            }
            if (empty($cpd_ui_id) && (empty($save_as_name) || empty($save_as_id))) {
                $this->input_missing_exception();
            }
            // Get the current subscription status.
            $user_id = $this->get_user_id();

            // Add the instance
            $this->ret['instance_id'] = $this->cpd_model->copy_cpd($user_id, $cpd_ui_id, $save_as_name, $save_as_id);
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Get the specified cpd
     *
     * @return void $this->ret details for the specified CPD
     */
    function get_cpd()
    {
        $this->check_ajax();

        try {

            // Get the inputs
            $id = $this->input->getInt('id');
            $quarter = $this->input->getInt('current_quarter');

            if (empty($id) || (empty($quarter) || $quarter == 0 || $quarter > 4)) {
                $this->input_missing_exception();
            }
            // Get the instance
            $cpd_details = $this->cpd_model->get_cpd($this->get_user_id(), $id, $quarter);
            $cpd_deals = $cpd_details->deals;

            unset($cpd_details->deals);
            $this->ret['cpd'] = $cpd_details;

            $deals_ret = [];

            foreach ($cpd_deals as $deal) {
                $deals_ret[$deal->ds_status][] = $deal;
            }

            $this->ret['cpd_deals'] = $deals_ret;

        } catch (\UnexpectedValueException $e) {

            $this->ret['status'] = 'redirect';
            $this->ret['redirect'] = '/tools/cpd';

        } catch (\Exception $e) {

            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Search for the specified user's CPD instances.
     *
     * @return void $this->ret list of CPD instances
     */
    function search_cpd_instances()
    {
        $this->check_ajax();
        try {
            // Get the CPD instances
            $user_id = $this->get_user_id();
            $profession_id = $this->app->get_profession();
            $this->ret = $this->cpd_model->search_cpd_instances($user_id, $profession_id, $this->input);
        } catch (\Exception $e) {
            $this->ret['error'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Add a new deal for CPD
     *
     * @return void $this->ret result for add_deal
     */
    function add_deal()
    {
        $this->check_ajax();
        try {

            $status_type = $this->input->getText('add_deal_status_type');

            // Get the inputs
            $deal['name'] = $this->input->getText('deal_name');
            $deal['cpd_id'] = $this->input->getText('add_deal_cpd_id');
            $deal['user_id'] = $this->get_user_id();
            $deal['status'] = $this->input->getText('deal_status');
            $deal['move_date'] = $this->input->getText('deal_move_date');
            $deal['clu'] = $this->input->getText('deal_clu');
            $deal['rwt'] = $this->input->getText('deal_rwt');
            $deal['lead'] = $this->input->getText('deal_lead');
            $deal['gen'] = $this->input->getText('deal_gen');
            $deal['type'] = $this->input->getText('deal_type');
            $deal['amount'] = $this->input->getText('deal_amount');

            if ($status_type != 'str_ds' && count(array_filter($deal)) != count($deal)) {
                $this->input_missing_exception();
            }
            // Add the instance
            $this->ret['deal_id'] = $this->cpd_model->add_deal($deal);
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Change the Status of the deal and move it to other table
     *
     * @return void $this->ret result for move_deal
     */
    function move_deal()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $deal['id'] = $this->input->getText('new_status_deal_id');
            $deal['current_status'] = $this->input->getText('deal_current_status');
            $deal['new_status'] = $this->input->getText('deal_new_status');
            $deal['new_move_date'] = $this->input->getText('deal_new_move_date');

            if (empty($deal['id']) || empty($deal['current_status']) || empty($deal['new_status']) || empty($deal['new_move_date'])) {
                $this->input_missing_exception();
            }
            // move the deal
            $this->ret['deal_status_id'] = $this->cpd_model->move_deal($deal);

        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Delete a deal
     *
     * @return void $this->ret result for delete_deal
     */
    function delete_deal()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $deal_id = $this->input->getInt('deal_id');
            if (empty($deal_id)) {
                $this->input_missing_exception();
            }
            // Delete the instance
            $this->ret['deal_id'] = $this->cpd_model->delete_deal($deal_id);
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Get the promos for specified deal
     *
     * @return void $this->ret list of promos for the specified deal
     */
    function get_promos()
    {
        $this->check_ajax();

        try {
            // Get the inputs
            $id = $this->input->getInt('deal_id');

            if (empty($id)) {
                $this->input_missing_exception();
            }
            // Add the instance
            $this->ret['promos'] = $this->cpd_model->get_promos($id);
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Add a new promo for deal
     *
     * @return void $this->ret result for adding promotions
     */
    function add_promo()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $promo['name'] = $this->input->getText('promo_name');
            $promo['datetime'] = $this->input->getText('promo_date');
            $promo['promo_deal_id'] = $this->input->getText('add_promo_deal_id');

            if (empty($promo['name']) || empty($promo['promo_deal_id'])) {
                $this->input_missing_exception();
            }
            // Add the instance
            $this->ret['promo_id'] = $this->cpd_model->add_promo($promo);
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Delete a promo
     *
     * @return void $this->ret result for deleting promotions
     */
    function delete_promo()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $promo_id = $this->input->getText('promo_id');
            if (empty($promo_id)) {
                $this->input_missing_exception();
            }
            // Delete the instance
            $this->ret['promo_id'] = $this->cpd_model->delete_promo($promo_id);
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Get the updates for specified deal
     *
     * @return void $this->ret result for fetching list of updates for the specified deals
     */
    function get_updates()
    {
        $this->check_ajax();

        try {

            // Get the inputs
            $id = $this->input->getInt('deal_id');

            if (empty($id)) {
                $this->input_missing_exception();
            }
            // Add the instance
            $this->ret['updates'] = $this->cpd_model->get_updates($id);
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Add a new update for deal
     *
     * @return void $this->ret result for adding updates for the specified deal
     */
    function add_update()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $update['name'] = $this->input->getText('update_name');
            $update['update_deal_id'] = $this->input->getText('add_update_deal_id');
            $update['datetime'] = $this->input->getText('update_date');

            if (empty($update['name']) || empty($update['update_deal_id'])) {
                $this->input_missing_exception();
            }
            // Add the instance
            $this->ret['deal_id'] = $this->cpd_model->add_update($update);
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Delete a update
     *
     * @return void $this->ret result for deleting a specified update
     */
    function delete_update()
    {
        $this->check_ajax();
        try {

            // Get the inputs
            $update_id = $this->input->getText('update_id');
            if (empty($update_id)) {
                $this->input_missing_exception();
            }
            // Delete the instance
            $this->ret['update_id'] = $this->cpd_model->delete_update($update_id);
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Updates the field name and description
     *
     * @return void $this->ret result for updating the detail for the deal tables in CPD
     */
    function update_deal_table_info()
    {
        $this->check_ajax();
        try {

            // Get the parameters
            $name = $this->input->getRaw('name');
            $id = $this->input->getInt('pk');
            $value = $this->input->getRaw('value');

            if (empty($value)) {
                $this->input_missing_exception();
            }
            // Update the value
            $this->ret['message'] = $this->cpd_model->update_deal_table_info($name, $id, $value);
            $this->ret['status'] = 'success';
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Updates the lead for simple deal
     *
     * @return void $this->ret result for updating the lead
     */
    function update_lead()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('pk');
            $lead = $this->input->get('value');

            if (empty($lead) || empty($id)) {
                $this->input_missing_exception();
            }
            $this->ret['message'] = $this->cpd_model->update_lead($id, $lead);
            $this->ret['status'] = 'success';

        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     *Updates the agent who generated the deal
     *
     * @return void $this->ret result for updating the gen
     */
    function update_gen()
    {
        $this->check_ajax();
        try {
            $id = $this->input->getInt('pk');
            $gen = $this->input->get('value');

            if (empty($gen) || empty($id)) {
                $this->input_missing_exception();
            }
            $this->ret['message'] = $this->cpd_model->update_gen($id, $gen);
            $this->ret['status'] = 'success';

        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     * Gets the FTG values for a year.
     *
     * @return void $this->ret resultv of FTG for the pofession.
     */
    function getFTG()
    {
        $this->check_ajax();
        try {
            $year = $this->input->getInt('year');

            if (empty($year)) {
                $this->input_missing_exception();
            }
            $user_id = $this->get_user_id();
            $profession_id = $this->app->get_profession();
            $this->ret['data'] = $this->cpd_model->get_ftg($user_id, $profession_id, $year);
            $this->ret['status'] = 'success';

        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }


}