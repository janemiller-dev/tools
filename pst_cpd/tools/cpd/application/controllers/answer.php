<?php
require __DIR__ . '/../../vendor/autoload.php';
use Twilio\TwiML;

$response = new Twilio\Twiml;
$response->say('Hello');
$response->play('https://api.twilio.com/cowbell.mp3', array("loop" => 5));
print $response;