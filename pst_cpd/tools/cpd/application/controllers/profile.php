<?php

namespace Controller;

use function explode;
use function json_encode;

class Profile extends Controller
{
    private $profile_model;
    private $ret;

    function __construct($app)
    {
        parent::__construct($app);
        $this->profile_model = new \Model\Profile;
        $this->ret['status'] = 'success';
    }

    /**
     * Checks if the credentials are valid then show Profile Form
     */
    function index()
    {
        $client_id =  $this->input->getInt('id');
        $token = $this->input->get('token', '', 'RAW');
        $profile_id = $this->input->getInt('profile_id');

        // Checks if any field is empty and validates token.
        if (empty($client_id) || empty($token) || empty($profile_id)
            || empty($this->profile_model->validate_token($token, $client_id, $profile_id))) {
            echo nl2br("Invalid Request!! \n Please Try again later!!");
            exit();
        }
        $ret['render'] = 'views/profile/profile.php';
        $ret['data'] = $this->view_data;
        return $ret;
    }

    /**
     * Fetches the list of questions for a profile.
     *
     */
    function get_ques()
    {
        $this->check_ajax();
        try {
            $outline_id = $this->input->getInt('outline');
            $this->ret['questions'] = $this->profile_model->get_ques($outline_id);
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Saves the client response to the database.
     */
    function set_ans()
    {
        $this->check_ajax();
        try {
            // Get the parameter.
            $question_id = $this->input->get('question_id', '', 'RAW');
            $array = explode(',', $question_id);
            $profile_id = $this->input->get('profile_id');
            $client_id = $this->input->getInt('client_id');
            $data = $this->input;

            $this->ret['answer'] = $this->profile_model->set_ans($array, $client_id, $profile_id, $data);

        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Gets the Buyer's profile questions.
     */
    function get_profile_questions()
    {
        $this->check_ajax();
        try {
            $profile_id = $this->input->getInt('id');
            $this->ret['questions'] = $this->profile_model->get_ques($profile_id);
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }

    /**
     *
     * Gets the list of already answered questions.
     */
    public function get_ans()
    {
        $this->check_ajax();
        try {
            $question_id = $this->input->get('question_id', '', 'RAW');
            $client_id = $this->input->getInt('client_id');
            $profile_id = $this->input->getInt('profile_id');
            $this->ret['answers'] = $this->profile_model->get_ans($question_id, $client_id, $profile_id);
        } catch (\Exception $e) {
            $this->ret['status'] = 'failure';
            $this->ret['message'] = $e->getMessage();
        }
        echo json_encode($this->ret);
    }
}