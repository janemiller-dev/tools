<?php
// PREVENT THE PAGE FROM BEING CACHED BY THE WEB BROWSER
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

// Start the session
if (!session_id())
    session_start();

// Load the workpress libraries
define('WP_USE_THEMES', false);
require_once("/var/www/html/pst_cpd/wordpress_cpd/wp-load.php");

// See errors - must be after the load of wp-load.
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

// If the user is not logged in, go to the wordpress login page.
if (!is_user_logged_in() &&
    (strpos($_SERVER['REQUEST_URI'], "tools/survey") === false) &&
    (strpos($_SERVER['REQUEST_URI'], "tools/profile") === false) &&
    (strpos($_SERVER['REQUEST_URI'], "tools/cpdv1/play_doc") === false) &&
    (strpos($_SERVER['REQUEST_URI'], "tools/cpdv1/reading_file") === false) &&
    (strpos($_SERVER['REQUEST_URI'], "tools/collab") === false) &&
    (strpos($_SERVER['REQUEST_URI'], "tools/web/viewer") === false) &&
    (strpos($_SERVER['REQUEST_URI'], "tools/web/view_ppt") === false) &&
    (strpos($_SERVER['REQUEST_URI'], "get_promo_data") === false) &&
    (strpos($_SERVER['REQUEST_URI'], "get_default_promo_answers") === false)) {

    // More graceful Ajax handing.
    if (isAJAX()) {
        $ret['status'] = 'redirect';
        $ret['redirect'] = "/wp-login.php?redirect_to=" . $_SERVER['HTTP_REFERER'];
        echo json_encode($ret);
        die();
    }

    header("location: http://" . $_SERVER['SERVER_NAME']);
    die();
}

require __DIR__ . '/../vendor/autoload.php';

require 'application.php';

// Create the main applications
$app = new \Application\Application();


$app->route();

//echo '<pre>';
//print_r($app);
//exit();
function isAJAX()
{
    // Check the source of the request.
    return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
        strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest');
}
