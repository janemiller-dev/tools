<?php
/**
 * Main Application object.
 *
 * Contains Common PHP functions.
 * @author     Sumit Kathayat <sumitk@mindfiresolutions.com>
 *
 */

namespace Application;

require '../../vendor/autoload.php';

use Aws\S3\S3Client;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Noodlehaus\Config;
use function print_r;
use \stdClass;

class Application
{


    public $config, $controllers, $log, $auth;
    private $s3, $twilio, $status, $email, $message;

    function __construct()
    {
        // Call different instances.
        $this->log = new Logger('tools');
        $this->log->pushHandler(new StreamHandler(__DIR__ . '/../logs/tools.log', Logger::DEBUG));
        $this->_loadConfiguration();
        $this->_loadControllers();
        $this->_createDBInstances();
//        $this->_createAWSInstances();
        $this->_createTwilioInstances();
        $this->_setStatus();
        $this->_createEmailInstance();
        $this->_setMessage();

    }

    /**
     * Load the configuration.
     */
    private function _loadConfiguration()
    {
        $suffixes = array('xml');

        // Load the configuration files.
        $this->config = new stdClass();
        foreach (scandir(__DIR__ . "/configs") as $file) {
            foreach ($suffixes as $suffix) {
                $len = strlen('.' . $suffix);
                if (substr_compare($file, '.' . $suffix, strlen($file) - $len, $len) === 0) {
                    $configuration = substr($file, 0, strlen($file) - $len);
                    $this->config->$configuration = new Config(__DIR__ . "/configs/$file");
                }
            }
        }
    }

    /**
     * Get the list controllers.  The purpose of this is to know if we are attempting to use a non-existant
     * controller.
     */
    private function _loadControllers()
    {
        // Get the list of controllers.
        $this->controllers = array();
        foreach (scandir(__DIR__ . "/controllers") as $file) {
            if (strlen($file) < 5) {
                continue;
            }
            if ($file === 'controller.php') {
                continue;
            }
            if (substr_compare($file, '.php', strlen($file) - 4, 4) === 0) {
                $controller = substr($file, 0, strlen($file) - 4);
                $this->controllers[] = $controller;
            }
        }
    }

    /**
     * Fetches Email User name and password.
     *
     */
    private function _createEmailInstance()
    {
        $this->email['username'] = $this->config->config['swiftmail']['username'];
        $this->email['password'] = $this->config->config['swiftmail']['password'];
    }

    /**
     * Load the models
     */
    private function _createDBInstances()
    {

        // Create the database instances.
        if (isset($this->config->config['database']['name'])) {
            $instance = \Model\PDO_model::instance($this->config->config['database']);
            $this->log->debug(print_r($instance, 1));
        } else {
            foreach ($this->config->config['database'] as $database) {
                \Model\PDO_model::instance($database);
            }
        }
    }

    /**
     * Load the AWS instances
     */
    private function _createAWSInstances()
    {
        $this->s3 = new S3Client([
            'credentials' => ['key' => $this->config->config['aws']['key'], 'secret' => $this->config->config['aws']['secret']],
            'version' => 'latest',
            'region' => 'us-west-2'
        ]);
    }

    /**
     * Fetches Twilio instances.
     *
     * @return mixed
     */
    private function _createTwilioInstances()
    {
        $this->twilio['app_sid'] = $this->config->config['twilio']['app_sid'];
        $this->twilio['auth_token'] = $this->config->config['twilio']['auth_token'];
        $this->twilio['account_sid'] = $this->config->config['twilio']['account_sid'];
    }

    /**
     * Sets the status and status for error, success and redirect.
     *
     */
    private function _setStatus()
    {
        $this->status = [];

        $this->status['success'] = 'success';
        $this->status['redirect'] = 'redirect';
        $this->status['fail'] = 'failure';
    }

    /**
     *
     * Set Message for different events.
     */
    private function _setMessage()
    {
        $this->message = [];
        $this->message['fail'] = 'Oops!! Some Error Occured. We are working on getting this Fixed';
    }


    public
    function getTwilio()
    {
        return $this->twilio;
    }

    /**
     * Route the request.
     */
    public
    function route()
    {
        $this->router = new \Klein\Klein();

        // Get the basepath.
        global $basepath, $gmap_key;
        $basepath = $this->config->config['basepath'];

        // Reset the server basepath.
        $request = \Klein\Request::createFromGlobals();
        $uri = $request->server()->get('REQUEST_URI');
        $request->server()->set('REQUEST_URI', substr($uri, strpos($uri, $basepath)));

        // Set the default layout
        $this->router->respond(function ($request, $response, $service) {
            $service->layout("views/template.php");
        });

        // Set the default page to /dashboard
        $this->router->respond(array('POST', 'GET'), $basepath, function () {
            $this->redirect("dashboard");
        });

        // Route to the controller.
        $this->router->respond(array('POST', 'GET'), $basepath . '[:controller]?/[**:rest]?',
            function ($request, $response, $service) use ($basepath) {
                // Set the basepath for the templates
                $service->basepath = $basepath;

                // Get the user ID from Wordpress.
                if (!isset($_SESSION['user_id']) || ($_SESSION['user_id'] !== wp_get_current_user()->ID)) {
                    $user = wp_get_current_user();
                    $_SESSION['user'] = $user;
                    $_SESSION['user_id'] = $user->ID;

                    echo('<script type="text/javascript">' .
                        ' window.addEventListener("DOMContentLoaded", (event) => {' .
                        ' document.getElementById("acm-reminder").click();})' .
                        '</script>');
                }

                // Not ajax?
                if (!$this->is_ajax() && (strpos($_SERVER['REQUEST_URI'], "tools/survey") === false)
                    && (strpos($_SERVER['REQUEST_URI'], "tools/cpdv1/play_doc") === false) &&
                    (strpos($_SERVER['REQUEST_URI'], "tools/profile") === false) &&
                    (strpos($_SERVER['REQUEST_URI'], "tools/web/viewer") === false) &&
                    (strpos($_SERVER['REQUEST_URI'], "tools/web/view_ppt") === false) &&
                    (strpos($_SERVER['REQUEST_URI'], "tools/collab") === false)) {
                    // If the user does not have any active tools and they are not going to a subscribe page,
                    // show the tool add page.
                    $menu_model = new \Model\Menu;
                    $menu = $menu_model->get_tools_menu($_SESSION['user_id']);
                    if (empty($menu['groups']) && empty($menu['tools'])) {
                        $this->redirect("/not-registered", false);
                    }
                }

                // Does the controller exist.
                $controller_name = strtolower($request->controller);
                if (!in_array($controller_name, $this->controllers)) {
                    echo "Unknown Route: " . $controller_name;
                    die();
                }

                // Make sure the class exists.
                $controller_class = 'Controller\\' . ucfirst($controller_name);
                if (class_exists($controller_class) && is_callable(array($controller_class, 'route'))) {
                    $controller = new $controller_class($this);
                    $controller->route($request, $response, $service);
                } else {
                    echo "Unknown Class: " . $controller_class;
                    die();
                }

            });

        $this->router->dispatch();
    }

    /**
     * Redirect to the specified page.
     */
    public
    function redirect($url, $use_basepage = true)
    {
        // Get the basepath
        $basepath = $this->config->config['basepath'];

        // If we are using the base page, adjust the redirect to include the base page.
        if ($use_basepage !== false) {
            ($url[0] == '/') ? $url = substr($url, 1) : '';
            $location = $basepath . $url;
        } else {
            $location = $url;
        }
        $this->log->debug("Redirecting to: " . $location);
        header('Location: ' . $location);
        die();
    }

    /**
     * Is this an ajax request?
     */
    public function is_ajax()
    {
        // Check the source of the request.
        return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest');
    }

    /**
     * Make sure the active product is set.
     */
    public
    function get_product()
    {
        // Already in session?
        if (isset($_SESSION['product'])) {
            return $_SESSION['product'];
        }

        // Look in database of the active profession.
        $industry_model = new \Model\Industry;
        if (($product = $industry_model->get_active_product($this->get_user_id())) === false) {
            return false;
        }

        // Set the profession in the session.
        $_SESSION['product'] = $product;
        return $product;
    }

    /**
     * Get the User ID.
     */
    public
    function get_user_id()
    {
        // Already in session?
        if (isset($_SESSION['user_id'])) {
            return $_SESSION['user_id'];
        }
        return false;
    }


    /**
     * Set the profession
     */
    public
    function set_product($product_id)
    {
        $_SESSION['product'] = $product_id;
    }

    public
    function set_email()
    {
        $user = $this->get_user_id();
        $_SESSION['email'] = $user->data->user_email;
    }

    public
    function get_s3()
    {
        return $this->s3;
    }

    /**
     * Return the remote IP address.
     */
    public
    function get_remote_ip()
    {
        $remoteip = '';

        (!empty($_SERVER['REMOTE_ADDR'])) ? $remoteip = $_SERVER['REMOTE_ADDR'] : '';
        return $remoteip;
    }


    /**
     * Fetches and return the static status
     *
     * @param $key
     * @return string
     */
    public function get_status($key, $err_message = '')
    {
        if ($key === 'fail') {
            try {
                $transporter = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
                    ->setUsername($this->get_email_credentials('username'))
                    ->setPassword($this->get_email_credentials('password'));

                $current_user = wp_get_current_user();
                $user_name = $current_user->data->user_nicename;

                $message = \Swift_Message::newInstance()
                    ->setSubject('Error Message')
                    ->setContentType('text/html')
                    ->setTo(array('sumitk@mindfiresolutions.com' => 'Sumit'))
                    ->setFrom(array($current_user->data->user_email => $user_name));

                $newHTML = "Error Message on cpd.powersellingtool.com";

                $msg_body = '<!DOCTYPE html><html><head></head><body>' .
                    '<div>' . 'User:' . $user_name .
                    '<br> Browser:' . $_SERVER['HTTP_USER_AGENT'] .
                    '<br> Time:' . date('m/d/Y h:i:s a', time()) .
                    '<br> Message: ' . $err_message .
                    '</div></body></html>';

                $message->setBody($msg_body, 'text/html');

                $this->log->debug("getting mailer");
                $mailer = \Swift_Mailer::newInstance($transporter);
                $mailer->send($message);
            } catch (\Exception $e) {
                echo $e;
                exit();
            }
        }

        return $this->status[$key];
    }


    public function get_message($key)
    {
        return $this->message[$key];
    }

    /**
     * Returns Email credentials.
     *
     * @param $key   String Key
     * @return mixed
     */
    public function get_email_credentials($key)
    {
        return $this->email[$key];
    }

}



