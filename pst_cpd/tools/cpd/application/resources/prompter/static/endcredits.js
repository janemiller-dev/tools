/*
 * jQuery endcredits Plugin
 *
 * Copyright (c) 2014 Daniel Malkafly <malk@epichail.com>
 * Dual licensed under the MIT or GPL Version 2 licenses or later.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */
$(document).ready(function () {
    var maskHeight, maskWidth;
    $('a[name=creditos]').click(function (e) {
        e.preventDefault();

        if (maskHeight === undefined) {
            $('#titles').fadeIn(1000);
            $('#titles').fadeTo("slow");
            $('#titles').fadeIn();
            $('#credits').css("left", (($('#credits').parent().width() - $('#credits').outerWidth()) / 2) + "px");
            $('#credits').css("top", $(this).parent().parent().siblings('#reader-body').height() - 100 + "px");
            $('#credits').show('slow');

            maskHeight = $('#credits')[0].scrollHeight;
            maskWidth = $(this).parent().parent().parent().width();

            $('#titles').css({
                'width': maskWidth,
                'height': $(this).parent().parent().parent().height() - 100
            });
        }
        $('#credits').animate({
            top: "-" + maskHeight + "px"
        }, {
            duration: (45 - $('#speed-control').val()) * 1000,
            complete: function () {
                $('#reset').trigger('click');
            },
            step: function (n, t) {
                // console.log(maskHeight);
                var pos = $(this).position();
            }
        });
    });

    $('#reset').click(function (e) {
        e.preventDefault();
        $('#titles').fadeOut();
        $('.window').fadeOut();
        $('#credits').clearQueue();
        $("#credits").stop();
        $('#credits').css("top", maskHeight + "px");
        maskHeight = undefined;
    });

    $('#next').click(function () {
        const height = $('#credits').find('p')[1].scrollHeight;
        $("#credits").stop();
        $('#credits').animate({
            top: "-=" + height + "px"
        }, {
            complete: function () {
                $('a[name=creditos]').trigger('click');
            }
        });
    });

    $('#back').click(function (e) {
        const height = $('#credits').find('p')[1].scrollHeight;
        $("#credits").stop();
        $('#credits').animate({
            top: "+=" + height + "px"
        }, {
            complete: function () {
                $('a[name=creditos]').trigger('click');
            }
        });
    });

    $('#stop').click(function (e) {
        e.preventDefault();
        // $('#credits').clearQueue();
        $('#credits').stop()
    });

    $('#titles').click(function () {
        $(this).hide();
        $('#credits').css("top", maskHeight + "px");
        $('.window').hide();
    });

    $('#speed-control').change(function () {
        $('#stop').trigger('click');
        $('a[name=creditos]').trigger('click');
    })
});
