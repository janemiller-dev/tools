var questions_id = [];
$(document).ready(function () {
    $('.tool-menu').css('display', 'none');

    // Fetch the Parameters from URL.
    const searchParams = new URLSearchParams(window.location.search),
        survey_id = searchParams.get('survey_id'),
        client_id = searchParams.get('id'),
        outline_id = searchParams.get('outline_id');

    const get_ques = makeAjaxCall('survey/get_ques', {outline: outline_id});

    // Enable Voice Recognition.
    voiceRecognition();

    // Get the Questions List for survey.
    get_ques.then(function (data) {

        let count = 0, phase_number = 0,
            phase_value = [
                'Phase 1 – Current Factors',
                'Phase 2 – Emerging Challenges',
                'Phase 3 – Future Implications',
                'Phase 4 – Known Options',
                'Phase 5 – Plan of Action'];

        // Append questions to Form.
        $('#bus-form')
            .append(
                $.map(data.questions, function (question, index) {
                    questions_id[index] = question.tsq_id;
                    let phase_string = '';
                    count++;

                    if (((count - 1) === 5 * phase_number) || count === 1) {
                        phase_string = '<h3 style="font-weight: bold">' + phase_value[phase_number] + '</h3>';
                        phase_number++;
                    }

                    const str = question.tsq_content.replace('{{', '').replace('}}', '');

                    return '<div class="container" style="margin-top: 3vh"><label>'
                        + phase_string + (index + 1) + '.' + str + '</label><div class="input-group">' +
                        '<textarea rows="2" class="form-control" name="' + 'ans' + question.tsq_id +
                        '" type = "text" id="' + 'ans' + question.tsq_id + '" ></textarea>' +
                        '<span class="input-group-addon btn btn-secondary stt"><i class="fa fa-microphone"></i></span>' +
                        '</div></div>'
                }));


        const get_ans = makeAjaxCall('survey/get_ans', {
            question_id: questions_id,
            client_id: client_id,
            survey_id: survey_id
        });

        get_ans.then(function (data) {

            // Remove slashes and add answer for the questions.
            $.each(data.answers, function (index, answer) {
                $('#ans' + answer.ts_ans_question_id).val(answer.ts_ans_content.replace('\\', ''));
            });
            speechFunctionality();
        });
    });


    // Saves the response from user.
    $(document).on('click', '#save_bus', function (e) {
        let data = $('#bus-form').serializeArray();
        data.push({name: 'question_id', value: questions_id});
        data.push({name: 'client_id', value: client_id});
        data.push({name: 'survey_id', value: survey_id});

        // Save the answer.
        const set_ans = makeAjaxCall('survey/set_ans', data);
        set_ans.then(function (data) {
            toastr.success('Response Saved`', 'Success!!');
        });
    });

});