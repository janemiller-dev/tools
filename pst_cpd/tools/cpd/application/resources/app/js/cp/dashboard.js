/**
 *
 * Content Provider Dashboard.
 *
 * @summary      Content Provider Dashboard.
 * @description  This file contains functions for Displaying CP tools.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */

$(document).ready(function () {
    view_notes.init();
});

/**
 *
 * View Notes available.
 */

let view_notes = function () {

    let max_seq = 0,
        deal_id;

    // Initialize and get the DMD ID.
    let init = function (event) {
            // Fetch the Notes Content.
            const cp_data = makeAjaxCall('code/get_components', {});
            $('#cp-component-table tbody').empty();

            // Append Fetched data to the Notes popup.
            cp_data.then(function (data) {
                let count = 1;
                $.each(data.components, function (index, component) {
                    if (('tsl' === component.uc_tool) &&
                        ('Survey' !== component.uc_name) &&
                        ('Profile' !== component.uc_name)) {
                        $('#cp-component-table tbody')
                            .append($('<tr>')
                                .append($('<td class="text-center">')
                                    .append(count++))
                                .append($('<td class="text-center">')
                                    .append($('<a class="cp-name" ' +
                                        'href="' + jsglobals.base_url + 'cp' + component.uc_controller +  '">'
                                        + component.uc_name + '</a>'))
                                )
                                .append($('<td class="text-center">')
                                    .append(component.uc_description)
                                )
                                .append($('<td class="text-center">')
                                    .append('0000-00-00')
                                )
                            );
                    }
                })
            });
        },

        // Stores Notes data.
        store_data = function () {
            const event_selector = $('.event');

            event_selector.unbind('change');
            event_selector.change(function () {
                makeAjaxCall('cpdv1/update_notes_content', {
                    'id': deal_id,
                    'col': $(this).data('name'),
                    'val': $(this).val(),
                    'seq': $(this).data('seq')
                });
            });
        },

        // Adds new column to the event List.
        add_event = function () {

            // Add a new event to the list.
            $('#add-event').click(function () {
                let dummy_content = $('#notes-div').clone();
                dummy_content.find('input')
                    .attr('data-seq', (parseInt(max_seq) + 1))
                    .attr('id', 'dn_name' + (parseInt(max_seq) + 1))
                    .attr('value', '');

                dummy_content.find('textarea')
                    .attr('data-seq', (parseInt(max_seq) + 1))
                    .attr('id', 'dn_note' + (parseInt(max_seq) + 1)).text('');

                // Append to the last of List.
                $('#notes-row').append('<div class="col-xs-12 additional-row">' + dummy_content.html() + '</div>');
                max_seq = parseInt(max_seq) + 1;
                store_data();
            });
        };

    return {
        init: init,
        add_event: add_event
    };
}();