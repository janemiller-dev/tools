$(document).ready(function() {

    // Handle the Add code dialog.
    $('#add-code-dialog').on('show.bs.modal', function(e) {
	var ct_id = $(e.relatedTarget).data('ct-id');
	var ct_name = $(e.relatedTarget).data('ct-name');

	$('#ac-subtitle').html('Add code for: ' + ct_name);
	$('#ac-ct-id').val(ct_id);

	$('#ac-name').val('');
	$('#ac-abbrev').val('');
	$('#ac-description').val('');
    });

    // Handle the Add Code Type dialog
    $('#add-code-form').formValidation({
	framework: 'bootstrap',
	icon: {
	    valid: 'glyphicon glyphicon-ok',
	    invalid: 'glyphicon glyphicon-remove',
	    validating: 'glyphicon glyphicon-refresh',
	},
	fields: {
	    name: {
		validators: {
		    notEmpty: {
			message: "The code name is required."
		    },
		}
	    },
	    abbrev: {
		validators: {
		    notEmpty: {
			message: "The code abbreviation is required."
		    },
		}
	    },
	    description: {
		validators: {
		    notEmpty: {
			message: "The code description is required."
		    },
		}
	    },
	}
    }).on('success.form.fv', function(e) {
	e.preventDefault();

	// Get the form instance
	var $form = $(e.target);
	var bv = $form.data('formValidation');

	// Get the form data and submit it.
	var form_data = $form.serialize();
	$.ajax({
	    url: jsglobals.base_url + 'code/add_code',
	    dataType: 'json',
	    type: 'post',
	    data: form_data
	}).done(function(data) {
	    if (data.status != 'success') {
		toastr.error(data.message);
	    }
	    else {
		$('#add-code-dialog').modal('hide');
		refreshPage();
	    }
	    bv.resetForm();
	}).fail(function(jqXHR, status) {
	    toastr.error('Error: ' + jqXHR.status + ': ' + jqXHR.statusText);
	});
    });
});

