$(document).ready(function() {

    // Handle the Add code dialog.
    $('#add-code-type-dialog').on('show.bs.modal', function(e) {
	$('#act-name').val('');
	$('#act-abbrev').val('');
	$('#act-description').val('');
    });

    // Handle the Add Code Type dialog
    $('#add-code-type-form').formValidation({
	framework: 'bootstrap',
	icon: {
	    valid: 'glyphicon glyphicon-ok',
	    invalid: 'glyphicon glyphicon-remove',
	    validating: 'glyphicon glyphicon-refresh',
	},
	fields: {
	    name: {
		validators: {
		    notEmpty: {
			message: "The code type name is required."
		    },
		}
	    },
	    abbrev: {
		validators: {
		    notEmpty: {
			message: "The code type abbreviation is required."
		    },
		}
	    },
	    description: {
		validators: {
		    notEmpty: {
			message: "The code type description is required."
		    },
		}
	    },
	}
    }).on('success.form.fv', function(e) {
	e.preventDefault();

	// Get the form instance
	var $form = $(e.target);
	var bv = $form.data('formValidation');

	// Get the form data and submit it.
	var form_data = $form.serialize();
	$.ajax({
	    url: jsglobals.base_url + 'code/add_code_type',
	    dataType: 'json',
	    type: 'post',
	    data: form_data
	}).done(function(data) {
	    if (data.status != 'success') {
		toastr.error(data.message);
	    }
	    else {
		$('#add-code-type-dialog').modal('hide');
		refreshPage();
	    }
	    bv.resetForm();
	}).fail(function(jqXHR, status) {
	    toastr.error('Error: ' + jqXHR.status + ': ' + jqXHR.statusText);
	});
    });
});


