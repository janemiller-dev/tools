$(document).ready(function() {
    refreshPage();
});

// Update the page
function refreshPage() {

    // Update the code table on entry.
    $.ajax({
	url: jsglobals.base_url + "code/get_code_types",
	dataType: "json",
	type: "post",
	data: {
	    return_type: 'indexed'
	}
    }).done(function(data) {

	// Check for data failure
	if (data.status == 'failure') {
	    toastr.error(data.message);
	    return;
	}

	// Display the list of codes.
	displayCodeTable(data.code_types);
    }).fail(function(jqXHR, status) {
	toastr.error("Unable to communicate with the server. Please re-try.");
    });
    
}

// The code datatable
var code_datatable = $('code-type-table').DataTable();

// Display the list of code types.
function displayCodeTable(code_types) {

    // Destroy the current datatable so that it can be rebuilt with the new data.
    code_datatable.destroy();

    // Clear the old table
    $('#code-type-table tbody').empty();

    // If there are no code_types, show the no-code message.
    if (code_types.length == 0) {
	$('#no-code-types').show();
	$('#code-table-div').hide();
	return;
    }
    $('#no-code-types').hide();
    $('#code-table-div').show();

    // Insert the code_types into the list.
    $.each(code_types, function(key, code_type) {

	// Allow the code name to be changed in place.
	var name = '<a href="#" class="editable" id="code-type-name-' + code_type.ct_id + '" data-type="text" data-name="ct_name" data-pk="' + code_type.ct_id + '" data-value="' + code_type.ct_name + '" data-url="' + jsglobals.base_url + 'code/update_field">' + code_type.ct_name + '</a>';

	name += '&nbsp;<button class="delete-code-type btn btn-default btn-xs" data-toggle="modal" data-target="#delete-code-type-dialog" data-ct-id="' + code_type.ct_id + '" data-ct-name="' + code_type.ct_name + '" data-ct-type="' + code_type.ct_abbrev + '" data-ct-description="' + code_type.ct_description + '"><span class="glyphicon glyphicon-trash"></span></button>';

	// Allow the code abbrev to be changed in place.
	var abbrev = '<a href="#" class="editable" id="code-type-abbrev-' + code_type.ct_id + '" data-type="text" data-name="ct_abbrev" data-pk="' + code_type.ct_id + '" data-value="' + code_type.ct_abbrev + '" data-url="' + jsglobals.base_url + 'code/update_field">' + code_type.ct_abbrev + '</a>';

	// Allow the code description to be changed in place.
	var description = '<a href="#" class="editable" id="code-type-description-' + code_type.ct_id + '" data-type="text" data-name="ct_description" data-pk="' + code_type.ct_id + '" data-value="' + code_type.ct_description + '" data-url="' + jsglobals.base_url + 'code/update_field">' + code_type.ct_description + '</a>';

	// Show the code type's codes.
	var codes = '';
	if (code_type.codes.length > 0) {
	    codes += '<table>';
	    $.each (code_type.codes, function(k, code) {
		codes += '<tr>';
		codes += '<td style="padding-right: 10px">';
		codes += '<a href="#" class="editable" id="code-name-' + code.code_id + '" data-type="text" data-name="code_name" data-pk="' + code.code_id + '" data-value="' + code.code_name + '" data-url="' + jsglobals.base_url + 'code/update_field">' + code.code_name + '</a>';
		codes += '&nbsp;<button class="delete-code btn btn-default btn-xs" data-toggle="modal" data-target="#delete-code-dialog" data-code-id="' + code.code_id + '" data-code-name="' + code.code_name + '" data-code-abbrev="' + code.code_abbrev + '" data-code-description="' + code.code_description + '"><span class="glyphicon glyphicon-trash"></span></button>';
		codes += '</td>';
		codes += '<td style="padding-right: 10px"><a href="#" class="editable" id="code-abbrev-' + code.code_id + '" data-type="text" data-name="code_abbrev" data-pk="' + code.code_id + '" data-value="' + code.code_abbrev + '" data-url="' + jsglobals.base_url + 'code/update_field">' + code.code_abbrev + '</a></td>';
		codes += '<td style="padding-right: 10px"><a href="#" class="editable" id="code-description-' + code.code_id + '" data-type="text" data-name="code_description" data-pk="' + code.code_id + '" data-value="' + code.code_description + '" data-url="' + jsglobals.base_url + 'code/update_field">' + code.code_description + '</a></td>';
		codes += '</tr>';
	    });
	    codes += '</table>';
	}
	codes += '<button class="delete-code-type btn btn-default btn-xs" data-toggle="modal" data-target="#add-code-dialog" data-ct-id="' + code_type.ct_id + '" data-ct-name="' + code_type.ct_name + '">Add Code</span></button>';
	
        // Create the list of code types.
	$("#code-type-table").find('tbody#main-body')
	    .append($('<tr>')
		    .append($('<td>')
			    .append(name))
		    .append($('<td>')
			    .append(abbrev))
		    .append($('<td>')
			    .append(description))
		    .append($('<td>')
			    .append(codes))
		   );

    });

    // Set the editable fields.
    $('.editable').editable({
	placement: "right",
	emptytext: 'Not Set',
	success: function() {
	    refreshPage();
	}
    });

    // Create the new code datatable.
    code_datatable = $('#code-type-table').DataTable();
}
