$(document).ready(function() {

    // Handle the delete code dialog
    $('#delete-code-type-dialog').on('show.bs.modal', function(e) {
	// Get the dialog parameters
	var ct_id = $(e.relatedTarget).data('ct-id');
	var ct_name = $(e.relatedTarget).data('ct-name');
	var ct_type = $(e.relatedTarget).data('ct-type');
	var ct_description = $(e.relatedTarget).data('ct-description');

	// Set the form fields
	$(e.currentTarget).find('#dct-ct-id').val(ct_id);
	$(e.currentTarget).find('#dct-name').val(ct_name);
	$(e.currentTarget).find('#dct-type').val(ct_type);
	$(e.currentTarget).find('#dct-description').val(ct_description);
    });

    // Handle the delete code form
    $('#delete-code-type-form').formValidation({
	framework: 'bootstrap',
	icon: {
	    valid: 'glyphicon glyphicon-ok',
	    invalid: 'glyphicon glyphicon-remove',
	    validating: 'glyphicon glyphicon-refresh',
	},
    }).on('success.form.fv', function(e) {
	e.preventDefault();

	// Get the form instance
	var $form = $(e.target);
	var bv = $form.data('formValidation');

	// Get the form data and submit it.
	var form_data = $form.serialize();
	$.ajax({
	    url: '/code/delete_code_type',
	    dataType: 'json',
	    type: 'post',
	    data: form_data
	}).done(function(data) {
	    if (data.status != 'success') {
		toastr.error(data.message);
	    }
	    else {
		$('#delete-code-type-dialog').modal('hide');
	    }
	    bv.resetForm();
	    refreshPage();
	}).fail(function(jqXHR, status) {
	    toastr.error("Server communication error. Please try again.");
	}).always(function() {
	});
    });
});
