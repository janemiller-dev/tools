$(document).ready(function() {

    // Handle the delete code dialog
    $('#delete-code-dialog').on('show.bs.modal', function(e) {
	// Get the dialog parameters
	var code_id = $(e.relatedTarget).data('code-id');
	var code_name = $(e.relatedTarget).data('code-name');
	var code_abbrev = $(e.relatedTarget).data('code-abbrev');
	var code_description = $(e.relatedTarget).data('code-description');

	// Set the form fields
	$(e.currentTarget).find('#dc-code-id').val(code_id);
	$(e.currentTarget).find('#dc-name').val(code_name);
	$(e.currentTarget).find('#dc-abbrev').val(code_abbrev);
	$(e.currentTarget).find('#dc-description').val(code_description);
    });

    // Handle the delete code form
    $('#delete-code-form').formValidation({
	framework: 'bootstrap',
	icon: {
	    valid: 'glyphicon glyphicon-ok',
	    invalid: 'glyphicon glyphicon-remove',
	    validating: 'glyphicon glyphicon-refresh',
	},
    }).on('success.form.fv', function(e) {
	e.preventDefault();

	// Get the form instance
	var $form = $(e.target);
	var bv = $form.data('formValidation');

	// Get the form data and submit it.
	var form_data = $form.serialize();
	$.ajax({
	    url: '/code/delete_code',
	    dataType: 'json',
	    type: 'post',
	    data: form_data
	}).done(function(data) {
	    if (data.status != 'success') {
		toastr.error(data.message);
	    }
	    else {
		$('#delete-code-dialog').modal('hide');
	    }
	    bv.resetForm();
	    refreshPage();
	}).fail(function(jqXHR, status) {
	    toastr.error("Server communication error. Please try again.");
	}).always(function() {
	});
    });
});
