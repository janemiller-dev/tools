$(document).ready(function() {
    refreshPage();
});

// Update the page
function refreshPage() {
    updateTools();
}

// Update the list of tools.
function updateTools() {

    // Create the databate - use server side data population.
    tool_datatable = $('#tool-table').DataTable({
        "pageLength": 100,
		"serverSide": true,
		"responsive": true,
		"ajax":{
		    url: "tool/get_tools",
		    type: "post",
			error: ajaxError
		},
		"columns": [
		    {"data": "tool_id", "name": "ID"},
		    {"data": "tg_id", "name": "Group ID"},
		    {"data": "tg_name", "name": "Group"},
		    {"data": "tool_name", "name": "Tool"},
		    {"data": "tool_description", "name": "Description"},
		    {"data": "tool_is_admin", "name": "Admin"},
		    {"data": "code_name", "name": "Status"},
		],
		"rowCallback": function(row, data, index) {
		    $('td', row).eq(0).html('<a href="tool/group/' + data.tg_id + '">' + data.tg_name + "</a>");
		    $('td', row).eq(1).html('<a href="tool/tool/' + data.tool_id + '">' + data.tool_name + "</a>");
		    $('td', row).eq(3).html((data.tool_is_admin == '1' ? 'Yes' : 'No'));
		},
		"columnDefs": [
		    {"targets": [0, 1], "visible": false, "searchable": false},
		    {"targets": [2, 3], "visible": true, "searchable": false},
		    {"targets": [4, 5, 6], "visible": true, "searchable": true},
		]
    });
}
