$(document).ready(function() {
    refreshPage();

});

// Update the page
function refreshPage() {
    // Get the ID from the URL
    var path = window.location.pathname;
    var components = path.split('/');
    var id = components[components.length - 1];

        // Get the subscription status.
    $.ajax({
        url: "tool/get_group",
        dataType: "json",
        type: 'post',
	data: {
	    id: id
	}
    }).done(function(data) {
        if (data.status == 'failure') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
	    window.location.href = data.redirect;
            return;
        }
        updateGroup(data.group);

    }).fail(function(jqXHR, status) {
        toastr.error("Error");
    });
}

// Update the tool group
function updateGroup(group) {

    // Update the title.
    $('#group-name').html(group.tg_name).addClass('editable')
	.attr({
	    'data-url': 'common/update_db_field',
	    'data-name': 'tool_group.tg_name',
	    'data-pk': group.tg_id,
	    'data-type': 'text',
	    'title': 'New Name',
	});

    // Update the description.
    $('#group-description').html(group.tg_description).addClass('editable')
	.attr({
	    'data-url': 'common/update_db_field',
	    'data-name': 'tool_group.tg_description',
	    'data-pk': group.tg_id,
	    'data-type': 'textarea',
	    'title': 'New Description',
	});

    // Administer only?
    $('#group-is-admin').addClass('editable')
	.attr({
	    'data-url': 'common/update_db_field',
	    'data-name': 'tool_group.tg_is_admin',
	    'data-pk': group.tg_id,
	    'data-type': 'select',
	    'data-source': "[{value: 0, text: 'No'}, {value: 1, text: 'Yes'}]",
	    'data-value': group.tg_is_admin,
	    'title': 'Administrator Only?',
	});

    // Get the list of tools in the tool group.
    tool_datatable = $('#tool-table').DataTable({
        "pageLength": 100,
	"serverSide": true,
	"responsive": true,
	"ajax":{
	    url: "tool/get_tools",
	    data: {
		"group_id": group.tg_id
	    },
	    type: "post",
	},
	"columns": [
	    {"data": "tool_id", "name": "ID"},
	    {"data": "tool_name", "name": "Tool"},
	    {"data": "tool_description", "name": "Description"},
	    {"data": "tool_is_admin", "name": "Admin"},
	    {"data": "code_name", "name": "Status"},
	],
	"rowCallback": function(row, data, index) {
	    $('td', row).eq(0).html('<a href="/tool/tool/' + data.tool_id + '">' + data.tool_name + "</a>");
	    $('td', row).eq(2).html((data.tool_is_admin == '1' ? 'Yes' : 'No'));
	},
	"columnDefs": [
	    {"targets": [0], "visible": false, "searchable": false},
	    {"targets": [1, 2, 4], "visible": true, "searchable": true},
	    {"targets": [3], "visible": true, "searchable": false},
	]
    });

    // Link the editable classes to the plugin.
    setEditable();
    //useTooltip();
    usePopover();
}
