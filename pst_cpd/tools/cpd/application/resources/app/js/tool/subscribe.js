$(document).ready(function() {
    refreshPage();

    // Catch the click on the subscribe toggle button
    $('#subscribe-toggle').click(function() {
	$.ajax({
            url: "tool/toggle_subscription",
            dataType: "json",
            type: 'post',
	}).done(function(data) {
            if (data.status == 'failure') {
		toastr.error(data.message);
		return;
            }
            else if (data.status == 'redirect') {
		window.location.href = data.redirect;
		return;
            }
            refreshPage();

	}).fail(function(jqXHR, status) {
            toastr.error("Error");
	});
    });

});

// Get the page data and display it.
function refreshPage() {

    // Get the list of tools
    $.ajax({
        url: "tool/get_subscription",
        dataType: "json",
        type: 'post',
    }).done(function(data) {
        if (data.status == 'failure') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
	    window.location.href = data.redirect;
            return;
        }
        updateSubscription(data.subscription);

    }).fail(function(jqXHR, status) {
        toastr.error("Error");
    });
}

// Update the subscription status
function updateSubscription(subscription) {

    // Display the current status.
    $('#subscription-status').html(subscription.status_name);

    // Display the eligible status.
    if (subscription.eligible)
	$('#eligibility-status').html('Yes');
    else
	$('#eligibility-status').html('No');
	
    // Set the subscribe toggle appropriately.
    if (subscription.status == 'no')
	$('#subscribe-toggle').html('Subscribe').attr('disabled', false);
    else if (subscription.status == 'inactive')
	$('#subscribe-toggle').html('Re-Activate').attr('disabled', false);
    else if (subscription.status == 'active')
	$('#subscribe-toggle').html('Cancel Subscription').attr('disabled', false);

    if (!subscription.eligible)
	$('#subscribe-toggle').attr("disabled", true);
}
