$(document).ready(function () {
    $('#view-asset-cond-info-dialog').on('show.bs.modal', function (event) {
        const asset_type = 'cond';
        let display_name = 'Condition';

        $('#view-asset-info-title').text('View/Edit ' + display_name);
        $('#name-col-head').text(display_name + ' Name');
        $('#add-asset-info-button').text('Add New ' + display_name);
        $('#add-asset-info-button').attr('data-type', asset_type);

        const get_asset_type_info = makeAjaxCall('homebase/get_asset_type_info', {
            type: asset_type
        });

        get_asset_type_info.then(function (data) {
            $('#asset-info-dialog-table tbody').empty();
            $.each(data.asset_type_info, function (index, info) {
                $('#asset-info-dialog-table tbody').append('<tr>' +
                    '<td class="text-center">' + (index + 1) + '</td>' +
                    '<td class="text-center"><span class="editable" data-type="text" data-pk="' + info.hai_id + '"' +
                    ' data-url="' + jsglobals.base_url + 'homebase/update_asset_info" ' +
                    ' data-name="' + info.hai_type + '">' + info.hai_value + '</td>' +
                    '<td class="text-center">' +
                    '<button class="btn btn-sm btn-default" data-toggle="modal" data-name="' + info.hai_value + '"' +
                    'data-target="#delete-asset-info-dialog" data-id="' + info.hai_id + '" ' +
                    'data-type="' + display_name + '">' +
                    '<i class="fa fa-trash"></i></td>' +
                    '</tr>');
            });
        });
    });

    $('#view-asset-info-dialog').on('shown.bs.modal', function (event) {
        setEditable();
        $(document).off('focusin.modal');
    });
});