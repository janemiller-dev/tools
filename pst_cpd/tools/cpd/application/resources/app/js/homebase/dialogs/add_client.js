$(document).ready(function () {
    let fv, id, origin;

    // Add Asset button click Handler
    $(document).on('click', '.add-form-asset', function (e) {
        let dummy_asset = $('.dummy-asset-info').clone();
        const position = $(this).parent('div').parent('div').siblings('div').find('input').attr('data-position'),
            parent_div = $(this).parent('div').parent('div').parent('div'),
            asset_number = $(this).parent('div').parent('div').siblings('div').find('input').attr('data-asset');

        // Set the appropriate Classes and attributes for Asset rows.
        dummy_asset = dummy_asset.removeClass('hidden dummy-asset-info').addClass('add-asset-info');
        dummy_asset.find('#asset-name').attr('name', 'asset_name' + position + '-' + asset_number);
        dummy_asset.find('#asset-location').attr('name', 'asset_location' + position + '-' + asset_number);
        dummy_asset.find('#address').attr('name', 'asset_address' + position + '-' + asset_number)
            .attr('id', 'address' + position + '-' + asset_number);
        dummy_asset.find('#asset-criteria').attr('name', 'asset_criteria' + position + '-' + asset_number);
        dummy_asset.find('#asset-zip').attr('name', 'asset_zip' + position + '-' + asset_number);
        dummy_asset.find('#asset-status').attr('name', 'asset_status' + position + '-' + asset_number);


        // New added fields.
        dummy_asset.find('#asset-city').attr('name', 'asset_city' + position + '-' + asset_number);
        dummy_asset.find('#asset-state').attr('name', 'asset_state' + position + '-' + asset_number);
        dummy_asset.find('#asset-product').attr('name', 'asset_product' + position + '-' + asset_number);
        dummy_asset.find('#asset-units').attr('name', 'asset_unit' + position + '-' + asset_number);
        dummy_asset.find('#asset-sq-ft').attr('name', 'asset_sq_ft' + position + '-' + asset_number);
        dummy_asset.find('#asset-cond').attr('name', 'asset_cond' + position + '-' + asset_number);
        dummy_asset.find('#asset-criteria').attr('name', 'asset_criteria' + position + '-' + asset_number);
        dummy_asset.find('#asset-lead').attr('name', 'asset_lead' + position + '-' + asset_number);
        dummy_asset.find('#asset-source').attr('name', 'asset_source' + position + '-' + asset_number);

        // Increment number of assets.
        $(this).parent('div').parent('div').siblings('div').find('input').attr('data-asset', parseInt(asset_number) + 1);

        parent_div.after(dummy_asset);
        e.preventDefault();
    });

    // Click handler for select box
    $(document).on('change', '.toggle-modal-select', function () {
        if ('-1' === $(this).val()) {
            $('#add-lead-button').attr('data-type', $(this).attr('data-type'));
            $('#lead-type').val($(this).attr('data-type'));

            $('#' + $(this).attr('data-target')).modal('show');
        }
    });

    // Click handler for Delete Button.
    $(document).on('click', '.delete-form-asset', function (e) {
        const asset_count = $(this).parent('div').parent('div').siblings('div').find('input').attr('data-asset');

        // Decrement asset count.
        $(this).parent('div').parent('div').siblings('div').find('input').attr('data-asset', parseInt(asset_count) - 1);
        $(this).parent('div').parent('div').remove();
        e.preventDefault();
    });

    $('#add-client-dialog').on('show.bs.modal', function () {
        // Remove any previous yellow rows and Asset info rows.
        $('.yellow_row').removeClass('yellow_row');
        $('.add-asset-info').remove();

        // Enable form cache.
        const add_client_form_selector = $("#add-client-form");
        add_client_form_selector.formcache("clear");
        add_client_form_selector.formcache("destroy");

        // Set the form input asset count to 2.
        $('.client-row input').attr('data-asset', 2);

        $('.additional-client-info').remove();
        add_client_form_selector[0].reset();

        const get_all_products = makeAjaxCall('industry/get_all_products'),
            product_selector = $('.add-client-product');

        get_all_products.then(function (data) {
            product_selector.empty();
            product_selector.append('<option value="0" selected>--None--</option>')
            $.each(data.products, function (index, product) {
                product_selector
                    .append('<option style="font-weight: bold; font-style: italic;"' +
                        ' value="' + product[0]['u_product_group_id'] + '">' + index + '</option>');

                $.each(product, function (index, individual_product) {
                    product_selector
                        .append('<option value="' + individual_product.u_product_id + '">' +
                            individual_product.u_product_name + '</option>');
                });
            });
        });

        get_all_deal_type();

        // Fetch the parameters from URL.
        const path = window.location.pathname,
            components = path.split('/'),
            client_id = components[components.length - 1];

        origin = components[2];

        // Fetch all the Criteria and Target Audience if request is from DMD.
        if (origin === 'tsl') {
            id = client_id;
            get_all_criteria();
        }
        else if (origin === 'cpdv1') {
            id = client_id;
            get_all_criteria();
        }
        else {
            get_all_criteria();
            id = 0;
        }
    });

    $(document).on('click', '#close-add-client-dialog', function () {
        $('#add-client-dialog').modal('hide');
    });

    // Append Additional rows for client info.
    $('#client-count').change(function () {
        $('.additional-client-info').remove();

        // Start from two as one row is present by default.
        let i = 2;
        while (i <= $(this).val()) {
            let info_html = $('.add-client-info').clone();
            info_html.find('.add-asset-info').remove();

            // Change the name and ID attribute for additional records.
            info_html.find('#client-name1')
                .attr('name', 'client_name' + i)
                .attr('id', 'client-name' + i)
                .attr('data-position', i)
                .attr('data-asset', 2);

            info_html.find('#client-email1')
                .attr('name', 'client_email' + i)
                .attr('id', 'client-email' + i);

            info_html.find('#main-phone1')
                .attr('name', 'main_phone' + i)
                .attr('id', 'main-phone' + i);

            info_html.find('#mobile-phone1')
                .attr('name', 'mobile_phone' + i)
                .attr('id', 'mobile-phone' + i);

            info_html.find('#second-phone1')
                .attr('name', 'second_phone' + i)
                .attr('id', 'second-phone' + i);

            info_html.find('#entity-name1')
                .attr('name', 'entity_name' + i)
                .attr('id', 'entity-name' + i);

            info_html.find('#asset-name1-1')
                .attr('name', 'asset_name' + i + '-1');

            info_html.find('#asset-location1-1')
                .attr('name', 'asset_location' + i + '-1');

            info_html.find('#address1-1')
                .attr('name', 'asset_address' + i + '-1');

            info_html.find('#asset_criteria1-1')
                .attr('name', 'asset_criteria' + i + '-1');

            info_html.find('#zip1-1')
                .attr('name', 'asset_zip' + i + '-1');

            info_html.find('#asset-status1-1')
                .attr('name', 'asset_status' + i + '-1');

            info_html.find('#city1-1')
                .attr('name', 'asset_city' + i + '-1');

            info_html.find('#state1-1')
                .attr('name', 'asset_state' + i + '-1');

            info_html.find('#asset-product1-1')
                .attr('name', 'asset_product' + i + '-1');

            info_html.find('#asset-units1-1')
                .attr('name', 'asset_unit' + i + '-1');

            info_html.find('#asset-sq-ft1-1')
                .attr('name', 'asset_sq_ft' + i + '-1');

            info_html.find('#asset-cond1-1')
                .attr('name', 'asset_cond' + i + '-1');

            info_html.find('#asset-criteria1-1')
                .attr('name', 'asset_criteria' + i + '-1');

            info_html.find('#asset-lead1-1')
                .attr('name', 'asset_lead' + i + '-1');

            info_html.find('#asset-source1-1')
                .attr('name', 'asset_source' + i + '-1');

            $('.add-client-modal').append('<div class="additional-client-info col-xs-12 client-info' + i + '"' +
                ' style="margin-top: 2vh">' +
                info_html.html() + '</div>');
            i++;
        }
        add_cache();
        refreshCriteria();
    });

    // Form Validation.
    $('#add-client-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            client_name1: {
                validators: {
                    notEmpty: {
                        message: "The client name is required."
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the add new client form.
        const $form = $(e.target),
            fv = $form.data('formValidation'),
            // Get the form data and submit it.
            client_data = $form.serializeArray();

        client_data.push({name: 'origin', value: origin});
        client_data.push({name: 'id', value: id});

        let promises = [];

        // Loop through all the clients and fetch there address if exits.
        for (let i = 1; i <= $('#client-count').val(); i++) {

            // Asset Count equals additional asset plus default assets.
            const asset_count = $('.client-info' + i + ' .add-asset-info').length +
                $('.client-info' + i + ' .asset-info').length;
            client_data.push({name: 'asset-count' + i, value: asset_count});
        }

        add_client();

        // Add a new client to the homebase.
        function add_client() {
            // add_client_loader();
            // Fetch Caller List.
            $.ajax({
                url: jsglobals.base_url + 'homebase/add_client',
                dataType: 'json',
                type: 'post',
                data: client_data,
                error: ajaxError
            }).done(function (data) {

                // console.log(data);

                if (data.status !== 'success') {
                    toastr.error(data.message);
                    return;
                } else if (data.status === 'redirect') {
                    window.location.href = data.redirect;
                    return;
                }

                toastr.success('Client Added!!', 'Success!!');
                // $.unblockUI();
                fv.resetForm();
                $($form)[0].reset();
                $('#add-client-form')[0].reset();
                $('#add-client-dialog').modal('hide');
                (typeof refreshPage !== 'undefined' && $.isFunction(refreshPage)) ? refreshPage() : homebase.init();
            }).fail(function (jqXHR, textStatus) {
                // $.unblockUI();
                toastr.error('Could not get the requested Client Project Dashboard instance.', 'Error!!');
            }).always(function () {
            });
        }
    });
});

const add_cache = () => {
    $("#add-client-form").formcache("destroy");
    $("#add-client-form").formcache("clear");
};

// Populate the Add Client Popup using fields extracted from excel file.
const populate_client = data => {
    // Remove any previous Clients and Assets.
    $('.additional-client-info, .add-asset-info').remove();

    let client_array = [],
        client_name_array = [],
        client_count = 1;
    // Populate add client info pop up fields.
    $.each(data, function (index, client) {
        $('.asset-info').remove();

        let current_index = index + 1,
            client_name_selector = $('#client-name' + current_index),
            asset_number = 1;

        // if (client_array.indexOf(client['Name']) > -1) {
        //     current_index = client_array.indexOf(client['Name']);
        //     asset_number = $('#add-client-' + current_index + ' .add-asset-info').length + 1;
        // } else {
        client_name_array.push(client['Client Name']);
        client_array[client_count] = client['Client Name'];

        // Check if this row is already present.
        if (0 === client_name_selector.length) {
            client_count = client_count + 1;

        let info_html = $('.add-client-info').clone();
        info_html.find('.add-asset-info').remove();

        // Change the name and ID attribute for additional records.
        info_html.find('#client-name1')
            .attr('name', 'client_name' + client_count)
            .attr('id', 'client-name' + current_index);

        info_html.find('#main-phone1')
            .attr('name', 'main_phone' + client_count)
            .attr('id', 'main-phone' + current_index);

        info_html.find('#second-phone1')
            .attr('name', 'second_phone' + client_count)
            .attr('id', 'second-phone' + current_index);

        info_html.find('#mobile-phone1')
            .attr('name', 'mobile_phone' + client_count)
            .attr('id', 'mobile-phone' + current_index);

        info_html.find('#client-email1')
            .attr('name', 'client_email' + client_count)
            .attr('id', 'client-email' + current_index);

        info_html.find('#entity-name1')
            .attr('name', 'entity_name' + client_count)
            .attr('id', 'entity-name' + current_index);

        $('.add-client-modal')
            .append('<div class="additional-client-info col-xs-12 client-row client-info' + client_count + '"' +
                ' id="add-client-' + current_index + '">' +
                info_html.html() + '</div>');
        }

        // Set the values for fields.
        $('#client-name' + current_index).val(client['Client Name']);
        $('#main-phone' + current_index).val(client['Main Phone']);
        $('#second-phone' + current_index).val(client['Second Phone']);
        $('#mobile-phone' + current_index).val(client['Mobile Phone']);
        $('#client-email' + current_index).val(client['Client Email']);
        $('#entity-name' + current_index).val(client['Entity Name']);
        // }

        // Set the Assets Row.
        if (('' !== client['Asset Name']) || ('' !== client['Location']) || ('' !== client['Address'])) {

            // Clone the HTML for Assets.
            let dummy_asset = $('.dummy-asset-info').clone();
            dummy_asset = dummy_asset.removeClass('hidden dummy-asset-info').addClass('add-asset-info');

            dummy_asset.find('#asset-name').attr('name', 'asset_name' + client_count + '-' + asset_number)
                .attr('id', 'asset-name' + client_count + '-' + asset_number).attr('data-name', client.Name);

            dummy_asset.find('#asset-location').attr('name', 'asset_location' + client_count + '-' + asset_number)
                .attr('id', 'asset-location' + client_count + '-' + asset_number);

            dummy_asset.find('#address').attr('name', 'asset_address' + client_count + '-' + asset_number)
                .attr('id', 'address' + client_count + '-' + asset_number);

            dummy_asset.find('#asset-criteria').attr('name', 'asset_criteria' + client_count + '-' + asset_number)
                .attr('id', 'asset-criteria' + client_count + '-' + asset_number).attr('data-name', client.Name);

            dummy_asset.find('#asset-zip').attr('name', 'asset_zip' + client_count + '-' + asset_number)
                .attr('id', 'asset-zip' + client_count + '-' + asset_number);

            dummy_asset.find('#asset-status').attr('name', 'asset_status' + client_count + '-' + asset_number);

            // New added fields.
            dummy_asset.find('#asset-city').attr('name', 'asset_city' + client_count + '-' + asset_number)
                .attr('id', 'asset-city' + client_count + '-' + asset_number).attr('data-name', client.Name);

            dummy_asset.find('#asset-state').attr('name', 'asset_state' + client_count + '-' + asset_number)
                .attr('id', 'asset-state' + client_count + '-' + asset_number).attr('data-name', client.Name);

            dummy_asset.find('#asset-product')
                .attr('name', 'asset_product' + client_count + '-' + asset_number)
                .attr('id', 'asset-product' + client_count + '-' + asset_number).attr('data-name', client.Name);

            dummy_asset.find('#asset-units').attr('name', 'asset_unit' + client_count + '-' + asset_number)
                .attr('id', 'asset-units' + client_count + '-' + asset_number).attr('data-name', client.Name);

            dummy_asset.find('#asset-sq-ft').attr('name', 'asset_sq_ft' + client_count + '-' + asset_number)
                .attr('id', 'asset-sq-ft' + client_count + '-' + asset_number).attr('data-name', client.Name);

            dummy_asset.find('#asset-cond').attr('name', 'asset_cond' + client_count + '-' + asset_number)
                .attr('id', 'asset-cond' + client_count + '-' + asset_number).attr('data-name', client.Name);

            dummy_asset.find('#asset-criteria').attr('name', 'asset_criteria' + client_count + '-' + asset_number)
                .attr('id', 'asset-criteria' + client_count + '-' + asset_number).attr('data-name', client.Name);

            dummy_asset.find('#asset-lead').attr('name', 'asset_lead' + client_count + '-' + asset_number);

            dummy_asset.find('#asset-source').attr('name', 'asset_source' + client_count + '-' + asset_number);

            $('#add-client-' + current_index).append(dummy_asset);

            $('#asset-name' + client_count + '-' + parseInt(asset_number)).val(client['Asset Name']);
            $('#asset-zip' + client_count + '-' + parseInt(asset_number)).val(client['Asset ZIP']);
            $('#address' + client_count + '-' + parseInt(asset_number)).val(client['Asset Address']);
            $('#asset-city' + client_count + '-' + parseInt(asset_number)).val(client['Asset City']);
            $('#asset-state' + client_count + '-' + parseInt(asset_number)).val(client['Asset State']);
            $('#asset-units' + client_count + '-' + parseInt(asset_number)).val(client['Units']);
            $('#asset-sq-ft' + client_count + '-' + parseInt(asset_number)).val(client['Sq. Ft.']);

            $('#asset-product' + client_count + '-' + parseInt(asset_number) + ' option')
                .filter(function () {
                    return $(this).html() == client['Product Type'];
                }).attr('selected', true);

            // Check if the value for product type doesn't exist.
            if (($('#asset-product' + client_count + '-' + parseInt(asset_number)).val() === '0')
                && $.trim(client['Product Type']) !== '') {
                toastr.warning('Do you wish to add a new product type,' +
                    ' if not go to the product type list and select the one you want.');
                $('#asset-product' + client_count + '-' + parseInt(asset_number))
                    .append('<option selected>' + client['Product Type'] + '</option>');
            }

            $('#asset-cond' + client_count + '-' + parseInt(asset_number) + ' option')
                .filter(function () {
                    return $(this).html() == client['Condition'];
                }).attr('selected', true);

            // Check if the value for condition doesn't exist.
            if (($('#asset-cond' + client_count + '-' + parseInt(asset_number)).val() === '0')
                && $.trim(client['Condition']) !== '') {
                $('#asset-cond' + client_count + '-' + parseInt(asset_number))
                    .append('<option selected>' + client['Condition'] + '</option>');
            }

            $('#asset-criteria' + client_count + '-' + parseInt(asset_number) + ' option')
                .filter(function () {
                    return $(this).html() == client['Criteria'];
                }).attr('selected', true);

            // Check if criteria doesn't exist.
            if (($('#asset-criteria' + client_count + '-' + parseInt(asset_number)).val() === '')
                && $.trim(client['Criteria']) !== '') {
                $('#asset-criteria' + client_count + '-' + parseInt(asset_number))
                    .append('<option selected>' + client['Criteria'] + '</option>');
            }

            $('#asset-location' + client_count + '-' + parseInt(asset_number) + ' option')
                .filter(function () {
                    return $(this).html() == client['Market Area'];
                }).attr('selected', true);

            // Check if market area doesn't exist.
            if (($('#asset-location' + client_count + '-' + parseInt(asset_number)).val() === '')
                && $.trim(client['Market Area']) !== '') {
                $('#asset-location' + client_count + '-' + parseInt(asset_number))
                    .append('<option selected>' + client['Market Area'] + '</option>');
            }
        }
        // $('#')
    });

    // Check if the format used if correct.
    if (client_name_array[0] === undefined) {
        toastr.error('Please download the form from import popup.', 'Wrong Format for excel form or No client present!!')
    } else {
        const get_existing_client = makeAjaxCall('homebase/get_existing_clients', {'clients': client_name_array});
        get_existing_client.then(function (data) {
            // Check for each Existing client.
            $.each(data.existing_clients, function (index, client) {
                toastr.warning('These clients will be highlighted with Yellow.',
                    'Client with name ' + client.hc_name + ' already exist!!');

                // Set the color for existing client name to green.
                $('input[data-name="' + client.hc_name + '"]').parent('div').parent('div')
                    .parent('div').parent('div').find('.client-name').addClass('yellow_row');
            });
        });

        //Change the number of enteries at the top.
        $('#client-count').val(client_count);
    }

    add_cache();
};
