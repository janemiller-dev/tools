/**
 *
 * Import Excel File.
 *
 * @summary      Import excel file.
 * @description  This file contains functions for importing excel file and converting that to JSON.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */

$(document).ready(function () {
    import_buyer_excel.init();

    $('#buyer_file_input').change(function (event) {
        import_buyer_excel.checkfile(this, event);
    });
});

let import_buyer_excel = (() => {
    let init = () => {
            let view_import = $('#view_buyer_import').clone();
            view_import.removeClass('view_import');
            view_import.removeClass('hidden');
            $('#view_buyer_sample').attr('data-content', view_import.html().replace(/"/g, "\'"));

            $('#download_buyer_sample').click(function () {
                window.location.href =
                    'https://cpd.powersellingtools.com/wp-content/uploads/2020/04/Buyer-Layout-For-Import-Form.xlsx';
            });
        },
        // Check if the file type is valid one.
        checkfile = (sender, event) => {

            const validExts = [".xlsx", ".xls", ".csv"];
            let fileExt = sender.value;

            // Get the file extension
            fileExt = fileExt.substring(fileExt.lastIndexOf('.'));

            // Check if the extension is valid.
            if (validExts.indexOf(fileExt) < 0) {
                toastr.error("Invalid file selected, valid files are of " +
                    validExts.toString() + " types.");
                return false;
            }
            else {
                buyer_file_picked(event);
            }
        },

        // Fetch the fields from excel file and convert to an JSON object.
        buyer_file_picked = (oEvent) => {
            // Get The File From The Input
            const oFile = oEvent.target.files[0],
                sFilename = oFile.name,
                reader = new FileReader();

            reader.onload = function (e) {
                const data = e.target.result,
                    workbook = XLSX.read(data, {
                        type: 'binary'
                    });

                const sheetName = workbook.SheetNames[0];
                // Here is your object
                // const XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName], {range:1}),
                const XL_row_object = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName], {range:3}),
                    json_object = JSON.stringify(XL_row_object);

                toastr.success('File Imported');
                $('#import-buyer-excel-dialog').modal('hide');
                populate_buyer(XL_row_object);
            };
            reader.onerror = function (ex) {
                console.log(ex);
            };
            reader.readAsBinaryString(oFile);
        };

    return {
        checkfile: checkfile,
        init: init
    }

})();