// let info_name;

$(document).ready(function () {
    $('#add-asset-info-dialog').on('show.bs.modal', function (event) {
        $('#view-asset-info-dialog').modal('hide');
        $('#view-asset-cond-info-dialog').modal('hide');
        const info_type = $(event.relatedTarget).data('type');

        info_name = 'Condition';
        ('Cond' !== info_name) ?
            info_name = info_type.charAt(0).toUpperCase() + info_type.slice(1) : '';

        $(event.relatedTarget).removeData('type');
        $('#asset-info-type').val(info_type);
        $('#label-asset-info-name').text(info_name + ' Name');
        $('#para-asset-info').text('Enter the Name of the ' + info_name);
        $('#add-asset-info-title').text('Add ' + info_name);
    });

    // Form validation.
    $('#add-asset-info-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            asset_info_name: {
                validators: {
                    notEmpty: {
                        message: "This field is required."
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the add new asset-info form.
        var $form = $(e.target);
        var fv = $form.data('formValidation');

        // Get the form data and submit it.
        var asset_info_data = $form.serialize();

        $.ajax({
            url: jsglobals.base_url + 'homebase/add_asset_info',
            dataType: 'json',
            type: 'post',
            data: asset_info_data,
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            } else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success(info_name + ' Added!!', 'Success!!')
            fv.resetForm();
            $($form)[0].reset();

            $('#add-asset-info-dialog, #view-asset-info-dialog').modal('hide');
            refreshPage();
            get_all_criteria();
            $("#add-client-form").formcache("outputCache");
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });
});