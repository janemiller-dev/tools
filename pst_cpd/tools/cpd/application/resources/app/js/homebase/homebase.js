/**
 *
 * Populate Homeabase Table
 *
 * @summary      Fetch and Display Homebase Table data.
 * @description  This file contains functions for fetching and displaying homebase data.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */
let offset = 10, current_page = 1, portfolio;
$(document).ready(() => {
    refreshPage();
});

function refreshPage() {
    homebase.init();
}

/*
*Adds a new client information/Reference
*
*/
let assignee,
    homebase = (() => {

        /*
        * Initialize Homebase.
        *
        */
        let homebase_table = $('#homebase-table').find('tbody'),
            initial_client_data,
            universal_components,
            leads,
            assets_info,
            sources,
            init = () => {

                $(document).off('click', '#buyer_tab');
                $(document).on('click', '#buyer_tab', function () {
                    $('#add-homebase-client').addClass('hidden');
                    $('#add-homebase-buyer').removeClass('hidden');


                    const homebase_table_selector = $('#buyer-table'),
                        fetch_buyer = makeAjaxCall('homebase/fetch_buyer_data', {});

                    fetch_buyer.then(function (data) {
                        $('#buyer-table tbody').empty();

                        $.each(data.buyer_data, function (index, buyer_data) {
                            // Add body for buyer row.
                            homebase_table_selector.find('tbody:first')
                                .append('<tr style="background: #008080cf; color: #fff;">' +
                                    '<th class="text-center"><span style="float: left">R' + (index + 1) + '</span>' +
                                    ' Buyer Name</th><th class="text-center">Main Phone</th>' +
                                    '<th class="text-center">Second Phone</th>' +
                                    '<th class="text-center">Mobile Phone</th>' +
                                    '<th class="text-center">Email</th>' +
                                    '<th class="text-center">Entity Name</th>' +
                                    '<th class="text-center">Profile</th>' +
                                    '<th class="text-center">RWT</th>' +
                                    '<th class="text-center">Add Asset</th>' +
                                    '<th class="text-center"><span style="color: #fff" data-id="677" ' +
                                    'class="glyphicon glyphicon-plus show-asset" id="show-asset677"></span></th></tr>');

                            homebase_table_selector
                                .append('<tr class="buyer_homebase_row' + buyer_data.hb_id + '" style="background: #8ff1fd17">' +
                                    '<td><input class="form-control homebase_buyer_field"' +
                                    ' type="text" id="buyer_name' + buyer_data.hb_id + '"' +
                                    ' value="' + buyer_data.hb_name + '"' +
                                    ' data-id="' + buyer_data.hb_id + '" data-col="hb_name"></td>' +
                                    '<td><input class="form-control homebase_buyer_field" type="text"' +
                                    ' id="main-phone' + buyer_data.hb_id + '" value="' + buyer_data.hb_main_phone + '"' +
                                    ' data-id="' + buyer_data.hb_id + '" data-col="hb_main_phone"></td>' +
                                    '<td><input class="form-control homebase_buyer_field" type="text"' +
                                    ' id="second-phone' + buyer_data.hb_id + '"' +
                                    ' value="' + buyer_data.hb_second_phone + '" data-id="' + buyer_data.hb_id + '"' +
                                    ' data-col="hb_second_phone"></td>' +
                                    '<td>' +
                                    '<input class="form-control homebase_buyer_field" type="text"' +
                                    ' id="mobile-phone' + buyer_data.hb_id + '"' +
                                    ' value="' + buyer_data.hb_mobile_phone + '" data-id="' + buyer_data.hb_id + '"' +
                                    ' data-col="hb_mobile_phone"></td>' +
                                    '<td><input class="form-control homebase_buyer_field" type="text"' +
                                    ' id="email' + buyer_data.hb_id + '"' +
                                    ' value="' + buyer_data.hb_email + '" data-id="' + buyer_data.hb_id + '"' +
                                    ' data-col="hb_email"></td>' +
                                    '<td><input class="form-control homebase_buyer_field" type="text"' +
                                    ' id="entity_name' + buyer_data.hb_id + '"' +
                                    ' value="' + buyer_data.hb_entity_name + '" data-id="' + buyer_data.hb_id + '"' +
                                    ' data-col="hb_entity_name"></td>' +
                                    '<td class="text-center"><button class="btn btn-default btn-sm"' +
                                    ' data-toggle="modal" data-target="#view-owner-profile-dialog" data-id="677">' +
                                    '<span class="glyphicon glyphicon-info-sign"></span></button></td>' +
                                    '<td><select class="form-control homebase_buyer_field"' +
                                    ' name="rwt' + buyer_data.hb_id + '" data-id="' + buyer_data.hb_id + '"' +
                                    ' data-col="hb_rwt" id="hb-rwt' + buyer_data.hb_id + '">' +
                                    '<option value="R">R</option>' +
                                    '<option value="W">W</option>' +
                                    '<option value="T">T</option></select>' +
                                    '</td>' +
                                    '<td class="text-center">' +
                                    '<button class="btn btn-primary add-asset" data-id="677"> Add Asset</button>' +
                                    '</td>' +
                                    '<td><button class="btn btn-primary btn-sm delete-client" data-id="677"' +
                                    ' data-type="client" data-key="hc">' +
                                    '<span class="has-tooltip" title=""' +
                                    ' data-original-title="Long press to delete multiple clients."> ' +
                                    '<span class="glyphicon glyphicon-remove" style="color: #8ff1fd">' +
                                    '</span></span></button> ' +
                                    '<input type="checkbox" class="delete-client-check hidden"' +
                                    ' name="id0" value="677"></td></tr>');

                            append_buyer_head(buyer_data);
                            append_buyer_body(buyer_data);

                            (buyer_data.hb_rwt !== null) ? $('#hb-rwt' + buyer_data.hb_id).val(buyer_data.hb_rwt) : '';
                        });

                        // Append Deal Types and Criteria to deal type and criteria select box.
                        let request = [];
                        const get_deal = get_all_deal_type();
                        request.push(get_deal);

                        const get_criteria = get_all_criteria();
                        request.push(get_criteria);

                        const get_products = get_all_products(),
                            homebase_product_type_selector = $('.homebase-product-type'),
                            filter_product_selector = $('#filter-product'),
                            combo_product_selector = $('#filter-product, .homebase-product-type');

                        get_products.then(function (data) {
                            homebase_product_type_selector
                                .append('<option value="-1" disabled selected>-- Select Product Type --</option>');
                            filter_product_selector
                                .append('<option value="" selected>-- None --</option>');


                            $.each(data, function (index, product) {
                                combo_product_selector
                                    .append('<option style="font-weight: bold; font-style: italic;"' +
                                        ' value="' + product[0]['u_product_group_id'] + '">' + index + '</option>');

                                $.each(product, function (index, individual_product) {
                                    combo_product_selector
                                        .append('<option value="' + individual_product.u_product_id + '">' +
                                            individual_product.u_product_name + '</option>');
                                });
                            });

                            homebase_product_type_selector
                                .append('<option value="0" style="font-weight:bold;">None of the Above</option>');
                        });

                        request.push(get_products);

                        // Buyer asset field change event handler.
                        $(document).off('change', '.buyer-asset-field');
                        $(document).on('change', '.buyer-asset-field', function () {

                            const update_buyer_data = makeAjaxCall('homebase/update_buyer_asset_data', {
                                'val': $(this).val(),
                                'name': $(this).attr('data-name'),
                                'id': $(this).attr('data-id')
                            });

                            update_buyer_data.then(function (data) {
                                console.log(data);
                            })
                        });

                        // Buyer Info change handler.
                        $(document).off('change', '.homebase_buyer_field');
                        $(document).on('change', '.homebase_buyer_field', function () {
                            const update_buyer_data = makeAjaxCall('homebase/update_buyer_data', {
                                'val': $(this).val(),
                                'id': $(this).attr('data-id'),
                                'col': $(this).attr('data-col')
                            });

                            update_buyer_data.then(function (data) {
                                console.log(data);
                            })
                        });

                        Promise.all(request).then(function (promise_data) {
                            // Iterate through each client data.
                            $.each(data.buyer_data, function (index, client) {
                                $('#buyer-status' + client.hba_id).val(client.hba_status)
                                    .attr('data-prev', client.hba_status);
                                $('#buyer-criteria' + client.hba_id).val(client.hba_criteria);
                                $('#buyer-location' + client.hba_id).val(client.hba_market_area);
                                $('#buyer-product' + client.hba_id).val(client.hba_product_type);

                            });

                            // Set the criteria and status drop down.
                            // $('#filter-criteria').val(criteria);
                            // $('#filter-status').val(status);
                            // $('#filter-location').val(location);
                            // $('#filter-product').val(product_type);
                        });
                    });
                });

                $(document).on('click', '#assets_tab, #portfolio_tab', function () {
                    $('#add-homebase-client').removeClass('hidden');
                    $('#add-homebase-buyer').addClass('hidden');
                });

                const get_asset_info = makeAjaxCall('homebase/get_asset_info', {});
                get_asset_info.then(function (data) {
                    assets_info = data.asset_info;

                    $.each(data.asset_info, function (index, info) {
                        $('#filter-' + index).append(
                            $.map(info, function (individual_info, index) {
                                return '<option value="' + individual_info.hai_id + '">' +
                                    individual_info.hai_value + '</option>'
                            }).join('')
                        )
                    })
                });

                const get_location_info = makeAjaxCall('homebase/get_location_info', {});

                get_location_info.then(function (data) {
                    $.each(data.location_info, function (index, info) {
                        $('#filter-' + index).append(
                            $.map(info, function (individual_info, key) {
                                return '<option value="' + individual_info['hca_' + index] + '">' +
                                    individual_info['hca_' + index] + '</option>'
                            }).join('')
                        )
                    });
                });

                const get_tool_info = makeAjaxCall('homebase/get_homebase_tool', {});

                get_tool_info.then(function (data) {
                    $('#filter-tool').append('<option value="0" selected>-- None --</option>');
                    $.each(data.tool_info, function (index, tools) {
                        $('#filter-tool').append(
                            $.map(tools, function (individual_tools, key) {
                                return '<option value="' + individual_tools.tool_id + '" data-type="' + index + '">' +
                                    index + ' : ' + individual_tools.tool_name + '</option>';
                            }).join('')
                        )
                    })
                })

                const get_lead_source = makeAjaxCall('homebase/get_lead_source', {});

                get_lead_source.then(function (data) {
                    leads = data.lead_source.lead;
                    sources = data.lead_source.source;
                });

                const get_components = makeAjaxCall('code/get_components', {});

                get_components.then(function (data) {
                    populate_homebase(data);
                });

                // Multiple delete on long pressing delete button.
                let timer;
                $(document).off('mousedown', '.delete-client');
                $(document).on('mousedown', '.delete-client', function () {
                    timer = setTimeout(function () {
                        toastr.info("Select clients to delete!!");

                        $('.delete-client').addClass('hidden');
                        $('.delete-client-check').removeClass('hidden');
                        $('#delete-check, #remove-multi-delete').removeClass('hidden')

                    }, 1000);
                }).on("mouseup mouseleave", function () {
                    clearTimeout(timer);
                });

                $(document).off('mousedown', '.move-client');
                $(document).on('mousedown', '.move-client', function () {
                    timer = setTimeout(function () {
                        toastr.info("Select clients to Submit!!");

                        $('.move-client').addClass('hidden');
                        $('.submit-client-check').removeClass('hidden');
                        $('#submit-check, #remove-multi-submit, #multi-select-status, #multi-submit-label')
                            .removeClass('hidden')

                    }, 1000);
                }).on("mouseup mouseleave", function () {
                    clearTimeout(timer);
                });


                // Toggle back to single Submit.
                $(document).off('click', '#remove-multi-submit');
                $(document).on('click', '#remove-multi-submit', function () {
                    $(".submit-client-check").prop('checked', false);
                    $('.move-client').removeClass('hidden');
                    $('.submit-client-check').addClass('hidden');
                    $('#submit-check, #remove-multi-submit, #multi-submit-label, #multi-select-status').addClass('hidden')
                });

                // Toggle back to single delete.
                $(document).off('click', '#remove-multi-delete');
                $(document).on('click', '#remove-multi-delete', function () {
                    $(".delete-client-check").prop('checked', false);
                    $('.delete-client').removeClass('hidden');
                    $('.delete-client-check').addClass('hidden');
                    $('#delete-check, #remove-multi-delete').addClass('hidden');
                });

                $(document).off('change', '.asset-info');
                $(document).on('change', '.asset-info', function () {
                    if ($(this).val() === '-1') {
                        $('#view-asset-cond-info-dialog').modal('show', $(this));
                    }
                });

                // Check for the page offset.
                $(document).off('change', '#page-offet');
                $(document).on('change', '#page-offet', function () {
                    offset = $(this).val();
                    get_homebase_client();
                });

                // Add new portfolio functionality.
                $(document).off('change', '.client-portfolio');
                $(document).on('change', '.client-portfolio', function () {
                    if ('-1' === $(this).val()) {
                        $('#view-portfolio-dialog').modal('show');
                    }
                });

                // Add Portfolio click handler.
                $(document).off('click', '.add-portfolio');
                $(document).on('click', '.add-portfolio', function () {
                    const asset_id = $(this).attr('data-id'),
                        use_portfolio = makeAjaxCall('homebase/use_portfolio', {
                            portfolio_id: $('#portfolio-select' + asset_id).val(),
                            asset_id: asset_id
                        });

                    use_portfolio.then(function (data) {
                        toastr.success('Asset Portfolio Updated.', 'Success!!');
                    })
                });


                // Event listener for select boxes.
                $(document).on('change', '.hca-lead, .hca-source', function (e) {
                    // If the change is to "new agent", show add agent dialog.
                    if ($(this).val() === "0") {
                        // $(e).preventDefault();
                        $('#' + $(this).attr('data-target')).modal('show');
                        $('#lead-type').val($(this).data('type'));
                        $(this).val('-1');
                    }
                });


                $('#copyright_text').empty();
                $('#copyright_text').append('<hr/>' +
                    '<div class="col-xs-2></div>' +
                    '<div class="col-xs-8">' +
                    '<p class="text-center">Copyright &copy; <?php echo date("Y"); ?> RothMethods, Inc. All rights' +
                    ' reserved</p>' +
                    '</div>');

                //Remove tool click handler.
                $(document).off('click', '.remove-tool');
                $(document).on('click', '.remove-tool', function () {
                    $('#status' + $(this).attr('data-id')).prop("selectedIndex", 0).trigger('change');
                });

                // Upload Owner Image Click handler.
                $(document).off('click', '#upload-asset-img');
                $(document).on('click', '#upload-asset-img', function () {
                    const asset_id = $(this).attr('data-id');
                    $('#asset-img-upload').trigger('click');

                    const target = 'asset-img-upload',
                        file_type_allowed = /.\.(png|jpeg|jpg)/i,
                        data = {
                            'asset_id': asset_id
                        };

                    // Upload Image file.
                    const upload_image = uploadFile(target, data, file_type_allowed);
                    upload_image.then(function (data) {
                        refreshPage();
                    });
                });

                // Display image in new tab.
                $(document).off('click', '.asset-image');
                $(document).on('click', '.asset-image', function () {
                    const src = $(this).attr('src');

                    // Check if an asset image is present.
                    ('/tools/resources/app/media/user-avatar.svg' !== src) ? window.open($(this).attr('src')) : '';
                })
            },

            populate_homebase = (data) => {
                // Destroy the table so that can be rebuilt
                $('#homebase-table').find('tbody').empty();
                universal_components = data;
                get_homebase_client();

                // Remove any porevious click handler.
                $(document).off('click', '.show-asset');
                // Show Asset click handler.
                $(document).on('click', '.show-asset', function (e) {
                    $(this).toggleClass('glyphicon-plus glyphicon-minus');
                    $(this).removeClass('show-asset').addClass('hide-asset');

                    const get_more_homebase_client = makeAjaxCall('homebase/fetch_more_row', {
                        'query': null,
                        'lower_limit': 1,
                        'upper_limit': 100,
                        'id': $(this).attr('data-id')
                    });

                    get_more_homebase_client.then(function (data) {
                        const client_data = data.client;

                        $.each(client_data, function (index, client) {
                            append_additional_asset_head(client, index + 2);
                        });

                        $.each(client_data, function (index, client) {
                            append_asset_body(client, 'add-', index + 2);
                        });

                        // Merge Initial client data fetched for the TSL with new client data.
                        $.merge(client_data, initial_client_data);
                        // Append Deal Types and Criteria to deal type and criteria select box.
                        let request = [],
                            products = '';
                        const get_deal = get_all_deal_type();
                        request.push(get_deal);

                        const get_criteria = get_all_criteria();
                        request.push(get_criteria);

                        // const get_all_products = append_all_products();

                        const get_all_homebase_products = makeAjaxCall('industry/get_all_products');

                        get_all_homebase_products.then(function (data) {
                            products = data.products;
                        });

                        request.push(get_all_homebase_products);

                        Promise.all(request).then(function (data) {
                            // Iterate through each client data.
                            $.each(client_data, function (index, client) {
                                const homebase_product_type_selector = $('#product' + client.hca_id);

                                homebase_product_type_selector.empty();
                                homebase_product_type_selector
                                    .append('<option value="-1" disabled selected>-- Select Product Type --</option>');

                                $.each(products, function (index, product) {
                                    homebase_product_type_selector
                                        .append('<option style="font-weight: bold; font-style: italic;"' +
                                            ' value="' + product[0]['u_product_group_id'] + '">' + index + '</option>');

                                    $.each(product, function (index, individual_product) {
                                        homebase_product_type_selector
                                            .append('<option value="' + individual_product.u_product_id + '">' +
                                                individual_product.u_product_name + '</option>');
                                    });
                                });

                                homebase_product_type_selector
                                    .append('<option value="0" style="font-weight:bold;">None of the Above</option>');

                                $('#status' + client.hca_id).val(client.hca_status)
                                    .attr('data-prev', client.hca_status);
                                $('#criteria' + client.hca_id).val(client.hca_criteria_used);
                                $('#location' + client.hca_id).val(client.hca_location);
                                $('#product' + client.hca_id).val(client.hca_product_id);
                            });

                            $('.filter-homebase option:eq(0)').prop('selected', true);
                            // $('.show-asset').trigger('click');
                        });
                    });
                });

                // Hide Asset click handler.
                $(document).off('click', '.hide-asset');
                $(document).on('click', '.hide-asset', function (e) {
                    $(this).toggleClass('glyphicon-plus glyphicon-minus');
                    $(this).removeClass('hide-asset').addClass('show-asset');
                    $('#asset-table' + $(this).attr('data-id')).siblings('table').remove();
                });

                $(document).off('click', '#delete-check');
                $(document).on('click', '#delete-check', function () {
                    $('.delete-client').trigger('click');

                });

                $('.delete-client').off('click');
                $('.delete-asset').off('click');
                $(document).off('click', '.delete-asset, .delete-client');
                $(document).on('click', '.delete-asset, .delete-client', function (e) {
                    const that = $(this);

                    toastr.error("<br /><button type='button' id='confirmYes' class='btn btn-default'>Yes</button>" +
                        "<button type='button' id='confirmNo' class='btn btn-default' style='margin-left: 5px'>No</button>",
                        'Are you sure you want to delete?',
                        {
                            timeOut: 0,
                            extendedTimeOut: 0,
                            preventDuplicates: true,
                            tapToDismiss: false,
                            closeButton: true,
                            allowHtml: true,
                            onShown: function (toast) {
                                $("#confirmYes").off('click');
                                $("#confirmYes").click(function () {

                                    const selected_clients_data = $('.delete-client-check:checked').serializeArray();
                                    let client_id = $(that).attr('data-id'),
                                        client_type = $(that).attr('data-type');

                                    if ((0 !== selected_clients_data.length) && ('client' === client_type)) {
                                        client_id = selected_clients_data;
                                    }

                                    const delete_asset_client = makeAjaxCall('homebase/delete_asset_client',
                                        {
                                            'id': client_id,
                                            'type': client_type,
                                            'key': $(that).attr('data-key')
                                        });

                                    delete_asset_client.then(function (data) {
                                        get_homebase_client();
                                    });
                                    // Close the Toastr.
                                    $('.toast-close-button').trigger('click');
                                });

                                $('#confirmNo').click(function () {
                                    $('.toast-close-button').trigger('click');
                                })
                            }
                        });
                });

                // Search for homebase on keyup event.
                // $(document).on('keyup', '#search-homebase', function () {
                //     get_homebase_client($(this).val());
                // });

                $(document).off('click', '#search-homebase-btn');
                $(document).on('click', '#search-homebase-btn', function () {
                    current_page = 1;
                    get_homebase_client($('#search-homebase-text').val())
                });

                // Check if criteria is changed.
                $(document).off('change', '.filter-homebase');
                $(document).on('change', '.filter-homebase', function () {
                    current_page = 1;
                    get_homebase_client(null, null, $(this).val(), $(this).data('key'));
                });

                //Update Asset fields on change event.
                $(document).off('change', '.asset-field');
                $(document).on('change', '.asset-field', function () {

                    // Check if asset already belongs to a DMD/TSL instance.
                    const asset_id = $(this).attr('data-id'),
                        that = $(this);

                    // Check if asset status is changed.
                    if ($(that).hasClass('client-status')) {
                        const is_asset_used = makeAjaxCall('homebase/is_asset_used', {
                            'id': asset_id
                        });

                        // check if this asset already belong to any DMD/TSL instance.
                        is_asset_used.then(function (data) {
                            if (0 !== data.asset_used.length) {
                                // Remove any previous instance of asset use.
                                toastr.error("<br /><button type='button' id='confirmationYes' class='btn btn-default'>Yes</button>" +
                                    "<button type='button' id='confirmationNo' class='btn btn-default' style='margin-left: 5px'>No</button>",
                                    'This Asset is already in use. <br/> Clicking Yes will remove any previous instance of asset used.',
                                    {
                                        timeOut: 0,
                                        extendedTimeOut: 0,
                                        preventDuplicates: true,
                                        tapToDismiss: false,
                                        closeButton: true,
                                        allowHtml: true,
                                        onShown: function (toast) {
                                            $("#confirmationYes").off('click');
                                            $("#confirmationYes").click(function () {

                                                $.each(data.asset_used, function (index, asset) {
                                                    const delete_prev_instance = makeAjaxCall('homebase/delete_prev_instance', {
                                                        'table_name': asset.table_name,
                                                        'deal_id': asset.deal_id,
                                                        'tc_id': asset.tc_id
                                                    });

                                                    delete_prev_instance.then(function (data) {
                                                        toastr.success('Asset Instance removed.', 'Success!!');
                                                        refreshPage();
                                                    });
                                                });

                                                update_asset_field(that);
                                                // Close the Toastr.
                                                $('.toast-close-button').trigger('click');
                                            });

                                            $('#confirmationNo').click(function () {
                                                $('.toast-close-button').trigger('click');
                                                refreshPage();
                                            })
                                        }
                                    });

                            }
                            else {
                                update_asset_field(that);
                            }
                        });

                    } else {
                        ('-1' !== $(that).val()) ? update_asset_field(that) : '';
                    }
                });

                $(document).off('click', '#next-page');
                $(document).on('click', '#next-page', function () {
                    current_page = current_page + 1;
                    get_homebase_client();
                });

                $(document).off('click', '#prev-page');
                $(document).on('click', '#prev-page', function () {
                    current_page = current_page - 1;
                    get_homebase_client();
                });

                // Update homebase field values on change.
                $(document).off('change', '.homebase_field');
                $(document).on('change', '.homebase_field', function () {
                    const id = $(this).attr('data-id'),
                        col = $(this).attr('data-col'),
                        val = $(this).val(),

                        update_homebase_field = makeAjaxCall('homebase/update_homebase_field', {
                            'id': id,
                            'col': col,
                            'val': val
                        });
                });
            },

            // Updates Asset Field.
            update_asset_field = (that) => {

                let lat = '',
                    long = '';

                if ('hca_address' === $(that).attr('data-name')) {
                    const address = getLongLat($(that).val());
                    address.then(function (data) {
                        lat = data.results[0].geometry.location.lat;
                        long = data.results[0].geometry.location.lng;

                        update_asset_data($(that), lat, long);
                    })
                } else {
                    update_asset_data($(that));
                }
            },

            update_asset_data = (that, lat = '', long = '') => {
                const id = $(that).attr('data-id'),
                    name = $(that).attr('data-name'),
                    val = $(that).val(),
                    update_asset_field = makeAjaxCall('homebase/update_asset_field', {
                        'id': id,
                        'col_name': name,
                        'val': val,
                        'long': long,
                        'lat': lat
                    });

                update_asset_field.then(function () {
                    if ($(that).hasClass('client-status')) {
                        get_homebase_client(null, 'update_initial_data');
                        $('#move-client' + id).attr('data-status', val);
                    }
                })
            },

            append_all_products = () => {
                const get_all_products = makeAjaxCall('industry/get_all_products'),
                    homebase_product_type_selector = $('.homebase-product-type');

                get_all_products.then(function (data) {
                    homebase_product_type_selector
                        .append('<option value="-1" disabled selected>-- Select Product Type --</option>');

                    $.each(data.products, function (index, product) {
                        homebase_product_type_selector
                            .append('<option style="font-weight: bold; font-style: italic;"' +
                                ' value="' + product[0]['u_product_group_id'] + '">' + index + '</option>');

                        $.each(product, function (index, individual_product) {
                            homebase_product_type_selector
                                .append('<option value="' + individual_product.u_product_id + '">' +
                                    individual_product.u_product_name + '</option>');
                        });
                    });

                    homebase_product_type_selector
                        .append('<option value="0" style="font-weight:bold;">None of the Above</option>');

                });
            },

            // Fetch clients that belong to this homebase.
            get_homebase_client =
                (query = null, update_initial_data = null, filter_key = null, filter_col = null) => {
                    const get_homebase_client_data = makeAjaxCall('homebase/get_homebase_client', {
                        'query': query,
                        'criteria': $('#filter-criteria').val(),
                        'status': $('#filter-status').val(),
                        'location': $('#filter-location').val(),
                        'sort': $('#filter-sort').val(),
                        'order': $('#filter-sort option:selected').attr('data-order'),
                        'product_type': $('#filter-product').val(),
                        'filter_col': filter_col,
                        'filter_key': filter_key,
                        'rwt': $('#filter-rwt').val(),
                        'lower_limit': 0,
                        'upper_limit': 1,
                        'current_page': current_page,
                        'offset': offset,
                        'city': $('#filter-city').val(),
                        'state': $('#filter-state').val(),
                        'zip': $('#filter-zip').val(),
                        'cond': $('#filter-cond').val(),
                        'unit': $('#filter-unit').val(),
                        'ft': $('#filter-ft').val(),
                        'tool_id': $('.client-tool').val(),
                        'tool_type': $('.client-tool option:selected').attr('data-type')
                    });

                    let dummy_thead = $('#dummy_thead').clone(),
                        client_data,
                        status,
                        criteria,
                        location,
                        product_type,
                        c_page,
                        l_page,
                        count = 0;
                    dummy_thead.removeClass('hidden, dummy_thead');

                    get_homebase_client_data.then(function (data) {
                        client_data = data.client.client;
                        assignee = data.client.assignee;
                        initial_client_data = client_data;
                        c_page = data.client.current_page;
                        l_page = data.client.last_page;
                        criteria = data.homebase_criteria;
                        status = data.homebase_status;
                        location = data.homebase_location;
                        product_type = data.product_type;
                        portfolio = data.portfolio;

                        // Disable Prev Button.
                        (1 === c_page) ?
                            $('#prev-page').addClass('no-click') : $('#prev-page').removeClass('no-click');

                        // Disable Next button
                        (l_page === c_page) ?
                            $('#next-page').addClass('no-click') : $('#next-page').removeClass('no-click');

                        if (null === update_initial_data) {
                            $('#homebase-table tbody').empty();

                            // Iterate through each client data.
                            $.each(client_data, function (index, client) {

                                // Check whether the row for this user already exist.
                                if ($('.homebase_row' + client.hc_id).length === 0) {
                                    count = count + 1;
                                    const homebase_table_selector = $('#homebase-table');

                                    // Add Header for each Client Row.
                                    homebase_table_selector.find('tbody:first')
                                        .append($('<tr style="background: #008080cf; color: #fff;">' +
                                            '<th class="text-center"><span style="float: left">R' + count +
                                            '</span> Owner Name</th>' +
                                            '<th class="text-center">Main Phone</th>' +
                                            '<th class="text-center">Second Phone</th>' +
                                            '<th class="text-center">Mobile Phone</th>' +
                                            '<th class="text-center">Email</th>' +
                                            '<th class="text-center">Entity Name</th>' +
                                            '<th class="text-center">Profile</th>' +
                                            '<th class="text-center">RWT</th>' +
                                            '<th class="text-center">Add Asset</th>' +
                                            '<th class="text-center"><span style="color: #fff" ' +
                                            'data-id="' + client.hc_id + '" class="glyphicon glyphicon-plus show-asset" ' +
                                            'id="show-asset' + client.hc_id + '"></span></th>' +
                                            '</tr>'));

                                    // Add body for each client row.
                                    homebase_table_selector.find('tbody:first')
                                        .append($('<tr class="homebase_row' + client.hc_id + '" style="background: #8ff1fd17">')
                                            // Append Homebase Owner Name.
                                                .append($('<td><input class="form-control homebase_field" type="text" ' +
                                                    'id="name' + client.hc_id + '" value="' + client.hc_name + '" ' +
                                                    'data-id="' + client.hc_id + '" data-col="hc_name" ></td>'))
                                                // Append Homebase Main Phone.
                                                .append($('<td><input class="form-control homebase_field" type="text" ' +
                                                    'id="main-phone' + client.hc_id + '" value="' + client.hc_main_phone + '" ' +
                                                    'data-id="' + client.hc_id + '" data-col="hc_main_phone"></td>'))

                                                // Append Homebase Second Phone.
                                                .append($('<td><input class="form-control homebase_field" ' +
                                                    'type="text" id="second-phone'
                                                    + client.hc_id + '" value="' + client.hc_second_phone + '" ' +
                                                    'data-id="' + client.hc_id + '" data-col="hc_second_phone"></td>'))

                                                // Append Homebase Mobile Phone.
                                                .append($('<td><input class="form-control homebase_field" ' +
                                                    'type="text" id="mobile-phone'
                                                    + client.hc_id + '" value="' + client.hc_mobile_phone + '" ' +
                                                    'data-id="' + client.hc_id + '" data-col="hc_mobile_phone"></td>'))

                                                // Append Email Phone.
                                                .append($('<td><input class="form-control homebase_field" type="text" ' +
                                                    'id="email' + client.hc_id + '" value="' + client.hc_client_email + '" ' +
                                                    'data-id="' + client.hc_id + '" data-col="hc_client_email"></td>'))

                                                // Append Website.
                                                .append($('<td><input class="form-control homebase_field" type="text" ' +
                                                    'id="entity_name' + client.hc_id + '"' +
                                                    ' value="' + client.hc_entity_name + '" ' +
                                                    'data-id="' + client.hc_id + '" data-col="hc_entity_name"></td>'))

                                                // Profile button
                                                .append($('<td class="text-center">')
                                                    .append('<button class=\'btn btn-default btn-sm\'\n' +
                                                        ' data-toggle=\'modal\'' +
                                                        ' data-target=\'#view-owner-profile-dialog\'' +
                                                        ' data-id=' + client.hc_id + '>' +
                                                        '<span class=\'glyphicon glyphicon-info-sign\'></span>' +
                                                        '</button></td>'
                                                    ))

                                                // Asset RWT
                                                .append('<td><select class="form-control asset-clu asset-field" name="clu' + client.hca_id +
                                                    '" data-id="' + client.hca_id + '" data-name="hca_rwt" id="rwt' + client.hca_id + '">' +
                                                    '<option value="R">R</option>' +
                                                    '<option value="W">W</option>' +
                                                    '<option value="T">T</option>' +
                                                    '</select></td>')

                                                //Add Asset button.
                                                .append($('<td class="text-center"><button class="btn btn-primary add-asset" ' +
                                                    'data-id="' + client.hc_id + '"> Add Asset</button></td>'))

                                                // Submit Button
                                                .append($('<td><button class="btn btn-primary btn-sm delete-client"' +
                                                    ' data-id="' + client.hc_id + '" data-type="client" data-key="hc">' +
                                                    '<span class="has-tooltip" title="Long press to delete multiple clients.">' +
                                                    ' <span class="glyphicon glyphicon-remove" style="color: #8ff1fd">' +
                                                    '</span></span></button>' +
                                                    ' <input type="checkbox" class="delete-client-check hidden"' +
                                                    ' name="id' + index + '" value="' + client.hc_id + '"></td>'))
                                                .append($('</tr>'))
                                        );
                                    (null !== client.hca_id) ? append_asset_head(client) : '';
                                }

                                (null !== client.hca_id) ? append_asset_body(client) : '';
                                // (null === filter_col) ? $('.show-asset').trigger('click') : '';
                            });

                            // Empty portfolio table.
                            $('#portfolio-table tbody').empty()
                            // Iterate through each client data.
                            $.each(data.portfolio_client, function (index, client) {

                                // Check whether the row for this user already exist.
                                if ($('.portfolio_row' + client.hc_id).length === 0) {
                                    count = count + 1;
                                    const homebase_table_selector = $('#portfolio-table');

                                    // Add Header for each Client Row.
                                    homebase_table_selector.find('tbody:first')
                                        .append($('<tr style="background: #008080cf; color: #fff;">' +
                                            '<th class="text-center"><span style="float: left">R' + count +
                                            '</span> Owner Name</th>' +
                                            '<th class="text-center">Main Phone</th>' +
                                            '<th class="text-center">Second Phone</th>' +
                                            '<th class="text-center">Mobile Phone</th>' +
                                            '<th class="text-center">Email</th>' +
                                            '<th class="text-center">Entity Name</th>' +
                                            '<th class="text-center">Profile</th>' +
                                            '<th class="text-center">Add Asset</th>' +
                                            '<th class="text-center"><span style="color: #fff" ' +
                                            'data-id="' + client.hc_id + '" class="glyphicon glyphicon-plus show-asset" ' +
                                            'id="show-asset' + client.hc_id + '"></span></th>' +
                                            '</tr>'));

                                    // Add body for each client row.
                                    homebase_table_selector.find('tbody:first')
                                        .append($('<tr class="portfolio_row' + client.hc_id + '" style="background: #8ff1fd17">')
                                            // Append Homebase Owner Name.
                                                .append($('<td><input class="form-control homebase_field" type="text" ' +
                                                    'id="name' + client.hc_id + '" value="' + client.hc_name + '" ' +
                                                    'data-id="' + client.hc_id + '" data-col="hc_name" ></td>'))
                                                // Append Homebase Main Phone.
                                                .append($('<td><input class="form-control homebase_field" type="text" ' +
                                                    'id="main-phone' + client.hc_id + '" value="' + client.hc_main_phone + '" ' +
                                                    'data-id="' + client.hc_id + '" data-col="hc_main_phone"></td>'))

                                                // Append Homebase Second Phone.
                                                .append($('<td><input class="form-control homebase_field" ' +
                                                    'type="text" id="second-phone'
                                                    + client.hc_id + '" value="' + client.hc_second_phone + '" ' +
                                                    'data-id="' + client.hc_id + '" data-col="hc_second_phone"></td>'))

                                                // Append Homebase Mobile Phone.
                                                .append($('<td><input class="form-control homebase_field" ' +
                                                    'type="text" id="mobile-phone'
                                                    + client.hc_id + '" value="' + client.hc_mobile_phone + '" ' +
                                                    'data-id="' + client.hc_id + '" data-col="hc_mobile_phone"></td>'))

                                                // Append Email.
                                                .append($('<td><input class="form-control homebase_field" type="text" ' +
                                                    'id="email' + client.hc_id + '" value="' + client.hc_client_email + '" ' +
                                                    'data-id="' + client.hc_id + '" data-col="hc_client_email"></td>'))

                                                // Append Entity Name.
                                                .append($('<td><input class="form-control homebase_field" type="text" ' +
                                                    'id="entity_name' + client.hc_id + '"' +
                                                    ' value="' + client.hc_entity_name + '" ' +
                                                    'data-id="' + client.hc_id + '" data-col="hc_entity_name"></td>'))

                                                // Profile button
                                                .append($('<td class="text-center">')
                                                    .append('<button class=\'btn btn-default btn-sm\'\n' +
                                                        ' data-toggle=\'modal\'' +
                                                        ' data-target=\'#view-owner-profile-dialog\'' +
                                                        ' data-id=' + client.hc_id + '>' +
                                                        '<span class=\'glyphicon glyphicon-info-sign\'></span>' +
                                                        '</button></td>'
                                                    ))

                                                // Add Asset Button.
                                                .append($('<td class="text-center"><button class="btn btn-primary add-asset" ' +
                                                    'data-id="' + client.hc_id + '"> Add Asset</button></td>'))

                                                // Submit Button
                                                .append($('<td><button class="btn btn-primary delete-client"' +
                                                    ' data-id="' + client.hc_id + '" data-type="client" data-key="hc">' +
                                                    '<span class="has-tooltip" title="Long press to delete multiple clients.">' +
                                                    ' <span class="glyphicon glyphicon-remove" style="color: #8ff1fd">' +
                                                    '</span></span></button>' +
                                                    ' <input type="checkbox" class="delete-client-check hidden"' +
                                                    ' name="id' + index + '" value="' + client.hc_id + '"></td>'))
                                                .append($('</tr>'))
                                        );
                                    (null !== client.hca_id) ? append_asset_head(client, homebase_table_selector, 'port-') : '';
                                }

                                (null !== client.hca_id) ? append_asset_body(client, 'port-') : '';
                                // (null === filter_col) ? $('.show-asset').trigger('click') : '';
                            });

                            // Set Income Popover content.
                            $(document).off('click', '.amount_button');
                            $(document).on('click', '.amount_button', function () {
                                $(this).popover({content: $(this).attr('data-dummy-content')});
                                $(this).popover('show');

                                const deal_id = $(this).attr('data-id'),
                                    gnet_selector = $('#gnet' + deal_id);

                                // Calculate and set Gross Net and Net income.
                                gnet_selector.val(numeral((numeral($('#gross' + deal_id).val())._value)
                                    * (numeral($('#gsplit' + deal_id).val())._value)).format('$0,0.00'));

                                $('#net' + deal_id).val('$' + (numeral(gnet_selector.val())._value
                                    * parseInt($('#split' + deal_id).val()) / 100).toLocaleString());

                                updateAmount();
                            });

                            $(document).off('click', '.add-asset');
                            $(document).on('click', '.add-asset', function () {
                                const add_asset = makeAjaxCall('homebase/add_new_asset', {'id': $(this).attr('data-id')});

                                add_asset.then(function (data) {
                                    toastr.success('Asset Added.');
                                    get_homebase_client();
                                })
                            });

                            // Append Deal Types and Criteria to deal type and criteria select box.
                            let request = [];
                            const get_deal = get_all_deal_type();
                            request.push(get_deal);

                            const get_criteria = get_all_criteria();
                            request.push(get_criteria);

                            const get_products = get_all_products(),
                                homebase_product_type_selector = $('.homebase-product-type'),
                                filter_product_selector = $('#filter-product'),
                                combo_product_selector = $('#filter-product, .homebase-product-type');

                            get_products.then(function (data) {
                                homebase_product_type_selector
                                    .append('<option value="-1" disabled selected>-- Select Product Type --</option>');
                                filter_product_selector
                                    .append('<option value="" selected>-- None --</option>');


                                $.each(data, function (index, product) {
                                    combo_product_selector
                                        .append('<option style="font-weight: bold; font-style: italic;"' +
                                            ' value="' + product[0]['u_product_group_id'] + '">' + index + '</option>');

                                    $.each(product, function (index, individual_product) {
                                        combo_product_selector
                                            .append('<option value="' + individual_product.u_product_id + '">' +
                                                individual_product.u_product_name + '</option>');
                                    });
                                });

                                homebase_product_type_selector
                                    .append('<option value="0" style="font-weight:bold;">None of the Above</option>');
                            });

                            request.push(get_products);

                            Promise.all(request).then(function (data) {
                                // Iterate through each client data.
                                $.each(client_data, function (index, client) {
                                    $('#status' + client.hca_id).val(client.hca_status)
                                        .attr('data-prev', client.hca_status);
                                    $('#criteria' + client.hca_id).val(client.hca_criteria_used);
                                    $('#location' + client.hca_id).val(client.hca_location);
                                    $('#product' + client.hca_id).val(client.hca_product_id);
                                });

                                // Set the criteria and status drop down.
                                $('#filter-criteria').val(criteria);
                                $('#filter-status').val(status);
                                $('#filter-location').val(location);
                                $('#filter-product').val(product_type);
                            });
                        }

                        // Set active DMD, if theres one.
                        if ((null === data.active_cpd) || (0 === data.active_cpd.length)) {
                            $('#linked-cpd').html('No DMD');
                        } else {
                            const cpd_name = data.active_cpd[0].cpd_name;
                            $('#linked-cpd').html
                            (cpd_name.link(jsglobals.base_url + 'cpdv1/cpdv1/' + data.active_cpd[0].uact_cpd_id));
                        }

                        $('#page-count').html('Page ' + c_page + ' Of ' + l_page);

                        // Append Portfolio to portfolio table.
                        $.each(portfolio, function (index, ind_portfolio) {
                            $('#portfolio-dialog-table').append('<tr>' +
                                '<td class="text-center">' + (index + 1) + '</td>' +
                                '<td class="text-center">' + ind_portfolio.up_name + '</td>' +
                                '<td class="text-center">' +
                                '<button class="btn btn-sm btn-default" data-toggle="modal"' +
                                ' data-target="#delete-portfolio-dialog"' +
                                ' data-id="' + ind_portfolio.up_id + '" data-name="' + ind_portfolio.up_name + '">' +
                                '<i class="fa fa-trash"></i></button></td>' +
                                '</tr>')
                        });
                    });
                },

            append_additional_asset_head = (client, count) => {
                let asset_table_selector = $('#asset-table' + client.hc_id);

                (count > 2) ? asset_table_selector = $('.add-asset-tr' + client.hc_id).last() : '';
                let image_url = jsglobals.base_url + 'resources/app/media/user-avatar.svg';

                // Check if image is present
                (null !== client.hca_avatar) ?
                    image_url = window.location.origin + '/tools/homebase/get_owner_image/?img=' + client.hca_avatar : '';

                asset_table_selector
                    .after($(
                        // '<tr class="add-asset-tr' + client.hc_id + '"><td colspan="10">' +
                        '<table id="add-asset-table' + client.hc_id + '" class="table table-condensed add-asset-tr' + client.hc_id + '" ' +
                        'style="border: 5px solid #008080cf;">' +
                        '<tr class="asset_head" id="add-asset-row' + client.hc_id + '-' + count + '" style="background: #ffffe09e;">' +
                        '<th rowspan="4" colspan="2" class="upload-asset-image"><img src=' + image_url +
                        ' id="asset-avatar" class="asset-image">' +
                        '<div class="upload">' +
                        '<input id="asset-img-upload" type="file" name="files[]"' +
                        ' data-url=' + jsglobals.base_url + 'homebase/upload_asset_image' +
                        ' style="visibility: hidden; float:left; width:0;">' +
                        '<a id="upload-asset-img" data-id="' + client.hca_id + '">Click to Change Image</a></div></th>' +
                        '<th class="text-center"><span style="float: left"><b>A' + count + '</b></span>Last Updated</th>' +
                        '<th class="text-center" colspan="2">Asset Name</th>' +
                        '<th class="text-center" colspan="2">Asset Address</th>' +
                        '<th class="text-center">City</th>' +
                        '<th class="text-center">State</th>' +
                        '<th class="text-center">Zip</th>' +
                        '<th class="text-center">Info</th>' +
                        '<th class="text-center">Notes</th>' +
                        '<th class="text-center">Action</th>' +
                        '<th class="text-center">ACM</th>' +
                        '<th class="text-center">Submit</th>' +
                        '<th class="text-center">Remove</th>' +
                        '</tr>' +
                        '<tr class="asset_head" id="add-asset-row' + client.hc_id + '-' + count + '-1"' +
                        ' style="background: #ffffe09e;">' +
                        '<th class="text-center">Product Type</th>' +
                        '<th class="text-center">Market Area</th>' +
                        '<th class="text-center">Units</th>' +
                        '<th class="text-center">Sq. Ft.</th>' +
                        '<th class="text-center">Condition</th>' +
                        '<th class="text-center">Criteria</th>' +
                        '<th class="text-center">Status</th>' +
                        '<th class="text-center" colspan="3">Tool</th>' +
                        '<th class="text-center" colspan="4">Portfolio</th>' +
                        '</tr>'));
            },

            // Append a new Asset Table Header.
            append_asset_head = (client, table = $('#homebase-table'), text_to_append = '') => {
                let image_url = jsglobals.base_url + 'resources/app/media/user-avatar.svg';

                // Check if image is present
                (null !== client.hca_avatar) ?
                    image_url = window.location.origin + '/tools/homebase/get_owner_image/?img=' + client.hca_avatar : '';

                table
                    .find('tbody:first')
                    .append($(
                        '<tr id="asset-tr' + client.hc_id + '"><td colspan="10">' +
                        '<table id="asset-table' + client.hc_id + '" class="table table-condensed" ' +
                        'style="border: 5px solid #008080cf;">' +
                        '<tr class="asset_head" id="' + text_to_append + 'asset-row' + client.hc_id + '-1" style="background: #ffffe09e;">' +
                        '<th rowspan="4" colspan="2" class="upload-asset-image"><img src=' + image_url +
                        ' id="asset-avatar" class="asset-image">' +
                        '<div class="upload">' +
                        '<input id="asset-img-upload" type="file" name="files[]"' +
                        ' data-url=' + jsglobals.base_url + 'homebase/upload_asset_image' +
                        ' style="visibility: hidden; float:left; width:0;">' +
                        '<a id="upload-asset-img" data-id="' + client.hca_id + '">Click to Change Image</a></div></th>' +
                        '<th class="text-center"><span style="float: left"><b>A1</b></span>Last Updated</th>' +
                        '<th class="text-center" colspan="2">Asset Name</th>' +
                        '<th class="text-center" colspan="2">Asset Address</th>' +
                        '<th class="text-center">City</th>' +
                        '<th class="text-center">State</th>' +
                        '<th class="text-center">Zip</th>' +
                        '<th class="text-center">Info</th>' +
                        '<th class="text-center">Notes</th>' +
                        '<th class="text-center">Action</th>' +
                        '<th class="text-center">ACM</th>' +
                        '<th class="text-center">Submit</th>' +
                        '<th class="text-center">Remove</th>' +
                        '</tr>' +
                        '<tr class="asset_head" id="' + text_to_append + 'asset-row' + client.hc_id + '-2"' +
                        ' style="background: #ffffe09e;">' +
                        '<th class="text-center">Product Type</th>' +
                        '<th class="text-center">Market Area</th>' +
                        '<th class="text-center">Units</th>' +
                        '<th class="text-center">Sq. Ft.</th>' +
                        '<th class="text-center">Condition</th>' +
                        '<th class="text-center">Criteria</th>' +
                        '<th class="text-center">Status</th>' +
                        '<th class="text-center" colspan="3">Tool</th>' +
                        '<th class="text-center" colspan="4">Portfolio</th>' +
                        '</tr>'));

            },

            // Append a new Asset Table Body.
            append_asset_body = (client, add = '', count = 1) => {
                let tsl_reference = '#',
                    tsl_name = 'None';

                // Check if this homebase belongs to any TSL instance.
                if (null !== client.tsl_name) {
                    tsl_reference = '/tools/tsl/tsl/' + client.tsl_id;
                    tsl_name = 'TSL: ' + client.tsl_name;
                } else if (null !== client.cpd_name) {
                    tsl_reference = '/tools/cpdv1/cpdv1/' + client.cpd_id;
                    tsl_name = 'DMD: ' + client.cpd_name;
                }

                // Clone dummy rows.
                let dummy_content = $('.dummy_content').clone();
                dummy_content.removeClass('dummy_content');
                dummy_content.removeClass('hidden');

                //Fetch the HTML content for Popover and replace with values.
                dummy_content.find('.price').attr('id', 'price' + client.hca_id);
                dummy_content.find('.price').attr('data-id', client.hca_id);
                dummy_content.find('.price').attr('value', '$' + parseInt(client.hca_price).toLocaleString());

                dummy_content.find('.rate').attr('value', parseInt(client.hca_rate) + '%');
                dummy_content.find('.rate').attr('data-id', client.hca_id);
                dummy_content.find('.rate').attr('id', 'rate' + client.hca_id);

                dummy_content.find('.gross').attr('value', '$' +
                    parseInt(client.hca_price * client.hca_rate / 100).toLocaleString());

                dummy_content.find('.gross').attr('data-id', client.hca_id);
                dummy_content.find('.gross').attr('id', 'gross' + client.hca_id);

                dummy_content.find('.csplit').attr('value', parseInt(client.hca_split) + '%');
                dummy_content.find('.csplit').attr('data-id', client.hca_id);
                dummy_content.find('.csplit').attr('id', 'split' + client.hca_id);

                dummy_content.find('.net').attr('value', '$' + parseInt(client.hca_net));
                dummy_content.find('.net').attr('data-id', client.hca_id);
                dummy_content.find('.net').attr('id', 'net' + client.hca_id);

                dummy_content.find('.gsplit').attr('value', parseInt(client.hca_gsplit) + '%');
                dummy_content.find('.gsplit').attr('data-id', client.hca_id);
                dummy_content.find('.gsplit').attr('id', 'gsplit' + client.hca_id);

                dummy_content.find('.gnet').attr('data-id', client.hca_id);
                dummy_content.find('.gnet').attr('id', 'gnet' + client.hca_id);

                $('#' + add + 'asset-row' + client.hc_id + '-' + count)
                    .after($('<tr>')
                        // Last Updated
                            .append('<td><input class="form-control" disabled type="text" ' +
                                'data-id="' + client.hca_id + '" data-name="hca_updated_at"' +
                                'name="updated' + client.hca_id + '" style="background: #fff" value="' +
                                (null !== client.hca_updated_at ? client.hca_updated_at : '') + '"></td>')

                            // Asset Name
                            .append('<td colspan="2"><input class="form-control asset-field" type="text" ' +
                                'data-id="' + client.hca_id + '" data-name="hca_name"' +
                                'name="asset' + client.hca_id + '" value="' +
                                (null !== client.hca_name ? client.hca_name : '') + '"></td>')

                            // Asset Address.
                            .append('<td colspan="2"><input class="form-control asset-field" type="text" ' +
                                'data-id="' + client.hca_id + '" data-name="hca_address"' +
                                'name="address' + client.hca_id + '" value="' +
                                (null !== client.hca_address ? client.hca_address : '') + '"></td>')

                            // Asset City
                            .append('<td>' +
                                '<input type="city" class="form-control asset-field asset-info" ' +
                                'data-type="city" data-name="hca_city" value="' + client.hca_city + '" ' +
                                'name="city' + client.hca_id + '" data-id="' + client.hca_id + '"/>' +
                                '</td>')

                            // Asset State
                            .append('<td>' +
                                '<input type="text" class="form-control asset-field asset-info" ' +
                                'data-type="state" data-name="hca_state" value="' + client.hca_state + '"' +
                                'name="state' + client.hca_id + '"  data-id="' + client.hca_id + '"/>' +
                                '</td>')

                            // Asset Zip
                            .append('<td>' +
                                '<input type="text" class="form-control asset-field asset-info" ' +
                                'data-type="zip" data-name="hca_zip" value="' + client.hca_zip + '" ' +
                                'name="zip' + client.hca_id + '"  data-id="' + client.hca_id + '"/>' +
                                '</td>')

                            // Asset Info button
                            .append($('<td class="text-center">')
                                .append('<button class=\'btn btn-default btn-sm\'\n' +
                                    ' data-toggle=\'modal\'' +
                                    ' data-target=\'#client-info-dialog\'' +
                                    ' data-id=' + client.hca_id + ' data-owner=' + client.hc_id + '>' +
                                    '<span class=\'glyphicon glyphicon-info-sign\'></span>' +
                                    '</button></td>'
                                ))

                            // Notes button
                            .append($('<td class="text-center">')
                                .append('<button class=\'btn btn-default btn-sm\'\n' +
                                    ' data-target=\'#view-notes-dialog\' data-toggle=\'modal\'' +
                                    ' data-id=' + client.hca_id + ' >' +
                                    '<span class=\'glyphicon glyphicon-info-sign\'></span>' +
                                    '</button></td>'
                                ))

                            // Action button
                            .append($('<td class="text-center">')
                                .append('<button class=\'btn btn-default btn-sm\'\n' +
                                    ' data-target=\'#view-action-dialog\' data-toggle="modal"' +
                                    ' data-id=' + client.hca_id + ' data-owner=' + client.hc_id + '>' +
                                    '<span class=\'glyphicon glyphicon-info-sign\'></span>' +
                                    '</button></td>'
                                ))

                            // ACM Button.
                            .append($('<td class="text-center">')
                                .append('<button class="btn btn-default btn-sm"' +
                                    ' data-id="' + client.hc_id + '" data-toggle="modal" ' +
                                    ' data-type="asset" data-target="#view-alert-dialog">' +
                                    '<span class="glyphicon glyphicon-book"></span></button></tr></table>'))

                            // Submit Button.
                            .append($('<td class="text-center">')
                                .append('<button class="btn btn-primary move-client"\n' +
                                    ' data-status="' + client.hca_status + '"' +
                                    ' data-id="' + client.hca_id + '" id="move-client' + client.hca_id + '">' +
                                    '<span title="Long press to submit multiple assets." class="has-tooltip">' +
                                    'Submit</span></button>' +
                                    '<input type="checkbox" class="submit-client-check hidden" ' +
                                    'name="id' + client.hca_id + '" ' +
                                    'value="' + client.hca_id + '">'))

                            // Delete Button.
                            .append($('<td class="text-center">')
                                .append('<button class="btn btn-default btn-primary delete-asset"' +
                                    ' data-id="' + client.hca_id + '" data-type="client_asset" data-key="hca">' +
                                    '<span class="glyphicon glyphicon-remove" style="color: #8ff1fd"></span></button></tr></table>'))
                    );

                let second_row_id = '-2',
                    unit_array = ['Under 10U', 'Between 10 & 20U', 'Between 20 & 50U',
                        'Between 50 & 100U', 'Between 100 & 200U', 'Over 200U'],
                    sq_ft_array = ['Under 5K', 'Between 5 & 10K', 'Between 10 & 20K',
                        'Between 20 & 50K', 'Between 50 & 100K', 'Over 100K'];
                count > 1 ? second_row_id = '-' + count + '-1' : '';

                $('#' + add + 'asset-row' + client.hc_id + second_row_id)
                    .after($('<tr>')

                        // Asset product type
                            .append('<td><select class="form-control homebase-product-type asset-field" ' +
                                'name="product' + client.hca_id + '" id="product' + client.hca_id + '"' +
                                'data-id="' + client.hca_id + '" data-name="hca_product_id"></select></td>')

                            // Asset Market Area
                            .append('<td><select class="form-control client-location asset-field"' +
                                ' name="location' + client.hca_id + '" data-id="' + client.hca_id + '"' +
                                ' data-name="hca_location" id="location' + client.hca_id + '"></select></td>')

                            // Asset Units
                            .append('<td><input class="form-control client-unit asset-field"' +
                                ' name="unit' + client.hca_id + '" data-id="' + client.hca_id + '"' +
                                ' data-name="hca_unit" value="' + client.hca_unit + '"' +
                                ' id="unit' + client.hca_id + '"/>' +
                                '</td>')

                            // Asset Square Foot
                            .append('<td><input class="form-control client-foot asset-field"' +
                                ' name="sq_ft' + client.hca_id + '" data-id="' + client.hca_id + '"' +
                                ' data-name="hca_ft" value="' + client.hca_ft + '"' +
                                ' id="sq_ft' + client.hca_id + '"/>' +
                                '</td>')

                            // Asset Condition
                            .append('<td>' +
                                '<select class="form-control asset-field asset-info"' +
                                ' data-type="cond" data-name="hca_cond" ' +
                                'name="cond' + client.hca_id + '"  data-id="' + client.hca_id + '" ' +
                                'style="min-width: 8vw">' +
                                '<option value="-1"> -- Add New --</option>' +
                                '<option value="0" selected> -- None --</option>' +
                                $.map(assets_info.cond, function (cond, index) {
                                    return '<option value="' + cond.hai_id + '" ' +
                                        (cond.hai_id === client.hca_cond ? 'selected' : '') + '>'
                                        + cond.hai_value + '</option>'
                                }).join('') +
                                '</select>' +
                                '</td>')

                            // Asset Criteria
                            .append('<td><select class="form-control client-criteria asset-field"' +
                                ' name="criteria' + client.hca_id + '"' +
                                ' data-id="' + client.hca_id + '" data-name="hca_criteria_used"' +
                                '" id="criteria' + client.hca_id + '"></select></td>')

                            // Asset Status
                            .append('<td><select class="form-control client-status asset-field"' +
                                ' name="status' + client.hca_id +
                                '" data-id="' + client.hca_id + '"' +
                                ' data-name="hca_status" id="status' + client.hca_id + '">' +
                                '</select></td>')

                            // Asset Tool
                            .append('<td colspan="3">' +
                                '<div class="input-group" style="width: 100%">' +
                                '<a href="' + tsl_reference + '" ' +
                                ('None' !== tsl_name ? 'target="_blank"' : '') + '>' +
                                '<input type="text" class="form-control asset-tool asset-field"' +
                                ' name="asset' + client.hca_id +
                                '" data-id="' + client.hca_id + '" id="tool' + client.hca_id + '" ' +
                                'value="' + tsl_name + '"></a>' +
                                '<span class="input-group-btn">' +
                                '<button class="btn btn-default remove-tool" data-id="' + client.hca_id + '">' +
                                '<i class="fa fa-times"></i>' +
                                '</button></span></div></td>')

                            // Asset Portfolio Info.
                            .append('<td colspan="4"><div class="input-group">' +
                                '<select id="portfolio-select' + client.hca_id + '" ' +
                                'class="form-control input-group-addon client-portfolio" ' +
                                'style="min-width: 5vw; background-color: #fff !important;">' +
                                '<option value="-1">-- Add Portfolio --</option>' +
                                '<option value="0" selected>-- None --</option>' +
                                $.map(portfolio, function (ind_portfolio, index) {
                                    let is_selected = '';
                                    // Check if this asset have any portfolio.
                                    if (client.hca_portfolio_id === ind_portfolio.up_id) {
                                        is_selected = 'selected';
                                    }

                                    // Append Portfolio names.
                                    return ('<option value="' + ind_portfolio.up_id + '" ' + is_selected + '>' +
                                        ind_portfolio.up_name + '</option>');
                                }).join('') +
                                '</select>' +
                                '<span class="input-group-btn">' +
                                '<span class="has-tooltip" title="Add to Portfolio" >' +
                                '<button class="btn btn-default add-portfolio" type="button"' +
                                ' data-id="' + client.hca_id + '">' +
                                '<i class="fa fa-chain-broken"></i></button></span></span>' +
                                '</div></td>')
                    );


                $(document).on('click', '.promo_sent, .info-popover, .dst', function () {
                    $(this).popover('show');
                    $(this).popover({
                        container: 'body'
                    });
                });
                useTooltip();
                usePopover();

                // Color select box if portfolio is present.
                if (client.hca_portfolio_id !== null) {
                    $('#asset-table' + client.hc_id).find('input, select, button, a')
                        .attr('disabled', 'true')
                        .attr('title', 'This asset can be updated in portfolio tab.');

                    $('#portfolio-select' + client.hca_id)
                        .css('cssText', 'background-color: #FFFFE0 !important');
                }

                (client.hca_clu !== null) ? $('#clu' + client.hca_id).val(client.hca_clu) : '';
                (client.hca_rwt !== null) ? $('#rwt' + client.hca_id).val(client.hca_rwt) : '';

                // Reset Asset Image Height.
                $('.upload-asset-image').css('height', 0);
                $('.upload-asset-image').css('height', $('.asset-image:visible').parent().parent().parent().height());
            },

            // Append a new Asset Table Header.
            append_buyer_head = (client, table = $('#buyer-table'), text_to_append = '') => {
                let image_url = jsglobals.base_url + 'resources/app/media/user-avatar.svg';
                // Check if image is present
                (null !== client.hba_avatar) ?
                    image_url = window.location.origin + '/tools/homebase/get_owner_image/?img=' + client.hba_avatar : '';

                table.find('tbody:first')
                    .append($(
                        '<tr id="asset-tr' + client.hb_id + '"><td colspan="10">' +
                        '<table id="asset-table' + client.hb_id + '" class="table table-condensed" ' +
                        'style="border: 5px solid #008080cf;">' +
                        '<tr class="asset_head" ' +
                        'id="' + text_to_append + 'buyer-asset-row' + client.hb_id + '-1" style="background: #ffffe09e;">' +
                        '<th rowspan="4" colspan="2" class="upload-asset-image"><img src=' + image_url +
                        ' id="asset-avatar" class="asset-image">' +
                        '<div class="upload">' +
                        '<input id="asset-img-upload" type="file" name="files[]"' +
                        ' data-url=' + jsglobals.base_url + 'homebase/upload_buyer_asset_image' +
                        ' style="visibility: hidden; float:left; width:0;">' +
                        '<a id="upload-asset-img" data-id="' + client.hca_id + '">Click to Change Image</a></div></th>' +
                        '<th class="text-center"><span style="float: left"><b>A1</b></span>Product Type</th>' +
                        '<th class="text-center" colspan="2">Most Recent Acquisition</th>' +
                        '<th class="text-center" colspan="2">Entity Address</th>' +
                        '<th class="text-center">City</th>' +
                        '<th class="text-center">State</th>' +
                        '<th class="text-center">Zip</th>' +
                        '<th class="text-center">Status</th>' +
                        '<th class="text-center">Info</th>' +
                        '<th class="text-center">Notes</th>' +
                        '<th class="text-center">Action</th>' +
                        '<th class="text-center">ACM</th>' +
                        '<th class="text-center">Submit</th>' +
                        '<th class="text-center">Remove</th>' +
                        '</tr>' +
                        '<tr class="asset_head" id="' + text_to_append + 'buyer-asset-row' + client.hb_id + '-2"' +
                        ' style="background: #ffffe09e;">' +
                        '<th class="text-center">Buyer Type</th>' +
                        '<th class="text-center">Market Area</th>' +
                        '<th class="text-center"> Units</th>' +
                        '<th class="text-center"> Size</th>' +
                        '<th class="text-center">Condition</th>' +
                        '<th class="text-center">Criteria</th>' +
                        '<th class="text-center">Price Range</th>' +
                        '<th class="text-center">CAP Rate</th>' +
                        '<th class="text-center">Financing</th>' +
                        '<th class="text-center" colspan="2">Contingencies</th>' +
                        '<th class="text-center" colspan="2">Terms</th>' +
                        '<th class="text-center" colspan="2">Strategy</th>' +
                        '</tr>'));
            },

            append_buyer_body = (client, add = '', count = 1) => {
                let tsl_reference = '#',
                    tsl_name = 'None';

                // Check if this homebase belongs to any TSL instance.
                if (null !== client.tsl_name) {
                    tsl_reference = '/tools/tsl/tsl/' + client.tsl_id;
                    tsl_name = 'TSL: ' + client.tsl_name;
                } else if (null !== client.cpd_name) {
                    tsl_reference = '/tools/cpdv1/cpdv1/' + client.cpd_id;
                    tsl_name = 'DMD: ' + client.cpd_name;
                }

                // Clone dummy rows.
                // let dummy_content = $('.dummy_content').clone();
                // dummy_content.removeClass('dummy_content');
                // dummy_content.removeClass('hidden');
                //
                // //Fetch the HTML content for Popover and replace with values.
                // dummy_content.find('.price').attr('id', 'price' + client.hba_id);
                // dummy_content.find('.price').attr('data-id', client.hba_id);
                // dummy_content.find('.price').attr('value', '$' + parseInt(client.hba_price).toLocaleString());
                //
                // dummy_content.find('.rate').attr('value', parseInt(client.hca_rate) + '%');
                // dummy_content.find('.rate').attr('data-id', client.hca_id);
                // dummy_content.find('.rate').attr('id', 'rate' + client.hca_id);
                //
                // dummy_content.find('.gross').attr('value', '$' +
                //     parseInt(client.hca_price * client.hca_rate / 100).toLocaleString());
                //
                // dummy_content.find('.gross').attr('data-id', client.hca_id);
                // dummy_content.find('.gross').attr('id', 'gross' + client.hca_id);
                //
                // dummy_content.find('.csplit').attr('value', parseInt(client.hca_split) + '%');
                // dummy_content.find('.csplit').attr('data-id', client.hca_id);
                // dummy_content.find('.csplit').attr('id', 'split' + client.hca_id);
                //
                // dummy_content.find('.net').attr('value', '$' + parseInt(client.hca_net));
                // dummy_content.find('.net').attr('data-id', client.hca_id);
                // dummy_content.find('.net').attr('id', 'net' + client.hca_id);
                //
                // dummy_content.find('.gsplit').attr('value', parseInt(client.hca_gsplit) + '%');
                // dummy_content.find('.gsplit').attr('data-id', client.hca_id);
                // dummy_content.find('.gsplit').attr('id', 'gsplit' + client.hca_id);
                //
                // dummy_content.find('.gnet').attr('data-id', client.hca_id);
                // dummy_content.find('.gnet').attr('id', 'gnet' + client.hca_id);

                $('#' + add + 'buyer-asset-row' + client.hb_id + '-' + count)
                    .after($('<tr>')
                        // Product Type
                            .append('<td><select class="form-control buyer-asset-field homebase-product-type" ' +
                                'name="product' + client.hba_id + '" id="buyer-product' + client.hba_id + '"' +
                                'data-id="' + client.hba_id + '" data-name="hba_product_type"></select></td>')

                            // Most Recent Acquisition
                            .append('<td colspan="2"><input class="form-control buyer-asset-field" type="text" ' +
                                'data-id="' + client.hba_id + '" data-name="hba_acquisition"' +
                                'name="acquisition' + client.hba_id + '" ' +
                                'value="' + (null !== client.hba_acquisition ? client.hba_acquisition : '') + '"></td>')

                            // Asset Address.
                            .append('<td colspan="2"><input class="form-control buyer-asset-field" type="text" ' +
                                'data-id="' + client.hba_id + '" data-name="hba_entity_address"' +
                                'name="address' + client.hba_id + '" value="' +
                                (null !== client.hba_entity_address ? client.hba_entity_address : '') + '"></td>')

                            // Asset City
                            .append('<td>' +
                                '<input type="city" class="form-control buyer-asset-field" ' +
                                'data-type="city" data-name="hba_city" value="' + client.hba_city + '" ' +
                                'name="city' + client.hba_city + '" data-id="' + client.hba_id + '"/>' +
                                '</td>')

                            // Asset State
                            .append('<td>' +
                                '<input type="text" class="form-control buyer-asset-field" ' +
                                'data-type="state" data-name="hba_state" value="' + client.hba_state + '"' +
                                'name="state' + client.hba_id + '"  data-id="' + client.hba_id + '"/>' +
                                '</td>')

                            // Asset Zip
                            .append('<td>' +
                                '<input type="text" class="form-control buyer-asset-field" ' +
                                'data-type="zip" data-name="hba_zip" value="' + client.hba_zip + '" ' +
                                'name="zip' + client.hba_id + '"  data-id="' + client.hba_id + '"/>' +
                                '</td>')

                            // Buyer Status
                            .append('<td>' +
                                '<select type="text" class="form-control buyer-asset-field buyer-asset-status"' +
                                ' data-type="zip" data-name="hba_status" value="' + client.hba_status + '"' +
                                ' id="buyer-status' + client.hba_id + '"  data-id="' + client.hba_id + '"' +
                                ' name="buyer_status' + client.hba_id + '"></select>' +
                                '</td>')

                            // Asset Info button
                            .append($('<td class="text-center">')
                                .append('<button class=\'btn btn-default btn-sm\'\n' +
                                    ' data-toggle=\'modal\'' +
                                    ' data-target=\'#client-info-dialog\'' +
                                    ' data-id=' + client.hba_id + ' data-owner=' + client.hb_id + '>' +
                                    '<span class=\'glyphicon glyphicon-info-sign\'></span>' +
                                    '</button></td>'
                                ))

                            // Notes button
                            .append($('<td class="text-center">')
                                .append('<button class=\'btn btn-default btn-sm\'\n' +
                                    ' data-target=\'#view-notes-dialog\' data-toggle=\'modal\'' +
                                    ' data-id=' + client.hba_id + ' >' +
                                    '<span class=\'glyphicon glyphicon-info-sign\'></span>' +
                                    '</button></td>'
                                ))

                            // Action button
                            .append($('<td class="text-center">')
                                .append('<button class=\'btn btn-default btn-sm\'\n' +
                                    ' data-target=\'#view-action-dialog\' data-toggle="modal"' +
                                    ' data-id=' + client.hba_id + ' data-owner=' + client.hb_id + '>' +
                                    '<span class=\'glyphicon glyphicon-info-sign\'></span>' +
                                    '</button></td>'
                                ))

                            // ACM Button.
                            .append($('<td class="text-center">')
                                .append('<button class="btn btn-default btn-sm"' +
                                    ' data-id="' + client.hb_id + '" data-toggle="modal" ' +
                                    ' data-type="asset" data-target="#view-alert-dialog">' +
                                    '<span class="glyphicon glyphicon-book"></span></button></tr></table>'))

                            // Submit Button.
                            .append($('<td class="text-center">')
                                .append('<button class="btn btn-primary move-client"\n' +
                                    ' data-status="' + client.hba_status + '" data-type="buyer"' +
                                    ' data-id="' + client.hba_id + '" id="move-client' + client.hba_id + '">' +
                                    '<span title="Long press to submit multiple assets." class="has-tooltip">' +
                                    'Submit</span></button>' +
                                    '<input type="checkbox" class="submit-client-check hidden" ' +
                                    'name="id' + client.hba_id + '" ' +
                                    'value="' + client.hba_id + '">'))

                            // Delete Button.
                            .append($('<td class="text-center">')
                                .append('<button class="btn btn-default btn-primary delete-asset"' +
                                    ' data-id="' + client.hba_id + '" data-type="client_asset" data-key="hba">' +
                                    '<span class="glyphicon glyphicon-remove" style="color: #8ff1fd"></span></button></tr></table>'))
                    );

                let second_row_id = '-2';
                count > 1 ? second_row_id = '-' + count + '-1' : '';

                $('#' + add + 'buyer-asset-row' + client.hb_id + second_row_id)
                    .after($('<tr>')

                        // '<input type="city" class="form-control asset-field buyer-asset-info" ' +
                        // 'data-type="city" data-name="hba_city" value="' + client.hba_city + '" ' +
                        // 'name="city' + client.hba_city + '" data-id="' + client.hba_id + '"/>' +
                        // Asset Buyer type
                            .append('<td><input type="text" class="form-control buyer-asset-field" ' +
                                'id="buyer-type' + client.hba_id + '" data-name="hba_buyer_type" ' +
                                'value="' + client.hba_buyer_type + '" name="buyer' + client.hba_id + '"' +
                                ' data-id="' + client.hba_id + '"/></td>')

                            // Asset Market Area
                            .append('<td><select class="form-control client-location buyer-asset-field"' +
                                ' name="location' + client.hba_id + '" data-id="' + client.hba_id + '"' +
                                ' data-name="hba_market_area" id="buyer-location' + client.hba_id + '"></select></td>')

                            // Asset Units
                            .append('<td><input class="form-control client-unit buyer-asset-field"' +
                                ' name="unit' + client.hba_id + '" data-id="' + client.hba_id + '"' +
                                ' data-name="hba_units" value="' + client.hba_units + '"' +
                                ' id="unit' + client.hba_id + '"/>' +
                                '</td>')

                            // Asset Square Foot
                            .append('<td><input class="form-control client-foot buyer-asset-field"' +
                                ' name="sq_ft' + client.hba_id + '" data-id="' + client.hba_id + '"' +
                                ' data-name="hba_size" value="' + client.hba_size + '"' +
                                ' id="hba_size' + client.hba_id + '"/>' +
                                '</td>')

                            // Asset Condition
                            .append('<td>' +
                                '<select class="form-control buyer-asset-field"' +
                                ' data-type="cond" data-name="hba_condition" ' +
                                'name="cond' + client.hba_id + '"  data-id="' + client.hba_id + '" ' +
                                'style="min-width: 8vw">' +
                                '<option value="-1"> -- Add New --</option>' +
                                '<option value="0" selected> -- None --</option>' +
                                $.map(assets_info.cond, function (cond, index) {
                                    return '<option value="' + cond.hai_id + '" ' +
                                        (cond.hai_id === client.hba_condition ? 'selected' : '') + '>'
                                        + cond.hai_value + '</option>'
                                }).join('') +
                                '</select>' +
                                '</td>')

                            // Asset Criteria
                            .append('<td><select class="form-control client-criteria buyer-asset-field"' +
                                ' name="criteria' + client.hba_id + '"' +
                                ' data-id="' + client.hba_id + '" data-name="hba_criteria"' +
                                ' " id="buyer-criteria' + client.hba_id + '"></select></td>')

                            // Asset Status
                            // .append('<td><select class="form-control client-status buyer-asset-field"' +
                            //     ' name="status' + client.hba_id +
                            //     '" data-id="' + client.hba_id + '"' +
                            //     ' data-name="hba_status" id="status' + client.hba_id + '">' +
                            //     '</select></td>')

                            .append('<td><input class="form-control client-foot buyer-asset-field"' +
                                ' name="price_range' + client.hba_id + '" data-id="' + client.hba_id + '"' +
                                ' data-name="hba_price_range" value="' + client.hba_price_range + '"' +
                                ' id="hba_price' + client.hba_id + '"/>' +
                                '</td>')

                            // Asset Tool
                            .append('<td><input type="text" class="form-control buyer-asset-field"' +
                                ' name="cap' + client.hba_id + '" data-id="' + client.hba_id + '"' +
                                ' data-name="hba_cap_rate" value="' + client.hba_cap_rate + '"' +
                                ' id="hba_cap' + client.hba_id + '"/></td>')

                            .append('<td><input type="text" class="form-control buyer-asset-field"' +
                                ' name="finance' + client.hba_id + '"  data-id="' + client.hba_id + '"' +
                                ' data-name="hba_financing" value="' + client.hba_financing + '"' +
                                ' id="hba_financing' + client.hba_id + '"/></td>')

                            .append('<td colspan="2"><input type="text" class="form-control buyer-asset-field"' +
                                ' name="contingency' + client.hba_id + '" data-id="' + client.hba_id + '"' +
                                ' data-name="hba_contingency" value="' + client.hba_contingency + '"' +
                                ' id="hba_contingency' + client.hba_id + '"/></td>')

                            .append('<td colspan="2"><input type="text" class="form-control buyer-asset-field"' +
                                ' name="term' + client.hba_id + '" data-id="' + client.hba_id + '"' +
                                ' data-name="hba_term" value="' + client.hba_term + '"' +
                                ' id="hba_term' + client.hba_id + '"/></td>')

                            .append('<td colspan="2"><input type="text" class="form-control buyer-asset-field"' +
                                ' name="strategy' + client.hba_id + '" data-id="' + client.hba_id + '"' +
                                ' data-name="hba_acquisition_strategy" value="' + client.hba_acquisition_strategy + '"' +
                                ' id="hba_acquisition_strategy' + client.hba_id + '"/></td>')
                    );


                $(document).on('click', '.promo_sent, .info-popover, .dst', function () {
                    $(this).popover('show');
                    $(this).popover({
                        container: 'body'
                    });
                });
                useTooltip();
                usePopover();

                // Color select box if portfolio is present.
                // if (client.hca_portfolio_id !== null) {
                //     $('#asset-table' + client.hc_id).find('input, select, button, a')
                //         .attr('disabled', 'true')
                //         .attr('title', 'This asset can be updated in portfolio tab.');
                //
                //     $('#portfolio-select' + client.hca_id)
                //         .css('cssText', 'background-color: #FFFFE0 !important');
                // }

                (client.hca_clu !== null) ? $('#clu' + client.hca_id).val(client.hca_clu) : '';
                (client.hca_rwt !== null) ? $('#rwt' + client.hca_id).val(client.hca_rwt) : '';

                // Reset Asset Image Height.
                $('.upload-asset-image').css('height', 0);
                $('.upload-asset-image').css('height', $('.asset-image:visible').parent().parent().parent().height());
            };

        return {
            init: init,
        };
    })();
