$(document).ready(function () {
    let fv, id, origin;

    // Add Asset button click Handler
    $(document).on('click', '.add-form-asset', function (e) {
        let dummy_asset = $('.dummy-buyer-asset-info').clone();
        const position = $(this).parent('div').parent('div').siblings('div').find('input').attr('data-position'),
            parent_div = $(this).parent('div').parent('div').parent('div'),
            asset_number = $(this).parent('div').parent('div').siblings('div').find('input').attr('data-asset');

        // Set the appropriate Classes and attributes for Asset rows.
        dummy_asset = dummy_asset.removeClass('hidden dummy-buyer-asset-info').addClass('add-buyer-asset-info');
        dummy_asset.find('#buyer-asset-name').attr('name', 'asset_name' + position + '-' + asset_number);
        dummy_asset.find('#buyer-asset-location').attr('name', 'asset_location' + position + '-' + asset_number);
        dummy_asset.find('#address').attr('name', 'asset_address' + position + '-' + asset_number)
            .attr('id', 'address' + position + '-' + asset_number);
        dummy_asset.find('#buyer-asset-criteria').attr('name', 'asset_criteria' + position + '-' + asset_number);
        dummy_asset.find('#buyer-asset-zip').attr('name', 'asset_zip' + position + '-' + asset_number);
        dummy_asset.find('#buyer-asset-status').attr('name', 'asset_status' + position + '-' + asset_number);

        // New added fields.
        dummy_asset.find('#buyer-asset-city').attr('name', 'asset_city' + position + '-' + asset_number);
        dummy_asset.find('#buyer-asset-state').attr('name', 'asset_state' + position + '-' + asset_number);
        dummy_asset.find('#buyer-asset-product').attr('name', 'asset_product' + position + '-' + asset_number);
        dummy_asset.find('#buyer-asset-units').attr('name', 'asset_unit' + position + '-' + asset_number);
        dummy_asset.find('#buyer-asset-sq-ft').attr('name', 'asset_sq_ft' + position + '-' + asset_number);
        dummy_asset.find('#buyer-asset-cond').attr('name', 'asset_cond' + position + '-' + asset_number);
        dummy_asset.find('#buyer-asset-criteria').attr('name', 'asset_criteria' + position + '-' + asset_number);
        dummy_asset.find('#buyer-asset-lead').attr('name', 'asset_lead' + position + '-' + asset_number);
        dummy_asset.find('#buyer-asset-source').attr('name', 'asset_source' + position + '-' + asset_number);

        // Increment number of assets.
        $(this).parent('div').parent('div').siblings('div').find('input').attr('data-asset', parseInt(asset_number) + 1);

        parent_div.after(dummy_asset);
        e.preventDefault();
    });

    // Click handler for select box
    $(document).on('change', '.toggle-modal-select', function () {
        if ('-1' === $(this).val()) {
            $('#add-lead-button').attr('data-type', $(this).attr('data-type'));
            $('#lead-type').val($(this).attr('data-type'));

            $('#' + $(this).attr('data-target')).modal('show');
        }
    });

    // Click handler for Delete Button.
    $(document).on('click', '.delete-form-asset', function (e) {
        const asset_count = $(this).parent('div').parent('div').siblings('div').find('input').attr('data-asset');

        // Decrement asset count.
        $(this).parent('div').parent('div').siblings('div').find('input').attr('data-asset', parseInt(asset_count) - 1);
        $(this).parent('div').parent('div').remove();
        e.preventDefault();
    });

    $('#add-buyer-dialog').on('show.bs.modal', function () {
        // Remove any previous yellow rows and Asset info rows.
        $('.yellow_row').removeClass('yellow_row');
        $('.add-buyer-asset-info').remove();

        // Enable form cache.
        const add_buyer_form_selector = $("#add-buyer-form");
        add_buyer_form_selector.formcache("clear");
        add_buyer_form_selector.formcache("destroy");

        // Set the form input asset count to 2.
        $('.buyer-row input').attr('data-asset', 2);

        $('.additional-buyer-info').remove();
        add_buyer_form_selector[0].reset();

        const get_all_products = makeAjaxCall('industry/get_all_products'),
            product_selector = $('.add-buyer-product');

        get_all_products.then(function (data) {
            product_selector.empty();
            $.each(data.products, function (index, product) {
                product_selector
                    .append('<option style="font-weight: bold; font-style: italic;"' +
                        ' value="' + product[0]['u_product_group_id'] + '">' + index + '</option>');

                $.each(product, function (index, individual_product) {
                    product_selector
                        .append('<option value="' + individual_product.u_product_id + '">' +
                            individual_product.u_product_name + '</option>');
                });
            });
        });

        get_all_deal_type();

        // Fetch the parameters from URL.
        const path = window.location.pathname,
            components = path.split('/'),
            buyer_id = components[components.length - 1];

        origin = components[2];

        // Fetch all the Criteria and Target Audience if request is from DMD.
        if (origin === 'tsl') {
            id = buyer_id;
            get_all_criteria();
        }
        else if (origin === 'cpdv1') {
            id = buyer_id;
            get_all_criteria();
        }
        else {
            get_all_criteria();
            id = 0;
        }
    });

    $(document).on('click', '#close-add-buyer-dialog', function () {
        $('#add-buyer-dialog').modal('hide');
    });

    // Append Additional rows for buyer info.
    $('#buyer-count').change(function () {
        $('.additional-buyer-info').remove();

        // Start from two as one row is present by default.
        let i = 2;
        while (i <= $(this).val()) {
            let info_html = $('.add-buyer-info').clone();
            info_html.find('.add-buyer-asset-info').remove();

            // Change the name and ID attribute for additional records.
            info_html.find('#buyer-name1')
                .attr('name', 'buyer_name' + i)
                .attr('id', 'buyer-name' + i)
                .attr('data-position', i)
                .attr('data-asset', 2);

            info_html.find('#buyer-email1')
                .attr('name', 'buyer_email' + i)
                .attr('id', 'buyer-email' + i);

            info_html.find('#buyer-main-phone1')
                .attr('name', 'main_phone' + i)
                .attr('id', 'buyer-main-phone' + i);

            info_html.find('#buyer-mobile-phone1')
                .attr('name', 'mobile_phone' + i)
                .attr('id', 'buyer-mobile-phone' + i);

            info_html.find('#buyer-second-phone1')
                .attr('name', 'second_phone' + i)
                .attr('id', 'buyer-second-phone' + i);

            info_html.find('#entity-name1')
                .attr('name', 'entity_name' + i)
                .attr('id', 'entity-name' + i);

            info_html.find('#buyer-asset-name1-1')
                .attr('name', 'asset_name' + i + '-1');

            info_html.find('#buyer-asset-location1-1')
                .attr('name', 'asset_location' + i + '-1');

            info_html.find('#address1-1')
                .attr('name', 'asset_address' + i + '-1');

            info_html.find('#asset_criteria1-1')
                .attr('name', 'asset_criteria' + i + '-1');

            info_html.find('#zip1-1')
                .attr('name', 'asset_zip' + i + '-1');

            info_html.find('#buyer-asset-status1-1')
                .attr('name', 'asset_status' + i + '-1');

            info_html.find('#city1-1')
                .attr('name', 'asset_city' + i + '-1');

            info_html.find('#state1-1')
                .attr('name', 'asset_state' + i + '-1');

            info_html.find('#buyer-asset-product1-1')
                .attr('name', 'asset_product' + i + '-1');

            info_html.find('#buyer-asset-units1-1')
                .attr('name', 'asset_unit' + i + '-1');

            info_html.find('#buyer-asset-sq-ft1-1')
                .attr('name', 'asset_sq_ft' + i + '-1');

            info_html.find('#buyer-asset-cond1-1')
                .attr('name', 'asset_cond' + i + '-1');

            info_html.find('#buyer-asset-criteria1-1')
                .attr('name', 'asset_criteria' + i + '-1');

            info_html.find('#buyer-asset-lead1-1')
                .attr('name', 'asset_lead' + i + '-1');

            info_html.find('#buyer-asset-source1-1')
                .attr('name', 'asset_source' + i + '-1');

            $('.add-buyer-modal').append('<div class="additional-buyer-info col-xs-12 buyer-info' + i + '"' +
                ' style="margin-top: 2vh">' +
                info_html.html() + '</div>');
            i++;
        }
        add_cache();
        refreshCriteria();
    });

    // Form Validation.
    $('#add-buyer-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            buyer_name1: {
                validators: {
                    notEmpty: {
                        message: "The buyer name is required."
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the add new buyer form.
        const $form = $(e.target),
            fv = $form.data('formValidation'),
            // Get the form data and submit it.
            buyer_data = $form.serializeArray();

        buyer_data.push({name: 'origin', value: origin});
        buyer_data.push({name: 'id', value: id});

        let promises = [];

        // Loop through all the buyers and fetch there address if exits.
        for (let i = 1; i <= $('#buyer-count').val(); i++) {

            // Asset Count equals additional asset plus default assets.
            const asset_count = $('.buyer-info' + i + ' .add-buyer-asset').length +
                $('.buyer-info' + i + ' .buyer-asset-info').length;
            buyer_data.push({name: 'buyer-asset-count' + i, value: asset_count});
        }

        add_buyer();

        // Add a new buyer to the homebase.
        function add_buyer() {
            // add_buyer_loader();
            // Fetch Caller List.
            $.ajax({
                url: jsglobals.base_url + 'homebase/add_buyer',
                dataType: 'json',
                type: 'post',
                data: buyer_data,
                error: ajaxError
            }).done(function (data) {

                // console.log(data);

                if (data.status !== 'success') {
                    toastr.error(data.message);
                    return;
                } else if (data.status === 'redirect') {
                    window.location.href = data.redirect;
                    return;
                }

                toastr.success('Buyer Added!!', 'Success!!');
                // $.unblockUI();
                fv.resetForm();
                $($form)[0].reset();
                $('#add-buyer-form')[0].reset();
                $('#add-buyer-dialog').modal('hide');
                // (typeof refreshPage !== 'undefined' && $.isFunction(refreshPage)) ? refreshPage() : homebase.init();
                $('#buyer_tab').trigger('click')

            }).fail(function (jqXHR, textStatus) {
                $.unblockUI();
                toastr.error('Could not get the requested Buyer Project Dashboard instance.', 'Error!!');
            }).always(function () {
            });
        }
    });
});

const add_buyer_cache = () => {
    $("#add-buyer-form").formcache("destroy");
    $("#add-buyer-form").formcache("clear");
};

// Populate the Add Buyer Popup using fields extracted from excel file.
const populate_buyer = data => {
    // Remove any previous Buyers and Assets.
    $('.additional-buyer-info, .add-buyer-asset').remove();

    let buyer_array = [],
        buyer_name_array = [],
        buyer_count = 1;
    // Populate add buyer info pop up fields.
    $.each(data, function (index, buyer) {
        $('.buyer-asset-info').remove();

        let current_index = index + 1,
            buyer_name_selector = $('#buyer-name' + current_index),
            asset_number = 1;

        // if (buyer_array.indexOf(buyer['Name']) > -1) {
        //     current_index = buyer_array.indexOf(buyer['Name']);
        //     asset_number = $('#add-buyer-' + current_index + ' .add-buyer-asset-info').length + 1;
        // } else {
        buyer_name_array.push(buyer['Buyer Name']);
        buyer_array[buyer_count] = buyer['Buyer Name'];

        // Check if this row is already present.
        if (0 === buyer_name_selector.length) {
            buyer_count = buyer_count + 1;

            let info_html = $('.add-buyer-info').clone();
            info_html.find('.add-buyer-asset').remove();

            // Change the name and ID attribute for additional records.
            info_html.find('#buyer-name1')
                .attr('name', 'buyer_name' + buyer_count)
                .attr('id', 'buyer-name' + current_index);

            info_html.find('#buyer-main-phone1')
                .attr('name', 'main_phone' + buyer_count)
                .attr('id', 'buyer-main-phone' + current_index);

            info_html.find('#buyer-second-phone1')
                .attr('name', 'second_phone' + buyer_count)
                .attr('id', 'buyer-second-phone' + current_index);

            info_html.find('#buyer-mobile-phone1')
                .attr('name', 'mobile_phone' + buyer_count)
                .attr('id', 'buyer-mobile-phone' + current_index);

            info_html.find('#buyer-email1')
                .attr('name', 'buyer_email' + buyer_count)
                .attr('id', 'buyer-email' + current_index);

            info_html.find('#entity-name1')
                .attr('name', 'entity_name' + buyer_count)
                .attr('id', 'entity-name' + current_index);

            $('.add-buyer-modal')
                .append('<div class="additional-buyer-info col-xs-12 buyer-row buyer-info' + buyer_count + '"' +
                    ' id="add-buyer-' + current_index + '">' +
                    info_html.html() + '</div>');
        }

        // Set the values for fields.
        $('#buyer-name' + current_index).val(buyer['Buyer Name']);
        $('#buyer-main-phone' + current_index).val(buyer['Main Phone']);
        $('#buyer-second-phone' + current_index).val(buyer['Second Phone']);
        $('#buyer-mobile-phone' + current_index).val(buyer['Mobile Phone']);
        $('#buyer-email' + current_index).val(buyer['Client Email']);
        $('#entity-name' + current_index).val(buyer['Entity Name']);
        // }

        // Set the Assets Row.
        if (('' !== buyer['Entity Name']) || ('' !== buyer['Location']) || ('' !== buyer['Entity Address'])) {

            // Clone the HTML for Assets.
            let dummy_asset = $('.dummy-buyer-asset').clone();
            dummy_asset = dummy_asset.removeClass('hidden dummy-buyer-asset').addClass('add-buyer-asset');

            dummy_asset.find('#entity-name').attr('name', 'entity_name' + buyer_count + '-' + asset_number)
                .attr('id', 'entity-name' + buyer_count + '-' + asset_number).attr('data-name', buyer.Name);

            dummy_asset.find('#entity-location').attr('name', 'entity_location' + buyer_count + '-' + asset_number)
                .attr('id', 'entity-location' + buyer_count + '-' + asset_number);

            dummy_asset.find('#entity-address').attr('name', 'entity_address' + buyer_count + '-' + asset_number)
                .attr('id', 'entity-address' + buyer_count + '-' + asset_number);

            dummy_asset.find('#entity-criteria').attr('name', 'entity_criteria' + buyer_count + '-' + asset_number)
                .attr('id', 'entity-criteria' + buyer_count + '-' + asset_number).attr('data-name', buyer.Name);

            dummy_asset.find('#zip').attr('name', 'entity_zip' + buyer_count + '-' + asset_number)
                .attr('id', 'zip' + buyer_count + '-' + asset_number);

            dummy_asset.find('#status').attr('name', 'status' + buyer_count + '-' + asset_number);

            // New added fields.
            dummy_asset.find('#city').attr('name', 'entity_city' + buyer_count + '-' + asset_number)
                .attr('id', 'city' + buyer_count + '-' + asset_number).attr('data-name', buyer.Name);

            dummy_asset.find('#state').attr('name', 'entity_state' + buyer_count + '-' + asset_number)
                .attr('id', 'state' + buyer_count + '-' + asset_number).attr('data-name', buyer.Name);

            dummy_asset.find('#entity-product').attr('name', 'entity_product' + buyer_count + '-' + asset_number);

            dummy_asset.find('#entity-units').attr('name', 'entity_unit' + buyer_count + '-' + asset_number)
                .attr('id', 'entity-units' + buyer_count + '-' + asset_number).attr('data-name', buyer.Name);

            dummy_asset.find('#entity-sq-ft').attr('name', 'entity_sq_ft' + buyer_count + '-' + asset_number)
                .attr('id', 'entity-sq-ft' + buyer_count + '-' + asset_number).attr('data-name', buyer.Name);

            dummy_asset.find('#entity-cond').attr('name', 'entity_cond' + buyer_count + '-' + asset_number)
                .attr('id', 'entity-cond' + buyer_count + '-' + asset_number).attr('data-name', buyer.Name);

            dummy_asset.find('#entity-criteria').attr('name', 'entity_criteria' + buyer_count + '-' + asset_number)
                .attr('id', 'entity-criteria' + buyer_count + '-' + asset_number).attr('data-name', buyer.Name);

            dummy_asset.find('#entity-lead').attr('name', 'entity_lead' + buyer_count + '-' + asset_number);

            dummy_asset.find('#entity-source').attr('name', 'entity_source' + buyer_count + '-' + asset_number);

            $('#add-buyer-' + current_index).append(dummy_asset);

            $('#entity-name' + buyer_count + '-' + parseInt(asset_number)).val(buyer['Entity Name']);
            $('#zip' + buyer_count + '-' + parseInt(asset_number)).val(buyer['ZIP']);
            $('#entity-address' + buyer_count + '-' + parseInt(asset_number)).val(buyer['Entity Address']);
            $('#city' + buyer_count + '-' + parseInt(asset_number)).val(buyer['City']);
            $('#state' + buyer_count + '-' + parseInt(asset_number)).val(buyer['State']);
            $('#entity-units' + buyer_count + '-' + parseInt(asset_number)).val(buyer['Units']);
            $('#entity-sq-ft' + buyer_count + '-' + parseInt(asset_number)).val(buyer['Sq. Ft.']);

            $('#entity-cond' + buyer_count + '-' + parseInt(asset_number) + ' option')
                .filter(function () {
                    return $(this).html() == buyer['Condition'];
                }).attr('selected', true);

            // Check if the value for condition doesn't exist.
            if (($('#entity-cond' + buyer_count + '-' + parseInt(asset_number)).val() === '0')
                && $.trim(buyer['Condition']) !== '') {
                $('#entity-cond' + buyer_count + '-' + parseInt(asset_number))
                    .append('<option selected>' + buyer['Condition'] + '</option>');
            }

            $('#entity-criteria' + buyer_count + '-' + parseInt(asset_number) + ' option')
                .filter(function () {
                    return $(this).html() == buyer['Criteria'];
                }).attr('selected', true);

            // Check if criteria doesn't exist.
            if (($('#entity-criteria' + buyer_count + '-' + parseInt(asset_number)).val() === '')
                && $.trim(buyer['Criteria']) !== '') {
                $('#entity-criteria' + buyer_count + '-' + parseInt(asset_number))
                    .append('<option selected>' + buyer['Criteria'] + '</option>');
            }

            $('#entity-location' + buyer_count + '-' + parseInt(asset_number) + ' option')
                .filter(function () {
                    return $(this).html() == buyer['Market Area'];
                }).attr('selected', true);

            // Check if market area doesn't exist.
            if (($('#entity-location' + buyer_count + '-' + parseInt(asset_number)).val() === '')
                && $.trim(buyer['Market Area']) !== '') {
                $('#entity-location' + buyer_count + '-' + parseInt(asset_number))
                    .append('<option selected>' + buyer['Market Area'] + '</option>');
            }
        }
    });

    // Check if the format used if correct.
    if (buyer_name_array[0] === undefined) {
        toastr.error('Please download the form from import popup.', 'Wrong Format for excel form or No buyer present!!')
    } else {
        // const get_existing_buyer = makeAjaxCall('homebase/get_existing_buyers', {'buyers': buyer_name_array});
        // get_existing_buyer.then(function (data) {
        //     // Check for each Existing buyer.
        //     $.each(data.existing_buyers, function (index, buyer) {
        //         toastr.warning('These buyers will be highlighted with Yellow.',
        //             'Buyer with name ' + buyer.hc_name + ' already exist!!');
        //
        //         // Set the color for existing buyer name to green.
        //         $('input[data-name="' + buyer.hc_name + '"]').parent('div').parent('div')
        //             .parent('div').parent('div').find('.buyer-name').addClass('yellow_row');
        //     });
        // });

        //Change the number of enteries at the top.
        $('#buyer-count').val(buyer_count);
    }

    add_buyer_cache();
};
