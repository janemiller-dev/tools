/**
 *
 * Moves a Deal to TSL/DMD from homebase.
 *
 * @summary      Copy the instance of homebase deal to TSL/DMD based on the deal status.
 * @description  This file contains functions for moving a deal from homebase to TSL/DMD based on status and TSL
 *                instance selected.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */

$(document).ready(() => {

    // Move a client to TSL/DMD instance.
    $(document).off('click', '.move-client');
    $(document).on('click', '.move-client', function () {
        const asset_id = $(this).attr('data-id'),
            asset_type = undefined !== $(this).attr('data-type') ? $(this).attr('data-type') + '-' : '';

        let assets_to_be_pushed = [asset_id];

        if ($('#portfolio-select' + asset_id).val() > 0) {
            const get_all_portfolio_assets = makeAjaxCall('homebase/get_all_portfolio_assets', {
                id: $('#portfolio-select' + asset_id).val()
            });

            get_all_portfolio_assets.then(function (data) {
                assets_to_be_pushed = [];
                $.each(data.portfolio_assets, function (index, asset) {
                    assets_to_be_pushed.push(asset.hca_id);
                });
                push_deal.init(assets_to_be_pushed,
                    $('#status' + asset_id).val(), $('#status' + asset_id + ' option:selected').text(), 'portfolio');
            })
        } else if (!$('.submit-client-check').hasClass('hidden')) {
            const checked_clients = $('.submit-client-check:checked').serializeArray();
            assets_to_be_pushed = [];
            $.each(checked_clients, function (index, client) {
                assets_to_be_pushed.push(client.value);
            });

            push_deal.init(assets_to_be_pushed, 'scheduled', '', 'multi');
        }
        else {
            push_deal.init(assets_to_be_pushed,
                $('#' + asset_type + 'status' + asset_id).val(),
                $('#' + asset_type + 'status' + asset_id + ' option:selected').text(), 'single', asset_type);
        }
    });
});

/*
*
* Move a deal to TSL/DMD .
*/
let push_deal = (() => {
    // Initialize modal
    let init = (asset_id, asset_status, asset_text, type, asset_type) => {

            if ('' === asset_status) {
                toastr.warning('Please Select Appropriate Asset Status', 'Warning!!');
                return;
            }

            // Check if asset already belongs to a DMD/TSL instance.
            const is_asset_used = makeAjaxCall('homebase/is_asset_used', {
                'id': asset_id,
                'asset_type': asset_type
            });

            is_asset_used.then(function (data) {

                if (0 !== data.asset_used.length) {
                    // Remove any previous instance of asset use.
                    toastr.error("<br /><button type='button' id='confirmationYes' class='btn btn-default'>Yes</button>" +
                        "<button type='button' id='confirmationNo' class='btn btn-default' style='margin-left: 5px'>No</button>",
                        'This Asset is already in use. <br/> Clicking Yes will remove any previous instance of asset used.',
                        {
                            timeOut: 0,
                            extendedTimeOut: 0,
                            preventDuplicates: true,
                            tapToDismiss: false,
                            closeButton: true,
                            allowHtml: true,
                            onShown: function (toast) {
                                $("#confirmationYes").off('click');
                                $("#confirmationYes").click(function () {

                                    $.each(data.asset_used, function (index, asset) {
                                        const delete_prev_instance = makeAjaxCall('homebase/delete_prev_instance', {
                                            'table_name': asset.table_name,
                                            'deal_id': asset.deal_id,
                                            'tc_id': asset.tc_id,
                                            'type': asset_type
                                        });

                                        delete_prev_instance.then(function (data) {
                                            toastr.success('Asset Instance removed.', 'Success!!');
                                        });
                                    });


                                    submit_deal(asset_id, asset_status, asset_text, type, asset_type);
                                    $("#push-deal-dialog").modal('show');
                                    // Close the Toastr.
                                    $('.toast-close-button').trigger('click');
                                });

                                $('#confirmationNo').click(function () {
                                    $('.toast-close-button').trigger('click');
                                })
                            }
                        });

                } else {
                    submit_deal(asset_id, asset_status, asset_text, type, asset_type);
                    $("#push-deal-dialog").modal('show');
                }
            });

        },

        // Submit a deal to the TSL/DMD based on status.
        submit_deal = (asset_id, asset_status, asset_text, type, asset_type) => {

            // Check if the status for deal is `To be contacted`.
            if (asset_status === '0') {
                $('#sent-div').removeClass('hidden');

                // Fetch TSL instances for this user and product ID.
                const get_tsl_details = makeAjaxCall('homebase/search_tsl_instances', {});
                get_tsl_details.then(function (data) {
                    const tsl_instance_selector = $('#tsl-instance');
                    tsl_instance_selector.empty();
                    tsl_instance_selector
                        .append($('<option disabled>-- Select TSL Instance --</option>'));

                    // Append TSL instances to select box.
                    $.each(data.tsl_instances, function (index, tsl_instance) {
                        $('#tsl-instance')
                            .append($('<option value="' + tsl_instance.tsl_id + '">'
                                + tsl_instance.tsl_name
                                + '</option>'))
                    });

                    $('.move-deal-title').remove();
                    $('#tsl-row').append($('<div class="text-center move-deal-title">' +
                        '<p> Do you want to move this deal to TSL?</p></div>'))
                });
            } else {
                $('#sent-div').addClass('hidden');
                $('.move-deal-title').remove();
                $('#tsl-row').append($('<div class="text-center move-deal-title">' +
                    '<p> Do you want to move this deal to Active DMD\'s ' +
                    asset_text + ' deal status?</p></div>'))
            }

            $('#submit_deal').off('click');
            $('#submit_deal').click({
                asset_id: asset_id, asset_status: asset_status, type: type,
                asset_type: asset_type
            }, push_deal);
        },

        // Move a client to TSL/DMD.
        push_deal = (event) => {
            const asset_status = event.data.asset_status,
                asset_id = event.data.asset_id,
                type = event.data.type,
                asset_type = event.data.asset_type;

            console.log(event.data);

            const push_client = makeAjaxCall('homebase/push_client', {
                'id': asset_id,
                'status': asset_status,
                'type': type,
                'tsl': $('#tsl-instance').val(),
                'asset_type': asset_type
            });

            push_client.then(function (data) {
                $('#push-deal-dialog').modal('hide');

                // Check if the request was from buyer tab.
                asset_type === 'buyer-'
                    ? $('#buyer_tab').trigger('click') : refreshPage();
            });
            $('#submit_deal').off('click');
        };

    return {
        init: init
    }
})();