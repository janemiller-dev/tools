$(document).ready(function () {
    $('#add-portfolio-dialog').on('show.bs.modal', function (event) {
        $('#view-portfolio-dialog').modal('hide');
    });

    // Form validation.
    $('#add-portfolio-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            portfolio_name: {
                validators: {
                    notEmpty: {
                        message: "The deal name is required."
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the add new portfolio form.
        var $form = $(e.target);
        var fv = $form.data('formValidation');

        // Get the form data and submit it.
        var portfolio_data = $form.serialize();

        $.ajax({
            url: jsglobals.base_url + 'homebase/add_portfolio',
            dataType: 'json',
            type: 'post',
            data: portfolio_data,
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            } else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('Portfolio Added!!', 'Success!!')
            fv.resetForm();
            $($form)[0].reset();

            $('#add-portfolio-dialog, #view-portfolio-dialog').modal('hide');
            refreshPage();

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });
});