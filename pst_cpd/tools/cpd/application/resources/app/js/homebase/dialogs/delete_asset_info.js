$(document).ready(function () {

    // When the dialog is displayed, set the current instance ID.
    $('#delete-asset-info-dialog').on('show.bs.modal', function (event) {
        $('#view-asset-info-dialog').modal('hide');

        const type = $(event.relatedTarget).attr('data-type'),
            id = $(event.relatedTarget).attr('data-id'),
            name = $(event.relatedTarget).attr('data-name');
        $('#delete-asset-info-title').text('Delete ' + type);
        $('#asset-info-name').text(name);

        // Delete Asset Info click handler.
        $('#delete-asset-info-button').off('click')
        $('#delete-asset-info-button').on('click', function () {
            const delete_asset_info = makeAjaxCall('homebase/delete_asset_info', {
                'id': id
            });

            // Success handler.
            delete_asset_info.then(function (data) {
                toastr.success(type + ' Deleted.');
                $('#delete-asset-info-dialog').modal('hide');
                refreshPage();
            })
        })
    });
});
