$(document).ready(function () {
    let assets_info = '',
        leads = '',
        sources = '',
        client_id,
        info_type,
        buyer_type = 'false',
        owner_type = 'true';


    $('#view-owner-profile-dialog').on('show.bs.modal', function (event) {
        const owner_id = $(event.relatedTarget).attr('data-id'),
            get_owner_info = makeAjaxCall('homebase/get_owner_info', {'owner_id': owner_id}),
            add_info_block_selector = $('#add-info-block');

        client_id = owner_id;
        const get_asset_info = makeAjaxCall('homebase/get_asset_info', {});
        get_asset_info.then(function (data) {
            assets_info = data.asset_info;
        });

        function get_owner_asset_info() {
            const get_asset_info = makeAjaxCall('common/get_asset_info', {
                type: $('#owner-profile-nav li.active').attr('data-type'),
                id: $('.owner-asset-active').attr('data-id')
            });

            get_asset_info.then(function (data) {
                $('.owner-asset-input').val('');
                $.each(data.asset_info[0], function (index, data) {
                    $('.owner-asset-input[data-col="' + index + '"]').val(data);
                })
            })
        }

        const get_lead_source = makeAjaxCall('homebase/get_lead_source', {});

        get_lead_source.then(function (data) {
            leads = data.lead_source.lead;
            sources = data.lead_source.source;
        });

        // Owner Asset Image click handler.
        $(document).on('click', '.owner-asset-image', function () {
            if ($(this).parent().parent().hasClass('owner-asset-active')) {
                $(this).parent().parent().removeClass('owner-asset-active');
            } else {
                $(this).parent().parent().addClass('owner-asset-active');
            }
        });

        if (owner_id !== 0) {
            // Fetches Property information.
            const get_prop_info = makeAjaxCall("cpdv1/get_prop_info", {
                client_id: owner_id,
                type: 'info',
                is_buyer: buyer_type,
                'is_owner': 'true'
            });


            get_prop_info.then(function (data) {
                updated_data = data;

                // Adds options to drop down.
                $.each(data.prop_info, function (index, info) {
                    $('#owner-info-' + info.cc_type).append('<option data-index="' + index + '" value="' + info.cc_id + '">'
                        + info.cc_name + '</option>');
                });

                const info_select_selector = $('.owner-info-select');

                // Looks for a info drop down change event.
                info_select_selector.unbind().change(function () {

                    const index = $(this).find(':selected').data('index');
                    if (0 === parseInt($(this).val())) {

                        // Hides add new block button.
                        add_info_block_selector.addClass('hidden');

                        info_type = 'info';
                        $(this).attr('data-client-id', owner_id);
                        $(this).attr('data-info-type', info_type);

                        active_id = -1;
                        $('#add-client-info-dialog').modal('show');
                        $('#view-owner-profile-dialog').modal('hide');
                        $('.owner-info-select').val(-1);
                        $('#is-owner').val(owner_type);
                        $('#info-type').val('info');
                        $('#client-id').val(client_id);
                        $('#add-client-info-title').text('Add new ' + $(this).data('name'));
                        $('#relation-type').val($(this).data('name'));
                    } else {

                        // Shows add new block button.
                        add_info_block_selector.removeClass('hidden');
                        $('.additional-row').remove();
                        $('#note_date').html('Date:');
                        active_id = updated_data.prop_info[index].cc_id;
                        const val = $(this).val();
                        $('.owner-info-select').val(-1);
                        $(this).val(val);
                        displayInfo(updated_data.prop_info[index]);
                        console.log(updated_data.prop_info[index]);
                    }
                });

                if (true === buyer_type) {
                    $('#client-info-title').text('Buyer Information');
                    $('#name-placeholder-div').find('p').eq(0).text('Buyer Name');
                    $('#name-placeholder-div').find('p').eq(1).text('Buyer Title');
                    $('#name-placeholder-div').find('p').eq(2).text('Buyer Entity');
                    $('#info-Buyer').val($('#buyer-drop' + buyer_id).val()).trigger('change');
                } else {
                    $('#client-info-title').text('Client Information');
                    $('#name-placeholder-div').find('p').eq(0).text('Client Name');
                    $('#name-placeholder-div').find('p').eq(1).text('Title');
                    $('#name-placeholder-div').find('p').eq(2).text('Entity');
                }
            });
        }


        // Profile button popover event handler
        $('.profile-btn').on('shown.bs.popover', function () {

            // Fetch the info about client.
            const get_owner_popup_info = makeAjaxCall('homebase/get_owner_popup_info', {
                'owner_id': owner_id
            });

            // Append info to the appropriate textbox.
            get_owner_popup_info.then(function (data) {
                $.each(data.owner_info, function (index, info) {
                    $('textarea[data-type=' + info.hoi_type + ']').val(info.hoi_val);
                })
            });

            // Change of field event handler.
            $('.owner-popup-info').off('change');
            $('.owner-popup-info').change(function (data) {
                makeAjaxCall('homebase/save_owner_popup_info', {
                    'owner_id': owner_id,
                    'val': $(this).val(),
                    'type': $(this).attr('data-type')
                });
            })
        });

        // Remove data attribute.
        // $(event.relatedTarget).removeAttr('data-id');
        $('#edit-owner-profile-button').attr('data-id', owner_id);

        // Append data after fetching.
        get_owner_info.then(function (ret_data) {
            const owner_info_selector = $('#owner-info'),
                owner_contact_selector = $('#owner-contact'),
                data = ret_data.owner_info[0];

            // Check id Owner Avatar is set.
            if (null !== data.hc_avatar) {
                url = window.location.origin + '/tools/homebase/get_owner_image/?img=' + data.hc_avatar;
                $('#owner-avatar').attr('src', url);
            } else {
                $('#owner-avatar').attr('src', 'resources/app/media/user-avatar.svg');
            }

            // Append Owner Info.
            owner_info_selector.empty();
            owner_info_selector.append('<p style="font-size: 30px">' +
                ((null === data.hc_name || '' === data.hc_name) ? '' : data.hc_name) + '</p>');
            owner_info_selector.append('<p style="font-size: 20px"><b> ' +
                ((null === data.hc_title || '' === data.hc_title) ? 'Owner' : data.hc_title) + '</b></p>');
            owner_info_selector.append('<p style="font-size: 18px">' +
                ((null === data.hc_entity_name || '' === data.hc_entity_name) ? '' : data.hc_entity_name) + '</p>');
            owner_info_selector.append('<p>' + ((null === data.hc_entity_address || '' === data.hc_entity_address)
                ? '' : data.hc_entity_address) + '</p>');

            // Append Contact Info.
            owner_contact_selector.empty();
            owner_contact_selector.append('<p>' +
                ((null === data.hc_main_phone || '' === data.hc_main_phone) ? '' : data.hc_main_phone) + ' (Main)</p>');
            owner_contact_selector.append('<p>' +
                ((null === data.hc_second_phone || '' === data.hc_second_phone) ? '' : data.hc_second_phone) +
                ' (Second)</p>');
            owner_contact_selector.append('<p><a href="mailto:'
                + ((null === data.hc_client_email || '' === data.hc_client_email) ? '' : data.hc_client_email) + '">'
                + ((null === data.hc_client_email || '' === data.hc_client_email) ? '' : data.hc_client_email) +
                '</a></p>');
            owner_contact_selector.append('<p><a href="'
                + ((null === data.hc_website || '' === data.hc_website) ? '' : data.hc_website) + '">'
                + ((null === data.hc_website || '' === data.hc_website) ? '' : data.hc_website) + '</a></p>');
            owner_contact_selector.append('<p>'
                + ((null === data.hc_messaging || '' === data.hc_messaging) ? '' : data.hc_messaging) + '</p>');
            owner_contact_selector.append('<p>'
                + ((null === data.hc_social || '' === data.hc_social) ? '' : data.hc_social) + '</p>');

            const get_owner_asset_info = makeAjaxCall('homebase/get_owner_asset_info', {
                'owner_id': owner_id,
                'qtr': moment().quarter(),
                'year': moment().year()
            });

            get_owner_asset_info.then(function (data) {
                let deals = data.owner_asset_info,
                    merged_array = {};

                // Merge the Presentation Presented and Requested arrays into Possible.
                merged_array['proposed'] = $.merge(deals['requested'] ?
                    deals['requested'] : [], deals['presented'] ?
                    deals['presented'] : []);

                // Merge Contract hard and contact array into Predictable array.
                merged_array['complete'] = deals['complete'] ?
                    deals['complete'] : [];

                // Merge Deals Listed and Offers made array into Probable array.
                merged_array['contract'] = $.merge(deals['listed'] ?
                    deals['listed'] : [], deals['contract'] ?
                    deals['contract'] : []);

                append_owner_pics(merged_array);

                // $('#agent-track').attr('data-content', get_agent_track_record(deals['complete']));
            });
        });

        // Upload Owner Image Click handler.
        $(document).off('click', '#upload-owner-img');
        $(document).on('click', '#upload-owner-img', function () {
            $('#owner-img-upload').trigger('click');

            const target = 'owner-img-upload',
                file_type_allowed = /.\.(png|jpeg|jpg)/i,
                data = {
                    'owner_id': owner_id
                };

            // Upload Image file.
            const upload_image = uploadFile(target, data, file_type_allowed);
            upload_image.then(function (data) {
                $('#view-owner-profile-dialog').modal('hide');
                page_added = 1;
                refreshPage();
            });
        });

        let append_owner_pics = (merged_array) => {
                const yellow_rows_array = [
                    'maybe',
                    'completed',
                    'presented',
                    'hard',
                    'launched',
                    'accepted_co',
                    'complete_to',
                    'accepted_o',
                    'p_complete',
                    'receive_sp',
                    'deliver_su',
                    'offer_sa',
                    'ref_pla',
                    'ref_pa',
                    'ref_off'
                ];

                $.each(merged_array, function (index, deals) {
                    $('#owner-' + index + '-table').empty();

                    $.each(deals, function (key, deal) {
                        let class_to_append = '';
                        if (-1 !== $.inArray(deal.ds_status, yellow_rows_array)) {
                            class_to_append = 'yellow_row';
                        }

                        const built_year = (null !== deal.aai_built_year) ? deal.aai_built_year : "Enter Built Year:",
                            list_price = (null !== deal.aai_list_price) ? deal.aai_list_price : 'Enter LIST PRICE: ',
                            price_per_sf = (null !== deal.aai_price_per_sf) ? deal.aai_price_per_sf : 'Enter Price / SF: ',
                            price_per_unit = (null !== deal.aai_price_per_unit) ? deal.aai_price_per_unit : 'Enter Price / Units:',
                            units = (null !== deal.aai_units) ? deal.aai_units : 'Enter Units: ',
                            avg_sf = (null !== deal.aai_avg_sf) ? deal.aai_avg_sf : 'Enter Avg SF: ';

                        let img_src = 'https://cpd.powersellingtools.com/wp-content/uploads/2020/04/Default-Profile-Pic.png';
                        (null !== deal.hca_avatar) ?
                            img_src = window.location.origin +
                                '/tools/homebase/get_owner_image/?img='
                                + deal.hca_avatar : '';

                        $('#owner-' + index + '-table')
                            .append('<tr style="border: solid 1px #eee" data-lat="' + deal.hca_lat + '"' +
                                ' data-lng="' + deal.hca_lng + '" data-address="' + deal.hca_address + '"' +
                                ' data-id="' + deal.hca_id + '">' +
                                '<td>' +
                                '<img src="' + img_src + '"' +
                                ' id="owner-asset-avatar" class="owner-asset-image" style="width: 11vw"></td>' +
                                '<td style="text-align: left">' +
                                '<table style="border-collapse: separate; border-spacing: 0.9em 0.3em;' +
                                ' font-size: 15px; width: 100%"' +
                                ' class="' + class_to_append + '">' +
                                '<tr><td colspan="2" style="text-align: center"><b> STATUS: '
                                + deal.ds_status.toUpperCase() + '</b></td></tr>' +
                                '<tr>' +
                                '<td style="text-align: left">BUILT:</td>' +
                                '<td style="text-align: right"><span class="editable" id="aai_built_year' + deal.hca_id + '"' +
                                ' data-pk="' + deal.hca_id + '" data-name="aai_built_year"' +
                                ' data-url="/tools/common/update_agent_asset_info" data-type="text">' +
                                built_year + '</span></td>' +
                                '</tr>' +
                                '<tr>' +
                                '<td style="text-align: left">UNITS</td>' +
                                '<td style="text-align: right"><span class="editable" id="aai_units' + deal.hca_id + '"' +
                                ' data-pk="' + deal.hca_id + '" data-name="aai_units"' +
                                ' data-url="/tools/common/update_agent_asset_info" data-type="text">' + units +
                                '</span></td> ' +
                                '</tr>' +
                                '<tr>' +
                                '<td style="white-space: nowrap; text-align: left">PRICE / UNIT:</td>' +
                                '<td style="text-align: right; white-space: nowrap;">' +
                                '<span class="editable" id="aai_price_per_unit' + deal.hca_id + '"' +
                                ' data-pk="' + deal.hca_id + '" data-name="aai_price_per_unit"' +
                                ' data-url="/tools/common/update_agent_asset_info" data-type="text">' + price_per_unit +
                                '</span></td>' +
                                '</tr>' +
                                '<tr>' +
                                '<td style="text-align: left">PRICE / SF:</td>' +
                                '<td style="text-align: right"><span class="editable" id="aai_price_per_sf'
                                + deal.hca_id + '"' +
                                ' data-pk="' + deal.hca_id + '" data-name="aai_price_per_sf"' +
                                ' data-url="/tools/common/update_agent_asset_info" data-type="text">'
                                + price_per_sf + '</span></td>' +
                                '</tr>' +
                                '<tr>' +
                                '<td style="text-align: left">AVG SF:</td>' +
                                '<td style="text-align: right"><span class="editable" id="aai_avg_sg' + deal.hca_id + '"' +
                                ' data-pk="' + deal.hca_id + '" data-name="aai_avg_sf"' +
                                ' data-url="/tools/common/update_agent_asset_info" data-type="text">' + avg_sf +
                                '</span></td>' +
                                '</tr>' +
                                '<tr>' +
                                '<td style="text-align: left">LIST PRICE:</td>' +
                                '<td style="text-align: right"><span class="editable" id="aai_list_price' + deal.hca_id + '"' +
                                ' data-pk="' + deal.hca_id + '" data-name="aai_list_price"' +
                                ' data-url="/tools/common/update_agent_asset_info" data-type="text">'
                                + list_price + '</span></td>' +
                                '</tr>' +
                                '</table>' +
                                '</td>' +
                                '</tr>'
                            );
                    });
                });

                let table_name = ['proposed', 'contract', 'complete'];

                $.each(table_name, function (index, data) {
                    $('#owner-' + data + '-table')
                        .append('<tr><td colspan="2">' +
                            '<div style="border: dashed 3px #A4A4A4; padding: 8vh 0 8vh 0;' +
                            ' font-size: 20px; color: #818181">(Add Asset)</div></td></tr>')
                })
            },
            displayInfo = function (obj) {

                $('#client-info, #client-contact, #client-image-div').removeClass('hidden');
                $('.placeholder-div').addClass('hidden');

                $(document).on('change', '.hbi_input_data', function () {
                    makeAjaxCall('cpdv1/update_hbi_data', {
                        'col': $(this).attr('data-col'),
                        'val': $(this).val(),
                        'seq': $(this).attr('data-seq'),
                        'id': $(this).attr('data-id')
                    });
                });

                // Fetch the data for popover
                $('.client-info-btn').off('shown.bs.popover');
                $('.client-info-btn').on('shown.bs.popover', function () {
                    const get_popup_info = makeAjaxCall('cpdv1/get_client_popup_info', {
                        'id': obj.cc_id,
                        'form-type': 'client'
                    });

                    get_popup_info.then(function (data) {
                        $.each(data.info, function (index, info) {
                            $('textarea[data-type=' + info.cdai_type + ']').val(info.cdai_val);
                        })
                    })
                });

                $('.job-field').change(function () {
                    const update_job_field = makeAjaxCall('cpdv1/update_job_field', {
                        val: $(this).val(),
                        id: obj.cc_id,
                        col: $(this).attr('data-col')
                    });

                    update_job_field.then(function () {
                        console.log('here');
                    })
                });

                // Change of field event handler.
                $(document).off('change', '.client-popup-info');
                $(document).on('change', '.client-popup-info', function (data) {
                    makeAjaxCall('cpdv1/save_client_popup_info', {
                        'id': obj.cc_id,
                        'val': $(this).val(),
                        'type': $(this).attr('data-type'),
                        'form-type': 'client'
                    });
                });

                $('#client-image-div').css('visibility', 'visible');

                // Set Job title and description value.
                $('#job-title').val(obj.cc_job_title);
                $('#job-desc').text(obj.cc_job_desc);

                // Upload Image functionality.
                $(document).off('click', '#upload-info-img');
                $(document).on('click', '#upload-info-img', function () {
                    $('#info-img-upload').trigger('click');
                    const target = 'info-img-upload',
                        file_type_allowed = /.\.(png|jpeg|jpg)/i,
                        data = {
                            'cc_id': obj.cc_id
                        };

                    // Upload a Document file.
                    const upload_image = uploadFile(target, data, file_type_allowed);
                    upload_image.then(function (data) {
                        page_added = 1;
                        $('#client-info-dialog').modal('hide');
                        refreshPage();
                    });
                });

                const client_info_selector = $('#owner-info'),
                    client_contact_selector = $('#owner-contact');

                if (null !== obj.cc_image) {
                    url = window.location.origin + '/tools/cpdv1/get_info_image/?img=' + obj.cc_image;
                    $('#info-avatar').attr('src', url);
                } else {
                    $('#info-avatar').attr('src', '/tools/resources/app/media/user-avatar.svg');
                }

                client_info_selector.empty();
                client_info_selector.append('<p style="font-size: 30px">' + obj.cc_name + '</p>');
                client_info_selector.append('<p style="font-size: 20px"><b>' + (
                    null === obj.cc_title ? '' : obj.cc_title) + '</b></p>');
                // client_info_selector.append('<p style="font-size: 20px"><b>' + obj.cc_type + '</b></p>');
                client_info_selector.append('<p style="font-size: 18px">' + obj.cc_entity + '</p>');
                client_info_selector.append('<p>' + obj.cc_address + '</p>');

                client_contact_selector.empty();
                client_contact_selector.append('<p>' + obj.cc_office_phone + ' Office</p>');
                client_contact_selector.append('<p>' + obj.cc_cell_phone + ' Cell</p>');
                client_contact_selector.append('<p><a href="mailto:' + obj.cc_email + '">' + obj.cc_email + '</a></p>');
                client_contact_selector.append('<p><a href="' + obj.cc_website + '">' + obj.cc_website + '</a></p>');
                client_contact_selector.append('<p>' + obj.cc_messaging + '</p>');
                client_contact_selector.append('<p>' + obj.cc_social + '</p>');

                $.each(obj, function (index, info) {
                    const info_popup_selector = $('#' + index.replace('cc_', ''));
                    info_popup_selector.val(info);
                    info_popup_selector.attr('data-id', obj.cc_id);
                    info_popup_selector.attr('data-col', index);
                });

                if (obj.cc_note_date != null) {
                    $('#note-date').text(obj.cc_note_date);
                    $('#owner-info-notes').text(obj.cc_notes);
                }

                // Check if the info is for a Owner.
                if (obj.cc_type == 'Partner') {

                    $('#owner-buttons-div, #owner-info-div, #owner-info-buyer-div, #add-counter-offer').removeClass('hidden');
                    $('#agent-info-row, #add-buyer-offer, #buyer-buttons-div, #add-retrade-offer,' +
                        ' #owner-profile-buttons-div').addClass('hidden');

                    // Fetch info about asset.
                    const get_asset_info = makeAjaxCall('homebase/get_asset_data', {
                        'asset_id': client_id
                    });

                    let client = '';
                    get_asset_info.then(function (data) {
                        // Remove owner info.
                        $('#owner-info-asset-table table').remove();

                        let portfolio = data.portfolio;
                        client = data.client[0];
                        append_owner_asset_head(client, 1, 'owner-info-asset-table');
                        append_owner_asset_body(client, 'add-', 1, assets_info, leads, sources, portfolio);

                        $('#owner-info-asset-table').find('button, input, select, a').attr('disabled', 'disabled');

                        // Check if the deal status is hard or in contract.
                        if ('hard' === client.hca_status || 'contract' === client.hca_status) {
                            $('#owner-info-buyer-div').addClass('hidden');

                        }
                    });

                    // Append Deal Types and Criteria to deal type and criteria select box.
                    let request = [],
                        products = '';
                    const get_deal = get_all_deal_type();
                    request.push(get_deal);

                    // Get all criteria and location.
                    const get_criteria = get_all_criteria();
                    request.push(get_criteria);

                    // Fetch all the products from DB.
                    const get_all_homebase_products = makeAjaxCall('industry/get_all_products');

                    get_all_homebase_products.then(function (data) {
                        products = data.products;
                    });

                    request.push(get_all_homebase_products);

                    Promise.all(request).then(function (data) {
                        appendOwnerProfile(client, products);

                    });
                } else if (obj.cc_type === 'Buyer') {

                    $('#buyer-buttons-div, #owner-info-div, #add-buyer-offer, #add-retrade-offer, #owner-info-buyer-div')
                        .removeClass('hidden');
                    $('#owner-buttons-div, #agent-info-row, #add-counter-offer, #owner-profile-buttons-div').addClass('hidden');

                    // Fetch info about asset.
                    const get_asset_info = makeAjaxCall('homebase/get_asset_data', {
                        'asset_id': client_id
                    });

                    let client = '';
                    get_asset_info.then(function (data) {
                        // Remove owner info.
                        $('#owner-info-asset-table table').remove();

                        let portfolio = data.portfolio;
                        client = data.client[0];
                        append_owner_asset_head(client, 1, 'owner-info-asset-table');
                        append_owner_asset_body(client, 'add-', 1, assets_info, leads, sources, portfolio);

                        $('#owner-info-asset-table').find('button, input, select, a').attr('disabled', 'disabled');
                    });
                }
                else {
                    $('#owner-buttons-div, #owner-info-div, #add-buyer-offer, #owner-info-buyer-div,' +
                        ' #buyer-buttons-div, #add-retrade-offer, #add-counter-offer, #owner-profile-buttons-div').addClass('hidden');
                    $('#agent-info-row').removeClass('hidden');
                }

                // Add new Buyer/Offer click handler.
                $('.add-info-offer').off('click');
                $('.add-info-offer').click(function () {
                    let dummy_content = $('#buyer-offer-div').clone();
                    dummy_content.removeAttr('id');
                    dummy_content.removeClass('hidden');

                    $('#owner-info-buyer-div').append(dummy_content);

                    const add_buyer_offer = makeAjaxCall('cpdv1/add_buyer_offer', {
                        id: obj.cc_deal_id,
                        date: moment().format(" DD-MM-YY hh:mm A"),
                        cc_id: obj.cc_id,
                        seq: $('#owner-info-buyer-div').find('> div').length,
                        type: $(this).attr('data-type')
                    })

                    add_buyer_offer.then(function (data) {
                        console.log(data);
                    })
                });

                usePopover();
                additionalInfoEvent();

                $('#add-info-row').attr('data-id', obj.cc_id);
            };


        // Pics button click handler.
        $(document).on('click', '#owner-asset-pics', function () {
            $('#enable-poi, #disable-poi').addClass('hidden');
            $('#owner-pics-div').removeClass('hidden');
            $('#owners-map-div').addClass('hidden');
            $('#owners-info-div').addClass('hidden');
        });

        // Info button click handler.
        $(document).on('click', '#owner-asset-info', function () {
            $('#enable-poi, #disable-poi').addClass('hidden');
            $('#owner-pics-div').addClass('hidden');
            $('#owners-map-div').addClass('hidden');
            $('#owners-info-div').removeClass('hidden');
            get_owner_asset_info();
        });

        // Show Agent Owner Map.
        $(document).on('click', '#owner-asset-map', function () {

            $('#owner-pics-div').addClass('hidden');
            $('#owners-info-div').addClass('hidden');
            $('#owners-map-div').removeClass('hidden');

            let markersOnMap = [],
                count = $('.owner-asset-active').length;

            for (let i = 0; i < $('.owner-asset-active').length; i++) {

                let asset = $('.owner-asset-active').eq(i),
                    lat = $(asset).attr('data-lat'),
                    lng = $(asset).attr('data-lat'),
                    address = $(asset).attr('data-address');

                if ((lat === 'null' || lng === 'null') && address === 'null') {
                    toastr.warning('Please update asset address field.', 'Asset Address is not present.')
                    count--;
                } else {
                    markersOnMap.push({
                        address: $(asset).attr('data-address'),
                        id: $(asset).attr('data-id'),
                        LatLng: [{
                            'lat': lat,
                            'lng': lng
                        }]
                    });
                }
            }


            const coordinate = getLongLat(markersOnMap[0].address);
            coordinate.then(function (data) {
                let lat = data.results[0].geometry.location.lat,
                    lng = data.results[0].geometry.location.lng;

                let asset_address = new google.maps.LatLng(lat, lng),
                    map = new google.maps.Map(document.getElementById('owners-map-div'), {
                        zoom: 15,
                        center: asset_address
                    }),
                    customStyled = [{
                        featureType: "poi",
                        stylers: [
                            {visibility: "off"}
                        ]
                    }],

                    initialContentString = '<table><tr>' + $('.owner-asset-active').eq(0).html() + '</tr></table>',

                    // Set up Popover with asset info.
                    infowindow = new google.maps.InfoWindow(),

                    marker = new google.maps.Marker({
                        position: asset_address,
                        map: map,
                        title: 'Owner Information'
                    });
                // Set Styling for map.
                map.set('styles', customStyled);

                // Add Owner info popover to map marker.
                google.maps.event.addListener(marker, 'click', (function(marker) {
                    return function() {
                        infowindow.setContent(initialContentString);
                        infowindow.open(map, marker);
                    }
                })(marker));

                $('#enable-poi').removeClass('hidden');

                // Show Points Of interest.
                $('#enable-poi').click(function () {
                    $('#enable-poi').addClass('hidden');
                    $('#disable-poi').removeClass('hidden');

                    let customStyle = [{
                        featureType: "poi",
                        stylers: [
                            {visibility: "on"}
                        ]
                    }];

                    map.set('styles', customStyle);
                });

                // Hide Points of Interest.
                $('#disable-poi').click(function () {
                    $('#enable-poi').removeClass('hidden');
                    $('#disable-poi').addClass('hidden');

                    let customStyle = [{
                        featureType: "poi",
                        stylers: [
                            {visibility: "off"}
                        ]
                    }];

                    map.set('styles', customStyle);
                });

                infowindow.open(map, marker);
                for (i = 1; i < markersOnMap.length; i++) {
                    let contentString = '<table><tr>' + $('.owner-asset-active').eq(i).html() + '</tr></table>';

                    if ('null' === markersOnMap[i].LatLng[0].lat) {
                        const coordinate = getLongLat(markersOnMap[i].address);
                        coordinate.then(function (data) {
                            let lat = data.results[0].geometry.location.lat,
                                lng = data.results[0].geometry.location.lng;

                            marker = new google.maps.Marker({
                                position: new google.maps.LatLng(lat, lng),
                                map: map
                            });

                            // Add Owner info popover to map marker.
                            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                                return function() {
                                    infowindow.setContent(contentString);
                                    infowindow.open(map, marker);
                                }
                            })(marker, i));
                        });
                    } else {
                        marker = new google.maps.Marker({
                            position: new google.maps.LatLng(markersOnMap[i].LatLng[0].lat, markersOnMap[i].LatLng[0].lng),
                            map: map
                        });

                        // Add Owner info popover to map marker.
                        google.maps.event.addListener(marker, 'click', (function(marker, i) {
                            return function() {
                                infowindow.setContent(contentString);
                                infowindow.open(map, marker);
                            }
                        })(marker, i));
                    }
                }
            });


            // $.when($('.owner-asset-active').each(async function (index, asset) {
            //     let lat = $(asset).attr('data-lat'),
            //         lng = $(asset).attr('data-lat'),
            //         address = $(asset).attr('data-address');
            //
            //     if ((lat === 'null' || lng === 'null') && address !== 'null') {
            //         const cordinate = getLongLat(address);
            //         cordinate.then(async function (data) {
            //             lat = data.results[0].geometry.location.lat;
            //             lng = data.results[0].geometry.location.lng;
            //
            //             await markersOnMap.push(
            //                 {
            //                     id: $(asset).attr('data-id'),
            //                     LatLng: [{
            //                         'lat': lat,
            //                         'lng': lng
            //                     }]
            //                 })
            //             count--;
            //         });
            //
            //     } else if ((lat === 'null' || lng === 'null') && address === 'null') {
            //         toastr.warning('Please update asset address field.', 'Asset Address is not present.')
            //         count--;
            //     } else {
            //         await markersOnMap.push({
            //             id: $(asset).attr('data-id'),
            //             LatLng: [{
            //                 'lat': lat,
            //                 'lng': lng
            //             }]
            //         });
            //     }
            // })).then(function () {
            //     console.log(markersOnMap);

            // });

            // console.log(markersOnMap);
            // console.log(markersOnMap[1]);
            //
            // console.log('here');

            // show_marker(markersOnMap);


            // addMarker();
            // let lat = $('.owner-asset-active').attr('data-lat'),
            //     lng = $('.owner-asset-active').attr('data-lng'),
            //     address = $('.owner-asset-active').attr('data-address'),
            //     map;
            //

            //
            // function addMarkerInfo() {
            //     for (var i = 0; i < markersOnMap.length; i++) {
            //         var contentString = '<table><tr>' +
            //          $('.owner-asset-active[data-id="' + markersOnMap[i].id + '"]').html() + '</tr></table>';
            //
            //         // console.log(new google.maps.LatLng(markersOnMap[i].LatLng[0]));
            //         // console.log(markersOnMap[i].LatLng[0]);
            //         //
            //         console.log('here');
            //         console.log(markersOnMap[i].LatLng[0]);
            //         const marker = new google.maps.Marker({
            //             position: markersOnMap[i].LatLng[0],
            //             map: map
            //         });
            //
            //         const infowindow = new google.maps.InfoWindow({
            //             content: contentString,
            //             maxWidth: 200
            //         });
            //
            //         // marker.addListener('click', function () {
            //         //     closeOtherInfo();
            //         //     infowindow.open(marker.get('map'), marker);
            //         //     InforObj[0] = infowindow;
            //         // });
            //         // marker.addListener('mouseover', function () {
            //         //     closeOtherInfo();
            //         //     infowindow.open(marker.get('map'), marker);
            //         //     InforObj[0] = infowindow;
            //         // });
            //         // marker.addListener('mouseout', function () {
            //         //     closeOtherInfo();
            //         //     infowindow.close();
            //         //     InforObj[0] = infowindow;
            //         // });
            //     }
            // }
            //
            //
            //
            //
            // if ((lat === 'null' || lng === 'null') && address !== 'null') {
            //     const cordinate = getLongLat(address);
            //     cordinate.then(function (data) {
            //         lat = data.results[0].geometry.location.lat;
            //         lng = data.results[0].geometry.location.lng;
            //         initMap();
            //     });
            // } else if ((lat === 'null' || lng === 'null') && address === 'null') {
            //     toastr.warning('Please update asset address field.', 'Asset Address is not present.')
            // } else {
            //     initMap();
            // }
            //
            // // initMap();
            // // Initialize Map.
            // function initMap() {
            //
            //     let asset_address = new google.maps.LatLng(lat, lng),
            //         map = new google.maps.Map(document.getElementById('owners-map-div'), {
            //             zoom: 15,
            //             center: asset_address
            //         }),
            //         customStyled = [{
            //             featureType: "poi",
            //             stylers: [
            //                 {visibility: "off"}
            //             ]
            //         }],
            //
            //         contentString = '<table><tr>' + $('.owner-asset-active').html() + '</tr></table>',
            //
            //         // Set up Popover with asset info.
            //         infowindow = new google.maps.InfoWindow({
            //             content: contentString
            //         }),
            //
            //         marker = new google.maps.Marker({
            //             position: asset_address,
            //             map: map,
            //             title: 'Owner Information'
            //         });
            //
            //     console.log(asset_address);
            //     // Set Styling for map.
            //     map.set('styles', customStyled);
            //     $('#enable-poi').removeClass('hidden');
            //
            //     // Show Points Of interest.
            //     $('#enable-poi').click(function () {
            //         $('#enable-poi').addClass('hidden');
            //         $('#disable-poi').removeClass('hidden');
            //
            //         let customStyle = [{
            //             featureType: "poi",
            //             stylers: [
            //                 {visibility: "on"}
            //             ]
            //         }];
            //
            //         map.set('styles', customStyle);
            //     });
            //
            //     // Hide Points of Interest.
            //     $('#disable-poi').click(function () {
            //         $('#enable-poi').removeClass('hidden');
            //         $('#disable-poi').addClass('hidden');
            //
            //         let customStyle = [{
            //             featureType: "poi",
            //             stylers: [
            //                 {visibility: "off"}
            //             ]
            //         }];
            //
            //         map.set('styles', customStyle);
            //     });
            //
            //     // Add Owner info popover to map marker.
            //     marker.addListener('click', function () {
            //         infowindow.open(map, marker);
            //     });
            //     addMarkerInfo();
            //     infowindow.open(map, marker);
            // }
        });
    });

    // Agent Asset Input change handler.
    $(document).on('change', '.owner-asset-input', function () {
        makeAjaxCall('common/update_agent_info', {
            'col': $(this).attr('data-col'),
            'table': $('#owner-profile-nav li.active').attr('data-table'),
            'id': $('.owner-asset-active').attr('data-id'),
            'val': $(this).val(),
            'key': $('#owner-profile-nav li.active').attr('data-key')
        });
    })
});

function show_marker(markersOnMap) {
    console.log(markersOnMap.length);
}

// function addMarker() {
//
//     get_asset_pics().then(function (markersOnMap) {
//
//         console.log(markersOnMap.length)
//         // console.log(markersOnMap);
//         var map = new google.maps.Map(document.getElementById('owners-map-div'), {
//             zoom: 10,
//             center: new google.maps.LatLng(-33.92, 151.25),
//             // mapTypeId: google.maps.MapTypeId.ROADMAP
//         });
//
//         var infowindow = new google.maps.InfoWindow();
//
//         var marker, i;
//
//         for (i = 0; i < markersOnMap.length; i++) {
//             marker = new google.maps.Marker({
//                 position: new google.maps.LatLng(markersOnMap[i].LatLng[0]),
//                 map: map
//             });
//
//             var contentString = '<table><tr>' +
//                 $('.owner-asset-active[data-id="' + markersOnMap[i].id + '"]').html() + '</tr></table>';
//
//             google.maps.event.addListener(marker, 'click', (function (marker, i) {
//                 return function () {
//                     infowindow.setContent(contentString);
//                     infowindow.open(map, marker);
//                 }
//             })(marker, i));
//         }
//     });
// }
//
// // function get_all_products() {
// //     try {
// //         // Create a new promise object.
// //         var product_promise = new Promise(function (resolve, reject) {
// //             const get_products = makeAjaxCall('industry/get_all_products');
// //
// //             get_products.then(function (data) {
// //                 resolve(data.products);
// //                 return;
// //             }).catch(e => reject(e));
// //         });
// //     } catch (e) {
// //         return false;
// //     }
// //     return product_promise;
// // }
//
//
// function get_asset_pics() {
//     try {

//         var pics_promise = new Promise(function (resolve, reject) {
//             var markersOnMap = [];
//             $('.owner-asset-active').each(async function (index, asset) {
//                 let lat = $(asset).attr('data-lat'),
//                     lng = $(asset).attr('data-lat'),
//                     address = $(asset).attr('data-address');
//
//                 if ((lat === 'null' || lng === 'null') && address !== 'null') {
//                     const cordinate = getLongLat(address);
//                     cordinate.then(async function (data) {
//                         lat = data.results[0].geometry.location.lat;
//                         lng = data.results[0].geometry.location.lng;
//
//                         await markersOnMap.push(
//                             {
//                                 id: $(asset).attr('data-id'),
//                                 LatLng: [{
//                                     'lat': lat,
//                                     'lng': lng
//                                 }]
//                             })
//                     });
//
//                 } else if ((lat === 'null' || lng === 'null') && address === 'null') {
//                     toastr.warning('Please update asset address field.', 'Asset Address is not present.')
//                 } else {
//                    await markersOnMap.push({
//                         id: $(asset).attr('data-id'),
//                         LatLng: [{
//                             'lat': lat,
//                             'lng': lng
//                         }]
//                     })
//                 }
//             });
//
//             resolve(markersOnMap);
//         });
//     } catch (e) {
//         return false;
//     }
//     return pics_promise;
// }