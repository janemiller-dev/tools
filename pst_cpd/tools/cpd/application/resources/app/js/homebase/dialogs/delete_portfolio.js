$(document).ready(function () {
    var portfolio_id = '';
    var ui_name = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-portfolio-dialog').on('show.bs.modal', function (event) {
        portfolio_id = $(event.relatedTarget).data('id');
        ui_name = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name')

        $('#portfolio-name').html(ui_name);
        $('#view-portfolio-dialog').modal('hide');
    });

    $(document).on('click', '#close-delete-portfolio-dialog', function () {
        $('#delete-portfolio-dialog').modal('hide');
    });

    // Deletes the portfolio name.
    $(document).on('submit', '#delete-portfolio-form', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $.ajax({
            url: jsglobals.base_url + 'homebase/delete_portfolio',
            dataType: 'json',
            type: 'post',
            data: {
                portfolio_id: portfolio_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            toastr.success('Portfolio Name Deleted', 'Success');
            $('#delete-portfolio-dialog').modal('hide');
            refreshDashboard();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
        return false;
    });
});
