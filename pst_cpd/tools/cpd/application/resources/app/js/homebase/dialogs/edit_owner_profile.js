$(document).ready(function () {

    // When the dialog is displayed, set the current instance ID.
    $('#edit-owner-profile-dialog').on('show.bs.modal', function (event) {
        $('#view-owner-profile-dialog').modal('hide');

        const client_id = $(event.relatedTarget).attr('data-id'),
            get_owner_info = makeAjaxCall('homebase/get_owner_info', {'owner_id': client_id});

        get_owner_info.then(function (data) {
            const owner_data = data.owner_info[0];

            // Set input fields values.
            $('#owner-id').val(owner_data.hc_id);
            $('#owner-name').val(owner_data.hc_name);
            $('#owner-title').val(owner_data.hc_title);
            $('#entity-name').val(owner_data.hc_entity_name);
            $('#owner-address').val(owner_data.hc_entity_address);
            $('#owner-office-phone').val(owner_data.hc_main_phone);
            $('#owner-cell-phone').val(owner_data.hc_second_phone);
            $('#owner-email').val(owner_data.hc_client_email);
            $('#owner-website').val(owner_data.hc_website);
            $('#owner-messaging').val(owner_data.hc_messaging);
            $('#owner-social-media').val(owner_data.hc_social);
        })
    });

    $(document).on('click', '#save-owner-info', function () {
        const save_owner_info = makeAjaxCall('homebase/save_owner_info',
            $("#edit-owner-profile-form").serialize()
        );

        save_owner_info.then(function (data) {
            $('#edit-owner-profile-dialog').modal('hide');
            toastr.success('Owner Information Updated.', 'Success!!');
        })
    });
});
