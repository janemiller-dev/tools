$(document).ready(function() {

    // Get the game types and start dates.
    $.ajax({
	url: '/wap/get_types',
	dataType: 'json',
	type: 'post',
    }).done(function(data) {
	if (data.status != 'success') {
	    toastr.error(data.message);
	    return;
	}
        else if (data.status == 'redirect') {
	    window.location.href = data.redirect;
	    return;
        }
        updateModelTypes(data.types);
    }).fail(function(jqXHR, status) {
	toastr.error("Server communication error. Please try again.");
    }).always(function() {
    });

    // Validate the add model form and submit it if it is valid.
    $('#add-model-form').formValidation({
	framework: 'bootstrap',
	icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
	},
	fields: {
	    wap_type_id: {
		validators: {
		    greaterThan: {
			value: 0,
			inclusive: false,
			message: "The type is required."
		    }
		}
	    },
	    name: {
		validators: {
		    notEmpty: {
			message: "The model name is required."
		    }
		}
	    },
	    description: {
		validators: {
		    notEmpty: {
			message: "The model description is required."
		    }
		}
	    }
	}
    }).on('success.form.fv', function(e) {
	e.preventDefault();

	// Get the form model
	var $form = $(e.target);
	var fv = $form.data('formValidation');

	// Get the form data and submit it.
	var message_data = $form.serialize();
	$.ajax({
	    url: '/wap/add_model',
	    dataType: 'json',
	    type: 'post',
	    data: message_data
	}).done(function(data) {
	    if (data.status != 'success') {
		toastr.error(data.message);
		return;
	    }
            else if (data.status == 'redirect') {
		window.location.href = data.redirect;
		return;
            }
	    $('#add-model-dialog').modal('hide');
	    fv.resetForm();

	    refreshPage();
	}).fail(function(jqXHR, status) {
	    toastr.error("Server communication error. Please try again.");
        }).always(function() {
        });
    });
});

// Update the game type select options.
function updateModelTypes(types) {

    // Clear the select list.
    $('#wap-model-type-id').empty();

    // Show each game type and update the game type periods table.
    $('#wap-model-type-id').append($('<option>')
				   .attr({value: 0})
				   .append("Select Activity Type"));
    $.each(types, function (t, type) {
	$('#wap-model-type-id').append($('<option>')
				       .attr({value: type.wap_type_id})
				       .append(type.wap_type_name));
    });
}
