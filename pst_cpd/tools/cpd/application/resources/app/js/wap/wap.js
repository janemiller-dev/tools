$(document).ready(function() {
    refreshPage();
});

// Get the page data and display it.
function refreshPage() {

    // Get the ID from the URL
    var path = window.location.pathname;
    var components = path.split('/');
    var id = components[components.length - 1];

    // Get the subscription status.
    $.ajax({
        url: "/wap/get_wap",
        dataType: "json",
        type: 'post',
	data: {
	    id: id
	}
    }).done(function(data) {
        if (data.status == 'failure') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
	    window.location.href = data.redirect;
            return;
        }
        updateWAP(data.wap);

    }).fail(function(jqXHR, status) {
        toastr.error("Error");
    });
}

// Update the WAP
function updateWAP(wap) {

    // Update the title and quarter.
    $('#wap-name').html(wap.wap_ui_name).addClass('editable')
	.attr({
	    'data-url': 'wap/update_instance',
	    'data-name': 'wap_ui_name',
	    'data-pk': wap.wap_ui_id,
	    'data-type': 'text',
	    'title': 'New Name',
	});

    // Draw the WAP column headers
    $('#wap-table thead.main-table').empty();
    $('#wap-table thead.main-table').append($('<tr>')
					    .append($('<th class="text-center">')
						    .append('')));

    // Determine the number of periods to display.
    var num_periods = wap.wap_ui_periods;
    
    // Draw the period date headers.
    var date = moment(wap.wap_ui_quarter);
    date.startOf('week').add(1, 'day');
    for (var period = 1; period <= num_periods; period++) {

	// The header cell contents includes the Quarter, Date, and all multipliers.
	var col_header = 'Week of<br/><span style="font-size: 1.3em">' + date.format('MMM Do') + '<br />';

	// Show the header
	$('#wap-table thead.main-table tr').append($('<th class="text-center">')
						   .append(col_header));

	// Increment to the next quarter.
	date.add(1, 'week');
    }

    // Create the rows and columns for each condition.
    $('#wap-table tbody.main-table').empty();
    var row = 1;
    $.each(wap.metrics, function(c, metric) {

	// Each metric label contains the metric name and the converstion rate.
	var metric_value = '<span class="metric-name">' + metric.wap_m_name + '</span><br />';

	if (metric.wap_m_has_rate !== '0') {
	    metric_value += 'Conversion Rate: <span class="editable" data-url="/wap/update_metric" data-name="wap_im_conversion_rate" data-pk="' + wap.wap_ui_id + '-' + metric.wap_m_id + '" data-type="number" data-title="Update Conversion Rate">' + numeral(metric.wap_im_conversion_rate).format('0%') + '</span>';
	}
	
	$('#wap-table tbody.main-table')
	    .append($('<tr id="metric-' + metric.wap_m_id + '">')
		    .append($('<td class="text-center">')
			    .append(metric_value)));

	// Create a cell for each metric's period and display the cell contents.
	var editable;
	for (var period = 1; period <= num_periods; period++) {
	    var id = metric.wap_m_id + '-' + period;
	    $('#wap-table tbody.main-table tr#metric-' + metric.wap_m_id)
		.append($('<td class="text-center wap-cell cell-' + row + '-' + period + ' col-' + period + '" id="cell-' + id + '">'));

	    // Is the cell fully editable?
	    if (metric.profession_m_prev_metric == null)
		editable = true;
	    else
		editable = false;

	    // Display the cell
	    displayCell(id, wap.cells, period, metric, wap.wap_ui_id, editable);
	}
	row += 1;
    });

    // Link the editible classes to the editable plugin.
    setEditable();
}

// Display the specified cell.
function displayCell(id, cells, period, metric, ui_id, editable) {

    // Get the metric ID from the metric.
    var metric_id = metric.wap_m_id;

    // Get the Unit of Measurement and set the cell's class appropriately.
    var format = '0,0.00';

    // Create the cell
    $('#cell-' + id).append($('<table class="table table-condensed cell-table" id="table-' + id + '">')
			    .append($('<thead>')
				    .append($('<tr>')
					    .append($('<th class="text-center">')
						    .append('FTG'))
					    .append($('<th class="text-center">')
						    .append($('<span class="has-popover glyphicon glyphicon-info-sign" style="float: right" data-toggle="popover" data-placement="top" data-html="true" title="Weekly ' + metric.wap_m_name + '" data-content="TBD">'))
						    .append('#')
						   )))
			    .append($('<tbody>')
				    .append($('<tr>')
					    .append($('<td class="shaded">')
						    .append('F'))
					    .append($('<td id="floor-' + id + '">')
						    .append(numeral(0).format(format))))
				    .append($('<tr>')
					    .append($('<td class="shaded">')
						    .append('T'))
					    .append($('<td id="target-' + id + '">')
						    .append(numeral(0).format(format))))
				    .append($('<tr>')
					    .append($('<td class="shaded">')
						    .append('G'))
					    .append($('<td id="game-' + id + '">')
						    .append(numeral(0).format(format)))))
			    .append($('<tfoot>')
				    .append($('<tr>')
					    .append($('<td class="shaded">')
						    .append('Actual'))
					    .append($('<td id="actual-' + id + '">')
						    .append(numeral(0).format(format)))))
			   );

    // Add the editable class and attributes for editable cells.  The Actual is always editable.
    $('#actual-' + id).addClass('editable')
	.attr({
	    'data-url': 'wap/update_cell',
	    'data-name': 'wap_ic_actual',
	    'data-pk': ui_id + '-' + metric.wap_m_id + '-' + period,
	    'data-type': 'number',
	    'title': 'Set Actual',
	});

    // If the cell is editable, make all of the values editable.
    if (editable) {
	$('#floor-' + id).addClass('editable')
	    .attr({
		'data-url': 'wap/update_cell',
		'data-name': 'wap_ic_floor',
		'data-pk': ui_id + '-' + metric.wap_m_id + '-' + period,
		'data-type': 'number',
		'title': 'Set Floor',
	    });
	$('#target-' + id).addClass('editable')
	    .attr({
		'data-url': 'wap/update_cell',
		'data-name': 'wap_ic_target',
		'data-pk': ui_id + '-' + metric.wap_m_id + '-' + period,
		'data-type': 'number',
		'title': 'Set Target',
	    });
	$('#game-' + id).addClass('editable')
	    .attr({
		'data-url': 'wap/update_cell',
		'data-name': 'wap_ic_game',
		'data-pk': ui_id + '-' + metric.wap_m_id + '-' + period,
		'data-type': 'number',
		'title': 'Set Game',
	    });
    }
    
    // Do we have any values for the cell?
    if (typeof cells[period] != 'undefined' && typeof cells[period][metric_id] !== 'undefined') {
	var cell = cells[period][metric_id];

	$('#floor-' + id).html(numeral(cell.wap_ic_floor).format(format));
	$('#game-' + id).html(numeral(cell.wap_ic_game).format(format));
	$('#target-' + id).html(numeral(cell.wap_ic_target).format(format));
	$('#actual-' + id).html(numeral(cell.wap_ic_actual).format(format));
    }
}
