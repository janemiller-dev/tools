$(document).ready(function() {

    // Get the game types and start dates.
    $.ajax({
	url: '/wap/get_types',
	dataType: 'json',
	type: 'post',
    }).done(function(data) {
	if (data.status != 'success') {
	    toastr.error(data.message);
	    return;
	}
        else if (data.status == 'redirect') {
	    window.location.href = data.redirect;
	    return;
        }
        updateTypes(data.types);
    }).fail(function(jqXHR, status) {
	toastr.error("Server communication error. Please try again.");
    }).always(function() {
    });

    // Update the possible start dates based on the selected game type.
    $('#wap-type-id').change(function() {

	// Only update the select if the game type period has changed.
	var new_type_id = $('#wap-type-id').val();
    });

    // Validate the add instance form and submit it if it is valid.
    $('#add-instance-form').formValidation({
	framework: 'bootstrap',
	icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
	},
	fields: {
	    wap_type_id: {
		validators: {
		    greaterThan: {
			value: 0,
			inclusive: false,
			message: "The type is required."
		    }
		}
	    },
	    name: {
		validators: {
		    notEmpty: {
			message: "The instance name is required."
		    }
		}
	    }
	}
    }).on('success.form.fv', function(e) {
	e.preventDefault();

	// Get the form instance
	var $form = $(e.target);
	var fv = $form.data('formValidation');

	// Get the form data and submit it.
	var message_data = $form.serialize();
	$.ajax({
	    url: '/wap/add_instance',
	    dataType: 'json',
	    type: 'post',
	    data: message_data
	}).done(function(data) {
	    if (data.status != 'success') {
		toastr.error(data.message);
		return;
	    }
            else if (data.status == 'redirect') {
		window.location.href = data.redirect;
		return;
            }
	    $('#add-instance-dialog').modal('hide');
	    fv.resetForm();

	    refreshPage();
	}).fail(function(jqXHR, status) {
	    toastr.error("Server communication error. Please try again.");
        }).always(function() {
        });
    });

    // Initialize the quarter dropdown.
    updateQuarters();
});

// Set the possible start dates based on the game type period.
function updateQuarters() {

    // Clear the old options.
    $('#wap-ui-quarter').empty();

    // Allow the current year and the next year.
    var date = moment();

    // Only 2 years allowed.
    for (var i = 0; i < 5; i++) {
	var quarter = 'Q' + date.quarter() + ': ' + date.endOf('quarter').format('MMM DD, YYYY');

	$('#wap-ui-quarter').append($('<option>')
				   .attr({value: date.startOf('quarter').format('YYYY-MM-DD')})
				   .append(quarter));
	date.add(1, 'quarter');
    }
}

// Update the game type select options.
function updateTypes(types) {

    // Clear the select list.
    $('#wap-type-id').empty();

    // Show each game type and update the game type periods table.
    $('#wap-type-id').append($('<option>')
			   .attr({value: 0})
			   .append("Select Game Type"));
    $.each(types, function (t, type) {
	$('#wap-type-id').append($('<option>')
			       .attr({value: type.wap_type_id})
			       .append(type.wap_type_name));
    });

}
