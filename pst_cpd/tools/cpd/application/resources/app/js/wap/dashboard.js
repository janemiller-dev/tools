$(document).ready(function() {
    refreshPage();
});

// Get the page data and display it.
function refreshPage() {

    // Get the list of instances status.
    $.ajax({
        url: "/wap/get_instances",
        dataType: "json",
        type: 'post',
    }).done(function(data) {
        if (data.status == 'failure') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
	    window.location.href = data.redirect;
            return;
        }
        updateInstances(data.instances);

    }).fail(function(jqXHR, status) {
        toastr.error("Error");
    });

    // Get the list of models status.
    $.ajax({
        url: "/wap/get_models",
        dataType: "json",
        type: 'post',
    }).done(function(data) {
        if (data.status == 'failure') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
	    window.location.href = data.redirect;
            return;
        }
        updateModels(data.models);

    }).fail(function(jqXHR, status) {
        toastr.error("Error");
    });
}

// Update the WAP instances
function updateInstances(instances) {

    // Empty the table.
    $('#wap-instance-table tbody').empty();

    // Are there any instances?
    if (instances.length == 0) {
	$('#no-wap-instances').show();
	$('#wap-instance-div').hide();
	return;
    }

    // Show the instance table and hide the "no instances" message.
    $('#no-wap-instances').hide();
    $('#wap-instance-div').show();

    // Fill in the table.
    $.each(instances, function(i, instance) {

	// Add a new row.
	$('#wap-instance-table tbody')
	    .append($('<tr>')
		    .append($('<td>')
			    .append('<a href="/wap/instance/' + instance.wap_ui_id + '">' + instance.wap_ui_name + '</a>'))
		    .append($('<td>')
			    .append('<a href="/wap/type/' + instance.wap_type_id + '">' + instance.wap_type_name + '</a>'))
		    .append($('<td>')
			    .append(instance.wap_ui_description))
		    .append($('<td>')
			    .append(moment(instance.first_week).format('MMM DD, YYYY')))
		    .append($('<td>')
			    .append(moment(instance.last_week).format('MMM DD, YYYY')))
		    .append($('<td class="text-center">')
			    .append($('<button>')
				    .attr({
					"class": "btn btn-default btn-sm",
					"data-toggle": "modal",
					"data-target": "#copy-instance-dialog",
					"data-id": instance.wap_ui_id,
				    })
				    .append($('<span class="glyphicon glyphicon-duplicate">'))))
		    .append($('<td class="text-center">')
			    .append($('<button>')
				    .attr({
					"class": "btn btn-default btn-sm",
					"data-toggle": "modal",
					"data-target": "#delete-instance-dialog",
					"data-id": instance.wap_ui_id,
					"data-name": instance.wap_ui_name,
				    })
				    .append($('<span class="glyphicon glyphicon-trash">'))))
		    );
    });
}

// Update the WAP models
function updateModels(models) {

    // Empty the table.
    $('#wap-model-table tbody').empty();

    // Are there any models?
    if (models.length == 0) {
	$('#no-wap-models').show();
	$('#wap-model-div').hide();
	return;
    }

    // Show the model table and hide the "no models" message.
    $('#no-wap-models').hide();
    $('#wap-model-div').show();

    // Fill in the table.
    $.each(models, function(i, model) {

	// Add a new row.
	$('#wap-model-table tbody')
	    .append($('<tr>')
		    .append($('<td>')
			    .append('<a href="/wap/model/' + model.wap_um_id + '">' + model.wap_um_name + '</a>'))
		    .append($('<td>')
			    .append('<a href="/wap/type/' + model.wap_type_id + '">' + model.wap_type_name + '</a>'))
		    .append($('<td>')
			    .append(model.wap_um_description))
		    .append($('<td class="text-center">')
			    .append($('<button>')
				    .attr({
					"class": "btn btn-default btn-sm",
					"data-toggle": "modal",
					"data-target": "#copy-model-dialog",
					"data-id": model.wap_um_id,
				    })
				    .append($('<span class="glyphicon glyphicon-duplicate">'))))
		    .append($('<td class="text-center">')
			    .append($('<button>')
				    .attr({
					"class": "btn btn-default btn-sm",
					"data-toggle": "modal",
					"data-target": "#delete-model-dialog",
					"data-id": model.wap_um_id,
					"data-name": model.wap_um_name,
				    })
				    .append($('<span class="glyphicon glyphicon-trash">'))))
		    );
    });
}
