$(document).ready(function() {

    // When the dialog is displayed, set the current instance ID.
    $('#delete-instance-dialog').on('show.bs.modal', function(event) {
	var ui_id = $(event.relatedTarget).data('id');
	var ui_name = $(event.relatedTarget).data('name');
	$('#wap-ui-id').val(ui_id);
	$('#wap-ui-name').html(ui_name);
    });

    // Validate the copy instance form and submit it if it is valid.
    $('#delete-instance-form').formValidation({
	framework: 'bootstrap',
	icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
	},
	fields: {
	}
    }).on('success.form.fv', function(e) {
	e.preventDefault();

	// Get the form instance
	var $form = $(e.target);
	var fv = $form.data('formValidation');

	// Get the form data and submit it.
	var message_data = $form.serialize();
	$.ajax({
	    url: '/wap/delete_instance',
	    dataType: 'json',
	    type: 'post',
	    data: message_data
	}).done(function(data) {
	    if (data.status != 'success') {
		toastr.error(data.message);
		return;
	    }
            else if (data.status == 'redirect') {
		window.location.href = data.redirect;
		return;
            }
	    $('#delete-instance-dialog').modal('hide');
	    fv.resetForm();

	    refreshPage();
	}).fail(function(jqXHR, status) {
	    toastr.error("Server communication error. Please try again.");
        }).always(function() {
        });
    });
});

// Update the list of instances in the drop down.
function copyInstanceUpdateInstances(other_instances) {

    // Clear the possible instances.
    $('#save-as-id').empty();
    $('#save-as-id').append($('<option value="0" selected="selected">Select Instance</option>'));

    // If there are no existing instances, hide the save as existing...
    if (other_instances.length == 0) {
	$('#select-existing-description').hide();
	$('#select-existing-div').hide();
	return;
    }

    // Show the form and description
    $('#select-existing-description').show();
    $('#select-existing-div').show();

    // Show each possible instance.
    $.each(other_instances, function(i, instance) {
	$('#save-as-id').append($('<option value="' + instance.wap_ui_id + '">' + instance.wap_ui_name + '</option>'));
    });
}
