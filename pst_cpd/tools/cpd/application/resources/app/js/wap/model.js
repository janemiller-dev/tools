$(document).ready(function() {
    refreshPage();
});

// Get the page data and display it.
function refreshPage() {

    // Get the ID from the URL
    var path = window.location.pathname;
    var components = path.split('/');
    var id = components[components.length - 1];

    // Get the model
    $.ajax({
        url: "/wap/get_model",
        dataType: "json",
        type: 'post',
	data: {
	    id: id
	}
    }).done(function(data) {
        if (data.status == 'failure') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
	    window.location.href = data.redirect;
            return;
        }
        updateModel(data.model);

    }).fail(function(jqXHR, status) {
        toastr.error("Error");
    });
}

// Update the model
function updateModel(model) {

    // Update the title and quarter.
    $('#wap-model-name').html(model.wap_um_name).addClass('editable')
	.attr({
	    'data-url': '/wap/update_model',
	    'data-name': 'wap_um_name',
	    'data-pk': model.wap_um_id,
	    'data-type': 'text',
	    'title': 'New Name',
	});

    // Draw the WAP column headers
    $('#wap-model-table thead.main-table').empty();
    $('#wap-model-table thead.main-table').append($('<tr>')
						  .append($('<th class="text-center">')
							  .append(''))
						  .append($('<th class="text-center">')
							  .append('Floor'))
						  .append($('<th class="text-center">')
							  .append('Target'))
						  .append($('<th class="text-center">')
							  .append('Game'))
						 );

    // Create the rows and cells for each metric in the type.
    $('#wap-model-table tbody.main-table').empty();
    $.each(model.metrics, function(c, metric) {

	// Each metric label contains the metric name and the converstion rate.
	var metric_value = '<span class="metric-name">' + metric.wap_m_name + '</span>';
	$('#wap-model-table tbody.main-table')
	    .append($('<tr id="metric-' + metric.wap_m_id + '">')
		    .append($('<td class="text-center">')
			    .append(metric_value)));

	// Create a cell for each metric's floor, target and game.
	var id = metric.wap_m_id + '-';
	$('#wap-model-table tbody.main-table tr#metric-' + metric.wap_m_id)
	    .append($('<td class="text-center wap-model-cell id="cell-' + id + '-floor">')
		    .append(metric.wap_model_floor))
	    .append($('<td class="text-center wap-model-cell id="cell-' + id + '-target">')
		    .append(metric.wap_model_target))
	    .append($('<td class="text-center wap-model-cell id="cell-' + id + '-game">')
		    .append(metric.wap_model_game));
    });

    // Link the editible classes to the editable plugin.
    setEditable();
}

/*
// Display the specified cell.
function displayCell(id, cells, period, metric, ui_id, editable) {

    // Get the metric ID from the metric.
    var metric_id = metric.wap_m_id;

    // Get the Unit of Measurement and set the cell's class appropriately.
    var format = '0,0.00';

    // Create the cell
    $('#cell-' + id).append($('<table class="table table-condensed cell-table" id="table-' + id + '">')
			    .append($('<thead>')
				    .append($('<tr>')
					    .append($('<th class="text-center">')
						    .append('FTG'))
					    .append($('<th class="text-center">')
						    .append($('<span class="has-popover glyphicon glyphicon-info-sign" style="float: right" data-toggle="popover" data-placement="top" data-html="true" title="Weekly ' + metric.wap_m_name + '" data-content="TBD">'))
						    .append('#')
						   )))
			    .append($('<tbody>')
				    .append($('<tr>')
					    .append($('<td class="shaded">')
						    .append('F'))
					    .append($('<td id="floor-' + id + '">')
						    .append(numeral(0).format(format))))
				    .append($('<tr>')
					    .append($('<td class="shaded">')
						    .append('T'))
					    .append($('<td id="target-' + id + '">')
						    .append(numeral(0).format(format))))
				    .append($('<tr>')
					    .append($('<td class="shaded">')
						    .append('G'))
					    .append($('<td id="game-' + id + '">')
						    .append(numeral(0).format(format)))))
			    .append($('<tfoot>')
				    .append($('<tr>')
					    .append($('<td class="shaded">')
						    .append('Actual'))
					    .append($('<td id="actual-' + id + '">')
						    .append(numeral(0).format(format)))))
			   );

    // Add the editable class and attributes for editable cells.  The Actual is always editable.
    $('#actual-' + id).addClass('editable')
	.attr({
	    'data-url': '/wap/update_cell',
	    'data-name': 'wap_ic_actual',
	    'data-pk': ui_id + '-' + metric.wap_m_id + '-' + period,
	    'data-type': 'number',
	    'title': 'Set Actual',
	});

    // If the cell is editable, make all of the values editable.
    if (editable) {
	$('#floor-' + id).addClass('editable')
	    .attr({
		'data-url': '/wap/update_cell',
		'data-name': 'wap_ic_floor',
		'data-pk': ui_id + '-' + metric.wap_m_id + '-' + period,
		'data-type': 'number',
		'title': 'Set Floor',
	    });
	$('#target-' + id).addClass('editable')
	    .attr({
		'data-url': '/wap/update_cell',
		'data-name': 'wap_ic_target',
		'data-pk': ui_id + '-' + metric.wap_m_id + '-' + period,
		'data-type': 'number',
		'title': 'Set Target',
	    });
	$('#game-' + id).addClass('editable')
	    .attr({
		'data-url': '/wap/update_cell',
		'data-name': 'wap_ic_game',
		'data-pk': ui_id + '-' + metric.wap_m_id + '-' + period,
		'data-type': 'number',
		'title': 'Set Game',
	    });
    }
    
    // Do we have any values for the cell?
    if (typeof cells[period] != 'undefined' && typeof cells[period][metric_id] !== 'undefined') {
	var cell = cells[period][metric_id];

	$('#floor-' + id).html(numeral(cell.wap_ic_floor).format(format));
	$('#game-' + id).html(numeral(cell.wap_ic_game).format(format));
	$('#target-' + id).html(numeral(cell.wap_ic_target).format(format));
	$('#actual-' + id).html(numeral(cell.wap_ic_actual).format(format));
    }
}
*/
