$(document).ready(function() {

    // When the dialog is displayed, set the instance.
    $('#remove-group-instance').on('show.bs.modal', function(event) {
	// Get the group ID from the URL
	var path = window.location.pathname;
	var components = path.split('/');
	var g_id = components[components.length - 1];

	var ui_id = $(event.relatedTarget).data('id');
	var ui_name = $(event.relatedTarget).data('name');
	$('#tgd-rm-g-id').val(g_id);
	$('#tgd-rm-ui-id').val(ui_id);
	$('#tgd-rm-ui-name').html(ui_name);
    });

    // Validate and submit
    $('#remove-group-instance-form').formValidation({
	framework: 'bootstrap',
	icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
	},
	fields: {
	}
    }).on('success.form.fv', function(e) {
	e.preventDefault();

	// Get the form group
	var $form = $(e.target);
	var fv = $form.data('formValidation');

	// Get the form data and submit it.
	var message_data = $form.serialize();
	$.ajax({
	    url: jsglobals.base_url + 'tgd/remove_group_instance',
	    dataType: 'json',
	    type: 'post',
	    data: message_data
	}).done(function(data) {
	    if (data.status != 'success') {
		toastr.error(data.message);
		return;
	    }
            else if (data.status == 'redirect') {
		window.location.href = data.redirect;
		return;
            }
	    $('#remove-group-instance').modal('hide');
	    fv.resetForm();

	    refreshPage();
	}).fail(function(jqXHR, status) {
	    toastr.error("Server communication error. Please try again.");
        }).always(function() {
        });
    });
});

