$(document).ready(function () {
    $('#propagate-other-income-dialog').on('show.bs.modal', function (event) {

        // Get inputs
        var oi_id = $(event.relatedTarget).data('id');
        var name = $(event.relatedTarget).data('name');
        var qtr = $(event.relatedTarget).data('quarter');


        console.log(qtr);
        $('#update-other-income-dialog').modal('hide');

        $('#prop-other-income-name').html(name);

        $('#propagate-other-income-button').off('click');
        $('#propagate-other-income-button').click(function () {
            $.ajax({
                url: jsglobals.base_url + 'tgd/propagate_other_income',
                dataType: 'json',
                data: {
                    oi_id: oi_id,
                    qtr: qtr
                },
                type: 'post',
                error: ajaxError
            }).done(function (data) {

                if (data.status != 'success') {
                    toastr.error(data.message);
                    return;
                } else if (data.status == 'redirect') {
                    window.location.href = data.redirect;
                    return;
                }

                $('#propagate-other-income-dialog').modal('hide');
                refreshPage();
                toastr.success('Other Income Propagated!!');
            }).fail(function (jqXHR, textStatus) {
                toastr.error("Request failed: " + textStatus);
            }).always(function () {
            });
        })
    });
});