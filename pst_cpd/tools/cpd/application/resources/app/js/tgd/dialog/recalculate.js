$(document).ready(function () {

    // Catch changes to the save-as-name and save-as-id fields to know whether or not to so the "new tab" question.
    $('#recalc-save-as-name, #recalc-save-as-id').on("change", function (event) {
        // Show the question or hide the questions?
        if ($.trim($('#recalc-save-as-name').val()) == "" && $('#recalc-save-as-id').val() == '0')
            $('#recalc-new-tab-checkbox').hide();
        else
            $('#recalc-new-tab-checkbox').show();
    });

    // When the dialog is displayed, set the current instance ID.
    $('#recalculate-dialog').on('show.bs.modal', function (event) {
        const ui_id = $(event.relatedTarget).data('id');
        let locked_quarter = $(event.relatedTarget).data('locked');
        $(event.relatedTarget).removeData('locked');
        $('#toggle-history-dialog').modal('hide');
        $('#recalc-tgd-ui-id').val(ui_id);
        $('#recalc-tgd-locked-quarter').val(locked_quarter);
        $('#recalc-save-as-name').val('');
        $('#recalc-copy-tgd-ui-id').val(ui_id);

        $('#recalc-new-tab').prop('checked', false);
        $('#recalc-new-tab-checkbox').hide();

        // Get the list of possible other instances.
        $.ajax({
            url: jsglobals.base_url + 'tgd/get_instances',
            dataType: 'json',
            type: 'post',
        }).done(function (data) {
            if (data.status != 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            recalcInstanceUpdateInstances(data.instances, ui_id);
        }).fail(function (jqXHR, status) {
            toastr.error("Server communication error. Please try again.");
        }).always(function () {
        });
    });

    // Validate the add instance form and submit it if it is valid.
    $('#recalculate-form').formValidation().on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);
        var fv = $form.data('formValidation');

        // Get the form data and submit it.
        var message_data = $form.serialize();
        $.ajax({
            url: jsglobals.base_url + 'tgd/recalculate',
            dataType: 'json',
            type: 'post',
            data: message_data
        }).done(function (data) {
            if (data.status != 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            // If the user created a new instance, open in a new tab.
            if (typeof data.instance_id != 'undefined' && $('#recalc-new-tab').is(':checked')) {
                // Open the new instance in a new tab.
                var address = jsglobals.base_url + "tgd/tgd/" + data.instance_id;
                var newwin = window.open(address, '_blank');
                if (newwin) {
                    newwin.focus();
                }
                else {
                    toastr.error('Please allow popups for this website');
                }
            }

            $('#recalculate-dialog').modal('hide');
            fv.resetForm();

            refreshPage();
            $('#toggle-history-dialog').modal('show');

        }).fail(function (jqXHR, status) {
            toastr.error("Server communication error. Please try again.");
        }).always(function () {
        });
    });
});

// Update the list of instances in the drop down.
function recalcInstanceUpdateInstances(other_instances, ignore_id) {

    // Clear the possible instances.
    $('#recalc-save-as-id').empty();
    $('#recalc-save-as-id').append($('<option value="0" selected="selected">Select Instance</option>'));

    // If there are no existing instances, hide the save as existing...
    if (other_instances.length == 0 || (other_instances.length == 1 && typeof ignore_id != 'undefined')) {
        $('#recalc-select-existing-description').hide();
        $('#recalc-select-existing-div').hide();
        return;
    }

    // Show the form and description
    $('#recalc-select-existing-description').show();
    $('#recalc-select-existing-div').show();

    // Show each possible instance.
    $.each(other_instances, function (i, instance) {
        if (ignore_id == 'undefined' || instance.tgd_ui_id != ignore_id)
            $('#recalc-save-as-id').append($('<option value="' + instance.tgd_ui_id + '">' + instance.tgd_ui_name + '</option>'));
    });
}
