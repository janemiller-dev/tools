$(document).ready(() => {

    $('#view-linked-tgd-dialog').on('show.bs.modal', function (event) {
        $('#linked-tgd-table tbody').empty();
        linked_tgd_dialog.init(event);
    });
});

let linked_tgd_dialog = (() => {

    let id, products,

        // Initialize linked TGD instances.
        init = function (event) {
            id = $(event.relatedTarget).data('id');

            const get_related_tgd = makeAjaxCall('tgd/get_related_tgd', {
                'id': id
            });

            // Append these TGD instances to Linked TGD table.
            get_related_tgd.then(function (data) {
                $.each(data.related_tgd, function (index, related_tgd) {
                    $('#linked-tgd-table').find('tbody')
                        .append($('<tr>' +
                                '<td class="text-center">' + (index + 1) + '</td>' +
                                '<td class="text-center">' + related_tgd.u_product_name + '</td>' +
                                '<td class="text-center">' + related_tgd.tgd_ui_name + '</td>'+
                            '</tr>'))
                })
            });
        };

    return {
        init: init
    };

})();