$(document).ready(function() {
    refreshPage();
});

// Get the page data and display it.
function refreshPage() {

    // Get the ID from the URL
    var path = window.location.pathname;
    var components = path.split('/');
    var id = components[components.length - 1];

    // Get the subscription status.
    $.ajax({
        url: jsglobals.base_url + "tgd/get_game_type",
        dataType: "json",
        type: 'post',
	data: {
	    id: id
	}
    }).done(function(data) {
        if (data.status == 'failure') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
	    window.location.href = data.redirect;
            return;
        }
        updateGameType(data.game_type);
    }).fail(function(jqXHR, status) {
        toastr.error("Error");
    });
}

// Update the Game Type
function updateGameType(game_type) {

    // Update the name.
    $('#game-type-name').html(game_type.tgd_gt_name);

    // Update the description.
    $('#game-type-description').html(game_type.tgd_gt_description);

    // Show the conditions.
    $.each(game_type.conditions, function(i, condition) {

	// Convert the condition type to a name. 
	var cond_value = '<span class=condition-label>';
	if (condition.tgd_c_type == 'obj')
	    cond_value += 'Objective';
	else
	    cond_value += 'Condition';
	cond_value += '</span>';

	$('#condition-table tbody').append($('<tr>')
					   .append($('<td class="text-left">')
						   .append(condition.tgd_c_name))
					   .append($('<td class="text-center">')
						   .append(cond_value))
					   .append($('<td class="text-left">')
						   .append($('<strong>')
							   .append(condition.tgd_c_description + ': '))
						   .append(condition.tgd_c_tooltip)));
    });

    // Show the multipliers
    $.each(game_type.multipliers, function(i, multiplier) {

	// Determine the effect.
	var effect_value;
	effect_value = "NOT SET";
	if (multiplier.tgd_m_operation == 'mult') {
	    if (multiplier.tgd_m_type == 'dollar')
		effect_value = 'Each completed deal is multiplied by the amount of the multiplier.';
	    else  (multiplier.tgd_m_type == 'percent')
		effect_value = 'Number number of completed deals are increased by the set percentage.';
	}
	else if (multiplier.tgd_m_operation == 'add') {
	    if (multiplier.tgd_m_type == 'dollar')
		effect_value = 'The amount set is added to the total value.';
	}
	$('#multiplier-table tbody').append($('<tr>')
					   .append($('<td class="text-left">')
						   .append(multiplier.tgd_m_name))
					   .append($('<td class="text-left">')
						   .append(multiplier.tgd_m_description))
					   .append($('<td class="text-left">')
						   .append(effect_value)));
    });

}
