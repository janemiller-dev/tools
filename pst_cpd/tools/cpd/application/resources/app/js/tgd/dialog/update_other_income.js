$(document).ready(function () {

    $('#update-other-income-dialog').on('show.bs.modal', function (event) {

        var id = $(event.relatedTarget).data('id');
        var qtr = $(event.relatedTarget).data('qtr');

        $('#oi-qtr').val(qtr);
        $('#oi-id').val(id);

        // Fetch Other income list
        $.ajax({
            url: jsglobals.base_url + 'tgd/get_other_income',
            dataType: 'json',
            type: 'post',
            data: {
                ui_id: id,
                qtr: qtr
            }
        }).done(function (data) {
            if (data.status != 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            $('#other-income-table tbody').empty();

            $.each(data.other_income.oi, function (index, other_income) {
                $('#other-income-table tbody')
                    .append($('<tr>')
                        .append($('<td class="text-center">')
                            .append(other_income.tois_value))
                        .append($('<td class="text-center">')
                            .append(other_income.toi_name))
                        .append($('<td class="text-center">')
                            .append(other_income.toi_amount))
                        .append($('<td class="text-center">')
                            .append($('<button type="button" class="btn btn-default btn-sm" ' +
                                'data-toggle="modal" data-backdrop="static" ' +
                                'data-target="#propagate-other-income-dialog" ' +
                                'data-id="' + other_income.toi_id + '" ' +
                                'data-quarter="' + qtr + '"' +
                                'data-name="' + other_income.toi_name + '">' +
                                '<span class="fa fa-exchange"></span>')))
                        .append($('<td class="text-center">')
                            .append($('<button type="button" class="btn btn-default btn-sm" ' +
                                'data-toggle="modal" data-backdrop="static" ' +
                                'data-target="#delete-other-incomes-dialog" ' +
                                'data-id="' + other_income.toi_id + '" ' +
                                'data-name="' + other_income.toi_name + '">' +
                                '<span class="fa fa-trash"></span>')))
                    );
            });

            $.each(data.other_income.ref, function (index, other_income) {
                $('#other-income-table tbody')
                    .append($('<tr>')
                        .append($('<td class="text-center">')
                            .append('Referral Fee'))
                        .append($('<td class="text-center">')
                            .append('DMD'))
                        .append($('<td class="text-center">')
                            .append(other_income.ref))
                        .append($('<td class="text-center">')
                            .append($('<button type="button" class="btn btn-default btn-sm" disabled>' +
                                '<span class="fa fa-exchange"></span>')))
                        .append($('<td class="text-center">')
                            .append($('<button type="button" class="btn btn-default btn-sm" disabled>' +
                                '<span class="fa fa-trash"></span>')))
                    );
            });

            $.each(data.other_income.buyer, function (index, other_income) {
                $('#other-income-table tbody')
                    .append($('<tr>')
                        .append($('<td class="text-center">')
                            .append('Buy Side Fee'))
                        .append($('<td class="text-center">')
                            .append('DMD'))
                        .append($('<td class="text-center">')
                            .append(other_income.buyer))
                        .append($('<td class="text-center">')
                            .append($('<button type="button" class="btn btn-default btn-sm" disabled>' +
                                '<span class="fa fa-exchange"></span>')))
                        .append($('<td class="text-center">')
                            .append($('<button type="button" class="btn btn-default btn-sm" disabled>' +
                                '<span class="fa fa-trash"></span>')))
                    );
            })

        }).fail(function (jqXHR, status) {
            toastr.error("Server communication error. Please try again.");
        }).always(function () {
        });
    });

});