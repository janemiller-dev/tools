$(document).ready(() => {

    $('#view-tgd-metrics-dialog').on('show.bs.modal', function (event) {
        const get_tgd_metrics_data = makeAjaxCall('tgd/get_metrics_data', {});

        get_tgd_metrics_data.then(function (data) {
            $.each(data.metrics_data.benchmark[0], function (index, value) {
                $('.metrics-benchmark[data-name="' + index.replace(/tm_/, '') + '"]').val(value);
            });

            $.each(data.metrics_data.tsl, function (index, value) {
                $('#' + index + '_count').val(value[0].val)
            });

            $.each(data.metrics_data.cpd, function (index, value) {
                $('.cpd-col[data-status="' + value.ds_status + '"][data-type="count"]').val(value.count);
                $('.cpd-col[data-status="' + value.ds_status + '"][data-type="price"]').val('$' + value.price);
                $('.cpd-col[data-status="' + value.ds_status + '"][data-type="gross"]').val('$' + value.gross);
                $('.cpd-col[data-status="' + value.ds_status + '"][data-type="net"]').val('$' + value.net);
            })
        });
    });

    $(document).on('change', '.metrics-benchmark', function () {
        const save_metrics_benchmark = makeAjaxCall('tgd/save_metrics_benchmark', {
            'col': $(this).attr('data-name'),
            'val': $(this).val()
        });

        save_metrics_benchmark.then(function (data) {

            console.log(data);
        })
    })
});
