$(document).ready(function() {

    // When the dialog is displayed, set the current instance ID.
    $('#delete-instance-dialog').on('show.bs.modal', function(event) {
	var ui_id = $(event.relatedTarget).data('id');
	var ui_name = $(event.relatedTarget).data('name');
	$('#tgd-del-ui-id').val(ui_id);
	$('#tgd-del-ui-name').html(ui_name);
    });

    // Validate the copy instance form and submit it if it is valid.
    $('#delete-instance-form').formValidation({
	framework: 'bootstrap',
	icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
	},
	fields: {
	}
    }).on('success.form.fv', function(e) {
	e.preventDefault();

	// Get the form instance
	var $form = $(e.target);
	var fv = $form.data('formValidation');

	// Get the form data and submit it.
	var message_data = $form.serialize();
	$.ajax({
	    url: jsglobals.base_url + 'tgd/delete_instance',
	    dataType: 'json',
	    type: 'post',
	    data: message_data
	}).done(function(data) {
	    if (data.status != 'success') {
		toastr.error(data.message);
		return;
	    }
            else if (data.status == 'redirect') {
		window.location.href = data.redirect;
		return;
            }
	    $('#delete-instance-dialog').modal('hide');
	    fv.resetForm();

	    refreshPage();
	}).fail(function(jqXHR, status) {
	    toastr.error("Server communication error. Please try again.");
        }).always(function() {
        });
    });
});

