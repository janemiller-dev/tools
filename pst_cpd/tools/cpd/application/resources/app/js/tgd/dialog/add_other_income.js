$(document).ready(function () {
    // Catch updates to the Other income type and change the display as appropriate.
    $('#oi-type').change(function () {
        // Reset the values
        $('#oi-amount').val(0);
    });

    // When the dialog is displayed, clear the values
    $('#add-other-income-dialog').on('show.bs.modal', function (event) {
        $('#update-other-income-dialog').modal('hide');

        $('#oi-type').val('amt').trigger('change');
        $('#oi-name').val('');

        $.ajax({
            url: jsglobals.base_url + 'tgd/get_source',
            dataType: 'json',
            type: 'post',
        }).done(function (data) {
            if (data.status != 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            $('#oi-type').empty();
            $('#oi-type').append('<option value="-2" selected>--Select Income Name--</option>');
            $('#oi-type').append('<option value="-1">--Add Name--</option>');
            $.each(data.sources, function (index, source) {
                $('#oi-type').append('<option value="' + source.tois_id + '">' + source.tois_value + '</option>');
            })
        }).fail(function (jqXHR, status) {
            toastr.error("Server communication error. Please try again.");
        }).always(function () {
        });
    });

    // Check if add new income is selected.
    $(document).on('change', '#oi-type', function () {
       if('-1' === $(this).val()) {
           $('#add-income-dialog').modal('show');
       }
    });

    // Add other income dialog show event handler.
    $('#add-income-dialog').on('show.bs.modal', function (event) {
        $('#add-other-income-dialog').modal('hide');

        // Add other income click handler.
        $('#add-income-source').off('click');
        $(document).on('click', '#add-income-source', function () {
            const add_income_source = makeAjaxCall('tgd/add_income_source', {
                'name': $('#income-source').val()
            });

            add_income_source.then(function (data) {
                toastr.success('New Income source added.', 'Success!!');
                $('#add-income-dialog').modal('hide');
            })
        })
    });

    // Validate the add instance form and submit it if it is valid.
    $('#add-other-income-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            name: {
                validators: {
                    notEmpty: {
                        message: "The other income name is required."
                    }
                }
            },
            amount: {
                enabled: false,
                validators: {
                    greaterThan: {
                        value: -1,
                        message: "An amount greater than or equal to 0 is required."
                    }
                }
            },
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);
        var fv = $form.data('formValidation');

        // Get the form data and submit it.
        var message_data = $form.serialize();
        $.ajax({
            url: jsglobals.base_url + 'tgd/add_other_income',
            dataType: 'json',
            type: 'post',
            data: message_data
        }).done(function (data) {
            if (data.status != 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            $('#add-other-income-dialog').modal('hide');
            fv.resetForm();
            refreshPage();

        }).fail(function (jqXHR, status) {
            toastr.error("Server communication error. Please try again.");
        }).always(function () {
        });
    });
});

// Update the possible TGDs in the select list.
function updatePossibleTGDs() {

    // Get the current TGD ID
    var path = window.location.pathname;
    var components = path.split('/');
    var id = components[components.length - 1];

    $.ajax({
        url: jsglobals.base_url + 'tgd/get_instances',
        dataType: 'json',
        type: 'post',
        tgd_id: id,
        other_income: true
    }).done(function (data) {
        if (data.status != 'success') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
            window.location.href = data.redirect;
            return;
        }

        // Clear the select list.
        $('#oi-tgd-id').empty();

        // Show each game type and update the game type periods table.
        $('#oi-tgd-id').append($('<option>')
            .attr({value: 0})
            .append("Select TGD"));
        $.each(instances, function (gt, instance) {
            $('#oi-tgd-id').append($('<option>')
                .attr({value: instance.tgd_ui_id})
                .append(instance.tgd_ui_name));
        });
    }).fail(function (jqXHR, status) {
        toastr.error("Server communication error. Please try again.");
    }).always(function () {
    });

}
