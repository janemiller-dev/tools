$(document).ready(function () {

    // When the dialog is displayed, clear the values
    $('#add-instance-dialog').on('show.bs.modal', function (event) {

        // Fetches the products list for the user.
        const get_products = makeAjaxCall('industry/get_all_products', {});

        get_products.then(function (data) {
            updateTGDProducts(data.products);
        });

        $('#tgd-ui-name').val('');
        $('#tgd-ui-mission').html('');

        // Initialize the year dropdown.
        instanceUpdateYears();
    });

    // Validate the add instance form and submit it if it is valid.
    $('#add-instance-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            product: {
                validators: {
                    notZero: {
                        message: "The instance name is required."
                    }
                }
            },
            name: {
                validators: {
                    notEmpty: {
                        message: "The instance name is required."
                    }
                }
            },
            mission: {
                validators: {
                    notEmpty: {
                        message: "The instance mission is required."
                    }
                }
            },
            condition: {
                validators: {
                    notEmpty: {
                        message: "The conditions are requiured"
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);
        var fv = $form.data('formValidation');

        $('#tgd-gt-id').val(2);
        // Get the form data and submit it.
        var message_data = $form.serialize();
        $.ajax({
            url: jsglobals.base_url + 'tgd/add_instance',
            dataType: 'json',
            type: 'post',
            data: message_data
        }).done(function (data) {
            if (data.status != 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            $('#add-instance-dialog').modal('hide');
            fv.resetForm();

            refreshProducts();
            refreshPage();

        }).fail(function (jqXHR, status) {
            toastr.error("Server communication error. Please try again.");
        }).always(function () {
        });
    });

// Set the possible start dates based on the game type period.
    function instanceUpdateYears() {

        // Clear the old options.
        $('#tgd-ui-year').empty();

        // Allow the current year and the next year.
        var date = moment();

        // Only 2 years allowed.
        for (var i = 0; i < 4; i++) {
            var year = date.year();

            $('#tgd-ui-year').append($('<option>').attr({value: year,}).append(year));
            date.add(1, 'year');
        }

        $('#tgd-ui-year').val('2018');
    }


    function updateTGDProducts(products) {


        const tgd_product_selector = $('#tgd-product');
        tgd_product_selector.empty().attr('disabled', false);

        // Show the industries for selection.
        tgd_product_selector.append($('<option>')
            .attr({value: 0})
            .append("Select Product"));
        $.each(products, function (index, product) {
            tgd_product_selector.append('<option style="font-weight: bold; font-style: italic;"' +
                ' data-id="' + product[0]['u_product_group_id'] + '"' +
                ' value="' + product[0]['u_product_group_id'] + '" data-type="group">' + index + '</option>');


            $.each(product, function (i, data) {
                tgd_product_selector.append('<option value="' + data.u_product_id + '">' +
                    data.u_product_name + '</option>')
            });
        });
    }


    /*
     * Update the DMD drop down based on selection of product type
     */
    $(document).on('change', '#tgd-product', function () {

        var product_id = $('option:selected', this).val();

        if (product_id == 0) {
            $('#tgd-cpd-id').empty().attr('disabled', 'disabled');
            return;
        } else {
            $.ajax({
                url: jsglobals.base_url + 'cpdv1/get_instances',
                dataType: 'json',
                type: 'post',
                data: {
                    'year': $('option:selected', '#tgd-ui-year').val(),
                    'product_id': product_id
                },
                error: ajaxError
            }).done(function (data) {

                if (data.status != 'success') {
                    toastr.error(data.message);
                    return;
                }
                else if (data.status == 'redirect') {
                    window.location.href = data.redirect;
                    return;
                }
                updateTgdCpd(data.instances);
            }).fail(function (jqXHR, status, errorThrown) {
                toastr.error("Server communication error. Please try again.");
            }).always(function () {
            });
        }
    });

    /*
     * Function to update the TGD drop down based on selection of profession
     */
    function updateTgdCpd(cpd_instances) {

        $('#tgd-cpd-id').empty().attr('disabled', false);

        // Show the TGD for selection.
        if (cpd_instances.length == 0) {
            $('#tgd-cpd-id').append($('<option>')
                .attr({value: 0})
                .append("No DMD Found for Specified Year and Profession"));
            return;
        }

        $('#tgd-cpd-id').append($('<option>')
            .attr({value: 0})
            .append("Select DMD"));
        $.each(cpd_instances, function (i, cpd_instance) {
            $('#tgd-cpd-id').append($('<option>')
                .attr({value: cpd_instance.cpd_id})
                .append(cpd_instance.cpd_name));
        });
    }
});