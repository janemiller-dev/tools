$(document).ready(function () {
    // Recalculate widths on display of tabs.
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        if (e.target.hash === '#groups') {
            $('#tgd-group-table').width('100%');
        }
    });

});

// Get the page data and display it.
function refreshPage() {
    // Update the instances and the groups.
    updateInstances();
}

// Update the TGD instances
let instance_datatable = $('#tgd-instance-table').DataTable();

function updateInstances() {
    // $.each($('#current-product option'), function (i, option) {

    // if (i >= 2) {
    //
    //     $('#tgd-instance-row' + i).remove();
    //     let demo_tgd_row = $('.tgd-instance-demo-div').clone();
    //     demo_tgd_row.removeClass('hidden tgd-instance-demo-div');
    //     demo_tgd_row.addClass('tgd-instance-row');
    //     demo_tgd_row.attr('id', 'tgd-instance-row' + i)
    //     demo_tgd_row.find('#tgd-instance-demo-table').attr('id', 'tgd-instance-table-' + i);
    //     demo_tgd_row.find('#table-demo-header').attr('id', 'table-header-' + i);
    //
    //     $('.tgd-instance-row').last().after(demo_tgd_row);
    // }
    let instance_datatable = $('#tgd-instance-table').DataTable();

    // Destroy the datatable so that it can be rebuilt.
    instance_datatable.destroy();
    // $('#table-header-1').text($('#current-product option').eq(1).text());

    // Create the instance table using server-side data.
    instance_datatable = $('#tgd-instance-table').DataTable({
        "pageLength": 20,
        "serverSide": true,
        "columnDefs": [
            {"targets": [0], "visible": false, "searchable": false},
        ],
        "ajax": {
            url: jsglobals.base_url + "tgd/search_instances",
            type: 'post',
            "data": {
                "product_id": $('#current-product option').eq(1).val()
            }
        },
        "order": [
            [1, "asc"]
        ],
        "columns": [
            {"data": "tgd_ui_id", "name": "UI ID"},
            {"data": "user_email", "name": "Owner"},
            {"data": "tgd_ui_name", "name": "Name"},
            // {"data": "tgd_ui_id", "name": "Linked TGD"},
            {"data": "tgd_ui_mission", "name": "Mission"},
            {"data": "tgd_ui_year", "name": "Year"},
            {"data": "tgd_ui_created", "name": "Created"},
            {"data": "tgd_ui_updated", "name": "Modified"},
            {"data": "tgd_cpd_id", "name": "Active"},
            {"data": null, "name": "Close", "className": "text-center"},
            {"data": null, "name": "Delete", "className": "text-center"},
        ],
        "rowCallback": function (row, data, index) {
            $('td', row).eq(1).html('<a href="' + jsglobals.base_url + 'tgd/tgd/' + data.tgd_ui_id + '">'
                + data.tgd_ui_name + "</a>");
            $('td', row).eq(5).html(moment(data.tgd_ui_created).format("MMM D, YYYY HH:mm:ss"));
            $('td', row).eq(6).html(moment(data.tgd_ui_updated).format("MMM D, YYYY HH:mm:ss"));

            // $('td', row).eq(2).html($('<select class="select-picker tgd-linked-products "' +
            //     ' multiple data-active-id="' + data.linked_product_id + '"' +
            //     ' data-id="' + data.tgd_ui_id + '" data-live-search="true"></select>'));

            // $('td', row).eq(2).html($('<select class="tgd-linked-products select-picker" multiple data-active-id="'
            //     + data.linked_product_id + '" data-id="' + data.tgd_ui_id + '" data-live-search="true"></select>'));

            $('td', row).eq(6).html('<input type="radio" name="active_tgd' + data.tgd_ui_product_id + '"' +
                ' data-id="' + data.tgd_ui_id + '" data-product="' + data.tgd_ui_product_id + '"' +
                'class="active_instance_id" data-name="tgd_id" value="' + data.tgd_ui_id + '">');

            if (data.uact_tgd_id === data.tgd_ui_id) {
                $('td', row).eq(6).find("input:radio").prop('checked', true);
            }

            if (data.share_id == null) {
                $('td', row).eq(7).html('<button class="btn btn-default btn-sm" data-toggle="modal" ' +
                    'data-target="#copy-instance-dialog" data-id="' + data.tgd_ui_id + '">' +
                    '<span class="fa fa-files-o"></span></button>');

                $('td', row).eq(8).html('<button class="btn btn-default btn-sm" data-toggle="modal"' +
                    ' data-target="#delete-instance-dialog" data-id="' + data.tgd_ui_id + '" ' +
                    'data-name="' + data.tgd_ui_name + '"><span class="fa fa-trash"></span></button>');
            }
            else {
                $('td', row).eq(7).html('');
                $('td', row).eq(8).html('<button class="btn btn-default btn-sm" data-toggle="modal"' +
                    ' data-target="#unshare-dialog" data-id="' + data.share_id + '"' +
                    ' data-name="' + data.tgd_ui_name + '"><span class="fa fa-chain-broken"></span></button>');
            }
        },
        "drawCallback": function () {
            // Remove TGD glider position.
            localStorage.removeItem('glider_position');
            updateActiveInstance();
            // Enable Multi select drop down.
            // $('.select-picker').selectpicker();
        },
        "initComplete": function () {

        }
    });
// }
//
// )
// ;

// Update the active instances of TGD.

// Fetches the list of all the linked TGDs.
// const get_linked_tgd = makeAjaxCall('tgd/get_linked_tgd', {});
//
// get_linked_tgd.then(function (data) {
//     const products = data.products;
//
//     // Append items to TGD Linked products select box.
//     const linked_products_selector = $('.tgd-linked-products');
//     linked_products_selector.empty().attr('disabled', false);
//     linked_products_selector.append($('<option>')
//         .attr({value: 0})
//         .append("-- Select Linked TGD --"));
//
//     // Iterate over each Products and append as option.
//     $.each(products.product, function (i, product) {
//         const product_id = (null === product.u_product_id) ? product.upg_id : product.u_product_id,
//             product_name = (null === product.u_product_name) ? product.upg_name : product.u_product_name;
//
//         if (products.active_id[0].uap_product_id !== product_id) {
//
//             $('.tgd-linked-products').append($('<option>')
//                 .attr({value: product_id})
//                 .append(product_name + ' : ' +
//                     (product.tgd_ui_name != null ? product.tgd_ui_name : 'None')));
//         }
//     });
//
//     $.each(linked_products_selector, function (index, product) {
//         // $(this).val($(product).attr('data-active-id'))
//         $(this).selectpicker('val', JSON.parse("[" + $(product).attr('data-active-id') + "]"));
//     });
//
//     // Listen to any changes in the linked TGD drop down.
//     linked_products_selector.change(function () {
//
//         // Update the TGD linked product.
//         const update_linked = makeAjaxCall('tgd/update_linked', {
//             'tgd_id': $(this).attr('data-id'),
//             'product_id': $(this).val()
//
//         });
//         update_linked.then(function (data) {
//             // refreshPage();
//         });
//     });
// });
}
