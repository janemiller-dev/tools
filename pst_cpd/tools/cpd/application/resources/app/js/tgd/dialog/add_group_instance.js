$(document).ready(function() {

    // Update the list of instances that can be added to the group.
    agiUpdateInstanceList();

    // When the dialog is displayed, clear the values
    $('#add-group-instance').on('show.bs.modal', function(event) {
	var g_id = $(event.relatedTarget).data('id');

	$('#add-group-ui-id').val('0');
	$('#add-group-g-id').val(g_id);
    });

    // Validate the add instance form and submit it if it is valid.
    $('#add-group-instance-form').formValidation({
	framework: 'bootstrap',
	icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
	},
	fields: {
	    tgd_gt_id: {
		validators: {
		    greaterThan: {
			value: 0,
			inclusive: false,
			message: "The industry is required."
		    }
		}
	    },
	    name: {
		validators: {
		    notEmpty: {
			message: "The instance name is required."
		    }
		}
	    },
	    mission: {
		validators: {
		    notEmpty: {
			message: "The instance mission is required."
		    }
		}
	    }
	}
    }).on('success.form.fv', function(e) {
	e.preventDefault();

	// Get the form instance
	var $form = $(e.target);
	var fv = $form.data('formValidation');

	// Get the form data and submit it.
	var message_data = $form.serialize();
	$.ajax({
	    url: jsglobals.base_url + 'tgd/add_group_instance',
	    dataType: 'json',
	    type: 'post',
	    data: message_data
	}).done(function(data) {
	    if (data.status != 'success') {
		toastr.error(data.message);
		return;
	    }
            else if (data.status == 'redirect') {
		window.location.href = data.redirect;
		return;
            }
	    $('#add-group-instance').modal('hide');
	    fv.resetForm();

	    refreshPage();
	    agiUpdateInstanceList();
	    
	}).fail(function(jqXHR, status) {
	    toastr.error("Server communication error. Please try again.");
        }).always(function() {
        });
    });

    // Initialize the year dropdown.
    instanceUpdateYears();
});

// Update the list of instances that can be added to the group.
function agiUpdateInstanceList() {
    
    // Get the ID from the URL
    var path = window.location.pathname;
    var components = path.split('/');
    var id = components[components.length - 1];

    // Get the possible instances
    $.ajax({
	url: jsglobals.base_url + 'tgd/get_instances?not_in=' + id,
	dataType: 'json',
	type: 'post',
    }).done(function(data) {
	if (data.status != 'success') {
	    toastr.error(data.message);
	    return;
	}
        else if (data.status == 'redirect') {
	    window.location.href = data.redirect;
	    return;
        }
        agiAddInstances(data.instances);
    }).fail(function(jqXHR, status) {
	toastr.error("Server communication error. Please try again.");
    }).always(function() {
    });
}

// Set the possible start dates based on the game type period.
function agiAddInstances(instances) {

    // Clear the old options.
    $('#add-group-ui-id').empty();

    // Show each game type and update the game type periods table.
    $('#add-group-ui-id').append($('<option>')
				 .attr({value: 0})
				 .append("Select Instance"));
    $.each(instances, function (gt, instance) {
	$('#add-group-ui-id').append($('<option>')
				     .attr({value: instance.tgd_ui_id})
				     .append(instance.tgd_ui_name));
    });

}
