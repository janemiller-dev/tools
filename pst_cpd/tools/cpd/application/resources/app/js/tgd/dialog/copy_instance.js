$(document).ready(function() {

    // When the dialog is displayed, set the current instance ID.
    $('#copy-instance-dialog').on('show.bs.modal', function(event) {
	var ui_id = $(event.relatedTarget).data('id');

	$('#save-as-name').val('');
	$('#copy-tgd-ui-id').val(ui_id);

	$('#save-as-new-tab').prop('checked', true);

	// Get the list of possible other instances.
	$.ajax({
	    url: jsglobals.base_url + 'tgd/get_instances',
	    dataType: 'json',
	    type: 'post',
	}).done(function(data) {
	    if (data.status != 'success') {
		toastr.error(data.message);
		return;
	    }
            else if (data.status == 'redirect') {
		window.location.href = data.redirect;
		return;
            }
	    copyInstanceUpdateInstances(data.instances, ui_id);
	}).fail(function(jqXHR, status) {
	    toastr.error("Server communication error. Please try again.");
        }).always(function() {
        });

    });

    // Validate the copy instance form and submit it if it is valid.
    $('#copy-instance-form').formValidation({
	framework: 'bootstrap',
	icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
	},
	fields: {
	}
    }).on('success.form.fv', function(e) {
	e.preventDefault();

	// Get the form instance
	var $form = $(e.target);
	var fv = $form.data('formValidation');

	// Get the form data and submit it.
	var message_data = $form.serialize();
	$.ajax({
	    url: jsglobals.base_url + 'tgd/copy_instance',
	    dataType: 'json',
	    type: 'post',
	    data: message_data
	}).done(function(data) {
	    if (data.status != 'success') {
		toastr.error(data.message);
		return;
	    }
            else if (data.status == 'redirect') {
		window.location.href = data.redirect;
		return;
            }

	    $('#copy-instance-dialog').modal('hide');
	    fv.resetForm();

	    refreshPage();

	    // If the user created a new instance, open in a new tab.
	    if (typeof data.instance_id != 'undefined' && $('#save-as-new-tab').is(':checked')) {
		// Open the new instance in a new tab.
		var address = jsglobals.base_url + "tgd/tgd/" + data.instance_id;
		var newwin = window.open(address, '_blank');
		if (newwin) {
		    newwin.focus();
		}
		else {
		    toastr.error('Please allow popups for this website');
		}
	    }
	}).fail(function(jqXHR, status) {
	    toastr.error("Server communication error. Please try again.");
        }).always(function() {
        });
    });
});

// Update the list of instances in the drop down.
function copyInstanceUpdateInstances(other_instances, ignore_id) {

    // Clear the possible instances.
    $('#save-as-id').empty();
    $('#save-as-id').append($('<option value="0" selected="selected">Select Instance</option>'));

    // If there are no existing instances, hide the save as existing...
    if (other_instances.length == 0 || (other_instances.length == 1 && typeof ignore_id != 'undefined')) {
	$('#select-existing-description').hide();
	$('#select-existing-div').hide();
	return;
    }

    // Show the form and description
    $('#select-existing-description').show();
    $('#select-existing-div').show();

    // Show each possible instance.
    $.each(other_instances, function(i, instance) {
	if (ignore_id == 'undefined' || instance.tgd_ui_id != ignore_id)
	    $('#save-as-id').append($('<option value="' + instance.tgd_ui_id + '">' + instance.tgd_ui_name + '</option>'));
    });
}
