$(document).ready(function() {

    // When the dialog is displayed, set the current instance ID.
    $('#open-instance-dialog').on('show.bs.modal', function(event) {
	$('#open-instance-new-tab').prop('checked', false);
    });

    // Validate the open instance form and submit it if it is valid.
    $('#open-instance-form').formValidation({
	framework: 'bootstrap',
	icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
	},
	fields: {
	}
    }).on('success.form.fv', function(e) {
	e.preventDefault();

	// Get the form instance
	var $form = $(e.target);
	var fv = $form.data('formValidation');

	// Close and reset the dialog.
	$('#open-instance-dialog').modal('hide');
	fv.resetForm();

	// Show in a new window or the same window.
	var address = jsglobals.base_url + "tgd/tgd/" + $('#open-instance-id').val();
	if ($('#open-instance-new-tab').is(':checked')) {
	    var newwin = window.open(address, '_blank');
	    if (newwin) {
		newwin.focus();
	    }
	    else {
		toastr.error('Please allow popups for this website');
	    }
	}
	else {
	    window.location.href = address;
	}
    });
});
