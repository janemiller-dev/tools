$(document).ready(function() {
    refreshPage();

    /*
    // Print as a PDF.
    $('#tool-buttons').on("click", '#print-pdf', function() {
	var path = window.location.pathname;
	var components = path.split('/');
	var t_id = components[components.length - 1];

	var address = jsglobals.base_url + 'tgd/print_group/' + g_id;
	window.location = address;
    });
    */
});

// Get the page data and display it.
function refreshPage(togglesOn) {

    // Get the ID from the URL
    var path = window.location.pathname;
    var components = path.split('/');
    var id = components[components.length - 1];

    // Get the subscription status.
    $.ajax({
        url: jsglobals.base_url + "tgd/get_group",
        dataType: "json",
        type: 'post',
	data: {
	    id: id
	}
    }).done(function(data) {
        if (data.status == 'failure') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
	    window.location.href = data.redirect;
            return;
        }
        updateGroup(data.group);

    }).fail(function(jqXHR, status) {
        toastr.error("Error");
    });
}

// Update the Group
function updateGroup(group) {

    // Update the title.
    $('#group-name').html(group.tgd_g_name).addClass('editable')
	.attr({
	    'data-url': jsglobals.base_url + 'common/update_db_field',
	    'data-name': 'tgd_group.tgd_g_name',
	    'data-pk': group.tgd_g_id,
	    'data-type': 'text',
	    'title': 'New Name',
	});

    // Update the description.
    $('#group-description').html(group.tgd_g_description).addClass('editable')
	.attr({
	    'data-url': jsglobals.base_url + 'common/update_db_field',
	    'data-name': 'tgd_group.tgd_g_description',
	    'data-pk': group.tgd_g_id,
	    'data-type': 'textarea',
	    'title': 'Update Mission',
	});

    // Show the group membership.
    $('#member-table tbody').empty();
    $.each(group.members, function(i, member) {
	$('#member-table tbody').append($('<tr>')
					.append($('<td class="text-left">')
						.append($('<a href="' + jsglobals.base_url + 'tgd/tgd/' + member.tgd_ui_id + '" target="_blank">')
							.append(member.tgd_ui_name)))
					.append($('<button class="btn btn-default btn-sm">')
						.attr({"data-toggle": "modal",
						       "data-target": "#remove-group-instance",
						       "data-id": member.tgd_ui_id,
						       "data-name": member.tgd_ui_name
						      })
						.append($('<span class="glyphicon glyphicon-remove">')))) ;
    });

    // If the add instance button is not there add it.
    if (!$('#add-group-instance-button').length) {
	$('#member-table tfoot').append($('<tr>')
					.append($('<td colspan="2" class="text-center">')
						.append($('<button id="add-group-instance-button" class="btn btn-default btn-sm">')
							.attr({"data-toggle": "modal",
							       "data-target": "#add-group-instance",
							       "data-id": group.tgd_g_id,
							      })
							.append('Add Instance to Group'))));
    }

    /*
    // Add the print PDF button to the button list if it is not already there.
    if (!$('#print-pdf').length) {
	$('#tool-buttons').append($('<button type="button" class="btn btn-primary has-tooltip" id="print-pdf" data-id="' + group.tgd_g_id + '" data-toggle="tooltip" title="Save a copy of the instance as a PDF" data-placement="top">')
				  .append('Print PDF'));
    }
    */

    // Draw the TGD column headers
    $('#group-table thead.main-table').empty();
    $('#group-table thead.main-table').append($('<tr>')
					    .append($('<th class="text-center">')
						    .append('')));

    // Draw the period date headers.
    var date = moment().year(group.tgd_g_year).dayOfYear(1).subtract(1, 'day');
    for (var period = 1; period <= 6; period++) {
	var next_quarter = new moment(date);
	next_quarter.add(1, 'quarter');

	// The header cell contents includes the Quarter, Date, and all multipliers.
	var col_header = '<span style="font-size: 1.3em">Q' + date.quarter() + ' ' + date.year() + '&nbsp;<br />' + date.endOf('quarter').format('MMM DD, YYYY') + '</span>';

	// Show the header
	$('#group-table thead.main-table tr').append($('<th class="text-center col-' + period + '">')
						   .append(col_header));

	// Increment to the next quarter.
	date.add(1, 'quarter');
    }

    // Create the rows and columns for each condition.
    $('#group-table tbody.main-table').empty();
    var row = 1;
    var cond_num = 1;
    $.each(group.conditions, function(c, condition) {

	var cond_value = '';

	// Show the tool tip if there is one.
	if (condition.tgd_c_tooltip != null) {
	    cond_value += '<span class="has-popover glyphicon glyphicon-info-sign" data-placement="top" data-toggle="popover" title="' + condition.tgd_c_name + '" data-content="' + condition.tgd_c_tooltip + '"></span><br />';
	}

	// Indicate whether or this this is an objective or a condition.
	if (condition.tgd_c_type == 'obj')
	    cond_value += '<span class=condition-label>Milestones</span><br />';
	else {
	    cond_value += '<span class=condition-label>Condition ' + cond_num + '</span><br />';
	    cond_num += 1;
	}

	// Each condition label contains the condition name and the converstion rate.
	cond_value += '<span class="condition-name">' + condition.tgd_c_name + '</span>';
	cond_value += '</br>';

	$('#group-table tbody.main-table')
	    .append($('<tr id="cond-' + condition.tgd_c_id + '">')
		    .append($('<td class="text-center">')
			    .append(cond_value)));

	// Create a cell for each condition's period and display the cell contents.
	for (var period = 1; period <= 6; period++) {
	    var id = condition.tgd_c_id + '-' + period;
	    $('#group-table tbody.main-table tr#cond-' + condition.tgd_c_id)
		.append($('<td class="text-center cell-' + row + '-' + period + ' col-' + period + '" id="cell-' + id + '">'));

	    // Display the cell
	    displayCell(id, group, period, condition, group.tgd_g_id, cond_num);
	}
	row += 1;
    });

    // Calculate and display the needed values.
    for (var row = 1; row <= group.conditions.length; row++) {
	for (var col = 1; col <= 6; col++) {
	    var format;
	    var target;
	    if (row == 1) {
		target = numeral($('#target-'+row+'-'+col).html());
		format = '$0,0';
		if (col == 1 || col == 2 || col == 6) {
		    $('#needed-'+row+'-'+col).html(numeral(target - numeral($('#actual-'+row+'-'+col).html())).format(format));
		}
		else {
		    var needed = target - numeral($('#actual-'+row+'-'+col).html());
		    needed += numeral($('#needed-'+row+'-'+(col-1)).html());
					 
		    $('#needed-'+row+'-'+col).html(numeral(needed).format(format));
		}
	    }
	    else {
		target = numeral($('#target-'+row+'-'+col).html()) + numeral($('#target-t-'+row+'-'+col).html());
		format = '0,0'
		$('#needed-'+row+'-'+col).html(numeral(target - numeral($('#actual-'+row+'-'+col).html())).format(format));
	    }
	}
    }

    // Update the actuals for the objective to be cumulative for columns 3 through 5.
    for (var col = 3; col <= 5; col++) {
	var format;
	format = '$0,0';
	var actual = numeral($('#actual-1-'+col).html()) + numeral($('#actual-1-'+(col-1)).html());
	$('#actual-1-'+col).html(numeral(actual).format(format));
    }

    // Draw the TGD table footers. Show the appropriate "Finalize" and "Allow Updates" button.
    $('#group-table tfoot.main-table').empty();
    $('#group-table tfoot.main-table').append($('<tr>')
					    .append($('<td>')));
    for (var period = 1; period <= 6; period++) {
	if (period == group.tgd_g_periods_final)
	    $('#group-table tfoot.main-table tr').append($('<td class="text-center col-' + period + '">')
						       .append($('<button type="button" class="btn btn-gray" id="allow">')
							       .append('Reopen Quarter'))
						       .append($('<span class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover" data-placement="bottom" title="Allow Updates to Period" data-content="This button reversizes the finalization of this quarter and allows updates." style="margin-left: 5px;">'))
						      );
	else if (period == (parseInt(group.tgd_g_periods_final) + 1)) {
	    $('#group-table tfoot.main-table tr').append($('<td class="text-center col-' + period + '">')
						       .append($('<button type="button" class="btn btn-gray" id="finalize">')
							       .append('Finalize Quarter'))
						       .append($('<span class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover" data-placement="bottom" title="Finalize Period" data-content="This allows you ro finalize a quarter after it has ended." style="margin-left: 5px">'))
						       .append('<br>')
						       .append($('<button type="button" class="btn btn-gray top-buffer propagate-fgt" id="propagate-' + period + '">')
							       .append('Propagate FGT Values'))
						       .append($('<span class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover" data-placement="bottom" title="Propagate FTG" data-content="Set the future Quarter\'s FTG to match this quarters." style="margin-left: 5px">'))
						      );
	}
	else if (period < group.tgd_g_periods_final)
	    $('#group-table tfoot.main-table tr').append($('<td class="text-center col-' + period + '">')
						       .append('Finalized')
						       .append($('<span class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover" data-placement="bottom" title="Finalized" data-content="This quarter has been finalized.  To reopen it, you have to first reopen the later quarters." style="margin-left: 5px">'))
						      );
	else
	    $('#group-table tfoot.main-table tr').append($('<td>')
						       .append(''));
    }

    // Show the finalize line
    //$('.col-' + group.tgd_g_periods_final).css('border-right', '5px ridge');

    // Link the editible classes to the editable plugin.
    setEditable();
    useTooltip();
    usePopover();
    enableDoubleScroll('#group-div');
}

// Display the specified cell.
function displayCell(id, group, period, condition, g_id, cond_num) {

    // Get the cells.
    var cells = group.cells;

    // Get the condition ID from the condition.
    var condition_id = condition.tgd_c_id;

    // Get the Unit of Measurement and set the cell's class appropriately.
    var format = '0,0';
    if (condition.tgd_c_uom == 'd')
	format = '$0,0';

    // Convert period to quarter
    var quarter = 4;
    var placement = 'right';
    if (period == 2)
	quarter = 1;
    else if (period == 3)
	quarter = 2;
    else if (period == 4)
	quarter = 3;
    else if (period == 6) {
	quarter = 1;
	placement = 'left';
    }

    // Set the label to be either Actual or Projected based on the number of periods finalized.
    var actual_label = 'Actual';
    if ((parseInt(group.tgd_g_periods_final) + 1) < period)
	actual_label = 'Projected';	

    // Objective cells have a total and different headings.
    if (condition.tgd_c_type == 'obj') {
	$('#cell-' + id).append($('<table class="table table-condensed cell-table" id="table-' + id + '">')
				.append($('<thead>')
					.append($('<tr>')
						.append($('<th class="text-center" colspan="4">')
							.append('FTG'))
						.append($('<th class="text-center" colspan="4">')
							.append('Amount'))
						.append($('<th class="text-center" colspan="4">')
							.append('Cumulative'))))
				.append($('<tbody>')
					.append($('<tr>')
						.append($('<td class="shaded" colspan="4">')
							.append('F'))
						.append($('<td id="floor-' + id + '" colspan="4">')
							.append(numeral(0).format(format)))
						.append($('<td id="floor-t-' + id + '" colspan="4">')
							.append(numeral(0).format(format))))
					.append($('<tr>')
						.append($('<td class="shaded" colspan="4">')
							.append('T'))
						.append($('<td id="target-' + id + '" colspan="4">')
							.append(numeral(0).format(format)))
						.append($('<td id="target-t-' + id + '" colspan="4">')
							.append(numeral(0).format(format))))
					.append($('<tr>')
						.append($('<td class="shaded" colspan="4">')
							.append('G'))
						.append($('<td id="game-' + id + '" colspan="4">')
							.append(numeral(0).format(format)))
						.append($('<td id="game-t-' + id + '" colspan="4">')
							.append(numeral(0).format(format)))))
				.append($('<tfoot>')
					.append($('<tr>')
						.append($('<td class="shaded" colspan="3">')
							.append(actual_label))
						.append($('<td id="actual-' + id + '" colspan="3">')
							.append(numeral(0).format(format)))
						.append($('<td class="shaded" colspan="3">')
							.append('Needed'))
						.append($('<td id="needed-' + id + '" colspan="3">')
							.append(numeral(0).format(format))))
				       )
			       );

    }
    else {
	$('#cell-' + id).append($('<table class="table table-condensed cell-table" id="table-' + id + '">')
				.append($('<thead>')
					.append($('<tr>')
						.append($('<th class="text-center" colspan="2">')
							.append('FTG'))
						.append($('<th class="text-center" colspan="2">')
							.append('Count'))))
				.append($('<tbody>')
					.append($('<tr>')
						.append($('<td class="shaded" colspan="2">')
							.append('F'))
						.append($('<td colspan="2">')
							.append($('<span id="floor-' + id + '">'))))
					.append($('<tr>')
						.append($('<td class="shaded" colspan="2">')
							.append($('<span>')
								.append('T')))
						.append($('<td colspan="2">')
							.append($('<span id="target-s-' + id + '">'))
							.append($('<span id="target-' + id + '">'))
							.append($('<span id="target-t-' + id + '">'))))
					.append($('<tr>')
						.append($('<td class="shaded" colspan="2">')
							.append('G'))
						.append($('<td colspan="2">')
							.append($('<span id="game-' + id + '">')))))
				.append($('<tfoot>')
					.append($('<tr>')
						.append($('<td class="shaded">')
							.append(actual_label))
						.append($('<td id="actual-' + id + '">')
							.append(numeral(0).format(format)))
						.append($('<td class="shaded">')
							.append('Needed'))
						.append($('<td id="needed-' + id + '">')
							.append(numeral(0).format(format)))
					       )
				       )
			       );
    }

    // Do we have any values for the cell?
    if (typeof cells[period] != 'undefined' && typeof cells[period][condition_id] !== 'undefined') {
	var cell = cells[period][condition_id];

	$('#floor-' + id).html(numeral(cell.tgd_ic_floor).format(format));
	$('#game-' + id).html(numeral(cell.tgd_ic_game).format(format));
	if (cell.tgd_ic_target_t != '0') {
	    $('#target-s-' + id).html(numeral(parseInt(cell.tgd_ic_target_p)+parseInt(cell.tgd_ic_target_t)).format(format) + ' [').show();
	    $('#target-' + id).html(numeral(cell.tgd_ic_target_p).format(format));
	    $('#target-t-' + id).html(numeral(cell.tgd_ic_target_t).format('+0,0') + ']')
	}
	else {
	    $('#target-s' + id).html(numeral(cell.tgd_ic_target_p).format(format)).hide();
	    $('#target-' + id).html(numeral(cell.tgd_ic_target_p).format(format));
	}
	$('#actual-' + id).html(numeral(cell.tgd_ic_actual).format(format));

	// If the condition type is Objective, add the value to the previous total.
	if (condition.tgd_c_type == 'obj') {

	    // If this is the 1st, 2nd or 6th period, the total is just the value.
	    var floor_total = numeral(cell.tgd_ic_floor);
	    var target_total = numeral(cell.tgd_ic_target_p);
	    var game_total = numeral(cell.tgd_ic_game);

	    // If this is the 1st period, the cumulative comes from the User Instance.
	    if (period == 1) {
		floor_total = numeral(group.tgd_g_py_cum_floor);
		target_total = numeral(group.tgd_g_py_cum_target);
		game_total = numeral(group.tgd_g_py_cum_game);
	    }
	    else if (period == 3 || period == 4 || period == 5) {
		floor_total = numeral(cell.tgd_ic_floor) + numeral($('#floor-t-' + condition.tgd_c_id + '-' + (period -1)).html());
		target_total = numeral(cell.tgd_ic_target_p) + numeral($('#target-t-' + condition.tgd_c_id + '-' + (period -1)).html());
		game_total = numeral(cell.tgd_ic_game) + numeral($('#game-t-' + condition.tgd_c_id + '-' + (period -1)).html());
	    }

	    $('#floor-t-' + id).html(numeral(floor_total).format(format));
	    $('#target-t-' + id).html(numeral(target_total).format(format));
	    $('#game-t-' + id).html(numeral(game_total).format(format));
	}
    }
}
