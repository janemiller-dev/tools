let current_year, total_cum, tgd_year, cpd_id;

$(document).ready(function () {
    refreshPage();
    var tgd_table = $('#tgd-table');
    // Handle the finalize and allow buttons.
    tgd_table.on("click", '#allow', function () {
        // Get the ID from the URL
        var path = window.location.pathname;
        var components = path.split('/');
        var id = components[components.length - 1];

        $.ajax({
            url: jsglobals.base_url + "tgd/allow_period",
            dataType: "json",
            type: 'post',
            data: {
                id: id
            }
        }).done(function (data) {
            if (data.status == 'failure') {
                toastr.error(data.message);
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            refreshPage();
        }).fail(function (jqXHR, status) {
            toastr.error("Error");
        });
    });

    tgd_table.on("click", '#finalize', function () {
        // Get the ID from the URL
        var path = window.location.pathname;
        var components = path.split('/');
        var id = components[components.length - 1];

        $.ajax({
            url: jsglobals.base_url + "tgd/finalize_period",
            dataType: "json",
            type: 'post',
            data: {
                id: id
            }
        }).done(function (data) {
            if (data.status == 'failure') {
                toastr.error(data.message);
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            // Remove TGD glider position.
            localStorage.removeItem('glider_position');
            refreshPage();
        }).fail(function (jqXHR, status) {
            toastr.error("Error");
        });
    });

    // Locks a quarter.
    tgd_table.on('click', '#lock', function () {

        var leap = $(this).hasClass('leap');

        toastr.warning("<br /><button type='button' id='confirmationYes' class='btn btn-default'>Yes</button>" +
            "<button type='button' id='confirmationNo' class='btn btn-default' style='margin-left: 5px'>No</button>",
            'Are you sure you want to lock this quarter?<br /> You won\’t be able to make further changes in the future.',
            {
                timeOut: 0,
                extendedTimeOut: 0,
                preventDuplicates: true,
                tapToDismiss: false,
                closeButton: true,
                allowHtml: true,
                onShown: function (toast) {
                    $("#confirmationYes").click(function () {
                        // Get the ID from the URL
                        var path = window.location.pathname;
                        var components = path.split('/');
                        var id = components[components.length - 1];
                        var lock_period = makeAjaxCall('tgd/lock_period', {id: id, leap: leap, year: tgd_year});

                        lock_period.then(function (data) {

                            // Get the newly inserted TGD ID.
                            const instance_id = data.tgd_data.tgd_instance_id;

                            // Checks if this was the last quarter of the year if 'YES' new TGD instance is opened in a new tab.
                            if (undefined !== instance_id) {
                                var address = jsglobals.base_url + "tgd/tgd/" + instance_id;
                                var newwin = window.open(address, '_blank');

                                // Check if poop ups are allowed or not.
                                newwin ? newwin.focus() : toastr.error('Please allow popups for this website');
                            } else {
                                refreshPage();
                            }

                            // Close the Toastr.
                            $('.toast-close-button').trigger('click');
                        })
                    });

                    $('#confirmationNo').click(function () {
                        $('.toast-close-button').trigger('click');
                    })
                }
            });
    });

    tgd_table.on("click", '.propagate-fgt', function (event) {
        // Get the ID from the URL
        var path = window.location.pathname;
        var components = path.split('/');
        var tgd_id = components[components.length - 1];

        // Get the period being propagated from the ID.
        var event_id = event.target.id;
        components = event_id.split('-');
        var period = components[1];

        $.ajax({
            url: jsglobals.base_url + "tgd/propagate_ftg",
            dataType: "json",
            type: 'post',
            data: {
                id: tgd_id,
                'period': period
            }
        }).done(function (data) {
            if (data.status == 'failure') {
                toastr.error(data.message);
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            refreshPage();
        }).fail(function (jqXHR, status) {
            toastr.error("Error");
        });
    });


    // Click handler for move glider right
    tgd_table.on("click", '.move_glider_right', function (event) {

        // Set the value in localStorage.

        if (4 > parseInt(localStorage.glider_position)) {
            localStorage.setItem('glider_position', parseInt(localStorage.glider_position) + 1);
            draw_glider();
        }
    });

    // Click handler for move glider left
    tgd_table.on("click", '.move_glider_left', function (event) {

        // Set the value in localStorage.
        if (-2 < parseInt(localStorage.glider_position)) {
            localStorage.setItem('glider_position', parseInt(localStorage.glider_position) - 1);
            draw_glider();
        }
    });


    tgd_table.on("click", '.adjust-target', function () {
        var ui = $(this).data('ui'),
            period = $(this).data('period'),
            condition_id = $(this).data('condition'),
            direction = $(this).data('direction');


        // Check if the user wants to move target to next year.
        if ((5 === period) && ('right' === direction)) {

            toastr.warning("<br /><button type='button' id='confirmationYes' class='btn btn-default'>Yes</button>" +
                "<button type='button' id='confirmationNo' class='btn btn-default' style='margin-left: 5px'>No</button>",
                'Do you want to create TGD instance for next year?.',
                {
                    timeOut: 0,
                    extendedTimeOut: 0,
                    preventDuplicates: true,
                    tapToDismiss: false,
                    closeButton: true,
                    allowHtml: true,
                    onShown: function (toast) {
                        $("#confirmationYes").click(function () {
                            // Get the ID from the URL
                            const path = window.location.pathname,
                                components = path.split('/'),
                                id = components[components.length - 1],
                                leap_tgd = makeAjaxCall('tgd/leap_tgd',
                                    {
                                        id: id,
                                        year: tgd_year
                                    });

                            leap_tgd.then(function (data) {

                                // Get the newly inserted TGD ID.
                                const instance_id = data.tgd_data.tgd_instance_id;

                                // Checks if this was the last quarter of the year if 'YES' new TGD instance is opened in a new tab.
                                if (undefined !== instance_id) {
                                    var address = jsglobals.base_url + "tgd/tgd/" + instance_id;
                                    var newwin = window.open(address, '_blank');

                                    // Check if poop ups are allowed or not.
                                    newwin ? newwin.focus() : toastr.error('Please allow popups for this website');
                                } else {
                                    refreshPage();
                                }

                                // Close the Toastr.
                                $('.toast-close-button').trigger('click');
                            })
                        });

                        $('#confirmationNo').click(function () {
                            $('.toast-close-button').trigger('click');
                        })
                    }
                });

        }

        $.ajax({
            url: jsglobals.base_url + "tgd/adjust_target",
            dataType: "json",
            type: 'post',
            data: {
                ui: ui,
                period: period,
                condition_id: condition_id,
                direction: direction
            }
        }).done(function (data) {
            if (data.status == 'failure') {
                toastr.error(data.message);
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            refreshPage(true);
        }).fail(function (jqXHR, status) {
            toastr.error("Error");
        });

    });

    // Toggle the controls.
    $('#tool-buttons').on("click", '#toggle-controls', function () {
        $('.ftg-control').toggle();
    });


    /*
    // Print as a PDF.
    $('#tool-buttons').on("click", '#print-pdf', function() {
	var path = window.location.pathname;
	var components = path.split('/');
	var ui_id = components[components.length - 1];

	var address = jsglobals.base_url + 'tgd/print_pdf/' + ui_id;
	window.location = address;
    });
    */
});

// Get the page data and display it.
function refreshPage(togglesOn) {

    // Get the ID from the URL
    var path = window.location.pathname;
    var components = path.split('/');
    var id = components[components.length - 1];

    current_year = (window.location.search.split('?y=')[1] && (0 != window.location.search.split('?y=')[1])) ?
        parseInt(window.location.search.split('?y=')[1]) : parseInt(0);

    // Get the subscription status.
    $.ajax({
        url: jsglobals.base_url + "tgd/get_tgd",
        dataType: "json",
        type: 'post',
        data: {
            id: id,
            year: current_year
        }
    }).done(function (data) {
        if (data.status == 'failure') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
            window.location.href = data.redirect;
            return;
        }
        updateTGD(data.tgd);
        if (typeof togglesOn !== 'undefined' && togglesOn == true) {
            $('.ftg-control').show();
        }
        else
            $('.ftg-control').hide();

    }).fail(function (jqXHR, status) {
        toastr.error("Error");
    });
}

// Update the TGD
function updateTGD(tgd) {
    tgd_year = parseInt(tgd.tgd_ui_year);
    total_cum = 0;
    current_year = tgd_year;

    const next_year_selector = $('#next-year-tgd'),
        prev_year_selector = $('#previous-year-tgd'),
        next_prev_year_selector = $('#next-year-tgd, #previous-year-tgd');

    // Check if previous year TGD instance exist.
    (null !== tgd.tgd_ui_next_year_id) ? next_year_selector.removeClass('hidden') :
        next_year_selector.addClass('hidden');

    // Check if next year TGD instance exist.
    (null !== tgd.tgd_ui_prev_year_id) ? prev_year_selector.removeClass('hidden') :
        prev_year_selector.addClass('hidden');

    var path = window.location.pathname;

    prev_year_selector
        .css('display', 'inline-block')
        .attr('href', jsglobals.base_url + 'tgd/tgd/' + tgd.tgd_ui_prev_year_id);

    next_year_selector
        .css('display', 'inline-block')
        .attr('href', jsglobals.base_url + 'tgd/tgd/' + tgd.tgd_ui_next_year_id);

    // Unset the glider position if Next/Prev Year is clicked.
    next_prev_year_selector.unbind('click');
    next_prev_year_selector.click(function () {
        // Remove TGD glider position.
        localStorage.removeItem('glider_position');
    });


    $('#tgd-year').html(tgd.tgd_ui_year);

    // Set active DMD, if theres one.
    if (null === tgd.cpd_name) {
        $('#linked-cpd').html('No DMD');
    } else {
        var cpd_name = tgd.cpd_name;
        cpd_id = tgd.tgd_cpd_id;
        $('#linked-cpd').html(cpd_name.link(jsglobals.base_url + 'cpdv1/cpdv1/' + cpd_id));
    }

    // Update the title.
    $('#tgd-name').val(tgd.tgd_ui_name)
        .attr({
            'data-url': jsglobals.base_url + 'tgd/update_instance',
            'data-name': 'tgd_ui_name',
            'data-pk': tgd.tgd_ui_id,
            'data-type': 'text',
            'title': 'New Name',
        });

    // Update the mission.
    $('#tgd-mission').val(tgd.tgd_ui_mission)
        .attr({
            'data-url': jsglobals.base_url + 'tgd/update_instance',
            'data-name': 'tgd_ui_mission',
            'data-pk': tgd.tgd_ui_id,
            'data-type': 'textarea',
            'title': 'Update Mission',
        });

    $('#tgd-product-type').val(tgd.u_product_name);

    // Show the floor, target and game.
    $('#year-floor').val(numeral(tgd.objective[0] ? tgd.objective[0].uo_floor : tgd.tgd_ui_floor).format('$0,0'));
    $('#year-target').val(numeral(tgd.objective[0] ? tgd.objective[0].uo_target : tgd.tgd_ui_target).format('$0,0'));
    $('#year-game').val(numeral(tgd.objective[0] ? tgd.objective[0].uo_game : tgd.tgd_ui_game).format('$0,0'));


    // Show the default check size
    /*
    $('#tgd-check-size').html(numeral(tgd.tgd_ui_default_check_size).format('$0,0')).addClass('editable')
	.attr({
	    'data-url': jsglobals.base_url + 'tgd/update_instance',
	    'data-name': 'tgd_ui_default_check_size',
	    'data-pk': tgd.tgd_ui_id,
	    'data-type': 'number',
	    'title': 'Default Check Size',
	});
    */

    /*
    // Add the print PDF button to the button list if it is not already there.
    if (!$('#print-pdf').length) {
	$('#tool-buttons').append($('<button type="button" class="btn btn-primary has-tooltip" id="print-pdf" data-id="' + tgd.tgd_ui_id + '" data-toggle="tooltip" title="Save a copy of the instance as a PDF" data-placement="top">')
				  .append('Print PDF'));
    }
    */

    // Add the Save button to the button list if it is not already there.
    if (!$('#save-instance').length) {
        $('#tool-buttons').append($('<button type="button" class="btn btn-primary has-tooltip" id="save-instance"' +
            ' data-toggle="modal" data-target="#copy-instance-dialog" data-id="' + tgd.tgd_ui_id + '">' +
            '<span class="has-tooltip" data-toggle="tooltip" title="Copy this instance to a new version"' +
            ' data-placement="top">Save Instance</span></button>'));
    }

    // Add the toggle controls to the button list if it is not already there.
    if (!$('#toggle-controls').length) {
        $('#tool-buttons').append($('<button type="button" class="btn btn-primary has-tooltip" id="toggle-controls"' +
            ' data-toggle="tooltip" title="Show or hide the adjustment controls" data-placement="top">')
            .append('Set Toggle'));
    }

    // Add the undo toggle to the button list if it is not already there.
    if (!$('#undo-toggle').length) {
        $('#tool-buttons').append($('<button type="button" class="btn btn-primary has-tooltip" id="undo-toggle" ' +
            'data-toggle="tooltip" title="Undo Previous  Push, Pull, Lift, and Drop operations." data-placement="top">')
            .append('Undo Toggle'));
    }

    // Add the redo toggle controls to the button list if it is not already there.
    if (!$('#redo-toggle').length) {
        $('#tool-buttons').append($('<button type="button" class="btn btn-primary has-tooltip" id="redo-toggle" ' +
            'data-toggle="tooltip" title="Redo Previous  Push, Pull, Lift, and Drop operations." data-placement="top">')
            .append('Redo Toggle'));
    }

    // Add the Toogle History button to the button list if it is not already there.
    if (!$('#toggle-history').length) {
        $('#tool-buttons').append($('<button type="button" class="btn btn-primary" ' +
            'id="toggle-history" data-toggle="modal" data-year="' + tgd.tgd_ui_year + '"' +
            'data-target="#toggle-history-dialog" data-id="' + tgd.tgd_ui_id + '"' +
            ' data-locked="' + tgd.tgd_ui_periods_lock + '">' +
            '<span class="has-tooltip" data-toggle="tooltip" title="Archive of all Push, Pull, Lift, and Drop' +
            ' operations." data-placement="top">Toggle History</span></button>'));
    }

    // Add the Metrics button to the button list if it is not already there.
    if (!$('#tgd-metrics').length) {
        $('#tool-buttons').append($('<button type="button" class="btn btn-primary pull-right" id="tgd-metrics"' +
            ' data-target="#view-tgd-metrics-dialog" data-toggle="modal">' +
            '<span class="has-tooltip" ' +
            'data-toggle="tooltip" title="Collaborate with Mentor/Manger." ' +
            'data-placement="top">Metrics</span>' +
            '</button>'));
    }

    // Add the Collaboration button to the button list if it is not already there.
    if (!$('#tgd-collaboration').length) {
        $('#tool-buttons').append($('<button type="button" class="btn btn-primary pull-right" id="tgd-collaboration"' +
            ' data-target="#view-collaboration-dialog" data-toggle="modal" data-id="' + tgd.tgd_ui_id + '">' +
            '<span class="has-tooltip" ' +
            'data-toggle="tooltip" title="Collaborate with Mentor/Manger." data-placement="top">Collaborate</span>' +
            '</button>'));
    }

    // Add the ACM button to the button list if it is not already there.
    if (!$('#tgd-acm').length) {
        $('#tool-buttons').append($('<button type="button" class="btn btn-primary pull-right" id="tgd-acm" data-id="'
            + tgd.tgd_ui_id + '"><span class="has-tooltip" data-toggle="tooltip" title="Accountability Manager." ' +
            'data-placement="top">ACM</span></button>'));
    }

    // Add the Linked TGD button to the button list if it is not already there.
    if (!$('#tgd-linked').length) {
        $('#tool-buttons').append($('<button type="button" class="btn btn-primary pull-right" id="tgd-linked" ' +
            'data-toggle="modal" data-target="#view-linked-tgd-dialog" data-id="' + tgd.tgd_ui_id + '">' +
            '<span class="has-tooltip" data-toggle="tooltip" title="Linked TGD instance." ' +
            'data-placement="top">Linked TGD</span></button>'));
    }


    // Are there other instances?
    // if (tgd.other_instances.length !== 0) {
    //     $('#open-instance-id').empty();
    //
    //     if (!$('#open-instance').length) {
    //         $('#tool-buttons').append($('<button type="button" class="btn btn-primary has-tooltip" id="open-instance" data-toggle="modal" data-target="#open-instance-dialog" data-id="' + tgd.tgd_ui_id + '"><span class="has-tooltip" data-toggle="tooltip" title="Open a difference instance." data-placement="top">Open Instance</span></button>'));
    //     }
    //
    //     $.each(tgd.other_instances, function (i, instance) {
    //         $('#open-instance-id').append($('<option value="' + instance.tgd_ui_id + '">')
    //             .append(instance.tgd_ui_name))
    //     });
    // }
    //
    // // Add the shre button
    // if (!$('#share-button').length) {
    //     $('#tool-buttons').append($('<button type="button"lass="btn btn-primary has-tooltip" id="share-button" data-toggle="modal" data-target="#share-dialog" data-id="' + tgd.tgd_ui_id + '" data-object="tgd"><span class="has-tooltip" data-toggle="tooltip" title="Share the TGD with other members.." data-placement="top">Share</span></button>'));
    // }

    // Add the Save button to the button list if it is not already there.
    /*
    if (!$('#extend-instance').length) {
	$('#tool-buttons').append($('<button type="button" class="btn btn-primary has-tooltip" id="extend-instance" data-toggle="modal" data-target="#extend-instance-dialog" data-id="' + tgd.tgd_ui_id + '"><span class="has-tooltip" data-toggle="tooltip" title="Extend this instance to the next year." data-placement="top">Extend Instance</span></button>'));
    }
    */

    // Update the save as drop down selection.
    copyInstanceUpdateInstances(tgd.other_instances);

    // Draw the TGD column headers
    $('#tgd-table thead.main-table').empty();
    $('#tgd-table thead.main-table').append($('<tr>')
        .append($('<th class="text-center">')
            .append('')));

    // Determin the number of periods to display.
    var num_periods = tgd.cells.length;

    // Draw the period date headers.
    var date = moment().year(tgd.tgd_ui_year).dayOfYear(1).subtract(1, 'day');
    for (var period = 1; period <= 6; period++) {
        var next_quarter = new moment(date);
        next_quarter.add(1, 'quarter');


        // Add referral fees to other income.
        if (undefined !== tgd.ref) {
            tgd.oi_total[period] = parseInt(tgd.oi_total[period]) +
                parseInt(null !== tgd.ref[period][0].ref ? tgd.ref[period][0].ref : 0);
        }

        // Add Buyer fees to other income.
        if (undefined !== tgd.buyer) {
            tgd.oi_total[period] = parseInt(tgd.oi_total[period]) +
                parseInt(null !== tgd.buyer[period][0].buyer ? tgd.buyer[period][0].buyer : 0);
        }


        // Display a message based on teh quarter.
        var placement = 'right';
        var percent = tgd.tgd_ui_tgt_q1_pct;
        var objective = tgd.tgd_ui_tgt_q1_obj;
        if (period == 2) {
            percent = tgd.tgd_ui_tgt_q2_pct;
            objective = tgd.tgd_ui_tgt_q2_obj;
        }
        else if (period == 3) {
            percent = tgd.tgd_ui_tgt_q3_pct;
            objective = tgd.tgd_ui_tgt_q3_obj;
        }
        else if (period == 4) {
            percent = tgd.tgd_ui_tgt_q4_pct;
            objective = tgd.tgd_ui_tgt_q4_obj;
        }
        else if (period == 5) {
            percent = tgd.tgd_ui_tgt_q5_pct;
            objective = tgd.tgd_ui_tgt_q5_obj;
        }
        else if (period == 6) {
            percent = tgd.tgd_ui_tgt_q6_pct;
            objective = tgd.tgd_ui_tgt_q6_obj;
            placement = 'left';
        }
        percent = percent / 100;

        // Display a message based on teh quarter.
        q_msg = 'Make <span class="editable" data-url="' + jsglobals.base_url + 'common/update_db_field" ' +
            'data-name="tgd_user_instance.tgd_ui_tgt_q' + period + '_pct" data-pk="' + tgd.tgd_ui_id + '" ' +
            'data-type="number" data-title="Set the percentage">' + numeral(percent).format('0%') +
            '</span> of <span class="editable" data-url="' + jsglobals.base_url + 'common/update_db_field"' +
            ' data-name="tgd_user_instance.tgd_ui_tgt_q' + period + '_obj" data-pk="' + tgd.tgd_ui_id + '"' +
            ' data-type="select" data-source="' + jsglobals.base_url + 'code/get_codes/tgdotype"' +
            ' data-value=' + objective + ' data-title="Set the Objective"></span>';

        // The header cell contents includes the Quarter, Date, and all multipliers.
        var col_header = '<div><em>' + q_msg + '</em></div><span style="font-size: 1.3em">Q' + date.quarter() + ' ' +
            date.year() + '&nbsp;</span><span class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover"' +
            ' title="Q' + date.quarter() + ' milestones" data-html="true" style="font-size: 14px" ' +
            'data-placement="right" data-content="Q' + date.quarter() + ' milestones feed next Q' +
            next_quarter.quarter() + ' results. Quarterly Metrics:<br/><strong>Average Check Size:</strong>' +
            ' Your average check size.</br><strong>Double End Factor:</strong> Percentage of deals both sides.' +
            '<br/><strong>Other Income:</strong> Dollar amount added on to actual."></span><br />';
        col_header += date.endOf('quarter').format('MMM DD, YYYY') + '</span>';

        // Get the multiplier name (if any);
        $.each(tgd.multipliers, function (m, multiplier) {
            col_header += '<br />' + multiplier.tgd_m_name + ': ';
            var format = '0,0';
            if (multiplier.tgd_m_type == 'dollar')
                format = '$0,0';
            else if (multiplier.tgd_m_type == 'percent')
                format = '0%';

            col_header += '<span class="editable" data-url="' + jsglobals.base_url + 'tgd/update_multiplier"' +
                ' data-name="tgd_im_multiple" data-pk="' + tgd.tgd_ui_id + '-' + multiplier.tgd_m_id + '-' + period + '"' +
                ' data-type="number" data-title="Update ' + multiplier.tgd_m_name + '">' +
                numeral(tgd.mcells[period][multiplier.tgd_m_id].tgd_im_multiple).format(format) + '</span>';
        });


        // Add the other income total
        col_header += '<br />Other Income:';
        col_header += '<span><button type="button" class="btn-link" id="other-income-button" data-toggle="modal"' +
            ' data-target="#update-other-income-dialog" data-id="' + tgd.tgd_ui_id + '" data-qtr="' + period + '">' +
            '<span class="has-tooltip editable" data-toggle="tooltip" title="Click to update other income."' +
            ' data-placement="top">' + numeral(tgd.oi_total[period]).format('$0,0') + '</span></button></span>';

        // Show the header
        $('#tgd-table thead.main-table tr').append($('<th class="text-center col-' + period + '">')
            .append(col_header));

        // Increment to the next quarter.
        date.add(1, 'quarter');
    }

    // Create the rows and columns for each condition.
    $('#tgd-table tbody.main-table').empty();
    var row = 1;
    var cond_num = 1;

    $.each(tgd.conditions, function (c, condition) {

        var cond_value = '';

        // Show the tool tip if there is one.
        if (condition.tgd_c_tooltip != null) {
            cond_value += '<span class="has-popover glyphicon glyphicon-info-sign" data-placement="top"' +
                ' data-toggle="popover" title="' + condition.tgd_c_name + '" ' +
                'data-content="' + condition.tgd_c_tooltip + '"></span><br />';
        }

        // Indicate whether or this this is an objective or a condition.
        if (condition.tgd_c_type == 'obj')
            cond_value += '<span class=condition-label>Milestones</span><br />';
        else {
            cond_value += '<span class=condition-label>Metric ' + cond_num + '</span><br />';
            cond_num += 1;
        }


        // Each condition label contains the condition name and the converstion rate.
        cond_value += '<span class="condition-label-name">' + condition.tgd_c_name + '</span><br>';

        cond_value += '<span class="condition-name editable" ' +
            'data-url="' + jsglobals.base_url + 'tgd/update_label_name' + '" ' +
            'data-pk="' + tgd.tgd_ui_id + '" data-name="tgd_ui_label_' + cond_num + '">' + tgd['tgd_ui_label_' + cond_num] + '</span>'

        // Clone FTG popover content.
        var dummy_content = $('.dummy_ftg_content').clone();
        dummy_content.removeClass('dummy_ftg_content');
        dummy_content.removeClass('hidden');
        dummy_content.find('#ftg-year').text('Year: ' + tgd.tgd_ui_year);

        // Check if the row is for milestone or conditions.
        if (condition.tgd_c_type != 'obj') {
            cond_value += '</br>';
            cond_value += 'Conversion Rate: <span id="cr-' + cond_num + '"' +
                ' class="editable" data-url="' + jsglobals.base_url + 'tgd/update_condition"' +
                ' data-name="tgd_icond_conversion_rate" data-pk="' + tgd.tgd_ui_id + '-' + condition.tgd_c_id + '"' +
                ' data-type="number" data-title="Update Conversion Rate"' +
                ' data-params="{locked: ' + tgd.tgd_ui_periods_lock + '}">' +
                numeral(condition.tgd_icond_conversion_rate).format('0%') + '</span>';

            let actual = 0;
            if (tgd.actual !== undefined) {
                for (var i = 2; i <= 5; i++)
                    actual += parseInt((tgd.actual[condition.tgd_c_id][i][0]).net);
            }

            const target = tgd['tgd_' + condition.tgd_c_col_name + '_t'];

            // Set FTG Values for Metric
            dummy_content.find('.floor').attr('value', tgd['tgd_' + condition.tgd_c_col_name + '_f'])
                .attr('data-col', condition.tgd_c_col_name + '_f');
            dummy_content.find('.target').attr('value', target)
                .attr('data-col', condition.tgd_c_col_name + '_t');
            dummy_content.find('.game').attr('value', tgd['tgd_' + condition.tgd_c_col_name + '_g'])
                .attr('data-col', condition.tgd_c_col_name + '_g');
            dummy_content.find('.actual').attr('data-col', condition.tgd_c_col_name + '_actual');


            const actual_field_val = (0 !== actual)
                ? actual : tgd['tgd_' + condition.tgd_c_col_name + '_actual'];

            // Set Actual/Needed/Percentage value for Metric.
            dummy_content.find('.actual')
                .attr('value', actual_field_val);

            const needed = target - actual_field_val,
                percent = (target != 0 ? (actual_field_val / target) * 100 : 0);

            dummy_content.find('.needed').attr('value', needed);

            dummy_content.find('.percent')
                .attr('value', ((percent + '%')));

        } else {
            let actual = 0;

            if (tgd.actual !== undefined) {
                for (var i = 2; i <= 5; i++) {
                    actual += parseInt((tgd.actual[condition.tgd_c_id][i][0]).net !== null
                        ? (tgd.actual[condition.tgd_c_id][i][0]).net : 0);
                }
            }
            const target = numeral($('#year-target').val())._value;

            dummy_content.find('.floor').prop('readonly', true);
            dummy_content.find('.target').prop('readonly', true);
            dummy_content.find('.game').prop('readonly', true);
            dummy_content.find('.floor').attr('value', ($('#year-floor').val()))
                .attr('data-col', condition.tgd_c_col_name + '_f');
            dummy_content.find('.actual').attr('data-col', condition.tgd_c_col_name + '_actual');


            dummy_content.find('.target').attr('value', ($('#year-target').val()))
                .attr('data-col', condition.tgd_c_col_name + '_t');

            dummy_content.find('.game').attr('value', ($('#year-game').val()))
                .attr('data-col', condition.tgd_c_col_name + '_g');

            const actual_field_val = (0 !== actual)
                ? actual : tgd['tgd_' + condition.tgd_c_col_name + '_actual'];

            // Set Actual/Needed/Percentage value for Metric.
            dummy_content.find('.actual')
                .attr('value', numeral(actual_field_val).format('$0,0'));

            const needed = target - actual_field_val,
                percent = (target != 0 ? (actual_field_val / target) * 100 : 0);

            dummy_content.find('.needed').attr('value', numeral(needed).format('$0,0'));

            dummy_content.find('.percent')
                .attr('value', ((percent + '%')));

        }

        cond_value += '<br><button id="tgd-ftg' + condition.tgd_c_id + '" class="has-popover tgd-ftg-button"' +
            ' data-html="true" data-toggle="popover" data-placement="right">FTG</button>';

        $('#tgd-table tbody.main-table')
            .append($('<tr id="cond-' + condition.tgd_c_id + '">')
                .append($('<td class="text-center">')
                    .append(cond_value)));


        $("#tgd-ftg" + condition.tgd_c_id).attr('data-content', dummy_content.html());

        // Create a cell for each condition's period and display the cell contents.
        var editable;
        for (var period = 1; period <= 6; period++) {
            var id = condition.tgd_c_id + '-' + period;
            $('#tgd-table tbody.main-table tr#cond-' + condition.tgd_c_id)
                .append($('<td class="text-center cell-' + row + '-' + period +
                    ' col-' + period + '" id="cell-' + id + '">'));

            // Is the cell fully editable?
            if (condition.tgd_c_prev_condition == null || period == 1)
                editable = true;
            else
                editable = false;

            // Display the cell
            displayCell(id, tgd, period, condition, tgd.tgd_ui_id, editable, cond_num);
        }
        row += 1;
    });

    var percent = total_cum / numeral($('#year-target').val())._value;

    // Set the Actual/Percent/Needed and Condition fields.
    $('#year-actual').val(numeral(total_cum).format('$0,0'));

    $('#year-percent').val((numeral(isNaN(percent === Infinity ? 0 : percent) ? 0 : percent)).format('0%'));

    $('#year-needed').val(numeral(numeral($('#year-target').val())._value - total_cum).format('$0,0'));
    $('#tgd-condition').val(tgd.tgd_ui_condition);

    $('#tgd-condition').change(function () {
        var update_condition = makeAjaxCall("tgd/update_condition_text", {
                id: tgd.tgd_ui_id,
                year: current_year,
                value: $(this).val()
            }
        );
    });

    // Calculate and display the needed values.
    for (var row = 1; row <= tgd.conditions.length; row++) {

        for (var col = 1; col <= 6; col++) {
            var format;
            var target;
            if (row == 1) {
                target = numeral($('#target-' + row + '-' + col).html());
                format = '$0,0';
                if (col == 1 || col == 2 || col == 6) {
                    $('#needed-' + row + '-' + col)
                        .html(numeral(target - numeral($('#actual-' + row + '-' + col).html())).format(format));
                }
                else {
                    var needed = target - numeral($('#actual-' + row + '-' + col).html());
                    needed += numeral($('#needed-' + row + '-' + (col - 1)).html());

                    $('#needed-' + row + '-' + col).html(numeral(needed).format(format));
                }
            }
            else {
                target = numeral($('#target-' + row + '-' + col).html()) +
                    numeral($('#target-t-' + row + '-' + col).html());
                format = '0,0'
                $('#needed-' + row + '-' + col)
                    .html(numeral(target - numeral($('#actual-' + row + '-' + col).html())).format(format));
            }
        }
    }

    // Update the actuals for the objective to be cumulative for columns 3 through 5.
    for (var col = 3; col <= 5; col++) {
        var format;
        format = '$0,0';
        var actual = numeral($('#actual-1-' + col).html()) + numeral($('#actual-1-' + (col - 1)).html());
        $('#actual-1-' + col).html(numeral(actual).format(format));
    }

    // Draw the TGD table footers. Show the appropriate "Finalize" and "Allow Updates" button.
    var table = $('#tgd-table tfoot.main-table');
    table.empty();
    table.append($('<tr>')
        .append($('<td>')));

    for (var period = 1; period <= 6; period++) {

        if ((period === parseInt(tgd.tgd_ui_periods_final)) &&
            (parseInt(tgd.tgd_ui_periods_final) === (parseInt(tgd.tgd_ui_periods_lock) + 1))) {
            if (5 === period) {
                table.find('tr').append($('<td class="text-center col-' + period + '">')
                    .append($('<button type="button" class="btn btn-default" id="allow">')
                        .append('REOPEN'))
                    .append($('<span class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover"' +
                        ' data-placement="bottom" title="Allow Updates to Period"' +
                        ' data-content="This button reversizes the finalization of this quarter and allows updates." ' +
                        'style="margin-right: 5px;">'))
                    .append($('<button type="button" class="btn btn-default leap lock-fgt" id="lock">')
                        .append('LEAP'))
                    .append($('<span class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover" ' +
                        'data-placement="bottom" title="Lock Period" data-content="This button allows you' +
                        ' to Lock the quarter which can\'t be edited." style="margin-right: 5px;">'))
                );
            } else {
                table.find('tr').append($('<td class="text-center col-' + period + '">')
                    .append($('<button type="button" class="btn btn-default" id="allow">')
                        .append('REOPEN'))
                    .append($('<span class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover"' +
                        ' data-placement="bottom" title="Allow Updates to Period"' +
                        ' data-content="This button reversizes the finalization of this quarter and allows updates."' +
                        ' style="margin-right: 5px;">'))
                    .append($('<button type="button" class="btn btn-default lock-fgt" id="lock">')
                        .append('LOCK'))
                    .append($('<span class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover"' +
                        ' data-placement="bottom" title="Lock Period"' +
                        ' data-content="This button allows you to Lock the quarter which can\'t be edited." ' +
                        'style="margin-right: 5px;">'))
                );
            }
        }
        else if (period === (parseInt(tgd.tgd_ui_periods_final) + 1)) {
            table.find('tr').append($('<td class="text-center col-' + period + '">')
                .append($('<button type="button" class="btn btn-default propagate-fgt" id="propagate-' + period + '">')
                    .append('PROP'))
                .append($('<span class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover"' +
                    ' data-placement="bottom" title="Propagate FTG" ' +
                    'data-content="Set the future Quarter\'s FTG to match this quarters." style="margin-right: 5px">'))

                .append($('<span class="has-tooltip" data-toggle="tooltip" title="Move Glider Left"' +
                    ' data-placement="top">' +
                    ' <span class="glyphicon glyphicon-arrow-left move_glider_left"' +
                    ' data-direction="left" style="margin-right: .5vw; margin-left: 1vw"' +
                    ' data-glider-position="' + tgd.tgd_ui_periods_final + '"></span></span>'))

                .append($('<span class="has-tooltip" data-toggle="tooltip" title="Move Glider Right"' +
                    ' data-placement="top">' +
                    ' <span class="glyphicon glyphicon-arrow-right move_glider_right"' +
                    ' data-direction="right" style="margin-right: 1vw;"' +
                    ' data-glider-position="' + tgd.tgd_ui_periods_final + '"></span></span>'))

                .append($('<button type="button" class="btn btn-default" id="finalize">')
                    .append('FIN'))
                .append($('<span class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover" ' +
                    'data-placement="bottom" title="Finalize Period"' +
                    ' data-content="This allows you to finalize a quarter after it has ended." style="margin-right: 5px">'))
            );
        }
        else if (period <= tgd.tgd_ui_periods_lock)
            table.find('tr').append($('<td class="text-center col-' + period + '">')
                .append('Locked')
                .append($('<span class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover" ' +
                    'data-placement="bottom" title="Locked"' +
                    ' data-content="This quarter has been locked.  You can\'t reopen it." style="margin-left: 5px">'))
            );
        else
            table.find('tr').append($('<td>')
                .append(''));
    }

    // Show the finalize line
    $('.col-' + tgd.tgd_ui_periods_final).css('border-right', '5px ridge');

    // Update the share list.
    if (tgd.shares == null || tgd.shares.length == 0) {
        $('#not-shared').show();
        $('#shared-list-div').hide();
    }
    else {
        $('#not-shared').hide();
        $('#shared-list-div').show();
        $('#shared-list').empty();
        $.each(tgd.shares, function (i, share) {
            $('#shared-list').append($('<li>')
                .append(share.user_email + ': ' + '<span class="editable" data-url="' + jsglobals.base_url +
                    'common/update_db_field" data-name="share.share_type" data-pk="' + share.share_id + '"' +
                    ' data-type="select" data-source="' + jsglobals.base_url + 'code/get_codes/sperm"' +
                    ' data-value=' + share.share_type + ' data-title="Change the permission"></span>'))
        });
    }

    // Show the finalize line
    $('.col-' + tgd.tgd_ui_periods_final).css('border-right', '3px ridge');

    // Check if the glider position is unset. If so set it to the final period
    (undefined === localStorage.glider_position) ?
        (localStorage.glider_position = tgd.tgd_ui_periods_final) : '';
    draw_glider();

    // Link the editible classes to the editable plugin.
    if (tgd.share_type == null || tgd.share_type == 'edit')
        setEditable();
    else
        clearEditable();
    useTooltip();
    usePopover();
    updateFTG(tgd.tgd_ui_id);

    $('.tgd-curtain').click(function () {

        // Check if Curtains are already present.
        if (0 !== $('.tgd-opaque').length) {
            $('.tgd-opaque').removeClass('tgd-opaque');
            return;
        }

        const period = $(this).attr('data-condition');
        $.each(tgd.conditions, function (index, condition) {
            for (let i = 1; i <= period; i++) {
                if ($('#cell-' + condition.tgd_c_id + '-' + i).hasClass('glider')) {
                    i = period;
                    continue;
                } else {
                    $('#cell-' + condition.tgd_c_id + '-' + i).addClass('tgd-opaque')
                }
            }
        });
    });
}

// Draws glider for viewing assistance.
function draw_glider() {

    $('.glider').css('background-color', "").removeClass('glider');
    $('.tgd-opaque').removeClass('tgd-opaque');

    for (var i = 0; i < 4; i++) {
        $('.cell-' + (4 - i) + '-' + (i + parseInt(localStorage.glider_position) + 1))
            .addClass('glider')
            .css('background-color', 'rgba(178, 34, 34, 0.5)');
    }
}

// Uopdate FTG values for TGD.
function updateFTG(ui_id) {

    // Updates TGD metric FTG values.
    $('.tgd-ftg-button').click(function () {

        // Check if any of metric FTG is changed.
        $('.tgd_ftg').add('.property_dropdown').change(function () {
            update_ftg($(this), ui_id);
        });
    });


    $('#undo-toggle').off('click');

    // Undo Toggle controls.
    $('#undo-toggle').click(function () {

        var undo_toggle = makeAjaxCall("tgd/undo_toggle", {ui: ui_id});

        undo_toggle.then(function () {
            toastr.success('Toggle Undone!!')
            refreshPage();
        });
    });

    $('#redo-toggle').off('click');
    $('#redo-toggle').click(function () {
        const redo_toggle = makeAjaxCall('tgd/redo_toggle', {ui: ui_id});

        redo_toggle.then(function () {
            toastr.success('Toggle Redone!!');
            refreshPage();
        })
    })

    $('.tgd_year_ftg').unbind('change');

    // Updates TGD yearly FTG values.
    $('.tgd_year_ftg').change(function () {
        update_ftg($(this), ui_id)
    });
}

// Updates FTG values for a TGD instance.
function update_ftg(that, ui_id) {
    var update_ftg = makeAjaxCall("tgd/update_ftg", {
            col: $(that).data('col'),
            id: ui_id,
            year: current_year,
            value: $(that).val()
        }
    );

    update_ftg.then(function () {
        refreshPage();
    });
}

// Display the specified cell.
function displayCell(id, tgd, period, condition, ui_id, editable, cond_num) {

    // Get the cells.
    var cells = tgd.cells;
    var actual = tgd.actual;
    var linked = tgd.linked;
    var oi_total = tgd.oi_total;

    // Get the condition ID from the condition.
    var condition_id = condition.tgd_c_id;

    // Get the Unit of Measurement and set the cell's class appropriately.
    var format = '0,0';
    if (condition.tgd_c_uom == 'd')
        format = '$0,0';

    // Convert period to quarter
    var quarter = 4;
    var placement = 'right';
    if (period == 2)
        quarter = 1;
    else if (period == 3)
        quarter = 2;
    else if (period == 4)
        quarter = 3;
    else if (period == 6) {
        quarter = 1;
        placement = 'left';
    }

    // Set the label to be either Actual or Projected based on the number of periods finalized.
    let actual_label = 'Actual',
        active_cpd = (tgd.uact_tgd_id === tgd.tgd_ui_id) ? tgd.tgd_cpd_id : 0;

    // Objective cells have a total and different headings.
    if (condition.tgd_c_type == 'obj') {
        $('#cell-' + id).append($('<table class="table table-condensed cell-table" id="table-' + id + '">')
            .append($('<thead>')
                .append($('<tr>')
                    .append($('<th class="text-center" colspan="4">')
                        .append($('<span class="fa fa-eye-slash tgd-curtain" data-condition="' + period + '"' +
                            ' style="color: #959595;" title="TGD Curtain">'))
                        .append(' FTG'))
                    .append($('<th class="text-center" colspan="4">')
                        .append('Amount'))
                    .append($('<th class="text-center" colspan="4">')
                        .append($('<span class="has-popover glyphicon glyphicon-info-sign" ' +
                            'style="float: right" data-toggle="popover" data-placement="' + placement + '"' +
                            ' data-html="true" title="Q' + quarter + ' ' + condition.tgd_c_name + '" ' +
                            'data-content="This block shows your Q' + quarter + ' ' + condition.tgd_c_name + '' +
                            '. It also shows:<br /><strong>F</strong> = Floor - Very least you will produce.<br />' +
                            '<strong>T</strong> = Target - What you intend to produce.<br /><strong>G</strong>' +
                            ' = Game - Take off the lid . what could happen.<br /><strong>Amount</strong> ' +
                            '= What you will produce for the quarter.<br /><strong>Cumulative</strong> =' +
                            ' Your income cumulative results.">'))
                        .append('Cumulative')
                    )))
            .append($('<tbody>')
                .append($('<tr>')
                    .append($('<td class="shaded" colspan="4">')
                        .append('F'))
                    .append($('<td id="floor-' + id + '" colspan="4">')
                        .append(numeral(0).format(format)))
                    .append($('<td id="floor-t-' + id + '" colspan="4">')
                        .append(numeral(0).format(format))))
                .append($('<tr>')
                    .append($('<td class="shaded" colspan="4">')
                        .append('T'))
                    .append($('<td id="target-' + id + '" colspan="4">')
                        .append(numeral(0).format(format)))
                    .append($('<td id="target-t-' + id + '" colspan="4">')
                        .append(numeral(0).format(format))))
                .append($('<tr>')
                    .append($('<td class="shaded" colspan="4">')
                        .append('G'))
                    .append($('<td id="game-' + id + '" colspan="4">')
                        .append(numeral(0).format(format)))
                    .append($('<td id="game-t-' + id + '" colspan="4">')
                        .append(numeral(0).format(format)))))
            .append($('<tfoot>')
                .append($('<tr>')
                    .append($('<td class="shaded" colspan="3" data-toggle="modal" data-id="' + active_cpd + '"' +
                        ' data-target="#toggle-history-dialog" data-label="Actual"' +
                        ' data-type="' + condition.tgd_c_col_name + '" data-quarter="' + period + '">')
                        .append(actual_label))
                    .append($('<td id="actual-' + id + '" colspan="3">')
                        .append(numeral(0).format(format)))
                    .append($('<td class="shaded" colspan="3">')
                        .append('Needed'))
                    .append($('<td id="needed-' + id + '" colspan="3">')
                        .append(numeral(0).format(format))))
                .append($('<tr>')
                    .append($('<td class="shaded" colspan="3">')
                        .append('Linked'))
                    .append($('<td id="linked-' + id + '" colspan="3" style="border-bottom: none">')
                        .append(numeral(0).format(format)))
                    .append($('<td class="shaded" colspan="3">')
                        .append('Cum'))
                    .append($('<td id="cum-' + id + '" colspan="3">')
                        .append(numeral(0).format(format))))
            )
        );
    }
    else {
        $('#cell-' + id).append($('<table class="table table-condensed cell-table" id="table-' + id + '">')
            .append($('<thead>')
                .append($('<tr>')
                    .append($('<th class="text-center" colspan="1">')
                        .append('FTG'))
                    .append($('<td class="shaded ftg-control" colspan="3">')
                        .append($('<span>')
                            .append('&nbsp;')))
                    .append($('<th class="text-center" colspan="1">')
                        .append('Count'))
                    .append($('<th class="text-center" colspan="2">')
                        .append($('<span class="has-popover glyphicon glyphicon-info-sign"' +
                            ' style="float: right" data-toggle="popover" data-placement="' + placement + '"' +
                            ' data-html="true" title="Q' + quarter + ' ' + condition.tgd_c_name + '"' +
                            ' data-content="This block shows your Q' + quarter + ' ' + condition.tgd_c_name +
                            '. It also shows:<br /><strong>F</strong> = Floor - Very least you will produce.<br />' +
                            '<strong>T</strong> = Target - What you intend to produce.<br /><strong>G</strong>' +
                            ' = Game - Take off the lid . what could happen.<br /><strong>Actual</strong> = ' +
                            'What you already have produced in this quarter, or are projected to produce given' +
                            ' your Actuals from previous quarters.<br /><strong>Needed</strong> = What you still' +
                            ' need to produce in this quarter to make your target.<br /><strong>Cumulative</strong>' +
                            ' = Your  cumulative count results.">'))
                        .append('Cumulative')
                    )))
            .append($('<tbody>')
                .append($('<tr>')
                    .append($('<td class="shaded" colspan="1">')
                        .append('F'))
                    .append($('<td class="shaded ftg-control" colspan="3">')
                        .append($('<span>')
                            .append('&nbsp;')))
                    .append($('<td colspan="1">')
                        .append($('<span id="floor-' + id + '">')))
                    .append($('<td colspan="2">')
                        .append($('<span id="cum-floor-' + id + '">'))
                    ))
                .append($('<tr>')
                    .append($('<td class="shaded" colspan="1">')
                        .append($('<span>')
                            .append('T')))
                    .append($('<td class="shaded ftg-control" colspan="3">')
                        .append($('<span class="ftg-control" style="display: none;" id="target-label-' + id + '">')
                            .append('')))
                    .append($('<td colspan="1">')
                    // .append($('<span id="target-s-' + id + '">'))
                        .append($('<span id="target-' + id + '">')))
                    // .append($('<span id="target-t-' + id + '">')))
                    .append($('<td colspan="2">')
                        .append($('<span id="cum-target-' + id + '">'))))
                .append($('<tr>')
                    .append($('<td class="shaded" colspan="1">')
                        .append('G'))
                    .append($('<td class="shaded ftg-control" colspan="3">')
                        .append($('<span>')
                            .append('&nbsp;')))
                    .append($('<td colspan="1">')
                        .append($('<span id="game-' + id + '">')))
                    .append($('<td colspan="2">')
                        .append($('<span id="cum-game-' + id + '">')))
                )
            )
            .append($('<tfoot>')
                .append($('<tr>')
                    .append($('<td class="shaded">')
                        .append(actual_label))
                    .append($('<td id="actual-' + id + '">')
                        .append(numeral(0).format(format)))
                    .append($('<td class="shaded ftg-control">')
                        .append('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'))
                    .append($('<td class="ftg-control">')
                        .append('&nbsp;&nbsp;&nbsp;&nbsp;'))
                    .append($('<td class="shaded">')
                        .append('Needed'))
                    .append($('<td id="needed-' + id + '">')
                        .append(numeral(0).format(format))))
                .append($('<tr>')
                    .append($('<td class="shaded">')
                        .append('Linked'))
                    .append($('<td id="linked-' + id + '" style="border-bottom: none">')
                        .append(numeral(0).format(format)))
                    .append($('<td class="shaded ftg-control">')
                        .append('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'))
                    .append($('<td class="ftg-control">')
                        .append('&nbsp;&nbsp;&nbsp;&nbsp;'))
                    .append($('<td class="shaded">')
                        .append('Cum'))
                    .append($('<td id="cum-' + id + '">')
                        .append(numeral(0).format(format)))
                )
            )
        );


        if (tgd.tgd_ui_periods_lock < period) {

            // Add arrow pointing left on all but the first period.
            if (1 !== period && tgd.tgd_ui_periods_lock != (period - 1)) {
                $('#target-label-' + id)
                    .append('&nbsp;')
                    .append($('<span class="glyphicon glyphicon-circle-arrow-left adjust-target"' +
                        ' data-ui="' + tgd.tgd_ui_id + '" data-period="' + period + '"' +
                        ' data-condition="' + condition_id + '" data-direction="left">'));
            }

            if (6 !== period) {
                $('#target-label-' + id)
                    .append('&nbsp;')
                    .append($('<span class="glyphicon glyphicon-circle-arrow-right adjust-target"' +
                        ' data-ui="' + tgd.tgd_ui_id + '" data-period="' + period + '" ' +
                        'data-condition="' + condition_id + '" data-direction="right">'));
            }
            if (2 < cond_num) {
                $('#target-label-' + id)
                    .append('&nbsp;')
                    .append($('<span class="glyphicon glyphicon-circle-arrow-up adjust-target" ' +
                        'data-ui="' + tgd.tgd_ui_id + '" data-period="' + period + '"' +
                        ' data-condition="' + condition_id + '" data-direction="up">'));
            }
            if (1 < cond_num && (cond_num <= (tgd.conditions.length - 1))) {
                $('#target-label-' + id)
                    .append('&nbsp;')
                    .append($('<span class="glyphicon glyphicon-circle-arrow-down adjust-target"' +
                        ' data-ui="' + tgd.tgd_ui_id + '" data-period="' + period + '"' +
                        ' data-condition="' + condition_id + '" data-direction="down">'));
            }
        }
    }

    // Add the editable class and attributes for editable cells.  The Actual and Needed are editable if the period is not
    // finalized.
    if (period == (parseInt(tgd.tgd_ui_periods_final) + 1) || editable) {
        $('#actual-' + id).addClass('editable')
            .attr({
                'data-url': jsglobals.base_url + 'tgd/update_cell',
                'data-name': 'tgd_ic_actual',
                'data-pk': ui_id + '-' + condition.tgd_c_id + '-' + period,
                'data-type': 'number',
                'title': 'Set Actual',
            });


        $('#linked-' + id).addClass('editable')
            .attr({
                'data-url': jsglobals.base_url + 'tgd/update_cell',
                'data-name': 'tgd_ic_linked',
                'data-pk': ui_id + '-' + condition.tgd_c_id + '-' + period,
                'data-type': 'number',
                'title': 'Set Linked',
            });
    }

    // If the cell is editable, make all of the values editable.
    if ((editable && (period > parseInt(tgd.tgd_ui_periods_final))) ||
        (period == (parseInt(tgd.tgd_ui_periods_final) + 1))) {
        $('#floor-' + id).addClass('editable')
            .attr({
                'data-url': jsglobals.base_url + 'tgd/update_cell',
                'data-name': 'tgd_ic_floor',
                'data-pk': ui_id + '-' + condition.tgd_c_id + '-' + period,
                'data-type': 'number',
                'title': 'Set Floor',
            });
        $('#target-' + id).addClass('editable')
            .attr({
                'data-url': jsglobals.base_url + 'tgd/update_cell',
                'data-name': 'tgd_ic_target_p',
                'data-pk': ui_id + '-' + condition.tgd_c_id + '-' + period,
                'data-type': 'number',
                'title': 'Set Target',
            });
        $('#game-' + id).addClass('editable')
            .attr({
                'data-url': jsglobals.base_url + 'tgd/update_cell',
                'data-name': 'tgd_ic_game',
                'data-pk': ui_id + '-' + condition.tgd_c_id + '-' + period,
                'data-type': 'number',
                'title': 'Set Game',
            });

        // The objective cumulative is editable for the first period.
        if (condition.tgd_c_type == 'obj' && period == 1) {
            $('#floor-t-' + id).addClass('editable')
                .attr({
                    'data-url': jsglobals.base_url + 'tgd/update_instance',
                    'data-name': 'tgd_ui_py_cum_floor',
                    'data-pk': ui_id,
                    'data-type': 'number',
                    'title': 'Set Last Year\'s Total Income',
                });
            $('#target-t-' + id).addClass('editable')
                .attr({
                    'data-url': jsglobals.base_url + 'tgd/update_instance',
                    'data-name': 'tgd_ui_py_cum_target',
                    'data-pk': ui_id,
                    'data-type': 'number',
                    'title': 'Set Last Year\'s Total Income',
                });
            $('#game-t-' + id).addClass('editable')
                .attr({
                    'data-url': jsglobals.base_url + 'tgd/update_instance',
                    'data-name': 'tgd_ui_py_cum_game',
                    'data-pk': ui_id,
                    'data-type': 'number',
                    'title': 'Set Last Year\'s Total Income',
                });
        }
    }

    // Do we have any values for the cell?
    if (typeof cells[period] != 'undefined' && typeof cells[period][condition_id] !== 'undefined') {
        const cell = cells[period][condition_id];
        const floor_val = parseInt(cell.tgd_ic_floor) + parseInt(cell.tgd_ic_target_t);
        const game_val = parseInt(cell.tgd_ic_game) + parseInt(cell.tgd_ic_target_t);


        if (condition.tgd_c_type == 'obj') {
            $('#floor-' + id).html(numeral((floor_val + oi_total[period]) > 0 ?
                (floor_val + oi_total[period]) : 0).format(format));
            $('#game-' + id).html(numeral((game_val + oi_total[period]) > 0 ?
                (game_val + oi_total[period]) : 0).format(format));
            $('#target-' + id).html(
                numeral(parseInt(cell.tgd_ic_target_p) + parseInt(cell.tgd_ic_target_t) + oi_total[period]).format(format));
        } else {
            $('#floor-' + id).html(numeral(floor_val > 0 ? floor_val : 0).format(format));
            $('#game-' + id).html(numeral(game_val > 0 ? game_val : 0).format(format));
            $('#target-' + id).html(numeral(parseInt(cell.tgd_ic_target_p) + parseInt(cell.tgd_ic_target_t)).format(format));
        }

        const actual_id = $('#actual-' + id);

        // Check if a DMD is linked to TGD or not.
        if (period == 1 || period == 6) {
            actual_id.html(numeral(cell.tgd_ic_actual).format(format));
        } else if ((actual != undefined) && (null !== actual[condition_id][period][0].net)) {
            actual_id.html(numeral(actual[condition_id][period][0].net).format(format));
        } else {
            actual_id.html(numeral(cell.tgd_ic_actual).format(format));
        }

        if (period == 1 || period == 6) {
            $('#linked-' + id).html(numeral(cell.tgd_ic_linked).format(format));
        } else if (linked != undefined) {
            $('#linked-' + id).html(numeral((undefined !== linked) ? linked[condition_id][period] : 0).format(format));
        } else {
            $('#linked-' + id).html(numeral(cell.tgd_ic_linked).format(format));
        }

        // Add other income to Actual for cumulative income.
        if (condition.tgd_c_name == 'Cumulative Income') {
            actual_id.html(numeral(numeral(actual_id.html()).value() + parseInt(oi_total[period])).format(format));

            // Update the TGD value for next year TGD
            // if ('$0' !== actual_id.html() && 5 === period) {
            //     makeAjaxCall('tgd/update_cell', {
            //         'pk' : ui_id + '-' + condition.tgd_c_id + '-' + period,
            //         'name': 'tgd_ic_actual',
            //         'value': numeral(actual_id.html()).value()
            //     });
            // }
        }

        if ((5 === period) && (0 !== numeral(actual_id.html()).value())) {
            makeAjaxCall('tgd/update_cell', {
                'pk': ui_id + '-' + condition.tgd_c_id + '-' + period,
                'name': 'tgd_ic_actual',
                'value': numeral(actual_id.html()).value()
            });
        }


        var diff = numeral($('#target-' + id).html()).value() - numeral($('#actual-' + id).html()).value();

        $('#needed-' + id).html(numeral(diff).format(format));

        let prev_cum = 0;
        // if (period > 2 && period < 6) {
        //     prev_cum = numeral($('#cum-' + condition.tgd_c_id + '-' + (period - 1)).html()).value();
        // }

        var cum = numeral($('#actual-' + id).html()).value() + numeral($('#linked-' + id).html()).value() + prev_cum;
        $('#cum-' + id).html(numeral(cum).format(format));

        // If the condition type is Objective, add the value to the previous total.
        if (condition.tgd_c_type == 'obj') {

            if (period == 2 || period == 3 || period == 4 || period == 5) {
                total_cum = total_cum + cum;
            }

            // If this is the 1st, 2nd or 6th period, the total is just the value.
            var floor_total = numeral($('#floor-' + condition.tgd_c_id + '-' + period).html());
            var target_total = numeral($('#target-' + condition.tgd_c_id + '-' + period).html())
            var game_total = numeral($('#game-' + condition.tgd_c_id + '-' + period).html())

            // If this is the 1st period, the cumulative comes from the User Instance.
            if (period == 1) {
                floor_total = numeral(tgd.tgd_ui_py_cum_floor);
                target_total = numeral(tgd.tgd_ui_py_cum_target);
                game_total = numeral(tgd.tgd_ui_py_cum_game);
            }
            else if (period == 3 || period == 4 || period == 5) {
                floor_total = floor_total +
                    numeral($('#floor-t-' + condition.tgd_c_id + '-' + (period - 1)).html());
                target_total = target_total +
                    numeral($('#target-t-' + condition.tgd_c_id + '-' + (period - 1)).html());
                game_total = game_total +
                    numeral($('#game-t-' + condition.tgd_c_id + '-' + (period - 1)).html());
            }

            $('#floor-t-' + id).html(numeral(floor_total).format(format));
            $('#target-t-' + id).html(numeral(target_total).format(format));
            $('#game-t-' + id).html(numeral(game_total).format(format));
        } else {

            var floor_total = numeral($('#floor-' + condition.tgd_c_id + '-' + (period)).text());
            var target_total = numeral($('#target-' + condition.tgd_c_id + '-' + (period)).text());
            var game_total = numeral($('#game-' + condition.tgd_c_id + '-' + (period)).text());

            // If this is the 1st period, the cumulative comes from the User Instance.
            if (period == 1) {
                floor_total = tgd.tgd_ui_py_cum_floor != 0 ? tgd.tgd_ui_py_cum_floor : floor_total;
                target_total = tgd.tgd_ui_py_cum_target != 0 ? tgd.tgd_ui_py_cum_target : target_total;
                game_total = tgd.tgd_ui_py_cum_game != 0 ? tgd.tgd_ui_py_cum_game : game_total;
            }
            else if (period == 3 || period == 4 || period == 5) {
                floor_total = numeral($('#floor-' + condition.tgd_c_id + '-' + (period)).text()) +
                    numeral($('#cum-floor-' + condition.tgd_c_id + '-' + (period - 1)).html());
                target_total = numeral($('#target-' + condition.tgd_c_id + '-' + (period)).text())
                    + numeral($('#cum-target-' + condition.tgd_c_id + '-' + (period - 1)).html());
                game_total = numeral($('#game-' + condition.tgd_c_id + '-' + (period)).text()) +
                    numeral($('#cum-game-' + condition.tgd_c_id + '-' + (period - 1)).html());
            }

            $('#cum-floor-' + id).html(numeral(floor_total).format(format));
            $('#cum-target-' + id).html(numeral(target_total).format(format));
            $('#cum-game-' + id).html(numeral(game_total).format(format));
        }
    }
}
