$(document).ready(() => {

    $('#toggle-history-dialog').on('show.bs.modal', function (event) {
        $('.recalculate-class').remove();

        $('#toggle-history-table tbody').empty();
        $('#toggle-history-table thead').empty();
        view_dialog.init(event);
    });
});

let view_dialog = (() => {

    let id, year,

        init = function (event) {
            id = $(event.relatedTarget).data('id');
            year = $(event.relatedTarget).data('year');

            // Check if this is call to actual or toggle pop up.
            const actual_label = $(event.relatedTarget).data('label');

            // Actual List Pop up
            if (undefined !== actual_label) {

                // Set the header and intro paragraph for pop up.
                $('#toggle-history-title').html('Actual List');
                $('#toggle-history-para').html('List of all the clients, from which Actual is made up of.');

                $('#toggle-history-table thead')
                    .append($('<tr>')
                        .append($('<th class="text-center">')
                            .append('#'))
                        .append($('<th class="text-center">')
                            .append('Client'))
                        .append($('<th class="text-center">')
                            .append('Dollars'))
                        .append($('<th class="text-center">')
                            .append('Property'))
                        .append($('<th class="text-center">')
                            .append('RWT'))
                        .append($('<th class="text-center">')
                            .append('CLU'))
                    );


                // Add the Recalculate button to the button list if it is not already there.
                // if (!$('#recalculate').length) {
                //     $('#tool-buttons').append($('<button type="button" class="btn btn-primary" id="recalculate" data-toggle="modal" ' +
                //         'data-target="#recalculate-dialog" data-id="' + tgd.tgd_ui_id + '"' +
                //         ' data-locked="' + tgd.tgd_ui_periods_lock +'"><span class="has-tooltip" ' +
                //         'data-toggle="tooltip" title="Clear all Push, Pull, Lift, and Drop operations." data-placement="top">' +
                //         'Clear Toggles</span></button>'));
                // }

                displayActualList(event);
            } else {

                // Set the header and intro paragraph for pop up.
                $('#toggle-history-title').html('Toggle History');
                $('#toggle-history-para').html('List of all the Floor, Target and Game toggles done by you.');

                $('#toggle-history-table thead')
                    .append($('<tr>')
                        .append($('<th class="text-center">')
                            .append('#'))
                        .append($('<th class="text-center">')
                            .append('Direction'))
                        .append($('<th class="text-center">')
                            .append('Metric'))
                        .append($('<th class="text-center">')
                            .append('Quarter'))
                        .append($('<th class="text-center">')
                            .append('Year'))
                        .append($('<th class="text-center">')
                            .append('When'))
                    );

                let locked_quarter = $(event.relatedTarget).data('locked');
                $(event.relatedTarget).removeData('locked');

                $('#toggle-history-dialog .modal-footer')
                    .append($('<button type="button" class="btn btn-primary recalculate-class" ' +
                        'id="recalculate" data-toggle="modal" data-locked="' + locked_quarter + '" ' +
                        'data-target="#recalculate-dialog" data-id="' + id + '"><span class="has-tooltip" ' +
                        'data-toggle="tooltip" title="Clear all Push, Pull, Lift, and Drop operations." ' +
                        'data-placement="top">' +
                        'Clear Toggles</span></button>'));

                displayToggleHistory();
            }
        },

        // Displays the toggle History
        displayToggleHistory = () => {

            // Make ajax call to fetch toggle history.
            toggle_history = makeAjaxCall('tgd/get_toggle_history', {'tgd_id': id});

            // Update List.
            toggle_history.then(data => {
                $.each(data.history, function (index, history) {

                    let instance_year = year;
                    const metric = 50 - history.tph_prev_cond_id;
                    let qtr = history.tph_prev_period - 1;

                    // Check if it's Prev year Q4 or Next Year Q1
                    if (0 === qtr) {
                        qtr = 4;
                        instance_year = instance_year - 1;
                    } else if (5 === qtr) {
                        qtr = 1;
                        instance_year = instance_year + 1;
                    }

                    let direction;

                    // Determine the direction for toggle.
                    if (history.tph_prev_cond_id === history.tph_current_cond_id) {

                        if (history.tph_current_period == (parseInt(history.tph_prev_period) + 1)) {
                            direction = 'right';
                        } else {
                            direction = 'left';
                        }
                    } else {

                        if (history.tph_current_cond_id == (parseInt(history.tph_prev_cond_id) + 1)) {
                            direction = 'up';
                        } else {
                            direction = 'down';
                        }
                    }

                    // Populate History Table.
                    $('#toggle-history-table tbody')
                        .append($('<tr>')
                            .append($('<td class="text-center">')
                                .append(index + 1))
                            .append($('<td class="text-center">')
                                .append($('<span class="glyphicon glyphicon-circle-arrow-' + direction + '"></span>')))
                            .append($('<td class="text-center">')
                                .append(metric))
                            .append($('<td class="text-center">')
                                .append('Q' + qtr))
                            .append($('<td class="text-center">')
                                .append(instance_year))
                            .append($('<td class="text-center">')
                                .append(history.tph_time))
                        );
                });
            });
        },

        displayActualList = (event) => {

            // Get the Parameters.
            const cpd_type = $(event.relatedTarget).data('type'),
                cpd_id = $(event.relatedTarget).data('id'),
                quarter = $(event.relatedTarget).data('quarter') - 1,
                active_tgd = $(event.relatedTarget).data('active');

            if (0 !== cpd_id) {

                // Call to fetch DMD details.
                const cpd_details = makeAjaxCall('tgd/get_cpd_details',
                    {
                        'type': cpd_type,
                        'id': cpd_id,
                        'quarter': quarter
                    });

                cpd_details.then(data => {
                    $.each(data.cpd_details, function (index, cpd_deals) {

                        // Populate Actual Table.
                        $('#toggle-history-table tbody')
                            .append($('<tr>')
                                .append($('<td class="text-center">')
                                    .append(index + 1))
                                .append($('<td class="text-center">')
                                    .append(cpd_deals.deal_name))
                                .append($('<td class="text-center">')
                                    .append(numeral(cpd_deals.deal_net).format('$0,0')))
                                .append($('<td class="text-center">')
                                    .append(cpd_deals.deal_property))
                                .append($('<td class="text-center">')
                                    .append((null === cpd_deals.deal_rwt) ?
                                        'None' || '' : cpd_deals.deal_rwt))
                                .append($('<td class="text-center">')
                                    .append((null === cpd_deals.deal_clu) ?
                                        'None' || '' : cpd_deals.deal_clu))
                            );
                    });
                });
            } else {
                $('#toggle-history-para').append('<br> You don\'t have any active DMD instance for this product' +
                    ' type, Please create one and mark it as active.')
            }

        };

    return {
        init: init
    };

})();