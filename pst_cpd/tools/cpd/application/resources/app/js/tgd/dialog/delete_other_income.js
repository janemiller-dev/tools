$(document).ready(function () {
    $('#delete-other-incomes-dialog').on('show.bs.modal', function (event) {

        // Get inputs
        var oi_id = $(event.relatedTarget).data('id');
        var name = $(event.relatedTarget).data('name');
        $('#update-other-income-dialog').modal('hide');

        $('#other-income-name').html(name);

        $('#delete-other-income-button').off('click');
        $('#delete-other-income-button').click(function () {
            $.ajax({
                url: jsglobals.base_url + 'tgd/delete_other_income',
                dataType: 'json',
                data: {
                    oi_id: oi_id
                },
                type: 'post',
                error: ajaxError
            }).done(function (data) {

                if (data.status != 'success') {
                    toastr.error(data.message);
                    return;
                } else if (data.status == 'redirect') {
                    window.location.href = data.redirect;
                    return;
                }

                $('#delete-other-incomes-dialog').modal('hide');
                refreshPage();
            }).fail(function (jqXHR, textStatus) {
                toastr.error("Request failed: " + textStatus);
            }).always(function () {
            });
        })
    });
});