$(document).ready(function() {

    // Get the game types and start dates.
    $.ajax({
	url: jsglobals.base_url + 'tgd/get_types',
	dataType: 'json',
	type: 'post',
    }).done(function(data) {
	if (data.status != 'success') {
	    toastr.error(data.message);
	    return;
	}
        else if (data.status == 'redirect') {
	    window.location.href = data.redirect;
	    return;
        }
        groupUpdateGameTypes(data.game_types);
    }).fail(function(jqXHR, status) {
	toastr.error("Server communication error. Please try again.");
    }).always(function() {
    });

    // When the dialog is displayed, clear the values
    $('#add-group-dialog').on('show.bs.modal', function(event) {
	$('#tgd-g-gt-id').val(0);
	$('#tgd-g-name').val('');
	$('#tgd-g-mission').html('');
    });

    // Validate the add group form and submit it if it is valid.
    $('#add-group-form').formValidation({
	framework: 'bootstrap',
	icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
	},
	fields: {
	    tgd_gt_id: {
		validators: {
		    greaterThan: {
			value: 0,
			inclusive: false,
			message: "The industry is required."
		    }
		}
	    },
	    name: {
		validators: {
		    notEmpty: {
			message: "The group name is required."
		    }
		}
	    },
	    mission: {
		validators: {
		    notEmpty: {
			message: "The group description is required."
		    }
		}
	    }
	}
    }).on('success.form.fv', function(e) {
	e.preventDefault();

	// Get the form group
	var $form = $(e.target);
	var fv = $form.data('formValidation');

	// Get the form data and submit it.
	var message_data = $form.serialize();
	$.ajax({
	    url: jsglobals.base_url + 'tgd/add_group',
	    dataType: 'json',
	    type: 'post',
	    data: message_data
	}).done(function(data) {
	    if (data.status != 'success') {
		toastr.error(data.message);
		return;
	    }
            else if (data.status == 'redirect') {
		window.location.href = data.redirect;
		return;
            }
	    $('#add-group-dialog').modal('hide');
	    fv.resetForm();

	    refreshPage();

	    // Open the new group in a new tab.
	    var address = jsglobals.base_url + "tgd/group/" + data.group_id;
	    var newwin = window.open(address, '_blank');
	    if (newwin) {
		newwin.focus();
	    }
	    else {
		toastr.error('Please allow popups for this website');
	    }
	    
	}).fail(function(jqXHR, status) {
	    toastr.error("Server communication error. Please try again.");
        }).always(function() {
        });
    });

    // Initialize the year dropdown.
    groupUpdateYears();
});

// Set the possible start dates based on the game type period.
function groupUpdateYears() {

    // Clear the old options.
    $('#tgd-g-year').empty();

    // Allow the current year and the next year.
    var date = moment();

    // Only 2 years allowed.
    for (var i = 0; i < 4; i++) {
	var year = date.year();

	$('#tgd-g-year').append($('<option>').attr({value: year,}).append(year));
	date.add(1, 'year');
    }

    $('#tgd-g-year').val('2018');
}

// Update the game type select options.
function groupUpdateGameTypes(game_types) {

    // Clear the select list.
    $('#tgd-g-gt-id').empty();

    // Show each game type and update the game type periods table.
    $('#tgd-g-gt-id').append($('<option>')
			   .attr({value: 0})
			   .append("Select Industry"));
    $.each(game_types, function (gt, game_type) {
	$('#tgd-g-gt-id').append($('<option>')
				 .attr({value: game_type.tgd_gt_id})
				 .append(game_type.tgd_gt_name));
    });

}
