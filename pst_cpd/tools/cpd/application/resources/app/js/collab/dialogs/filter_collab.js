/**
 *
 * Filter Collaboration Data for a user.
 *
 * @summary       Filter Collaboration data for user.
 * @description  This file contains functions for filtering Collaboration Information based on .
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */
$(document).ready(function () {
    $('#filer-collab-dialog').on('show.bs.modal', function (event) {
        filter_collab.init();
    });
});

/*
*  Client information to be displayed in the info pop up.
*/
let filter_collab = function () {

    let init = () => {

        // Fetch Collaboration Details.
        const get_collab_details = makeAjaxCall('collab/get_collab_details', {});

        get_collab_details.then(function (data) {

            // Append Collaboration format.
            const filter_action_selector = $('#filter-collab-action');
            filter_action_selector.empty();
            filter_action_selector.append('<option value="0">Everything</option>');

            let optgroup = $("<optgroup label='Comment(s)'>");
            // Iterate over each of the formats available
            $.each(data.collab_details.format, function (index, format) {

                if (format.collab_format_id === '2' || format.collab_format_id === '3') {
                    filter_action_selector.append('<option value="' + format.collab_format_id + '">'
                        + format.collab_format_name + '</option>')
                } else {
                    optgroup
                        .append('<option value="' + format.collab_format_id + '">'
                            + format.collab_format_name + '</option>')
                }
            });

            filter_action_selector.append(optgroup);

            // Append Collaboration Script options.
            const filter_recipient_selector = $('#filter-recipient-name');
            filter_recipient_selector.empty();
            filter_recipient_selector.append('<option value="0">Everyone</option>');
            $.each(data.collab_details.recipient, function (index, recipient) {
                filter_recipient_selector.append('<option value="' + recipient.collab_recipient_id + '">' +
                    recipient.collab_recipient_name + '</option>');
            })
        });

        // Filter the collaboration data.
        const filter_collab_selector = $('#filter_collab');
        filter_collab_selector.off('click');
        filter_collab_selector.click(function () {
            $('.collab_row, .collaborator_row').remove();
            setupCollab();
        });

        // Play Roleplay click handler
        $(document).off('click', '#play_roleplay');
        $(document).on('click', '#play_roleplay', function () {
            if (!speechSynthesis.speaking) {
                let array = [];

                // Loop through each collaboration row.
                $.each($('.user_row, .collaborator_row').find('.tts'), function (index, collab) {

                    let input = $(collab).siblings('textarea').val(),
                        separator = '';

                    // Check if this is a collaboration row.
                    if ($(collab).siblings('textarea').hasClass('collborator_data')) {
                        separator = '[[c]]';
                    }

                    // Check if the textarea content length exceeds 230 words.
                    while (input.length > 230) {
                        let last_index = input.indexOf(' ', 220);
                        const ip = input.substr(0, last_index);
                        input = input.substr(last_index + 1);
                        array.push(separator + ip);
                    }
                    array.push(separator + input);
                });
                readOutLoud(array);
            } else {
                speechSynthesis.cancel();
            }
        })
    };

    return {
        init: init,
    };
}();