/**
 *
 * Displays and updates Collaboration Details for a user.
 *
 * @summary       Display and update Collaboration Details.
 * @description  This file contains functions for Displaying and updating Collaboration Information.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */
$(document).ready(function () {
    $('#add-lead-dialog').on('show.bs.modal', function (event) {

    });
        collab.init();
});

/*
*  Client information to be displayed in the info pop up.
*/
let collab = function () {

    let init = () => {

        // Fetch Collaboration Details.
        const get_collab_details = makeAjaxCall('collab/get_collab_details', {});

        get_collab_details.then(function (data) {

            // Append Collaboration format.
            $.each(data.collab_details.format, function (index, format) {
                $('#collab-format').append('<option value="' + format.collab_format_id + '">' +
                    format.collab_format_name + '</option>')
            });

            // Append Collaboration Script options.
            $.each(data.collab_details.script, function(index, script) {
                $('#collab-script').append('<option value="' + script.collab_script_id + '">' +
                    script.collab_script_name + '</option>');
            });

            // Append Collaboration Script options.
            $.each(data.collab_details.recipient, function(index, recipient) {
                $('#collab-recipient').append('<option value="' + recipient.collab_recipient_id + '">' +
                    recipient.collab_recipient_name + '</option>');
            })
        });

        // Click handler for Drop Down.
        $(document).on('click', '.collab-menu',function () {

            // Check if the user has selected to add a new instance of Element.
            if ($(this).val() === '0') {
                $('#add-collab-dialog').modal('show');
                $(this).val(-1);
            }
        });
    };

    return {
        init: init,
    };
}();