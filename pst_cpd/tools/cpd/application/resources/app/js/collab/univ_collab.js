/**
 *
 * Displays and updates Collaboration Details for a user.
 *
 * @summary       Display and update Collaboration Details.
 * @description  This file contains functions for Displaying and updating Collaboration Information.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */
$(document).ready(function () {
    univ_collab.init();
});

/*
*  Client information to be displayed in the info pop up.
*/
let univ_collab = function () {
    // Fetch the Parameters from URL.
    const searchParams = new URLSearchParams(window.location.search),
        user_id = searchParams.get('id'),
        recipient = searchParams.get('r');
    let component_type, ans_method, default_ans_method;

    let init = () => {
        $('.tool-menu').css('display', 'none');

        // Get the collaboration details.
        const get_collab_info = makeAjaxCall('collab/get_collab_info', {
            id: user_id,
            recipient: recipient
        });

        get_collab_info.then(function (data) {

            $.each(data.collab_info, function (index, info) {
                let dummy_row = $('#dummy-row').clone();
                dummy_row.removeClass('hidden');
                dummy_row.removeAttr('id');
                dummy_row.find('#title').text(info.collab_sender_name + ' '
                    + info.collab_created_at + ': ' + info.collab_format_name);
                dummy_row.find('#collab_ques').text(info.collab_data);
                dummy_row.find('.collab-reply').attr('data-id', info.collab_id);
                dummy_row.find('.collab-reply').attr('data-email', info.collab_user_email);
                if (info.collab_collab_id === '0') {
                    $('#collab-row').append(dummy_row);
                } else {
                    dummy_row.find('textarea').addClass('collab-reply-text');
                    dummy_row.find('textarea').attr('data-id', info.collab_id);
                    dummy_row.css('background', '#fffbed');
                    // dummy_row.find('.collab-reply');

                    $('span[data-id="' + info.collab_collab_id + '"]').closest('.collab-box')
                        .after(dummy_row);
                }
            });

            // Update Collaboration Data.
            $(document).off('change', '.collab-reply-text');
            $(document).on('change', '.collab-reply-text', function (data) {
                const update_collab_data = makeAjaxCall('collab/save_collab_text', {
                    id: $(this).attr('data-id'),
                    val: $(this).val()
                });

                update_collab_data.then(function (data) {
                    console.log(data);
                })
            })
        });

        // Collaboration reply click handler.
        $(document).off('click', '.collab-reply');
        $(document).on('click', '.collab-reply', function () {
            const id = $(this).attr('data-id'),
                email = $(this).attr('data-email'),
                get_collab_details = makeAjaxCall('collab/get_univ_collab_details', {
                    id: id
                });

            get_collab_details.then(function (data) {

                const sender = data.univ_collab[0].collab_recipient_name;
                let date_time = '',
                    format = '';
                $('#add-collab-dialog').modal('show');
                $('#collaborator_div').hide();
                $('#sender_div').removeClass('col-xs-4').addClass('col-xs-6');
                $('#action_div').removeClass('col-xs-4').addClass('col-xs-6');

                $('#sender_name').attr('disabled', true).val(sender);
                $.each(data.collab_data.format, function (index, format) {
                    $('#collab-action').append(
                        '<option value="' + format.collab_format_id + '">' + format.collab_format_name + '</option>');
                });

                // Save Collaboration Details click handler.
                $(document).off('click', '#save_collab_details');
                $(document).on('click', '#save_collab_details', function () {
                    date_time = moment().format(" DD-MM-YY hh:mm A");
                    format = $('#collab-action option:selected').text();
                    const add_collab_reply = makeAjaxCall('collab/add_collab_reply', {
                        id: id,
                        format: $('#collab-action').val(),
                        sender: sender,
                        date_time: date_time,
                        email: email
                    });

                    add_collab_reply.then(function (data) {
                        const reply_id = data.reply_added;

                        $('#add-collab-dialog').modal('hide');
                        let dummy_row = $('#dummy-row').clone();
                        dummy_row.removeClass('hidden');
                        dummy_row.removeAttr('id');
                        dummy_row.find('textarea').addClass('collab-reply-text').attr('data-id', reply_id);
                        dummy_row.find('#title').text(sender + ' '
                            + date_time + ': ' + format);
                        dummy_row.css('background', '#fffbed');
                        $('span[data-id="' + id + '"]').closest('.collab-box')
                            .after(dummy_row);
                    });
                });
            });
        })
    };

    return {
        init: init,
    };
}();