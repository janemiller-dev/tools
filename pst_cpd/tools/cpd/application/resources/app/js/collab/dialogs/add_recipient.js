$(document).ready(function () {
    $('#save_recipient').off('click');
    $('#save_recipient').click(function () {
        const save_recipient = makeAjaxCall('collab/save_recipient', {
            'name': $('#recipient-name').val()
        });

        save_recipient.then(function (data) {
            $('#add-recipient-dialog').modal('hide');
            toastr.success('Recipient Added!!');
        })
    })
});