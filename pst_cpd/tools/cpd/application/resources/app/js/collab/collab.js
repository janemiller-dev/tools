/**
 *
 * Displays and updates Collaboration Details for a user.
 *
 * @summary       Display and update Collaboration Details.
 * @description  This file contains functions for Displaying and updating Collaboration Information.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */
$(document).ready(function () {
    collab.init();
});

/*
*  Client information to be displayed in the info pop up.
*/
let collab = function () {
    // Fetch the Parameters from URL.
    const searchParams = new URLSearchParams(window.location.search),
        collab_id = searchParams.get('collab_id'),
        tool_id = searchParams.get('id'),
        outline_id = searchParams.get('outline'),
        instance_id = searchParams.get('i'),
        recipient = searchParams.get('r');
    let component_type, ans_method, default_ans_method, user_name;

    let init = () => {
            $('.tool-menu').css('display', 'none');


            const get_component_info = makeAjaxCall('collab/get_component_info', {
                'collab_id': collab_id
            });

            get_component_info.then(function (data) {

                component_type = data.component_info[0].sc_type;
                if (data.component_info[0].sc_type === 'script') {
                    const get_script_outline = makeAjaxCall('collab/get_script_outline', {
                        outline_id: outline_id
                    });

                    get_script_outline.then(function (data) {
                        update_content(data);
                    });

                    ans_method = 'collab/get_script_ans';
                    default_ans_method = 'get_default_answers';
                } else if (data.component_info[0].sc_type === 'vmail') {
                    const get_message_outline = makeAjaxCall('collab/get_message_outline', {
                        outline_id: outline_id
                    });

                    get_message_outline.then(function (data) {
                        update_content(data);
                    });

                    ans_method = 'tsl/get_message_content';
                    default_ans_method = 'collab/get_default_seq';
                } else {
                    const get_page_outline = makeAjaxCall('collab/get_page_outline', {
                        outline_id: outline_id
                    });

                    get_page_outline.then(function (data) {
                        update_content(data);
                    });

                    ans_method = 'tsl/get_promo_data';
                    default_ans_method = 'tsl/get_default_promo_answers';
                }
            });

            //Remove click functionality from add and send collab button.
            $(document).off('click', '.add_collab, .send_collab, #save_collab_details');

            $(document).off('click', '.add_collab');
            // Add Collaboration click handler.
            $(document).on('click', '.add_collab', function () {

                let collaboration_id = $(this).attr('data-collab');
                $('#add-collab-dialog').modal('show');

                $('#collaborator_div').hide();
                $('#sender_div').removeClass('col-xs-4').addClass('col-xs-6');
                $('#action_div').removeClass('col-xs-4').addClass('col-xs-6');

                const ques_id = $(this).parent().find('textarea').attr('data-id'),
                    that = $(this),
                    parent_margin = $(this).parent().css('margin-left'),
                    order = $(this).parent().siblings('.collab_response_row').length;

                $('#save_collab_details').off('click');
                $('#save_collab_details').click(function () {
                    const sender_name = $('#sender_name').val(),
                        action = $('#collab-action :selected').text(),
                        today = new Date(),
                        date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate(),
                        time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

                    // Create a collab instance in database.
                    const save_collab_data = makeAjaxCall('collab/save_collab_data', {
                        type: component_type,
                        instance_id: instance_id,
                        collaboration_id: collaboration_id,
                        ques_id: ques_id,
                        order: order,
                        sender: sender_name,
                        recipient: $('#recipient_name').val(),
                        action: $('#collab-action :selected').val(),
                        date: date,
                        time: time,
                        category: 'scr'
                    });

                    // Add Row after collab data is saved.
                    save_collab_data.then(function (data) {
                        toastr.success('Collaboration Instance Added!!');
                        $('#add-collab-dialog').modal('hide');

                        // Format date and time
                        const formatted_date = moment(date).format('DD-MM-YY'),
                            formatted_time = moment(time, "HH:mm:ss").format('hh:mm A');

                        // Append text box after DB record is added.
                        $(that).parent().after(
                            '<div style="margin-left: calc(' + parent_margin + ')" class="collaborator_row">' +
                            '<label>' + sender_name + '. ' +
                            formatted_date + ' ' + formatted_time + ' : ' + action + '</label>' +
                            '<div class="input-group">' +
                            '<span class="input-group-addon btn btn-secondary send-response" data-toggle="modal"' +
                            ' data-id="' + data.collab_data_saved + '" data-target="#send-collab-response-dialog">' +
                            '<i class="fa fa-paper-plane"></i></span>' +
                            '<span class="input-group-addon btn btn-secondary tts">' +
                            '<i class="fa fa-bullhorn"></i></span>' +
                            '<textarea onkeyup="auto_grow(this)" class="form-control custom-control collaborator_response" ' +
                            'rows="2" cols="70" ' +
                            'style="height: 52px; font-size: 100%; background-color: rgb(255, 251, 237)" ' +
                            'data-id="' + ques_id + '" data-collab="' + data.collab_data_saved + '"></textarea>' +
                            '<span class="input-group-addon btn btn-secondary stt">' +
                            '<i class="fa fa-microphone"></i></span></div></div>')
                    });
                })
            });
        },

        update_content = (data) => {

            const outline_content = data.script_outline[0].tsl_script_content;

            let z = outline_content.replace(/\[\[/g, '<h4>').replace(/\]\]/g, '</h4>')
                    .replace(/\(\(/g, '<label style="margin:15px 0 0 30px">').replace(/\)\)/g, '</label>\n' +
                    '<div class="input-group" style="margin-left: 30px;">' +
                    '<span class="input-group-addon btn btn-secondary tts">' +
                    '<i class="fa fa-bullhorn"></i></span><textarea onkeyup="auto_grow(this)"' +
                    ' class="form-control custom-control" placeholder="Click Microphone icon and speak or start typing."' +
                    ' rows="2" cols="10"></textarea><span class="input-group-addon btn btn-secondary stt">' +
                    '<i class="fa fa-microphone"></i>' +
                    '</span></div><span id=\'span_ans\' class=\'hidden\'></span>')
                    .replace(/{{/g, '<label style="margin-top: 15px">').replace(/}}/g, '</label>\n' +
                    '<div class="input-group"><span class="input-group-addon btn btn-secondary tts">' +
                    '<i class="fa fa-bullhorn"></i></span><textarea onkeyup="auto_grow(this)" ' +
                    'class="form-control custom-control" placeholder="Click Microphone icon and speak or start typing."' +
                    ' rows="2" cols="70"></textarea><span class="input-group-addon btn btn-secondary stt">' +
                    '<i class="fa fa-microphone"></i>' +
                    '</span></div><span id=\'span_ans\' class=\'hidden\'></span>').replace(/<br>/g, ''),

                x = z.trim().match(/(<label(.*?)<\/label>)|(<h4>(.*?)<\/h4>)|(<([^>]+)>)/g),
                t = '';

            $.each(x, function (index, text) {
                t = t + text;
            });

            const collab_container_selector = $('#collab-container');
            collab_container_selector.html('');
            collab_container_selector.html('<h4 id="script_content" class="main-con-header">COLLABORATION' +
                '</h4><form id="script-form" class="form-horizontal">' +
                '<input type="hidden" name="script_name_id" id="script_names_id">' + t + '</form>');

            // Add Id, Name and data attribute to Textarea.
            $.each($('#script-form textarea'), function (index, t) {
                count = (index + 1);
                $(t).attr('id', 'ans' + count);
                $(t).attr('name', 'ans' + count);
                $(t).attr('data-id', count);
                max = count;
            });

            fetch_ans();
        },

        fetch_ans = () => {
            // Get the script answers from database.
            const get_script_ans = makeAjaxCall(ans_method, {
                id: instance_id,
                type: component_type
            });

            get_script_ans.then(function (data) {

                if (data.data.length === 0) {
                    // get_default_answers();

                    const get_default_answer = makeAjaxCall(default_ans_method, {
                        seq: outline_id,
                    });

                    get_default_answer.then(function (data) {
                        $.each(data.default_answers, function (index, answer) {
                            $('#ans' + answer.tpda_answer_number).val(answer.tpda_content.replace('\\', ''));
                        });

                        console.log(data);
                    })
                } else {
                    $.each(data.data, function (index, answer) {
                        $('#ans' + (index + 1)).val(answer.answer.replace('\\', ''));
                    });
                }
            });
            fetch_collab_data();
        },

        fetch_collab_data = () => {

            // Fetch Collaboration data.
            const get_collab_data = makeAjaxCall('collab/get_collab_data', {
                type: component_type,
                id: instance_id,
                category: 'sc',
                filter_recipient: recipient
            });

            // Append Text for collaboration.
            get_collab_data.then(function (data) {
                user_name = data.collab_data[0].sc_sender;

                $.each(data.collab_data, function (index, collab_data) {
                    const formatted_date = moment(collab_data.sc_date).format('DD-MM-YY'),
                        formatted_time = moment(collab_data.sc_time, "HH:mm:ss").format('hh:mm A');

                    $('#ans' + collab_data.sc_ques_id).parent()
                        .after(
                            '<div style="margin-left: calc(4% + ' + $('#ans' + collab_data.sc_ques_id).parent().css('margin-left')
                            + ')" class="collab_response_row"><label>' + collab_data.sc_sender + '. ' +
                            formatted_date + ' ' + formatted_time + ' : ' + collab_data.sc_action + '</label>' +
                            '<div class="input-group" id="collaboration' + collab_data.sc_id + '">' +
                            '<span class="input-group-addon btn btn-secondary add_collab" ' +
                            'data-collab="' + collab_data.sc_id + '">' +
                            '<i class="fa fa-plus"></i></span>' +
                            '<span class="input-group-addon btn btn-secondary tts">' +
                            '<i class="fa fa-bullhorn"></i></span>' +
                            '<textarea onkeyup="auto_grow(this)" class="form-control custom-control" rows="2" ' +
                            'cols="70" style="height: 52px; font-size: 100%;" ' +
                            'data-collab="' + collab_data.sc_id + '" ' +
                            'data-id="' + collab_data.sc_ques_id + '">' + collab_data.sc_content + '</textarea>' +
                            '<span class="input-group-addon btn btn-secondary stt">' +
                            '<i class="fa fa-microphone"></i></span></div></div>')
                });

                // Fetch Collaborator data.
                const get_collaborator_data = makeAjaxCall('collab/get_collab_data', {
                    type: component_type,
                    id: instance_id,
                    category: 'scr'
                });

                // Append Text for collaborator.
                get_collaborator_data.then(function (data) {
                    $.each(data.collab_data, function (index, collab_data) {
                        const formatted_date = moment(collab_data.sc_date).format('DD-MM-YY'),
                            formatted_time = moment(collab_data.sc_time, "HH:mm:ss").format('hh:mm A'),
                            parent_margin = $('#collaboration' + collab_data.sc_collaboration_id).parent().css('margin-left');

                        $('#collaboration' + collab_data.sc_collaboration_id).parent()
                            .after(
                                '<div style="margin-left: calc(' + parent_margin + ')" class="collaborator_row"><label>'
                                + collab_data.sc_sender + '. ' +
                                formatted_date + ' ' + formatted_time + ' : ' + collab_data.sc_action + '</label>' +
                                '<div class="input-group">' +
                                '<span class="input-group-addon btn btn-secondary send-response" data-toggle="modal"' +
                                ' data-id="' + collab_data.sc_id + '" data-target="#send-collab-response-dialog">' +
                                '<i class="fa fa-paper-plane"></i></span>' +
                                '<span class="input-group-addon btn btn-secondary tts">' +
                                '<i class="fa fa-bullhorn"></i></span>' +
                                '<textarea onkeyup="auto_grow(this)"' +
                                ' class="form-control custom-control collaborator_response" rows="2" ' +
                                'cols="70" style="height: 52px; font-size: 100%; background-color: rgb(255, 251, 237)"' +
                                ' data-collab="' + collab_data.sc_id + '" ' +
                                'data-id="' + collab_data.sc_ques_id + '">' + collab_data.sc_content + '</textarea>' +
                                '<span class="input-group-addon btn btn-secondary stt">' +
                                '<i class="fa fa-microphone"></i></span>' +
                                '</div></div>')
                    });
                });
                voiceRecognition();
                speechFunctionality();
            });

            //Save Collaborator data to database.
            $(document).off('change', '.collaborator_response');
            $(document).on('change', '.collaborator_response', function () {
                const save_collab_data = makeAjaxCall('collab/update_collab_data', {
                    id: $(this).attr('data-collab'),
                    content: $(this).val()
                });

                save_collab_data.then(function (data) {
                    // console.log(data);
                })
            });

            // Send Email click handler.
            // $(document).off('click', '.send-response, .add_collab');
            // $(document).on('click', '.send-response', function() {
            //     console.log($(this).attr('data-id'));
            // });    $('#send-collab-dialog').on('show.bs.modal', function (event) {


            $('#send-collab-response-dialog').on('show.bs.modal', function (event) {
                const get_collab_info = makeAjaxCall('collab/get_collab_response_data', {
                    collab_id: $(event.relatedTarget).attr('data-id')
                });

                // Append Info to send modal.
                get_collab_info.then(function (data) {


                    $.each(data.response.format, function (index, data) {
                        $('#send-collab-format').append('<option value="' + data.collab_format_id + '">' + data.collab_format_name + '</option>');
                    });

                    $('#send-collab-response-sender-name').val(data.response['sender']);

                    $('#collab-response-send-recipient').empty();
                    $('#collab-response-send-recipient')
                        .append('<option selected>' + user_name + '</option>');
                    $('.collab-response-client-email').val(data.response['recipient_email']);
                });

                $('#send-collab-response-button').off('click');
                $('#send-collab-response-button').click(function () {
                    enableLoader();
                    const send_collab_response = makeAjaxCall('collab/send_collab_response',
                        $('#collab-response-form').serialize());

                    send_collab_response.then(function (data) {
                        $.unblockUI();
                        $('#send-collab-response-dialog').modal('hide');
                    });
                })
            });

            // Add Collaboration Message click handler.
            $(document).on('click', '.add_collab', function () {
                $('#add-collab-dialog').modal('show');

                const get_collaborator_details = makeAjaxCall('collab/get_collaborator_details', {
                    collab_id: $(this).attr('data-collab')
                });

                get_collaborator_details.then(function (data) {

                    $('#collab-action').empty();

                    // Iterate over each of the formats available
                    let optgroup = $("<optgroup label='Comment(s)'>");
                    $.each(data.collaborator_details.format, function (index, format) {

                        if (format.collab_format_id === '2' || format.collab_format_id === '3') {
                            $('#collab-action').append('<option value="' + format.collab_format_id + '">'
                                + format.collab_format_name + '</option>')
                        } else {
                            optgroup
                                .append('<option value="' + format.collab_format_id + '">'
                                    + format.collab_format_name + '</option>')
                        }
                    });

                    $('#collab-action').append(optgroup);

                    // Append Sender Name.
                    $('#recipient_name').val(data.collaborator_details.user).attr('readonly', true);
                    $('#sender_name').val(data.collaborator_details.recipient[0].sc_recipient).attr('readonly', true);

                    // Append text box after DB record is added.
                    // $(that).parent().after(
                    //     '<div style="margin: calc(1% + ' + parent_margin + ')" class="collab_response_row">' +
                    //     '<label>' + sender_name + '. ' +
                    //     formatted_date + ' ' + formatted_time + ' : ' + action + '</label>' +
                    //     '<div class="input-group">' +
                    //     '<span class="input-group-addon btn btn-secondary add_collab"><i class="fa fa-plus"></i></span>' +
                    //     '<span class="input-group-addon btn btn-secondary tts">' +
                    //     '<i class="fa fa-bullhorn"></i></span>' +
                    //     '<textarea onkeyup="auto_grow(this)" class="form-control custom-control" ' +
                    //     'rows="2" cols="70" style="height: 52px;" ' +
                    //     'data-id="' + ques_id + '" data-collab="' + data.collab_data_saved + '"></textarea>' +
                    //     '<span class="input-group-addon btn btn-secondary stt">' +
                    //     '<i class="fa fa-microphone"></i></span></div></div>')
                });
            });
        };

    return {
        init: init,
    };
}();