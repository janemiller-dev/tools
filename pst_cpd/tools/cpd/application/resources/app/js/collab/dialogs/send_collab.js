$(document).ready(function () {
    let client_id, client_collab, collab_id, type, random_name, origin, pdf_id, action, additional_text,
        path = window.location.pathname,
        components = path.split('/');

    client_id = components[components.length - 1];

    // When the dialog is displayed, set the current instance ID.
    $('#send-collab-dialog').on('show.bs.modal', function (event) {
        additional_text = $(event.relatedTarget).siblings('textarea').val();
        message = $(event.relatedTarget).data('message');
        collab_id = $(event.relatedTarget).data('id');

        $('#client-subject').val(message);
        tinymce.remove("#collab-content");

        // Fetch the collaboration data.
        const fetch_recipient = makeAjaxCall('collab/get_collab_details', {
            'id': collab_id
        });

        fetch_recipient.then(function (data) {

            $('#send-datetime').datetimepicker();
            $('#email-sender-name').val(data.collab_details.user);
            // $('#email-collab-format').val(data.collab_details.collab_format_name);

            // Append recipients name to select box.
            $('#collaborator_name').empty();
            $.each(data.collab_details.recipient, function (index, recipient) {
                $('#collaborator_name').append(
                    '<option value="' + recipient.collab_recipient_id + '">' +
                    recipient.collab_recipient_name + '</option>')
            });

            // Append Collab Format.
            $('#email-collab-format').empty();
            $.each(data.collab_details.format, function (index, format) {
                $('#email-collab-format').append(
                    '<option value="' + format.collab_format_id + '">' +
                    format.collab_format_name + '</option>')
            });
        });

        // Set Client collab.
        // $('#send-collab-id').val($('#collab').val());
        // tinymce.init({
        //     selector: '#collab-content',
        //     branding: false,
        //     init_instance_callback: function (ed) {
        //         tinyMCE.DOM.setStyle(tinyMCE.DOM.get('collab-content_ifr'), 'height', '55vh');
        //         tinyMCE.activeEditor.setContent(additional_text);
        //     }
        // });

        // Embed text to text area of Text editor.
        // const embed_content_selector = $('#embed-content');
        // embed_content_selector.unbind('click');
        // embed_content_selector.click(function () {
        //     const content = tinyMCE.activeEditor.getContent() +
        //         $('#canvas-container_ifr').contents().find('#tinymce').html();
        //
        //     tinyMCE.activeEditor.setContent(content);
        // });

        pdf_id = 0;
        $('#attachment_span').addClass('hidden');
    });

    $(document).on('click', '#close-send-collab-dialog', function () {
        $('#send-collab-dialog').modal('hide');
    });

    $(document).on('click', '#add-recipient', function () {
        $('.client-collab-div')
            .after('<div class="col-xs-9 col-xs-offset-3" style="padding:0; margin-top:5px !important">' +
                '<input type="collab" class="form-control client-collab"></div>');
    });

    // Sends the collabtional document.
    $(document).on('submit', '#send-collab-form', function (ev) {
        let email_array = [];
        $('.client-collab').each(function () {
            ('' !== $(this).val()) ? email_array.push($(this).val()) : '';
        });

        enableLoader();

        ev.preventDefault();
        ev.stopPropagation();

        // Get the text from editor.
        const send_collab = makeAjaxCall('collab/send_collab', {
                // tool_id: window.location.search.split('?id=')[1],
                subject: $('#client-subject').val(),
                text: $('#email-text').val(),
                id: collab_id,
                instance_id: activeId,
                outline_id: activeOutlineId,
                client_email: email_array,
                recipient_id: $('#collaborator_name').val(),
                type: window.location.pathname.split('/')[3],
            });

        send_collab.then(function (data) {
            $.unblockUI();

            toastr.success('Collaboration Email Sent!!');
            $('#send-collab-dialog').modal('hide');
            $('#collab-text').val('');
        });

        // $.ajax({
        //     url: jsglobals.base_url + 'collb/send_attachment',
        //     dataType: 'json',
        //     type: 'post',
        //     data: {
        //         client_id: client_id,
        //         client_collab: collab_array,
        //         id: collab_id,
        //         tool_id: window.location.search.split('?id=')[1],
        //         type: type,
        //         origin: origin,
        //         random: random_name,
        //         subject: $('#client-subject').val(),
        //         text: text,
        //         pdf_id: pdf_id,
        //     },
        //     error: ajaxError
        // }).done(function (data) {
        //     $.unblockUI();
        //     if (data.status != 'success') {
        //         toastr.error(data.message, 'Invalid collab ID')
        //         return;
        //     }
        //     else if (data.status == 'redirect') {
        //
        //         window.location.href = data.redirect;
        //         return;
        //     }
        //
        //     toastr.success('collab Sent!!');
        //     $('#send-collab-dialog').modal('hide');
        //     $('#collab-text').val('');
        // }).fail(function (jqXHR, textStatus) {
        //     $.unblockUI();
        //     toastr.error('Please check collab ID provided.', 'Unable to Send collab!!');
        //     return false;
        // }).always(function () {
        // });

        return false;
    })
});
