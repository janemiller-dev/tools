$(document).ready(function() {

    // Get the possible custom field types.
    $.ajax({
        url: jsglobals.base_url + "setting/get_cf_types",
        dataType: "json",
        type: 'post',
    }).done(function(data) {
        if (data.status == 'failure') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
	    window.location.href = data.redirect;
            return;
        }

	// Update the custom field type selection
	$('#cf-type').empty();
	$.each(data.cf_types, function(id, cf_type) {
	    $('#cf-type').append('<option value="' + cf_type.cft_name.toLowerCase() + '">' + cf_type.cft_name + '</option>');
	});

    }).fail(function(jqXHR, status) {
        toastr.error("Error");
    });

    // Catch changes to the field type.
    $('#cf-type').on('change', function() {

	// Show only the selected types type specific fields.
	$('.type-specific').hide();
	var type = $('#cf-type option:selected').text().toLowerCase()
	$('.type-specific-' + type).show();
    });

    // Validate the add cf form and submit it if it is valid.
    $('#add-cf-form').formValidation({
	framework: 'bootstrap',
	icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
	},
	fields: {
	    name: {
		validators: {
		    notEmpty: {
			message: "The custom field name is required."
		    }
		}
	    },
	    sys_name: {
		validators: {
		    notEmpty: {
			message: "The custom field system name is required."
		    }
		}
	    },
	}
    }).on('success.form.fv', function(e) {
	e.preventDefault();

	// Get the form
	var $form = $(e.target);
	var fv = $form.data('formValidation');

	// Get the form data and submit it.
	var message_data = $form.serialize();
	$.ajax({
	    url: jsglobals.base_url + 'setting/add_cf_definition',
	    dataType: 'json',
	    type: 'post',
	    data: message_data
	}).done(function(data) {
	    if (data.status != 'success') {
		toastr.error(data.message);
		return;
	    }
            else if (data.status == 'redirect') {
		window.location.href = data.redirect;
		return;
            }
	    $('#add-cf-dialog').modal('hide');
	    fv.resetForm();

	    refreshPage();
	}).fail(function(jqXHR, status) {
	    toastr.error("Server communication error. Please try again.");
        }).always(function() {
        });
    });

});
