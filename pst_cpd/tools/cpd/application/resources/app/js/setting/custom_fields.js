$(document).ready(function() {
    refreshPage();

});

// Update the page
function refreshPage() {
    updateCustomFields();
}

cf_datatable = null;

// Update the list of custom field definitions.
function updateCustomFields() {

    // Destroy the datatable if it already exists.
    if (cf_datatable != null)
	cf_datatable.destroy();

    // Create the databate - use server side data population.
    cf_datatable = $('#cf-table').DataTable({
        "pageLength": 100,
	"serverSide": true,
	"ajax":{
	    url: jsglobals.base_url + "setting/get_cf_definitions",
	    type: "post",
	},
	"columnDefs": [
	    {"targets": 0, 
	     "name": "ID",
	     "visible": false, 
	     "searchable": false,
	     "data": "cfd_id"
	    },
	    {"targets": 1, 
	     "name": "Object",
	     "visible": true, 
	     "searchable": true,
	     "data": "cfd_object_name"
	    },
	    {"targets": 2, 
	     "name": "Name",
	     "visible": true, 
	     "searchable": true,
	     "data": "cfd_name"
	    },
	    {"targets": 3, 
	     "name": "System Name",
	     "visible": true, 
	     "searchable": true,
	     "data": "cfd_system_name"
	    },
	    {"targets": 4, 
	     "name": "Type",
	     "visible": true, 
	     "searchable": true,
	     "data": "cft_name"
	    },
	    {"targets": 5, 
	     "name": "Type Extras",
	     "visible": true, 
	     "searchable": true,
	     "data": "cfd_type_extra",
	     "render": function(data, type, row, meta) {
		 var html = '';
		 $.each(row.type_extra, function(name, value) {
		     if (html.length > 0)
			 html += '<br/>';
		     html += name + ': ' + value;
		 });
		 return html;
	     }
	    },
	]
    });
}
