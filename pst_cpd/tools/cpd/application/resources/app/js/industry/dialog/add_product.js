$(document).ready(function () {

    $('#add-product-dialog').on('show.bs.modal', function (event) {
        const modal_id = $('#modal-id').val();
        $('#' + modal_id).modal('hide');
    });

    // Validate the add product form and submit it if it is valid.
    $('#add-product-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            product: {
                validators: {
                    notEmpty: {
                        message: "The product must be specified."
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the form
        var $form = $(e.target);
        var fv = $form.data('formValidation');

        // Get the form data and submit it.
        var data = $form.serialize();
        var add_new_product = makeAjaxCall('industry/add_new_product', data);

        add_new_product.then(function () {
            fv.resetForm();
            $($form)[0].reset();
            $('#add-product-dialog').modal('hide');
            refreshProducts();
        });
    });

});