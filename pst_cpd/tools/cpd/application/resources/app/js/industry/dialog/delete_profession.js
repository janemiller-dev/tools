$(document).ready(function() {

    // When the dialog is displayed, set the current profession ID.
    $('#delete-profession-dialog').on('show.bs.modal', function(event) {
	var gt_id = $(event.relatedTarget).data('id');
	var gt_name = $(event.relatedTarget).data('name');
	$('#profession-id').val(gt_id);
	$('#tgd-del-gt-name').html(gt_name);
    });

    // Validate the copy profession form and submit it if it is valid.
    $('#delete-profession-form').formValidation({
	framework: 'bootstrap',
	icon: {
		valid: 'glyphicon glyphicon-ok',
		invalid: 'glyphicon glyphicon-remove',
		validating: 'glyphicon glyphicon-refresh',
	},
	fields: {
	}
    }).on('success.form.fv', function(e) {
	e.preventDefault();

	// Get the form profession
	var $form = $(e.target);
	var fv = $form.data('formValidation');

	// Get the form data and submit it.
	var message_data = $form.serialize();
	$.ajax({
	    url: jsglobals.base_url + 'industry/delete_profession',
	    dataType: 'json',
	    type: 'post',
	    data: message_data
	}).done(function(data) {

	    if (data.status != 'success') {
			toastr.error(data.message);
			return;
	    }
		else if (data.status == 'redirect') {
			window.location.href = data.redirect;
			return;
		}
	    $('#delete-profession-dialog').modal('hide');
	    fv.resetForm();

	    refreshPage();
	}).fail(function(jqXHR, status) {
	    	toastr.error("Server communication error. Please try again.");
        }).always(function() {
        });
    });
});

