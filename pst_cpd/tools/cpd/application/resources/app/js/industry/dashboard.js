$(document).ready(function()
{
    refreshPage();
});

// Get the page data and display it.
function refreshPage()
{
    // Get the subscription status.
    $.ajax({
        url: jsglobals.base_url + "industry/get_industries",
        dataType: "json",
        type: 'post'
    }).done(function(data) {

        if (data.status == 'failure') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
	    window.location.href = data.redirect;
            return;
        }
        updateTypes(data.industries);

    }).fail(function(jqXHR, status) {
        toastr.error("Error");
    });
}

// Update the TGD industy types
function updateTypes(types)
{
    // Empty the table.
    $('#tgd-industry-table tbody').empty();

    // Are there any types?
    if (types.length == 0) {
	$('#no-tgd-industries').show();
	$('#tgd-industry-div').hide();
	return;
    }

    // Show the industry table and hide the "no types" message.
    $('#no-tgd-industries').hide();
    $('#tgd-industry-div').show();

    // Fill in the table.
    $.each(types, function(i, industry) {

	// Add a new row.
	$('#tgd-industry-table tbody')
	    .append($('<tr>')
		    .append($('<td>')
			    .append($('<a href="' + jsglobals.base_url + 'industry/industry/' + industry.industry_id + '">')
				    .append(industry.industry_name)))
		    .append($('<td>')
			    .append(industry.industry_description))
		    .append($('<td class="text-center">')
			    .append($('<button>')
				    .attr({
					"class": "btn btn-default btn-sm",
					"data-toggle": "modal",
					"data-target": "#copy-industry-dialog",
					"data-id": industry.industry_id
				    })
				    .append($('<span class="glyphicon glyphicon-duplicate">'))))
		    .append($('<td class="text-center">')
			    .append($('<button>')
				    .attr({
					"class": "btn btn-default btn-sm",
					"data-toggle": "modal",
					"data-target": "#delete-industry-dialog",
					"data-id": industry.industry_id,
					"data-name": industry.industry_name
				    })
				    .append($('<span class="glyphicon glyphicon-trash">'))))
		    );
    });
}
