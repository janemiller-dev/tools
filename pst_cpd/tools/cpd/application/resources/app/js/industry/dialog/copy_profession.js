$(document).ready(function() {

    // When the dialog is displayed, set the current profession ID.
    $('#copy-profession-dialog').on('show.bs.modal', function(event) {
		var gt_id = $(event.relatedTarget).data('id');

		$('#save-as-name').val('');
		$('#copy-profession-id').val(gt_id);

		// Get the list of possible other industries.
		$.ajax({
			url: jsglobals.base_url + 'industry/get_industries',
			dataType: 'json',
			type: 'post',
		}).done(function(data) {

			if (data.status != 'success') {
				toastr.error(data.message);
				return;
			}
			else if (data.status == 'redirect') {
				window.location.href = data.redirect;
				return;
			}
			copyProfessionUpdateIndustries(data.industries, gt_id);
		}).fail(function(jqXHR, status) {
				toastr.error("Server communication error. Please try again.");
			}).always(function() {
			});

    });

    // Validate the copy profession form and submit it if it is valid.
    $('#copy-profession-form').formValidation({
	framework: 'bootstrap',
	icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
	},
	fields: {
	}
    }).on('success.form.fv', function(e) {
	e.preventDefault();

	// Get the form profession
	var $form = $(e.target);
	var fv = $form.data('formValidation');

	// Get the form data and submit it.
	var message_data = $form.serialize();
	$.ajax({
	    url: jsglobals.base_url + 'industry/copy_profession',
	    dataType: 'json',
	    type: 'post',
	    data: message_data
	}).done(function(data) {

	    if (data.status != 'success') {
			toastr.error(data.message);
			return;
	    }
		else if (data.status == 'redirect') {
			window.location.href = data.redirect;
			return;
		}
	    $('#copy-profession-dialog').modal('hide');
	    fv.resetForm();

	    refreshPage();

	    // Open the new profession in a new tab.
	    var address = jsglobals.base_url + "industry/profession/" + data.profession_id;
	    var newwin = window.open(address, '_blank');

	    if (newwin) {
			newwin.focus();
	    }
	    else {
			toastr.error('Please allow popups for this website');
	    }
	}).fail(function(jqXHR, status) {
	   		toastr.error("Server communication error. Please try again.");
        }).always(function() {
        });
    });
});

// Update the list of industries in the drop down.
function copyProfessionUpdateIndustries(other_industries, ignore_id) {

    // Clear the possible industries.
    $('#save-as-id').empty();
    $('#save-as-id').append($('<option value="0" selected="selected">Select Profession</option>'));

    // If there are no existing industries, hide the save as existing...
    if (other_industries.length == 0 || (other_industries.length == 1 && typeof ignore_id != 'undefined')) {
		$('#select-existing-description').hide();
		$('#select-existing-div').hide();
		return;
    }

    // Show the form and description
    $('#select-existing-description').show();
    $('#select-existing-div').show();

    // Show each possible profession.
    $.each(other_industries, function(i, profession) {
		if (ignore_id == 'undefined' || profession.profession_id != ignore_id)
			$('#save-as-id').append($('<option value="' + profession.profession_id + '">' + profession.profession_name + '</option>'));
    });
}
