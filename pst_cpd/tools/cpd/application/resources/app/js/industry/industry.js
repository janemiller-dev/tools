$(document).ready(function()
{
    refreshPage();
});

// Get the page data and display it.
function refreshPage()
{
    // Get the ID from the URL
    var path = window.location.pathname;
    var components = path.split('/');
    var id = components[components.length - 1];

    // Get the subscription status.
    $.ajax({
	    url: jsglobals.base_url + "industry/get_industry",
        dataType: "json",
        type: 'post',
		data: {
			id: id
		}
    }).done(function(data) {

        if (data.status == 'failure') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
	    window.location.href = data.redirect;
            return;
        }
        updateGameType(data.game_type);

    }).fail(function(jqXHR, status) {
        toastr.error("Error");
    });
}

// Update the Game Type
function updateGameType(game_type)
{
    // Update the name.
    $('#industry-name').html(game_type.industry_name).addClass('editable')
	.attr({
	    'data-url': jsglobals.base_url + 'common/update_db_field',
	    'data-name': 'industry.industry_name',
	    'data-pk': game_type.industry_id,
	    'data-type': 'text',
	    'title': 'New Name'
	});

    // Update the description.
    $('#industry-description').html(game_type.industry_description).addClass('editable')
	.attr({
	    'data-url': jsglobals.base_url + 'common/update_db_field',
	    'data-name': 'industry.industry_description',
	    'data-pk': game_type.industry_id,
	    'data-type': 'textarea',
	    'title': 'New Description'
	});

    // Show the conditions if there are any.
    $('#profession-table tbody').empty();

    if (typeof game_type.profession == 'undefined' || game_type.profession.length == 0) {
		$('#no-profession').show();
		$('#profession-table-div').hide();
    }
    else {
		$('#no-profession').hide();
		$('#profession-table-div').show();
		$.each(game_type.profession, function(i, profession) {

		    // Convert the condition type to a name.
		    var cond_value = '<span class=profession-label>';
		    $('#profession-table tbody')
			    .append($('<tr>')
					.append($('<td class="text-left">')
						.append($('<a href="' + jsglobals.base_url + 'industry/profession/' + profession.profession_id + '">')
						.append(profession.profession_name)))
						.append($('<td class="text-left">')
						.append(profession.profession_description))
				    .append($('<td class="text-center">')
					    .append($('<button>')
						    .attr({
							    "class": "btn btn-default btn-sm",
							    "data-toggle": "modal",
							    "data-target": "#copy-profession-dialog",
							    "data-id": profession.profession_id
						    })
						    .append($('<span class="glyphicon glyphicon-duplicate">'))))
			    );

		    // Show the delete icon.
		    $('#profession-table tbody tr:last')
			    .append($('<td class="text-center">')
					.append($('<button>')
					   .attr({
						   "class": "btn btn-default btn-sm",
						   "data-toggle": "modal",
						   "data-target": "#delete-profession-dialog",
						   "data-id": profession.profession_id,
						   "data-name": profession.profession_name
					   })
					.append($('<span class="glyphicon glyphicon-trash">'))));
		});
    }

    setEditable();
}
