$(document).ready(function ()
{
	// When the dialog is displayed, clear the values
	$('#add-condition-dialog').on('show.bs.modal', function (event) {
		$('#profession-add-c-type').val('cond');
		$('#profession-add-c-name').val('');
		$('#profession-add-c-description').val('');
		$('#profession-add-c-tooltip').val('');
		$('#profession-add-c-uom').val('n');
	});

	// Validate the add condition form and submit it if it is valid.
	$('#add-condition-form').formValidation({
		framework: 'bootstrap',
		icon: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			name: {
				validators: {
					notEmpty: {
						message: "The name is required."
					}
				}
			},
			description: {
				validators: {
					notEmpty: {
						message: "The description is required."
					}
				}
			},
			tooltip: {
				validators: {
					notEmpty: {
						message: "The help text is required."
					}
				}
			}
		}
	}).on('success.form.fv', function (e) {
		e.preventDefault();

		// Get the ID from the URL
		var path = window.location.pathname;
		var components = path.split('/');
		var id = components[components.length - 1];
		$('#profession-add-c-id').val(id);

		// Get the form condition
		var $form = $(e.target);
		var fv = $form.data('formValidation');

		// Get the form data and submit it.
		var message_data = $form.serialize();
		$.ajax({
			url: jsglobals.base_url + 'industry/add_condition',
			dataType: 'json',
			type: 'post',
			data: message_data,
			error: ajaxError
		}).done(function (data) {
			if (data.status != 'success') {
				toastr.error(data.message);
				return;
			} else if (data.status == 'redirect') {
				window.location.href = data.redirect;
				return;
			}
			$('#add-condition-dialog').modal('hide');
			fv.resetForm();

			refreshPage();

		}).fail(function (jqXHR, status) {
			toastr.error("Server communication error. Please try again.");
		}).always(function () {
		});
	});
});
