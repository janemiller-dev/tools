$(document).ready(function() {

    // When the dialog is displayed, set the current multiplier ID.
    $('#delete-multiplier-dialog').on('show.bs.modal', function(event) {
	var m_id = $(event.relatedTarget).data('id');
	var m_name = $(event.relatedTarget).data('name');
	$('#industry-delete-m-id').val(m_id);
	$('#industry-delete-m-name').html(m_name);
    });

    // Validate the copy multiplier form and submit it if it is valid.
    $('#delete-multiplier-form').formValidation({
	framework: 'bootstrap',
	icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
	},
	fields: {
	}
    }).on('success.form.fv', function(e) {
	e.preventDefault();

	// Get the form multiplier
	var $form = $(e.target);
	var fv = $form.data('formValidation');

	// Get the form data and submit it.
	var message_data = $form.serialize();
	$.ajax({
	    url: jsglobals.base_url + 'industry/delete_multiplier',
	    dataType: 'json',
	    type: 'post',
	    data: message_data
	}).done(function(data) {

	    if (data.status != 'success') {
			toastr.error(data.message);
			return;
	    }
		else if (data.status == 'redirect') {
			window.location.href = data.redirect;
			return;
		}
	    $('#delete-multiplier-dialog').modal('hide');
	    fv.resetForm();
	    refreshPage();
	}).fail(function(jqXHR, status) {
	    toastr.error("Server communication error. Please try again.");
        }).always(function() {
        });
    });
});

