$(document).ready(function() {

    // When the dialog is displayed, set the current industry ID.
    $('#copy-industry-dialog').on('show.bs.modal', function(event) {
	var gt_id = $(event.relatedTarget).data('id');

	$('#save-as-name').val('');
	$('#copy-industry-id').val(gt_id);

	// Get the list of possible other industries.
	$.ajax({
	    url: jsglobals.base_url + 'industry/get_industries',
	    dataType: 'json',
	    type: 'post'
	}).done(function(data) {

	    if (data.status != 'success') {
			toastr.error(data.message);
			return;
	    }
	    else if (data.status == 'redirect') {
			window.location.href = data.redirect;
			return;
		}
	    copyIndustryUpdateIndustries(data.industries, gt_id);
	}).fail(function(jqXHR, status) {
	   		toastr.error("Server communication error. Please try again.");
        }).always(function() {
        });

    });

    // Validate the copy industry form and submit it if it is valid.
    $('#copy-industry-form').formValidation({
		framework: 'bootstrap',
		icon: {
				valid: 'glyphicon glyphicon-ok',
				invalid: 'glyphicon glyphicon-remove',
				validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
		}
    }).on('success.form.fv', function(e) {
	e.preventDefault();

	// Get the form industry
	var $form = $(e.target);
	var fv = $form.data('formValidation');

	// Get the form data and submit it.
	var message_data = $form.serialize();
	$.ajax({
	    url: jsglobals.base_url + 'industry/copy_industry',
	    dataType: 'json',
	    type: 'post',
	    data: message_data
	}).done(function(data) {

	    if (data.status != 'success') {
			toastr.error(data.message);
			return;
	    }
		else if (data.status == 'redirect') {
			window.location.href = data.redirect;
			return;
		}
	    $('#copy-industry-dialog').modal('hide');
	    fv.resetForm();

	    refreshPage();

	    // Open the new industry in a new tab.
	    var address = jsglobals.base_url + "industry/industry/" + data.industry_id;
	    var newwin = window.open(address, '_blank');
	    if (newwin) {
			newwin.focus();
	    }
	    else {
			toastr.error('Please allow popups for this website');
	    }
	}).fail(function(jqXHR, status) {
	   		toastr.error("Server communication error. Please try again.");
        }).always(function() {
        });
    });
});

// Update the list of industries in the drop down.
function copyIndustryUpdateIndustries(other_industries, ignore_id) {

    // Clear the possible industries.
    $('#save-as-id').empty();
    $('#save-as-id').append($('<option value="0" selected="selected">Select Industry</option>'));

    // If there are no existing industries, hide the save as existing...
    if (other_industries.length == 0 || (other_industries.length == 1 && typeof ignore_id != 'undefined')) {
		$('#select-existing-description').hide();
		$('#select-existing-div').hide();
		return;
    }

    // Show the form and description
    $('#select-existing-description').show();
    $('#select-existing-div').show();

    // Show each possible industry.
    $.each(other_industries, function(i, industry) {
		if (ignore_id == 'undefined' || industry.industry_id != ignore_id)
			$('#save-as-id').append($('<option value="' + industry.industry_id + '">' + industry.industry_name + '</option>'));
    });
}
