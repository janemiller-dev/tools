$(document).ready(function() {

    // When the dialog is displayed, clear the values
    $('#add-industry-dialog').on('show.bs.modal', function(event) {
	$('#industry-name').val('');
	$('#industry-description').html('');
    });

    // Validate the add industry form and submit it if it is valid.
    $('#add-industry-form').formValidation({
	framework: 'bootstrap',
	icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
	},
	fields: {
	    name: {
		validators: {
		    notEmpty: {
			message: "The industry name is required."
		    }
		}
	    },
	    description: {
		validators: {
		    notEmpty: {
			message: "The industry mission is required."
		    }
		}
	    }
	}
    }).on('success.form.fv', function(e) {
        e.preventDefault();

        // Get the form industry
        var $form = $(e.target);
        var fv = $form.data('formValidation');

        // Get the form data and submit it.
        var message_data = $form.serialize();
        $.ajax({
            url: jsglobals.base_url + 'industry/add_industry',
            dataType: 'json',
            type: 'post',
            data: message_data
        }).done(function(data) {

            if (data.status != 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            $('#add-industry-dialog').modal('hide');
            fv.resetForm();

            refreshPage();

            // Open the new industry in a new tab.
            var address = jsglobals.base_url + "industry/industry/" + data.industry_id;
            var newwin = window.open(address, '_blank');

            if (newwin) {
                newwin.focus();
            }
            else {
                toastr.error('Please allow popups for this website');
            }

        }).fail(function(jqXHR, status) {
                toastr.error("Server communication error. Please try again.");
            }).always(function() {
            });
    });
});
