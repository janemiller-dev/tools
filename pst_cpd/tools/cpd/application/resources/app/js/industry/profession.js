$(document).ready(function()
{
	refreshPage();

	// Catch the clicks on the up arrow.
	$('#condition-table tbody').on('click', '.condition-move', function() {
		var profession_c_id = $(this).data('id');
		var direction = $(this).data('direction');;

		// Do the operation.
		$.ajax({
			url: jsglobals.base_url + "industry/move_condition/" + direction,
			dataType: "json",
			type: 'post',
			data: {
				id: profession_c_id
			}
		}).done(function(data) {

			if (data.status == 'failure') {
				toastr.error(data.message);
				return;
			}
			else if (data.status == 'redirect') {
				window.location.href = data.redirect;
				return;
			}
			refreshPage();
		}).fail(function(jqXHR, status) {
			toastr.error("Error");
		});
	});

	// Catch the clicks on the up arrow.
	$('#multiplier-table tbody').on('click', '.multiplier-move', function() {
		var profession_c_id = $(this).data('id');
		var direction = $(this).data('direction');;

		// Do the operation.
		$.ajax({
			url: jsglobals.base_url + "industry/move_multiplier/" + direction,
			dataType: "json",
			type: 'post',
			data: {
				id: profession_c_id
			}
		}).done(function(data) {

			if (data.status == 'failure') {
				toastr.error(data.message);
				return;
			}
			else if (data.status == 'redirect') {
				window.location.href = data.redirect;
				return;
			}
			refreshPage();
		}).fail(function(jqXHR, status) {
			toastr.error("Error");
		});
	});
});

// Get the page data and display it.
function refreshPage()
{
	// Get the ID from the URL
	var path = window.location.pathname;
	var components = path.split('/');
	var id = components[components.length - 1];

	// Get the subscription status.
	$.ajax({
		url: jsglobals.base_url + "industry/get_profession",
		dataType: "json",
		type: 'post',
		data: {
			id: id
		}
	}).done(function(data) {

		if (data.status == 'failure') {
			toastr.error(data.message);
			return;
		}
		else if (data.status == 'redirect') {
			window.location.href = data.redirect;
			return;
		}
		updateProfession(data.game_type);
	}).fail(function(jqXHR, status) {
		toastr.error("Error");
	});
}

// Update the Profession
function updateProfession(game_type)
{
	// Update the industry link.
    $('#industry-name').empty();
    $('#industry-name').append($('<a>')
			       .attr({
				   'href': jsglobals.base_url + 'industry/industry/' + game_type.industry_id
			       })
			       .append(game_type.industry_name));

	// Update the name.
	$('#profession-name').html(game_type.profession_name).addClass('editable')
		.attr({
			'data-url': jsglobals.base_url + 'common/update_db_field',
			'data-name': 'profession.profession_name',
			'data-pk': game_type.profession_id,
			'data-type': 'text',
			'title': 'New Name'
		});

	// Update the description.
	$('#profession-description').html(game_type.profession_description).addClass('editable')
		.attr({
			'data-url': jsglobals.base_url + 'common/update_db_field',
			'data-name': 'profession.profession_description',
			'data-pk': game_type.profession_id,
			'data-type': 'textarea',
			'title': 'New Description'
		});

	// Add the Add Condition Button.
	if (!$('#add-condition-btn').length) {
		$('#tool-buttons')
			.append($('<button type="button" class="btn btn-primary" id="add-condition-btn" data-toggle="modal" data-target="#add-condition-dialog" data-id="'
				+ game_type.profession_c_id
				+ '"><span class="has-tooltip" data-toggle="tooltip" title="Add a new condition or objective." data-placement="top">Add Condition</span></button>'));
	}

	// Add the Add Multiplier/Adder Button
	if (!$('#add-multiplier-btn').length) {
		$('#tool-buttons')
			.append($('<button type="button" class="btn btn-primary" id="add-multiplier-btn" data-toggle="modal" data-target="#add-multiplier-dialog" data-id="'
				+ game_type.profession_c_id
				+ '"><span class="has-tooltip" data-toggle="tooltip" title="Add a new multiplier or adder." data-placement="top">Add Multiplier/Adder</span></button>'));
	}

	// Show the conditions if there are any.
	$('#condition-table tbody').empty();

	if (typeof game_type.conditions === 'undefined' || game_type.conditions.length === 0) {
		$('#no-conditions').show();
		$('#condition-table-div').hide();
	}
	else {
		$('#no-conditions').hide();
		$('#condition-table-div').show();
		$.each(game_type.conditions, function(i, condition) {

			// Convert the condition type to a name.
			var cond_value = '<span class=condition-label>';

			if (condition.profession_c_type === 'obj')
				cond_value += 'Objective';
			else
				cond_value += 'Condition';
			cond_value += '</span>';

			$('#condition-table tbody').append($('<tr>')
				.append($('<td class="text-left">')
						.append(cond_value))
				.append($('<td class="text-left">')
					.addClass('editable')
					.attr({
						'data-url': jsglobals.base_url + 'common/update_db_field',
						'data-name': 'profession_condition.profession_c_name',
						'data-pk': condition.profession_c_id,
						'data-type': 'text',
						'title': 'Change Condition Name'
					})
					.append(condition.profession_c_name))
				.append($('<td class="text-left">').addClass('editable')
					.attr({
						'data-url': jsglobals.base_url + 'common/update_db_field',
						'data-name': 'profession_condition.profession_c_uom',
						'data-pk': condition.profession_c_id,
						'data-type': 'select',
						'data-source': jsglobals.base_url + 'code/get_codes/uom',
						'data-value': condition.profession_c_uom,
						'title': 'Change Condition Unit'
					}))
				.append($('<td class="text-left">').addClass('editable')
					.attr({
						'data-url': jsglobals.base_url + 'common/update_db_field',
						'data-name': 'profession_condition.profession_c_description',
						'data-pk': condition.profession_c_id,
						'data-type': 'text',
						'title': 'Change Condition Description'
					})
					.append(condition.profession_c_description))
				.append($('<td class="text-left">').addClass('editable')
					.attr({
						'data-url': jsglobals.base_url + 'common/update_db_field',
						'data-name': 'profession_condition.profession_c_tooltip',
						'data-pk': condition.profession_c_id,
						'data-type': 'text',
						'title': 'Change Condition Tool Tip'
					})
					.append(condition.profession_c_tooltip))
			);

			// Up arrow?
			if (i <= 1) {
				$('#condition-table tbody tr:last').append($('<td>').append(''));
			}
			else {
				$('#condition-table tbody tr:last').append($('<td class="text-center">')
					.append($('<button>')
						.attr({
							"class": "btn btn-default btn-sm condition-move",
							"data-id": condition.profession_c_id,
							"data-direction": "up",
						})
						.append($('<span class="glyphicon glyphicon-arrow-up">'))));
			}

			// Show the delete icon.
			$('#condition-table tbody tr:last').append($('<td class="text-center">')
				.append($('<button>')
					.attr({
						"class": "btn btn-default btn-sm",
						"data-toggle": "modal",
						"data-target": "#delete-condition-dialog",
						"data-id": condition.profession_c_id,
						"data-name": condition.profession_c_name
					})
					.append($('<span class="glyphicon glyphicon-trash">'))));
		});
	}

	// Show the multipliers
	$('#multiplier-table tbody').empty();

	if (typeof game_type.multipliers == 'undefined' || game_type.multipliers.length == 0) {
		$('#no-multipliers').show();
		$('#multiplier-table-div').hide();
	}
	else {
		$('#no-multipliers').hide();
		$('#multiplier-table-div').show();
		$.each(game_type.multipliers, function(i, multiplier) {

			// Show the multiplier.
			$('#multiplier-table tbody').append($('<tr>')
				.append($('<td class="text-left">').addClass('editable')
					.attr({
						'data-url': jsglobals.base_url + 'common/update_db_field',
						'data-name': 'profession_multiplier.profession_m_name',
						'data-pk': multiplier.profession_m_id,
						'data-type': 'text',
						'title': 'Change Multiplier/Adder Name'
					})
					.append(multiplier.profession_m_name))
				.append($('<td class="text-left">').addClass('editable')
					.attr({
						'data-url': jsglobals.base_url + 'common/update_db_field',
						'data-name': 'profession_multiplier.profession_m_description',
						'data-pk': multiplier.profession_m_id,
						'data-type': 'text',
						'title': 'Change Multiplier/Adder Description'
					})
					.append(multiplier.profession_m_description))
				.append($('<td class="text-left">').addClass('editable')
					.attr({
						'data-url': jsglobals.base_url + 'common/update_db_field',
						'data-name': 'profession_multiplier.profession_m_type',
						'data-pk': multiplier.profession_m_id,
						'data-type': 'select',
						'data-source': jsglobals.base_url + 'code/get_codes/mult_t',
						'data-value': multiplier.profession_m_type,
						'title': 'Change Multiplier Type'
					}))
				.append($('<td class="text-left">').addClass('editable')
					.attr({
						'data-url': jsglobals.base_url + 'common/update_db_field',
						'data-name': 'profession_multiplier.profession_m_operation',
						'data-pk': multiplier.profession_m_id,
						'data-type': 'select',
						'data-source': jsglobals.base_url + 'code/get_codes/mult_o',
						'data-value': multiplier.profession_m_operation,
						'title': 'Change Multiplier operation'
					}))
				.append($('<td class="text-left">').addClass('editable')
					.attr({
						'data-url': jsglobals.base_url + 'common/update_db_field',
						'data-name': 'profession_multiplier.profession_m_propagate',
						'data-pk': multiplier.profession_m_id,
						'data-type': 'select',
						'data-source': jsglobals.base_url + 'code/get_codes/yes_no',
						'data-value': multiplier.profession_m_propagate,
						'title': 'Propagate on Insert?'
					}))
			);

			if (i < 1) {
				$('#multiplier-table tbody tr:last').append($('<td>').append(''));
			}
			else {
				$('#multiplier-table tbody tr:last').append($('<td class="text-center">')
					.append($('<button>')
						.attr({
							"class": "btn btn-default btn-sm multiplier-move",
							"data-id": multiplier.profession_m_id,
							"data-direction": "up"
						})
						.append($('<span class="glyphicon glyphicon-arrow-up">'))));
			}


			$('#multiplier-table tbody tr:last').append($('<td class="text-center">')
				.append($('<button>')
					.attr({
						"class": "btn btn-default btn-sm",
						"data-toggle": "modal",
						"data-target": "#delete-multiplier-dialog",
						"data-id": multiplier.profession_m_id,
						"data-name": multiplier.profession_m_name
					})
					.append($('<span class="glyphicon glyphicon-trash">'))));
		});
	}

	setEditable();
}
