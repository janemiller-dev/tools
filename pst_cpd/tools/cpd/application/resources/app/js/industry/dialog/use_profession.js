$(document).ready(function () {

    // Set the list of industries
    $.ajax({
	url: jsglobals.base_url + 'industry/get_industries',
	dataType: 'json',
	type: 'post',
	error: ajaxError
    }).done(function (data) {

	if (data.status != 'success') {
	    toastr.error(data.message);
	    return;
	}
	else if (data.status == 'redirect') {
	    window.location.href = data.redirect;
	    return;
	}
	$('#up-industry').empty();
	$('#up-industry').append($('<option value="0">-- Select Industry --</option>'));
	$.each(data.industries, function(i, industry) {
	    $('#up-industry').append($('<option value="' + industry.industry_id + '">' + industry.industry_name + '</option>'));
	});

    }).fail(function () {
	toastr.error("Server communication error. Please try again.");
    }).always(function () {
    });

    // Handle a change in the industry.
    $('#up-industry').change(function () {
	// Clear the old list.
	$('#up-profession').empty();

	// If the change is to "New Profession", show the add profession modal dialog.
	if ($('#up-industry').val() === "0") {
	    $('#up-profession').append($('<option value="0">-- Select Industry First --</option>'));
	}
	else {
	    $.ajax({
		url: jsglobals.base_url + 'industry/get_unused_professions',
		dataType: 'json',
		type: 'post',
		data: {
		    'industry_id': $('#up-industry').val()
		}
	    }).done(function (data) {

		if (data.status != 'success') {
		    toastr.error(data.message);
		    return;
		}
		else if (data.status == 'redirect') {
		    window.location.href = data.redirect;
		    return;
		}

		// If there are no professions, let the user know that he has used all of them.
		if (data.professions.length === 0) {
		    $('#up-profession').append($('<option value="0">-- All Professions Used --</option>'));
		}
		else {
		    $.each(data.professions, function(i, profession) {
			$('#up-profession').append($('<option value="' + profession.profession_id + '">' + profession.profession_name + '</option>'));
		    });
		}

	    }).fail(function (jqXHR, textStatus) {
		toastr.error("Request failed: " + textStatus);
	    }).always(function () {
	    });
	}
    });

    // Validate the add cpd form and submit it if it is valid.
    $('#use-profession-form').formValidation({
	framework: 'bootstrap',
	icon: {
	    valid: 'glyphicon glyphicon-ok',
	    invalid: 'glyphicon glyphicon-remove',
	    validating: 'glyphicon glyphicon-refresh'
	},
	fields: {
	    industry: {
		validators: {
		    notZero: {
			message: "The industry must be specified."
		    }
		}
	    },
	    profession: {
		validators: {
		    notZero: {
			message: "The profession must be specified."
		    }
		}
	    },
	    year: {
		validators: {
		    notZero: {
			message: "The objective year must be specified."
		    }
		}
	    },
	    floor: {
		validators: {
		    notEmpty: {
			message: "The objective floor must be speified."
		    }
		}
	    },
	    target: {
		validators: {
		    notEmpty: {
			message: "The objective target must be speified."
		    }
		}
	    },
	    game: {
		validators: {
		    notEmpty: {
			message: "The objective game must be speified."
		    }
		}
	    }
	}
    }).on('success.form.fv', function (e) {
	e.preventDefault();

	// Get the form
	var $form = $(e.target);
	var fv = $form.data('formValidation');

	// Get the form data and submit it.
	var data = $form.serialize();
	$.ajax({
	    url: jsglobals.base_url + 'industry/use_new_profession',
	    dataType: 'json',
	    type: 'post',
	    data: data,
	    error: ajaxError
	}).done(function (data) {

	    if (data.status != 'success') {
            toastr.error(data.message);
            return;
	    }
	    else if (data.status == 'redirect') {
            window.location.href = data.redirect;
            return;
	    }

	    //clear the validation rules and form data.
	    fv.resetForm();
        $($form)[0].reset();
	    $('#use-profession-dialog').modal('hide');

	    //refresh the selected profession and page.
	    refreshProfessions();
	    // refreshPage();
	}).fail(function (jqXHR, textStatus) {
	    toastr.error("Request failed: " + textStatus);
	}).always(function () {
	});
    });
});
