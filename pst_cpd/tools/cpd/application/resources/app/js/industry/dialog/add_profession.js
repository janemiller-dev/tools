$(document).ready(function()
{
    // When the dialog is displayed, clear the values
    $('#add-profession-dialog').on('show.bs.modal', function(event) {
		$('#profession-name').val('');
		$('#profession-description').html('');
    });

    // Validate the add profession form and submit it if it is valid.
    $('#add-profession-form').formValidation({
		framework: 'bootstrap',
		icon: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
		    name: {
				validators: {
				    notEmpty: {
						message: "The profession name is required."
				    }
				}
		    },
		    description: {
				validators: {
				    notEmpty: {
						message: "The profession role is required."
			        }
				}
	        }
		}
    }).on('success.form.fv', function(e) {
		e.preventDefault();

	    // Get the ID from the URL
	    var path = window.location.pathname;
	    var components = path.split('/');
	    var id = components[components.length - 1];
	    $('#industry-p-id').val(id);

		// Get the form profession
		var $form = $(e.target);
		var fv = $form.data('formValidation');

		// Get the form data and submit it.
		var message_data = $form.serialize();
		$.ajax({
		    url: jsglobals.base_url + 'industry/add_profession',
		    dataType: 'json',
		    type: 'post',
		    data: message_data
		}).done(function(data) {

		    if (data.status != 'success') {
				toastr.error(data.message);
				return;
		    } else if (data.status == 'redirect') {
				window.location.href = data.redirect;
				return;
            }
		    $('#add-profession-dialog').modal('hide');
		    fv.resetForm();

		    refreshPage();

		}).fail(function(jqXHR, status) {
		    toastr.error("Server communication error. Please try again.");
        }).always(function() {});
    });
});
