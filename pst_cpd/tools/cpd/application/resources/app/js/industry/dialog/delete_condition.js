$(document).ready(function () {

	// When the dialog is displayed, set the current condition ID.
	$('#delete-condition-dialog').on('show.bs.modal', function (event) {
		var c_id = $(event.relatedTarget).data('id');
		var c_name = $(event.relatedTarget).data('name');
		$('#profession-delete-c-id').val(c_id);
		$('#profession-delete-c-name').html(c_name);
	});

	// Validate the copy condition form and submit it if it is valid.
	$('#delete-condition-form').formValidation({
		framework: 'bootstrap',
		icon: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {}
	}).on('success.form.fv', function (e) {
		e.preventDefault();

		// Get the form condition
		var $form = $(e.target);
		var fv = $form.data('formValidation');

		// Get the form data and submit it.
		var message_data = $form.serialize();
		$.ajax({
			url: jsglobals.base_url + 'industry/delete_condition',
			dataType: 'json',
			type: 'post',
			data: message_data,
			error: ajaxError
		}).done(function (data) {

			if (data.status != 'success') {
				toastr.error(data.message);
				return;
			}
			else if (data.status == 'redirect') {
				window.location.href = data.redirect;
				return;
			}
			$('#delete-condition-dialog').modal('hide');
			fv.resetForm();

			refreshPage();
		}).fail(function (jqXHR, status) {
			toastr.error("Server communication error. Please try again.");
		}).always(function () {
		});
	});
});

