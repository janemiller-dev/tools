$(document).ready(function() {

    // Handle the delete company dialog
    $('#delete-company-dialog').on('show.bs.modal', function(e) {
	var id = $(e.relatedTarget).data('id');
	var name = $(e.relatedTarget).data('name');
	$('#confirm-msg').html(name);
	$('#dm-company-id').val(id);
    });

    // Manage the delete company form.
    $('#delete-company-form').bootstrapValidator()
	.on('success.form.bv', function(e) {
	    e.preventDefault();

	    // Get the form instance
	    var $form = $(e.target);
	    var bv = $form.data('bootstrapValidator');

	    // Get the form data and submit it.
	    var message_data = $form.serialize();
	    $.ajax({
		url: jsglobals.base_url + 'company/delete_company',
		dataType: 'json',
		type: 'post',
		data: message_data
	    }).done(function(data) {
		if (data.status != 'success')
		    toastr.error(data.message);
		else {
		    refreshPage();
		    $('#delete-company-dialog').modal('hide');
		}
		bv.resetForm();
	    }).fail(function(jqXHR, status) {
		toastr.error("Server communication error. Please try again.");
	    }).always(function() {
	    });
	}).on('error.form.bv', function(e) {
	});
});
