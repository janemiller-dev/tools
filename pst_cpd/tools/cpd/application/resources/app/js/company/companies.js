$(document).ready(function() {
    refreshPage();

});

// Update the page
function refreshPage() {
    updateCompanies();
}

company_datatable = null;

// Update the list of companies.
function updateCompanies() {

    // Destroy the datatable if it already exists.
    if (company_datatable != null)
	company_datatable.destroy();

    // Create the databate - use server side data population.
    company_datatable = $('#company-table').DataTable({
        "pageLength": 100,
	"serverSide": true,
	//"responsive": true,
	"ajax":{
	    url: jsglobals.base_url + "company/get_companies",
	    type: "post",
	},
	"columnDefs": [
	    {"targets": 0, 
	     "name": "ID",
	     "visible": false, 
	     "searchable": false,
	     "data": "company_id"
	    },
	    {"targets": 1, 
	     "name": "Name",
	     "visible": true, 
	     "searchable": true,
	     "data": "company_name",
	     "render": function(data, type, row, meta) {
		 return '<a href="' + jsglobals.base_url + 'company/company/' + row.company_id + '">' + row.company_name + '</a><div><a href="' + row.company_domain + '" target="_blank">' + row.company_domain + '</a></div><div>' + ellipsify(row.company_description, 30) + '</div>';
	     }
	    },
	    {"targets": 2, 
	     "name": "Domain",
	     "visible": false, 
	     "searchable": true,
	     "data": "company_domain",
	    },
	    {"targets": 3,
	     "name": "Contacts",
	     "visible": true, 
	     "searchable": false,
	     "data": "contacts",
	     "render": function(data, type, row, meta) {
		 var html = '';
		 $.each(data, function(i, contact) {
		     html += '<div class="company_contact">';
		     if (contact.contact_title != null && contact.contact_title.length > 0)
			 html += '<span class="contact_label">' + contact.contact_title + ': </span>';
		     html += '<a href="' + jsglobals.base_url + 'contact/contact/' + contact.contact_id + '">' + contact.contact_first_name + ' ' + contact.contact_last_name + '</a>';
		     html += '</div>';
		 });
		 return html;
	     }
	    },
	    {"targets": 4,
	     "name": "Phones",
	     "visible": true, 
	     "searchable": false,
	     "data": "phones",
	     "render": function(data, type, row, meta) {
		 var html = '';
		 $.each(data, function(i, phone) {
		     html += '<div class="company_phone">';
		     if (phone.phone_description != null && phone.phone_description.length > 0)
			 html += phone.phone_description + ': ';
		     html += phone.formatted;
		     html += '</div>';
		 });
		 return html;
	     }
	    },
	    {"targets": 5,
	     "name": "Addresses",
	     "visible": true, 
	     "searchable": false,
	     "data": "addresses",
	     "render": function(data, type, row, meta) {
		 var html = '';
		 $.each(data, function(i, address) {
		     html += '<div class="company_address">';
		     address_fmt = '';

		     // Attempt to format the address.
		     if (address.address_street_1 != null && address.address_street_1.length > 0) {
			 address_fmt += address.address_street_1;
		     }
		     if (address.address_street_2 != null && address.address_street_2.length > 0) {
			 if (address_fmt.length > 0)
			     address_fmt += '<br/>';
			 address_fmt += address.address_street_2;
		     }
		     if (address.address_city != null && address.address_city.length > 0) {
			 if (address_fmt.length > 0)
			     address_fmt += '<br/>';
			 address_fmt += address.address_city;
		     }
		     if (address.address_state_code != null && address.address_state_code.length > 0) {
			 if (address.address_city != null && address.address_city.length > 0)
			     address_fmt += ', ';
			 else
			     address_fmt += '<br/>';
			 address_fmt += address.address_state_code;
		     }
		     if (address.address_postal_code != null && address.address_postal_code.length > 0) {
			 if ((address.address_city != null && address.address_city.length > 0) ||
			     (address.address_state_code != null && address.address_state_code.length > 0))
			     address_fmt += ' ';
			 else
			     address_fmt += '<br/>';
			 address_fmt += address.address_postal_code;
		     }

		     // Description?
		     if (address_fmt.length > 0 && address.address_description != null && address.address_description.length > 0)
			 address_fmt = '<div class="address_description">' +address.address_description + '</div>' + address_fmt;

		     html += address_fmt + '</div>';
		 });
		 return html;
	     }
	    },
	    {"targets": 6,
	     "name": "Emails",
	     "visible": true, 
	     "searchable": false,
	     "data": "emails",
	     "render": function(data, type, row, meta) {
		 var html = '';
		 $.each(data, function(i, email) {
		     html += '<div class="company_email">';
		     if (email.email_description != null && email.email_description.length > 0)
			 html += email.email_description + ': ';
		     html += email.email_address;
		     html += '</div>';
		 });
		 return html;
	     }
	    },
	    {"targets": 7,
	     "name": "Description",
	     "visible": false, 
	     "searchable": true,
	     "data": "company_description",
	    },
	    {"targets": 8,
	     "name": "Delete",
	     "visible": true, 
	     "searchable": true,
	     "data": null,
	     "render": function(data, type, row, meta) {
		 return '<button class="btn btn-default btn-sm" data-toggle="modal" data-target="#delete-company-dialog" data-id=' + row.company_id + ' data-name="' + row.company_name + '"><span class="glyphicon glyphicon-trash"></span></button>';
	     }
	    },
	]
    });
}
