$(document).ready(function() {
    refreshPage();
});

// Get the page data and display it.
function refreshPage() {

    // Get the ID from the URL
    var path = window.location.pathname;
    var components = path.split('/');
    var id = components[components.length - 1];

    // Get the subscription status.
    $.ajax({
        url: jsglobals.base_url + "company/get_company",
        dataType: "json",
        type: 'post',
	data: {
	    id: id
	}
    }).done(function(data) {
        if (data.status == 'failure') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
	    window.location.href = data.redirect;
            return;
        }
        updateCompany(data.company);

    }).fail(function(jqXHR, status) {
        toastr.error("Error");
    });
}

// Update the Company
function updateCompany(company) {

    // Update the title.
    $('#company-name').html(company.company_name).addClass('editable')
	.attr({
	    'data-url': jsglobals.base_url + 'common/update_db_field',
	    'data-name': 'company.company_name',
	    'data-pk': company.company_id,
	    'data-type': 'text',
	    'title': 'New Name',
	});

    // Update the domain
    $('#company-domain').html(company.company_domain).addClass('editable')
	.attr({
	    'data-url': jsglobals.base_url + 'common/update_db_field',
	    'data-name': 'company.company_domain',
	    'data-pk': company.company_id,
	    'data-type': 'url',
	    'title': 'New Domain',
	});

    // Link the editible classes to the editable plugin.
    setEditable();
    //useTooltip();
    usePopover();
}

