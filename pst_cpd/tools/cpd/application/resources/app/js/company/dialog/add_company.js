$(document).ready(function() {

    $('#address').geocomplete({
	details: "form",
	types: ["geocode", "establishment"],
	detailsAttribute: "address-data"
    });

    // Validate the add company form and submit it if it is valid.
    $('#add-company-form').formValidation({
	framework: 'bootstrap',
	icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
	},
	fields: {
	    name: {
		validators: {
		    notEmpty: {
			message: "The company name is required."
		    }
		}
	    },
	}
    }).on('success.form.fv', function(e) {
	e.preventDefault();

	// Get the form
	var $form = $(e.target);
	var fv = $form.data('formValidation');

	// Get the form data and submit it.
	var message_data = $form.serialize();
	$.ajax({
	    url: jsglobals.base_url + 'company/add_company',
	    dataType: 'json',
	    type: 'post',
	    data: message_data
	}).done(function(data) {
	    if (data.status != 'success') {
		toastr.error(data.message);
		return;
	    }
            else if (data.status == 'redirect') {
		window.location.href = data.redirect;
		return;
            }
	    $('#add-company-dialog').modal('hide');
	    fv.resetForm();

	    refreshPage();
	}).fail(function(jqXHR, status) {
	    toastr.error("Server communication error. Please try again.");
        }).always(function() {
        });
    });

});
