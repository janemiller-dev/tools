$(document).ready(function () {
    let client_id, client_email, email_id, type, random_name, origin, pdf_id,
        path = window.location.pathname,
        components = path.split('/');

    client_id = components[components.length - 1];

    // When the dialog is displayed, set the current instance ID.
    $('#send-email-dialog').on('show.bs.modal', function (event) {
        tinymce.remove("#email-content");

        // Set Client Email.
        $('#send-email-id').val($('#email').val());

        tinymce.init({
            selector: '#email-content',
            branding: false,
            init_instance_callback: function (ed) {
                tinyMCE.DOM.setStyle(tinyMCE.DOM.get('email-content_ifr'), 'height', '55vh');
            }
        });

        // Embed text to text area of Text editor.
        const embed_content_selector = $('#embed-content');
        embed_content_selector.unbind('click');
        embed_content_selector.click(function () {
            const content = tinyMCE.activeEditor.getContent() +
                $('#canvas-container_ifr').contents().find('#tinymce').html();
            tinyMCE.activeEditor.setContent(content);
        });

        pdf_id = 0;
        $('#attachment_span').addClass('hidden');
    });

    $(document).on('click', '#close-send-email-dialog', function () {
        $('#send-email-dialog').modal('hide');
    });

    $(document).on('click', '#add-recipient', function () {
        $('.client-email-div')
            .after('<div class="col-xs-9 col-xs-offset-3" style="padding:0; margin-top:5px !important">' +
                '<input type="email" class="form-control client-email"></div>');
    });

    // Click handler for attach PDF button.
    $('#attach-pdf').click(function () {
        pdf_id = $('.active_pdf').attr('data-id');
        $('#attachment_name').text(' ' + $('.active_pdf').text());
        $('#attachment_span').removeClass('hidden');
    });


    // Sends the emailtional document.
    $(document).on('submit', '#send-email-form', function (ev) {
        let email_array = [];
        $('.client-email').each(function(){
            ('' !== $(this).val()) ? email_array.push($(this).val()) : '';
        });

        enableLoader();

        ev.preventDefault();
        ev.stopPropagation();

        // Get the text from editor.
        const text = tinymce.activeEditor.getContent();
        $.ajax({
            url: jsglobals.base_url + 'tsl/send_attachment',
            dataType: 'json',
            type: 'post',
            data: {
                client_id: client_id,
                client_email: email_array,
                id: email_id,
                tool_id: window.location.search.split('?id=')[1],
                type: type,
                origin: origin,
                random: random_name,
                subject: $('#client-subject').val(),
                text: text,
                pdf_id: pdf_id,
            },
            error: ajaxError
        }).done(function (data) {
            $.unblockUI();
            if (data.status != 'success') {
                toastr.error(data.message, 'Invalid Email ID')
                return;
            }
            else if (data.status == 'redirect') {

                window.location.href = data.redirect;
                return;
            }

            toastr.success('Email Sent!!');
            $('#send-email-dialog').modal('hide');
            $('#email-text').val('');
        }).fail(function (jqXHR, textStatus) {
            $.unblockUI();
            toastr.error('Please check EMail ID provided.', 'Unable to Send Email!!');
            return false;
        }).always(function () {
        });

        return false;
    })
});
