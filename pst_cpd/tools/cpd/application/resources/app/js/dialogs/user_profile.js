$(document).ready(function () {
    $('#user-profile-dialog').on('show.bs.modal', function (event) {
        user_profile.init();
    });
});

let product_added = 0,
    user_profile = function () {

        // Initialize and get the DMD ID.
        let init = () => {
            const fetch_user_data = makeAjaxCall('user/get_user_data', {});

            fetch_user_data.then(function (data) {
                $('#user-name').val(data.user_data[0].name);
                $('#user-email').val(data.user_data[0].email);
            });

            const get_all_products = makeAjaxCall('industry/get_all_products', {});
            get_all_products.then(function (data) {
                const get_user_product = makeAjaxCall('industry/get_my_products', {});

                get_user_product.then(function (user_product) {

                    let products_array = [];
                    $.each(user_product.products, function (index, product) {

                        // (index);
                        if (index === 'groups') {
                            $.each(product, function (index, group) {
                                products_array[group.usp_seq] = [];
                                products_array[group.usp_seq]['name'] = group.upg_name;
                                products_array[group.usp_seq]['id'] = group.upg_id;
                                products_array[group.usp_seq]['is_group'] = 'group';
                            })
                        } else if (index === 'products') {
                            $.each(product, function (index, product) {
                                products_array[product.usp_seq] = [];
                                products_array[product.usp_seq]['name'] = product.u_product_name;
                                products_array[product.usp_seq]['id'] = product.u_product_id;
                            })
                        }
                        // products_array[product.seq][] =

                    });

                    products_array = products_array.filter(function( product ) {
                        return product !== undefined;
                    });

                    const count = products_array.length,
                        diff = count - 3;
                    if ((diff > 0) && 0 === product_added) {
                        $('.additional-product').remove();

                        for (let i = 0; i < diff; i++) {
                            const array = ['Fourth', 'Fifth', 'Sixth', 'Seventh', 'Eighth', 'Ninth', 'Tenth', 'Eleventh',
                                'Twelfth', 'Thirteenth', 'Fourteenth', 'Fifteenth', 'Sixteenth', 'Seventeenth', 'Eighteenth',
                                'Nineteenth', 'Twentieth', 'Twenty First'],
                                additional_product_selector = $('.additional-product');

                            const demo_product = $('.demo_product').clone();
                            demo_product.removeClass('hidden demo_product');
                            demo_product.find('#demo-product-label').text('Select ' + array[additional_product_selector.length] + ' Product Type');
                            demo_product.addClass('additional-product');
                            demo_product.find('#demo-product-type').addClass('profile-product-type');

                            // Check if Additional Product Exist.
                            (0 === additional_product_selector.length) ? $('.product-type').last().after(demo_product) :
                                additional_product_selector.last().after(demo_product);

                        }
                    }

                    //
                    // $('#primary-product-type').val(user_product.products.data[0].usp_primary_product);
                    // $('#secondary-product-type').val(user_product.products.data[0].usp_secondary_product);
                    // $('#tertiary-product-type').val(user_product.products.data[0].usp_tertiary_product);

                    const product_type_selector = $('.profile-product-type');
                    product_type_selector.empty();

                    product_type_selector
                        .append('<option value="-1" disabled selected>-- Select Product Type --</option>');

                    product_type_selector
                        .append('<option value="-1">((-- Add Product type --))</option>')

                    $.each(data.products, function (index, product) {
                        product_type_selector
                            .append('<option style="font-weight: bold; font-style: italic;"' +
                                ' data-id="' + product[0]['u_product_group_id'] + '"' +
                                ' value="' + product[0]['u_product_group_id'] + '" data-type="group">' + index + '</option>');

                        $.each(product, function (index, individual_product) {
                            product_type_selector
                                .append('<option value="' + individual_product.u_product_id + '"' +
                                    ' data-type="product" data-id="' + individual_product.u_product_id + '">' +
                                    individual_product.u_product_name + '</option>');
                        });
                    });

                    product_type_selector
                        .append('<option value="0" style="font-weight:bold;">None of the Above</option>');

                    $.each(products_array, function (index, product_info) {
                        if (product_info.is_group === 'group') {
                            $('.profile-product-type').eq(index).find('option[data-type=\'group\']' +
                                '[data-id=' + product_info.id + ']').attr('selected', 'selected');
                        } else {
                            $('.profile-product-type').eq(index).find('option[data-type=\'product\']' +
                                '[data-id=' + product_info.id + ']').attr('selected', 'selected');
                        }
                    })
                })
            });

            // Save Profile functionality.
            $(document).off('click', '#save-profile');
            $(document).on('click', '#save-profile', function () {
                const selected_product_types = [],
                    profile_data = $('#user-profile-form').serializeArray();

                $.each($('.profile-product-type'), function (index, product_type) {
                    selected_product_types[index] = [];
                    selected_product_types[index]['id'] = $(product_type).val();
                    selected_product_types[index]['is_group'] = $(product_type).find(":selected").attr('data-type');
                });


                $.each(selected_product_types, function (index, product_type) {
                    profile_data.push({name: 'id' + index, value: product_type['id']});
                    profile_data.push({name: 'is_group' + index, value: product_type['is_group']});
                });


                profile_data.push({name: 'product_count', value: selected_product_types.length});
                // Push question ID to the form data.
                profile_data.push({name: 'name', value: $('#user-name').val()});
                profile_data.push({name: 'email', value: $('#user-email').val()});

                const update_user_data = makeAjaxCall('user/update_user_data', profile_data);

                update_user_data.then(function (data) {
                    toastr.success('User Profile Updated!!', 'Success!!');
                    refreshProducts(('undefined' === typeof(refreshPage)) ? '' : refreshPage);
                    $('#user-profile-dialog').modal('hide');
                });
            });

            $(document).off('click', '#add-product');
            $(document).on('click', '#add-product', function () {
                const array = ['Fourth', 'Fifth', 'Sixth', 'Seventh', 'Eighth', 'Ninth', 'Tenth', 'Eleventh',
                    'Twelfth', 'Thirteenth', 'Fourteenth', 'Fifteenth', 'Sixteenth', 'Seventeenth', 'Eighteenth',
                    'Nineteenth', 'Twentieth', 'Twenty First'];
                additional_product_selector = $('.additional-product');

                if (additional_product_selector.length < 18) {
                    const demo_product = $('.demo_product').clone();
                    demo_product.removeClass('hidden demo_product');
                    demo_product.find('#demo-product-label').text('Select ' + array[additional_product_selector.length] + ' Product Type');
                    demo_product.addClass('additional-product');
                    demo_product.find('#demo-product-type').addClass('profile-product-type');

                    // Check if Additional Product Exist.
                    (0 === additional_product_selector.length) ? $('.product-type').last().after(demo_product) :
                        additional_product_selector.last().after(demo_product);

                    product_added = 1;
                    init();
                } else {
                    toastr.warning('Please update the selected Product Type.', 'You can have upto 21 Product Types');
                }
            })
        };

        return {
            init: init
        };

    }();
