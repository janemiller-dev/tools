$(document).ready(function () {
    $('#view-alert-dialog').on('show.bs.modal', function (event) {

        let client_id = 0;

        if ('asset' === $(event.relatedTarget).attr('data-type')) {
            client_id = $(event.relatedTarget).attr('data-id');
        }

        view_alert.init(client_id);

        // Duration Change click handler.
        $(document).off('change', '#select-acm-duration');
        $(document).on('change', '#select-acm-duration', function () {
            view_alert.init(client_id);
        });

        // ACM row color picker.
        $(document).off('change', '.acm-row-color');
        $(document).on('change', '.acm-row-color', function () {
            $(this).parent().parent().css('background', $(this).val());
            makeAjaxCall('common/update_row_color', {
                id: $(this).attr('data-id'),
                val: $(this).val(),
                type: $(this).attr('data-type')
            });
        });

        // ACM rows color reset click handler.
        $(document).off('click', '#reset-acm-color');
        $(document).on('click', '#reset-acm-color', function () {
            const reset_acm_color = makeAjaxCall('common/reset_acm_color', {});

            reset_acm_color.then(function (data) {
                toastr.success('Color Reset Successful.', 'Success!!');
                $('#view-alert-dialog').modal('hide');
            })
        })
    });

    // Enable Editable.
    $('#view-alert-dialog').on('shown.bs.modal', function (event) {
        setEditable();
        $(document).off('focusin.modal');

        $('#delete-alert-dialog').on('show.bs.modal', function (event) {
            const id = $(event.relatedTarget).attr('data-id'),
                name = $(event.relatedTarget).attr('data-name'),
                type = $(event.relatedTarget).attr('data-type'),
                seq = $(event.relatedTarget).attr('data-seq');

            $('#view-alert-dialog').modal('hide');
            $('#alert-name').text(name);

            // Delete Alert click handler.
            $('#delete-alert-button').off('click')
            $('#delete-alert-button').on('click', function () {

                if ('alert' === type) {
                    const delete_alert = makeAjaxCall('common/delete_alert', {
                        'id': id,
                        'name': name
                    });

                    delete_alert.then(function (data) {
                        toastr.success('Alert Deleted!!');
                        $('#delete-alert-dialog').modal('hide');
                    });
                } else if ('action' === type) {
                    const delete_action = makeAjaxCall('tsl/delete_client_action', {
                        id: id,
                        seq: seq
                    });

                    delete_action.then(function (data) {
                        toastr.success('Action Deleted.', 'Success!!');
                        $('#delete-alert-dialog').modal('hide');
                    });
                }
            })
        })
    });
});

let view_alert = function () {

    // Initialize and get the DMD ID.
    let init = (client_id) => {
        const get_cb_data = makeAjaxCall('common/get_alert_data', {
                id: client_id,
                duration: moment().subtract($('#select-acm-duration').val(), 'months').format('YYYY-MM-DD hh:mm:ss')
            }),
            time_array = [
                'Pending',
                '<span style="color: #0000ffad">Due Today</span>',
                '<span style="color: red">Past Due</span>'
            ];

        get_cb_data.then(function (data) {
            $('#alert-table tbody').empty();

            // Set the Next Action.
            $.each(data.data.action, function (index, action) {
                let scheduled = new Date(action.ta_when),
                    now = new Date(moment().format("YYYY-MM-DD HH:mm:ss")),
                    i = 0,
                    color = '#90ee9036',
                    tr_style = 'background: #90ee9036; border: solid 2px #93820038';

                // Set the color for rows.
                if (now.getDate() === scheduled.getDate() &&
                    now.getMonth() === scheduled.getMonth() &&
                    now.getFullYear() === scheduled.getFullYear()) {
                    i = 1;
                    tr_style = 'background: #ffffe08f; border: solid 2px #93820038';
                    color = '#ffffe08f';
                } else if (scheduled < now) {
                    i = 2;
                    tr_style = 'background: #8ff1fd17; border: solid 2px #93820038';
                    color = '#8ff1fd17';
                }

                color = color.slice(0, 7);

                if (action.ta_row_color !== null) {
                    tr_style = tr_style.replace(/background:[\s\S]*?;/g, 'background: ' + action.ta_row_color + ';');
                    color = action.ta_row_color;
                }

                // Append Next action data to alert table.
                $('#alert-table tbody')
                    .append('<tr style="' + tr_style + '">' +
                        '<td></td>' +
                        '<td><span class="date-picker" data-type="text" ' +
                        'data-url="' + jsglobals.base_url + 'common/update_alert_date" ' +
                        'data-name="action" data-pk="' + action.ta_id + '">' + action.ta_when + '</span></td>' +
                        '<td>' + action.hc_name + '</td>' +
                        '<td>TSL</td>' +
                        '<td>' +
                        '<div class="dropdown">\n' +

                        // Objectives Drop down.
                        '<button class="btn btn-default dropdown-toggle acm-objective" type="button"' +
                        ' data-toggle="dropdown" id="action-objective-' + action.ta_id + '">' +
                        'Select Objectives' + ' <span class="caret"></span></button>' +
                        '<ul class="dropdown-menu acm-objective">' +
                        $.map(data.data.objective, function (objective, index) {
                            return ('<li class="dropdown-submenu">' +
                                '<a href="#" class="objective-submenu"><b>' + index +
                                '</b><span class="caret"></span></a>' +
                                '<ul style="list-style-type: none;">' +

                                // Looop through Objective Array.
                                $.map(objective, function (individual_objective) {
                                    let active = '';
                                    (individual_objective.uao_id === action.ta_reason) ? active = 'active' : '';

                                    return ('<li class="' + active + '">' +
                                        '<a href="#" data-val="' + individual_objective.uao_id + '"' +
                                        ' class="acm-objective-item" data-id="' + action.ta_id + '" data-type="action">'
                                        + individual_objective.uao_name + '</a></li>');
                                }).join('') +
                                '</ul>' +
                                '</li>');
                        }).join('') +
                        '<li><a data-val="-1" class="acm-objective-item">  -- Add Objective --</a></li>' +
                        '</ul>' +
                        '</div>' +
                        '</td>' +
                        '<td>' + time_array[i] + '</td>' +
                        '<td><input type="checkbox" class="mark-alert-done"' +
                        ' data-type="action" data-id="action.ta_id" /></td>' +
                        '<td><span class="fa fa-eye" data-toggle="modal" data-target="#view-action-dialog" ' +
                        'data-id="' + action.ta_client_id + '"></span></td>' +
                        '<td><input class="acm-row-color" type="color" value="' + color + '"' +
                        ' data-id="' + action.ta_id + '" data-type="action"/></td>' +
                        '<td><span class="fa fa-trash" data-target="#delete-alert-dialog" data-toggle="modal" ' +
                        'data-id="' + action.ta_client_id + '" data-name="' + action.hc_name + '" data-type="action" ' +
                        'data-seq="' + action.ta_seq + '" ></span></td>' +
                        '</tr>'
                    );

                // Set the button text
                ($('#action-objective-' + action.ta_id).next('ul').find('li.active').length !== 0) ?
                    $("#action-objective-" + action.ta_id)
                        .html($('#action-objective-' + action.ta_id).next('ul')
                                .find('li.active').parent().siblings('a').text() + ' – ' +
                            $('#action-objective-' + action.ta_id).next('ul')
                                .find('li.active').text() + ' <span class="caret"></span>') : '';
            });

            // Add an empty separator to ACM list.
            $('#alert-table').append('<tr><td colspan="11">' +
                '<span data-type="text" style="visibility: hidden">Empty Line</span></td></tr>');

            $('#alert-table').find('tr').eq(1).find('td').eq(0).html('<b>Next Action</b>');

            // Set the Call backs.
            $.each(data.data.cb, function (index, cb) {
                let scheduled = new Date(cb.tn_datetime),
                    now = new Date(moment().format("YYYY-MM-DD HH:mm:ss")),
                    i = 0,
                    color = '#90ee9036',
                    tr_style = 'background: #90ee9036; border: solid 2px #93820038';

                // Set the color for rows.
                if (now.getDate() === scheduled.getDate() &&
                    now.getMonth() === scheduled.getMonth() &&
                    now.getFullYear() === scheduled.getFullYear()) {
                    i = 1;
                    tr_style = 'background: #ffffe08f; border: solid 2px #93820038';
                    color = '#ffffe08f';
                } else if (scheduled < now) {
                    i = 2;
                    tr_style = 'background: #8ff1fd17; border: solid 2px #93820038';
                    color = '#8ff1fd17';
                }
                color = color.slice(0, 7);

                // Check if the color is set.
                if (cb.tn_row_color !== null) {
                    tr_style = tr_style.replace(/background:[\s\S]*?;/g, 'background: ' + cb.tac_cb_row_color + ';');
                    color = cb.tn_row_color;
                }

                // Append call backs to ACM table.
                $('#alert-table tbody')
                    .append('<tr style="' + tr_style + '">' +
                        '<td></td>' +
                        '<td><span class="date-picker" data-type="text"' +
                        ' data-url="' + jsglobals.base_url + 'common/update_alert_date" data-name="cb"' +
                        ' data-pk="' + cb.tn_id + '">' + cb.tn_datetime + '</td>' +
                        '<td>' + cb.hc_name + '</td>' +
                        '<td>TSL</td>' +
                        '<td>' +
                        '<div class="dropdown">\n' +
                        '<button class="btn btn-default dropdown-toggle acm-objective" type="button"' +
                        ' data-toggle="dropdown" id="cb-objective-' + cb.tn_id + '">' +
                        'Select Objectives' +
                        '<span class="caret"></span></button>' +
                        '<ul class="dropdown-menu acm-objective">' +
                        $.map(data.data.objective, function (objective, index) {
                            return ('<li class="dropdown-submenu">' +
                                '<a href="#" class="objective-submenu"><b>' + index + '</b><span class="caret"></span></a>' +
                                '<ul style="list-style-type: none;">' +
                                $.map(objective, function (individual_objective) {
                                    let active = '';
                                    (individual_objective.uao_id === cb.tac_cb_reason) ? active = 'active' : '';

                                    console.log(active);
                                    console.log(individual_objective.uao_name);
                                    console.log(cb.hc_name)
                                    return ('<li class="' + active + '"><a href="#" data-val="' + individual_objective.uao_id + '"' +
                                        ' class="acm-objective-item" data-type="cb" data-id="' + cb.tac_id + '">'
                                        + individual_objective.uao_name + '</a></li>');
                                }).join('') +
                                '</ul>' +
                                '</li>');
                        }).join('') +
                        '<li>' +
                        '<a data-val="-1" class="acm-objective-item">  -- Add Objective --</a></li>' +
                        '</ul></div>' +
                        '</td>' +
                        '<td>' + time_array[i] + '</td>' +
                        '<td><input type="checkbox" class="mark-alert-done"' +
                        ' data-type="cb" data-id="' + cb.tac_id + '" /></td>' +
                        '<td><span class="fa fa-eye"></span></td>' +
                        '<td><input class="acm-row-color" type="color" value="' + color + '"' +
                        ' data-type="cb" data-id="' + cb.tac_id + '"/></td>' +
                        '<td><span class="fa fa-trash" data-target="#delete-alert-dialog" data-toggle="modal" ' +
                        'data-id="' + cb.tac_id + '" data-name="' + cb.hc_name + '" data-type="cb"></span></td>' +
                        '</tr>'
                    );

                // Set the button text
                ($('#cb-objective-' + cb.tn_id).next('ul').find('li.active').length !== 0) ?
                    $("#cb-objective-" + cb.tn_id)
                        .html($('#cb-objective-' + cb.tn_id).next('ul')
                                .find('li.active').parent().siblings('a').text() + ' – ' +
                            $('#cb-objective-' + cb.tn_id).next('ul')
                                .find('li.active').text() + ' <span class="caret"></span>') : '';
            });

            // Append callback string.
            $('#alert-table').find('tr').eq(data.data.action.length + 2)
                .find('td').eq(0).html('<b>Call Back</b>');

            let component_array = {};
            // Append tool alerts to ACM table.
            $.each(data.data.alerts, function (index, alert) {

                if ((component_array[alert.ua_component_type] === undefined)) {
                    component_array[alert.ua_component_type] = [];
                }

                component_array[alert.ua_component_type].push(alert);
            });

            // Loop through Component Array.
            $.each(component_array, function (key, individual_component_array) {

                // Append an empty line.
                $('#alert-table').append('<tr><td colspan="11"><span class="date-ref"' +
                    ' style="visibility: hidden">Empty Line</span></td></tr>');

                // Loop through Every Component Array.
                $.each(individual_component_array, function (index, alert) {

                    let scheduled = new Date(alert.ua_datetime),
                        now = new Date(moment().format("YYYY-MM-DD HH:mm:ss")),
                        i = 0,
                        color = '#90ee9036',
                        tr_style = 'background: #90ee9036; border: solid 2px #93820038';
                    // Set the color for rows.
                    if (now.getDate() === scheduled.getDate() &&
                        now.getMonth() === scheduled.getMonth() &&
                        now.getFullYear() === scheduled.getFullYear()) {
                        i = 1;
                        tr_style = 'background: #ffffe08f; border: solid 2px #93820038';
                        color = '#ffffe08f';
                    } else if (scheduled < now) {
                        i = 2;
                        tr_style = 'background: #8ff1fd17; border: solid 2px #93820038';
                        color = '#8ff1fd17';
                    }
                    color = color.slice(0, 7);

                    // Check if the color is set.
                    if (alert.ua_row_color !== null) {
                        tr_style = tr_style.replace(/background:[\s\S]*?;/g, 'background: ' + alert.ua_row_color + ';');
                        color = alert.ua_row_color;
                    }

                    let completion_text = 'Incomplete';
                    (alert.ua_completion_date !== null) ?
                        completion_text = alert.ua_completion_date : '';

                    // Append tools alerts to table.
                    $('#alert-table tbody')
                        .append('<tr style="' + tr_style + '">' +
                            '<td></td>' +
                            '<td><span class="date-picker" data-type="text" ' +
                            'data-url="' + jsglobals.base_url + 'common/update_alert_date" data-name="alert" ' +
                            'data-pk="' + alert.ua_id + '">' + alert.ua_datetime + '</span></td>' +
                            '<td>' + alert.hc_name + '</td>' +
                            '<td>' + alert.ua_set.toUpperCase() + '</td>' +
                            '<td>' +
                            '<div class="dropdown">\n' +
                            '<button class="btn btn-default dropdown-toggle acm-objective"' +
                            ' type="button" data-toggle="dropdown" id="alert-objective-' + alert.ua_id + '">' +
                            'Select Objectives' +
                            '<span class="caret"></span></button>' +
                            '<ul class="dropdown-menu acm-objective">' +
                            $.map(data.data.objective, function (objective, index) {
                                return ('<li class="dropdown-submenu">' +
                                    '<a href="#" class="objective-submenu"><b>' + index + '</b><span class="caret"></span></a>' +
                                    '<ul style="list-style-type: none;">' +
                                    $.map(objective, function (individual_objective) {
                                        let active = '';
                                        (individual_objective.uao_id === alert.ua_reason) ? active = 'active' : '';

                                        return ('<li class="' + active + '">' +
                                            ' <a href="#" data-val="' + individual_objective.uao_id + '"' +
                                            ' class="acm-objective-item" data-id="' + alert.ua_id + '"' +
                                            ' data-type="alert">'
                                            + individual_objective.uao_name + '</a></li>');
                                    }).join('') +
                                    '</ul>' +
                                    '</li>');
                            }).join('') +
                            '<li><a data-val="-1" class="acm-objective-item">  -- Add Objective --</a></li>' +
                            '</ul></div>' +
                            '</td>' +
                            '<td>' + time_array[i] + '</td>' +
                            '<td><input type="checkbox" class="mark-alert-done"' +
                            ' data-type="alert" data-id="' + alert.ua_id + '" /></td>' +
                            '<td><span class="fa fa-eye" ' +
                            'onclick="location.href=\'' + jsglobals.base_url + 'tsl/' +
                            (alert.ua_component_type === 'Promo' ? 'page' : 'vmail') +
                            '/' + alert.ua_asset_id + '?id=' + alert.deal_cpd_id +
                            '?ref=' + alert.ua_component_id + '\'"></span></td>' +
                            '<td><input class="acm-row-color" type="color" value="' + color + '"' +
                            ' data-type="alert" data-id="' + alert.ua_id + '"/>' +
                            '</td>' +
                            '<td><span class="fa fa-trash" data-target="#delete-alert-dialog" data-toggle="modal" ' +
                            'data-id="' + alert.ua_id + '" data-name="' + alert.hc_name + '" data-type="alert">' +
                            '</span></td>' +
                            '</tr>'
                        );

                    // Set the button text
                    ($('#alert-objective-' + alert.ua_id).next('ul').find('li.active').length !== 0) ?
                        $('#alert-objective-' + alert.ua_id)
                            .html($('#alert-objective-' + alert.ua_id).next('ul')
                                    .find('li.active').parent().siblings('a').text() + ' – ' +
                                $('#alert-objective-' + alert.ua_id).next('ul')
                                    .find('li.active').text() + ' <span class="caret"></span>') : '';
                });

                // Append Component Name.
                $('#alert-table').find('tr').eq('-' + individual_component_array.length)
                    .find('td').eq(0).html('<b>' + key + '</b>');
            });

            // Close every submenu by default.
            $('.objective-submenu').next('ul').css('display', 'none')

            // Objective Menu Click handler.
            $('.dropdown-submenu a.objective-submenu').on("click", function (e) {
                $('ul.acm-objective ul').css('display', 'none');
                $(this).next('ul').toggle();
                e.stopPropagation();
                e.preventDefault();
            });

        }).then(function () {
            $('.date-picker').editable({
                inputclass: 'some_class',
            });

            // Datetime picker for date field.
            $(document).on('click', ".date-picker", function () {
                $('.some_class').datetimepicker({
                    format: 'YYYY-MM-DD HH:mm:ss'
                })
            });

            $('#add-alert-objective-dialog').on('show.bs.modal', function (event) {
                $('#view-alert-dialog').modal('hide');

                $(document).on('click', '#save-objective', function () {
                    const add_new_objective = makeAjaxCall('common/add_new_objective', {
                        val: $('#objective-title').val()
                    });

                    add_new_objective.then(function () {
                        toastr.success('Alert Objective Added.', 'Success!!');
                        $('#add-alert-objective-dialog').modal('hide');
                    })
                });
            });
            $('.objective-submenu').next('ul').find('a')
            // ACM objective update.
            $(document).off('click', '.acm-objective-item')
            $(document).on('click', '.acm-objective-item', function () {
                if ('-1' === $(this).attr('data-val')) {
                    $('#add-alert-objective-dialog').modal('show');
                } else {
                    const update_objective_data = makeAjaxCall('common/update_alert_objective', {
                        id: $(this).attr('data-id'),
                        val: $(this).attr('data-val'),
                        type: $(this).attr('data-type')
                    });

                    update_objective_data.then(function (data) {
                        init(client_id);
                    })
                }
            })
        });
    };
    return {
        init: init
    };
}();
