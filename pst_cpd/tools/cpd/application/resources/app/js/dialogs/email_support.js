$(document).ready(function () {

    // Update the list of tools
    $.ajax({
        url: jsglobals.base_url + "tool/get_user_tools",
        dataType: "json",
        type: 'post',
    }).done(function (data) {
        if (data.status == 'failure') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
            window.location.href = data.redirect;
            return;
        }
        // Place each tool on the drop down list.
        $('#support-topic').empty();
        $('#support-topic').append($('<option value="general">General Question</option>'));

        $.each(data.tools, function (i, tool) {
            $('#support-topic').append($('<option value="' + tool.tool_controller + '">' + tool.tool_name + '</option>'));
        });

    }).fail(function (jqXHR, status) {
        toastr.error("Error");
    });

    // On the display of the form, clear all of the contents.
    $('#email-support-dialog').on('show.bs.modal', function () {
        $('#topic-id').val('0');
        $('#support-question').val('');
    });

    // Validate the add instance form and submit it if it is valid.
    $('#email-support-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            subject: {
                validators: {
                    notEmpty: {
                        message: "The subject is required."
                    }
                }
            },
            question: {
                validators: {
                    notEmpty: {
                        message: "The question is required."
                    }
                }
            },
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);
        var fv = $form.data('formValidation');

        // Get the form data and submit it.
        var message_data = $form.serialize();
        $.ajax({
            url: jsglobals.base_url + 'email/send_support_message',
            dataType: 'json',
            type: 'post',
            data: message_data
        }).done(function (data) {
            fv.resetForm();
            if (data.status != 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            $('#email-support-dialog').modal('hide');
            refreshPage();
        }).fail(function (jqXHR, status) {
            toastr.error("Server communication error. Please try again.");
        }).always(function () {
        });
    });
});
