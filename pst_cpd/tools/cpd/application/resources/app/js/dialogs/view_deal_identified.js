$(document).ready(function () {

    $('#view-deal-identified-dialog').on('shown.bs.modal', function (event) {
        setEditable();
        $(document).off('focusin.modal');
    });

    $('#view-deal-identified-dialog').on('show.bs.modal', function (event) {
        const get_deals_identified = makeAjaxCall('common/get_deals_identified', {});

        get_deals_identified.then(function (data) {
            let content = '<table class="table"><tbody><tr style="border: solid 1px #eee">';
            $('#deals-identified-table').empty();

            $.each(data.identified_deals, function (index, deal) {
                let img_src = 'https://cpd.powersellingtools.com/wp-content/uploads/2020/04/Default-Profile-Pic.png';
                (null !== deal.hca_avatar) ?
                    img_src = window.location.origin +
                        '/tools/homebase/get_owner_image/?img='
                        + deal.hca_avatar : '';

                // Check if this is the third element in array.
                // If so add new row.
                if ((index !== 0) && (index % 3 === 0)) {
                    content += '</tr><tr style="border: solid 1px #eee">';
                }

                const built_year = (null !== deal.aai_built_year) ? deal.aai_built_year : "Enter Built Year:",
                    list_price = (null !== deal.aai_list_price) ? deal.aai_list_price : 'Enter LIST PRICE: ',
                    price_per_sf = (null !== deal.aai_price_per_sf) ? deal.aai_price_per_sf : 'Enter Price / SF: ',
                    price_per_unit = (null !== deal.aai_price_per_unit) ? deal.aai_price_per_unit : 'Enter Price / Units:',
                    units = (null !== deal.aai_units) ? deal.aai_units : 'Enter Units: ',
                    avg_sf = (null !== deal.aai_avg_sf) ? deal.aai_avg_sf : 'Enter Avg SF: ';

                content += '<td>' +
                    '<img src="' + img_src + '"' +
                    ' id="agent-asset-avatar" class="agent-asset-image" style="width: 11vw"></td>' +
                    '<td style="text-align: left">' +
                    '<table style="border-collapse: separate; border-spacing: 0.9em 0.3em; font-size: 15px; width: 100%">' +
                    '<tr>' +
                    '<td style="text-align: left">BUILT:</td>' +
                    '<td style="text-align: right"><span class="editable" id="aai_built_year' + deal.hca_id + '"' +
                    ' data-pk="' + deal.hca_id + '" data-name="aai_built_year" data-placement="top"' +
                    ' data-url="/tools/common/update_agent_asset_info" data-type="text">' +
                    built_year + '</span></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="text-align: left">UNITS:</td>' +
                    '<td style="text-align: right"><span class="editable" id="aai_units' + deal.hca_id + '"' +
                    ' data-pk="' + deal.hca_id + '" data-name="aai_units" data-placement="top"' +
                    ' data-url="/tools/common/update_agent_asset_info" data-type="text">' + units +
                    '</span></td> ' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="white-space: nowrap; text-align: left">PRICE / UNIT:</td>' +
                    '<td style="text-align: right"><span class="editable" id="aai_price_per_unit' + deal.hca_id + '"' +
                    ' data-pk="' + deal.hca_id + '" data-name="aai_price_per_unit" data-placement="top"' +
                    ' data-url="/tools/common/update_agent_asset_info" data-type="text">' + price_per_unit +
                    '</span></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="white-space: nowrap; text-align: left">PRICE / SF:</td>' +
                    '<td style="text-align: right"><span class="editable" id="aai_price_per_sf' + deal.hca_id + '"' +
                    ' data-pk="' + deal.hca_id + '" data-name="aai_price_per_sf" data-placement="top"' +
                    ' data-url="/tools/common/update_agent_asset_info" data-type="text">'
                    + price_per_sf + '</span></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="text-align: left">AVG SF:</td>' +
                    '<td style="text-align: right"><span class="editable" id="aai_avg_sg' + deal.hca_id + '"' +
                    ' data-pk="' + deal.hca_id + '" data-name="aai_avg_sf" data-placement="top"' +
                    ' data-url="/tools/common/update_agent_asset_info" data-type="text">' + avg_sf +
                    '</span></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="text-align: left">LIST PRICE:</td>' +
                    '<td style="text-align: right"><span class="editable" id="aai_list_price' + deal.hca_id + '"' +
                    ' data-pk="' + deal.hca_id + '" data-name="aai_list_price" data-placement="top"' +
                    ' data-url="/tools/common/update_agent_asset_info" data-type="text">' + list_price + '</span></td>' +
                    '</tr>' +
                    '</table>' +
                    '</td>'
            });

            content += '</tr></table>';
            $('#deals-identified-table').append(content);
        })

    });
});