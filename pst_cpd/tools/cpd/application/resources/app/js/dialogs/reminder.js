let recurrence_interval_var;

$(document).ready(function () {

    $('#view-reminder-dialog').on('show.bs.modal', function (event) {

        $('#reminder-date-picker').val(moment().format("YYYY-MM-DD"));

        $('#reminder-date-picker').datetimepicker({
            timepicker: false,
            format: 'YYYY-MM-DD'
        });

        // Date Picker change event handler.
        $('#reminder-date-picker').off('change');
        $('#reminder-date-picker').on('change', function () {
            fetch_alerts()
        });

        // Previous day click handler.
        $('#reminder-prev-day').off('click');
        $('#reminder-prev-day').click(function () {
            $('#reminder-date-picker')
                .val(moment($('#reminder-date-picker').val())
                    .subtract(1, "days").format("YYYY-MM-DD")).trigger('change');
        });

        // Next day click handler.
        $('#reminder-next-day').off('click');
        $('#reminder-next-day').click(function () {
            $('#reminder-date-picker')
                .val(moment($('#reminder-date-picker').val())
                    .add(1, "days").format("YYYY-MM-DD")).trigger('change');
        });
        fetch_alerts();

    });

    function fetch_alerts() {
        // Fetch alerts data from database.
        const fetch_alerts = makeAjaxCall('common/fetch_alerts', {
            date: $('#reminder-date-picker').val()
        });
        fetch_alerts.then(function (data) {
            $('#reminder-table tbody').empty();

            let objective_array = [],
                disabled_action = [],
                disabled_cb = [];

            // objective_array = data.alerts_fetched.objective;
            // Prepare Objective Array.
            $.each(data.alerts_fetched.objective, function (index, objective) {
                $.each(objective, function (key, individual_objective) {
                    objective_array[individual_objective.uao_id] = index + ' - ' + individual_objective.uao_name;
                });
            });

            $.each(data.disabled_reminder, function (index, reminder) {
                'action' === reminder.udr_component_type ?
                    disabled_action.push(reminder.udr_component_id) : disabled_cb.push(reminder.udr_component_id);
            });

            // Append Next Action.
            $.each(data.alerts_fetched.action, function (index, action) {
                let due_by_date = moment(action.ta_when),
                    today_date = moment(new Date()),
                    diff = calculate_diff(today_date, due_by_date);

                // Check if this action belongs to disabled group.
                if (-1 === disabled_action.indexOf(action.ta_id)) {
                    // Append data to reminder table.
                    if ('Due Today' === diff) {
                        $('#reminder-table tbody').prepend('<tr>' +
                            '<td>' + action.hc_name + '</td>' +
                            '<td>Next Action</td>' +
                            '<td>' + objective_array[action.ta_reason] + '</td>' +
                            // '<td>' + diff + '</td>' +
                            '<td>' + action.ta_when.split(' ').join(' / ') + '</td>' +
                            '<td><select class="form-control">' +
                            '<option>Complete</option>' +
                            '<option>Abandon</option>' +
                            '<option>Transfer</option>' +
                            '</select></td>' +
                            '<td><button class="btn btn-sm dismiss-reminder" data-id="' + action.ta_id + '"' +
                            ' data-type="action">' +
                            '<i class="fa fa-trash"></i></button></td>' +
                            '</tr>');
                    } else {
                        $('#reminder-table tbody').append('<tr>' +
                            '<td>' + action.hc_name + '</td>' +
                            '<td>Next Action</td>' +
                            '<td>' + objective_array[action.ta_reason] + '</td>' +
                            // '<td>' + diff + '</td>' +
                            '<td>' + action.ta_when.split(' ').join(' / ') + '</td>' +
                            '<td><select class="form-control">' +
                            '<option>Complete</option>' +
                            '<option>Abandon</option>' +
                            '<option>Transfer</option>' +
                            '</select></td>' +
                            '<td><button class="btn btn-sm dismiss-reminder" data-id="' + action.ta_id + '"' +
                            ' data-type="action">' +
                            '<i class="fa fa-trash"></i></button></td>' +
                            '</tr>');
                    }

                }
            });

            // Append Call Back.
            $.each(data.alerts_fetched.cb, function (index, cb) {
                let due_by_date = moment(cb.tac_cb_when),
                    today_date = moment(new Date()),
                    diff = calculate_diff(today_date, due_by_date);

                // Check if the call back belongs to disabled group.
                if (-1 === disabled_cb.indexOf(cb.tac_id)) {
                    if ('Due Today' === diff) {
                        $('#reminder-table tbody').prepend('<tr>' +
                            '<td>' + cb.hc_name + '</td>' +
                            '<td>Call Back</td>' +
                            '<td>' + objective_array[cb.tac_cb_reason] + '</td>' +
                            '<td>' + cb.tac_cb_when.split(' ').join(' / ') + '</td>' +
                            '<td><select class="form-control">' +
                            '<option>Complete</option>' +
                            '<option>Abandon</option>' +
                            '<option>Transfer</option>' +
                            '</select></td>' +
                            '<td><button class="dismiss-reminder btn btn-sm" data-type="cb" data-id="' + cb.tac_id + '">' +
                            '<i class="fa fa-trash"></i></button></td>' +
                            '</tr>');
                    } else {
                        $('#reminder-table tbody').append('<tr>' +
                            '<td>' + cb.hc_name + '</td>' +
                            '<td>Call Back</td>' +
                            '<td>' + objective_array[cb.tac_cb_reason] + '</td>' +
                            '<td>' + cb.tac_cb_when.split(' ').join(' / ') + '</td>' +
                            '<td><select class="form-control">' +
                            '<option>Complete</option>' +
                            '<option>Abandon</option>' +
                            '<option>Transfer</option>' +
                            '</select></td>' +
                            '<td><button class="dismiss-reminder btn btn-sm" data-type="cb" data-id="' + cb.tac_id + '">' +
                            '<i class="fa fa-trash"></i></button></td>' +
                            '</tr>');
                    }
                }
            });

            // Table row selector.
            // $('#reminder-table tbody tr').on('click', function () {
            //     $(this).toggleClass('selected-row')
            // });

            // Calculates difference between two dates.
            function calculate_diff(today_date, due_by_date) {
                // Calculate the difference in days.
                let diff = today_date.diff(due_by_date, 'days') > 0 ?
                    today_date.diff(due_by_date, 'days') + ' Days' : 'To be Done';

                // Calculate the difference in Weeks.
                diff = today_date.diff(due_by_date, 'weeks') > 0 ?
                    'Over a week' : diff;

                // Due Today.
                diff = today_date.diff(due_by_date, 'days') === 0 ?
                    'Due Today' : diff;

                // Calculate the difference in Months.
                diff = today_date.diff(due_by_date, 'months') > 0 ?
                    'Over a month' : diff;

                // Calculate the difference in Years.
                diff = today_date.diff(due_by_date, 'years') > 0 ?
                    today_date.diff(due_by_date, 'years') + ' Years' : diff;

                return diff;
            }

            // Set the time out for recurrence of the reminder.
            clearInterval(recurrence_interval_var);
            recurrence_interval_var = setInterval(function () {
                $('#view-reminder-dialog').modal('show');
            }, data.recurrence[0].urr_interval * 60 * 1000);
        });
    };

    // Dismiss Button click handler.
    $(document).on('click', '.dismiss-reminder', function () {
        const type = $(this).attr('data-type'),
            id = $(this).attr('data-id');

        // Update the reminder state to disabled.
        const update_reminder = makeAjaxCall('common/update_reminder', {
            type: type,
            id: id
        });

        update_reminder.then(function (data) {
            $('#view-reminder-dialog').modal('hide');
            toastr.success('Reminder Deleted.', 'Success!!');
        })
    });

    // Set the recurrence for reminder.
    $(document).on('click', '#set-recurrence', function () {

        const set_recurrence = makeAjaxCall('common/set_reminder_recurrence', {
            'interval': $('#reminder-snooze').val()
        });

        set_recurrence.then(function () {
            toastr.success('Reminder recurrence Set.', 'Success!!');
        });
        console.log('here');
    });
});