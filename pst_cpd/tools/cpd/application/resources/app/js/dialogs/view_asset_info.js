$(document).ready(function () {

    // Features array.
    let features_array = [];

    // Asset Information Array.
    features_array['ai'] = [];
    features_array['ai'][1] = ['Property Type (Describe Feature)', 'Year Built (Describe Feature)',
        'Year Renovated (Describe Feature)', 'Unit Mix (Describe Feature)',
        'Utility Provider (Describe Feature)', 'Electric Meter Type(Describe Feature)'],
        features_array['ai'][2] = ['Address', 'City', 'Zip', 'Submarket', 'Township/Range',
            'Section', 'Tax Parcel Number'],
        features_array['ai'][3] = ['Land Area', 'Improved SF', 'Height', 'Parking Spaces', 'Density'],
        features_array['ai'][4] = ['Profile', 'Prestige/Desirability', 'Quality', 'Condition', 'Demographics'],
        features_array['ai'][5] = ['Physical', 'Financial, Legal', 'When Purchased', 'Loan Due Date',
            'Major Tenants', 'Lease Expirations'];

    // Market Information Array.
    features_array['pi'] = [],
        features_array['pi'][1] = ['Desired Asset Type', 'Geographic Preference', 'Assets Not Considered',
            'Charity', 'Immediate & Future Needs'],
        features_array['pi'][2] = ['Name', 'Role', 'Problems', 'Needs', 'Challenges'],
        features_array['pi'][3] = ['Name', 'Partnership Interest', 'Reputation', 'Advisory Role', 'Other Dealings With'],
        features_array['pi'][4] = ['Specialization', 'Effectiveness', 'Trust', 'Challenges', 'Opportunities'],
        features_array['pi'][5] = ['Agent', 'Years of Service', 'Status', 'Transaction History', 'Future Retention'];

    // Financial Information Array.
    features_array['fi'] = [],
        features_array['fi'][1] = ['Equity', 'Gain', 'Exposure', 'Strategy', 'Plan'],
        features_array['fi'][2] = ['Lender', 'Amount', 'Terms', 'Date Due', 'Exit Strategy'],
        features_array['fi'][3] = ['Cash Flow', 'Value Goal', 'Objective', 'Risks', 'Opportunities'],
        features_array['fi'][4] = ['Income', 'Lease Renewal Rates', 'Operating Expenses', 'Changes', 'Taxes'],
        features_array['fi'][5] = ['Deployable Capital', 'Financial Condition of Tenants',
            'Lease Expiration Dates', 'Future Asset Capital Requirements', 'Immediate Investment Needs',
            'State and Local Law Changes'];

    // Legal Information Array.
    features_array['li'] = [],
        features_array['li'][1] = ['City Violations', 'Neighborhood Stabilization Program target (NSP)',
            'Opportunity Zone', 'Qualified Census Tract (QCT)', 'Title Issues/Exemptions'],
        features_array['li'][2] = ['Status/details', 'Pending', 'Ground Lease', 'Lease Type', 'Next Step'],
        features_array['li'][3] = ['Partnership Structure', 'Responsibility', 'Conflicts', 'Authority', 'Resolution'],
        features_array['li'][4] = ['Transaction Privilege Tax Permit (Past)', 'Permit to operate',
            'Pool Permit', 'Fire Compliance', 'Citations', 'Insurance Claims',
            'Americans With Disabilities Act (ADA)', 'Environmental (Lead Paint)', 'Radon', 'Fire Alarms', 'CO2',
            'Safety', 'Crime free'],
        features_array['li'][5] = ['Pending Litigation', 'Insurance Claims', 'Fair Housing', 'Evictions',
            'Crime Abatement Programs', 'Site Security', 'Tenant Screening'];

    // Market Information Array.
    features_array['mi'] = [],
        features_array['mi'][1] = ['Average Rent', 'Renovated Asset', 'Rent Change Trend', 'Concessions',
            'YOY Changes in Rent by Unit Type'],
        features_array['mi'][2] = ['12 Months’ Transactions', 'Date Purchased', 'Average Cap Rates',
            'SFR Values in Area', 'On Market Assets in Area'],
        features_array['mi'][3] = ['Unsold Assets', 'Other Brokers Active in Area', 'Demographic Trend',
            'Area Employer Changes', 'Area Crime', 'Foreclosure Activity in Area'],
        features_array['mi'][4] = ['SF of Commercial Assets in Area', '# of Competing Assets in Area',
            'New Development Pipeline', 'Recent Additions and Deletions to Inventory', 'Vacancy in Area'],
        features_array['mi'][5] = ['Comparable Unit Type YOY rent Changes', 'Subject’s Unique Offering to Market',
            'Traffic Counts', 'Immediate area SFR Values', 'Renvoated Status'];

    // Sub Category Names array.
    let sub_category_name = [];
    sub_category_name['ai'] = [],
        sub_category_name['pi'] = [],
        sub_category_name['fi'] = [],
        sub_category_name['li'] = [],
        sub_category_name['mi'] = [],
        sub_category_name['ai'][0] = 'Product',
        sub_category_name['ai'][1] = 'Location',
        sub_category_name['ai'][2] = 'Specification',
        sub_category_name['ai'][3] = 'Class',
        sub_category_name['ai'][4] = 'Issues',
        sub_category_name['pi'][0] = 'Principal',
        sub_category_name['pi'][1] = 'Partner',
        sub_category_name['pi'][2] = 'Attorney',
        sub_category_name['pi'][3] = 'Manager',
        sub_category_name['pi'][4] = 'Advisor',
        sub_category_name['fi'][0] = 'Value',
        sub_category_name['fi'][1] = 'Debt',
        sub_category_name['fi'][2] = 'Upside',
        sub_category_name['fi'][3] = 'Accounting',
        sub_category_name['fi'][4] = 'Risk',
        sub_category_name['li'][0] = 'Code',
        sub_category_name['li'][1] = 'Contract',
        sub_category_name['li'][2] = 'Entity',
        sub_category_name['li'][3] = 'Compliance',
        sub_category_name['li'][4] = 'Exposure',
        sub_category_name['mi'][0] = 'Income',
        sub_category_name['mi'][1] = 'Value',
        sub_category_name['mi'][2] = 'Challenges',
        sub_category_name['mi'][3] = 'Supply',
        sub_category_name['mi'][4] = 'Upside';

    $('#view-asset-info-dialog').on('show.bs.modal', function (event) {
        $('#category-name').text($('#asset-info-type li.active').text());
        const asset_id = $(event.relatedTarget).attr('data-id');
        fetch_asset_info();

        $(document).off('click', '.show-hide-feature')
            $(document).on('click', '.show-hide-feature', function () {
            if (('fa fa-plus-circle' === $(this).find('i').attr('class'))) {
                if (0 === $('#feature-div' + $(this).attr('data-seq')).length) {
                    const ind_feature_div = $('#dummy-individual-feature').clone();
                    ind_feature_div.removeAttr('id');
                    ind_feature_div.removeClass('hidden');

                    const seq = $(this).attr('data-seq'),
                        sub_seq = $('.show-hide-feature[data-seq="1"]').parent().siblings('textarea').attr('data-sub-seq');

                    let div_to_append = '<div class="feature-div" id="feature-div' +
                        $(this).attr("data-seq") + '"><span id="add-feature"' +
                        ' data-seq="' + $(this).attr("data-seq") + '">(( Add Feature ))</span>';

                    if (6 > $(this).attr('data-seq')) {
                        $.each(features_array[$('#asset-info-type li.active').attr('data-val')][seq],
                            function (index, feature) {
                                ind_feature_div.find('textarea').attr('placeholder', 'Feature ' + (index + 1) + ' :' +
                                    feature).attr('data-type', 'feature').addClass('asset-info-field')
                                    .attr('data-seq', seq).attr('data-sub-seq', index + 1);

                                div_to_append += '<div class="input-group feature-div">' + ind_feature_div.html() + '</div>';
                            });

                    } else {
                        for (i = 1; i < 6; i++) {
                            ind_feature_div.find('textarea').attr('placeholder', 'Feature ' + (i))
                                .attr('data-type', 'feature').addClass('asset-info-field')
                                .attr('data-seq', seq).attr('data-sub-seq', i);

                            div_to_append += '<div class="input-group feature-div">' + ind_feature_div.html() + '</div>';
                        }
                    }
                    div_to_append += '</div>';

                    $('.asset-info-field[data-type="sub-category"][data-sub-seq="' +
                        seq + '"]').after(div_to_append);
                    fetch_asset_info();
                } else {
                    $('#feature-div' + $(this).attr('data-seq')).removeClass('hidden');
                }
            } else {
                $('#feature-div' + $(this).attr('data-seq')).addClass('hidden');
            }
            $(this).find('i').toggleClass('fa-plus-circle fa-minus-circle');
        });

        $(document).on('click', '#asset-info-type li', function () {
            $('.feature-div').remove();
            $('.additional-division').remove();

            $('.show-hide-feature i').removeClass('fa-minus-circle');
            $('.show-hide-feature i').addClass('fa-plus-circle');

            $('#category-name').text($('#asset-info-type li.active').text());
            $('#asset-info-row textarea').val('');
            fetch_asset_info();
        });

        $(document).off('change', '.asset-info-field');
        $(document).on('change', '.asset-info-field', function () {
            console.log($(this).attr('data-sub-seq'));

            const update_asset_info = makeAjaxCall('common/update_asset_info', {
                'id': asset_id,
                'val': $(this).val(),
                'seq': $(this).attr('data-seq'),
                'type': $(this).attr('data-type'),
                'sub-seq': $(this).attr('data-sub-seq'),
                'category': $('#asset-info-type li.active').attr('data-val')
            });

            update_asset_info.then(function (data) {
            })
        });

        //Add Sub Category click handler.
        $(document).on('click', '#add-sub-category', function () {
            const add_sub_category = makeAjaxCall('common/update_asset_info', {
                'id': asset_id,
                'val': '',
                'seq': $(this).attr('data-seq'),
                'type': 'sub-category',
                'sub-seq': $('.asset-info-field[data-type="sub-category"]').length,
                'category': $('#asset-info-type li.active').attr('data-val')
            });

            add_sub_category.then(function (data) {
                const dummy_sub_category = $('#dummy-sub-category').clone();
                dummy_sub_category.find('.sub-category-name, .asset-info-field')
                    .attr('data-sub-seq', $('.asset-info-field[data-type="sub-category"]').length);
                dummy_sub_category.removeAttr('id');
                dummy_sub_category.find('h4').html('Sub-Category :'
                    + $('.asset-info-field[data-type="sub-category"]').length +
                    ' <span class="editable sub-category-name"' +
                    'data-kind="sub-category" data-seq="1"' +
                    ' data-sub-seq="' + $('.asset-info-field[data-type="sub-category"]').length + '"> Name </span>' +
                    '<span style="float: right" class="show-hide-feature"' +
                    ' data-seq="' + $('.asset-info-field[data-type="sub-category"]').length + '">' +
                    '<i class="fa fa-plus-circle"></i></span>');

                dummy_sub_category.addClass('additional-division');

                $('#sub-category-div').append(dummy_sub_category.removeClass('hidden'))
            });


        });

        // Add Feature click handler.
        $(document).on('click', '#add-feature', function () {
            const sub_seq = $('#feature-div' + $(this).attr('data-seq') + ' div').length + 1,
                seq = $(this).attr('data-seq'),
                add_feature = makeAjaxCall('common/update_asset_info', {
                    'id': asset_id,
                    'val': '',
                    'seq': seq,
                    'type': 'feature',
                    'sub-seq': sub_seq,
                    'category': $('#asset-info-type li.active').attr('data-val')
                });

            add_feature.then(function (data) {
                const ind_feature_div = $('#dummy-individual-feature').clone();
                ind_feature_div.removeAttr('id');
                ind_feature_div.removeClass('hidden');

                ind_feature_div.find('textarea').attr('placeholder', 'Feature ' + sub_seq)
                    .attr('data-type', 'feature').addClass('asset-info-field')
                    .attr('data-seq', seq).attr('data-sub-seq', sub_seq);

                $('#feature-div' + seq).append(ind_feature_div);
            });


        });

        // Fetch asset information.
        function fetch_asset_info() {

            const get_asset_info = makeAjaxCall('common/fetch_asset_info', {
                id: asset_id,
                category: $('#asset-info-type li.active').attr('data-val')
            });

            get_asset_info.then(function (data) {
                $.each(data.asset_info, function (index, info) {
                    if ('sub-category' === info.hapi_type && 5 < info.hapi_sub_seq &&
                        (0 === $('.asset-info-field[data-type="sub-category"][data-sub-seq="' + info.hapi_sub_seq + '"]').length)) {
                        const dummy_sub_category = $('#dummy-sub-category').clone();
                        dummy_sub_category.find('.sub-category-name, .asset-info-field')
                            .attr('data-sub-seq', $('.asset-info-field[data-type="sub-category"]').length);
                        dummy_sub_category.removeAttr('id');
                        dummy_sub_category.find('h4').html('Sub-Category :'
                            + $('.asset-info-field[data-type="sub-category"]').length +
                            ' <span class="editable sub-category-name"' +
                            'data-kind="sub-category" data-seq="1"' +
                            ' data-sub-seq="' + $('.asset-info-field[data-type="sub-category"]').length + '"> Name </span>' +
                            '<span style="float: right" class="show-hide-feature"' +
                            ' data-seq="' + $('.asset-info-field[data-type="sub-category"]').length + '">' +
                            '<i class="fa fa-plus-circle"></i></span>');

                        dummy_sub_category.addClass('additional-division');

                        $('#sub-category-div').append(dummy_sub_category.removeClass('hidden'))
                    } else if ('feature' === info.hapi_type && 5 < info.hapi_seq && 5 < info.hapi_sub_seq) {
                        const ind_feature_div = $('#dummy-individual-feature').clone();
                        ind_feature_div.removeAttr('id');
                        ind_feature_div.removeClass('hidden');

                        ind_feature_div.find('textarea').attr('placeholder', 'Feature ' + info.hapi_sub_seq)
                            .attr('data-type', 'feature').addClass('asset-info-field')
                            .attr('data-seq', info.hapi_seq).attr('data-sub-seq', info.hapi_sub_seq);

                        $('#feature-div' + info.hapi_seq).append(ind_feature_div);
                    }

                    $('textarea[data-type="' + info.hapi_type + '"][data-seq="' + info.hapi_seq + '"]' +
                        '[data-sub-seq="' + info.hapi_sub_seq + '"]').val(info.hapi_value)
                })
            });

            // Set Placeholder for Subcategories
            $.each($('.sub-category-name'), function (index, sub_category) {
                $(sub_category).text(sub_category_name[$('#asset-info-type li.active').attr('data-val')][index]);
                $('.asset-info-field[data-type="sub-category"][data-sub-seq="' + (index + 1) + '"]')
                    .attr('placeholder', 'General Comments on ' + $(sub_category).text() + '.');
            });

            // Set placeholder for categories.
            $('.asset-info-field[data-type="category"]')
                .attr('placeholder', 'One or two sentence description of Challenge this Factor' +
                    ' presents and the implications of this Challenge presents to the client future.')
        }
    })
});
