$(document).ready(function () {
    $('#view-sgd-dialog').on('show.bs.modal', function (event) {
        $('#sgd-type li.active').trigger('click');
    });

    // Header for each tabs.
    const tab_header = [];
    tab_header['accomplishment'] = '<b>What has been accomplished in the past' +
        ' <input type="text" id="accomplishment-month-count" class="form-control month-count" ' +
        'style="width: 4vw; display: inline-block"/> months?</b>';

    tab_header['projects'] = '<b>What projects will I take on in the next ' +
        '<input type="text" id="projects-month-count" class="form-control month-count" ' +
        'style="width: 4vw; display: inline-block"/> months?</b>';

    tab_header['possibilities'] = '<b>What will become possible for me in the next ' +
        '<input type="text" id="possibilities-month-count" class="form-control month-count"' +
        ' style="width: 4vw; display: inline-block"/> months? </b>';

    tab_header['commitments'] = '<b>What commitments will I make to myself and others in the next ' +
        '<input type="text" class="form-control month-count" id="commitments-month-count"' +
        ' style="width: 4vw; display: inline-block"/> months?</b>';

    tab_header['actions'] = '<b>What actions will I take to fulfill these commitments in the next ' +
        '<input type="text" class="form-control month-count" id="actions-month-count"' +
        ' style="width: 4vw; display: inline-block"/> months?</b>';

    tab_header['results'] = '<b>What results will these actions produce in the next ' +
        '<input type="text" class="form-control month-count" id="results-month-count"' +
        ' style="width: 4vw; display: inline-block"/> months?</b>';


    // Textarea content change handler.
    $(document).off('change', '#view-sgd-dialog textarea');
    $(document).on('change', '#view-sgd-dialog textarea', function () {
        makeAjaxCall('common/update_sgd_text', {
            seq: $(this).attr('data-seq'),
            tab: $('#sgd-type li.active').attr('data-val'),
            val: $(this).val(),
            type: $(this).closest('div').attr('data-type')
        });
    });

    // Date picker change handler.
    $(document).off('change', '.date-picker');
    $(document).on('change', '.date-picker', function () {
        makeAjaxCall('common/update_sgd_date', {
            seq: $(this).attr('data-seq'),
            tab: $('#sgd-type li.active').attr('data-val'),
            val: $(this).val(),
            type: $(this).closest('div').attr('data-type')
        })
    });

    // Month Count Change Handler.
    $(document).off('change', '.month-count');
    $(document).on('change', '.month-count', function () {
        makeAjaxCall('common/update_sgd_month_count', {
            'val': $(this).val(),
            'type': $('#sgd-type li.active').attr('data-val')
        });
    });

    // Tab click handler.
    $(document).on('click', '#sgd-type li', function () {
        const sgd_tab_val = $(this).attr('data-val');
        $('#sgd-type').find('.active').removeClass('active');
        $(this).addClass('active');

        if ('projects' === $(this).attr('data-val') || 'actions' === $(this).attr('data-val')) {
            const dummy_html = $('#projects').clone().removeClass('hidden').removeAttr('id');
            dummy_html.find('h4').html(tab_header[sgd_tab_val]);

            $('#sgd-tab-body').empty();
            $('#sgd-tab-body').append(dummy_html.html().replace(/data-id/g, 'data-type'));

            // Enable DateTime picker.
            $('.date-picker').datetimepicker({
                format: 'YYYY-MM-DD',
                timepicker: false
            });
        } else {
            const dummy_html = $('#accomplishment').clone().removeClass('hidden').removeAttr('id');
            dummy_html.find('h4').html(tab_header[sgd_tab_val]);

            $('#sgd-tab-body').empty();
            $('#sgd-tab-body').append(dummy_html.html().replace(/data-id/g, 'data-type'));
        }

        fetch_sgd();
    });

    // Fetch SGD data.
    function fetch_sgd() {
        // Fetch SGD information.
        const get_sgd_info = makeAjaxCall('common/get_sgd_info', {
            tab: $('#sgd-type li.active').attr('data-val')
        });

        get_sgd_info.then(function (data) {
            $.each(data.sgd_info.info, function (index, info) {
                $('div[data-type="' + info.us_type + '"] textarea[data-seq="' + info.us_seq + '"]').val(info.us_text);

                $('.date-picker[data-type="' + info.us_type + '"][data-seq="' + info.us_seq + '"]').val(info.us_by_when);
            });

            const month_count = data.sgd_info.month_count[0];
            undefined !== month_count ?
                $('#' + month_count.usmc_type + '-month-count').val(month_count.usmc_month) : '';
        })
    }
});