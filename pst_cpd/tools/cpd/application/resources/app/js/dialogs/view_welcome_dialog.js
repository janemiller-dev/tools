$(document).ready(function () {
    // When the dialog is displayed, set the current instance ID.
    $('#view-welcome-dialog').on('show.bs.modal', function (event) {
        let img_src = $(event.relatedTarget).data('href'),
            title = $(event.relatedTarget).data('title');

        $('#welcome-title').text(title);
        $('#welcome-img').attr('src', img_src);
    });
});