$(document).ready(function () {

    $('#view-quick-contact-dialog').on('show.bs.modal', function (event) {
        // Set the default contact date.
        $('#contact-date-picker').val(moment().format("YYYY-MM-DD"));

        $('#contact-date-picker').datetimepicker({
            timepicker: false,
            format: 'YYYY-MM-DD'
        });

        // Date Picker change event handler.
        $('#contact-date-picker').off('change');
        $('#contact-date-picker').on('change', function () {
            refreshContactPage()
        });

        // Previous day click handler.
        $('#contact-prev-day').off('click');
        $('#contact-prev-day').click(function () {
            $('#contact-date-picker')
                .val(moment($('#contact-date-picker').val())
                    .subtract(1, "days").format("YYYY-MM-DD")).trigger('change');
        });

        // Next day click handler.
        $('#contact-next-day').off('click');
        $('#contact-next-day').click(function () {
            $('#contact-date-picker')
                .val(moment($('#contact-date-picker').val())
                    .add(1, "days").format("YYYY-MM-DD")).trigger('change');
        });
       
       
        refreshContactPage();
    });

    function refreshContactPage() {
        const get_quick_contact = makeAjaxCall('common/get_quick_contact', {
            'when': $('#contact-date-picker').val()
        });

        get_quick_contact.then(function (data) {
            $('#quick-contact-table tbody').empty();
            $.each(data.quick_contact.contact_list, function (index, contact) {
                $('#quick-contact-table tbody').append('<tr data-table="quick_contact"' +
                    ' data-id="' + contact.qc_id + '" data-key="qc_id">' +
                    '<td>' + (index + 1) + '</td>' +
                    '<td><select class="form-control quick-tool-field"' +
                    ' data-col="qc_ceo" id="contact-ceo' + contact.qc_id + '">' +
                    '<option value="c">Critical</option>' +
                    '<option value="e">Essential</option>' +
                    '<option value="o">Optional</option>' +
                    '</td>' +
                    '<td>' +
                    '<textarea class="form-control quick-tool-field" id="contact' + contact.qc_id + '"' +
                    ' rows="1" cols="23" data-col="qc_contact" onkeyup="auto_grow(this)"></textarea>' +
                    '</td>' +
                    '<td>' +
                    '<textarea class="form-control quick-tool-field" id="contact-information' + contact.qc_id + '"' +
                    ' rows="1" data-col="qc_info" cols="23"  onkeyup="auto_grow(this)"></textarea>' +
                    '</td>' +
                    '<td><select class="form-control quick-tool-field quick-tool-who" data-col="qc_who" ' +
                    'id="contact-who' + contact.qc_id + '">' +
                    '<option value="0">--Add New--</option>' +
                    $.map(data.quick_contact.assignee, function (assignee, index) {
                        return '<option value="' + assignee.tsl_assignee_id + '">' + assignee.tsl_assignee_name + '</option>';
                    }).join('') +
                    '</select></td>' +
                    '<td><input type="text" class="form-control quick-tool-field quick-field-tag"' +
                    ' id="contact-tag' + contact.qc_id + '" data-col="qc_tag"></td>' +
                    '<td><select class="form-control quick-tool-field" id="contact-cat' + contact.qc_id + '"' +
                    ' data-col="qc_cat">' +
                    '<option value="c">Complete</option>' +
                    '<option value="a">Abandon</option>' +
                    '<option value="t">Transfer</option>' +
                    '<option value="p">Pending</option>' +
                    '</select></td>' +
                    '<td><span class="btn btn-default delete-quick-tool"><i class="fa fa-trash"></i></span></td>' +
                    '</tr>');

                $('#contact-ceo' + contact.qc_id).val(contact.qc_ceo);
                $('#contact-who' + contact.qc_id).val(contact.qc_who);
                $('#contact' + contact.qc_id).val(contact.qc_contact);
                $('#contact-information' + contact.qc_id).val(contact.qc_info);
                $('#contact-cat' + contact.qc_id).val(contact.qc_cat);
                $('#contact-tag' + contact.qc_id).val(contact.qc_tag);
                $('#' + contact.qc_id).val(contact.qc_tag);
            })
        })
    }

    // Add Project Click Handler.
    $('#add-contact').off('click');
    $('#add-contact').on('click', function () {

        // AJax call to add new contact.
        const add_new_contact = makeAjaxCall('common/add_new_contact', {
            'when': $('#contact-date-picker').val()
        });
        add_new_contact.then(function () {
            toastr.success('Contact Added.');
            refreshContactPage();
        })
    })

});