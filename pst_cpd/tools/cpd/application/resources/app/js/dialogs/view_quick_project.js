$(document).ready(function () {

    $('#view-quick-project-dialog').on('show.bs.modal', function (event) {

        // Set the default project date.
        $('#project-date-picker').val(moment().format("YYYY-MM-DD"));

        $('#project-date-picker').datetimepicker({
            timepicker: false,
            format: 'YYYY-MM-DD'
        });

        // Date Picker change event handler.
        $('#project-date-picker').off('change');
        $('#project-date-picker').on('change', function () {
            refreshProjectPage()
        });

        // Previous day click handler.
        $('#project-prev-day').off('click');
        $('#project-prev-day').click(function () {
            $('#project-date-picker')
                .val(moment($('#project-date-picker').val())
                    .subtract(1, "days").format("YYYY-MM-DD")).trigger('change');
        });

        // Next day click handler.
        $('#project-next-day').off('click');
        $('#project-next-day').click(function () {
            $('#project-date-picker')
                .val(moment($('#project-date-picker').val())
                    .add(1, "days").format("YYYY-MM-DD")).trigger('change');
        });

        refreshProjectPage();
    });

    function refreshProjectPage() {

        const get_quick_project = makeAjaxCall('common/get_quick_project', {
            'when' : $('#project-date-picker').val()
        });

        get_quick_project.then(function (data) {
            $('#quick-project-table tbody').empty();
            $.each(data.quick_project.project_list, function (index, project) {
                $('#quick-project-table tbody').append('<tr data-table="quick_project"' +
                    ' data-id="' + project.qp_id + '" data-key="qp_id">' +
                    '<td>' + (index + 1) + '</td>' +
                    '<td><select class="form-control quick-tool-field" id="project-ceo' + project.qp_id + '"' +
                    ' data-col="qp_ceo">' +
                    '<option value="c">Critical</option>' +
                    '<option value="e">Essential</option>' +
                    '<option value="o">Optional</option>' +
                    '</td>' +
                    '<td>' +
                    '<textarea class="form-control quick-tool-field" id="project' + project.qp_id + '"' +
                    ' rows="1" cols="30" data-col="qp_project" onkeyup="auto_grow(this)" style="width: 100%"></textarea>' +
                    '</td>' +
                    '<td><select class="form-control quick-tool-field quick-tool-who" data-col="qp_who"' +
                    ' id="project-who' + project.qp_id + '">' +
                    '<option value="0">--Add Name--</option>' +
                    $.map(data.quick_project.assignee, function (assignee, index) {
                        return '<option value="' + assignee.tsl_assignee_id + '">' + assignee.tsl_assignee_name + '</option>';
                    }).join('') +
                    '</select></td>' +
                    '<td><input type="text" class="form-control quick-tool-field quick-field-tag"' +
                    ' id="project-tag' + project.qp_id + '" data-col="qp_tag"></td>' +
                    '<td><select class="form-control quick-tool-field" id="project-cat' + project.qp_id + '"' +
                    ' data-col="qp_cat">' +
                    '<option value="c">Complete</option>' +
                    '<option value="a">Abandon</option>' +
                    '<option value="t">Transfer</option>' +
                    '<option value="p">Pending</option>' +
                    '</select></td>' +
                    '<td><span class="btn btn-default delete-quick-tool"><i class="fa fa-trash"></i></span></td>' +
                    '</tr>');

                $('#project-ceo' + project.qp_id).val(project.qp_ceo);
                $('#project-tag' + project.qp_id).val(project.qp_tag);
                $('#project-who' + project.qp_id).val(project.qp_who);
                $('#project' + project.qp_id).val(project.qp_project);
                $('#project-cat' + project.qp_id).val(project.qp_cat);
                $('#' + project.qp_id).val(project.qp_tag);
            });
        });
    }

    // Add Project Click Handler.
    $('#add-project').off('click');
    $('#add-project').on('click', function () {

        // AJax call to add new project.
        const add_new_project = makeAjaxCall('common/add_new_project', {
            'when' : $('#project-date-picker').val()
        });
        add_new_project.then(function () {
            toastr.success('Project Added.');
            refreshProjectPage();
        })
    })

});