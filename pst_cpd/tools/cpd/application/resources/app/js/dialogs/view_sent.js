let asset_id, sent_controller, origin;
$(document).ready(function () {

    $('#view-sent-dialog').on('show.bs.modal', function (event) {

        // Get the Client ID from the URL
        const path = window.location.pathname,
            components = path.split('/'),
            client_id = components[components.length - 1],
            component_name = $(event.relatedTarget).attr('data-name'),
            table_name = $(event.relatedTarget).attr('data-table'),
            col_name = $(event.relatedTarget).attr('data-col'),
            ref_primary_key = $(event.relatedTarget).attr('data-primary-key');

            origin = components[2];
            sent_controller = $(event.relatedTarget).attr('data-controller');
            asset_id = $(event.relatedTarget).attr('data-id');

        $('#sent-div-heading').html(component_name + ' Documents');
        $('#sent-name').html(component_name);

        $.ajax({
            url: jsglobals.base_url + 'cpdv1/view_sent',
            dataType: 'json',
            type: 'post',
            data: {
                deal_id: asset_id,
                type: origin,
                component: component_name,
                col: col_name,
                table: table_name,
                ref_key: ref_primary_key
            },
            error: ajaxError
        }).done(function (data) {
            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            $('#edit-page-dialog').modal('hide');
            sa_added = 1;
            updateSentList(data.sent_items);
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });


    });

    // Update Sent List.
    function updateSentList(lists) {
        $('#no-sent').hide();
        $('#sent-table tbody').empty();

        $.each(lists, function (instance, doc) {

            // Check if the request is from homebase
            ('homebase' === origin) ? origin = doc.component_sent_origin : '';

            $('#sent-table tbody')
                .append($('<tr>')
                    .append($('<td class="text-center">')
                        .append(instance + 1))
                    .append($('<td class="text-center">')
                        .append('<a href="' + jsglobals.base_url + origin + sent_controller + asset_id +
                            '?ref=' + doc.component_sent_doc_id + '?id=' + doc.component_sent_tool_id + '">'
                            + doc.name + '</a>'))
                    .append($('<td class="text-center">')
                        .append(doc.component_sent_datetime))
                )
        })
    }
});
// Make ref pop up drop down empty.