$(document).ready(function() {

    // Update to show selected value.
    $( document ).on( 'click', '.bs-dropdown-to-select-group .dropdown-menu li', function( event ) {
	var $target = $( event.currentTarget );
	$target.closest('.bs-dropdown-to-select-group')
	    .find('[data-bind="bs-drp-sel-value"]').val($target.attr('data-value'))
	    .end()
	    .children('.dropdown-toggle').dropdown('toggle');
	$target.closest('.bs-dropdown-to-select-group')
	    .find('[data-bind="bs-drp-sel-label"]').text($target.context.textContent);
	return false;
    });

    $('#share-names').tokenfield({
	autocomplete: {
	    source: function(request, response) {
		jQuery.get(jsglobals.base_url + "user/search_user_email", {
		    q: request.term
		}, function(data) {
		    data = $.parseJSON(data);
		    response(data);
		});
	    },
	    delay: 100
	},
	showAutocompleteOnFocus: false,
	createTokensOnBlur: false,
	inputType: 'email'
    });

    $('#share-names').on('tokenfield:createtoken', function (event) {
	var existingTokens = $(this).tokenfield('getTokens');
	$.each(existingTokens, function(index, token) {
	    if (token.value === event.attrs.value)
		event.preventDefault();
	});
    });


    // On the display of the form, clear all of the contents.
    $('#share-dialog').on('show.bs.modal', function() {
	// Determine the object and the id from the URL.
	// Get the ID from the URL
	var path = window.location.pathname;
	var components = path.split('/');
	var id = components[components.length - 1];
	var object = components[components.length - 2];

	$('#share-names').val('');
	$('#share-permission').val('');
	$('#share-object').val(object);
	$('#share-id').val(id);

	// Remove all of the tokens.
	$(".tokenfield .token").remove();
    });

    // Validate the add instance form and submit it if it is valid.
    $('#share-form').formValidation({
	framework: 'bootstrap',
	icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
	},
	fields: {
	    emails: {
		validators: {
		    notEmpty: {
			message: "At least one email address must be specified."
		    }
		}
	    },
	    permission: {
		validators: {
		    notEmpty: {
			message: "The permission must be set.."
		    }
		}
	    },
	}
    }).on('success.form.fv', function(e) {
	e.preventDefault();

	// Get the form instance
	var $form = $(e.target);
	var fv = $form.data('formValidation');

	// Get the form data and submit it.
	var message_data = $form.serialize();
	$.ajax({
	    url: jsglobals.base_url + 'common/share',
	    dataType: 'json',
	    type: 'post',
	    data: message_data
	}).done(function(data) {
	    fv.resetForm();
	    if (data.status != 'success') {
		toastr.error(data.message);
		return;
	    }
            else if (data.status == 'redirect') {
		window.location.href = data.redirect;
		return;
            }
	    $('#share-dialog').modal('hide');
	    refreshPage();
	}).fail(function(jqXHR, status) {
	    toastr.error("Server communication error. Please try again.");
        }).always(function() {
        });
    });
});
