let update_drop_down = false;

$(document).ready(function () {
    $('#view-agent-profile-dialog').on('show.bs.modal', function () {
        $('.agent-info-select').val('0');
        update_drop_down = true;
        agent_profile.init();
    });

    // Enable editable.
    $('#view-agent-profile-dialog').on('shown.bs.modal', function () {
        setEditable();
        $(document).off('focusin.modal');

        //Agent Position popover tabs.
        $('#agent-position, #agent-track').on('shown.bs.popover', function () {
            $('.position-radio').off('change');
            $('.position-radio').on('change', function () {
                $('.position-tab').addClass('hidden');
                $('#' + $(this).val() + '-div').removeClass('hidden');
            })

            $('.agd1-radio').off('change');
            $('.agd1-radio').on('change', function () {
                $('.agd1-tab').addClass('hidden');
                $('#' + $(this).val() + '-div').removeClass('hidden');
            })

            $('.agd2-radio').off('change');
            $('.agd2-radio').on('change', function () {
                $('.agd2-tab').addClass('hidden');
                $('#' + $(this).val() + '-div').removeClass('hidden');
            })
        });

    });

    // Drop Down change event handler.
    $(document).on('change', '.agent-info-select', function () {
        const selected = $(this).val();
        $('.agent-info-select').val('0');
        $(this).val(selected);

        // Check if add new option is selected.
        if ('-1' === $(this).val()) {
            $('#add-agent').val(1);
            $('#edit-agent-profile-dialog').modal('show');
            $('#agent-type').val($(this).attr('data-name'));
        } else {
            update_drop_down = false;
            agent_profile.init();
        }
    });

    // Agent Asset Image click handler.
    $(document).on('click', '.agent-asset-image', function () {
        $('.agent-asset-active').removeClass('agent-asset-active');
        $(this).parent().parent().addClass('agent-asset-active');
    });

    // Pics button click handler.
    $(document).on('click', '#agent-asset-pics', function () {
        $('#enable-poi, #disable-poi').addClass('hidden');
        $('#agent-pics-div').removeClass('hidden');
        $('#agents-map-div').addClass('hidden');
        $('#agents-info-div').addClass('hidden');
    });

    // Enable Voice Recognition.
    $(document).on('shown.bs.popover', '#view-agent-profile-dialog [data-toggle=popover]', function () {
        voiceRecognition();
        speechFunctionality();
    });

    // Info button click handler.
    $(document).on('click', '#agent-asset-info', function () {
        // Check if active asset is present.
        if ($('.agent-asset-active').length === 0) {
            toastr.warning('Please select a deal from deals tab.', 'No Deal Selected');
            return;
        } else {
            $('#enable-poi, #disable-poi').addClass('hidden');
            $('#agent-pics-div').addClass('hidden');
            $('#agents-map-div').addClass('hidden');
            $('#agents-info-div').removeClass('hidden');
            get_agent_asset_info();
        }
    });

    // Show Agent Asset Map.
    $(document).on('click', '#agent-asset-map', function () {
        if ($('.agent-asset-active').length === 0) {
            toastr.warning('Please select a deal from deals tab.', 'No Deal Selected');
            return;
        } else {
            $('#agent-pics-div').addClass('hidden');
            $('#agents-info-div').addClass('hidden');
            $('#agents-map-div').removeClass('hidden');
            let lat = $('.agent-asset-active').parent().parent().attr('data-lat'),
                lng = $('.agent-asset-active').parent().parent().attr('data-lng'),
                address = $('.agent-asset-active').parent().parent().attr('data-address');

            if ((lat === 'null' || lng === 'null') && address !== 'null') {
                const cordinate = getLongLat(address);
                cordinate.then(function (data) {
                    lat = data.results[0].geometry.location.lat;
                    lng = data.results[0].geometry.location.lng;

                    console.log(lat);
                    console.log(lng);
                    initMap();
                });
            } else if ((lat === 'null' || lng === 'null') && address === 'null') {
                toastr.warning('Please update asset address field.', 'Asset Address is not present.')
            } else {
                initMap();
            }
        }

        // Initialize Map.
        function initMap() {
            let asset_address = new google.maps.LatLng(lat, lng),
                map = new google.maps.Map(document.getElementById('agents-map-div'), {
                    zoom: 15,
                    center: asset_address
                }),
                customStyled = [{
                    featureType: "poi",
                    stylers: [
                        {visibility: "off"}
                    ]
                }],

                contentString = '<table><tr>' + $('.agent-asset-active').html() + '</tr></table>',

                // Set up Popover with asset info.
                infowindow = new google.maps.InfoWindow({
                    content: contentString
                }),

                marker = new google.maps.Marker({
                    position: asset_address,
                    map: map,
                    title: 'Asset Information'
                });

            // Set Styling for map.
            map.set('styles', customStyled);
            $('#enable-poi').removeClass('hidden');

            // Show Points Of interest.
            $('#enable-poi').click(function () {
                $('#enable-poi').addClass('hidden');
                $('#disable-poi').removeClass('hidden');

                let customStyle = [{
                    featureType: "poi",
                    stylers: [
                        {visibility: "on"}
                    ]
                }];

                map.set('styles', customStyle);
            });

            // Hide Points of Interest.
            $('#disable-poi').click(function () {
                $('#enable-poi').removeClass('hidden');
                $('#disable-poi').addClass('hidden');

                let customStyle = [{
                    featureType: "poi",
                    stylers: [
                        {visibility: "off"}
                    ]
                }];

                map.set('styles', customStyle);
            });

            // Add Asset info popover to map marker.
            marker.addListener('click', function () {
                infowindow.open(map, marker);
            });

            infowindow.open(map, marker);
        }
    });

    function get_agent_asset_info() {
        const get_asset_info = makeAjaxCall('common/get_asset_info', {
            type: $('#agent-profile-nav li.active').attr('data-type'),
            id: $('.agent-asset-active').parent().parent().attr('data-id')
        });

        get_asset_info.then(function (data) {
            $('.agent-asset-input').val('');
            $.each(data.asset_info[0], function (index, data) {
                $('.agent-asset-input[data-col="' + index + '"]').val(data);
            })
        })
    }

    // Agent Asset Input change handler.
    $(document).on('change', '.agent-asset-input', function () {
        makeAjaxCall('common/update_agent_info', {
            'col': $(this).attr('data-col'),
            'table': $('#agent-profile-nav li.active').attr('data-table'),
            'id': $('.agent-asset-active').parent().parent().attr('data-id'),
            'val': $(this).val(),
            'key': $('#agent-profile-nav li.active').attr('data-key')
        });
    })
});

let agent_profile = function () {

    // Initialize and get the DMD ID.
    let init = () => {
            let id = $(".agent-info-select").filter(function () {
                return $(this).val() !== '0' ? $(this) : '';
            }).val();

            const fetch_agent_info = makeAjaxCall('common/get_agent_info', {
                'id': id
            });

            fetch_agent_info.then(function (data) {
                const info = data.agent_info[0];

                if (info === undefined) {
                    $('.agent-info-select').empty();

                    // Add add new info option.
                    $('.agent-info-select')
                        .append('<option value="0" selected>-- None --</option>')
                        .append('<option value="-1">-- Add New --</option>');
                } else {
                    id = info.ap_id
                }

                // Agent Image upload.
                $(document).off('click', '#upload-agent-img');
                $(document).on('click', '#upload-agent-img', function () {
                    $('#agent-img-upload').trigger('click');

                    const target = 'agent-img-upload',
                        file_type_allowed = /.\.(png|jpeg|jpg)/i,
                        data = {
                            'agent_id': id
                        };

                    // Upload Image file.
                    const upload_image = uploadFile(target, data, file_type_allowed);
                    upload_image.then(function (data) {
                        console.log('here');
                        // refreshPage();
                    });
                });

                // Profile button popover event handler
                $('.agent-profile-btn').off('shown.bs.popover')
                $('.agent-profile-btn').on('shown.bs.popover', function () {

                    // Fetch the info about client.
                    const get_agent_popup_info = makeAjaxCall('common/get_agent_popup_info', {
                        'agent_id': id
                    });

                    // Append info to the appropriate textbox.
                    get_agent_popup_info.then(function (data) {
                        $.each(data.agent_info, function (index, info) {
                            $('textarea[data-type=' + info.ai_type + ']').val(info.ai_value);
                        })
                    });

                    // Change of field event handler.
                    // $('.owner-popup-info').off('change');
                    // $('.owner-popup-info').change(function (data) {
                    //     makeAjaxCall('homebase/save_owner_popup_info', {
                    //         'owner_id': owner_id,
                    //         'val': $(this).val(),
                    //         'type': $(this).attr('data-type')
                    //     });
                    // })
                });

                // Agent Track record popover.
                $('#agent-track').on('shown.bs.popover', function () {
                    setEditable();
                });

                // Check for agent popover info change.
                $(document).off('change', '.agent-popup-info');
                $(document).on('change', '.agent-popup-info', function () {
                    makeAjaxCall('common/save_agent_popover_info', {
                        'type': $(this).attr('data-type'),
                        'val': $(this).val(),
                        'id': id
                    })
                });

                // Set the value for agent Id and add agent popup.
                $('#edit-agent-profile-button').click(function () {
                    $('#agent-id').val(id);
                    $('#add-agent').val(0);
                });

                (null !== info['ap_image']) ? $('#agent-avatar').attr('src', window.location.origin +
                    '/tools/homebase/get_owner_image/?img=' + info['ap_image']) :
                    $('#agent-avatar').attr('src', '/tools/resources/app/media/user-avatar.svg');

                // Append Owner Information.
                $('#agent-info').html(
                    '<h1 style="margin: 0">' + info['ap_name'] + '</h1>' +
                    '<h3>' + info['ap_title'] + '</h3>' +
                    '<h4>' + info['ap_years_experience'] + '</h4>' +
                    '<h4>' + info['ap_focus'] + '</h4>'
                );

                $('#agent-contact').html(
                    '<h4>' + info['ap_company'] + '</h4>' +
                    '<p>' + info['ap_address'] + '</p>' +
                    '<p>' + info['ap_office_phone'] + '</p>' +
                    '<p>' + info['ap_second_phone'] + '</p>' +
                    '<p>' + info['ap_cell_phone'] + '</p>' +
                    '<p>' + info['ap_email'] + '</p>'
                );

                // Check if ID field is set or not.
                if (true === update_drop_down) {
                    $('.agent-info-select').empty();

                    // Add add new info option.
                    $('.agent-info-select')
                        .append('<option value="0" selected>-- None --</option>')
                        .append('<option value="-1">-- Add New --</option>');

                    // Append Options to Drop downs.
                    $.each(data.agent_info, function (index, info) {
                        $('#info-' + info.ap_type).append('<option value="' + info.ap_id + '">' + info.ap_name + '</option>');

                    })
                }
            });

            const fetch_agent_assets = makeAjaxCall('common/get_user_assets', {
                'qtr': moment().quarter(),
                'year': moment().year()
            });

            fetch_agent_assets.then(function (data) {
                let deals = data.agent_info,
                    merged_array = {};

                // Merge the Presentation Presented and Requested arrays into Possible.
                merged_array['prospective'] = $.merge(deals['requested'] ?
                    deals['requested'] : [], deals['presented'] ?
                    deals['presented'] : []);

                // Merge Contract hard and contact array into Predictable array.
                merged_array['predictable'] = $.merge(deals['contract'] ?
                    deals['contract'] : [], deals['hard'] ?
                    deals['hard'] : []);

                // Merge Deals Listed and Offers made array into Probable array.
                merged_array['probable'] = $.merge(deals['listed'] ?
                    deals['listed'] : [], deals['launched'] ?
                    deals['launched'] : []);

                // Completed Array.
                merged_array['completed'] = deals['complete'];

                // Identified Array.
                merged_array['identified'] = deals['identified'];

                // console.log(merged_array['completed']);
                append_management_board(merged_array);

                // $('#agent-track').attr('data-content', get_agent_track_record(deals['complete']));
            });
        },
        get_agent_track_record = (deals_completed) => {
            let content = '<table class="table"><tbody><tr style="border: solid 1px #eee">';
            $.each(deals_completed, function (index, deal) {
                let img_src = 'https://cpd.powersellingtools.com/wp-content/uploads/2020/04/Default-Profile-Pic.png';
                (null !== deal.hca_avatar) ?
                    img_src = window.location.origin +
                        '/tools/homebase/get_owner_image/?img='
                        + deal.hca_avatar : '';

                // Check if this is the third element in array.
                // If so add new row.
                if ((index !== 0) && (index % 3 === 0)) {
                    content += '</tr><tr style="border: solid 1px #eee">';
                }

                const built_year = (null !== deal.aai_built_year) ? deal.aai_built_year : "Enter Built Year:",
                    list_price = (null !== deal.aai_list_price) ? deal.aai_list_price : 'Enter LIST PRICE: ',
                    price_per_sf = (null !== deal.aai_price_per_sf) ? deal.aai_price_per_sf : 'Enter Price / SF: ',
                    price_per_unit = (null !== deal.aai_price_per_unit) ? deal.aai_price_per_unit : 'Enter Price / Units:',
                    units = (null !== deal.aai_units) ? deal.aai_units : 'Enter Units: ',
                    avg_sf = (null !== deal.aai_avg_sf) ? deal.aai_avg_sf : 'Enter Avg SF: ';

                content += '<td>' +
                    '<img src="' + img_src + '"' +
                    ' id="agent-asset-avatar" class="agent-asset-image" style="width: 11vw"></td>' +
                    '<td style="text-align: left">' +
                    '<table style="border-collapse: separate; border-spacing: 0.9em 0.3em; font-size: 15px; width: 100%">' +
                    '<tr>' +
                    '<td style="text-align: left">BUILT:</td>' +
                    '<td style="text-align: right"><span class="editable" id="aai_built_year' + deal.hca_id + '"' +
                    ' data-pk="' + deal.hca_id + '" data-name="aai_built_year" data-placement="top"' +
                    ' data-url="/tools/common/update_agent_asset_info" data-type="text">' +
                    built_year + '</span></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="text-align: left">UNITS:</td>' +
                    '<td style="text-align: right"><span class="editable" id="aai_units' + deal.hca_id + '"' +
                    ' data-pk="' + deal.hca_id + '" data-name="aai_units" data-placement="top"' +
                    ' data-url="/tools/common/update_agent_asset_info" data-type="text">' + units +
                    '</span></td> ' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="white-space: nowrap; text-align: left">PRICE / UNIT:</td>' +
                    '<td style="text-align: right"><span class="editable" id="aai_price_per_unit' + deal.hca_id + '"' +
                    ' data-pk="' + deal.hca_id + '" data-name="aai_price_per_unit" data-placement="top"' +
                    ' data-url="/tools/common/update_agent_asset_info" data-type="text">' + price_per_unit +
                    '</span></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="white-space: nowrap; text-align: left">PRICE / SF:</td>' +
                    '<td style="text-align: right"><span class="editable" id="aai_price_per_sf' + deal.hca_id + '"' +
                    ' data-pk="' + deal.hca_id + '" data-name="aai_price_per_sf" data-placement="top"' +
                    ' data-url="/tools/common/update_agent_asset_info" data-type="text">'
                    + price_per_sf + '</span></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="text-align: left">AVG SF:</td>' +
                    '<td style="text-align: right"><span class="editable" id="aai_avg_sg' + deal.hca_id + '"' +
                    ' data-pk="' + deal.hca_id + '" data-name="aai_avg_sf" data-placement="top"' +
                    ' data-url="/tools/common/update_agent_asset_info" data-type="text">' + avg_sf +
                    '</span></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="text-align: left">LIST PRICE:</td>' +
                    '<td style="text-align: right"><span class="editable" id="aai_list_price' + deal.hca_id + '"' +
                    ' data-pk="' + deal.hca_id + '" data-name="aai_list_price" data-placement="top"' +
                    ' data-url="/tools/common/update_agent_asset_info" data-type="text">' + list_price + '</span></td>' +
                    '</tr>' +
                    '</table>' +
                    '</td>'
            });
            content += '</tr></table>';

            return content;
        },
        append_management_board = (merged_array) => {
            $.each(merged_array, function (index, deals) {
                $('#agent-' + index + '-table').empty();
                const content = get_management_board(deals);

                if (((content.match(/<img/g) || []).length) === 1) {
                    $('#agent-' + index + '-table').parent('div').toggleClass('col-xs-12 col-xs-4');
                } else if (((content.match(/<img/g) || []).length) === 2) {
                    $('#agent-' + index + '-table').parent('div').toggleClass('col-xs-12 col-xs-8');
                }
                $('#agent-' + index + '-table').append(content);
            });
        },

        get_management_board = (deals) => {
            const yellow_rows_array = [
                'maybe',
                'completed',
                'presented',
                'hard',
                'launched',
                'accepted_co',
                'complete_to',
                'accepted_o',
                'p_complete',
                'receive_sp',
                'deliver_su',
                'offer_sa',
                'ref_pla',
                'ref_pa',
                'ref_off'
            ];

            let content = '<tr>';
            $.each(deals, function (key, deal) {
                let class_to_append = '';
                if (-1 !== $.inArray(deal.ds_status, yellow_rows_array)) {
                    class_to_append = 'yellow_row';
                }

                if ((key !== 0) && (key % 3 === 0)) {
                    content += '</tr><tr style="border: solid 1px #eee">';
                }

                const built_year = (null !== deal.aai_built_year) ? deal.aai_built_year : "Enter Built Year:",
                    list_price = (null !== deal.aai_list_price) ? deal.aai_list_price : 'Enter LIST PRICE: ',
                    price_per_sf = (null !== deal.aai_price_per_sf) ? deal.aai_price_per_sf : 'Enter Price / SF: ',
                    price_per_unit = (null !== deal.aai_price_per_unit) ? deal.aai_price_per_unit : 'Enter Price / Units:',
                    units = (null !== deal.aai_units) ? deal.aai_units : 'Enter Units: ',
                    avg_sf = (null !== deal.aai_avg_sf) ? deal.aai_avg_sf : 'Enter Avg SF: ';

                let img_src = 'https://cpd.powersellingtools.com/wp-content/uploads/2020/04/Default-Profile-Pic.png';
                (null !== deal.hca_avatar) ?
                    img_src = window.location.origin +
                        '/tools/homebase/get_owner_image/?img='
                        + deal.hca_avatar : '';

                // <tr style="border: solid 1px #eee" data-lat="' + deal.hca_lat + '"' +
                //     ' data-id="' + deal.hca_id + '"' +
                //     ' data-lng="' + deal.hca_lng + '" data-address="' + deal.hca_address + '">


                content += '<td>' +
                    '<table data-lat="' + deal.hca_lat + '" data-id="' + deal.hca_id + '"' +
                    ' data-lng="' + deal.hca_lng + '" data-address="' + deal.hca_address + '">' +
                    '<td style="border: solid 1px #eee">' +
                    '<img src="' + img_src + '"' +
                    ' id="agent-asset-avatar" class="agent-asset-image" style="width: 11vw"></td>' +
                    '<td style="text-align: left">' +
                    '<table style="border-collapse: separate; border-spacing: 0.9em 0.3em;' +
                    ' font-size: 15px; width: 100%"' +
                    ' class="' + class_to_append + '">' +
                    '<tr>' +
                    '<td style="text-align: left">BUILT:</td>' +
                    '<td style="text-align: right"><span class="editable" id="aai_built_year' + deal.hca_id + '"' +
                    ' data-pk="' + deal.hca_id + '" data-name="aai_built_year"' +
                    ' data-url="/tools/common/update_agent_asset_info" data-type="text">' +
                    built_year + '</span></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="text-align: left">UNITS</td>' +
                    '<td style="text-align: right"><span class="editable" id="aai_units' + deal.hca_id + '"' +
                    ' data-pk="' + deal.hca_id + '" data-name="aai_units"' +
                    ' data-url="/tools/common/update_agent_asset_info" data-type="text">' + units +
                    '</span></td> ' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="white-space: nowrap; text-align: left">PRICE / UNIT:</td>' +
                    '<td style="text-align: right"><span class="editable" id="aai_price_per_unit' + deal.hca_id + '"' +
                    ' data-pk="' + deal.hca_id + '" data-name="aai_price_per_unit"' +
                    ' data-url="/tools/common/update_agent_asset_info" data-type="text">' + price_per_unit +
                    '</span></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="text-align: left">PRICE / SF:</td>' +
                    '<td style="text-align: right"><span class="editable" id="aai_price_per_sf' + deal.hca_id + '"' +
                    ' data-pk="' + deal.hca_id + '" data-name="aai_price_per_sf"' +
                    ' data-url="/tools/common/update_agent_asset_info" data-type="text">'
                    + price_per_sf + '</span></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="text-align: left">AVG SF:</td>' +
                    '<td style="text-align: right"><span class="editable" id="aai_avg_sg' + deal.hca_id + '"' +
                    ' data-pk="' + deal.hca_id + '" data-name="aai_avg_sf"' +
                    ' data-url="/tools/common/update_agent_asset_info" data-type="text">' + avg_sf +
                    '</span></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<td style="text-align: left">LIST PRICE:</td>' +
                    '<td style="text-align: right"><span class="editable" id="aai_list_price' + deal.hca_id + '"' +
                    ' data-pk="' + deal.hca_id + '" data-name="aai_list_price"' +
                    ' data-url="/tools/common/update_agent_asset_info" data-type="text">' + list_price + '</span></td>' +
                    '</tr>' +
                    '</table>' +
                    '</td></table></td>';
                // '<td style="vertical-align: middle">' +
                // '<table>' +
                // '<tr>' +
                // '<td><span class="glyphicon glyphicon-plus"></span></td>' +
                // '</tr>' +
                // '<tr>' +
                // '<td><span class="glyphicon glyphicon-trash"></span></td>' +
                // '</tr>' +
                // '</table>' +
                // '</td>' +
                //  '</tr>'

            });

            return content;
        };

    return {
        init: init
    };

}();
