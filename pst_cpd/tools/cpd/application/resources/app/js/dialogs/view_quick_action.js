$(document).ready(function () {

    $('#view-quick-action-dialog').on('show.bs.modal', function (event) {

        // Set the default action date.
        $('#action-date-picker').val(moment().format("YYYY-MM-DD"));

        $('#action-date-picker').datetimepicker({
            timepicker: false,
            format: 'YYYY-MM-DD'
        });

        // Date Picker change event handler.
        $('#action-date-picker').off('change');
        $('#action-date-picker').on('change', function () {
            refreshActionPage()
        });

        // Previous day click handler.
        $('#action-prev-day').off('click');
        $('#action-prev-day').click(function () {
            $('#action-date-picker')
                .val(moment($('#action-date-picker').val())
                    .subtract(1, "days").format("YYYY-MM-DD")).trigger('change');
        });

        // Next day click handler.
        $('#action-next-day').off('click');
        $('#action-next-day').click(function () {
            $('#action-date-picker')
                .val(moment($('#action-date-picker').val())
                    .add(1, "days").format("YYYY-MM-DD")).trigger('change');
        });
        
        refreshActionPage();
    });

    function refreshActionPage() {
        const get_quick_action = makeAjaxCall('common/get_quick_action', {
            'when': $('#action-date-picker').val()
        });

        get_quick_action.then(function (data) {
            $('#quick-action-table tbody').empty();
            $.each(data.quick_action.action_list, function (index, action) {
                $('#quick-action-table tbody').append('<tr data-table="quick_action"' +
                    ' data-id="' + action.qa_id + '" data-key="qa_id">' +
                    '<td>' + (index + 1) + '</td>' +
                    '<td><select class="form-control quick-tool-field" id="action-ceo' + action.qa_id + '" data-col="qa_ceo">' +
                    '<option value="c">Critical</option>' +
                    '<option value="e">Essential</option>' +
                    '<option value="o">Optional</option>' +
                    '</td>' +
                    '<td>' +
                    '<textarea class="form-control quick-tool-field" id="action' + action.qa_id + '" rows="1"' +
                    ' cols="35" data-col="qa_action"  onkeyup="auto_grow(this)" style="width: 100%"></textarea>' +
                    '</td>' +
                    '<td><select class="form-control quick-tool-field quick-tool-who" data-col="qa_who"' +
                    ' id="action-who' + action.qa_id + '">' +
                    '<option value="0">--Add New--</option>' +
                    $.map(data.quick_action.assignee, function (assignee, index) {
                        return '<option value="' + assignee.tsl_assignee_id + '">' + assignee.tsl_assignee_name + '</option>';
                    }).join('') +
                    '</select></td>' +
                    '<td>' +
                    '<input type="text" class="form-control quick-tool-field quick-field-tag"' +
                    ' id="action-tag' + action.qa_id + '" data-col="qa_tag"></td>' +
                    '<td><select class="form-control quick-tool-field" id="action-cat' + action.qa_id + '"' +
                    ' data-col="qa_cat">' +
                    '<option value="c">Complete</option>' +
                    '<option value="a">Abandon</option>' +
                    '<option value="t">Transfer</option>' +
                    '<option value="p">Pending</option>' +
                    '</select></td>' +
                    '<td><span class="btn btn-default delete-quick-tool"><i class="fa fa-trash"></i></span></td>' +
                    '</tr>');

                $('#action-ceo' + action.qa_id).val(action.qa_ceo);
                $('#action-who' + action.qa_id).val(action.qa_who);
                $('#action' + action.qa_id).val(action.qa_action);
                $('#action-tag' + action.qa_id).val(action.qa_tag);
                $('#action-cat' + action.qa_id).val(action.qa_cat);
                $('#' + action.qa_id).val(action.qa_tag);
            })
        })
    }

    // Add Project Click Handler.
    $('#add-quick-action').off('click');
    $('#add-quick-action').on('click', function () {

        // AJax call to add new action.
        const add_new_action = makeAjaxCall('common/add_new_action', {
            'when': $('#action-date-picker').val()
        });
        add_new_action.then(function () {
            toastr.success('Action Added.');
            refreshActionPage();
        })
    })

});