$(document).ready(function () {

    // When the dialog is displayed, set the current instance ID.
    $('#edit-agent-profile-dialog').on('show.bs.modal', function (event) {
        $('#view-agent-profile-dialog').modal('hide');

        // const client_id = $(event.relatedTarget).attr('data-id'),
        //     get_agent_info = makeAjaxCall('homebase/get_agent_info', {'agent_id': client_id});
        //
        // get_agent_info.then(function (data) {
        //     const agent_data = data.agent_info[0];
        //
        //     // Set input fields values.
        //     $('#agent-id').val(agent_data.hc_id);
        //     $('#agent-name').val(agent_data.hc_name);
        //     $('#agent-title').val(agent_data.hc_title);
        //     $('#entity-name').val(agent_data.hc_entity_name);
        //     $('#agent-address').val(agent_data.hc_entity_address);
        //     $('#agent-office-phone').val(agent_data.hc_main_phone);
        //     $('#agent-cell-phone').val(agent_data.hc_second_phone);
        //     $('#agent-email').val(agent_data.hc_client_email);
        //     $('#agent-website').val(agent_data.hc_website);
        //     $('#agent-messaging').val(agent_data.hc_messaging);
        //     $('#agent-social-media').val(agent_data.hc_social);
        // })
    });

    $(document).on('click', '#save-agent-info', function () {
        const save_agent_info = makeAjaxCall('common/save_agent_info',
            $("#edit-agent-profile-form").serialize()
        );

        save_agent_info.then(function (data) {
            $('#edit-agent-profile-dialog').modal('hide');
            toastr.success('Owner Information Updated.', 'Success!!');
        })
    });
});
