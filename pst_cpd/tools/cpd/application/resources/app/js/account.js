$(document).ready(function() {

    // Draw the page.
    refreshPage();
});

// Get the page data and display it.
function refreshPage() {

    // Get the list of students
    $.ajax({
        url: "/account/get_accounts",
        dataType: "json",
        type: 'post',
    }).done(function(data) {
        if (data.status == 'failure') {
            toastr.error(data.message);
            return;
        }
        updateAccounts(data.accounts);

    }).fail(function(jqXHR, status) {
        toastr.error("Error");
    });
}

// Update the list of accounts (if there are any).
function updateAccounts(accounts) {

    // Any existing accounts?
    if (accounts.length == 0) {
	$("#no-existing-accounts").show();
	$("#existing-accounts").hide();
	return;
    }

    // There are accounts, show them.
    $("#no-existing-accounts").hide();
    $("#existing-accounts").show();
    $("#account-table tbody").empty();
    $.each(accounts, function(k, account) {

	// Allow the account name to be edited.
	var account_name = '<a href="#" class="editable" id="account_name-' + account.account_id + '" data-type="text" data-name="account.account_name" data-pk="' + account.account_id + '" data-url="/common/update_db_field">' + account.account_name + '</a>'

	$('#account-table').find('tbody')
	    .append($('<tr>')
		    .append($('<td>')
			    .append(account.account_type))
		    .append($('<td>')
			    .append(account_name))
		    .append($('<td>')
			    .append(''))
		    .append($('<td>')
			    .append(''))
		    );
    });
    setEditable();
}
