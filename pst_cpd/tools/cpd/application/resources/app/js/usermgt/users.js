$(document).ready(function() {
    refreshPage();

});

// Update the page
function refreshPage() {
    updateUsers();
}

// Update the list of users.
function updateUsers() {

    // Create the databate - use server side data population.
    user_datatable = $('#user-table').DataTable({
        "pageLength": 100,
	"serverSide": true,
	"responsive": true,
	"ajax":{
	    url: "/usermgt/get_users",
	    type: "post",
	},
	"columns": [
	    {"data": "user_id", "name": "ID"},
	    {"data": "user_avatar", "name": "Avatar"},
	    {"data": "display_name", "name": "Name"},
	    {"data": "user_email", "name": "Email"},
	    {"data": "user_is_admin", "name": "Admin"},
	],
	"rowCallback": function(row, data, index) {
	    $('td', row).eq(0).html('<a href="/user/profile/' + data.user_id + '"><img src="' + data.user_avatar + '" style="max-width:64px"></img></a>');
	    $('td', row).eq(1).html('<a href="/user/profile/' + data.user_id + '">' + data.display_name + "</a>");
	    $('td', row).eq(3).html((data.user_is_admin == '1' ? 'Yes' : 'No'));
	},
	"columnDefs": [
	    {"targets": [0], "visible": false, "searchable": false},
	    {"targets": [1, 4], "visible": true, "searchable": false},
	    {"targets": [2, 3], "visible": true, "searchable": true},
	]
    });

}
