// App configuration
var authEndpoint = 'https://login.microsoftonline.com/common/oauth2/v2.0/authorize?';
var redirectUri = 'https://cpd.powersellingtools.com/tools/dashboard';
var appId = 'cc3e1785-976e-46db-b5c3-04b4837e92f8';
// var redirectUri = 'https://local-pst/tools/dashboard';
// var appId = '65270a50-e775-4b6f-a40d-c993c1fd99ed';
var scopes = 'openid profile User.Read Mail.Read Mail.Send Contacts.Read Calendars.Read MailboxSettings.Read';

// Check for browser support for crypto.getRandomValues
var cryptObj = window.crypto || window.msCrypto; // For IE11

// Helper method to validate token and refresh if needed
function getAccessToken(callback) {
    var now = new Date().getTime();
    var isExpired = now > parseInt(sessionStorage.tokenExpires);
    // Do we have a token already?
    if (sessionStorage.accessToken && !isExpired) {
        // Just return what we have
        if (callback) {
            callback(sessionStorage.accessToken);
        }
    } else {
        // Attempt to do a hidden iframe request
        makeSilentTokenRequest(callback);
    }
}

// Refresh Tokens.
function makeSilentTokenRequest(callback) {
    // Build up a hidden iframe
    var iframe = $('<iframe/>');
    iframe.attr('id', 'auth-iframe');
    iframe.attr('name', 'auth-iframe');
    iframe.appendTo('body');
    iframe.hide();

    iframe.load(function () {
        callback(sessionStorage.accessToken);
    });

    iframe.attr('src', buildAuthUrl() + '&prompt=none&domain_hint=' +
        sessionStorage.userDomainType + '&login_hint=' +
        sessionStorage.userSigninName);
}

// OAUTH FUNCTIONS =============================
function buildAuthUrl() {
    // Generate random values for state and nonce
    sessionStorage.authState = guid();
    sessionStorage.authNonce = guid();

    var authParams = {
        response_type: 'id_token token',
        client_id: appId,
        redirect_uri: redirectUri,
        scope: scopes,
        state: sessionStorage.authState,
        nonce: sessionStorage.authNonce,
        response_mode: 'fragment'
    };

    // $('#outlook-email-client-dialog').modal('show');
    // toastr.warning('Connect to Outlook.', 'Outlook Access Required.');
    return authEndpoint + $.param(authParams);
}

function guid() {
    var buf = new Uint16Array(8);
    cryptObj.getRandomValues(buf);

    function s4(num) {
        var ret = num.toString(16);
        while (ret.length < 4) {
            ret = '0' + ret;
        }
        return ret;
    }

    return s4(buf[0]) + s4(buf[1]) + '-' + s4(buf[2]) + '-' + s4(buf[3]) + '-' +
        s4(buf[4]) + '-' + s4(buf[5]) + s4(buf[6]) + s4(buf[7]);
}