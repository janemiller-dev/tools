var questions_id = [];
$(document).ready(function () {
    $('.tool-menu').css('display', 'none');

    // Fetch the Parameters from URL.
    const searchParams = new URLSearchParams(window.location.search),
        profile_id = searchParams.get('profile_id'),
        client_id = searchParams.get('id'),
        outline_id = searchParams.get('outline_id');
    let count = 0, phase_number = 0,
        phase_value = [
            'History Question',
            'Current Facts Questions',
            'Accomplished Questions',
            'Needed - Problems Questions',
            'Possible - Project Questions'];

    const get_ques = makeAjaxCall('profile/get_ques', {outline: outline_id});

    // Enable Voice Recognition.
    voiceRecognition();

    // Get the Questions List for profile.
    get_ques.then(function (data) {
        // Append questions to Form.
        $('#profile-form')
            .append(
                $.map(data.questions, function (question, index) {
                    questions_id[index] = question.tpq_id;
                    let phase_string = '';
                    count++;

                    if (((count - 1) === 3 * phase_number) || count === 1) {
                        phase_string = '<h3 style="font-weight: bold">' + phase_value[phase_number] + '.' + '</h3>';
                        phase_number++;
                    }

                    let str = question.tpq_content.replace('{{', '').replace('}}', '');
                    str = str + (question.tpq_question !== null ?  ' : ' + question.tpq_question : '');

                    return '<div class="container" style="margin-top: 3vh"><label>'
                        + phase_string  + str + '</label>' +
                        '<div class="input-group">' +
                        '<textarea rows="2" class="form-control" name="' + 'ans' + question.tpq_id +
                        '" type = "text" id="' + 'ans' + question.tpq_id + '" ></textarea>' +
                        '<span class="input-group-addon btn btn-secondary stt"><i class="fa fa-microphone"></i></span>' +
                        '</div></div>'
                }));


        const get_ans = makeAjaxCall('profile/get_ans', {
            question_id: questions_id,
            client_id: client_id,
            profile_id: profile_id
        });

        get_ans.then(function (data) {

            // Remove slashes and add answer for the questions.
            $.each(data.answers, function (index, answer) {
                $('#ans' + answer.tsl_profile_ans_ques_id).val(answer.tsl_profile_ans_content.replace('\\', ''));
            });
            speechFunctionality();
        });
    });


    // Saves the response from user.
    $(document).on('click', '#save_profile', function (e) {
        let data = $('#profile-form').serializeArray();
        data.push({name: 'question_id', value: questions_id});
        data.push({name: 'client_id', value: client_id});
        data.push({name: 'profile_id', value: profile_id});

        // Save the answer.
        const set_ans = makeAjaxCall('profile/set_ans', data);
        set_ans.then(function (data) {
            toastr.success('Response Saved`', 'Success!!');
        });
    });

});