$(document).ready(function () {
    // When the dialog is displayed, set the current instance ID.
    $('#view-presentation-list-dialog').on('show.bs.modal', function (event) {
        console.log('here');

        const get_ppt_list = makeAjaxCall('web/get_ppt_list', {});

        get_ppt_list.then(function (data) {
            $('#presentation-list-table tbody').empty();
            $.each(data.ppt_list, function (index, ppt) {
                $('#presentation-list-table tbody').append('<tr>' +
                    '<td class="text-center">' + (index + 1) + '</td>' +
                    '<td class="text-center">' + ppt.up_name + '</td>' +
                    '<td class="text-center">' +
                    '<span class="fa fa-eye view-ppt" data-id="' + ppt.up_id + '" ' +
                    'data-random="' + ppt.up_random + '"></span></td>' +
                    '<td class="text-center">' +
                    '<span class="fa fa-trash delete-ppt" data-id="' + ppt.up_id + '" ' +
                    'data-random="' + ppt.up_random + '" data-toggle="modal" ' +
                    'data-target="#delete-presentation-dialog" data-name="' + ppt.up_name + '"></span></td>' +
                    '</tr>')
            });
        });
    });

    $('#delete-presentation-dialog').on('show.bs.modal', function (event) {
        $('#view-presentation-list-dialog').modal('hide');
        $('#delete-presentation-name').text($(event.relatedTarget).attr('data-name'));

        $('#delete-presentation-instance').off('click');
        $('#delete-presentation-instance').on('click', function () {
            const delete_presentation = makeAjaxCall('web/delete_presentation', {
                'id': $(event.relatedTarget).attr('data-id'),
                'rand': $(event.relatedTarget).attr('data-random'),
            });

            delete_presentation.then(function (data) {
                toastr.success('Presentation Deleted.', 'Success!!');
                $('#delete-presentation-dialog').modal('hide');
            });
        })
    });

    $('#add-new-presentation').on('click', function () {
        const file_type_allowed = /.\.(ppt|pptx|odt)/i;
        $('#upload-ppt').trigger('click');
        // enableLoader();

        const target = 'upload-ppt',
            upload_ppt = uploadFile(target, {}, file_type_allowed);

        upload_ppt.then(function (data) {
            // $.unblockUI;
            toastr.success('Presentation Uploaded!!', 'Success');
            $('#view-presentation-list-dialog').modal('hide');
        });
        return false;
    });

    $(document).on('click', '.view-ppt', function () {
        $('#view-presentation-list-dialog').modal('hide');
        $('#web-video').addClass('hidden');
        $('#ppt-viewer').attr('src', 'https://sg1-powerpoint.officeapps.live.com/p/PowerPointFrame' +
            '.aspx?PowerPointView=SlideShowView&ui=en-GB&rs=en-GB&WOPISrc=' +
            'http://sg1-view-wopi.wopi.live.net:808/oh/wopi/files/@/wFileId?wFileId' +
            window.location.origin + '/info.pptx');
        $('#ppt-viewer').removeClass('hidden');

        // https://sg1-powerpoint.officeapps.live.com/p/PowerPointFrame.aspx?PowerPointView=SlideShowView&ui=en-GB&rs=en-GB&WOPISrc=http://sg1-view-wopi.wopi.live.net:808/oh/wopi/files/@/wFileId?wFileId=https://cpd.powersellingtools.com/info.pptx
    })
});