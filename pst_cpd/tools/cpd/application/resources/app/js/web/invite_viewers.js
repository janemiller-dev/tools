$(document).ready(function () {
    let client_id, client_email, viewers_id, type, random_name, origin, pdf_id,
        path = window.location.pathname,
        components = path.split('/');

    // When the dialog is displayed, set the current instance ID.
    let user_name = '';

    const get_user_data = makeAjaxCall('user/get_user_data', {});

    get_user_data.then(function (data) {
        user_name = data.user_data[0].name;
    });

    client_id = components[components.length - 1];

    // When the dialog is displayed, set the current instance ID.
    $('#invite-viewers-dialog').on('show.bs.modal', function (event) {
        tinymce.remove("#invite-email-content");

        // Initialize Trix Editor.
        tinymce.init({
            selector: '#invite-email-content',
            branding: false,
            init_instance_callback: function (ed) {
                tinyMCE.DOM.setStyle(tinyMCE.DOM.get('invite-email-content_ifr'), 'height', '55vh');
            }
        }).then(function () {
            let url_to_append =  'https://cpd.powersellingtools.com/tools/web/viewer/' + user_name;

            tinyMCE.activeEditor.setContent('<p>' + user_name +
                ' has invited you to join Web Presentation.' +
                ' Please click on the following link to join meeting.</p><a ' +
                'href="' + url_to_append + '">'
                + url_to_append + '</a>');
        });
    });

    $(document).on('click', '#add-recipient', function () {
        $('.client-email')
            .after('<div class="col-xs-9 col-xs-offset-3" style="padding:0; margin-top:5px !important">' +
                '<input type="email" class="form-control viewers-client-email"></div>');
    });

    // Sends the viewerstional document.
    $(document).on('click', '#send-viewers-button', function (ev) {
        let email_array = [];
        // $('.viewers-client-email').each(function () {
        //     ('' !== $(this).val()) ? email_array.push($(this).val()) : '';
        // });

        getAccessToken(function (accessToken) {
            if (accessToken) {
                // Create a Graph client
                var client = MicrosoftGraph.Client.init({
                    authProvider: (done) => {
                        // Just return the token
                        done(null, accessToken);
                    }
                });

                // Send Emails.
                const sendMail = {
                    message: {
                        subject: $('#viewers-client-subject').val(),
                        body: {
                            contentType: "HTML",
                            content: tinymce.activeEditor.getContent()

                        },
                        toRecipients: [
                            {
                                emailAddress: {
                                    address: $('.viewers-client-email').val()
                                }
                            }
                        ],
                        singleValueExtendedProperties: [
                            {
                                "id": "String {" + appId + "} Name cecp-ea577cd1-cd74-48e4-8127-0255a8e4d4b7",
                                "value": "AST"
                            }
                        ]
                    }
                };

                client.api('/me/sendMail')
                    .post(sendMail)
                    .then(function () {
                        toastr.success('Email Sent.', 'Success!!');
                        $('#invite-viewers-dialog').modal('hide');
                    });

            } else {
                var error = {responseText: 'Could not retrieve access token'};
                console.log(error);
                // callback(null, error);
            }
        });

        // enableLoader();
        ev.preventDefault();
        ev.stopPropagation();
    });
});
