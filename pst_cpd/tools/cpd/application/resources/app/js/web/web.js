$(document).ready(function () {
    // When the dialog is displayed, set the current instance ID.
    let user_name = '';

    const get_user_data = makeAjaxCall('user/get_user_data', {});

    get_user_data.then(function (data) {
        user_name = data.user_data[0].name;
    });

    let connection = new RTCMultiConnection();

    // Create a socket URL.
    connection.socketURL = 'https://rtcmulticonnection.herokuapp.com:443/';

    connection.iceServers = [];
    connection.iceServers = [{
        'urls': [
            'stun:stun.l.google.com:19302',
            'stun:stun1.l.google.com:19302',
            'stun:stun2.l.google.com:19302',
            'stun:stun.l.google.com:19302?transport=udp',
        ]
    }];
    window.getExternalIceServers = true;

    // last step, set TURN url (recommended)
    connection.iceServers.push({
        urls: 'turn:numb.viagenie.ca',
        credential: 'mfsi@123',
        username: 'sumitk@mindfiresolutions.com'
    });

    // Set Connection variables.
    connection.session = {
        screen: true,
        oneway: true
    };

    connection.sdpConstraints.mandatory = {
        OfferToReceiveAudio: true,
        OfferToReceiveVideo: true
    };

    // connection.onstream = function (event) {
    //     document.getElementById('web-video').srcObject = event.stream;
    // };

    connection.onstreamended = function (event) {
        toastr.warning('Presentation Ended.')
    };

    document.getElementById('join-room').onclick = function () {
        this.disabled = true;
        connection.join(user_name);
    };

    // Stop share click handler.
    document.getElementById('stop-share').onclick = function () {
        connection.close();
        connection.getAllParticipants().forEach(function (participant) {
            connection.disconnectWith(participant);
        });
        connection.attachStreams.forEach(function (stream) {
            stream.stop();
        });
    };

    // Check if screen sharing is stopped.
    connection.onPeerStateChanged = function (state) {
        if (state.iceConnectionState.search(/closed|failed/gi) !== -1) {
            window.location.reload();
        }
    };

    document.getElementById('create-room').onclick = function () {

        let roomid = user_name;
        connection.checkPresence(roomid, function (isRoomExist, roomid) {
            isRoomExist ? connection.close(user_name) : '';

            connection.open(user_name);
            window.requestAnimationFrame(() => {
                var divToShare = document.getElementById('screen-share-div');
                html2canvas(divToShare, {
                    grabMouse: false,
                    onrendered: function (canvas) {
                        var screenshot = canvas.toDataURL();
                        document.getElementById('web-video').src = screenshot;
                        var socket = connection.socket;
                        socket.send(screenshot);
                    }
                });
            });

            $('#access-link').html('Link for screen share: ' +
                '<a href="' + window.location.origin + '/tools/web/viewer/' + user_name + '">' +
                window.location.origin + '/tools/web/viewer/' + user_name + '</a>');
        });
        // this.disabled = true;
    };

    // Full Screen Video/Web Presentation.
    $('.web-full-screen').off('click');
    $('.web-full-screen').on('click', function () {
        $('#ppt-viewer').hasClass('hidden') ? '' : document.getElementById('ppt-viewer').requestFullscreen();
        $('#web-video').hasClass('hidden') ? '' : document.getElementById('web-video').requestFullscreen();
    });

    loadPrev();
});