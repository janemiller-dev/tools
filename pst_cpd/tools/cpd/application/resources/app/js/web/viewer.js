$(document).ready(function () {
    // When the dialog is displayed, set the current instance ID.
    let user_name = window.location.pathname.split('/')[4];

    let connection = new RTCMultiConnection();

    // Create a socket URL.
    connection.socketURL = 'https://rtcmulticonnection.herokuapp.com:443/';

    connection.iceServers = [];
    connection.iceServers = [{
        'urls': [
            'stun:stun.l.google.com:19302',
            'stun:stun1.l.google.com:19302',
            'stun:stun2.l.google.com:19302',
            'stun:stun.l.google.com:19302?transport=udp',
        ]
    }];

    // last step, set TURN url (recommended)
    connection.iceServers.push({
        urls: 'turn:numb.viagenie.ca',
        credential: 'mfsi@123',
        username: 'sumitk@mindfiresolutions.com'
    });
    window.getExternalIceServers = true;

    // Set Connection variables.
    connection.session = {
        screen: true,
        oneway: true
    };

    connection.sdpConstraints.mandatory = {
        OfferToReceiveAudio: true,
        OfferToReceiveVideo: true
    };

    connection.onstream = function (event) {
        document.getElementById('web-viewer-video').srcObject = event.stream;
    };

    connection.onstreamended = function (event) {
        toastr.warning('Presentation Ended.');
        window.location.reload();
    };

    document.getElementById('join-room').onclick = function () {
        this.disabled = true;
        connection.join(user_name);
    };

    // Stop share click handler.
    document.getElementById('stop-share').onclick = function () {
        // this.disabled = true;
        connection.close();
        connection.getAllParticipants().forEach(function (participant) {
            connection.disconnectWith(participant);
        });
        connection.attachStreams.forEach(function (stream) {
            stream.stop();
        });
    };

    document.getElementById('create-room').onclick = function () {

        let roomid = user_name;
        connection.checkPresence(roomid, function (isRoomExist, roomid) {
            // console.log(isRoomExist)
            // isRoomExist ? connection.close(user_name) : '';

            connection.open(user_name);

            $('#access-link').html('Link for screen share: ' +
                '<a href="' + window.location.origin + '/tools/web-viewer/viewer/' + user_name + '">' +
                window.location.origin + '/tools/web-viewer/viewer/' + user_name + '</a>');
        });
        // this.disabled = true;
    };


    // Full Screen Video/Web Presentation.
    $('.viewer-full-screen').off('click');
    $('.viewer-full-screen').on('click', function () {
        document.getElementById('web-viewer-video').requestFullscreen();
    });
});