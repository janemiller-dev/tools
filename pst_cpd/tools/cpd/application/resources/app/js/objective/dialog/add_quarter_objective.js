$(document).ready(function ()
{
    // Get the year for the objective.
    var path = window.location.pathname;
    var components = path.split('/');
    var year = components[components.length - 1];
    $('#add-quarter-objective-year').val(year);

    // Validate the add objective form and submit it if it is valid.
    $('#add-objective-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            quarter: {
                validators: {
                    notZero: {
                        message: "The objective quarter is required."
                    },
                    numeric: {
                        message: 'Invalid quarter'
                    }
                }
            },
            floor: {
                validators: {
                    notZero: {
                        message: "Floor is required."
                    }
                }
            },
            target: {
                validators: {
                    notZero: {
                        message: "Target is required."
                    }
                }
            },
            game: {
                validators: {
                    notZero: {
                        message: "Game is required."
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the form add objective
        var $form = $(e.target);
        var fv = $form.data('formValidation');

        // Get the form data and submit it.
        var quarter_objective_data = $form.serialize();
        // quarter_objective_data.push({'value':2018});
        $.ajax({
            url: jsglobals.base_url + 'objective/add_quarter_objective',
            dataType: 'json',
            type: 'post',
            data: quarter_objective_data,
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            // Clear the validation rule and form data.
            fv.resetForm();
            $($form)[0].reset();
            $('#add-quarter-objective-dialog').modal('hide');
            refreshPage();

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });

});

// Updates the years list in add objective dialog.
function updateObjectiveQuarters(quarters)
{
    // Clear the years list first.
    $('#add-quarter-objective-quarter').empty();

    // Check if objective is present for all the years.
    if (quarters.length === 4) {
        $("#add-quarter-objective-quarter").append($('<option value="0">-- Objective for maximum quarters added --</option>'));
    } else {

        for (var i = 1; i < 5; i++) {

            $("#add-quarter-objective-quarter").append($('<option>').attr({value: i,}).append('Quarter '+i));
        }

        // Removes the years from list for which objective is already present.
        $.each(quarters, function(index, quarter){
            var val = quarter.upqo_quarter;
            $("#add-quarter-objective-quarter option[value = " + val + "]").remove();
        });
    }
}

