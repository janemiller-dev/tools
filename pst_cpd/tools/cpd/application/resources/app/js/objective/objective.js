$(document).ready(function ()
{
    refreshPage();
});

// Get objectives and display them
function refreshPage()
{
    var path = window.location.pathname;
    var components = path.split('/');
    var year = components[components.length - 1];
    $('#quarter-year').text(year);
    refreshQuarter(year);
    updateObjective(year);
}

quarter_objective_table = $('#quarter-objective-table').DataTable();

// Update the Objective table
function updateObjective(year)
{
    // Destroy the table so that can be rebuilt
    quarter_objective_table.destroy();

    quarter_objective_table = $('#quarter-objective-table').DataTable({
        "pageLength": 50,
        "serverSide": true,
        "processing": true,
        "paging":   false,
        "searching": false,
        "columnDefs": [
            {
                "targets": [0, 1, 2, 3],
                "orderable": false
            }],
        "ajax": {
            "url": jsglobals.base_url + "objective/get_quarter_objective",
            "data": {
                "year": year
            },
            "type": "post"
        },
        "columns":
            [
                {"data": "quarter", "name": "Quarter"},
                {"data": "floor", "name": "Floor"},
                {"data": "target", "name": "Target"},
                {"data": "game", "name": "Game"}
            ],
        "rowCallback": function (row, data, index) {

            // Adding editable class and appropriate data attributes.
            $('td', row).eq(1).addClass("editable")
                .attr({
                    'data-url': jsglobals.base_url + 'objective/update_quarter_objective',
                    'data-pk': data.id,
                    'data-type': 'text',
                    'data-name': 'floor',
                    'title': "New Floor"
                });

            $('td', row).eq(2).addClass("editable")
                .attr({
                    'data-url': jsglobals.base_url + 'objective/update_quarter_objective',
                    'data-pk': data.id,
                    'data-type': 'text',
                    'data-name': 'target',
                    'title': "New Target"
                });

            $('td', row).eq(3).addClass("editable")
                .attr({
                    'data-url': jsglobals.base_url + 'objective/update_quarter_objective',
                    'data-pk': data.id,
                    'data-type': 'text',
                    'data-name': 'game',
                    'title': "New Game"
                });

        },

        // Set fields as Editable after initialization.
        "initComplete": function () {
            setEditable();
            if (quarter_objective_table.data().count() === 0) {
                $('#no-objective-instances').show();
            } else
                $('#no-objective-instances').hide();
        }
    });

}
