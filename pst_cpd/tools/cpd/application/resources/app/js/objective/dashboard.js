$(document).ready(function ()
{
    refreshPage();
});

// Get objectives and display them
function refreshPage()
{
    refreshYear();
    updateObjective();
}

objective_table = $('#objective-table').DataTable();

// Update the Objective table
function updateObjective()
{
    // Destroy the table so that can be rebuilt
    objective_table.destroy();

    objective_table = $('#objective-table').DataTable({
        "pageLength": 50,
        "serverSide": true,
        "processing": true,
        "paging":   false,
        "searching": false,
        "columnDefs": [
            {
                "targets": [0, 1, 2, 3],
                "orderable": false
            }],
        "ajax": {
            "url": jsglobals.base_url + "objective/get_objective",
            "type": "post"
        },
        "columns":
        [
        {"data": "year", "name": "Year"},
        {"data": "floor", "name": "Floor"},
        {"data": "target", "name": "Target"},
        {"data": "game", "name": "Game"}
        ],
        "rowCallback": function (row, data, index) {

            // Adding link in the year column to add/edit objective for a quarter.
            $('td', row).eq(0).html('<a href="' + jsglobals.base_url + 'objective/objective/' + data.year + '">' + data.year + "</a>");

            // Adding editable class and appropriate data attributes.
            $('td', row).eq(1).addClass("editable")
                .attr({
                    'data-url': jsglobals.base_url + 'objective/update_objective',
                    'data-pk': data.year,
                    'data-type': 'text',
                    'data-name': 'floor',
                    'title': "New Floor"
                });

            $('td', row).eq(2).addClass("editable")
                .attr({
                    'data-url': jsglobals.base_url + 'objective/update_objective',
                    'data-pk': data.year,
                    'data-type': 'text',
                    'data-name': 'target',
                    'title': "New Target"
                });

            $('td', row).eq(3).addClass("editable")
                .attr({
                    'data-url': jsglobals.base_url + 'objective/update_objective',
                    'data-pk': data.year,
                    'data-type': 'text',
                    'data-name': 'game',
                    'title': "New Game"
                });

        },

        // Set fields as Editable after initialization.
        "initComplete": function () {
            setEditable();
            if (objective_table.data().count() === 0) {
                $('#no-objective-instances').show();
            } else
                $('#no-objective-instances').hide();
        }
    });

}
