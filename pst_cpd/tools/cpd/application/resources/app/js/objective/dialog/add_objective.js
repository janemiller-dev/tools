$(document).ready(function ()
{
    // Validate the add objective form and submit it if it is valid.
    $('#add-objective-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            year: {
                validators: {
                    notZero: {
                        message: "The objective year is required."
                    },
                    numeric: {
                        message: 'Invalid Year'
                    }
                }
            },
            floor: {
                validators: {
                    notZero: {
                        message: "Floor is required."
                    }
                }
            },
            target: {
                validators: {
                    notZero: {
                        message: "Target is required."
                    }
                }
            },
            game: {
                validators: {
                    notZero: {
                        message: "Game is required."
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the form add objective
        var $form = $(e.target);
        var fv = $form.data('formValidation');

        // Get the form data and submit it.
        var objective_data = $form.serialize();
        $.ajax({
            url: jsglobals.base_url + 'objective/add_objective',
            dataType: 'json',
            type: 'post',
            data: objective_data,
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            // Clear the validation rule and form data.
            fv.resetForm();
            $($form)[0].reset();
            $('#add-objective-dialog').modal('hide');
            refreshPage();

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });

});

// Updates the years list in add objective dialog.
function updateObjectiveYears(years)
{
    // Clear the years list first.
    $('#add-objective-year').empty();

    // Check if objective is present for all the years.
    if (years.length === 4) {
        $("#add-objective-year").append($('<option value="0">-- Objective for maximum years added --</option>'));
    } else {
        updateYears('#add-objective-year');

        // Removes the years from list for which objective is already present.
        $.each(years, function(index, year){
            var val = year.upo_year;
            $("#add-objective-year option[value = " + val + "]").remove();
        });
    }
}

