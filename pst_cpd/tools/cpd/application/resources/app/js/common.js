let universal_components, recognition, stt_selector;

$(document).ready(function () {
    // Append close button to popover title.
    $(document).on('show.bs.popover', '[data-toggle=popover]', function () {
        const title = $(this).data('bs.popover').options.title || $(this).attr('data-original-title');
        // Check if close button is already present.
        (-1 === title.indexOf('class=\'close\'')) ?
            $(this).attr('data-original-title',
                (undefined !== title ? title : '') +
                '<span id=\'close\' class=\'close\' ' +
                'onclick=\'$(this).popover(&quot;hide&quot;);\'>&times;</span>') : '';
    });

    $(document).on('shown.bs.popover', '[data-toggle=popover]', function () {
        useTooltip();
    });

    // Hide any already open popover, when modal is shown.
    $(document).on('shown.bs.modal', '', function () {
        $('.popover').hide()
    });

    useTooltip();
    collaborate();

    // Traverse Between different components.
    // $(document).off('mousedown', '.back-button');
    // $(document).on('mousedown', '.back-button', function (e) {
    //     timer = setTimeout(function () {
    //         e.preventDefault();
    //         e.stopPropagation();
    //         toastr.info("Select clients to delete!!");
    //
    //
    //         //
    //         // $('.delete-client').addClass('hidden');
    //         // $('.delete-client-check').removeClass('hidden');
    //         // $('#delete-check, #remove-multi-delete').removeClass('hidden')
    //
    //     }, 1000);
    // }).on("mouseup mouseleave", function () {
    //     clearTimeout(timer);
    // });

    // Quick tools change event handler.
    $(document).on('change', '.quick-tool-field', function () {
        if ($(this).hasClass('quick-tool-who') && ('0' === $(this).val())) {
            $('#view-quick-action-dialog').modal('hide');
            $('#view-assignee-dialog').modal('show');
        } else {
            makeAjaxCall('common/update_quick_field', {
                id: $(this).parent().parent().attr('data-id'),
                col: $(this).attr('data-col'),
                table: $(this).parent().parent().attr('data-table'),
                val: $(this).val(),
                key: $(this).parent().parent().attr('data-key')
            });
        }
    });

    // Quick Action delete handler.
    $(document).on('click', '.delete-quick-tool', function () {
        let table = $(this).parent().parent().attr('data-table'),
            id = $(this).parent().parent().attr('data-id'),
            key = $(this).parent().parent().attr('data-key');

        toastr.error("<br /><button type='button' id='confirmationYes' class='btn btn-default'>Yes</button>" +
            "<button type='button' id='confirmationNo' class='btn btn-default' style='margin-left: 5px'>No</button>",
            'Are you sure you want to delete this quick tool instance?',
            {
                timeOut: 0,
                extendedTimeOut: 0,
                preventDuplicates: true,
                tapToDismiss: false,
                closeButton: true,
                allowHtml: true,
                onShown: function (toast) {
                    $("#confirmationYes").off('click');
                    $("#confirmationYes").click(function () {

                        const delete_quick_tool = makeAjaxCall('common/delete_quick_tool', {
                            id: id,
                            table: table,
                            key: key
                        });

                        delete_quick_tool.then(function () {
                            // Close the Toastr.
                            $('.toast-close-button').trigger('click');
                            $('#view-' + table.replace(/_/g, "-") + '-dialog').modal('hide');
                        });
                    });

                    $('#confirmationNo').click(function () {
                        $('.toast-close-button').trigger('click');
                    })
                }
            });
    });

    // Hides pop over whenever outside popover us clicked.
    $('body').on('click', function (e) {
        $('[data-toggle=popover]').each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0
                && $('.popover').has(e.target).length === 0) {
                (($(this).popover('hide').data('bs.popover') || {}).inState || {}).click = false;

                if ($(this).hasClass('amount_button') && undefined !== $('.amount_button').data('bs.popover')) {
                    $(this).popover('destroy');
                }
            }
        });

        // $('.coming-soon-tool').each(function () {
        //     if (!$(this).is(e.target) && $(this).has(e.target).length === 0
        //         && $('.popover').has(e.target).length === 0) {
        //         (($(this).popover('hide').data('bs.popover') || {}).inState || {}).click = false;
        //
        //         if ($(this).hasClass('amount_button'))
        //             $(this).popover('destroy');
        //     }
        // });

        // Close Dummy Button Popover.
        $('.dummy-component').each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0
                && $('.popover').has(e.target).length === 0) {
                (($(this).popover('hide').data('bs.popover') || {}).inState || {}).click = false;

                if ($(this).hasClass('amount_button')) {
                    $(this).popover('destroy');
                }
            }
        });
    });

    $(document).off('click', '#close');
    $(document).on('click', '#close', function () {
        $(this).parent().closest('.popover').popover('hide').data('bs.popover').inState.click = false;
    });

    // Delete archive handler.
    $('#delete-archive-dialog').on('show.bs.modal', function (event) {
        $('#view-promo-archive-dialog').modal('hide');
        const id = $(event.relatedTarget).attr('data-id'),
            name = $(event.relatedTarget).attr('data-name');


        $('#archive-name').text(name);
        $(document).off('click', '#delete-archive-button');
        $(document).on('click', '#delete-archive-button', function () {
            const delete_archive = makeAjaxCall('tsl/delete_archive', {
                'id': id
            });

            delete_archive.then(function (data) {
                toastr.success('Archive Deleted.', 'Success!');
                $('#delete-archive-dialog').modal('hide');
            })
        });
    });

    // Delete collaboration modal show handler.
    $('#delete-collab-dialog').on('show.bs.modal', function (event) {
        const id = $(event.relatedTarget).attr('data-id'),
            action = $(event.relatedTarget).attr('data-action'),
            sender = $(event.relatedTarget).attr('data-sender');

        $('#collab-name').text(sender);
        $('#collab-format').text(action);

        // Delete collaboration click handler.
        $(document).off('click', '#delete-collab-button');
        $(document).on('click', '#delete-collab-button', function () {
            const delete_collab = makeAjaxCall('cpdv1/delete_collab', {
                id: id
            });

            delete_collab.then(function (data) {
                toastr.success('Collboration Message Deleted.', 'Success!!');
                $('#delete-collab-dialog').modal('hide');
                $('.collab_row').remove();
                $('.collaborator_row').remove();
                setupCollab();
            })
        })
    });

    // Add to alert click handler.
    $(document).off('click', '.add-to-alert');
    $(document).on('click', '.add-to-alert', function () {
        const add_alert = makeAjaxCall('common/add_to_alert', {
            type: $(this).attr('data-type'),
            id: $(this).attr('data-id'),
            client_id: client_id,
            set: window.location.pathname.split('/')[2],
            datetime: moment().format("YYYY-MM-DD hh:mm:ss")
        })

        add_alert.then(function (data) {
            toastr.success('Added to alert list.', 'Success!!');
        })
    })

    // Change event handler for TSL dropdown.
    // $(document).on('change', '.tsl_dropdown', function () {
    //
    //     if (0 !== parseInt($(this).val())) {
    //         const update_dropdown = makeAjaxCall('tsl/update_dashboard_dropdown', {
    //             name: $(this).attr('data-name'),
    //             id: $(this).attr('data-id'),
    //             val: $(this).val(),
    //             key: $(this).attr('data-key'),
    //             table: $(this).attr('data-table-name')
    //         });
    //     }
    // });

    $(document).on('change', '.profile-product-type', function (e) {
        if ($(this).val() === '-1') {
            e.preventDefault();
            e.stopPropagation();

            $('#modal-id').val('user-profile-dialog');
            $('#add-product-dialog').modal('show')
        }
    });


    disableComponent();
    $('[data-toggle="popover"]').popover();


    // Refresh Page on tab change.
    // $(window).on("blur focus", function(e) {
    //     var prevType = $(this).data("prevType");
    //
    //     if (prevType != e.type) {   //  reduce double fire issues
    //         switch (e.type) {
    //             case "blur":
    //                 console.log('blurred');
    //                 // do work
    //                 break;
    //             case "focus":
    //                 refreshPage();
    //                 console.log('focussed');
    //                 // do work
    //                 break;
    //         }
    //     }
    //
    //     $(this).data("prevType", e.type);
    // });

    // $(document).on('click', '.coming-soon-tool', function (e) {
    //
    //     e.preventDefault();
    //     e.stopPropagation();
    //
    //     $('.coming-soon-tool').popover({
    //         selector: '[rel=popover]',
    //         trigger: 'hover',
    //         container: 'body',
    //         placement: 'top',
    //         html: true,
    //         title: 'Coming Soon!!',
    //         content: function () {
    //             return '<p>This component will be available soon.</p>';
    //         }
    //     });
    //
    //     $(this).popover('show');
    // });

    // Handle the finalize and allow buttons.
    $('#toggle-tips-button').click(function () {
        // Toggle the tips shown
        tips_shown = !tips_shown;

        if (tips_shown) {
            $('#toggle-tips-text').html('Hide Tips');
            $('.has-popover').show();
        }
        else {
            $('#toggle-tips-text').html('Show Tips');
            $('.has-popover').hide();
        }
    });

    // Dummy Component Popover.
    $(document).on('click', '.dummy-component', function (e) {

        e.preventDefault();
        e.stopPropagation();

        $('.dummy-component').popover({
            selector: '[rel=popover]',
            trigger: 'hover',
            container: 'body',
            placement: 'top',
            html: true,
            title: 'Dummy Button!!',
            content: function () {
                return '<p>This is Dummy button. Please Add new clients using Add Clients Button to access full features.</p>';
            }
        });

        $(this).popover('show');
    });

    // Handle a change in product.
    const current_product_selector = $('#current-product');
    current_product_selector.change(function () {

        if (current_product_selector.val() === null) {
            return false;
        } else {
            $.ajax({
                url: jsglobals.base_url + 'industry/set_active_product',
                dataType: 'json',
                type: 'post',
                data: {
                    'id': current_product_selector.val(),
                    'is_group': current_product_selector.find('option:selected').attr('data-group')
                },
            }).done(function (data) {

                if (data.status != 'success') {
                    toastr.error(data.message);
                    return;
                }
                else if (data.status == 'redirect') {
                    window.location.href = data.redirect;
                    return;
                }
                refreshProducts();

                (typeof refreshPage !== 'undefined' && $.isFunction(refreshPage)) ?
                    refreshPage() : '';
            }).fail(function (jqXHR, textStatus) {
                toastr.error("Request failed: " + textStatus);
            }).always(function () {
            });
        }
    });

    $(document).on('click', '.collab-button', function () {
        // window.location = jsglobals.base_url + 'collab';
        const path = window.location.pathname,
            components = path.split('/');

        client_id = components[components.length - 1];

        let content = getComponentPopover('collab-button', 'Marketing Campaign Overlay.',
            '<th>Promos</th><th>Messages</th>' +
            '<th style="white-space: nowrap">Call Scripts</th>\n' +
            '<th>Confirms<s/th>\n' +
            '<th>Stories</th>\n', '(component.uc_type === \'promo\') && (component.uc_tool === \'tsl\')',
            'tsl', client_id);

        $(this).attr('data-content', content);
        $(this).attr('data-placement', 'bottom');
        $(this).popover('show');
    });

    //Hide initial Navbar.
    $(document).on('click', '#hide_cap', function () {
        $('#cap').hide();
    })
});

// Invoke the double scroll on the specified object.
function enableDoubleScroll(element) {
    $(element).doubleScroll();
}

function enableDatePicker() {
    // Set the Format fr date picker.
    $('.when-date-picker').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
    });
}

// Enables Ajax Loader.
function enableLoader() {

    const loader_src = "../../resources/app/media/loader/loader.gif";

    // const src = '<?php echo $this->basepath; ?>../thumbnails/loader.gif';
    $.blockUI({
        message: '<h3><img src=' + loader_src + ' style="height: 20px"/> Sending Email...</h3>',
        baseZ: 2000,
        css: {width: '20%', left: '44%'}

    });
    $('.blockOverlay').click($.unblockUI);

}

function add_client_loader() {
    // Enables Ajax Loader.
    const loader_src = "../../resources/app/media/loader/loader.gif";

    // const src = '<?php echo $this->basepath; ?>../thumbnails/loader.gif';
    $.blockUI({
        message: '<h3><img src=' + loader_src + ' style="height: 20px"/> Please wait...</h3>',
        baseZ: 2000,
        css: {width: '20%', left: '44%'}

    });
    $('.blockOverlay').click($.unblockUI);
}

// Get and display the user's professions.
function refreshProducts(callback) {
    const current_product = $('#current-product');

    $.ajax({
        url: jsglobals.base_url + 'industry/get_my_products',
        dataType: 'json',
        type: 'post',
        error: ajaxError
    }).done(function (data) {

        if (data.status != 'success') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
            window.location.href = data.redirect;
            return;
        }

        var products_array = [];
        $.each(data.products, function (index, product) {

            // (index);
            if (index === 'groups') {
                $.each(product, function (index, group) {
                    products_array[group.usp_seq] = [];
                    products_array[group.usp_seq]['name'] = group.upg_name;
                    products_array[group.usp_seq]['id'] = group.upg_id;
                    products_array[group.usp_seq]['is_group'] = 'group';
                })
            } else if (index === 'products') {
                $.each(product, function (index, product) {
                    products_array[product.usp_seq] = [];
                    products_array[product.usp_seq]['name'] = product.u_product_name;
                    products_array[product.usp_seq]['id'] = product.u_product_id;
                })
            }
        });

        // Display each of the user's profession.
        current_product.empty();
        var current = -1,
            active_id = (undefined !== data.products.active_id[0]) ? data.products.active_id[0].uap_product_id : 0,
            text_to_append = ['Primary Product: ', 'Second Product: ', 'Third Product: ', 'Fourth Product: ',
                'Fifth Product: ', 'Sixth Product: ', 'Seventh Product: ',
                'Eighth Product: ', 'Ninth Product: ', 'Tenth Product: ', 'Eleventh Product: ',
                'Twelfth Product: ', 'Thirteenth Product: ', 'Fourteenth Product: ', 'Fifteenth Product: ',
                'Sixteenth Product: ', 'Seventeenth Product: ', 'Eighteenth Product: ',
                'Nineteenth Product: ', 'Twentieth Product: ', 'Twenty First Product: '],
            selected = '';

        // options_to_append = [];

        current_product.append($('<option disabled value="-1"> --Select Product Type--</option>'));

        products_array = products_array.filter(function (product) {
            return product !== undefined;
        });

        $.each(products_array, function (index, product) {

            let option_style = '';
            (product.is_group === 'group')
                ?
                option_style = 'style="font-weight: bold; font-style: italic;"' : '';
            current_product.append(
                $('<option value="' + product.id + '" ' + selected +
                    ' data-group="' + product.is_group + '"' + option_style + '>'
                    + text_to_append[index] + product.name + '</option>'));
        });

        // Set Products in sorted order.
        // $.each(data.products.data, function (k, product) {
        //
        //     if (product.usp_secondary_product === product.u_product_id) {
        //         options_to_append[1] = product;
        //     } else if (product.usp_primary_product === product.u_product_id) {
        //         selected = product.u_product_id;
        //         options_to_append[0] = product;
        //     } else {
        //         options_to_append[2] = product;
        //     }
        // });
        //
        // // Append Options product drop down.
        // $.each(options_to_append, function (index, product) {
        //
        //     (active_id == product.u_product_id) ? current = product.u_product_id : '';
        //
        //     current_product.append(
        //         $('<option value="' + product.u_product_id + '" ' + selected + '>' +
        //             text_to_append[index] + product.u_product_name + ' (' + product.u_product_group_short + ') ' + '</option>'));
        // });


        current = active_id;
        current_product.val(current);

        selected = products_array[0].id;

        // Check if none product type is active.
        if (null === current_product.val()) {
            current_product.val(selected).trigger('change');
        }

        // Check if callback is defined.
        ((callback !== undefined) && callback !== '') ? callback() : '';

    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
        // Check if current product is selected or not.
        if (current_product.val() === null) {
            if (window.location.origin + "/tools/dashboard" !== window.location.href) {
                window.location.href = jsglobals.base_url;
            }
        }
    });
}

// Tips shown has to be global.
var tips_shown = true;

/**
 * Common functions.
 */

// If stuff is not editable, clear the editable class
function clearEditable() {
    $('.editable').addClass('waseditable');
    $('.editable').removeClass('editable');
}

// Set the editable class as editable.
function setEditable(placement, emptytext) {

    // Set the defaults
    if (typeof placement === 'undefined')
        placement = 'right';

    if (typeof emptytext === 'undefined')
        emptytext = 'Not set';

    $('.editable').editable({
        container: 'body',
        placement: placement,
        emptytext: emptytext,
        validate: function (value) {
            // Check if the max length of the editable field is set.
            // if (!isNaN($(this).data('name'))) {
            //     if (value.length > $(this).data('name'))
            //         return "Length cannot be more than " + $(this).data('name');
            // }

            if (value === '') {
                (function () {
                    var original = $.fn.editableutils.setCursorPosition;
                    $.fn.editableutils.setCursorPosition = function () {
                        try {
                            original.apply(this, Array.prototype.slice.call(arguments));
                        } catch (e) {
                        }
                    };
                })();
                return 'Cannot be null';
            }
        },
        success: function () {
            if (window.location.pathname.split('/')[2] === 'tgd')
                refreshPage();
        }
    });
}

// Fetch product types from DB.
function get_all_products() {
    try {
        // Create a new promise object.
        var product_promise = new Promise(function (resolve, reject) {
            const get_products = makeAjaxCall('industry/get_all_products');

            get_products.then(function (data) {
                resolve(data.products);
                return;
            }).catch(e => reject(e));
        });
    } catch (e) {
        return false;
    }
    return product_promise;
}

// Get the list of years for which objective is already present.
function refreshYear() {

    // Call to get the years for which objective is already present.
    $.ajax({
        url: jsglobals.base_url + 'objective/get_used_years',
        dataType: 'json',
        type: 'post',
        error: ajaxError
    }).done(function (data) {

        if (data.status != 'success') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
            window.location.href = data.redirect;
            return;
        }

        // Updates the years in add objective dialog.
        updateObjectiveYears(data.years);

    }).fail(function () {
        toastr.error("Server communication error. Please try again.");
    }).always(function () {
    });
}

//Get the list of quarters fir which list is already presesnt.
function refreshQuarter(year) {

    // Call to get the quarter for which objective is already present.
    $.ajax({
        url: jsglobals.base_url + 'objective/get_used_quarters',
        dataType: 'json',
        type: 'post',
        data: {
            'year': year
        },
        error: ajaxError
    }).done(function (data) {

        if (data.status != 'success') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
            window.location.href = data.redirect;
            return;
        }

        // Updates the years in add objective dialog.
        updateObjectiveQuarters(data.quarters);

    }).fail(function () {
        toastr.error("Server communication error. Please try again.");
    }).always(function () {
    });

}

// Opt-in to the tool tips.
function useTooltip() {
    $('.has-tooltip').tooltip().show();
}

// Opt-in to the popover tips.
function usePopover() {
    $('.has-popover').popover();

    if (tips_shown) {
        $('#toggle-tips-text').html('Hide Tips');
        $('.has-popover').show();
    }
    else {
        $('#toggle-tips-text').html('Show Tips');
        $('.has-popover').hide();
    }
}

// Ellipsisify a string.
function ellipsify(str, len) {
    // Cast numbers
    str = str.toString();

    // Already short enough?
    if (str.length <= len)
        return str;

    var shortened = str.substr(0, len - 1);

    return '<span class="ellipsis" title="' + esc(str) + '">' + shortened + '&#8230;</span>';
};

// Escape a string.
function esc(t) {
    return t
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;');
};

/*
* Function to show ajax error
*
* jqXHR - error object
*
* */
function ajaxError(jqXHR, textStatus, errorThrown) {
    console.log(JSON.stringify(jqXHR));
    console.log(jqXHR.responseText)
    console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
};

/*
* Function to reset width of DMD tables.
*
*/
function resetWidth(tObj) {
    setTimeout(function () {
        try {
            $(tObj.table().container()).find('table').css("width", "100%");
        } catch (err) {
            console.log(err);
        }
    }, 50)
}

/*
* Update the year for objectives.
*
*/

function updateYears(element) {

    // Clear the old options.
    $(element).empty();

    // Allow the current year and the next year.
    var date = moment();

    // Only 4 years allowed.
    for (var i = 0; i < 4; i++) {
        var year = date.year();

        $(element).append($('<option>').attr({value: year,}).append(year));
        date.add(1, 'year');
    }

    $(element).val(moment().year());
}

/*
* Function to increase the height of the textarea automatically.
*
*/
function auto_grow(element) {
    element.style.height = "5px";
    element.style.height = (element.scrollHeight) + "px";
}

// Validate the field, not to be zero
FormValidation.Validator.notZero = {
    validate: function (validator, $field, options) {
        var value = $field.val();
        return value != 0;
    }
};

// Validate the field, not to be zero
FormValidation.Validator.notNegative = {
    validate: function (validator, $field, options) {
        var value = $field.val();
        return value > 0;
    }
};

function setDateTimePicker() {
    $('.datetimepicker').datetimepicker({
        format: 'DD/MM HH:mm',
    });
}

// Add indent and outdent buttons to trix editor
function trixButtons() {

    const trix_button_block_tools_selector = $('.trix-button-group--block-tools');
    $('.trix-button-group--text-tools, ' +
        '.trix-button--icon-decrease-nesting-level, ' +
        '.trix-button--icon-increase-nesting-level').css('display', 'none');

    $('.trix-button-group--history-tools').css('margin-left', 0);

    // $('.trix-button-group--history-tools').css('display', 'none');
    trix_button_block_tools_selector.css('width', '100%');

    const buttonIndent = '<button type="button" class="trix-button trix-button--icon' +
        ' trix-button--icon-increase-nesting-level trix-extra-button" data-format="indent">Indent</button>',

        buttonOutdent = '<button type="button" class="trix-button trix-button--icon' +
            ' trix-button--icon-decrease-nesting-level trix-extra-button" data-format="outdent">Outdent</button>',

        buttonBold = '<button type="button" class="trix-button trix-button--icon' +
            ' trix-button--icon-bold trix-extra-button" data-trix-attribute="bold"' +
            ' data-trix-key="b"></button>',

        buttonItalic = '<button type="button" class="trix-button trix-button--icon' +
            ' trix-button--icon-italic  trix-extra-button" data-trix-attribute="italic" data-trix-key="i"></button>';

    $('.trix-extra-button').remove();
    $('.trix-info').html('');
    trix_button_block_tools_selector.css('margin-left', 0);

    // Append extra buttons to Trix editor.
    trix_button_block_tools_selector.append(buttonIndent);
    trix_button_block_tools_selector.append(buttonOutdent);
    trix_button_block_tools_selector.prepend(buttonItalic);
    trix_button_block_tools_selector.prepend(buttonBold);

    // $('<div class="trix-info"><i><ul><li>Place Steps Inside {{ }}</li>' +
    //     '<li>Place Substeps Inside (( ))</li>' +
    //     '<li>Place Headings Inside [[ ]]</li>' +
    //     '<ul></ul></i></div>').insertAfter($('.trix-button-row'));


    const trix = document.getElementById('trix-editor'),
        dataFormats = document.querySelectorAll('[data-format]');

    function formatText(format) {
        var {editor} = trix;
        const {currentAttributes} = editor.composition;

        if (format === 'indent') {
            if (!currentAttributes['quote']) {
                editor.activateAttribute('quote')
            } else {
                editor.increaseIndentationLevel()
            }
            return
        } else if (format === 'outdent') {
            editor.decreaseIndentationLevel()
            return
        }

        if (currentAttributes[format]) {
            editor.deactivateAttribute(format)
        } else {
            editor.activateAttribute(format)
        }
    }

    function handleMouseDown(e) {
        e.preventDefault()
        formatText(this.getAttribute('data-format'))
    }

    for (let i = dataFormats.length; i--;) {
        dataFormats[i].addEventListener('mousedown', handleMouseDown)
    }

    $('trix-toolbar').css('display', 'none');
}

// jQuery datetie picker, integrated with momentJs
$.datetimepicker.setDateFormatter('moment');


$('#quick-calculator').click(function () {
    $('#calculator').toggle();
});


// Speech to Text functionality.
function voiceRecognition() {
    try {
        var SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
        recognition = new SpeechRecognition();
    } catch (e) {
        toastr.error('Please use Google Chrome for voice typing.', 'Browser Not Supported!!');
        return;
    }

    stt_selector = $('.stt');

    // recognition.interimResults = true;
    recognition.continuous = true;
    recognition.onstart = function () {
        toastr.success('Voice recognition activated. Try speaking into the microphone.');
    };

    recognition.onspeechend = function () {
        toastr.success('You were quiet for a while so voice recognition turned itself off.');
        stt_selector.removeClass('btn-success');
        stt_selector.addClass('btn-secondary');
        stt_selector.find('i').removeClass('fa-microphone-slash');
        stt_selector.find('i').addClass('fa-microphone');
    };

    recognition.onerror = function (event) {

        if (event.error == 'no-speech') {
            stt_selector.removeClass('btn-success');
            stt_selector.addClass('btn-secondary');
            stt_selector.find('i').removeClass('fa-microphone-slash');
            stt_selector.find('i').addClass('fa-microphone');
            toastr.error('No speech was detected. Try again.');
        }
    };

    recognition.onresult = function (event) {

        // event is a SpeechRecognitionEvent object.
        // It holds all the lines we have captured so far.
        // We only need the current one.
        var current = event.resultIndex;

        // Get a transcript of what was said.
        var transcript = event.results[current][0].transcript;

        // Add the current transcript to the contents of textarea.
        const active_text_area_selector = $('#' + active_textarea);
        active_text_area_selector.val(active_text_area_selector.val() + transcript + ' ');
    }
}

// Dispaly add new location/criteria popup.
function add_location_criteria() {
    // Check if Add new criteria or Add new location option is selected.
    $(document).off('change', '.client-criteria, .client-location');
    $(document).on('change', '.client-criteria', function () {
        const that = $(this);
        // Check if the add new criteria option is selected.
        if ($(this).val() === '-1') {
            $('#view-tsl-criteria-dialog').modal('show');
            $(that).val('').trigger('change');
        }
    });

    $(document).on('change', '.client-location', function (e) {
        if ($(this).val() === '-1') {
            e.preventDefault();
            e.stopPropagation();
            $(this).prop("selectedIndex", 0);
            $('#view-target-audience-dialog').modal('show')
        }
    });

    $("#add-client-form").formcache("outputCache");
}


// Converts the text to speech using speechanalysis.
function speechFunctionality() {

    // Enable click functionality for TTS.
    $(document).off('click', '.tts')
    $(document).on('click', '.tts', function (e) {
        let text = [],
            input = $(this).siblings('textarea').val();

        // Check if there's any ongoing TTS.
        if (!speechSynthesis.speaking) {
            if ("" !== input) {

                while (input.length > 230) {
                    let last_index = input.indexOf(' ', 220);
                    const ip = input.substr(0, last_index);
                    input = input.substr(last_index + 1);
                    text.push(ip);
                }
                text.push(input);
            } else {
                text.push('No Text to read. Please use microphone icon on right or start typing text!!')
            }
            readOutLoud(text);
        } else {
            speechSynthesis.cancel();
        }
    });


    stt_selector = $('.stt');
    $(document).off('click', '.stt');
    $(document).on('click', '.stt', function () {
        try {
            if ($(this).hasClass('btn-success')) {
                recognition.stop();

                stt_selector.removeClass('btn-success');
                stt_selector.find('i').removeClass('fa-microphone-slash');
                $(this).addClass('btn-secondary');
                $(this).find('i').addClass('fa-microphone');

            } else {
                recognition.start();

                stt_selector.removeClass('btn-success');
                stt_selector.find('i').removeClass('fa-microphone-slash');
                $(this).addClass('btn-success');
                $(this).removeClass('btn-secondary');
                $(this).find('i').addClass('fa-microphone-slash');
            }
        } catch (e) {
            toastr.success('Voice Recognition already activated. ');
            return;
        }
        active_textarea = $(this).parent().find('textarea').attr('id');
    });

}

// Reads out Text using Speech to text.
function readOutLoud(messages) {
    $.each(messages, function (index, message) {
        let voice_tone = 3;

        //Check if it's a collaboration message.
        if (message.startsWith('[[c]]')) {
            voice_tone = 7;
            message = message.replace('[[c]]', '');
        }

        let speech = new SpeechSynthesisUtterance(message);
        // Set the text and voice attributes.
        // speech.text = message;


        speech.volume = 1;
        let voices = window.speechSynthesis.getVoices();
        speech.voice = voices[voice_tone];
        speech.voiceURI = 'native';
        speech.rate = 1;
        speech.lang = 'en-us';
        speech.pitch = 1;

        speech.onend = function (e) {
            console.log('Finished in ' + event.elapsedTime + ' seconds.');
        };
        speechSynthesis.speak(speech);
    });
}

function getComponents() {
    const path = window.location.pathname,
        components = path.split('/'),
        origin = ('cpdv1' === components[2]) ? 'cpdv1' : 'tsl';

    const get_components = makeAjaxCall('code/get_components', {'origin': origin});
    get_components.then(function (data) {
        universal_components = data;
    });
}

function loadPrev() {
    $('.back-button').click(function () {
        window.history.back();
    });
}

function disableComponent() {
// Disables web button.
    $('.component_button').on('shown.bs.popover', function () {
        $('#WEB').addClass('disabled');
    })
}

// Updates active DMD/TGD Id.
function updateActiveInstance() {
    $('.active_instance_id').change(function () {

        const id = $(this).attr('data-id'),
            col = $(this).attr('data-name'),
            product = $(this).attr('data-product');

        $.ajax({
            url: jsglobals.base_url + 'common/update_active_instances',
            dataType: 'json',
            data: {
                id: id,
                name: col,
                product: product
            },
            type: 'post',
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            universal_components = data;
            refreshPage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });

    })
}

// Makes an Ajax Call using Promises.
function makeAjaxCall(url, data) {
    try {
        // Create a new promise object.
        var ajax_promise = new Promise(function (resolve, reject) {
            $.ajax({
                url: jsglobals.base_url + url,
                dataType: "json",
                type: 'post',
                data: data
            }).done(function (data) {

                if (data.status === 'failure') {
                    toastr.error(data.message);
                    reject(data);
                    return;
                }
                else if (data.status === 'redirect') {
                    window.location.href = data.redirect;
                    reject(data);
                    return;
                }
                resolve(data);
                return;
            }).fail(function (jqXHR, textStatus) {
                $.unblockUI();
                reject('Ajax Error');
                toastr.error("Request failed: " + textStatus);
            });
        });
    } catch (e) {
        return false;
    }
    return ajax_promise;
}

// Uploads a File to server.
function uploadFile(target, data, file_type) {

    var upload_promise = new Promise(function (resolve, reject) {
        $('#' + target).fileupload({
            method: 'POST',
            dataType: 'json',
            formData: data,
            done: function (e, data) {
                toastr.success('File Uploaded!!', 'Success!!');
                resolve(data);
            },
            autoUpload: false,
        }).on('fileuploadadd', function (e, data) {
            var file_type_allowed;
            file_type_allowed = file_type;

            var file_name = data.originalFiles[0]['name'];

            // Checks if file type is valid to upload.
            if (!file_type_allowed.test(file_name)) {
                $('#error').html('File Type not allowed');
            } else {
                $('#error').html('');
                $('.fileupload-progress').toggle();
                data.submit();
            }
        }).on('fileuploadprogressall', function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('.progress-bar-success').css('width', progress + '%');

            if (progress == 100) {
                $('.fileupload-progress').toggle();
                $('.progress-bar-success').css('width', '0%');
            }
        }).on('fileupload')
            .on('fileuploadfail', function (e, data) {
                $.each(data.files, function (index, file) {
                    var error = $('<span class="text-danger"/>').text('File upload failed.');
                    reject(new Error('File Upload Failed'));
                    $(data.context.children()[index])
                        .append('<br>')
                        .append(error);
                });
            }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');
    });

    return upload_promise;
}


// Displays a PDF document.
function preview_pdf(url, canvas) {

    var pdfjsLib = window['pdfjs-dist/build/pdf'];
    pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

    var pdfDoc = null,
        pageNum = 1,
        pageRendering = false,
        pageNumPending = null,
        num = null,
        ctx = canvas.getContext('2d');

    // Get page info from document, resize canvas accordingly, and render page.
    function renderPage(num) {
        pageRendering = true;

        // Using promise to fetch the page
        pdfDoc.getPage(num).then(function (page) {
            var scale = $('#canvas-container').width() / (page.getViewport({scale: 1}).width + 10);
            var viewport = page.getViewport({scale: scale});
            canvas.height = viewport.height;
            canvas.width = viewport.width;

            // Render PDF page into canvas context
            var renderContext = {
                canvasContext: ctx,
                viewport: viewport
            };
            var renderTask = page.render(renderContext);


            // Wait for rendering to finish
            renderTask.promise.then(function () {
                pageRendering = false;
                if (pageNumPending !== null) {
                    // New page rendering is pending
                    renderPage(pageNumPending);
                    pageNumPending = null;
                }
            });
        });

        // Update page counters
        // document.getElementById('page_num').textContent = num;
    }

    // Check if another page is being rendered.
    function queueRenderPage(num) {
        if (pageRendering) {
            pageNumPending = num;
        } else {
            renderPage(num);
        }
    }

    // Displays previous page.
    function onPrevPage() {
        if (pageNum <= 1) {
            return;
        }
        pageNum--;
        queueRenderPage(pageNum);
    }

    $('.prev').bind('click', onPrevPage);

    // Displays next page.
    function onNextPage() {

        if (pageNum >= pdfDoc.numPages) {
            return;
        }
        pageNum++;
        queueRenderPage(pageNum);
    }

    $('.next').bind('click', onNextPage);

    // Asynchronously downloads PDF.
    pdfjsLib.getDocument(url).promise.then(function (pdfDoc_) {
        pdfDoc = null;
        pdfDoc = pdfDoc_;
        // document.getElementById('page_count').textContent = pdfDoc.numPages;
        // Initial/first page rendering
        renderPage(pageNum);
    });
}

function get_send_email_container(data) {
    $('#email-content').html('<form id="outline-form">' +
        '<input type="hidden" id="promo_focus" name="promo_focus">' +
        '<input type="hidden" id="promo_format" name="promo_format">' +
        '<input type="hidden" id="client_type" name="client_type">' +
        '<input id="y" value="' + data + '" type="hidden" name="outline_content">\n' +
        '<trix-editor id="trix-editor" class="editor" input="y" name="tsl_promo_editor"></trix-editor>' +
        '</form>');
}

// Refresh the List of Drop downs for the Promotional Document.
function refreshTSLdropdown() {

    try {
        var refresh_promise = new Promise(function (resolve, reject) {
            const id = (undefined !== window.location.search.split('?id=')[1]) ?
                window.location.search.split('?id=')[1] : window.location.search.split('?ref=')[1];

            // get_all_criteria();
            // resolve('Promise Resolved');
            // Fetch Target Audience and List Criteria.
            const get_tsl_ta_criteria = makeAjaxCall('tsl/get_tsl_ta_criteria', {'tsl_id': id});
            get_tsl_ta_criteria.then(function (res) {

                // Clear any previous table body.
                $('#criteria-table').find('tbody').empty();
                $('#target-audience-table').find('tbody').empty();

                // Append Options for suspect list criteria.
                const list_criteria_selector = $('#list-criteria');
                list_criteria_selector.empty();
                list_criteria_selector
                    .append($('<option value="-1" disabled>-- None --</option>'))

                // Iterate through each Criteria for this TSL.
                $.each(res.data.criteria, function (index, criterion) {
                    list_criteria_selector
                        .append($('<option value="' + criterion.uc_id + '">' +
                            criterion.uc_name + '</option>'));


                    // Select a target audience if none selected.
                    if (res.data.criteria.length > 0) {
                        list_criteria_selector.find('option:eq(2)').prop('selected', true);
                        list_criteria_selector.attr('data-prev', list_criteria_selector.val());
                    }
                    else
                        list_criteria_selector.find('option:eq(0)').prop('selected', true);
                });

                // Append Target Audience.
                const target_audience_selector = $('#target-audience');
                target_audience_selector.empty();
                target_audience_selector
                    .append($('<option value="-1" disabled>-- None --</option>'))

                // Iterate through each Target Audience for this TSL.
                $.each(res.data.ta, function (index, audience) {
                    target_audience_selector
                        .append($('<option value="' + audience.ul_id + '">' + audience.ul_name + '</option>'));
                });

                // Select a target audience if none selected.
                if (res.data.ta.length > 0) {
                    target_audience_selector.find('option:eq(1)').prop('selected', true);
                    target_audience_selector.attr('data-prev', target_audience_selector.val());
                }
                else
                    target_audience_selector.find('option:eq(0)').prop('selected', true);

                resolve('Promise Resolved');
            });
        });
    } catch (e) {
        reject('Promise Rejected');
        return false;
    }

    return refresh_promise;
}

// Collaboration Click handler.
function collaborate() {
    if (!window.location.href.includes('collab')) {
        // Add Collab Click handler
        $(document).on('click', '.add_collab', function () {
            $('#add-collab-dialog').modal('show');
            // $('#send-collab-dialog').modal('show');

            const ques_id = $(this).parent().find('textarea').attr('data-id'),
                that = $(this),
                parent_margin = $(this).parent().css('margin'),
                order = $(this).parent().siblings('.collab_row').length,
                get_collab_details = makeAjaxCall('collab/get_collab_details', {});

            get_collab_details.then(function (data) {
                $('#collab-action').empty();
                // $('#collab-action').append('<optgroup label="Comment(s)"></optgroup>')

                let optgroup = $("<optgroup label='Comment(s)'>");
                // Iterate over each of the formats available
                $.each(data.collab_details.format, function (index, format) {

                    if (format.collab_format_id === '2' || format.collab_format_id === '3') {
                        $('#email-collab-formatn').append('<option value="' + format.collab_format_id + '">'
                            + format.collab_format_name + '</option>')
                    } else {
                        optgroup
                            .append('<option value="' + format.collab_format_id + '">'
                                + format.collab_format_name + '</option>')
                    }
                });

                $('#collab-action').append(optgroup);

                // Append Sender Name.
                $('#sender_name').val(data.collab_details.user);

                const recipient_name_selector = $('#recipient_name');

                recipient_name_selector.empty();
                recipient_name_selector.append('<option value="-1" selected>-- Select Recipient --</option>');
                recipient_name_selector.append('<option value="0">-- Add New Recipient --</option>');

                //Iterate over the collaborator names.
                $.each(data.collab_details.recipient, function (index, recipient) {
                    recipient_name_selector.append
                    ('<option value="' + recipient.collab_recipient_id + '">' + recipient.collab_recipient_name + '</option>')
                });

                // Recipient Name drop down change event.
                recipient_name_selector.change(function () {
                    if ($(this).val() === '0') {
                        $('#add-collab-dialog').modal('hide')
                        $('#add-recipient-dialog').modal('show')
                    }
                })
            });

            $('#save_collab_details').off('click');
            $('#save_collab_details').click(function () {
                const sender_name = $('#sender_name').val(),
                    action = $('#collab-action option:selected').text(),
                    today = new Date(),
                    date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate(),
                    time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds(),

                    // Create a collab instance in database.
                    save_collab_data = makeAjaxCall('collab/save_collab_data', {
                        type: window.location.pathname.split('/')[3],
                        instance_id: activeId,
                        ques_id: ques_id,
                        order: order,
                        sender: sender_name,
                        recipient: $('#recipient_name').val(),
                        action: $('#collab-action').val(),
                        date: date,
                        time: time,
                        collaboration_id: 0,
                        category: 'sc'
                    });

                // Add Row after collab data is saved.
                save_collab_data.then(function (data) {
                    toastr.success('Collaboration Instance Added!!');
                    $('#add-collab-dialog').modal('hide');

                    // Format date and time
                    const formatted_date = moment(date).format('DD-MM-YY'),
                        formatted_time = moment(time, "HH:mm:ss").format('hh:mm A');

                    const collab_action_message = [
                        'Request for Comments.',
                        'Request for Examples.',
                        'Request for Roleplay.',
                        'Do you have any Questions?',
                        'Do you have any Recommendations?',
                        'Do you have any Requests?'
                    ];

                    // Append text box after DB record is added.
                    $(that).parent().after(
                        '<div style="margin-left: calc(4% + ' + $('#ans' + ques_id).parent().css('margin-left')
                        + ')"  class="collab_row">' +
                        '<label>' + sender_name + '. ' +
                        formatted_date + ' ' + formatted_time + ' : ' + action + '</label>' +
                        '<div class="input-group">' +
                        '<span class="input-group-addon btn btn-secondary send_collab" data-toggle="modal"' +
                        'data-action="' + action + '" data-target="#send-collab-dialog" ' +
                        'data-message="' + collab_action_message[$('#collab-action').val() - 1] + '"' +
                        ' data-id="' + data.collab_data_saved + '">' +
                        '<i class="fa fa-paper-plane"></i></span>' +
                        '<span class="input-group-addon btn btn-secondary tts">' +
                        '<i class="fa fa-bullhorn"></i></span>' +
                        '<textarea onkeyup="auto_grow(this)" class="form-control custom-control collab-user-row" ' +
                        'rows="1" cols="70" style="height: 52px;" ' +
                        'data-id="' + ques_id + '" data-collab="' + data.collab_data_saved + '" ' +
                        'placeholder="Add an Email Message."></textarea>' +
                        '<span class="input-group-addon btn btn-secondary stt">' +
                        '<i class="fa fa-microphone"></i></span></div></div>'
                    )
                });
            })
        });
    }

    //Save Collaboration data to database.
    $(document).off('change', '.collab_row textarea');
    $(document).on('change', '.collab_row textarea', function () {
        console.log('here');
        const that = $(this),
            save_collab_data = makeAjaxCall('collab/update_collab_data', {
                id: $(this).attr('data-collab'),
                content: $(this).val(),
            });

        save_collab_data.then(function (data) {
            // console.log($(that).siblings('.send_collab'));
            // $(that).siblings('.send_collab').attr('data-message', $())
        })
    })
}


// Fetches and setup Collab details
function setupCollab() {
    const get_collab_data = makeAjaxCall('collab/get_collab_data', {
        type: window.location.pathname.split('/')[3],
        id: activeId,
        category: 'sc',
        filter_recipient: $('#filter-recipient-name').val(),
        filter_action: $('#filter-collab-action').val()
    });

    // Append Text for collaboration.
    get_collab_data.then(function (data) {
        $.each(data.collab_data, function (index, collab_data) {
            const formatted_date = moment(collab_data.sc_date).format('DD-MM-YY'),
                formatted_time = moment(collab_data.sc_time, "HH:mm:ss").format('hh:mm A');

            $('#ans' + collab_data.sc_ques_id).parent()
                .after(
                    '<div style="margin-left: calc(4% + ' + $('#ans' + collab_data.sc_ques_id).parent().css('margin-left')
                    + ')" class="collab_row"><label>' + collab_data.sc_sender + '. ' +
                    formatted_date + ' ' + formatted_time + ' : ' + collab_data.sc_action + '</label>' +
                    '<div class="input-group" id="collaboration' + collab_data.sc_id + '">' +
                    '<span class="input-group-addon btn btn-secondary send_collab" ' +
                    'data-message="' + collab_data.message + '" data-toggle="modal"' +
                    'data-target="#send-collab-dialog" data-id="' + collab_data.sc_id + '" >' +
                    '<i class="fa fa-paper-plane"></i></span>' +
                    '<span class="input-group-addon btn btn-secondary tts">' +
                    '<i class="fa fa-bullhorn"></i></span>' +
                    '<textarea onkeyup="auto_grow(this)" class="form-control custom-control collab-user-row" rows="1" ' +
                    'cols="70" data-collab="' + collab_data.sc_id + '" ' +
                    'data-id="' + collab_data.sc_ques_id + '" placeholder="Add an Email Message." ' +
                    'id="collab' + collab_data.sc_id + '">' +
                    collab_data.sc_content + '</textarea>' +
                    '<span class="input-group-addon btn btn-secondary stt">' +
                    '<i class="fa fa-microphone"></i></span>' +
                    '<span class="input-group-addon btn btn-secondary" data-toggle="modal" ' +
                    'data-target="#delete-collab-dialog" data-id="' + collab_data.sc_id + '" ' +
                    'data-sender="' + collab_data.sc_sender + '" data-action="' + collab_data.sc_action + '">' +
                    '<i class="fa fa-trash"></i></span>' +
                    '</div></div>')
        });

        // Fetch Collaborator data.
        const get_collaborator_data = makeAjaxCall('collab/get_collab_data', {
            type: window.location.pathname.split('/')[3],
            id: activeId,
            category: 'scr',
            filter_recipient: $('#filter-recipient-name').val(),
            filter_action: $('#filter-collab-action').val()
        });

        // Append Text for collaborator.
        get_collaborator_data.then(function (data) {
            $.each(data.collab_data, function (index, collab_data) {
                const formatted_date = moment(collab_data.sc_date).format('DD-MM-YY'),
                    formatted_time = moment(collab_data.sc_time, "HH:mm:ss").format('hh:mm A'),
                    collab_selector = $('#collaboration' + collab_data.sc_collaboration_id).parent(),
                    parent_margin = collab_selector.css('margin-left');

                // Append Collaborator Responses.
                collab_selector
                    .after(
                        '<div style="margin-left: calc(4% + ' + $('#ans' + collab_data.sc_ques_id).parent().css('margin-left')
                        + ')" class="collaborator_row"><label>'
                        + collab_data.sc_sender + '. ' +
                        formatted_date + ' ' + formatted_time + ' : ' + collab_data.sc_action + '</label>' +
                        '<div class="input-group">' +
                        '<span class="input-group-addon btn btn-secondary tts">' +
                        '<i class="fa fa-bullhorn"></i></span>' +
                        '<textarea onkeyup="auto_grow(this)" class="form-control custom-control collaborator_data" rows="1" ' +
                        'cols="70" data-collab="' + collab_data.sc_id + '" ' +
                        'data-id="' + collab_data.sc_ques_id + '" id="collab' + collab_data.sc_id + '">' +
                        collab_data.sc_content + '</textarea>' +
                        '<span class="input-group-addon btn btn-secondary stt">' +
                        '<i class="fa fa-microphone"></i></span>' +
                        '<span class="input-group-addon btn btn-secondary delete-collab"' +
                        ' data-id="' + collab_data.sc_id + '" data-toggle="modal" data-target="#delete-collab-dialog"' +
                        ' data-sender="' + collab_data.sc_sender + '" data-action="' + collab_data.sc_action + '">' +
                        '<i class="fa fa-trash"></i></span>' +
                        '</div></div>')
            });
            // TTS and STT functionality.
            speechFunctionality();

            // Check if roleplay is used for filtering collaboration data.
            if ($('#filter-collab-action').val() === '3') {
                // Append play roleplay button.
                $('#cpd_full_view')
                    .after('<button id="play_roleplay" class="btn btn-secondary full-view-hide"' +
                        ' style="margin-left:2px">Play Roleplay <i class="fa fa-play"></i></button>');

                // Append remove roleplay button.
                $('.collaborator_row')
                    .find('textarea')
                    .after('<span class="input-group-addon btn btn-secondary remove-roleplay">' +
                        '<i class="fa fa-minus"></i></span>')
            }
            else {
                $('#play_roleplay').remove();
            }
        });
    });
}


function refreshDMDDropdown() {
    try {

        var refresh_promise = new Promise(function (resolve, reject) {
            const promo_focus_selector = $('#promo-focus');
            let id = window.location.search.split('?id=')[1];

            // Check if Id and origin field are not set.
            if (id === undefined) {
                id = tsl_id;
                origin = 'tsl';
            }

            // Fetch the promotional focus.
            const get_promo_focus = makeAjaxCall('tsl/get_promo_focus', {
                'tool_id': id,
                'origin': origin
            });

            get_promo_focus.then(function (res) {
                promo_focus_selector.empty();
                $('#promo-focus-table').empty();

                promo_focus_selector.append($('<option value="-1" disabled>-- Select Promotional Focus --</option>'));
                promo_focus_selector.append($('<option value="0">( Add Promotional Focus )</option>'));

                $.each(res.data, function (index, focus) {
                    promo_focus_selector.append($('<option value="' + focus.tpf_id + '">' + focus.tpf_name + '</option>'));

                    $('#promo-focus-table').append($('<tr>' +
                        '<td class="text-center">' + eval(index + 1) + '</td>' +
                        '<td class="text-center">' + focus.tpf_name + '</td>' +
                        '<td class="text-center">' +
                        '<button class="btn btn-default" data-name="' + focus.tpf_name + '"' +
                        ' data-toggle="modal" data-target="#delete-promo-focus-dialog"' +
                        ' data-id="' + focus.tpf_id + '" ' + (focus.tpf_tool_id === '0' ? 'disabled' : '') + '>' +
                        '<i class="glyphicon glyphicon-trash"></i>' +
                        '</button></td>' +
                        '</tr>'))
                });

                if (res.data.length > 0) {
                    promo_focus_selector.find('option:eq(2)').prop('selected', true);
                    promo_focus_selector.attr('data-prev', promo_focus_selector.val());
                } else
                    promo_focus_selector.find('option:eq(0)').prop('selected', true);

                // Check if Add new promo option is selected.
                $(document).on('change', '#promo-focus', function (e) {
                    if ('0' === $(this).val()) {
                        $('#view-promo-focus-dialog').modal('show');
                        $(this).val($(this).attr('data-prev'));
                    } else {
                        makeAjaxCall('tsl/update_promo_focus', {
                            'id': id,
                            'origin': origin,
                            'val': $(this).val()
                        });
                    }
                });
                resolve('Promise');
            });

        });
    } catch (e) {
        reject(e);
        return false;
    }
    return refresh_promise;
}


// Fetches all the Deal Types.
function get_all_deal_type() {

    try {
        var deal_promise = new Promise(function (resolve, reject) {
            const codes = {
                "gen_status": "gds",
                "com_status": "cds",
                "str_status": "str_ds",
                "clu": "clu",
                "rwt": "rwt",
                "bds": "bds",
                "complete_ds": "comds",
                "generation_ds": "gends",
                "stra_status": "stra_ds",
                "buyer_ds": "buyer_ds",
                // "rds": "rds",
                // "referral_ds": "ref_ds"
            };

            // Get the all available codes.
            const get_codes = makeAjaxCall('code/get_codes/dummy', {
                'codes': codes
            });

            get_codes.then(function (data) {

                const gen_status = data.codes.gds,
                    com_status = data.codes.cds,
                    strategic_status = data.codes.str_ds,
                    buyer_status = data.codes.bds,
                    referral_status = data.codes.rds,
                    clu = data.codes.clu,
                    rwt = data.codes.rwt,
                    deal_status_selector = $('.client-status');


                deal_status_selector.empty();

                // Append TSL status to status List
                deal_status_selector
                    .append($('<option>')
                        .attr({value: ''})
                        .append("-- None -- "))
                    .append($('<optgroup>')
                        .attr({label: 'TSL Status'})
                        .append('<option value="0">To Be Contacted.</option>')
                    );

                // Append Generation status to select List
                deal_status_selector
                    .append($('<optgroup>')
                        .attr({label: 'Generation Status'})
                        .append(
                            $.map(gen_status, function (gds, d_status) {
                                return '<option value="' + gds.code_abbrev +
                                    '" data-status-type="gds">' + gds.code_name + '</option>'
                            })
                        )
                    );

                // Append Completion status to select List
                deal_status_selector
                    .append($('<optgroup>')
                        .attr({label: 'Completion Status'})
                        .append(
                            $.map(com_status, function (cds, d_status) {
                                return '<option value="' + cds.code_abbrev +
                                    '" data-status-type="cds">' + cds.code_name + '</option>'
                            })
                        )
                    );

                // Append Strategic status to select List
                deal_status_selector
                    .append($('<optgroup>')
                        .attr({label: 'Strategic Status'})
                        .append(
                            $.map(strategic_status, function (sdt, d_status) {
                                return '<option value="' + sdt.code_abbrev +
                                    '" data-status-type="str_ds">' + sdt.code_name + '</option>'
                            })
                        )
                    );

                // Append Buyer status to select List.
                $('.buyer-asset-status').empty();
                $('.buyer-asset-status')
                    .append($('<option>')
                        .attr({value: ''})
                        .append("-- None -- "))
                    .append($('<optgroup>')
                        .attr({label: 'TSL Status'})
                        .append('<option value="0">To Be Contacted.</option>'))
                    .append($('<optgroup>')
                        .attr({label: 'Buyer Status'})
                        .append(
                            $.map(buyer_status, function (sdt, d_status) {
                                return '<option value="' + sdt.code_abbrev +
                                    '" data-status-type="str_ds">' + sdt.code_name + '</option>'
                            })
                        )
                    );
                //
                // deal_status_selector
                //     .append($('<optgroup>')
                //         .attr({label: 'Referral Status'})
                //         .append(
                //             $.map(referral_status, function (sdt, d_status) {
                //                 return '<option value="' + sdt.code_abbrev +
                //                     '" data-status-type="rds">' + sdt.code_name + '</option>'
                //             })
                //         ));
            });
            resolve('Promise');
        })
    } catch (e) {
        reject(e);
        return false;
    }
    return deal_promise;
}

// Fetch the list of criteria for this user.
function get_all_criteria() {
    try {
        var criteria_promise = new Promise(function (resolve, reject) {
            const get_all_ta_criteria = makeAjaxCall('cpdv1/get_all_ta_criteria', {});

            get_all_ta_criteria.then(function (data) {
                const client_criteria_selector = $('.client-criteria'),
                    client_ta_selector = $('.client-location');

                // Clear any previos option present
                client_ta_selector.empty();
                client_criteria_selector.empty();

                // Append Add new and none options
                client_criteria_selector.append('<option value="" selected > -- None -- </option>');
                client_criteria_selector.append('<option value="-1"> (( Add New Criteria )) </option>');
                client_ta_selector.append('<option value="" selected> -- None -- </option>');
                client_ta_selector.append('<option value="-1"> (( Add New Location )) </option>');
                $('#target-audience-table').find('tbody').empty();
                $('#criteria-table').find('tbody').empty();

                // Append options to Criteria drop down.
                $.each(data.ta_criteria.criteria, function (index, criteria) {
                    client_criteria_selector.append('<option value="' + criteria.uc_id + '">'
                        + criteria.uc_name + '</option>');

                    $('#criteria-table tbody').append(
                        $('<tr><th class="text-center">' + (index + 1) + '</th>' +
                            '<th class="text-center">' + criteria.uc_name + '</th>' +
                            '<th class="text-center"><button class="btn btn-sm" data-toggle="modal"' +
                            ' data-target="#delete-criteria-dialog"' +
                            ' data-id="' + criteria.uc_id + '" data-name="' + criteria.uc_name + '">' +
                            '<i class="fa fa-trash"></button></th></tr>')
                    )
                });

                // Append options to Location drop down.
                $.each(data.ta_criteria.ta, function (index, ta) {
                    client_ta_selector.append('<option value="' + ta.ul_id + '">'
                        + ta.ul_name + '</option>');

                    $('#target-audience-table tbody').append(
                        $('<tr><th class="text-center">' + (index + 1) + '</th>' +
                            '<th class="text-center">' + ta.ul_name + '</th>' +
                            '<th class="text-center"><button class="btn btn-sm" data-toggle="modal"' +
                            ' data-target="#delete-target-audience-dialog"' +
                            ' data-id="' + ta.ul_id + '" data-name="' + ta.ul_name + '">' +
                            '<i class="fa fa-trash"></button></th></tr>')
                    )
                });

                // Fetches asset information.
                const get_asset_info = makeAjaxCall('homebase/get_asset_info', {});
                get_asset_info.then(function (data) {
                    $.each(data.asset_info, function (index, info) {
                        const info_selector = $('.add-client-' + index);
                        info_selector.empty();

                        info_selector.addClass('toggle-modal-select');
                        info_selector.append('<option value="-1">-- Add New --</option>');
                        info_selector.append('<option selected value="0">-- None --</option>');

                        // Append options to ZIP/City/State/Condition.
                        info_selector.append($.map(info, function (individual_info, index) {
                            return '<option value="' + individual_info.hai_id + '">' +
                                individual_info.hai_value + '</option>'
                        }).join(''));
                    })
                    resolve('Promise');
                });

                add_location_criteria();

            });
        }).then(function () {
            $("#add-client-form").formcache("outputCache");
        })
    } catch (e) {
        reject(e);
        return false;
    }
    return criteria_promise;
}


function append_owner_asset_head(client, count, selector, append = '') {
    let asset_table_selector = $('#' + selector),
        image_url = jsglobals.base_url + 'resources/app/media/user-avatar.svg';

    // Check if image is present
    (null !== client.hca_avatar) ?
        image_url = window.location.origin + '/tools/homebase/get_owner_image/?img=' + client.hca_avatar : '';
    asset_table_selector
        .append($(
            // '<tr class="add-asset-tr' + client.hc_id + '"><td colspan="10">' +
            '<table id="add-asset-table' + client.hc_id + '" ' +
            'class="table table-condensed table-responsive add-asset-tr' + client.hc_id + '" ' +
            'style="border: 5px solid #008080cf;">' +
            '<tr class="asset_head" id="' + append + 'add-asset-row' + client.hc_id + '-' + count + '" style="background: #ffffe09e;">' +
            '<th rowspan="4" colspan="2" class="upload-owner-asset-image"><img src=' + image_url +
            ' id="asset-avatar" class="asset-image">' +
            '<div class="upload">' +
            '<input id="asset-img-upload" type="file" name="files[]"' +
            ' data-url=' + jsglobals.base_url + 'homebase/upload_asset_image' +
            ' style="visibility: hidden; float:left; width:0;">' +
            '<a id="upload-asset-img" data-id="' + client.hca_id + '">Click to Change Image</a></div></th>' +
            '<th class="text-center"><span style="float: left"><b>A' + count + '</b></span>Last Updated</th>' +
            '<th class="text-center" colspan="2">Asset Name</th>' +
            '<th class="text-center" colspan="2">Asset Address</th>' +
            '<th class="text-center">City</th>' +
            '<th class="text-center">State</th>' +
            '<th class="text-center">Zip</th>' +
            '<th class="text-center">Info</th>' +
            '<th class="text-center">Notes</th>' +
            '<th class="text-center">Action</th>' +
            '<th class="text-center">ACM</th>' +
            '<th class="text-center">Submit</th>' +
            '<th class="text-center">Remove</th>' +
            '</tr>' +
            '<tr class="asset_head" id="' + append + 'add-asset-row' + client.hc_id + '-' + count + '-1"' +
            ' style="background: #ffffe09e;">' +
            '<th class="text-center">Product Type</th>' +
            '<th class="text-center">Market Area</th>' +
            '<th class="text-center">Units</th>' +
            '<th class="text-center">Sq. Ft.</th>' +
            '<th class="text-center">Condition</th>' +
            '<th class="text-center">Criteria</th>' +
            '<th class="text-center">Status</th>' +
            '<th class="text-center" colspan="3">Tool</th>' +
            '<th class="text-center" colspan="4">Portfolio</th>' +
            '</tr>'));
}

// Initialize trix editor.
function initEditor(obj) {

    let obj_content = '';
    obj_content = $(obj).attr('data-content');

    // Check if object data attribute is set.
    ('' === obj_content || null === obj_content) ? $(obj)
        .attr('data-content', $('#default_promo_content').html().replace(/\\"/g, "'").replace(/\\/g, '')) : '';

    // Forward Propagate for Promo Version
    $('#propagate-promo').off('click');
    $('#propagate-promo').click(function () {
        const propagate_promo = makeAjaxCall('tsl/propagate_promo', {
            client_id: client_id,
            promo_id: activePromoId,
            content: obj_content
        });

        propagate_promo.then(function (data) {
            toastr.success('Promo Content Propagated!!', 'Success!!');
        })
    });

    tinymce.init({
        mode: 'exact',
        selector: '#email-container',
        branding: false,
        content_style: 'body { font-size: 3vh; }',
        entity_encoding: "raw",
        setup: function (ed) {
            ed.on('init', function (args) {
                tinymce.activeEditor.setContent($(obj).attr('data-content').replace(/\\/g, ''));
            });
        },
        init_instance_callback: function (ed) {
            let height = ($('#outline-container').height()
                // - $('.mce-top-part').height() - $('.mce-statusbar').height()
            );
            (height < 300) ? (height = $('#outline-container').height() - 100) : '';
            height = height;

            tinyMCE.DOM.setStyle(tinyMCE.DOM.get('email-container_ifr'), 'height', height + 'px');
            $('#edit-promo').click(function () {
                tinymce.activeEditor.getBody().setAttribute('contenteditable', true);
            });
            tinymce.activeEditor.getBody().setAttribute('contenteditable', false);
        },
    });

    check_dummy_component(client_id);
}

// Set the content for component popover.
function getComponentPopover(target, title, table_head, condition, tool, hca_id) {
    let arr = [];
    arr.DST = 'Deal Status Tracker';
    arr.DCB = 'Deal Conversion Board';
    arr.SGD = 'Strategic Game Designer';
    arr.QVC = 'Quick View Components';
    arr.MPA = 'Model, Plan and Actual';
    arr.Pages = 'Promo Builder';
    arr.Vmail = 'Voice Mail Builder';
    arr.Script = 'Initial Contact Script Builder';
    arr.Email = 'Email Builder';
    arr.Stories = 'Stories Builder';
    arr.Profile = 'Client Profile Builder';
    arr.Worksheet = 'Worksheet Builder';
    arr.SAP = 'Strategic Advisory Program';
    arr.Updates = 'Updates Builder';
    arr.Notices = 'Notices Builder';
    arr.Survey = 'Survey Builder';
    arr.Email = 'Email Builder';
    arr.Event = 'Event Manager';
    arr.Review = 'Review Manager';
    arr.Followup = 'Notices Builder';
    arr.Assembly = 'Specification Documents';
    arr.Media = 'Media Documents';
    arr.Proposal = 'Proposal Documents';
    arr.Agreement = 'Agreement Documents';
    arr['Web Presentation'] = 'Web Presentation';
    const path = window.location.pathname,
        components = path.split('/');
    origin = ('cpdv1' === components[2]) ? 'cpdv1' : 'tsl';

    return '<div class=\'row col-xs-12\'>\n' +
        '<table style=\'width:100%\' class=\'table table-striped\'>\n' +
        '<tr>\n' +
        '<thead>\n' + table_head +
        '</thead>\n' +
        '</tr>\n' +
        '<tbody>\n' +
        '<tr>\n' +
        $.map(universal_components.components, function (component) {

            if (Function('return (function(component, condition){ return (eval(  condition ))})')()(
                    component, condition)) {

                return ('<td class=\'text-center\'>' +
                    '<span class=\'has-tooltip\' data-title=\'' + arr[component.uc_name] + '\'>' +
                    '<a class=\'btn btn-default btn-sm coming-soon-tool\' ' +
                    'href=' + jsglobals.base_url + tool + component.uc_controller
                    + hca_id + '?id=' + window.location.search.split('?id=')[1] + ' id=' + component.uc_name + '>' +
                    '<span class=\'glyphicon glyphicon-' + component.uc_icon + '\'></span></a></span>' +
                    '</td>');
            }
        }).join('') +
        '</tr>\n' +
        '</tbody>\n' +
        '</table>\n' +
        '</div>\n' +
        '</div>';

}


// Helper method to validate token and refresh if needed
// function getAccessToken(callback) {
//     var now = new Date().getTime();
//     var isExpired = now > parseInt(sessionStorage.tokenExpires);
//     // Do we have a token already?
//     if (sessionStorage.accessToken && !isExpired) {
//         // Just return what we have
//         if (callback) {
//             callback(sessionStorage.accessToken);
//         }
//     } else {
//         // Attempt to do a hidden iframe request
//         makeSilentTokenRequest(callback);
//     }
// }

// Set the content for component popover.
function setupComponentPopover(target, title, table_head, condition, tool, hca_id) {
    let arr = [];
    arr.DST = 'Deal Status Tracker';
    arr.DCB = 'Deal Conversion Board';
    arr.SGD = 'Strategic Game Designer';
    arr.QVC = 'Quick View Components';
    arr.MPA = 'Model, Plan and Actual';
    arr.Pages = 'Promo Builder';
    arr.Vmail = 'Voice Mail Builder';
    arr.Script = 'Initial Contact Script Builder';
    arr.Email = 'Email Builder';
    arr.Stories = 'Stories Builder';
    arr.Profile = 'Client Profile Builder';
    arr.Worksheet = 'Worksheet Builder';
    arr.SAP = 'Strategic Advisory Program';
    arr.Updates = 'Updates Builder';
    arr.Notices = 'Notices Builder';
    arr.Survey = 'Survey Builder';
    arr.Email = 'Email Builder';
    arr.Event = 'Event Manager';
    arr.Review = 'Review Manager';
    arr.Followup = 'Notices Builder';
    arr.Assembly = 'Specification Documents';
    arr.Media = 'Media Documents';
    arr.Proposal = 'Proposal Documents';
    arr.Agreement = 'Agreement Documents';
    arr['Web Presentation'] = 'Web Presentation';
    const path = window.location.pathname,
        components = path.split('/');
    origin = ('cpdv1' === components[2]) ? 'cpdv1' : 'tsl';

    $('#' + target)
        .popover({
            selector: '[rel=popover]',
            trigger: 'hover',
            container: 'body',
            placement: 'top',
            html: true,
            title: title,
            content: function () {
                return '<div class=\'row col-xs-12\'>\n' +
                    '<table style=\'width:100%\' class=\'table table-striped\'>\n' +
                    '<tr>\n' +
                    '<thead>\n' + table_head +
                    '</thead>\n' +
                    '</tr>\n' +
                    '<tbody>\n' +
                    '<tr>\n' +
                    $.map(universal_components.components, function (component) {

                        if (Function('return (function(component, condition){ return (eval(  condition ))})')()(
                                component, condition)) {

                            return ('<td class=\'text-center\'>' +
                                '<span class=\'has-tooltip\' data-title=\'' + arr[component.uc_name] + '\'>' +
                                '<a class=\'btn btn-default btn-sm coming-soon-tool\' ' +
                                'href=' + jsglobals.base_url + tool + component.uc_controller
                                + hca_id + '?id=' + cpd_id + ' id=' + component.uc_name + '>' +
                                '<span class=\'glyphicon glyphicon-' + component.uc_icon + '\'></span></a></span>' +
                                '</td>');
                        }
                    }).join('') +
                    '</tr>\n' +
                    '</tbody>\n' +
                    '</table>\n' +
                    '</div>\n' +
                    '</div>';
            }
        });
}

function append_owner_asset_body(client, add = '', count = 1, assets_info, leads, sources, portfolio) {
    let tsl_reference = '#',
        tsl_name = 'None';

    // Check if this homebase belongs to any TSL instance.
    if (null !== client.tsl_name) {
        tsl_reference = '/tools/tsl/tsl/' + client.tsl_id;
        tsl_name = 'TSL: ' + client.tsl_name;
    } else if (null !== client.cpd_name) {
        tsl_reference = '/tools/cpdv1/cpdv1/' + client.cpd_id;
        tsl_name = 'DMD: ' + client.cpd_name;
    }

    // Clone dummy rows.
    let dummy_content = $('.dummy_content').clone();
    dummy_content.removeClass('dummy_content');
    dummy_content.removeClass('hidden');

    //Fetch the HTML content for Popover and replace with values.
    dummy_content.find('.price').attr('id', 'price' + client.hca_id);
    dummy_content.find('.price').attr('data-id', client.hca_id);
    dummy_content.find('.price').attr('value', '$' + parseInt(client.hca_price).toLocaleString());

    dummy_content.find('.rate').attr('value', parseInt(client.hca_rate) + '%');
    dummy_content.find('.rate').attr('data-id', client.hca_id);
    dummy_content.find('.rate').attr('id', 'rate' + client.hca_id);

    dummy_content.find('.gross').attr('value', '$' +
        parseInt(client.hca_price * client.hca_rate / 100).toLocaleString());

    dummy_content.find('.gross').attr('data-id', client.hca_id);
    dummy_content.find('.gross').attr('id', 'gross' + client.hca_id);

    dummy_content.find('.csplit').attr('value', parseInt(client.hca_split) + '%');
    dummy_content.find('.csplit').attr('data-id', client.hca_id);
    dummy_content.find('.csplit').attr('id', 'split' + client.hca_id);

    dummy_content.find('.net').attr('value', '$' + parseInt(client.hca_net));
    dummy_content.find('.net').attr('data-id', client.hca_id);
    dummy_content.find('.net').attr('id', 'net' + client.hca_id);

    dummy_content.find('.gsplit').attr('value', parseInt(client.hca_gsplit) + '%');
    dummy_content.find('.gsplit').attr('data-id', client.hca_id);
    dummy_content.find('.gsplit').attr('id', 'gsplit' + client.hca_id);

    dummy_content.find('.gnet').attr('data-id', client.hca_id);
    dummy_content.find('.gnet').attr('id', 'gnet' + client.hca_id);

    $('#' + add + 'asset-row' + client.hc_id + '-' + count)
        .after($('<tr>')
            .append('<td><input class="form-control" disabled type="text" ' +
                'data-id="' + client.hca_id + '" data-name="hca_updated_at"' +
                'name="updated' + client.hca_id + '" style="background: #fff" value="' +
                (null !== client.hca_updated_at ? client.hca_updated_at : '') + '"></td>')

            // Asset Name
            .append('<td colspan="2"><input class="form-control asset-field" type="text" ' +
                'data-id="' + client.hca_id + '" data-name="hca_name"' +
                'name="asset' + client.hca_id + '" value="' +
                (null !== client.hca_name ? client.hca_name : '') + '"></td>')

            // Asset Address.
            .append('<td colspan="2"><input class="form-control asset-field" type="text" ' +
                'data-id="' + client.hca_id + '" data-name="hca_address"' +
                'name="address' + client.hca_id + '" value="' +
                (null !== client.hca_address ? client.hca_address : '') + '"></td>')

            // Asset City
            .append('<td>' +
                '<input type="city" class="form-control asset-field asset-info" ' +
                'data-type="city" data-name="hca_city" value="' + client.hca_city + '" ' +
                'name="city' + client.hca_id + '" data-id="' + client.hca_id + '"/>' +
                '</td>')

            // Asset State
            .append('<td>' +
                '<input type="text" class="form-control asset-field asset-info" ' +
                'data-type="state" data-name="hca_state" value="' + client.hca_state + '"' +
                'name="state' + client.hca_id + '"  data-id="' + client.hca_id + '"/>' +
                '</td>')

            // Asset Zip
            .append('<td>' +
                '<input type="text" class="form-control asset-field asset-info" ' +
                'data-type="zip" data-name="hca_zip" value="' + client.hca_zip + '" ' +
                'name="zip' + client.hca_id + '"  data-id="' + client.hca_id + '"/>' +
                '</td>')

            // Asset Info button
            .append($('<td class="text-center">')
                .append('<button class=\'btn btn-default btn-sm\'\n' +
                    ' data-toggle=\'modal\'' +
                    ' data-target=\'#client-info-dialog\'' +
                    ' data-id=' + client.hca_id + ' data-owner=' + client.hc_id + '>' +
                    '<span class=\'glyphicon glyphicon-info-sign\'></span>' +
                    '</button></td>'
                ))

            // Notes button
            .append($('<td class="text-center">')
                .append('<button class=\'btn btn-default btn-sm\'\n' +
                    ' data-target=\'#view-notes-dialog\' data-toggle=\'modal\'' +
                    ' data-id=' + client.hca_id + ' >' +
                    '<span class=\'glyphicon glyphicon-info-sign\'></span>' +
                    '</button></td>'
                ))

            // Action button
            .append($('<td class="text-center">')
                .append('<button class=\'btn btn-default btn-sm\'\n' +
                    ' data-target=\'#view-action-dialog\' data-toggle="modal"' +
                    ' data-id=' + client.hca_id + ' data-owner=' + client.hc_id + '>' +
                    '<span class=\'glyphicon glyphicon-info-sign\'></span>' +
                    '</button></td>'
                ))

            // ACM Button.
            .append($('<td class="text-center">')
                .append('<button class="btn btn-default btn-sm"' +
                    ' data-id="' + client.hc_id + '" data-toggle="modal" ' +
                    ' data-type="asset" data-target="#view-alert-dialog">' +
                    '<span class="glyphicon glyphicon-book"></span></button></tr></table>'))

            // Submit Button.
            .append($('<td class="text-center">')
                .append('<button class="btn btn-primary move-client"\n' +
                    ' data-status="' + client.hca_status + '"' +
                    ' data-id="' + client.hca_id + '" id="move-client' + client.hca_id + '">' +
                    '<span title="Long press to submit multiple assets." class="has-tooltip">' +
                    'Submit</span></button>' +
                    '<input type="checkbox" class="submit-client-check hidden" ' +
                    'name="id' + client.hca_id + '" ' +
                    'value="' + client.hca_id + '">'))

            // Delete Button.
            .append($('<td class="text-center">')
                .append('<button class="btn btn-default btn-primary delete-asset"' +
                    ' data-id="' + client.hca_id + '" data-type="client_asset" data-key="hca">' +
                    '<span class="glyphicon glyphicon-remove" style="color: #8ff1fd"></span></button></tr></table>'))
        );

    let second_row_id = '-' + count + '-1',
        unit_array = ['Under 10U', 'Between 10 & 20U', 'Between 20 & 50U',
            'Between 50 & 100U', 'Between 100 & 200U', 'Over 200U'],
        sq_ft_array = ['Under 5K', 'Between 5 & 10K', 'Between 10 & 20K',
            'Between 20 & 50K', 'Between 50 & 100K', 'Over 100K'];

    $('#' + add + 'asset-row' + client.hc_id + second_row_id)
        .after($('<tr>')
            // Asset product type
                .append('<td><select class="form-control owner-popup-product-type asset-field" ' +
                    'name="product' + client.hca_id + '" id="owner-product' + client.hca_id + '"' +
                    'data-id="' + client.hca_id + '" data-name="hca_product_id"></select></td>')

                // Asset Market Area
                .append('<td><select class="form-control client-location asset-field"' +
                    ' name="location' + client.hca_id + '" data-id="' + client.hca_id + '"' +
                    ' data-name="hca_location" id="location' + client.hca_id + '"></select></td>')

                // Asset Units
                .append('<td><select class="form-control client-unit asset-field"' +
                    ' name="unit' + client.hca_id + '" data-id="' + client.hca_id + '" data-name="hca_unit"' +
                    ' id="unit' + client.hca_id + '">' +
                    $.map(unit_array, function (unit, index) {
                        return '<option value="' + index + '" ' +
                            (parseInt(client.hca_unit) === index ? 'selected' : '') + '>' + unit + '</option>'
                    }).join() +
                    '</select></td>')

                // Asset Square Foot
                .append('<td><select class="form-control client-foot asset-field"' +
                    ' name="sq_ft' + client.hca_id + '" data-id="' + client.hca_id + '" data-name="hca_ft"' +
                    ' id="sq_ft' + client.hca_id + '">' +
                    $.map(sq_ft_array, function (sq_ft, index) {
                        return '<option value="' + index + '" ' +
                            (parseInt(client.hca_ft) === index ? 'selected' : '') + '>' + sq_ft + '</option>'
                    }).join()
                    + '</select></td>')

                // Asset Condition
                .append('<td>' +
                    '<select class="form-control asset-field asset-info" data-type="cond" data-name="hca_cond" ' +
                    'name="cond' + client.hca_id + '"  data-id="' + client.hca_id + '" style="min-width: 8vw">' +
                    '<option value="-1"> -- Add New --</option>' +
                    '<option value="0" selected> -- None --</option>' +
                    $.map(assets_info.cond, function (cond, index) {
                        return '<option value="' + cond.hai_id + '" ' +
                            (cond.hai_id === client.hca_cond ? 'selected' : '') + '>'
                            + cond.hai_value + '</option>'
                    }).join('') +
                    '</select>' +
                    '</td>')

                // Asset Criteria
                .append('<td><select class="form-control client-criteria asset-field"' +
                    ' name="criteria' + client.hca_id + '"' +
                    ' data-id="' + client.hca_id + '" data-name="hca_criteria_used"' +
                    '" id="criteria' + client.hca_id + '"></select></td>')

                // Asset Status
                .append('<td><select class="form-control client-status asset-field" name="status' + client.hca_id +
                    '" data-id="' + client.hca_id + '" data-name="hca_status" id="status' + client.hca_id + '">' +
                    '</select></td>')

                // Asset Tool
                .append('<td colspan="2">' +
                    '<div class="input-group" style="width: 100%"><a href="' + tsl_reference + '" target="_blank">' +
                    '<input type="text" class="form-control asset-tool asset-field"' +
                    ' name="asset' + client.hca_id +
                    '" data-id="' + client.hca_id + '" id="tool' + client.hca_id + '" ' +
                    'value="' + tsl_name + '"></a>' +
                    '<span class="input-group-btn">' +
                    '<button class="btn btn-default remove-tool" data-id="' + client.hca_id + '">' +
                    '<i class="fa fa-times"></i>' +
                    '</button></span></div></td>')

                // Asset Portfolio Info.
                .append('<td colspan="2"><div class="input-group">' +
                    '<select id="portfolio-select' + client.hca_id + '" ' +
                    'class="form-control input-group-addon client-portfolio" ' +
                    'style="min-width: 5vw; background-color: #fff !important;">' +
                    '<option value="-1">-- Add Portfolio --</option>' +
                    '<option value="0" selected>-- None --</option>' +
                    $.map(portfolio, function (ind_portfolio, index) {
                        let is_selected = '';
                        // Check if this asset have any portfolio.
                        if (client.hca_portfolio_id === ind_portfolio.up_id) {
                            is_selected = 'selected';
                        }

                        // Append Portfolio names.
                        return ('<option value="' + ind_portfolio.up_id + '" ' + is_selected + '>' +
                            ind_portfolio.up_name + '</option>');
                    }).join('') +
                    '</select>' +
                    '<span class="input-group-btn">' +
                    '<span class="has-tooltip" title="Add to Portfolio" >' +
                    '<button class="btn btn-default add-portfolio" type="button"' +
                    ' data-id="' + client.hca_id + '">' +
                    '<i class="fa fa-chain-broken"></i></button></span></span>' +
                    '</div></td>')

                // SA button Content.
                .append($('<td class="text-center">')
                    .append('<button class=\'btn btn-default btn-sm\'\n' +
                        ' data-toggle=\'modal\'' +
                        ' data-target=\'#view-sa-dialog\'' +
                        ' data-id=' + client.hca_id + '>' +
                        '<span class=\'glyphicon glyphicon-list-alt\'></span>' +
                        '</button></td>'
                    ))

                // Sent Button Content
                .append($('<td class="text-center">')
                    .append('<button class="btn btn-default btn-sm has-popover component_button promo_sent">' +
                        '<span class="glyphicon glyphicon-envelope"></span></button>'))
                // // ACM button
                .append($('<td class="text-center">')
                    .append('<a class="btn btn-default btn-sm" ' +
                        'href="' + jsglobals.base_url + 'cpdv1/acm/' + client.hca_id + '?id=0">' +
                        '<span class="glyphicon glyphicon-book"></span></a>'))
        );


    $(document).on('click', '.promo_sent, .info-popover, .dst', function () {
        $(this).popover('show');
        $(this).popover({
            container: 'body'
        });
    });

    useTooltip();
    usePopover();

    // Color select box if portfolio is present.
    if (client.hca_portfolio_id !== null) {
        $('#asset-table' + client.hc_id).find('input, select, button, a')
            .attr('disabled', 'true')
            .attr('title', 'This asset can be updated in portfolio tab.');

        $('#portfolio-select' + client.hca_id)
            .css('cssText', 'background-color: #FFFFE0 !important');
    }

    (client.hca_clu !== null) ? $('#owner-clu' + client.hca_id).val(client.hca_clu) : '';
    (client.hca_rwt !== null) ? $('#owner-rwt' + client.hca_id).val(client.hca_rwt) : '';

    // Disable Asset elements.
    $('#owner-info-asset-table select, ' +
        '#owner-info-asset-table input, ' +
        '#owner-info-asset-table button').attr('disabled', true);

}

function openNav() {
    document.getElementById("tools-side-nav").style.width = "250px";
}

function closeNav() {
    document.getElementById("tools-side-nav").style.width = "0";
}

/**
 * Updates the Deal Information.
 *
 */
let updateAmount = () => {

        // Listen to any change in amount fields.
        $('.amount_detail').add('.property_dropdown').change(function () {
            const asset_id = $(this).attr('data-id');

            // Update changed field value in the DB.
            const update_amount = makeAjaxCall("homebase/update_amount", {
                id: asset_id, col: $(this).attr('data-name'),
                val: $(this).val().replace('$', '').replace('%', '').replace(/,/g, ''),
                table: 'homebase_client_asset',
                pk: 'hca_id'
            });

            update_amount.then(function (data) {
                // ID selectors.
                const price_selector = $('#price' + asset_id),
                    rate_selector = $('#rate' + asset_id),
                    gross_selector = $('#gross' + asset_id),
                    gsplit_selector = $('#gsplit' + asset_id),
                    gnet_selector = $('#gnet' + asset_id),
                    split_selector = $('#split' + asset_id),
                    net_selector = $('#net' + asset_id),

                    // Calculate different income fields.
                    price = numeral(price_selector.val())._value,
                    rate = numeral(rate_selector.val() + '%')._value,
                    gross = price * rate,
                    gsplit = numeral(gsplit_selector.val() + '%')._value,
                    gnet = gsplit * gross,
                    split = numeral(split_selector.val() + '%')._value,
                    net = split * gnet;

                // Set Format for different fields.
                gross_selector.val(numeral(gross).format('$0,0'));
                gnet_selector.val(numeral(gnet).format('$0,0'));
                net_selector.val(numeral(net).format('$0,0'));

                let popover = gnet_selector.closest('.popover-content');

                // Update Popover content.
                let updated_content = popover.clone();
                updated_content.find('.price').attr('value', price_selector.val());
                updated_content.find('.rate').attr('value', rate_selector.val());
                updated_content.find('.gross').attr('value', gross_selector.val());
                updated_content.find('.gsplit').attr('value', gsplit_selector.val());
                updated_content.find('.csplit').attr('value', split_selector.val());
                updated_content.find('.net').attr('value', net_selector.val());
                updated_content.find('.gnet').attr('value', gnet_selector.val());

                // Set the Pop over dummy content data attribute.
                const amount_button_selector = $('#amount_button' + asset_id);
                amount_button_selector.removeAttr('data-dummy-content');
                amount_button_selector.attr('data-dummy-content', updated_content.html());

                // Update the net amount in the DB.
                makeAjaxCall("homebase/update_amount", {
                    id: asset_id,
                    col: 'hca_net',
                    val: net,
                    table: 'homebase_client_asset',
                    pk: 'hca_id'
                });

                // Popover hide event handler.
                $('.amount_button[data-id="' + asset_id + '"]').off('hidden.bs.popover');
                $('.amount_button[data-id="' + asset_id + '"]').on('hidden.bs.popover', function () {
                    refreshPage();
                });
            });
        });
    },

    updateBuySideAmount = () => {

        // Listen to any change in amount fields.
        $('.amount_detail').add('.property_dropdown').change(function () {
            const hba_id = $(this).attr('data-id');

            // Update changed field value in the DB.
            const update_amount = makeAjaxCall("homebase/update_amount", {
                id: hba_id,
                col: $(this).attr('data-name'),
                val: $(this).val().replace('$', '').replace('%', '').replace(/,/g, ''),
                table: 'homebase_buyer_asset',
                pk: 'hba_id'
            });

            update_amount.then(function (data) {
                // ID selectors.
                const price_selector = $('#b_price' + hba_id),
                    rate_selector = $('#b_rate' + hba_id),
                    gross_selector = $('#b_gross' + hba_id),
                    gsplit_selector = $('#b_gsplit' + hba_id),
                    gnet_selector = $('#b_gnet' + hba_id),
                    split_selector = $('#b_split' + hba_id),
                    net_selector = $('#b_net' + hba_id),

                    // Calculate different income fields.
                    price = numeral(price_selector.val())._value,
                    rate = numeral(rate_selector.val() + '%')._value,
                    gross = price * rate,
                    gsplit = numeral(gsplit_selector.val() + '%')._value,
                    gnet = gsplit * gross,
                    split = numeral(split_selector.val() + '%')._value,
                    net = split * gnet;

                // Set Format for different fields.
                gross_selector.val(numeral(gross).format('$0,0'));
                gnet_selector.val(numeral(gnet).format('$0,0'));
                net_selector.val(numeral(net).format('$0,0'));

                let popover = gnet_selector.closest('.popover-content');

                // Update Popover content.
                let updated_content = popover.clone();
                updated_content.find('.price').attr('value', price_selector.val());
                updated_content.find('.rate').attr('value', rate_selector.val());
                updated_content.find('.gross').attr('value', gross_selector.val());
                updated_content.find('.gsplit').attr('value', gsplit_selector.val());
                updated_content.find('.csplit').attr('value', split_selector.val());
                updated_content.find('.net').attr('value', net_selector.val());
                updated_content.find('.gnet').attr('value', gnet_selector.val());

                // Set the Pop over dummy content data attribute.
                // const amount_button_selector = $('#amount_button' + hba_id);
                // amount_button_selector.removeAttr('data-dummy-content');
                // amount_button_selector.attr('data-dummy-content', updated_content.html());

                // Update the net amount in the DB.
                makeAjaxCall("homebase/update_amount", {
                    id: hba_id,
                    col: 'hba_net',
                    val: net,
                    table: 'homebase_buyer_asset',
                    pk: 'hba_id'
                });

                // Popover hide event handler.
                // $('.amount_button[data-id="' + hba_id + '"]').off('hidden.bs.popover');
                // $('.amount_button[data-id="' + hba_id + '"]').on('hidden.bs.popover', function () {
                //     refreshPage();
                // });
            });
        });
    },

    updateValue = () => {

        // Listen to any change in amount fields.
        $('.value_detail').add('.property_dropdown').change(function () {
            const asset_id = $(this).attr('data-id');

            // Update changed field value in the DB.
            const update_amount = makeAjaxCall("homebase/update_amount", {
                id: asset_id, col: $(this).attr('data-name'),
                val: $(this).val().replace('$', '').replace('%', '').replace(/,/g, ''),
                table: 'homebase_client_asset',
                pk: 'hca_id'
            });

            update_amount.then(function (data) {
                // ID selectors.
                const price_selector = $('#value-price' + asset_id),
                    usable_sf_selector = $('#usable-sf' + asset_id),
                    gross_sf_selector = $('#gross-sf' + asset_id),
                    livable_sf_price_selector = $('#livable-sf-price' + asset_id),
                    gross_sf_price_selector = $('#gross-sf-price' + asset_id),

                    // Calculate different income fields.
                    price = numeral(price_selector.val())._value,
                    usable_sf = usable_sf_selector.val(),
                    gross_sf = gross_sf_selector.val(),
                    livable_sf_price = parseInt(price / usable_sf),
                    gross_sf_price = parseInt(price / gross_sf);

                // Set Format for different fields.
                livable_sf_price_selector.val(numeral(livable_sf_price).format('$0,0'));
                gross_sf_price_selector.val(numeral(gross_sf_price).format('$0,0'));

                let popover = price_selector.closest('.popover-content');

                // Update Popover content.
                let updated_content = popover.clone();
                updated_content.find('.price').attr('value', price_selector.val());
                updated_content.find('.usable_sf').attr('value', usable_sf);
                updated_content.find('.gross_sf').attr('value', gross_sf);
                updated_content.find('.livable_sf_price').attr('value', livable_sf_price_selector.val());
                updated_content.find('.gross_sf_price').attr('value', gross_sf_price_selector.val());

                // Set the Pop over dummy content data attribute.
                const amount_button_selector = $('#info_button' + asset_id);

                amount_button_selector.removeAttr('data-value-content');
                amount_button_selector.attr('data-value-content', updated_content.html());

                // Update the net amount in the DB.
                makeAjaxCall("homebase/update_amount", {
                    id: asset_id,
                    col: 'hca_gross_sf_price',
                    val: gross_sf_price,
                    table: 'homebase_client_asset',
                    pk: 'hca_id'
                });

                // Update the net amount in the DB.
                makeAjaxCall("homebase/update_amount", {
                    id: asset_id,
                    col: 'hca_livable_sf_price',
                    val: livable_sf_price,
                    table: 'homebase_client_asset',
                    pk: 'hca_id'
                });

                // Popover hide event handler.
                $('.value_button[data-id="' + asset_id + '"]').off('hidden.bs.popover');
                $('.value_button[data-id="' + asset_id + '"]').on('hidden.bs.popover', function () {
                    refreshPage();
                });
            });
        });
    },

    updateRevenue = () => {

        // Listen to any change in amount fields.
        $('.revenue_detail').add('.property_dropdown').change(function () {
            const asset_id = $(this).attr('data-id');

            // Update changed field value in the DB.
            const update_revenue = makeAjaxCall("homebase/update_revenue", {
                id: asset_id,
                col: $(this).attr('data-name'),
                type: $(this).attr('data-type'),
                val: $(this).val().replace('$', '').replace('%', '').replace(/,/g, '')
            });

            update_revenue.then(function (data) {

                let type_array = ['a', 'b'],
                    total_monthly_income = 0,
                    total_units = 0,
                    total_asset_sq_ft = 0;

                $.each(type_array, function (index, type) {
                    total_monthly_income += parseInt(revenue.har_rent_per_mo);
                    total_units += 1;
                    total_asset_sq_ft += parseInt(revenue.har_sq_ft);

                    const number = $('.revenue_detail[data-id="' + asset_id + '"][data-name="har_number"]' +
                        '[data-type="' + type + '"]').val(),
                        total_sq_ft = number *
                            $('.revenue_detail[data-id="' + asset_id + '"][data-name="har_sq_ft"]' +
                                '[data-type="' + type + '"]').val(),
                        total_income_per_mo = number *
                            $('.revenue_detail[data-id="' + asset_id + '"][data-name="har_rent_per_mo"]' +
                                '[data-type="' + type + '"]').val();

                    $('.total_sq_ft[data-id="' + asset_id + '"][data-type="' + type + '"]')
                        .val(total_sq_ft);

                    $('.total_rent_per_mo[data-id="' + asset_id + '"][data-type="' + type + '"]')
                        .val(total_income_per_mo);


                    makeAjaxCall("homebase/update_revenue", {
                        id: asset_id,
                        col: 'har_total_rent_per_mo',
                        type: type,
                        val: total_income_per_mo
                    });

                    makeAjaxCall("homebase/update_revenue", {
                        id: asset_id,
                        col: 'har_total_sq_ft',
                        type: type,
                        val: total_sq_ft
                    });
                });

                // Set the total input fields.
                $('#total-monthly-gross-income' + asset_id).val(numeral(total_monthly_income).format('$0,0'));
                $('#total-number-units' + asset_id).val(total_units);
                $('#total-livable-sf' + asset_id).val(total_asset_sq_ft);
                $('#total-yearly-gross-income' + asset_id).val(numeral(total_monthly_income * 12).format('$0,0'));
            });
        });
    },

    // Check if this is a Dummy Component.
    check_dummy_component = asset_id => {
        // Make all the buttons and select box disabled if it's a Dummy Component.
        if ('0' === asset_id) {
            $(":button, select option").prop('disabled', true).attr('title', 'Dummy Promotional Document.');
        }
    },

    // Returns Longitude and latitude coordinates for address.
    getLongLat = address => {

        try {
            var long_lat_promise = new Promise(function (resolve, reject) {
                $.ajax({
                    url: "https://maps.googleapis.com/maps/api/geocode/json?&address="
                    + address + "&key=AIzaSyBCwpF5xPvzTUNhXuKc-SKw655Pdty5qWs",
                    dataType: 'json',
                    type: 'post',
                    error: ajaxError
                }).done(function (data) {

                    if (data.status === 'failure') {
                        toastr.error(data.message);
                        return;
                    }
                    else if (data.status === 'redirect') {
                        window.location.href = data.redirect;
                        return;
                    }
                    resolve(data);
                }).fail(function (jqXHR, textStatus) {
                    toastr.error("Request failed: " + textStatus);
                }).always(function () {
                    reject('Error');
                });
            })
        } catch (e) {
            reject('Error');
            return false;
        }
        return long_lat_promise;
    };