$(document).ready(function() {
    refreshPage();
});

// Get the page data and display it.
function refreshPage() {

    // Get the list of tools
    $.ajax({
        url: "/user/get_tools",
        dataType: "json",
        type: 'post',
    }).done(function(data) {
        if (data.status == 'failure') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
	    window.location.href = data.redirect;
            return;
        }
        updateTools(data.groups);

    }).fail(function(jqXHR, status) {
        toastr.error("Error");
    });
}

// Update the list of Tool Groups
function updateTools(groups) {

    // Show each tool group
    $.each(groups, function(tgid, group) {

	// Create the collapsible panel.
	$('#tool-groups').append($('<div class="panel panel-default" id="panel-' + tgid + '">')
				 .append($('<div class="panel-heading" role="tab" id="tg-' + tgid + '">')
					 .append($('<h4 class="panel-title">')
						 .append($('<a role="button" data-toggle="collapse" data-parent="#tools-groups" href="#collapse-' + tgid + '" aria-expanded="true" aria-controls="collapse-' + tgid + '">')
							 .append(group.tg_name))))
				 .append($('<div id="collapse-' + tgid + '" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading-' + tgid + '">')
					 .append($('<div class="panel-body">')
						 .append($('<h4>')
							 .append(group.tg_description))
						 .append($('<table id="table-tg-' + tgid + '" class="table table-striped">')
							 .append($('<thead>')
								 .append($('<tr>')
									 .append($('<th>').append('Name'))
									 .append($('<th>').append('Description'))
									 .append($('<th>').append('Subscription Status'))
									 )
								 )
							 .append($('<tbody>')))))
				 );

	// Show each tool in the group.
	$.each(group.tools, function(tid, tool) {

	    // Does the user have access to this tool?
	    var status;
	    if (tool.ut_id == null)
		status = '<a class="btn btn-default" href="/' + tool.tool_controller + '/subscribe">Subscribe</a>';
	    else {

		// Convert the dates to moments.
		var now = moment();
		var eff_date = moment(tool.ut_eff_date);
		var term_date;
		if (tool.ut_term_date == null)
		    term_date = moment().add(1, 'days');
		else
		    term_date = moment(tool.ut_term_date);

		// Is the tool still active.
		if (now.isAfter(eff_date) && now.isBefore(term_date))
		    status = '<a class="btn btn-default" href="/' + tool.tool_controller + '/subscribe">Active - Click to Cancel</a>';
		else
		    status = '<a class="btn btn-default" href="/' + tool.tool_controller + '/subscribe">Reactivate</a>';
	    }

	    // Show the tool and status.
	    $('#tool-groups').find('#table-tg-' + tgid).find('tbody')
		.append($('<tr>')
			.append($('<td>')
				.append(tool.tool_name))
			.append($('<td>')
				.append(tool.tool_description
					+ ((tool.tool_is_admin == '1') ? ' <em><strong>Admin Only</strong></em>' : '')))
			.append($('<td>')
				.append(status))
			);
	});

    });

}
