$(document).ready(function() {
    refreshPage();
});

// Get the page data and display it.
function refreshPage() {

    // Are we viewing our own profile or someone else's?
    var pathname = window.location.pathname;
    var path_components = pathname.split("/");
    var user_id = path_components[path_components.length - 1];
    if (!$.isNumeric(user_id))
	user_id = 0;

    // Get the specified profile.
    $.ajax({
        url: "/user/get_profile",
        dataType: "json",
        type: 'post',
	data: {
	    user_id: user_id
	}
    }).done(function(data) {
        if (data.status == 'failure') {
            toastr.error(data.message);
            return;
        }
        updateProfile(data.profile);

    }).fail(function(jqXHR, status) {
        toastr.error("Error");
    });
}

// Update the profile.
function updateProfile(profile) {

    // Get the user.
    if (typeof profile.user === 'undefined')
	window.location.replace('/');
    var user = profile.user;

    // Show the name and the image
    $('#name').html(user.user_first_name + ' ' + user.user_last_name);
    $('#profile-image').attr('src', user.user_avatar);

    // If the profile is editable, create editable fields.
    if (profile.editable) {
	$('#first-name').html('<a href="#" class="editable" data-type="text" data-name="user.user_first_name" data-pk="' + user.user_id + '" data-url="/common/update_db_field">' + user.user_first_name + '</a>').show();

	$('#last-name').html('<a href="#" class="editable" data-type="text" data-name="user.user_last_name" data-pk="' + user.user_id + '" data-url="/common/update_db_field">' + user.user_last_name + '</a>').show();

	$('#email-address').html('<a href="#" class="editable" data-type="text" data-name="user.user_email" data-pk="' + user.user_id + '" data-url="/common/update_db_field">' + user.user_email + '</a>');

	// Show all of the links
	$('#facebook-link, #linkedin-link, #google-link').show();
	$('#facebook-linked, #linkedin-linked, #google-linked').hide();

	// Handle each authenticator
	$.each(user.authenticators, function(k, auth) {
	    if (auth.ua_authenticator === 'facebook')
		showFacebookLink(auth);
	    else if (auth.ua_authenticator === 'linkedin')
		showLinkedInLink(auth);
	    else if (auth.ua_authenticator === 'google')
		showGoogleLink(auth);
	});

	// Set the editable class as editable.
	setEditable();
    }
    else {
	$('#first-name').hide();
	$('#last-name').hide();
	$('#authenticators').hide();
	$('#email-address').html(user.user_email);
    }

}

// Show the Facebook information.
function showFacebookLink(facebook) {

    // Show the appropriate section
    $('#facebook-linked').show();
    $('#facebook-link').hide();
    $('#facebook-name').html(facebook.ua_data.displayName);
    $('#facebook-email').html(facebook.ua_data.email);
    $('#facebook-profile').html('<a href="' + facebook.ua_data.profileURL + '" target="_blank">' +
			       facebook.ua_data.profileURL + '</a>');
    $('#facebook-photo').html('<img src="' + facebook.ua_data.photoURL + '">');

}

// Show the LinkedIn information.
function showLinkedInLink(linkedin) {

    // Show the appropriate section
    $('#linkedin-linked').show();
    $('#linkedin-link').hide();
    $('#linkedin-name').html(linkedin.ua_data.displayName);
    $('#linkedin-email').html(linkedin.ua_data.email);
    $('#linkedin-profile').html('<a href="' + linkedin.ua_data.profileURL + '" target="_blank">' +
			       linkedin.ua_data.profileURL + '</a>');
    $('#linkedin-photo').html('<img src="' + linkedin.ua_data.photoURL + '">');

}

// Show the Google information.
function showGoogleLink(google) {

    // Show the appropriate section
    $('#google-linked').show();
    $('#google-link').hide();
    $('#google-name').html(google.ua_data.displayName);
    $('#google-email').html(google.ua_data.email);
    $('#google-profile').html('<a href="' + google.ua_data.profileURL + '" target="_blank">' +
			       google.ua_data.profileURL + '</a>');
    $('#google-photo').html('<img src="' + google.ua_data.photoURL + '">');

}
