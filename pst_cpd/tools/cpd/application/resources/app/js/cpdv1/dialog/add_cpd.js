/**
 *
 * Adds new Client Project Dashboard Instance.
 *
 * @summary      Adds Client's Project Dashboard instance.
 * @description  This file contains functions for adding new Client Project Dashboard Instance.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */

$(document).ready(function () {

    // Validate the add cpd form and submit if valid.
    $('#add-cpd-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            cpd_product_id: {
                validators: {
                    notNegative: {
                        message: "The Product ID is required."
                    },
                    notZero: {
                        message: "The Product ID is required."
                    }
                }
            },
            name: {
                validators: {
                    notEmpty: {
                        message: "The DMD name is required."
                    }
                }
            },
            description: {
                validators: {
                    notEmpty: {
                        message: "The DMD description is required."
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the form cpd
        const $form = $(e.target),
            fv = $form.data('formValidation'),

            // Get the form data and submit it.
            cpd_data = $form.serialize(),

            //Adds a new DMD instance.
            add_cpd = makeAjaxCall('cpdv1/add_cpd', cpd_data);

        add_cpd.then(function (data) {
            $($form)[0].reset();
            fv.resetForm();
            $('#add-cpd-dialog').modal('hide');
            refreshProducts();
            refreshPage();
        });
    });

    $('#cpd-year').empty();

    // Initialize the year dropdown.
    updateYears('#cpd-year');

    // Update the TGD drop down based on selection of profession
    $(document).on('change', '#cpd-product-id', function () {

        const product_id = $('option:selected', this).val();

        // If the "default" product is 0, then raise the add-product dialog.
        if (0 === product_id) {
            $('#add-product-dialog').modal('show');
            $('#current-product').val("-1");
            $('#add-cpd-dialog').modal('hide');
            return false;
        }
    });

    /**
     *
     * Update DMD Products list.
     */
    function updateDMDProducts(products) {

        const cpd_product_id_selector = $('#cpd-product-id');
        cpd_product_id_selector.empty().attr('disabled', false);

        // Show the industries for selection.
        cpd_product_id_selector.append($('<option>')
            .attr({value: -1})
            .attr({disabled: true})
            .attr({selected: true})
            .append("-- Select Product --"));

        $.each(products, function (i, product) {
            cpd_product_id_selector.append('<option style="font-weight: bold; font-style: italic;"' +
                ' data-id="' + product[0]['u_product_group_id'] + '"' +
                ' value="' + product[0]['u_product_group_id'] + '" data-type="group">' + i + '</option>');


            $.each(product, function (index, data) {
                cpd_product_id_selector.append($('<option>')
                    .attr({value: data.u_product_id})
                    .append(data.u_product_name));
            });

            cpd_product_id_selector.append('</optgroup>');
        });
    }

    $('#add-cpd-dialog').on('show.bs.modal', function () {

        // Fetches products type.
        const get_products = makeAjaxCall('industry/get_all_products', {});
        // Updates product list.
        get_products.then(function (data) {
            refreshProducts();

            updateDMDProducts(data.products);
        });
    });
});

