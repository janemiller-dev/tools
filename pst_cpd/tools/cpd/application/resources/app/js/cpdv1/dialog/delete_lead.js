/**
 *
 * Delete Lead.
 *
 * @summary      Delete Dialog to delete DMD components.
 * @description  This file contains functions for Deleting different DMD components.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */
$(document).ready(function () {

    // When the dialog is displayed, set the current instance ID.
    $('#delete-lead-dialog').on('show.bs.modal', function (event) {

        // Hide any Previous instance of view
        $('#view-lead-dialog').modal('hide');
        $('#view-source-dialog').modal('hide');

        // Fetch the ID/Name/Type.
        const ui_id = $(event.relatedTarget).data('id'),
            ui_name = $(event.relatedTarget).data('name'),
            type = $(event.relatedTarget).data('type');

        // Set the Title and Name of pop up.
        $('#delete-lead-title').html('Delete ' + type.charAt(0).toUpperCase() + type.slice(1));
        $('#delete-lead-id').val(ui_id);
        $('#delete-lead-name').html(ui_name);
        $('#delete-lead-type').val(type);

    });

    // Validate delete instance form and submit if valid.
    $('#delete-lead-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {}
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the form instance
        const $form = $(e.target),
            fv = $form.data('formValidation'),

            // Get the form data and submit it.
            message_data = $form.serialize(),

            // Delete Lead instance.
            delete_lead = makeAjaxCall('cpdv1/delete_lead', message_data);

        delete_lead.then(function (data) {
            $('#delete-lead-dialog').modal('hide');
            fv.resetForm();
            refreshPage();
        });
    });
});