/**
 *
 * Displays and updates Information/Reference for a client.
 *
 * @summary       Display and update Information/Reference for a client.
 * @description  This file contains functions for Displaying and updating existing Client Reference/Information.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */
let client_id,
    info_type,
    assets_info = '',
    leads = '',
    buyer_type = 'false',
    owner_type = 'false'
    sources = '';
$(document).ready(function () {

    $(document).on('click', '#info-send-email', function () {
        $('#send-email-dialog').modal('show');
    });

    $(document).on('click', '#info-visit-web', function () {
        window.open($(this).parent().parent().siblings('input').val())
    });

    // Call to generate initial layout for page as modal is shown.
    $('#client-info-dialog').on('show.bs.modal', function (event) {
        $('#buyer-buttons-div, #owner-buttons-div, #owner-info-div, #owner-info-buyer-div,' +
            ' #add-buyer-offer, #agent-info-row, #notes-div, #agent-info-row, #client-image-div,' +
            ' #add-counter-offer, #add-retrade-offer').addClass('hidden');
        $('.placeholder-div').removeClass('hidden');

        buyer_type = $(event.relatedTarget).data('buyer');
        buyer_id = $(event.relatedTarget).data('id');

        const get_asset_info = makeAjaxCall('homebase/get_asset_info', {});
        get_asset_info.then(function (data) {
            assets_info = data.asset_info;
        });

        const get_lead_source = makeAjaxCall('homebase/get_lead_source', {});

        get_lead_source.then(function (data) {
            leads = data.lead_source.lead;
            sources = data.lead_source.source;
        });
        client_info.init(event);
    });

    // Client Info dialog on shown event.
    $('#client-info-dialog').on('shown.bs.modal', function (event) {
        // Display owner info by default
        // $('#info-Partner').val(-2).trigger('change');
    });
});

/*
*  Client information to be displayed in the info pop up.
*/
let client_info = function () {
    let active_id,
        updated_data,
        add_info_block_selector,

        // Initialize the function.
        init = function (event) {
            add_info_block_selector = $('#add-info-block');

            $('#client-image-div').css('visibility', 'hidden');
            $('#client-info').empty();
            $('#client-contact').empty();
            // Hides the add info block button.
            add_info_block_selector.addClass('hidden');
            $('#ref-source-div').remove();

            // Remove additional rows (if any)
            $('.additional-row').remove();
            $('#note_date').html('Date:');

            info_type = (undefined !== $(event.relatedTarget).data('type') ?
                $(event.relatedTarget).data('type') : 'info');

            // Checks if the request is for a info pop up or for a reference pop up and accordingly sets the content.
            // For reference pop up.
            if ('ref' === $(event.relatedTarget).data('type')) {
                // Set title and labels
                $('#client-info-title').text('Referral Information');
                $('#info-advisor-label').text('Investor');
                $('#info-contractor-label').text('Developer');

                // Add referral source and event text box.
                $('<div class="col-xs-12" id="ref-source-div" style="padding: 10px">' +
                    '<div class="col-xs-6">' +
                    '<label class="col-xs-12 col-sm-12 col-md-5 col-lg-5 control-label" for="first_name">' +
                    'Referral Source</label>' +
                    '<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">' +
                    '<input type="text" style="display: inline" data-col="cc_ref_source" ' +
                    ' class="form-control info-col" name="ref_source" id="ref_source"/></div></div>' +
                    '<div class="col-xs-6">' +
                    '<label class="col-xs-12 col-sm-12 col-md-5 col-lg-5 control-label" ' +
                    'for="first_name">Event</label><div class="col-xs-12 col-sm-12 col-md-7 col-lg-7">' +
                    '<input type="text" style="display: inline" class="form-control info-col" ' +
                    'data-col="cc_ref_event" name="ref_event" id="ref_event"/>' +
                    '</div></div></div>').insertBefore($('#company-container'));

            } else {
                // Set title and labels
                $('#info-advisor-label').text('Advisors');
                $('#info-contractor-label').text('Vendors');
            }

            // Make info pop up drop down empty.
            const info_col_selector = $('.info-col');
            info_col_selector.removeAttr('data-id');
            info_col_selector.removeData('id');
            $(event.relatedTarget).removeAttr('data-type');

            // Remove data attributes from notes column.
            const info_note_selector = $('.info-note');
            info_note_selector.removeAttr('data-id');
            info_note_selector.removeData('id');
            active_id = -1;

            // Remove data attibutes from info column.
            const add_info_col_selector = $('#add-info-row');
            add_info_col_selector.removeData('id');
            add_info_col_selector.removeAttr('data-id');

            $('.info-select').empty();
            info_col_selector.val('');
            info_note_selector.val('');

            // Append initial options for partner info drop downs.
            $('#info-Partner').append('<option disabled selected value="-1" >-- Select Partner --</option>\n' +
                '<option value="0" data-type="' + info_type + '">Add new Partner</option>');

            // Append owner to the string.
            $('#info-Partner').append('<option value="-2" data-type="owner" data-index="-2">(( Owner ))</option>');

            // Append initial options for staff info drop downs.
            $('#info-Staff').append('<option disabled selected value="-1">-- Select Staff --</option>\n' +
                '<option value="0" data-type="' + info_type + '">Add new Staff</option>');

            // Append initial options for advisor info drop downs.
            $('#info-Advisor').append('<option disabled selected value="-1">-- Select Advisor --' +
                '</option>\n <option value="0"  data-type="'
                + info_type + '">Add new Advisor</option>');

            // Append initial options for vendor info drop downs.
            $('#info-Buyer').append('<option disabled selected value="-1"  ' +
                'data-type="' + info_type + '">-- Select Buyer --</option>\n' +
                '<option value="0">Add new Buyer</option>');

            // Get the client ID.
            client_id = ("undefined" === typeof $(event.relatedTarget).data('id'))
                ? client_id : $(event.relatedTarget).data('id');

            $(this).find('form').attr('id', 'info_form' + client_id);
            if (client_id !== 0) {
                // Fetches Property information.
                const get_prop_info = makeAjaxCall("cpdv1/get_prop_info", {
                    client_id: client_id,
                    type: info_type,
                    is_buyer: buyer_type
                });

                get_prop_info.then(function (data) {
                    updated_data = data;

                    // Adds options to drop down.
                    $.each(data.prop_info, function (index, info) {
                        $('#info-' + info.cc_type).append('<option data-index="' + index + '" value="' + info.cc_id + '">'
                            + info.cc_name + '</option>');
                    });

                    const info_select_selector = $('.info-select');

                    // Looks for a info drop down change event.
                    info_select_selector.unbind().change(function () {

                        const index = $(this).find(':selected').data('index');
                        if (0 === parseInt($(this).val())) {

                            // Hides add new block button.
                            add_info_block_selector.addClass('hidden');

                            $(this).attr('data-client-id', client_id);
                            $(this).attr('data-info-type', info_type);
                            active_id = -1;
                            $('#add-client-info-dialog').modal('show');
                            $('#client-info-dialog').modal('hide');
                            $('.info-select').val(-1);
                            $('#client-id').val(client_id);

                            $('#add-client-info-title').text('Add new ' + $(this).data('name'));
                            $('#relation-type').val($(this).data('name'));
                        } else {

                            // Shows add new block button.
                            add_info_block_selector.removeClass('hidden');
                            $('.additional-row').remove();
                            $('#note_date').html('Date:');

                            // Check if index variable is set.
                            if (-2 === index) {
                                const owner_id = $(event.relatedTarget).attr('data-owner'),
                                    get_owner_info = makeAjaxCall('homebase/get_owner_info', {'owner_id': owner_id});

                                // Image upload click handler.
                                $(document).off('click', '#upload-info-img');
                                $(document).on('click', '#upload-info-img', function () {
                                    $('#upload-owner-img').click();
                                });

                                const get_asset_info = makeAjaxCall('homebase/get_asset_info', {});
                                get_asset_info.then(function (data) {
                                    assets_info = data.asset_info;
                                });

                                const get_lead_source = makeAjaxCall('homebase/get_lead_source', {});

                                get_lead_source.then(function (data) {
                                    leads = data.lead_source.lead;
                                    sources = data.lead_source.source;
                                });

                                // Profile button popover event handler
                                $('.profile-btn').on('shown.bs.popover', function () {

                                    // Fetch the info about client.
                                    const get_owner_popup_info = makeAjaxCall('homebase/get_owner_popup_info', {
                                        'owner_id': owner_id
                                    });

                                    // Append info to the appropriate textbox.
                                    get_owner_popup_info.then(function (data) {
                                        $.each(data.owner_info, function (index, info) {
                                            $('textarea[data-type=' + info.hoi_type + ']').val(info.hoi_val);
                                        })
                                    });

                                    // Change of field event handler.
                                    $('.owner-popup-info').off('change');
                                    $('.owner-popup-info').change(function (data) {
                                        makeAjaxCall('homebase/save_owner_popup_info', {
                                            'owner_id': owner_id,
                                            'val': $(this).val(),
                                            'type': $(this).attr('data-type')
                                        });
                                    })
                                });

                                // Remove data attribute.
                                // $(event.relatedTarget).removeAttr('data-id');
                                $('#edit-owner-profile-button').attr('data-id', owner_id);

                                // Append data after fetching.
                                get_owner_info.then(function (ret_data) {

                                    console.log('her');
                                    $('#client-image-div').css('visibility', 'visible');
                                    $('#client-info, #client-contact, #client-image-div').removeClass('hidden');
                                    $('#owner-info-div, #owner-info-buyer-div,' +
                                        ' #add-counter-offer').removeClass('hidden');
                                    $('#owner-buttons-div, #agent-info-row, #add-buyer-offer, #buyer-buttons-div,' +
                                        ' #add-retrade-offer').addClass('hidden');
                                    $('.placeholder-div').addClass('hidden');

                                    const owner_info_selector = $('#client-info'),
                                        owner_contact_selector = $('#client-contact'),
                                        data = ret_data.owner_info[0];

                                    // Check id Owner Avatar is set.
                                    if (null !== data.hc_avatar) {
                                        url = window.location.origin + '/tools/homebase/get_owner_image/?img=' + data.hc_avatar;
                                        $('#info-avatar').attr('src', url);
                                    } else {
                                        $('#info-avatar').attr('src', '/tools/resources/app/media/user-avatar.svg');
                                    }

                                    // Append Owner Info.
                                    owner_info_selector.empty();
                                    owner_info_selector.append('<p style="font-size: 30px">' +
                                        ((null === data.hc_name || '' === data.hc_name) ? '' : data.hc_name) + '</p>');
                                    owner_info_selector.append('<p style="font-size: 20px"><b> ' +
                                        ((null === data.hc_title || '' === data.hc_title) ? 'Owner' : data.hc_title) + '</b></p>');
                                    owner_info_selector.append('<p style="font-size: 18px">' +
                                        ((null === data.hc_entity_name || '' === data.hc_entity_name) ? '' : data.hc_entity_name) + '</p>');
                                    owner_info_selector.append('<p>' + ((null === data.hc_entity_address || '' === data.hc_entity_address)
                                        ? '' : data.hc_entity_address) + '</p>');

                                    // Append Contact Info.
                                    owner_contact_selector.empty();
                                    owner_contact_selector.append('<p>' +
                                        ((null === data.hc_main_phone || '' === data.hc_main_phone) ? '' : data.hc_main_phone) + ' (Main)</p>');
                                    owner_contact_selector.append('<p>' +
                                        ((null === data.hc_second_phone || '' === data.hc_second_phone) ? '' : data.hc_second_phone) +
                                        ' (Second)</p>');
                                    owner_contact_selector.append('<p><a href="mailto:'
                                        + ((null === data.hc_client_email || '' === data.hc_client_email) ? '' : data.hc_client_email) + '">'
                                        + ((null === data.hc_client_email || '' === data.hc_client_email) ? '' : data.hc_client_email) +
                                        '</a></p>');
                                    owner_contact_selector.append('<p><a href="'
                                        + ((null === data.hc_website || '' === data.hc_website) ? '' : data.hc_website) + '">'
                                        + ((null === data.hc_website || '' === data.hc_website) ? '' : data.hc_website) + '</a></p>');
                                    owner_contact_selector.append('<p>'
                                        + ((null === data.hc_messaging || '' === data.hc_messaging) ? '' : data.hc_messaging) + '</p>');
                                    owner_contact_selector.append('<p>'
                                        + ((null === data.hc_social || '' === data.hc_social) ? '' : data.hc_social) + '</p>');

                                    const get_asset_info = makeAjaxCall('homebase/fetch_more_row', {
                                        'id': owner_id,
                                        'lower_limit': 0,
                                        'upper_limit': 100,
                                        'query': ''
                                    });

                                    get_asset_info.then(function (data) {
                                        const client_data = data.client;
                                        $('#owner-info-asset-table').empty()

                                        $.each(client_data, function (index, client) {
                                            append_owner_asset_head(client, index + 1, 'owner-info-asset-table', 'info-');
                                            append_owner_asset_body(client, 'info-add-', index + 1, assets_info, leads, sources, portfolio);
                                        });

                                        $('#asset-div').find('button, input, select, a').attr('disabled', 'disabled');

                                        // Append Deal Types and Criteria to deal type and criteria select box.
                                        let request = [],
                                            products = '';
                                        const get_deal = get_all_deal_type();
                                        request.push(get_deal);

                                        // Get all criteria and location.
                                        const get_criteria = get_all_criteria();
                                        request.push(get_criteria);

                                        // Fetch all the products from DB.
                                        const get_all_homebase_products = makeAjaxCall('industry/get_all_products');

                                        get_all_homebase_products.then(function (data) {
                                            products = data.products;
                                        });

                                        request.push(get_all_homebase_products);

                                        Promise.all(request).then(function (data) {

                                            // Iterate through each client data.
                                            $.each(client_data, function (index, client) {
                                                appendOwnerProfile(client, products);
                                            });
                                        });

                                    })
                                });

                                // Upload Owner Image Click handler.
                                $(document).off('click', '#upload-owner-img');
                                $(document).on('click', '#upload-owner-img', function () {
                                    $('#owner-img-upload').trigger('click');

                                    const target = 'owner-img-upload',
                                        file_type_allowed = /.\.(png|jpeg|jpg)/i,
                                        data = {
                                            'owner_id': owner_id
                                        };

                                    // Upload Image file.
                                    const upload_image = uploadFile(target, data, file_type_allowed);
                                    upload_image.then(function (data) {
                                        $('#view-owner-profile-dialog').modal('hide');
                                        $('#client-info-dialog').modal('hide');
                                        page_added = 1;
                                        refreshPage();
                                    });
                                });

                                const val = $(this).val();
                                $('.info-select').val(-1);
                                $(this).val(val);
                            }
                            else if (undefined !== index) {
                                active_id = updated_data.prop_info[index].cc_id;
                                const val = $(this).val();
                                $('.info-select').val(-1);
                                $(this).val(val);
                                displayInfo(updated_data.prop_info[index]);
                            }
                        }
                    });

                    if (true === buyer_type) {
                        $('#client-info-title').text('Buyer Information');
                        $('#name-placeholder-div').find('p').eq(0).text('Buyer Name');
                        $('#name-placeholder-div').find('p').eq(1).text('Buyer Title');
                        $('#name-placeholder-div').find('p').eq(2).text('Buyer Entity');
                        $('#info-Buyer').val($('#buyer-drop' + buyer_id).val()).trigger('change');
                    } else {
                        $('#client-info-title').text('Client Information');
                        $('#name-placeholder-div').find('p').eq(0).text('Client Name');
                        $('#name-placeholder-div').find('p').eq(1).text('Title');
                        $('#name-placeholder-div').find('p').eq(2).text('Entity');
                    }
                });
            }

            // Updates Content
            info_col_selector.unbind().change(function () {
                updateInfo(active_id, $(this).data('col'), $(this).val());
            });

            // Set the notes column content.
            $('#owner-info-notes').off('change');
            $('#owner-info-notes').change(function () {
                const date = moment().format("DD-MM-YYYY hh:mm A");
                const data = $(this).val();

                updateInfo(active_id, 'cc_notes', data);
                updateInfo(active_id, 'cc_note_date', date);
            });

            // Adds new block to pop up.
            let dummy_content;
            $(document).off('change', '.add-block');
            $(document).on('change', '.add-block', function () {
                if ('phone' === $(this).val()) {

                    //Set HTML for phone's additional row.
                    dummy_content = $('.phone-info').clone();
                    dummy_content.removeClass('hidden');
                    dummy_content.find('.add-phone-info').removeClass('add-phone-info')
                        .addClass('additional-phone-info');
                    dummy_content = '<div class="additional-row">' + dummy_content.html() + '</div>';
                    $('#phone-container').append(dummy_content);
                } else if ('email-web' === $(this).val()) {

                    // Set HTML for email's additional row.
                    dummy_content = $('.email-info').clone();
                    dummy_content.removeClass('hidden');
                    dummy_content.find('.add-email-info').removeClass('add-email-info')
                        .addClass('additional-email-info');
                    dummy_content = '<div class="additional-row">' + dummy_content.html() + '</div>';
                    $('#web-container').append(dummy_content);
                } else if ('address' === $(this).val()) {

                    // Set HTML for address's additional row.
                    dummy_content = $('.address-info').clone();
                    dummy_content.addClass('additional-row');
                    dummy_content.removeClass('hidden');
                    dummy_content.find('.add-address-info').removeClass('add-address-info')
                        .addClass('additional-address-info');
                    dummy_content = '<div class="additional-row">' + dummy_content.html() + '</div>';
                    $('#address-container').append(dummy_content);
                } else if ('message-social' === $(this).val()) {
                    dummy_content = $('.message-info').clone();
                    dummy_content.addClass('additional-row');
                    dummy_content.removeClass('hidden');
                    dummy_content.find('.add-message-info').removeClass('add-message-info')
                        .addClass('additional-message-info');
                    dummy_content = '<div class="additional-row">' + dummy_content.html() + '</div>';
                    $('#social-media-container').append(dummy_content);
                }

                additionalInfoEvent();
            });
        },

        /*
         * Set value for different info pop up fields.
         */
        displayInfo = function (obj) {

            $('#client-info, #client-contact, #client-image-div').removeClass('hidden');
            $('.placeholder-div').addClass('hidden');

            const get_buyer_info = makeAjaxCall('cpdv1/get_buyer_offer', {
                asset_id: obj.cc_deal_id
            });

            get_buyer_info.then(function (data) {
                $('#owner-info-buyer-div').empty();

                $.each(data.buyer_offer, function (index, data) {
                    let dummy_content = $('#buyer-offer-div').clone();
                    dummy_content.removeAttr('id');
                    dummy_content.removeClass('hidden');
                    dummy_content.find('#buyer-name').text(data.cc_name +
                        ' – ' + data.hbi_type.charAt(0).toUpperCase() +
                        data.hbi_type.slice(1) + ' Submission: ' + data.hbi_created_on);
                    dummy_content.find('#offer-text').val(data.hbi_offer);
                    dummy_content.find('#ca-text').val(data.hbi_ca);
                    dummy_content.find('#tour-date-text').val(data.hbi_tour_date);
                    dummy_content.find('#offer-contingencies-text').val(data.hbi_offer_contingencies);
                    dummy_content.find('#funds-proof-text').val(data.hbi_proof);
                    dummy_content.find('#financing-method-text').val(data.hbi_financing);
                    dummy_content.find('#purchase-term-text').val(data.hbi_terms);
                    dummy_content.find('#fee-arrangement-text').val(data.hbi_fee);
                    dummy_content.find('input').attr('data-seq', data.hbi_seq)
                        .attr('data-id', data.hbi_asset_id).addClass('hbi_input_data');

                    $('#owner-info-buyer-div').append(dummy_content);
                });
            });

            $(document).on('change', '.hbi_input_data', function () {
                makeAjaxCall('cpdv1/update_hbi_data', {
                    'col': $(this).attr('data-col'),
                    'val': $(this).val(),
                    'seq': $(this).attr('data-seq'),
                    'id': $(this).attr('data-id')
                });
            });

            // Fetch the data for popover
            $('.client-info-btn').off('shown.bs.popover');
            $('.client-info-btn').on('shown.bs.popover', function () {
                const get_popup_info = makeAjaxCall('cpdv1/get_client_popup_info', {
                    'id': obj.cc_id,
                    'form-type': 'client'
                });

                get_popup_info.then(function (data) {
                    $.each(data.info, function (index, info) {
                        $('textarea[data-type=' + info.cdai_type + ']').val(info.cdai_val);
                    })
                })
            });

            $('.job-field').change(function () {
                const update_job_field = makeAjaxCall('cpdv1/update_job_field', {
                    val: $(this).val(),
                    id: obj.cc_id,
                    col: $(this).attr('data-col')
                });

                update_job_field.then(function () {
                    console.log('here');
                })
            });

            // Change of field event handler.
            $(document).off('change', '.client-popup-info');
            $(document).on('change', '.client-popup-info', function (data) {
                makeAjaxCall('cpdv1/save_client_popup_info', {
                    'id': obj.cc_id,
                    'val': $(this).val(),
                    'type': $(this).attr('data-type'),
                    'form-type': 'client'
                });
            });

            $('#client-image-div').css('visibility', 'visible');

            // Set Job title and description value.
            $('#job-title').val(obj.cc_job_title);
            $('#job-desc').text(obj.cc_job_desc);

            // Upload Image functionality.
            $(document).off('click', '#upload-info-img');
            $(document).on('click', '#upload-info-img', function () {
                $('#info-img-upload').trigger('click');
                const target = 'info-img-upload',
                    file_type_allowed = /.\.(png|jpeg|jpg)/i,
                    data = {
                        'cc_id': obj.cc_id
                    };

                // Upload a Document file.
                const upload_image = uploadFile(target, data, file_type_allowed);
                upload_image.then(function (data) {
                    page_added = 1;
                    $('#client-info-dialog').modal('hide');
                    refreshPage();
                });
            });

            const client_info_selector = $('#client-info'),
                client_contact_selector = $('#client-contact');

            if (null !== obj.cc_image) {
                url = window.location.origin + '/tools/cpdv1/get_info_image/?img=' + obj.cc_image;
                $('#info-avatar').attr('src', url);
            } else {
                $('#info-avatar').attr('src', '/tools/resources/app/media/user-avatar.svg');
            }

            client_info_selector.empty();
            client_info_selector.append('<p style="font-size: 30px">' + obj.cc_name + '</p>');
            client_info_selector.append('<p style="font-size: 20px"><b>' + (
                null === obj.cc_title ? '' : obj.cc_title) + '</b></p>');
            // client_info_selector.append('<p style="font-size: 20px"><b>' + obj.cc_type + '</b></p>');
            client_info_selector.append('<p style="font-size: 18px">' + obj.cc_entity + '</p>');
            client_info_selector.append('<p>' + obj.cc_address + '</p>');

            client_contact_selector.empty();
            client_contact_selector.append('<p>' + obj.cc_office_phone + ' Office</p>');
            client_contact_selector.append('<p>' + obj.cc_cell_phone + ' Cell</p>');
            client_contact_selector.append('<p><a href="mailto:' + obj.cc_email + '">' + obj.cc_email + '</a></p>');
            client_contact_selector.append('<p><a href="' + obj.cc_website + '">' + obj.cc_website + '</a></p>');
            client_contact_selector.append('<p>' + obj.cc_messaging + '</p>');
            client_contact_selector.append('<p>' + obj.cc_social + '</p>');

            $.each(obj, function (index, info) {
                const info_popup_selector = $('#' + index.replace('cc_', ''));
                info_popup_selector.val(info);
                info_popup_selector.attr('data-id', obj.cc_id);
                info_popup_selector.attr('data-col', index);
            });

            if (obj.cc_note_date != null) {
                $('#note-date').text(obj.cc_note_date);
                $('#owner-info-notes').text(obj.cc_notes);
            }

            // Check if the info is for a Owner.
            if (obj.cc_type == 'Partner') {

                $('#owner-buttons-div, #owner-info-div, #owner-info-buyer-div, #add-counter-offer').removeClass('hidden');
                $('#agent-info-row, #add-buyer-offer, #buyer-buttons-div, #add-retrade-offer').addClass('hidden');

                // Fetch info about asset.
                const get_asset_info = makeAjaxCall('homebase/get_asset_data', {
                    'asset_id': client_id
                });

                let client = '';
                get_asset_info.then(function (data) {
                    // Remove owner info.
                    $('#owner-info-asset-table table').remove();

                    let portfolio = data.portfolio;
                    client = data.client[0];
                    append_owner_asset_head(client, 1, 'owner-info-asset-table');
                    append_owner_asset_body(client, 'add-', 1, assets_info, leads, sources, portfolio);

                    $('#owner-info-asset-table').find('button, input, select, a').attr('disabled', 'disabled');

                    // Check if the deal status is hard or in contract.
                    if ('hard' === client.hca_status || 'contract' === client.hca_status) {
                        $('#owner-info-buyer-div').addClass('hidden');

                    }
                });

                // Append Deal Types and Criteria to deal type and criteria select box.
                let request = [],
                    products = '';
                const get_deal = get_all_deal_type();
                request.push(get_deal);

                // Get all criteria and location.
                const get_criteria = get_all_criteria();
                request.push(get_criteria);

                // Fetch all the products from DB.
                const get_all_homebase_products = makeAjaxCall('industry/get_all_products');

                get_all_homebase_products.then(function (data) {
                    products = data.products;
                });

                request.push(get_all_homebase_products);

                Promise.all(request).then(function (data) {
                    appendOwnerProfile(client, products);

                });
            } else if (obj.cc_type === 'Buyer') {

                $('#buyer-buttons-div, #owner-info-div, #add-buyer-offer, #add-retrade-offer, #owner-info-buyer-div')
                    .removeClass('hidden');
                $('#owner-buttons-div, #agent-info-row, #add-counter-offer').addClass('hidden');

                // Fetch info about asset.
                const get_asset_info = makeAjaxCall('homebase/get_asset_data', {
                    'asset_id': client_id
                });

                let client = '';
                get_asset_info.then(function (data) {
                    // Remove owner info.
                    $('#owner-info-asset-table table').remove();

                    let portfolio = data.portfolio;
                    client = data.client[0];
                    append_owner_asset_head(client, 1, 'owner-info-asset-table');
                    append_owner_asset_body(client, 'add-', 1, assets_info, leads, sources, portfolio);

                    $('#owner-info-asset-table').find('button, input, select, a').attr('disabled', 'disabled');
                });
            }
            else {
                $('#owner-buttons-div, #owner-info-div, #add-buyer-offer, #owner-info-buyer-div,' +
                    ' #buyer-buttons-div, #add-retrade-offer, #add-counter-offer').addClass('hidden');
                $('#agent-info-row').removeClass('hidden');
            }

            // Add new Buyer/Offer click handler.
            $('.add-info-offer').off('click');
            $('.add-info-offer').click(function () {
                let dummy_content = $('#buyer-offer-div').clone();
                dummy_content.removeAttr('id');
                dummy_content.removeClass('hidden');

                $('#owner-info-buyer-div').append(dummy_content);

                const add_buyer_offer = makeAjaxCall('cpdv1/add_buyer_offer', {
                    id: obj.cc_deal_id,
                    date: moment().format(" DD-MM-YY hh:mm A"),
                    cc_id: obj.cc_id,
                    seq: $('#owner-info-buyer-div').find('> div').length,
                    type: $(this).attr('data-type')
                })

                add_buyer_offer.then(function (data) {
                    console.log(data);
                })
            });

            usePopover();
            additionalInfoEvent();

            $('#add-info-row').attr('data-id', obj.cc_id);
        },

        /*
        * Append Owner Profile options.
        */
        appendOwnerProfile = function (client, products) {
            const owner_profile_product_selector = $('#owner-product' + client.hca_id);

            owner_profile_product_selector.empty();
            owner_profile_product_selector
                .append('<option value="-1" disabled selected>-- Select Product Type --</option>');

            $.each(products, function (index, product) {
                owner_profile_product_selector
                    .append('<option style="font-weight: bold; font-style: italic;"' +
                        ' value="' + product[0]['u_product_group_id'] + '">' + index + '</option>');

                $.each(product, function (index, individual_product) {
                    owner_profile_product_selector
                        .append('<option value="' + individual_product.u_product_id + '">' +
                            individual_product.u_product_name + '</option>');
                });
            });

            owner_profile_product_selector
                .append('<option value="0" style="font-weight:bold;">None of the Above</option>');

            $('#owner-status' + client.hca_id).val(client.hca_status)
                .attr('data-prev', client.hca_status);
            $('#owner-criteria' + client.hca_id).val(client.hca_criteria_used);
            $('#owner-location' + client.hca_id).val(client.hca_location);
            $('#owner-product' + client.hca_id).val(client.hca_product_id);
        },

        /*
        * Function to check if any additional info is changed.
        */
        additionalInfoEvent = function () {

            const additional_email_info_selector = $('.additional-email-info');

            // Check if email info is changed.
            additional_email_info_selector.change(function () {
                let obj = {};

                // Generates JSON object for email field.
                $.each(additional_email_info_selector, function (index, info) {
                    if (0 === (index % 2)) {
                        let col1 = info.getAttribute('data-type');
                        const col2 = additional_email_info_selector[++index].getAttribute('data-type');
                        obj[index] = {};
                        obj[index][col1] = info.textContent;
                        obj[index][col2] = $('.additional-email-info')[index].value;
                    }
                });
                obj = JSON.stringify(obj);
                updateInfo(active_id, 'cc_add_email', obj);
            });


            const additional_phone_info_selector = $('.additional-phone-info');

            // Check if Phone info is changed.
            additional_phone_info_selector.change(function () {
                let obj = {};
                // Generates JSON object for phone field.
                $.each(additional_phone_info_selector, function (index, info) {
                    if (0 === (index % 2)) {
                        const col1 = info.getAttribute('data-type');
                        const col2 = additional_phone_info_selector[++index].getAttribute('data-type');
                        obj[index] = {};
                        obj[index][col1] = info.textContent;
                        obj[index][col2] = additional_phone_info_selector[index].value;
                    }
                });
                obj = JSON.stringify(obj);
                updateInfo(active_id, 'cc_add_phone', obj);
            });

            const additional_address_info = $('.additional-address-info');

            // Check if address info is changed.
            additional_address_info.change(function () {
                let obj = {};

                // Generates an JSON object for address field.
                $.each(additional_address_info, function (index, info) {
                    if (0 === (index % 2)) {
                        const col1 = info.getAttribute('data-type');
                        const col2 = additional_address_info[++index].getAttribute('data-type');
                        obj[index] = {};
                        obj[index][col1] = info.textContent;
                        obj[index][col2] = additional_address_info[index].value;
                    }
                });
                obj = JSON.stringify(obj);
                updateInfo(active_id, 'cc_add_address', obj);
            });

            const additional_messaging_info = $('.additional-message-info');

            // Check if address info is changed.
            additional_messaging_info.change(function () {
                let obj = {};

                // Generates an JSON object for address field.
                $.each(additional_messaging_info, function (index, info) {
                    if (0 === (index % 2)) {
                        const col1 = info.getAttribute('data-type');
                        const col2 = additional_messaging_info[++index].getAttribute('data-type');
                        obj[index] = {};
                        obj[index][col1] = info.textContent;
                        obj[index][col2] = additional_messaging_info[index].value;
                    }
                });
                obj = JSON.stringify(obj);
                updateInfo(active_id, 'cc_add_message', obj);
            });
        },


        /*
        * Updates the Information about client.
        */
        updateInfo = function (id, col, value) {

            // Checks if  instance of Buyer/Advisor/Seller/Contractor is selected or not
            if (0 === client_id) {
                toastr.warning('Please add a new client using \'Add Client\' Button.', 'Dummy Component');
            } else if (-1 !== id) {

                // Updates property info.
                const update_prop_info = makeAjaxCall('cpdv1/update_prop_info', {
                    id: id,
                    col: col,
                    value: value
                });

                update_prop_info.then(function () {
                    const get_prop_info = makeAjaxCall("cpdv1/get_prop_info", {
                        client_id: client_id,
                        type: info_type,
                        is_buyer: buyer_type
                    });
                    get_prop_info.then(function (data) {
                        updated_data = data;
                    });
                })
            } else {
                toastr.warning('Please select an instance of Seller/Buyer/Advisor/Contractor.');
            }
        };

    return {
        init: init
    };
}();



