/**
 *
 * Client Project Dashboard Instance Listing.
 *
 * @summary      This file contains functions related to listing all the DMD instances.
 * @description  Contains functions for drawing DMD instances list and Event Handling.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */

$(document).ready(function () {
    // refreshPage();
});

// Get the page data and display it.
function refreshPage() {
    updateInstances();
}

// Update the DMD instances

/**
 * Updates DMD Listing Table.
 */
function updateInstances() {
    let i = 1,
        instance_datatable = $('#cpd-instance-table-' + i).DataTable();
    $('#table-header-' + i).text($('#current-product option').eq(i).text());

    // Destroy the datatable so that it can be rebuilt.
    instance_datatable.destroy();

    // Create the instance table using server-side data.
    instance_datatable = $('#cpd-instance-table-' + i).DataTable({
        "pageLength": 50,
        "serverSide": true,
        "processing": true,
        "ajax": {
            "url": jsglobals.base_url + "cpdv1/search_cpd_instances",
            "type": "post",
            "data": {}
        },
        "columns": [
            {"data": "cpd_id", "name": "DMD ID"},
            {"data": "cpd_name", "name": "Name"},
            {"data": "cpd_description", "name": "Description"},
            {"data": "cpd_created", "name": "Created"},
            {"data": "cpd_updated", "name": "Modified"},
            {"data": "cpd_id", "name": "Active"},
            {"data": null, "name": "Delete", "className": "text-center"},
        ],
        "rowCallback": function (row, data, index) {
            $('td', row).eq(0).html(index + 1);
            $('td', row).eq(1).html('<a data-id="' + data.cpd_id + '" data-active="' + data.uact_cpd_id + '"' +
                ' class="go-to-cpd">' + data.cpd_name + "</a>");
            $('td', row).eq(5).html('<input type="radio" class="active_instance_id" data-name="cpd_id"' +
                ' data-product="' + data.cpd_product_id + '" data-id="' + data.cpd_id + '"' +
                ' name="active_cpd' + data.cpd_product_id + '" value="' + data.cpd_id + '"' + '>');

            if (data.uact_cpd_id === data.cpd_id) {
                $('td', row).eq(5).find("input:radio").prop('checked', true);
            }

            $('td', row).eq(6).html('<button class="btn btn-default btn-sm"' +
                ' data-toggle="modal"' +
                ' data-target="#delete-dialog" data-backdrop="static"' +
                ' data-id="' + data.cpd_id +
                '" data-name="' + data.cpd_name +
                '" data-controller=cpd' +
                '> <span class="glyphicon glyphicon-trash"></span></button>');
        },
        "initComplete": function () {
            if (instance_datatable.data().count() === 0) {
                $('#no-cpd-instances').show();
            } else
                $('#no-cpd-instances').hide();
            updateActiveInstance();

            $(document).on('click', '.go-to-cpd', function () {

                const instance_id = $(this).attr('data-id');

                if ('null' === $(this).attr('data-active')) {
                    toastr.error("Click the radio button under the Active heading to indicate your selection." +
                        " You may only have one active DMD at a time.",
                        "You will not be able to proceed unless you select an Active DMD.");

                } else {
                    window.location.href = jsglobals.base_url + 'cpdv1/cpdv1/' + instance_id;
                }
            })
        }
    });
}
