/**
 *
 * Strategic Advisory Page.
 *
 * @summary      Strategic Advisory Documents: PDF documents that user can create.
 * @description  This file contains functions for uploading, listing, displaying, compiling, updating and deleting SA docs.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */

let activeOutlineId, activeSAId, outline_added, sa_added, sa_compiled, client_id, obj, ref, template_mapping_array = {};
$(document).ready(function () {

    // Get the Client ID from the URL
    const path = window.location.pathname,
        components = path.split('/');
    client_id = components[components.length - 1];
    const origin = components[2];

    loadPrev();

    ref = window.location.search.split('?ref=')[1];

    // Check if ref is defined or not.
    if (undefined !== ref) {
        const ref_array = ref.split('?');
        ref = ref_array[0];
    }

    $('#send-sa').attr('data-origin', origin);
    activeSAId = -1;

    // Setup the content for Template mapping array.
    template_mapping_array['lp'] = 'lp';
    template_mapping_array['wp'] = 'wp';
    template_mapping_array['pp'] = 'pp';
    template_mapping_array['ap'] = 'ap';
    template_mapping_array['rp'] = 'rp';
    template_mapping_array['amp'] = 'amp';

    // Display outline for the sa.
    $('#outline-container').html('');
    $('#canvas-container').html('');

    // To check if a new sa is added.
    outline_added = 0;
    activeOutlineId = 0;

    // List of Buyer's Survey
    const container_listing_selector = $('#container-listing');
    container_listing_selector.html('');
    container_listing_selector.html(
        ' <h4 class="main-con-header">SA Version</h4>' +
        ' <ul class="nav" id="tsl-sa-nav">\n' +
        ' </ul>');

    refreshSA();
    updateSAList();

    // Compile SA to PDF.

    $(document).on('click', '#compile-sa', function () {
        console.log('here');

        if ('0' !== activeSAId) {
            // Compile a new SA document.
            const compile_sa = makeAjaxCall('cpdv1/compile_sa', {
                sa_id: activeSAId,
                sa_type: $('#sa-type option:selected').attr('data-name')
            });
            compile_sa.then(function (data) {
                toastr.success('Document created!!', 'Success');
                $('#edit-page-dialog').modal('hide');
                sa_compiled = 1;
                updateSAList();
            });
        } else {
            toastr.error('Please Select One or Create One using Add New button.', 'No SA instance selected!!');
        }
    });

    // Refresh SA on dropdown change.
    const sa_menu_selector = $('.sa-menu');
    sa_menu_selector.off('click');
    sa_menu_selector.on('change', function (data) {
        refreshSA();
    });

    // Upload background/Image to be embedded with SA doc.
    $('.upload-sa, .upload-sa-2').click(function () {

        const file_type_allowed = /.\.(jpg|jpeg|png)/i;
        $('#fileupload').trigger('click');

        const target = 'fileupload',
            upload_sa = uploadFile(target, {
                'id': activeSAId,
                'page': $('#template-version').val(),
                'number': $(this).attr('data-number')
            }, file_type_allowed);

        upload_sa.then(function (data) {
            $('.upload-sa').attr('data-name', data._response.result.name);
        });
        return false;
    });

    // Generates new page for SA doc.
    $('#compose-new-sa').click(function () {

        // Compose a new SA document.
        const compose_sa = makeAjaxCall('cpdv1/compose_sa', {
            client_id: client_id,
            sa_type: $('#sa-type').val()
        });
        compose_sa.then(function (data) {
            toastr.success('New Instance of Document Created!!', 'Success');
            sa_added = 1;
            refreshSA();
            updateSAList();
        });
    });

    $(document).on('click', '#save-promo-content', function () {
        let compose_promo_form_selector = $('#compose-promo-form'),
            textAreaCount = 0,
            dObj;

        // Sets the value from the text field  into span.
        compose_promo_form_selector.find('.span_ans').each(function () {
            dObj = $(this);
            compose_promo_form_selector.find('textarea').eq(textAreaCount).attr('name', 'ans' + textAreaCount);
            dObj.text(compose_promo_form_selector.find('textarea').eq(textAreaCount).val());

            textAreaCount++;
        });

        let data = '';

        data = $('#compose-promo-form').html().replace(/<span class="hidden" id="span-selector"><\/span><\/div>/g, '')
            .replace(/<span class="input-group-addon[\s\S]*?<\/span>/g, '')
            .replace(/<div class="input-group" style="margin-left: 30px;">/g, '')
            .replace(/<strong><h1>Client Name:<\/h1><br><\/strong>/g, '')
            .replace(/<br><strong><\/strong><br>/, '')
            .replace(/<strong><br><\/strong><br>/, '')
            .replace(/<br><strong><br><\/strong>/, '')
            .replace(/<br><strong><\/strong>/, '')
            .replace(/<br><br>/g, '<br>');

        // Generate the PDF text from inputs.
        // Check if the template version is 5 para left, Long Pic Right.
        if ($('#template-version').val() === 'pp') {
            data = data.replace(/<blockquote>/g,
                "<blockquote style='margin-top: 1em; margin-bottom: 1em; margin-left: 40px; margin-right: 40px;'>")
                .replace(/<h1>/, '<h1 style="font-size: 23pt; margin-top: 10px">')
                .replace(/<br><span>/g, " <span>")
                .replace(/<\/label>/g, '</strong><\/label><br>')
                .replace(/<label[\s\S]*?<\/label>/g, '<br>')
                .replace(/<textarea[\s\S]*?<\/textarea>/g, '')
                .replace(/<span class="hidden span_ans">/g, "<span>")
                .replace(/<br>/g, '');
        }
        // Check if the template version is 5 para top, Center Pic and 5 para bottom.
        else if ($('#template-version').val() === 'ap') {
            data = data.replace(/<blockquote>/g,
                "<blockquote style='margin-top: 1em; margin-bottom: 1em; margin-left: 40px; margin-right: 40px;'>")
                .replace(/<br><span>/g, " <span>")
                .replace(/<\/label>/g, '</strong><\/label>')
                .replace(/<textarea[\s\S]*?<\/textarea>/g, '').replace(/<span class="hidden span_ans">/g, "<span>")
                .replace(/<label>The challenge is that . . .<\/strong><\/label>/g, '')
                .replace(/<label>The consequence of this option . . .<\/strong><\/label>/g, '')
                .replace(/<label>Title<\/strong><\/label>/g, '')
                .replace
                (/<div><strong><h1><br><label><strong>Title<\/strong><\/label><span><\/span><\/h1><\/strong><\/div>/g, '')
                .replace(/<br><br>/g, '<br>')
                .replace(/<label>/g, '<br><label style="font-size:13pt; font-weight: bold;"><strong>')
                .replace(/<\/label>/g, '')
                .replace(/<\/span><\/strong>/g, '</span></label></strong>');
        }
        // Check if the template version is set to Full Page Pic and One Para.
        else if ($('#template-version').val() === 'lp') {
            data = data.replace(/<blockquote>/g,
                "<blockquote style='margin-top: 1em; margin-bottom: 1em; margin-left: 40px; margin-right: 40px;'>")
                .replace(/<label> Title <\/label>/, '')
                .replace(/<label> Document Description <\/label>/, '')
                .replace(/<label>/g, '<label style="font-size: 14pt; font-weight: lighter">')
                .replace(/<textarea[\s\S]*?<\/textarea>/g, '')
                .replace(/<span class="hidden span_ans">/g, "<span class='strong'>")
                .replace(/<\/strong> <br>/g, "</label><\/strong><br>")
                .replace(/<\/strong><br>/g, "</label><\/strong><br>")
                .replace(/<\/strong><\/blockquote>/g, "</label></strong></blockquote>")
                // .replace(/<\/label><\/label>/g, '</label>')
                .replace(/<h1>/g, '<h1 style="font-size: 26pt; margin-bottom: 0">')
                .replace(/<div>/, '')
                .replace(/<\/div>/, '');
        }
        else {
            data = data.replace(/<blockquote>/g,
                "<blockquote style='margin-top: 1em; margin-bottom: 1em; margin-left: 40px; margin-right: 40px;'>")
                .replace(/<label>Agent Name<\/label>/, '<label>Agent Name</label><<break>>')
                .replace
                (/<label>Option 5: <\/label>/, '<span style="font-size:13pt; font-family: gothicb">Option 5: </span>')
                // .replace(/<br><br>/, '<br>')
                .replace(/<label[\s\S]*?<\/label>/g, '<br>').replace(/<textarea[\s\S]*?<\/textarea>/g, '')
                .replace(/<span id="span-ans" class="hidden span_ans">/g, "<span>")
                // .replace(/<br><span>/g, "<span>")
                .replace(/<\/span><span>/g, '');

            data = (($('#template-version').val() === 'wp') ? data
                    .replace(/<h1>[\s\S]*?<\/h1>/g, '')
                    .replace(/<br><strong><\/strong><br>/g, '<br>')
                    // .replace(/<br><strong><\/strong><\/div><div><br>/g, '<br><br>')
                    // .replace(/<br><strong><br><\/strong>/g, '')
                    .replace(/<\/div><div>/g, '')
                : data.replace(/<br><br>/g, ''));


        }

        if ($('#template-version').val() === 'amp') {
            data = data.replace(/Analysis/g, 'Analysis<br>');
        }

        const save_promo_content = makeAjaxCall('cpdv1/save_sa_data',
            {
                data: data,
                compose: $('#compose-page').attr('data-compose'),
                bg_image: $('.upload-sa').attr('data-name'),
                sa: $('#sa-type').val(),
                page: $('#template-version').val(),
                template: $('#template-version').val(),
                template_id: $('#background-template').val(),
                sa_id: activeSAId
            });

        save_promo_content.then(function (data) {
            toastr.success('Content Saved', 'Success!!');
            $('#compose-promo-dialog').modal('hide');
        })
    });
})
;

// Check if any drop down menu is changed, fetch outline version again.
$('#sa-type').change(function () {
    $('#canvas-container').html('');
    activeOutlineId = 0;
    activeSAId = 0;
    updateSAList();
});

// Check if page template is changed.
$('#template-version').change(function () {
    $('#template-version').val(template_mapping_array[$(this).val()]);
    refreshSA();
});

// Checks if no instance of sa is present.
$('#send-sa, #delete-sa').click(function () {
    if (undefined === $(this).attr('data-toggle')) {
        toastr.error('To create one use compose button.', 'No Instance of sa.');
    }
});

// Uploads Team and company Logo.
$('.upload-logo').click(function () {
    const type = $(this).attr('data-name'),
        data = {
            sa_id: activeSAId,
            type: type
        },
        file_type_allowed = /.\.(jpg|jpeg|png)/i;
    $('#logo_upload').trigger('click');

    const target = 'logo_upload',

        // Upload Logo for PDF.
        upload_logo = uploadFile(target, data, file_type_allowed);
    upload_logo.then(function (data) {
        $('.upload-sa').attr('data-name', data._response.result.name);
    });
    return false;
});

/**
 * Fetches questions and puts them in a form.
 *
 */
function setupSA(obj, default_data) {

    // Remove Extra Padding from outline container.
    const outline_container_first_selector = $('#outline-container h1:first');
    outline_container_first_selector.html($.trim(outline_container_first_selector.text()))
    activeOutlineId = $(obj).attr('data-id');

    // Jquery file upload.
    $('#fileupload').fileupload({
        dataType: 'json',
        formData: {'outline_id': activeOutlineId, 'sa_id': activeSAId},
        done: function (e, data) {
            if ("success" !== data.result.status) {
                toastr.error(data.result.message);
                return;
            }
            $.each(data.result.files, function (index, file) {
                $('<p/>').text(file.name).appendTo(document.body);
            });
            toastr.success('Document uploaded successfully...');
            sa_added = 1;
            refreshSA();
            updateSAList();
        }
    });

    // Set data attributes for edit sa outline button.
    const edit_sa_selector = $('#edit-sa-outline');
    edit_sa_selector.attr('data-toggle', 'modal');
    edit_sa_selector.attr('data-target', '#edit-sa-outline-dialog');
    edit_sa_selector.attr('data-backdrop', 'static');

    //Setup edit sa outline form.
    $('#outline-container').html('<h4 class="main-con-header">SA Outline</h4><form id="outline-form">' +
        '<input type="hidden" id="sa_focus" name="sa_focus">' +
        '<input type="hidden" id="sa_format" name="sa_format">' +
        '<input type="hidden" id="client_type" name="client_type">' +
        '<input id="y" value="' + $(obj).attr('data-content') + '" type="hidden" name="outline_content">\n' +
        '<trix-editor id="trix-editor" class="editor" input="y" name="tsl_sa_editor"></trix-editor>' +
        '</form>');

    trixButtons();

    const compose_promo_form_selector = $('#compose-promo-form'),
        compose_sa_selector = $('#compose-sa');
    compose_promo_form_selector.empty();
    compose_promo_form_selector.append($(obj)
        .attr('data-content').replace(/\[\[/g, '<h1>').replace(/\]\]/g, '</h1>')
        .replace(/{{/g, '<label>').replace(/}}/g, '</label>' +
            '<div class="input-group" style="margin-left: 30px;">' +
            '<textarea class="form-control" rows="1" cols="70" name="ans" id="ans' + Date.now() +
            '" placeholder="Click Microphone icon and speak or start typing." onkeyup="auto_grow(this)"></textarea>' +
            '<span class="input-group-addon btn btn-secondary stt">' +
            '<i class="fa fa-microphone"></i></span>' +
            '<span id="span-ans" class="hidden span_ans"></span><span class="hidden" id="span-selector"></span></div>'));

    // Set the default content for textarea.
    $.each(default_data, function (index, data) {
        $('#compose-promo-form textarea').eq(index).val(data.csdd_content);
    });

    // Set the default height for Compose Promo Form Textarea.
    $('#compose-promo-dialog').on('shown.bs.modal', function (event) {
        $('#compose-promo-form textarea').each(function () {
            $(this).css('height', $(this)[0].scrollHeight + 'px');
        });
    });

    // Enable Voice Recognition.
    voiceRecognition();
    speechFunctionality();

    // Set data attribute for compose sa.
    compose_sa_selector.attr('data-toggle', 'modal');
    compose_sa_selector.attr('data-id', activeOutlineId);
    compose_sa_selector.attr('data-backdrop', 'static');

    // Click Handler for Compose SA.
    compose_sa_selector.unbind('click');
    compose_sa_selector.click(function () {
        if ('0' !== activeSAId) {
            $('#compose-promo-dialog').modal('show');
        } else {
            toastr.error('Please Select One or Create One using Add New button.', 'No SA instance selected!!');
        }
    });

    const view_sa_outline_selector = $('#view-sa-outline');
    view_sa_outline_selector.attr('data-toggle', 'modal');
    view_sa_outline_selector.attr('data-id', activeOutlineId);
    view_sa_outline_selector.attr('data-target', '#view-sa-outline-dialog');
    view_sa_outline_selector.attr('data-backdrop', 'static');
}

/**
 * Fetches SA outline.
 */

function refreshSA() {

    // Check if thye type of template is Recommendation page.
    ('rp' === $('#template-version').val()) ?
        $('.upload-sa-2').css('display', '') :
        $('.upload-sa-2').css('display', 'none');

    // Get SA Outlines.
    const get_outline = makeAjaxCall('cpdv1/get_sa_outline', {
        page_template: $('#template-version').val(),
        template_version: $('#template-version').val(),
    });

    // Update Outline and Compose popup content.
    get_outline.then(function (data) {
        updateOutline(data.sa_outline.outline, data.sa_outline.default_data);
    });
}


/**
 * Display selected outline.
 */
function updateOutline(outlines, default_data) {

    const outline_container_selector = $('#outline-container');
    outline_container_selector.empty();
    outline_container_selector.html('<h4 class="main-con-header">SA Outline</h4><br>' + outlines[0]);

    // Create an Object with id and content received.
    obj = {
        "data-id": 0,
        "data-content": outlines[0].csd_content,
    };

    setupSA(obj, default_data);
}

// function update()

/**
 * Display list of Buyer's sa instances.
 */
function updateSAList() {
    let sa_list;

    // Fetches and displays list of Strategic Analyser.
    const get_sa_list = makeAjaxCall('cpdv1/get_sa_list', {
        sa_type: $('#sa-type').val(),
    });
    get_sa_list.then(function (data) {
        sa_list = data.list;
        $('#tsl-sa-nav').empty();

        $('#tsl-sa-nav')
            .append($('<li class="text-center" id="0"></li>')
                .append('<a href="#" onclick="setupPDF(this); return false;" style="color: black; font-weight: bold" ' +
                    'data-name="Example" data-id="0" data-random="' + sa_list[0].cs_random_name + '">' +
                    'Example</a></li>'))

        new_sa_list = sa_list.slice(1);
        // Append items to list.
        $.each(new_sa_list, function (index, sa) {
            $('#tsl-sa-nav')
                .append($('<li class="text-center" id="' + sa.cs_id + '">')
                    .append('<a href="#" class="edit-ref" onclick="setupPDF(this); return false;" ' +
                        'data-name="' + sa.cs_name + '"data-id="' + sa.cs_id + '"' +
                        'data-random="' + sa.cs_random_name + '">' +
                        '<span class="editable" data-type="text" data-pk = "' + sa.cs_id + '" ' +
                        'data-url = "' + jsglobals.base_url + 'cpdv1/update_sa_name">' +
                        sa.cs_name + '</span></a></li>'))
        });

        $('#send-sa').removeAttr('data-toggle');
        $('#delete-sa').removeAttr('data-toggle');

        // If no ID is selected and reference is null.
        if ((undefined !== ref) && (-1 === activeSAId)) {
            // Fetch Strategic Analyzer Doc Details.
            const get_sa_details = makeAjaxCall('cpdv1/get_sa_details', {
                sa_id: ref
            });

            get_sa_details.then(function (data) {
                const sa_data = data.sa_details[0],
                    sa_type_selector = $('#sa-type');
                sa_type_selector.val(sa_data.cs_type);
                sa_type_selector.trigger('change');
                $("a[data-id=" + ref + "]").trigger('click');
            });

        } else if (0 === (sa_list).length) {
            activeSAId = 0;
        } else if ((0 === activeSAId) || (-1 === activeSAId)) {
            obj = {
                "data-id": sa_list[0].cs_id,
                "data-name": sa_list[0].cs_name,
                "data-random": sa_list[0].cs_random_name
            };

            $('#delete-sa').attr('disabled', true);
            setupPDF(obj);
        } else if (1 === sa_added) {
            obj = {
                "data-id": sa_list[sa_list.length - 1].cs_id,
                "data-name": sa_list[sa_list.length - 1].cs_name,
                "data-random": sa_list[sa_list.length - 1].cs_random_name
            }
            setupPDF(obj);
        } else if (1 === sa_compiled) {
            const index = sa_list.findIndex(x => x.cs_id === activeSAId);
            obj = {
                "data-id": sa_list[index].cs_id,
                "data-name": sa_list[index].cs_name,
                "data-random": sa_list[index].cs_random_name
            }

            setupPDF(obj);
        } else {
            $("a[data-id=" + activeSAId + "]").trigger('click');
        }
        setEditable('bottom');
    });
}

/**
 * Fetch and display already answered questions for a sa.
 */
function setupPDF(obj) {

    activeSAId = $(obj).attr('data-id');

    // Displaying the active email name.
    $('.active').removeClass('active');
    $('#' + activeSAId).addClass('active');

    const send_sa_selector = $('#send-sa'),
        delete_sa_selector = $('#delete-sa');

    // Check if the active SA ID is set to zero.
    (0 == activeSAId) ?
        $('#delete-sa').attr('disabled', true) : $('#delete-sa').attr('disabled', false);

    // Set attribute for send button.
    send_sa_selector.attr('data-id', activeSAId);
    send_sa_selector.attr('data-toggle', 'modal');
    send_sa_selector.attr('data-type', 'sa');
    send_sa_selector.attr('data-target', '#send-sa-dialog');
    send_sa_selector.attr('data-backdrop', 'static');
    send_sa_selector.attr('data-name', $(obj).attr('data-name'));
    send_sa_selector.attr('data-random', $(obj).attr('data-random'));

    // Set attribute for delete button.
    delete_sa_selector.attr('data-id', activeSAId);
    delete_sa_selector.attr('data-toggle', 'modal');
    delete_sa_selector.attr('data-random', $(obj).attr('data-random'));
    delete_sa_selector.attr('data-type', 'sa');
    delete_sa_selector.attr('data-target', '#delete-promo-dialog');
    delete_sa_selector.attr('data-backdrop', 'static');
    delete_sa_selector.attr('data-name', $(obj).attr('data-name'));

    // Display PDF.
    fetch_pdf($(obj));

    // Makes the names editable.
    $(".edit-ref").children().click(function (event) {
        event.stopPropagation();
        $("form").children().click(function (event) {
            event.stopPropagation();
            refreshSA();
            updateSAList();
        });
    });

    const expand_bus_selector = $('#expand-sa');
    expand_bus_selector.unbind('click');
    expand_bus_selector.click(function (e) {
        const sa_id = $(obj).attr('data-id'),
            url = window.location.origin + '/tools/cpdv1/view_sa/?id=' + sa_id;
        window.open(url);
    });

    // Check if the component is a dummy.
    check_dummy_component(client_id);
}

// Fetches and Displays PDF in middle panel.
function fetch_pdf(obj) {
    const canvas_container_selector = $('#canvas-container');
    canvas_container_selector.html('');

    // Displays the SA document.
    canvas_container_selector.html('');
    canvas_container_selector.html('<h4 class="main-con-header">SA Composition</h4>' +
        '<canvas id="pdf-canvas"></canvas>');

    $('#pdf-canvas').after('<div align="center" class="attach_next_prev" ' +
        'style="position: absolute; top: 0.35em; bottom: 0; left: 0; right: 0;">' +
        '<button id="prev" class="btn btn-secondary prev" style="float: left; width:2%; margin-left: 10px">' +
        '<img src="https://cpd.powersellingtools.com/wp-content/uploads/2019/08/arrow_left.png"' +
        ' style="height: 100%; width: 100%;" alt="back"></button>\n' +
        '<button id="next" class="btn btn-secondary next" style="float: right; width: 2%;  margin-right: 10px">' +
        '<img src="https://cpd.powersellingtools.com/wp-content/uploads/2019/08/arrow_right.png"' +
        ' style="height: 100%; width: 100%; " alt="back">' +
        '</button>&nbsp; &nbsp;\n' +
        '</div>');

    const sa_id = $(obj).attr('data-id'),
        url = window.location.origin + '/tools/cpdv1/view_sa/?id=' + sa_id,
        canvas = document.getElementById('pdf-canvas');

    preview_pdf(url, canvas);
}