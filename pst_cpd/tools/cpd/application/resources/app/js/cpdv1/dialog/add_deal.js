/**
 *
 * Adds new Deal instance.
 *
 * @summary       Adds new Deal instance to an existing DMD instance.
 * @description  This file contains functions for adding new Deal to an existing Client Project Dashboard Instance.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */
$(document).ready(function () {

    // Get DMD ID from URL
    const path = window.location.pathname,
        components = path.split('/'),
        cpd_id = components[components.length - 1];

    $('#add-deal-cpd-id').val(cpd_id);

    // On changing deal status set deal status type (Generation/Complete/Strategic/Buyer/).
    $(document).on('change', 'select#deal-status', function () {
        const status_type = $('option:selected', this).data('status-type');
        $('#add-deal-status-type').val(status_type);
    });

    // Set the Phone number format.
    $('#client-phone').keyup(function () {
        $(this).val($(this).val().replace(/(\d{3})(\d{3})(\d{4})/, '$1-$2-$3'));
    });

    // Validate Add Deal form and If valid submit data.
    $('#add-deal-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            deal_name: {
                validators: {
                    notEmpty: {
                        message: "The client name is required."
                    }
                }
            },
            property_name: {
                validators: {
                    notEmpty: {
                        message: "The client location is required."
                    }
                }
            },
            deal_status: {
                validators: {
                    notZero: {
                        message: "The deal initial status is required."
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the form cpd
        const $form = $(e.target),
            fv = $form.data('formValidation');

        // Get the form data and submit it.
        $.ajax({
            url: "https://maps.googleapis.com/maps/api/geocode/json?&address=" +
            $('#property-name').val() + "&key=AIzaSyBCwpF5xPvzTUNhXuKc-SKw655Pdty5qWs",
            dataType: "json",
            type: 'post',
        }).done(function (data) {

            if ('failure' === data.status) {
                toastr.error(data.message);
                return;
            }
            else if ( 'redirect' === data.status) {
                window.location.href = data.redirect;
                return;
            }

            // Get Longitude and Latitude coordinates.
            $('#add-deal-lat').val(data.results[0].geometry.location['lat']);
            $('#add-deal-long').val(data.results[0].geometry.location['lng']);
            const deal_data = $form.serialize(),

                // Adds a new deal.
                add_deal = makeAjaxCall('cpdv1/add_deal', deal_data);
            add_deal.then(function (data) {
                $($form)[0].reset();
                fv.resetForm();
                $('#add-deal-dialog').modal('hide');
                refreshPage();
            });
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });

    });
});

