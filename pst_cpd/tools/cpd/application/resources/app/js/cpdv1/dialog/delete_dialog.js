/**
 *
 * Delete Dialog.
 *
 * @summary      Delete Dialog to delete DMD components.
 * @description  This file contains functions for Deleting different DMD components.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */

let ui_name, controller;

$(document).ready(function () {
    // When the dialog is displayed, set the current instance ID.
    $('#delete-dialog').on('show.bs.modal', function (event) {

        // Fetch Deal ID.
        const ui_id = $(event.relatedTarget).data('id');
        ui_name = $(event.relatedTarget).data('name');
        ui_title = $(event.relatedTarget).data('title');
        controller = $(event.relatedTarget).data('controller');

        // Set the title for Delete Dialog.
        $('#delete-title').html('Delete ' + ui_title + ' instance?');
        $('#id').val(ui_id);
        $('#name').html(ui_name);
        $('#dialog-instance-name').html(ui_name);
    });

    // Validate the delete instance form and submit it if it is valid.
    $('#delete-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {}
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the form instance
        const $form = $(e.target),
            fv = $form.data('formValidation'),

            // Get the form data and submit it.
            message_data = $form.serialize(),

            // Delete ACM instances.
            delete_dialog = makeAjaxCall('cpdv1/delete_' + controller, message_data);
        delete_dialog.then(function (data) {
            $('#delete-dialog').modal('hide');
            fv.resetForm();
            active_id = -1;
            refreshPage();
        });
    });
});

