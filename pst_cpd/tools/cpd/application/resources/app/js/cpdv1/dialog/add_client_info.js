/**
 *
 * Adds Client's Information and Client's Reference.
 *
 * @summary      Adds Client's information and client's reference. .
 * @description  This file contains functions for adding new client's information.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */
$(document).ready(() => {
    // Adds form validation.
    addClientInfo.validateForm();

    // On Pop up shown event.
    $('#add-client-info-dialog').on('show.bs.modal', function (event) {
        addClientInfo.init();
    });
});

/*
*Adds a new client information/Reference
*
*/
let addClientInfo = (() => {

    /*
    * Initialize pop up.
    *
    */
    let init = () => {
            type = (info_type !== '' ? info_type : 'info');

            $('.additional-ref-col').remove();

            // Add additional reference and event columns for reference pop up.
            if ('ref' === type) {
                $('<div class="additional-ref-col"><div class="form-group" id="">' +
                    '<label class="col-xs-12 col-sm-3 control-label" for="client-ref">Reference</label>' +
                    '<div class="col-xs-12 col-sm-9">' +
                    '<input type="text" class="form-control" id="client-ref" name="client_ref">' +
                    '<p class="form-text text-muted">Enter Reference Name.</p>' +
                    '</div></div>' +
                    '<div class="form-group" id="">' +
                    '<label class="col-xs-12 col-sm-3 control-label" for="client-event">Event</label>' +
                    '<div class="col-xs-12 col-sm-9">' +
                    '<input type="text" class="form-control" id="client-event" name="client_event">' +
                    '<p class="form-text text-muted">Enter Event Name.</p>' +
                    '</div></div></div>')
                    .insertBefore('#add_company')
            }
        },

        /*
        * Validates all the form fields and submit form if correct.
        *
        */
        validateForm = () => {

            // Form validation.
            $('#add-client-info-form').formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh',
                },
                fields: {
                    client_title: {
                        validators: {
                            notEmpty: {
                                message: "Title is required."
                            }
                        }
                    },
                    name: {
                        validators: {
                            notEmpty: {
                                message: "First Name is required."
                            }
                        }
                    }
                }
            }).on('success.form.fv', function (e) {
                e.preventDefault();

                // Get the form cpd
                const $form = $(e.target),
                    fv = $form.data('formValidation');

                // $('#client-id').val(client_id);
                // $('#info-type').val(type);
                $('#is-buyer').val(buyer_type);
                // $('#is-owner').val(owner_type);

                const deal_data = $form.serialize(),
                    //Adds client information.
                    addClientInfo = makeAjaxCall('cpdv1/add_client_info', deal_data);

                addClientInfo.then(function (data) {
                    $($form)[0].reset();
                    fv.resetForm();
                    $('#add-client-info-dialog').modal('hide');
                    refreshPage();
                    toastr.success('Success!!');
                });
            });
        };

    return {
        init: init,
        validateForm: validateForm
    };
})();
