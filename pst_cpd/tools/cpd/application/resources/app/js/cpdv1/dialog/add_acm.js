/**
 *
 * Add Accountability Manager Instance.
 *
 * @summary      Adds a new Accountability Manager Instance.
 * @description  This file contains functions for adding new Accountability Manager Instance.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */
let deal_id;
$(document).ready(function () {

    // Fetch Parameters from URL.
    const path = window.location.pathname,
        components = path.split('/');
    deal_id = components[components.length - 1];

    $('#deal-id').val(deal_id);

    setDateTimePicker();

    // Validate Add ACM form and Submit if valid.
    $('#add-acm-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            acm_ceo_id: {
                validators: {
                    notZero: {
                        message: "CEO is required."
                    }
                }
            },
            acm_action: {
                validators: {
                    notZero: {
                        message: "Next Action is required."
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the form cpd
        const $form = $(e.target),
            fv = $form.data('formValidation'),
            acm_when_selector = $('#acm_when');

        // Check if Date field is set.
        if ("" !== acm_when_selector.val()) {

            // Change the format for date.
            const val = acm_when_selector.val().split(' '),
                date = val[0].split(/\//g),
                time = val[1].split(/:/g),
                value = '0000-' + date[1] + '-' + date[0] + ' ' + time[0] + ':' + time[1];

            acm_when_selector.val(value);
        }

        // Get the form data and submit it.
        const cpd_data = $form.serializeArray(),
            active_tab_selector = $('.req-prom-nav').find('li.active');

        // Append column name and value to form data.
        cpd_data.push({name: 'acm_type', value: active_tab_selector.find('a').data('type')});
        cpd_data.push({
            name: 'req_prom',
            value: active_tab_selector.data('name')
        });

        // Adds a new Accountability manager instance.
        const add_acm = makeAjaxCall('cpdv1/add_acm', cpd_data);
        add_acm.then(function (data) {
            $($form)[0].reset();
            fv.resetForm();
            $('#add-acm-dialog').modal('hide');
            refreshPage();
        });
    });
});

