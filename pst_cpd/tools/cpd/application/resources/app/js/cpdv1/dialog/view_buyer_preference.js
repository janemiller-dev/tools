/**
 *
 * View Comparables.
 *
 * @summary      Dialog used to display Comparables for an assets.
 * @description  This file contains functions for Displaying Comparables.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */

$(document).ready(function () {

    $('#view-buyer-preference-dialog').on('show.bs.modal', function (event) {
        viewPrefer.init(event);
    });
});

/**
 *
 * View Notes available.
 */

let viewPrefer = function () {
    // Initialize and get the DMD ID.
    let type, buyer_id;
    init = function (event) {
        type = $(event.relatedTarget).attr('data-type');
        buyer_id = $(event.relatedTarget).attr('data-id');

        // Set the heading and content for popup.
        $('#prefer-title').text($(event.relatedTarget).attr('data-heading'));
        $('.prefer-content').addClass('hidden');
        $('#' + type + '-div').removeClass('hidden');

        const get_prefer_content = makeAjaxCall('cpdv1/get_prefer_content', {
            'id': buyer_id
        });

        get_prefer_content.then(function (data) {
            $.each(data.prefer_content[0], function (index, content) {
                $('.prefer-content textarea[data-name="' + index + '"]').val(content);
            })
        });

        // Preference textarea change handler.
        $(document).off('change', '.prefer-content textarea');
        $(document).on('change', '.prefer-content textarea', function () {
            const update_text = makeAjaxCall('cpdv1/update_prefer_text', {
                'id': buyer_id,
                'val': $(this).val(),
                'col': $(this).attr('data-name')
            });

            update_text.then(function (data) {
                console.log(data);
            })
        })
    };

    return {
        init: init,
    };
}();