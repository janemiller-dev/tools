/**
 *
 * View DMD notes.
 *
 * @summary      Dialog used to display SA for DMD clients.
 * @description  This file contains functions for Displaying DMD client Strategic Analysis..
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */

$(document).ready(function () {
    let asset_id = 0;

    $('#view-sa-dialog').on('show.bs.modal', function (event) {
        view_sa.init(event);
        asset_id = $(event.relatedTarget).attr('data-id');
    });

    $(document).on('click', '#make-sa-pdf', function () {

        let content = $('#sa-div').html(),
            ans_array = [],
            textarea_count = 0;


        $.each($('.sa-field'), function (index, field) {
            if (!$(this).parent().hasClass('hidden')) {
                ans_array.push($(field).val())
                textarea_count++;
            }
        });

        content = content.replace(/<button[\s\S]*?<\/button>/g, '')
            .replace(/<span[\s\S]*?>/g, '')
            .replace(/<\/span>/g, '')
            .replace(/<i[\s\S]*?<\/i>/g, '')
            .replace(/<div class="hidden"[\s\S]*?<\/div>/g, '')
            // .replace(/<h4>/g, '<h4 style="padding-left: -30px">')
            .replace(/<\/textarea>/g, '</textarea><span class="ans"></span>')
            .replace(/<textarea[\s\S]*?<\/textarea>/g, '')


        for (var i = 0; i < textarea_count; i++)
        {
            content = content.replace(/<span class="ans">/g, '<span>' + ans_array[i]);
        }

        const generate_pdf = makeAjaxCall('cpdv1/generate_sa_pdf', {
            content: content,
            asset_id: asset_id
        });

        generate_pdf.then(function () {
            toastr.success('PDF generated.', 'Success!!')
        })
    })
});

/**
 *
 * View Notes available.
 */
let view_sa = function () {
    let asset_id,
        // Initialize and get the DMD ID.
        init = function (event) {
            asset_id = $(event.relatedTarget).data('id');
            const fetch_sa = makeAjaxCall('cpdv1/fetch_sa', {
                'id': asset_id
            });

            fetch_sa.then(function (data) {
                const sa_info = data.sa;

                // Reset form fields.
                $('#sa-row').find('textarea').val('');
                $('.additional-sa-row').remove();
                $('#sa-row .editable').removeClass('editable').text('Name');

                // Append the values for fields.
                (sa_info.length > 0) ?
                    $.each(sa_info[0], function (index, info) {
                        $('textarea[data-type="' + index.replace("has_", "") + '"]').val(info);
                    }) : '';

                // $('#project-description').text(sa_info[0].has_desc);
                // $('#partnership-declaration').text(sa_info[0].has_declaration);
                $.each(sa_info, function (index, info) {

                    // Check if this is an additional row and if so append it.
                    if ($('textarea[data-seq="' +
                            info.hasi_seq + '"][data-type="' + info.hasi_type + '"]').length === 0) {

                        let type = info.hasi_type,
                            seq = info.hasi_seq,
                            dummy_row = $('#dummy_' + type).clone();

                        dummy_row.removeClass('hidden');
                        dummy_row.addClass('additional-sa-row');
                        dummy_row.removeAttr('id');
                        dummy_row.find('textarea').attr('data-seq', seq).val(info.hasi_text);
                        dummy_row.find('h4 b')
                            .html(type.charAt(0).toUpperCase() + type.slice(1) + ' ' +
                                seq + ': <span class="editable">Name</span>');

                        $(dummy_row).find('span')
                            .attr('data-type', 'text')
                            .attr('data-seq', seq)
                            .attr('data-kind', type)
                            .attr('data-pk', asset_id)
                            .attr('data-url', jsglobals.base_url + 'cpdv1/update_sa_info_name')
                            .text(info.hasi_name);

                        $('textarea[data-type="' + type + '"]').last().parent().after(dummy_row);
                    } else {
                        $('span[data-seq="' + info.hasi_seq + '"][data-kind="' + info.hasi_type + '"]')
                            .attr({
                                'data-info-type': info.hasi_type,
                                'class': 'editable',
                                'data-url': jsglobals.base_url + 'cpdv1/update_sa_info_name',
                                'data-name': info.hasi_type,
                                'data-pk': asset_id,
                                'data-type': 'text',
                                // 'title': 'New ' + deal['code_name'] + ' Name'
                            })
                            .text(info.hasi_name);
                        $('textarea[data-seq="' + info.hasi_seq + '"][data-type="' + info.hasi_type + '"]')
                            .val(info.hasi_text);
                    }
                });

                enableEditable();
            });

            // Update SA fields
            $(document).off('change', '.sa-field');
            $(document).on('change', '.sa-field', function (event) {
                const type = $(this).attr('data-type');
                makeAjaxCall('cpdv1/save_sa',
                    {
                        id: asset_id,
                        type: type,
                        val: $(this).val(),
                        seq: $(this).data('seq')
                    });
            });

            // Add new SA info click handler.
            $(document).off('click', '.add-sa-info');
            $(document).on('click', '.add-sa-info', function () {
                const type = $(this).attr('data-type'),
                    dummy_row = $('#dummy_' + type).clone(),
                    seq = $('textarea[data-type="' + type + '"]').length;

                dummy_row.removeClass('hidden');
                dummy_row.removeAttr('id');
                dummy_row.find('textarea').attr('data-seq', seq);
                dummy_row.find('h4 b')
                    .html(type.charAt(0).toUpperCase() + type.slice(1) + ' ' +
                        seq + ': <span class="editable">Name</span>');

                $('textarea[data-type="' + type + '"]').last().parent().after(dummy_row);
            });


            $('#sa-row .editable').click(function () {
                $(document).off('focusin.modal');
            });
        },
        enableEditable = function () {
            $(document).on('click', '#sa-row .editable', function () {
                $(document).off('focusin.modal');

                $('#sa-row .editable').editable({
                    params: function (params) {
                        let data = {};
                        data['seq'] = $(this).attr('data-seq');
                        data['pk'] = $(this).attr('data-pk');
                        data['type'] = $(this).attr('data-kind');
                        data['value'] = params.value;
                        return data;
                    }
                });

                setEditable();
            });
        };

    return {
        init: init,
    };
}();