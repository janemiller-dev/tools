/**
 *
 * View Comparables.
 *
 * @summary      Dialog used to display Comparables for an assets.
 * @description  This file contains functions for Displaying Comparables.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */

$(document).ready(function () {

    $('#view-comps-dialog').on('show.bs.modal', function (event) {
        viewComps.init(event);
    });

    // Set Editable
    $('#view-comps-dialog').on('shown.bs.modal', function () {
        setEditable();
        $(document).off('focusin.modal');
    });
});

/**
 *
 * View Notes available.
 */

let viewComps = function () {
    // Initialize and get the DMD ID.
    let asset_id,
        init = function (event) {
            asset_id = $(event.relatedTarget).attr('data-id');
            const get_comps = makeAjaxCall('cpdv1/get_comps', {
                asset_id: asset_id
            });

            get_comps.then(function (data) {
                let content = '<table class="table"><tbody><tr style="border: solid 1px #eee">';
                $('#comps-table').empty();

                $.each(data.comps_data, function (index, deal) {
                    let img_src = 'https://cpd.powersellingtools.com/wp-content/uploads/2020/04/Default-Profile-Pic.png';
                    (null !== deal.ac_avatar) ?
                        img_src = window.location.origin +
                            '/tools/homebase/get_owner_image/?img='
                            + deal.ac_avatar : '';

                    // Check if this is the third element in array.
                    // If so add new row.
                    if ((index !== 0) && (index % 3 === 0)) {
                        content += '</tr><tr style="border: solid 1px #eee">';
                    }

                    const built_year = (null !== deal.ac_built) ? deal.ac_built : "Enter Built Year:",
                        list_price = (null !== deal.ac_price) ? deal.ac_price : 'Enter LIST PRICE: ',
                        price_per_sf = (null !== deal.ac_price_per_sf) ? deal.ac_price_per_sf : 'Enter Price / SF: ',
                        price_per_unit = (null !== deal.ac_price_per_unit) ? deal.ac_price_per_unit : 'Enter Price / Units:',
                        units = (null !== deal.ac_units) ? deal.ac_units : 'Enter Units: ',
                        avg_sf = (null !== deal.ac_avg_sf) ? deal.ac_avg_sf : 'Enter Avg SF: ';

                    content += '<td><table><td class="upload-comps-image" >' +
                        '<img src="' + img_src + '"' +
                        ' id="agent-comps-avatar" style="width: 11vw">' +
                        '<div class="upload">' +
                        '<input id="asset-comp-upload" type="file" name="files[]"' +
                        ' data-url=' + jsglobals.base_url + 'homebase/upload_comp_image' +
                        ' style="visibility: hidden; float:left; width:0;">' +
                        '<a id="upload-comp-img" data-id="' + deal.ac_id + '">Click to Change Image</a></div></td>' +
                        '<td style="text-align: left">' +
                        '<table style="border-collapse: separate; border-spacing: 0.9em 0.3em; font-size: 15px;' +
                        ' width: 100%" data-address="' + deal.ac_address + '" data-id="' + deal.ac_id + '">' +
                        '<tr>' +
                        '<td style="text-align: left">BUILT:</td>' +
                        '<td style="text-align: right"><span class="editable" id="ac_built_year' + deal.ac_id + '"' +
                        ' data-pk="' + deal.ac_id + '" data-name="ac_built" data-placement="top"' +
                        ' data-url="/tools/common/update_comps_info" data-type="text">' +
                        built_year + '</span></td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td style="text-align: left">UNITS:</td>' +
                        '<td style="text-align: right"><span class="editable" id="ac_units' + deal.ac_id + '"' +
                        ' data-pk="' + deal.ac_id + '" data-name="ac_units" data-placement="top"' +
                        ' data-url="/tools/common/update_comps_info" data-type="text">' + units +
                        '</span></td> ' +
                        '</tr>' +
                        '<tr>' +
                        '<td style="white-space: nowrap; text-align: left">PRICE / UNIT:</td>' +
                        '<td style="text-align: right"><span class="editable" id="ac_price_per_unit' + deal.ac_id + '"' +
                        ' data-pk="' + deal.ac_id + '" data-name="ac_price_per_unit" data-placement="top"' +
                        ' data-url="/tools/common/update_comps_info" data-type="text">' + price_per_unit +
                        '</span></td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td style="white-space: nowrap; text-align: left">PRICE / SF:</td>' +
                        '<td style="text-align: right"><span class="editable" id="ac_price_per_sf' + deal.ac_id + '"' +
                        ' data-pk="' + deal.ac_id + '" data-name="ac_price_per_sf" data-placement="top"' +
                        ' data-url="/tools/common/update_comps_info" data-type="text">'
                        + price_per_sf + '</span></td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td style="text-align: left">AVG SF:</td>' +
                        '<td style="text-align: right"><span class="editable" id="aai_avg_sg' + deal.ac_id + '"' +
                        ' data-pk="' + deal.ac_id + '" data-name="ac_avg_sf" data-placement="top"' +
                        ' data-url="/tools/common/update_comps_info" data-type="text">' + avg_sf +
                        '</span></td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td style="text-align: left">LIST PRICE:</td>' +
                        '<td style="text-align: right"><span class="editable" id="aai_list_price' + deal.ac_id + '"' +
                        ' data-pk="' + deal.ac_id + '" data-name="ac_price" data-placement="top"' +
                        ' data-url="/tools/common/update_comps_info" data-type="text">' + list_price + '</span></td>' +
                        '</tr>' +
                        '</table>' +
                        '</td></table></td>'
                });

                content += '</tr></table>';
                $('#comps-table').append(content);
            });

            $('#add-comps').off('click');
            $('#add-comps').click(function () {
                const add_comps = makeAjaxCall('cpdv1/add_comps', {
                    'asset_id': asset_id
                });

                add_comps.then(function (data) {
                })
            });

            // Upload Owner Image Click handler.
            $(document).off('click', '#upload-comp-img');
            $(document).on('click', '#upload-comp-img', function () {
                const comp_id = $(this).attr('data-id');
                $('#asset-comp-upload').trigger('click');

                const target = 'asset-comp-upload',
                    file_type_allowed = /.\.(png|jpeg|jpg)/i,
                    data = {
                        'comp_id': comp_id
                    };

                // Upload Image file.
                const upload_image = uploadFile(target, data, file_type_allowed);
                upload_image.then(function (data) {
                    refreshPage();
                });
            });

            // Agent Comps Image click handler.
            $(document).off('click', '.agent-comps-image');
            $(document).on('click', '.agent-comps-image', function () {
                $('.agent-asset-active').removeClass('agent-asset-active');
                $(this).parent().addClass('agent-asset-active');
                $(this).parent().next().addClass('agent-asset-active');
            });

            // Comps pics image click handler.
            $(document).on('click', '#owner-comps-pics', function () {
                $('#comps-deals-div').removeClass('hidden');
                $('#comps-map-div').addClass('hidden');
                $('#comps-info-div').addClass('hidden');
            });

            // Comps Info click handler.
            $(document).on('click', '#owner-comps-info', function () {
                $('#comps-info-div').removeClass('hidden');
                $('#comps-deals-div').addClass('hidden');
                $('#comps-map-div').addClass('hidden');

                const get_comps_info = makeAjaxCall('cpdv1/get_comps_info', {
                    id: $('.agent-asset-active table').attr('data-id')
                });

                get_comps_info.then(function (data) {
                    $.each(data.comps_info[0], function (key, val) {
                        $('.agent-comps-input[data-col="' + key + '"]').val(val);
                    })
                })
            });

            // Comps Input field change.
            $(document).off('change', '.agent-comps-input');
            $(document).on('change', '.agent-comps-input', function () {
                makeAjaxCall('cpdv1/update_comps_data', {
                    val: $(this).val(),
                    id: $('.agent-asset-active table').attr('data-id'),
                    col: $(this).attr('data-col')
                });
            });

            // Show Agent Asset Map.
            $(document).off('click', '#owner-comps-map');
            $(document).on('click', '#owner-comps-map', function () {
                $('#comps-deals-div').addClass('hidden');
                $('#comps-info-div').addClass('hidden');
                $('#comps-map-div').removeClass('hidden');
                let lat,
                    lng,
                    address = $('.agent-asset-active table').attr('data-address');

                if (address !== 'null') {
                    const cordinate = getLongLat(address);
                    cordinate.then(function (data) {
                        lat = data.results[0].geometry.location.lat;
                        lng = data.results[0].geometry.location.lng;
                        initCompsMap();
                    });
                } else {
                    toastr.warning('Please update asset address field.', 'Asset Address is not present.')
                }

                // Initialize Map.
                function initCompsMap() {
                    let asset_address = new google.maps.LatLng(lat, lng),
                        map = new google.maps.Map(document.getElementById('comps-map-div'), {
                            zoom: 15,
                            center: asset_address
                        }),
                        customStyled = [{
                            featureType: "poi",
                            stylers: [
                                {visibility: "off"}
                            ]
                        }],

                        contentString = '<table>' + $('.agent-asset-active').parent().html() + '</table>',

                        // Set up Popover with asset info.
                        infowindow = new google.maps.InfoWindow({
                            content: contentString
                        }),

                        marker = new google.maps.Marker({
                            position: asset_address,
                            map: map,
                            title: 'Asset Information'
                        });

                    // Set Styling for map.
                    map.set('styles', customStyled);
                    $('#enable-poi').removeClass('hidden');

                    // Show Points Of interest.
                    $('#enable-poi').click(function () {
                        $('#enable-poi').addClass('hidden');
                        $('#disable-poi').removeClass('hidden');

                        let customStyle = [{
                            featureType: "poi",
                            stylers: [
                                {visibility: "on"}
                            ]
                        }];

                        map.set('styles', customStyle);
                    });

                    // Hide Points of Interest.
                    $('#disable-poi').click(function () {
                        $('#enable-poi').removeClass('hidden');
                        $('#disable-poi').addClass('hidden');

                        let customStyle = [{
                            featureType: "poi",
                            stylers: [
                                {visibility: "off"}
                            ]
                        }];

                        map.set('styles', customStyle);
                    });

                    // Add Asset info popover to map marker.
                    marker.addListener('click', function () {
                        infowindow.open(map, marker);
                    });

                    infowindow.open(map, marker);
                }
            });
        };

    return {
        init: init,
    };
}();