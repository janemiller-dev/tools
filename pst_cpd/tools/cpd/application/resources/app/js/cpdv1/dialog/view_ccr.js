/**
 *
 * View DMD ccr.
 *
 * @summary      Dialog used to display Notes for DMD clients.
 * @description  This file contains functions for Displaying DMD client Notes..
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */

$(document).ready(function () {

    $('#view-ccr-dialog').on('show.bs.modal', function (event) {
        viewCCR.init(event);
    });
});

/**
 *
 * View Notes available.
 */

let viewCCR = function () {

    let max_seq = 0,
        deal_id;

    // Initialize and get the DMD ID.
    let init = function (event) {
        deal_id = $(event.relatedTarget).data('id');

        const get_ccr_info = makeAjaxCall('cpdv1/get_ccr_info', {
            id: deal_id
        });

        get_ccr_info.then(function (data) {
            $('#ccr-table tbody').empty();
            $.each(data.ccr_info, function (index, ccr) {
                let date_col = '',
                    duration_col = '',
                    purpose_col = '',
                    rating_col = '',
                    day = '',
                    day_name = ['', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
                $.each(ccr, function (index, ind_ccr) {
                    rating_col += '<p>';

                    for (let i = 1; i <= 5; i++) {
                        let fa_class = 'fa-star-o';
                        ind_ccr.ccm_rating >= i ? fa_class = 'fa-star' : '';
                        rating_col += '<span class="fa ' + fa_class + ' rating"' +
                            ' data-val="' + i + '" data-id="' + ind_ccr.ccm_id + '"></span>';

                    }

                    rating_col += '</p>';

                    day = moment(ind_ccr.ccm_date, "DD-MM-YYYY").day();
                    date_col += '<p>' + ind_ccr.ccm_date + ' / ' + day_name[day] + '</p>';

                    (null !== ind_ccr.ccm_end) ?
                        duration_col += '<p>' + moment.utc(moment.utc(ind_ccr.ccm_end, "HH:mm:ss") -
                            moment.utc(ind_ccr.ccm_start, "HH:mm:ss")).format('H:mm:ss') + '</p>' :
                        duration_col += '<p></p>';

                    (null !== ind_ccr.ccm_purpose) ?
                        purpose_col += '<p>' + ind_ccr.ccm_purpose + '</p>' : purpose_col += '<p></p>';
                });

                $('#ccr-table tbody').append('<tr>' +
                    '<td class="text-center">' + date_col + '</td>' +
                    '<td class="text-center">' + duration_col + '</td>' +
                    '<td class="text-center">' + purpose_col + '</td>' +
                    '<td class="text-center">' + rating_col + '</td>' +
                    '</tr>')
            });

            // Give star rating to calls.
            $('.rating').click(function () {
                const val = $(this).attr('data-val') - 1,
                    ccm_id = $(this).attr('data-id');

                $(this).siblings('.rating').removeClass('fa-star').addClass('fa-star-o');
                $(this).addClass('fa-star').removeClass('fa-star-o');

                for (let i = 0; i < val; i++) {
                    $(this).siblings('.rating').eq(i).addClass('fa-star').removeClass('fa-star-o');
                }

                const update_rating = makeAjaxCall('cpdv1/update_call_rating', {
                    val: val + 1,
                    id: ccm_id
                });

                update_rating.then(function (data) {
                })
            })
        });
    };

    return {
        init: init,
    };
}();