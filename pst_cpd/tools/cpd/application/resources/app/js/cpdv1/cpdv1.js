/**
 *
 * Client Project Dashboard Deals Listing.
 *
 * @summary      This file contains functions related to Deals present in DMD.
 * @description  Contains functions for drawing DMD layout and Event Handling.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */

let assignee, products, portfolio, leads_info, data_info;
$(document).ready(function () {
    getComponents();
    refreshPage();
    get_all_products().then(function (data) {
        const product_type_selector = $('select[data-type="product-type"]');

        product_type_selector
            .append('<option value="-1" disabled selected>-- Select Product Type --</option>')
            .append('<option value="" selected>-- None --</option>');

        $.each(data, function (index, product) {
            product_type_selector
                .append('<option style="font-weight: bold; font-style: italic;"' +
                    ' value="' + product[0]['u_product_group_id'] + '">' + index + '</option>');

            $.each(product, function (index, individual_product) {
                product_type_selector
                    .append('<option value="' + individual_product.u_product_id + '">' +
                        individual_product.u_product_name + '</option>');
            });
        });

        // Check for Objective drop down change.
        $('#cpd-objective').off('change');
        $('#cpd-objective').change(function () {
            $('#' + $(this).val()).removeClass('hidden');
            $('#' + $('#cpd-objective').find('option:not(:selected)').val()).addClass('hidden');
        })
    });

    // Enable Double scroll.
    $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
        TabDoubleScroll();
    });
});

let year, current_quarter, cpd_id, deal_codes;

// Array for converting month number to name.
const month_name = {
    1: 'JAN',
    2: 'FEB',
    3: 'MAR',
    4: 'APR',
    5: 'MAY',
    6: 'JUN',
    7: 'JUL',
    8: 'AUG',
    9: 'SEP',
    10: 'OCT',
    11: 'NOV',
    12: 'DEC'
};

// Array for quarters.
const quarter = {
    1: [1, 3],
    2: [4, 6],
    3: [7, 9],
    4: [10, 12]
};


// Fetch the DMD metric Data.
function getMetric(that) {
    localStorage.setItem('active_nav', $(that).find('a').attr('href'));

    // Fetch the DMD Metrics Data.
    const cpd_metrics = makeAjaxCall('cpdv1/get_cpd_metric', {
        'id': cpd_id,
        'tab': $(that).attr('data-type'),
        'quarter': current_quarter,
    });

    let count, amount, price, gross, deal_name = [];
    count = new Array(3).fill(0);
    amount = new Array(3).fill(0);
    price = new Array(3).fill(0);

    cpd_metrics.then(function (data) {

        let content = '<div class="row">',
            color_array = ["https://cpd.powersellingtools.com/wp-content/uploads/2020/01/white.png",
                "https://cpd.powersellingtools.com/wp-content/uploads/2020/01/yellow1.png"],
            colors_div = '<ul style="list-style-type:none; padding:20px">';

        // Prepare array for count, amount and price.
        // Generation deal Status
        if ($(that).attr('data-type') === 'gds') {
            deal_name[0] = 'Meeting Scheduled Count';
            deal_name[1] = 'Meeting Completed Count';
            deal_name[2] = 'Proposals Requested Count';
            deal_name[3] = 'Proposals Presented Count';
            deal_name[4] = 'Deals Listed Count';
            deal_name[5] = 'Deals Launched Count';
            count = new Array(6).fill(0);
            colors_div = '<ul style="list-style-type:none; padding:20px">';

            $.each(data.metric, function (index, metric) {
                if ('scheduled' === metric.ds_status) {
                    count[0] = parseInt(metric.count);
                } else if ('completed' === metric.ds_status) {
                    count[1] = parseInt(metric.count);
                } else if ('requested' === metric.ds_status) {
                    count[2] = parseInt(metric.count);
                } else if ('presented' === metric.ds_status) {
                    count[3] = parseInt(metric.count);
                } else if ('listed' === metric.ds_status) {
                    count[4] = parseInt(metric.count);
                } else if ('launched' === metric.ds_status) {
                    count[5] = parseInt(metric.count);
                }
            });

            const col = 12 / count.length;

            // Append the popover content.
            for (let i = 0; i < count.length; i++) {
                content += '<div class="col-xs-' + col + '" style="border-right: 1px solid #e1c8c8">' +
                    '<span><b> ' + deal_name[i] + ' </b></span>' +
                    '<input type=\'text\' class=\'form-control' +
                    ' display:inline-block\' value="' + count[i] + '">' +
                    '</div>';

                colors_div += '<li><img ' +
                    'src="' + color_array[i % 2] + '"/> : '
                    + deal_name[i].replace(" Count", "") + '</li>';
            }

            colors_div += '</ul>';

            $('#cpd-colors').attr('data-content', colors_div);
        }
        // Complete deal Status
        else if ($(that).attr('data-type') === 'cds') {
            deal_name[0] = 'Contracts Signed Count';
            deal_name[1] = 'Contracts Hard Count';
            deal_name[2] = 'Deals Closed Count';
            deal_name[3] = 'Repeat Business Count';

            count = new Array(4).fill(0);
            amount = new Array(4).fill(0);
            price = new Array(4).fill(0);
            gross = new Array(4).fill(0);

            $.each(data.metric, function (index, metric) {
                getMetricData(metric, count, amount, price, gross);
            });

            content = get_metric_content(count, price, gross, amount, deal_name);
        }
        // Strategic Deal Status.
        else if ('str_ds' === $(that).attr('data-type')) {
            deal_name[0] = 'Package Requested';
            deal_name[1] = 'Updates Scheduled';
            deal_name[2] = 'Analysis Delivered';


            $.each(data.metric, function (index, metric) {
                if ('request_sp' === metric.ds_status) {
                    count[0] = parseInt(metric.count);
                    amount[0] = parseInt(metric.net);
                    price[0] = parseInt(metric.price);
                } else if ('schedule_su' === metric.ds_status) {
                    count[1] = parseInt(metric.count);
                    amount[1] = parseInt(metric.net);
                    price[1] = parseInt(metric.price);
                } else if ('deliver_sa' === metric.ds_status) {
                    count[2] = parseInt(metric.count);
                    amount[2] = parseInt(metric.net);
                    price[2] = parseInt(metric.price);
                }
            });

            const col = 12 / count.length;
            colors_div = '<ul style="list-style-type:none; padding:20px">';

            // Append the popover content.
            for (let i = 0; i < count.length; i++) {
                content += '<div class="col-xs-' + col + '" style="border-right: 1px solid #e1c8c8">' +
                    '<span><b> ' + deal_name[i] + ' </b></span>' +
                    '<input type=\'text\' class=\'form-control' +
                    ' display:inline-block\' value="' + count[i] + '">' +
                    '</div>'

                colors_div += '<li><img ' +
                    'src="' + color_array[i % 2] + '"/> : '
                    + deal_name[i].replace(" Count", "") + '</li>';
            }

            colors_div += '</ul>';

            $('#cpd-colors').attr('data-content', colors_div);
        }
        // Buyer Deal status.
        else if ('bds' === $(that).attr('data-type')) {
            deal_name[0] = 'Tour Scheduled';
            deal_name[1] = 'Tour Completed';
            deal_name[2] = 'Offer Submitted';
            deal_name[3] = 'Offer Received';
            deal_name[4] = 'Contract Offered';
            deal_name[5] = 'Contract Accept';

            count = new Array(6).fill(0);
            amount = new Array(6).fill(0);
            price = new Array(6).fill(0);
            gross = new Array(6).fill(0);

            $.each(data.metric, function (index, metric) {
                getMetricData(metric, count, amount, price, gross);
            });

            content = get_metric_content(count, price, gross, amount, deal_name);

        }
        content += '</div>';
        $('#cpd-metrics').attr('data-content', content)
    });
}

// Set the data for metrics.
function getMetricData(metric, count, amount, price, gross) {

    if (('contract' === metric.ds_status) || ('schedule_to' === metric.ds_status)) {
        count[0] = parseInt(metric.count);
        amount[0] = parseInt(metric.net);
        price[0] = parseInt(metric.price);
        gross[0] = parseInt(metric.gross);
    } else if (('hard' === metric.ds_status) || ('complete_to' === metric.ds_status)) {
        count[1] = parseInt(metric.count);
        amount[1] = parseInt(metric.net);
        price[1] = parseInt(metric.price);
        gross[1] = parseInt(metric.gross);
    } else if (('complete' === metric.ds_status) || ('submitted_o' === metric.ds_status)) {
        count[2] = parseInt(metric.count);
        amount[2] = parseInt(metric.net);
        price[2] = parseInt(metric.price);
        gross[2] = parseInt(metric.gross);
    } else if (('deal_pros' === metric.ds_status) || ('accepted_o' === metric.ds_status)) {
        count[3] = parseInt(metric.count);
        amount[3] = parseInt(metric.net);
        price[3] = parseInt(metric.price);
        gross[3] = parseInt(metric.gross);
    } else if (('offered_co' === metric.ds_status)) {
        count[4] += parseInt(metric.count);
        amount[4] += parseInt(metric.net);
        price[4] += parseInt(metric.price);
        gross[4] = parseInt(metric.gross);
    } else if (('accepted_co' === metric.ds_status)) {
        count[5] += parseInt(metric.count);
        amount[5] += parseInt(metric.net);
        price[5] += parseInt(metric.price);
        gross[5] = parseInt(metric.gross);
    }
}

function get_metric_content(count, price, gross, amount, deal_name) {

    const col = 12 / count.length;
    let content = '',
        colors_div = '<ul style="list-style-type:none; padding:20px">',
        color_array = ["https://cpd.powersellingtools.com/wp-content/uploads/2020/01/white.png",
            "https://cpd.powersellingtools.com/wp-content/uploads/2020/01/yellow1.png"];

    // Append the popover content.
    for (let i = 0; i < count.length; i++) {
        content += '<div class="col-xs-' + col + '" style="border-right: 1px solid #e1c8c8">' +
            '<span><b> ' + deal_name[i] + ' </b></span>' +
            '<input type=\'text\' class=\'form-control' +
            ' display:inline-block\' value="' + count[i] + '">' +
            '<span><b> Total Value </b></span>' +
            '<input type=\'text\' class=\'form-control' +
            ' display:inline-block\' value="' + numeral(price[i]).format(('$0,0')) + '">' +
            '<span><b> Total Gross </b></span>' +
            '<input type=\'text\' class=\'form-control' +
            ' display:inline-block\'  value="' + numeral(gross[i]).format(('$0,0')) + '">' +
            '<span><b> Total Net </b></span>' +
            '<input type=\'text\' class=\'form-control' +
            ' display:inline-block\'  value="' + numeral(amount[i]).format(('$0,0')) + '">' +
            '</div>';

        colors_div += '<li><img ' +
            'src="' + color_array[i % 2] + '"/> : ' + deal_name[i].replace(" Count", "") + '</li>';
    }

    colors_div += '</ul>';

    $('#cpd-colors').attr('data-content', colors_div);

    return content;
}

/**
 * Fetches DMD data from server and redraws the layout.
 *
 */
function refreshPage() {

    // Get the DMD ID from the URL
    const path = window.location.pathname;
    const components = path.split('/');
    cpd_id = components[components.length - 1];

    // Get the current quarter value.
    current_quarter = (window.location.search.split('?q=')[1] &&
        (0 !== parseInt(window.location.search.split('?q=')[1]))) ?
        parseInt(window.location.search.split('?q=')[1]) : parseInt(moment().quarter());

    // Click Handler for Navtabs.
    $('.cpd-tabs li').unbind('click');
    $('.cpd-tabs li').click(function () {
        getMetric($(this));
    });

    // Get active navbar if there were any.
    $('.cpd-tabs a[href="' + localStorage.active_nav + '"]').trigger('click');
    $('#qtr_actual').val(numeral(0).format('$0,0'));

    // $('#user-acm').attr('onclick', "location.href=jsglobals.base_url + 'cpdv1/acm' + '?id=' + cpd_id");
    // ID selector.
    const prev_quarter_selector = $('#previous-quarter-cpd');
    const next_quarter_selector = $('#next-quarter-cpd');

    // Prev Quarter Link
    prev_quarter_selector
        .css('display', 'inline-block')
        .attr('href', path + '?q=' + (current_quarter - 1));

    // Next Quarter Link.
    next_quarter_selector
        .css('display', 'inline-block')
        .attr('href', path + '?q=' + (current_quarter + 1));

    $('span#quarter-number').text(current_quarter);

    // Check if current quarter is 1 or 4 and hide previous or next quarter link.
    (current_quarter === 1) ? prev_quarter_selector.css('display', 'none') : '';
    (current_quarter === 4) ? next_quarter_selector.css('display', 'none') : '';

    // Get all the codes for deal dialog and status table
    const codes = {
        "gen_status": "gds", "com_status": "cds", "str_status": "str_ds", "clu": "clu", "rwt": "rwt", "bds": "bds",
        "complete_ds": "comds", "generation_ds": "gends", "stra_status": "stra_ds", "buyer_ds": "buyer_ds",
        "rds": "rds", "referral_ds": "ref_ds"
    };

    // Get the all available codes.
    const get_codes = makeAjaxCall('code/get_codes/dummy', {
        'codes': codes
    });

    get_codes.then(function (data) {
        deal_codes = data.codes;
        updateDMD(deal_codes, cpd_id, current_quarter);
    });

    // Look if any of the FTG values is changed.
    $('.cpd_ftg').change(function () {

        // Ajax call to update FTG values.
        makeAjaxCall('cpdv1/update_ftg', {
            cpd_id: cpd_id, quarter: current_quarter,
            year: year, col: $(this).attr('data-name'), type: $(this).attr('data-type'),
            val: $(this).val().replace('$', '').replace('%', '').replace(/,/g, '')
        });
    });

    // Filter DMD records
    const filter_cpd_selector = $('.filter-cpd'),
        sort_cpd_selector = $('.sort-cpd');
    filter_cpd_selector.off('change');
    filter_cpd_selector.change(function () {
        filter_cpd_selector.not(this).val('');
        sort_cpd_selector.val('');
        updateDMD(deal_codes, cpd_id, current_quarter, $(this).attr('data-key'), $(this).val())
    });

    // Sort DMD record.
    sort_cpd_selector.off('change');
    sort_cpd_selector.change(function () {
        filter_cpd_selector.val('');
        updateDMD(deal_codes, cpd_id, current_quarter, $(this).attr('data-key'), $(this).val(), 'sort');
    })

}


/**
 *  Update DMD list.
 *
 * @param  deal_codes      Object List of codes for all type of deals in DMD.
 * @param  cpd_id          Int    DMD Instance ID.
 * @param  current_quarter Int    Currently selected quarter Number.
 * @param key              String Search Key.
 * @param val              String Search Value.
 * @param is_sort          String Check if sort is enabled.
 */
function updateDMD(deal_codes, cpd_id, current_quarter, key, val, is_sort) {

    const month_list = moment.months();

    //empty the select lists firstly
    $('#deal-move-date, #deal-new-move-date, #deal-status, #deal-clu, #deal-rwt, #deal-new-status').empty();

    const date_selector = $('#deal-move-date, #deal-new-move-date');

    // Show the list of months.
    date_selector.append($('<option>')
        .attr({value: 0})
        .append("Select Move Month"));

    $.each(month_list, function (month, m_name) {
        date_selector.append($('<option>')
            .attr({value: month + 1})
            .append(m_name));
    });

    // Get the codes values.
    const gen_status = deal_codes.gds,
        com_status = deal_codes.cds,
        strategic_status = deal_codes.str_ds,
        buyer_status = deal_codes.bds,
        referral_status = deal_codes.rds,
        clu = deal_codes.clu,
        rwt = deal_codes.rwt,
        deal_status_selector = $('#deal-new-status, #deal-status');

    // Add the status to New Deal Dialog select box and Move Deal dialog box.
    deal_status_selector.empty();
    // Append Generation status to select List
    deal_status_selector
        .append($('<option disabled>')
            .attr({value: 0})
            .append("-- Select Status --"))
        .append($('<optgroup>')
            .attr({label: 'Generation Status'})
            .append(
                $.map(gen_status, function (gds) {
                    return '<option value="' + gds.code_abbrev +
                        '" data-status-type="gds">' + gds.code_name + '</option>'
                })
            )
        );

    // Append Completion status to select List
    deal_status_selector
        .append($('<optgroup>')
            .attr({label: 'Completion Status'})
            .append(
                $.map(com_status, function (cds) {
                    return '<option value="' + cds.code_abbrev +
                        '" data-status-type="cds">' + cds.code_name + '</option>'
                })
            )
        );

    // Append Strategic status to select List
    deal_status_selector
        .append($('<optgroup>')
            .attr({label: 'Strategic Status'})
            .append(
                $.map(strategic_status, function (sdt) {
                    return '<option value="' + sdt.code_abbrev +
                        '" data-status-type="str_ds">' + sdt.code_name + '</option>'
                })
            )
        );

    // Append Buyer status to select List.
    deal_status_selector
        .append($('<optgroup>')
            .attr({label: 'Buyer Status'})
            .append(
                $.map(buyer_status, function (sdt) {
                    return '<option value="' + sdt.code_abbrev +
                        '" data-status-type="bds">' + sdt.code_name + '</option>'
                })
            )
        );

    // Append referral status to select list.
    // deal_status_selector
    //     .append($('<optgroup>')
    //         .attr({label: 'Referral Status'})
    //         .append(
    //             $.map(referral_status, function (sdt) {
    //                 return '<option value="' + sdt.code_abbrev +
    //                     '" data-status-type="rds">' + sdt.code_name + '</option>'
    //             })
    //         ));

    // Show each deal clu.
    $('#deal-clu').append($('<option>')
        .attr({value: 0})
        .append("Select CLU"));

    // Append Certain/Likely/Uncertain to CLU drop down.
    $.each(clu, function (d_clu, dclu) {
        $('#deal-clu').append($('<option>')
            .attr({value: dclu.code_abbrev})
            .append(dclu.code_name));
    });

    // Show each deal rwt.
    $('#deal-rwt').append($('<option>')
        .attr({value: 0})
        .append("Select RWT"));

    // Append Radish/Wheat/Tree to RWT drop down.
    $.each(rwt, function (d_rwt, drwt) {
        $('#deal-rwt').append($('<option>')
            .attr({value: drwt.code_abbrev})
            .append(drwt.code_name));
    });

    // Get the all available Client Project Dashboard Instances.
    let get_cpd = makeAjaxCall('cpdv1/get_cpd', {
        id: cpd_id,
        current_quarter: current_quarter,
        year: moment().year(),
        key: key,
        val: val,
        is_sort: is_sort
    });

    get_cpd.then(function (data) {

        let prev_portfolio_deals = {},
            prev_individual_deals = {},
            cpd_deals = Object.create(data.cpd_deals);

        $.each(cpd_deals, function (status, deal) {
            prev_individual_deals[status] = deal.filter(ind_deal => ind_deal.hca_portfolio_id === null);
            prev_portfolio_deals[status] = deal.filter(ind_deal => ind_deal.hca_portfolio_id !== null);
        });

        let individual_deals = Object.create(prev_individual_deals),
            portfolio_deals = Object.create(prev_portfolio_deals);

        let port_merged_array = {};

        // Get portfolio deals merged.
        getMergedArray(port_merged_array, portfolio_deals);

        // Remove array if it's an empty array.
        $.each(port_merged_array, function (status, array) {
            0 !== array.length ? portfolio_deals[status] = array : '';
        });

        let ind_merged_array = {};

        // Get individual deal merged.
        getMergedArray(ind_merged_array, individual_deals);

        // Remove array if it's an empty array.
        $.each(ind_merged_array, function (status, array) {
            0 !== array.length ? individual_deals[status] = array : '';
        });

        updateDeals(data.cpd, data.cpd_deals, deal_codes, data.cpd.leads, portfolio_deals, individual_deals);
        assignee = data.cpd.assignee;
    });
}

function getMergedArray(merged_array, deals) {
    // Merge Deal Repeat and Completed array into Complete.
    merged_array['complete'] = deals['complete'] ? deals['complete'] : [];

    merged_array['deal_pros'] = $.merge(deals['deal_pros'] ?
        deals['deal_pros'] : [], deals['deal_pros_a'] ?
        deals['deal_pros_a'] : []);

    // Merge Meeting Scheduled and Completed array into Prospective.
    merged_array['possible'] = $.merge(deals['scheduled'] ?
        deals['scheduled'] : [], deals['completed'] ?
        deals['completed'] : []);

    // Merge Referral Array.
    merged_array['ref_acce'] = $.merge(deals['ref_pla'] ?
        deals['ref_pla'] : [], deals['ref_acc'] ?
        deals['ref_acc'] : []);

    // Merge completed array.
    merged_array['ref_compl'] = $.merge(deals['ref_pa'] ?
        deals['ref_pa'] : [], deals['ref_com'] ?
        deals['ref_com'] : []);

    //Merge referral offer and recieved.
    merged_array['ref_rece'] = $.merge(deals['ref_off'] ?
        deals['ref_off'] : [], deals['ref_rec'] ?
        deals['ref_rec'] : []);

    // Merge Meeting No and Maybe arrays into Potential
    merged_array['potential'] = $.merge(deals['no'] ?
        deals['no'] : [], deals['maybe'] ?
        deals['maybe'] : []);

    // Merge the Presentation Presented and Requested arrays into Possible.
    merged_array['prospective'] = $.merge(deals['requested'] ?
        deals['requested'] : [], deals['presented'] ?
        deals['presented'] : []);

    // Merge Contract hard and contact array into Predictable array.
    merged_array['predictable'] = $.merge(deals['contract'] ?
        deals['contract'] : [], deals['hard'] ?
        deals['hard'] : []);

    // Merge Deals Listed and Offers made array into Probable array.
    merged_array['probable'] = $.merge(deals['listed'] ?
        deals['listed'] : [], deals['launched'] ?
        deals['launched'] : []);

    // Merge Presentation In Person and Phone/Web arrays into Presentation.
    merged_array['present'] = $.merge(deals['deliver_sa'] ?
        deals['deliver_sa'] : [], deals['offer_sa'] ?
        deals['offer_sa'] : []);

    // Merge Exploratory In Person and Phone/Web arrays into Delivered.
    merged_array['delivered'] = $.merge(deals['schedule_su'] ?
        deals['schedule_su'] : [], deals['deliver_su'] ?
        deals['deliver_su'] : []);

    // Merge Exploratory In Person and Phone/Web arrays into Delivered.
    merged_array['invitation'] = $.merge(deals['request_sp'] ?
        deals['request_sp'] : [], deals['receive_sp'] ?
        deals['receive_sp'] : []);

    // Merge Accepted In Person and Phone/Web arrays into Delivered.
    merged_array['accepted'] = $.merge(deals['p_requested'] ?
        deals['p_requested'] : [], deals['p_complete'] ?
        deals['p_complete'] : []);

    //Merge Offers Submitted and Counter Offers.
    merged_array['offer'] = $.merge(deals['subm_offer'] ?
        deals['subm_offer'] : [],
        deals['count_offer'] ? deals['count_offer'] : []);

    // Merge Buyer Requested and Signed CA.
    merged_array['tour'] = $.merge(deals['buyer_req'] ?
        deals['buyer_req'] : [], deals['sign_ca'] ?
        deals['sign_ca'] : []);

    // Merge Offer Accepted and Diligence.
    merged_array['diligence'] = $.merge(deals['offer_acc'] ?
        deals['offer_acc'] : [],
        deals['diligence'] ? deals['diligence'] : []);

    // Merge Contingency Removed and Contract Closed.
    merged_array['b_contract'] = $.merge(deals['contingency'] ?
        deals['contingency'] : [], deals['b_contr_c'] ?
        deals['b_contr_c'] : []);

    // Merge Buyer Inactive and Active.
    merged_array['b_active'] = $.merge(deals['b_active'] ?
        deals['b_active'] : [], deals['b_inactive'] ?
        deals['b_inactive'] : []);

}

/**
 * Update the deal table
 *
 * @param  cpd              Object Info about DMD deal tables name/title/description.
 * @param  cpd_passed_deals Object Info about each deal rows.
 * @param  deal_codes       Object List of all the codes used for DMD.
 * @param  leads            Object List of all the Leads.
 * @param portfolio_deals   Object List of Portfolio Deals.
 * @param individual_deals  Object List of Individula Deals.
 */

function updateDeals(cpd, cpd_passed_deals, deal_codes, leads, portfolio_deals, individual_deals) {
    portfolio = cpd.portfolio;
    leads_info = leads;
    let buyers = {},
        deals = {};

    // Separat Buyers and Deals
    Object.keys(individual_deals).map(function (key, data) {
        if (('b_active' === key) || ('tour' === key) || ('offer' === key)
            || ('diligence' === key) || ('b_contract' === key)) {
            buyers[key] = individual_deals[key];
        } else {
            deals[key] = individual_deals[key];
        }
    });

    individual_deals = deals;
    // Get required attributes for making fields editable.
    const deal_name = $.makeArray(cpd)[0];
    let deal_status = deal_codes['cds'],
        cpd_deals = Object.create(cpd_passed_deals);

    // Truncate any previous entry in table.
    $('#lead-table').find('tbody').empty();
    $('#source-table').find('tbody').empty();
    $('#action-table').find('tbody').empty();
    $('#whom-table').find('tbody').empty();

    $('#cpd-name').text('DMD Name: ' + cpd.cpd_name + ' || ');
    $('#cpd-desc').text('DMD Description: ' + cpd.cpd_description);


    let lead_count = 1;
    // Append Leads to the Lead Table.
    $.each(leads, function (index, lead) {

        $('#' + lead.dl_type + '-table tbody')
            .append($('<tr>')
                .append($('<td class="text-center">')
                    .append(lead_count++))
                .append($('<td class="text-center">')
                    .append(lead.dl_name))
                .append($('<td class="text-center">')
                    .append($('<button class="btn btn-default btn-sm"' +
                        ' data-toggle="modal"' +
                        ' data-target="#delete-lead-dialog"' +
                        ' data-backdrop="static"' +
                        ' data-type="' + lead.dl_type + '"' +
                        ' data-id="' + lead.dl_id +
                        '" data-name="' + lead.dl_name +
                        '"> <span class="glyphicon glyphicon-trash"></span>')))
            )
    });

    // Update the value of year.
    year = deal_name['cpd_year'];
    $('span#quarter-year').text(moment().year());

    // Merge deals codes
    deal_status = $.merge(deal_status, deal_codes['buyer_ds']);
    deal_status = $.merge(deal_status, deal_codes['ref_ds']);
    deal_status = $.merge(deal_status, deal_codes['stra_ds']);
    deal_status = $.merge(deal_status, deal_codes['comds']);
    deal_status = $.merge(deal_status, deal_codes['gends']);

    // Update or Fetch the value of the name fields.
    $.each(deal_status, function (index, deal) {
        $('#' + deal['code_abbrev'] + '-name').html(deal_name['cpd_' + deal['code_abbrev'] + '_name'])
            .attr({
                'data-url': jsglobals.base_url + 'cpdv1/update_deal_table_info',
                'data-name': 'cpd_' + deal['code_abbrev'] + '_name',
                'data-pk': cpd.cpd_id,
                'data-type': 'text',
                'title': 'New ' + deal['code_name'] + ' Name'
            });
    });

    // Update the Tgd Link.
    $('#linked-tgd').html(cpd.tgd_ui_name ? cpd.tgd_ui_name : 'No TGD')
        .attr({
            'href': cpd.tgd_ui_id ? (jsglobals.base_url + 'tgd/tgd/' + cpd.tgd_ui_id) : '',
        });

    // Show zero for no deal.
    $('input.deal-count').val(0);
    // Show zero dollar for no dollar.
    $('input.deal-dollar').val('$0');

    $('.deal-table tbody').empty();

    // Clone dummy rows.
    let dummy_content = $('.dummy_content').clone();
    dummy_content.removeClass('dummy_content');
    dummy_content.removeClass('hidden');

    let dummy_value_content = $('.dummy_value_content').clone();
    dummy_value_content.removeClass('dummy_value_content');
    dummy_value_content.removeClass('hidden');

    let dummy_revenue_content = $('.dummy_revenue_content').clone();
    dummy_revenue_content.removeClass('dummy_revenue_content');
    dummy_revenue_content.removeClass('hidden');

    let coming_soon_content = $('#coming_soon_content').clone();
    coming_soon_content.removeClass('coming_soon_content');
    coming_soon_content.removeClass('hidden');

    let coming_soon_dst = $('#coming_soon_dst').clone();
    coming_soon_dst.removeClass('coming_soon_dst');
    coming_soon_dst.removeClass('hidden');

    let merged_array = {};

    const yellow_rows_array = [
        'maybe',
        'completed',
        'presented',
        'hard',
        'launched',
        'accepted_co',
        'complete_to',
        'accepted_o',
        'p_complete',
        'receive_sp',
        'deliver_su',
        'offer_sa',
        'ref_pla',
        'ref_pa',
        'ref_off',
        'sign_ca',
        'count_offer',
        'deal_pros_a',
        'diligence',
        'b_contr_c',
        'b_inactive'
    ];

    // Merge Deal Repeat and Completed array into Complete.
    getMergedArray(merged_array, cpd_deals);

    // Remove array if it's an empty array.
    $.each(merged_array, function (status, array) {
        0 !== array.length ? cpd_deals[status] = array : '';
    });

    // Looping through the deal status and populate data in data table.
    $.each(individual_deals, function (status, deal) {
        // Destroy any previous instance of data table.
        try {
            $('#cpd-deal-' + status + '-table').DataTable().destroy();
        } catch (er) {
        }

        // Initialize data table.
        try {
            $('#cpd-deal-' + status + '-table').DataTable({
                fixedHeader: {
                    header: true
                },
                responsive: true,
                "aoColumnDefs": [
                    {"sWidth": "3vw", "aTargets": [0]},
                    {"sWidth": "5vw", "aTargets": [1]},
                    {"sWidth": "6vw", "aTargets": [2]},
                    {"sWidth": "12vw", "aTargets": [3]},
                    {"sWidth": "14vw", "aTargets": [4]},
                    {"sWidth": "4vw", "aTargets": [5]},
                    {"sWidth": "10vw", "aTargets": [6]},
                    {"sWidth": "3vw", "aTargets": [7]},
                    {"sWidth": "3vw", "aTargets": [8]},
                    {"sWidth": "15vw", "aTargets": [9]},
                    {"sWidth": "4vw", "aTargets": [10]},
                    {"sWidth": "12vw", "aTargets": [11]},
                    {"sWidth": "3vw", "aTargets": [12]},
                    {"sWidth": "3vw", "aTargets": [13]},
                    {"sWidth": "3vw", "aTargets": [14]},
                ],
                // "offsetTop": 12,
                "paging": false,
                "searching": false,
                "info": false,
                "bAutoWidth": false,
                "ordering": false,
                "order": [],
                "columnDefs": [
                    {
                        "targets": [0, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                        "orderable": false
                    }],
                "columns": [
                    {"data": null, "name": "Move", "className": "text-center"},
                    {"data": "ds_md", "name": "CD", "className": "text-center"},
                    {"data": null, "name": "product", "className": "text-center"},
                    {"data": "ds_status", "name": "Type", "className": "text-center"},
                    {"data": "deal_name", "name": "Client-Property", "className": "text-center"},
                    {"data": "deal_phone", "name": "deal-phone", "className": "text-center"},
                    {"data": null, "name": "property", "className": "text-center"},
                    {"data": null, "name": "info", "className": "text-center"},
                    {"data": null, "name": "maps", "className": "text-center"},
                    {"data": null, "name": "project", "className": "text-center"},
                    {"data": null, "name": "ref", "className": "text-center"},
                    {"data": null, "name": "rwt", "className": "text-center"},
                    {"data": null, "name": "dcl", "className": "text-center"},
                    {"data": null, "name": "dcg", "className": "text-center"},
                    {"data": "deal_price", "name": "amount", "className": "text-center"},
                    {"data": "deal_lead", "name": "Lead", "className": "text-center"},
                ],
                "data": $.makeArray(deal),
                "rowCallback": function (row, data) {
                    data_info = data;

                    // Check if buyer info is available.
                    let buyer_array = [];
                    (data.cc_name !== null)
                        ? buyer_array = data.cc_name.split(',') : '';

                    //Fetch the HTML content for Popover and replace with values.
                    setupAmountPopoverContent(dummy_content, data);

                    setupValuePopoverContent(dummy_value_content, data);

                    setupRevenuePopoverContent(dummy_revenue_content, data);

                    let text_status = data.ds_status,
                        row_data = '';

                    // Check if deal status is complete, if so change Info button to ref button.
                    row_data = getRowData(row_data, data);

                    const fetch_status = getStatusText(data.ds_status);
                    text_status = ('' !== fetch_status) ? fetch_status : text_status;

                    // Move Button.
                    $('td', row).eq(0).html('<button class = "btn btn-default btn-sm"' +
                        ' data-toggle = "modal"' +
                        ' data-target = "#move-deal-dialog" data-backdrop = "static"' +
                        ' data-id="' + data.deal_id + '" ' +
                        ' data-current-status="' + data.ds_status + '">' +
                        ' <span class = "glyphicon glyphicon-sort"></span></button>');

                    // Move Month Text area.
                    $('td', row).eq(1).html('<input type="text" ' +
                        ' data-id=="' + data.deal_id + '"' +
                        ' style="background-color: #fff; width: 4vw !important"' +
                        ' class="form-control" readonly> ');

                    let p_type = '';

                    (data.upg_name !== '' && data.upg_name !== null) ?
                        p_type = data.upg_name : p_type = data.u_product_name;

                    // Status .
                    $('td', row).eq(2).html('<input type="text" ' +
                        ' data-id=="' + data.deal_id + '" readonly value="' + (text_status).toUpperCase() + '"' +
                        ' style="background-color: #fff"' +
                        ' class="form-control" readonly> ');

                    // Product Type.
                    $('td', row).eq(3).html('<input type="text"' +
                        ' data-id=="' + data.deal_id + '" readonly value="' + p_type + '"' +
                        ' style="background-color: #fff"' +
                        ' class="form-control" readonly> ');

                    // Radish/Wheat/Tree select box.
                    $('td', row).eq(5).html('<select' +
                        ' name="rwt' + data.deal_id + '" class="form-control cpd-info hca-info"' +
                        ' data-id="' + data.hca_id + '" data-name="hca_rwt" ' +
                        ' data-current-status="' + data.ds_status + '">' +
                        '<option value="R">R</option>' +
                        '<option value="W">W</option>' +
                        '<option value="T">T</option>' +
                        '</select>');

                    // Client Name Text field.
                    $('td', row).eq(4).html('<input type="text" ' +
                        ' name="client' + data.deal_id + '" class="form-control cpd-info"' +
                        ' data-id="' + data.hc_id + '" data-name="hc_name" ' +
                        ' data-current-status="' + data.ds_status + '">');

                    $('td', row).eq(6).html('<select class="form-control cpd-info cpd-client-phone" data-id="' + data.hc_id + '"' +
                        ' name="client-phone' + data.deal_id + '"  data-name="hc_selected_phone" >' +
                        '<option value="hc_main_phone">' + data.deal_phone + '(O)</option>' +
                        '<option value="hc_second_phone">' + data.hc_second_phone + '(C)</option>' +
                        '<option value="hc_mobile_phone">' + data.hc_mobile_phone + '(M)</option>' +
                        '</select>');

                    // Info Button
                    $('td', row).eq(7).html('<span class="btn btn-default btn-sm has-popover info_button"' +
                        ' data-toggle="popover" title="Client & Asset Information. <span id=\'close\'' +
                        ' class=\'close\' onclick=\'$(this).popover(&quot;hide&quot;);\'>&times;</span>"' +
                        ' data-container="body" id="info_button' + data.hca_id + '"' +
                        ' data-id="' + data.deal_id + '" data-html="true" data-placement="top"' +
                        ' data-dummy-content="' + dummy_content.html().replace(/"/g, "\'") + '"' +
                        ' data-value-content="' + dummy_value_content.html().replace(/"/g, "\'") + '"' +
                        ' data-revenue-content="' + dummy_revenue_content.html().replace(/"/g, "\'") + '"' +
                        ' data-content="' + getInfoContent(data) + '">' +
                        '<span class="glyphicon glyphicon-info-sign"></span></button>');

                    // Dial Button
                    $('td', row).eq(8).html('<button class="btn btn-default btn-sm" data-toggle="modal" ' +
                        'data-target="#view-ccm-dialog" data-id="' + data.hca_id + '" data-owner="' + data.hc_id + '">' +
                        '<span class="glyphicon glyphicon-phone-alt"></span></button>');

                    // Address Text area.
                    $('td', row).eq(9).html('<textarea rows="1" cols="15" onkeyup="auto_grow(this)"' +
                        ' name="prop' + data.deal_id + '" class="form-control property cpd-info"' +
                        ' data-id="' + data.deal_id + '" data-name="hca_name"' +
                        ' data-current-status="' + data.ds_status + '">' +
                        '</textarea>');

                    // Remove any previous values from drop down.
                    $('td', row).eq(11).html('');
                    $('td', row).eq(11).append(
                        $(getBuyerLayout(buyer_array, data)));

                    //Notes Button
                    $('td', row).eq(12).html('<span class="has-tooltip" title="Next Action Manager">' +
                        '<button class="btn btn-default btn-sm has-popover" data-content="' + getNotesContent(data) + '"' +
                        ' id="notes" data-id="' + data.deal_id + '" data-container="body" data-html="true"' +
                        ' data-toggle="popover" data-placement="top" data-title="Next Action Manager">' +
                        '<span class="glyphicon glyphicon-list"></span></button></span>');

                    // Build components popover.
                    $('td', row).eq(13).html('<button class="btn btn-default btn-sm has-popover component_button"' +
                        ' data-toggle="popover" title="Build Components. <span id=\'close\'' +
                        ' class=\'close\'>&times;</span>"' +
                        ' data-container="body"' +
                        ' data-id="' + data.deal_id + '" data-html="true" data-placement="top"' +
                        ' data-content="' + getBuildComponentLayout(data) + '">' +
                        '<span class="glyphicon glyphicon-send"></span></button>');

                    // Sent Components.
                    $('td', row).eq(14).html('<button class="btn btn-default btn-sm has-popover component_button" ' +
                        'data-toggle="popover" title="Sent Components. <span id=\'close\'' +
                        ' class=\'close\'>&times;</span>' +
                        '" data-container="body"' +
                        ' data-id="' + data.deal_id + '" data-html="true" data-placement="top"' +
                        ' data-content="' + getSentComponents(data) + '"' +
                        '><span class="glyphicon glyphicon-envelope"></span></button>');

                    // Accountability Manager Button.
                    $('td', row).eq(15).html('<button class="btn btn-default btn-sm coming-soon-tool" data-toggle="modal" ' +
                        'data-target="#view-alert-dialog" data-type="asset" data-id="' + data.hc_id + '">' +
                        ' <span class="glyphicon glyphicon-book"></span></button>');

                    // Check if deal status is complete, if so change Info button to ref button.
                    $('td', row).eq(10).html(row_data);

                    // Set values for table columns.
                    $('td:eq(10) select', row).val(data.deal_clu);
                    $('td:eq(1) input', row).val(month_name[data.ds_md]);
                    $('td:eq(5) select', row).val(data.deal_rwt);
                    $('td:eq(9) textarea:first', row).val(data.deal_property);
                    $('td:eq(9) textarea:nth-child(2)', row).val(data.deal_property_note);
                    $('td:eq(11) input', row).val(data.deal_price);
                    $('td:eq(4) input', row).val(data.deal_name);
                    $('td:eq(6) select', row).val(data.hc_selected_phone);

                    $(row).addClass('cpd_deal');
                    $(row).attr('id', 'row' + data.deal_id);

                    disableComponent();

                    if (-1 !== $.inArray(data.ds_status, yellow_rows_array)) {
                        $(row).css('background-color: lightyellow;');
                        $(row).addClass('yellow_row');
                    }
                }
            });
        } catch (e) {
        }

        const deal_table_tr = $('#cpd-deal-' + status + '-table tbody tr');

        // Updating the total number of deals.
        $('#cpd-deal-' + status + '-total-deal').val(
            (0 !== deal_table_tr.length) ?
                deal_table_tr.length : 0
        );

        // Declare Variables and set default to 0.
        let total_doll = 0,
            total_gross = 0,
            total_price = 0;
        usePopover();

        // Sum up total dollar and price.
        $.each(deal, function (index, data) {
            total_doll += ((parseInt(data.deal_price) * parseInt(data.deal_rate) / 100) * (data.deal_gsplit / 100)
                * (parseInt(data.deal_split)) / 100);

            total_price += parseInt(data.deal_price);

            total_gross += (parseInt(data.deal_price) * parseInt(data.deal_rate) / 100);
        });

        // Updating Total Dollars.
        $('#cpd-deal-' + status + '-total-doll').val(
            (0 !== total_doll) ?
                '$' + total_doll.toLocaleString() : '$' + 0
        );

        // Updating Total Gross.
        $('#cpd-deal-' + status + '-total-gross').val(
            (0 !== total_gross) ?
                '$' + total_gross.toLocaleString() : '$' + 0
        );

        // Update Total Value.
        $('#cpd-deal-' + status + '-total-value').val(
            (0 !== total_price) ?
                '$' + total_price.toLocaleString() : '$' + 0
        );

        // Update Quarter Actual Value if deal has a status of complete.
        if ('complete' === status) {
            let qtr_total = 0;
            $.each(deal, function (index, data) {
                if ((quarter[current_quarter][0] <= data.ds_md) && (quarter[current_quarter][1] >= data.ds_md)) {
                    qtr_total += ((parseInt(data.deal_price) * parseInt(data.deal_rate) / 100) *
                        (data.deal_gsplit / 100) * (parseInt(data.deal_split)) / 100);
                }
            });
            $('#qtr_actual').val(numeral(qtr_total).format(('$0,0')));
        }
    });

    // Append Portfolio Deals.
    $.each(portfolio_deals, function (status, deal) {

        if (undefined === $('#cpd-deal-' + status + '-table').find('tbody').find('tr').eq(0).attr('id')) {
            $('#cpd-deal-' + status + '-table').find('tbody').find('tr').eq(0).remove();
        }

        $.each(deal, function (index, data) {
            //Fetch the HTML content for Popover and replace with values.
            setupAmountPopoverContent(dummy_content, data);

            let p_type = '',
                row_data = '';

            // Check if deal status is complete, if so change Info button to ref button.
            row_data = getRowData(row_data, data);

            if ($('#portfolio-row' + data.hca_portfolio_id).length === 0) {

                let text_status = status;
                const fetch_status = getStatusText(data.ds_status);
                text_status = ('' !== fetch_status) ? fetch_status : text_status;

                (data.upg_name !== '' && data.upg_name !== null) ?
                    p_type = data.upg_name : p_type = data.u_product_name;

                let custom_style = '', custom_class = '';
                if (-1 !== $.inArray(data.ds_status, yellow_rows_array)) {
                    custom_style = 'background-color: lightyellow;';
                    custom_class = 'yellow_row';
                }

                // Check if buyer info is available.
                let buyer_array = [];
                (data.cc_name !== null)
                    ? buyer_array = data.cc_name.split(',') : '';

                $('#cpd-deal-' + status + '-table').find('tbody')
                    .append($('<tr id="portfolio-row' + data.hca_portfolio_id + '" data-status="' + status + '" ' +
                        'style="' + custom_style + '" class="' + custom_class + '">')
                        .append($('<td class="text-center">')
                            .append($('<button class = "btn btn-default btn-sm"' +
                                ' data-toggle = "modal"' +
                                ' data-target = "#move-deal-dialog" data-backdrop = "static"' +
                                ' data-id="' + data.deal_id + '" ' +
                                ' data-current-status="' + data.ds_status + '">' +
                                ' <span class = "glyphicon glyphicon-sort"></span></button>')))
                        .append($('<td class="text-center">')
                            .append($('<input type="text" value="' + month_name[data.ds_md] + '" ' +
                                ' data-id=="' + data.deal_id + '"' +
                                ' style="background-color: #fff; width: 4vw !important"' +
                                ' class="form-control" readonly> ')))
                        .append($('<td class="text-center">')
                            .append($('<input type="text" ' +
                                ' data-id=="' + data.deal_id + '" readonly value="' + (text_status).toUpperCase() + '"' +
                                ' style=" background-color: #fff"' +
                                ' class="form-control" readonly> ')))
                        .append($('<td class="text-center">')
                            .append($('<input type="text"' +
                                ' data-id=="' + data.deal_id + '" readonly value="' + p_type + '"' +
                                ' style="background-color: #fff"' +
                                ' class="form-control" readonly> ')))
                        .append($('<td class="text-center">')
                            .append($('<input type="text" value="' + data.deal_name + '" ' +
                                ' name="client' + data.deal_id + '" class="form-control cpd-info"' +
                                ' data-id="' + data.hc_id + '" data-name="hc_name" ' +
                                ' data-current-status="' + data.ds_status + '">' +
                                ' </input>')))
                        .append($('<td class="text-center">')
                            .append($('<select' +
                                ' name="rwt' + data.deal_id + '" class="form-control cpd-info hca-info"' +
                                ' data-id="' + data.hca_id + '" data-name="hca_rwt" ' +
                                ' data-current-status="' + data.ds_status + '">' +
                                ' <option value="R">R</option>' +
                                ' <option value="W">W</option>' +
                                ' <option value="T">T</option>' +
                                ' </select>')))

                        // Phone Number drop down.
                        .append($('<td class="text-center">')
                            .append($('<select value="' + data.deal_phone + '"' +
                                ' name="client-phone' + data.deal_id + '" class="form-control cpd-info cpd-client-phone"' +
                                ' data-id="' + data.hc_id + '" data-name="hc_selected_phone" ' +
                                ' data-current-status="' + data.ds_status + '" id="phone' + data.hc_id + '" >' +
                                '<option value="hc_main_phone">' + data.deal_phone + '(O)</option>' +
                                '<option value="hc_second_phone">' + data.hc_second_phone + '(C)</option>' +
                                '<option value="hc_mobile_phone">' + data.hc_mobile_phone + '(M)</option>' +
                                ' </select>')))

                        // Info popup.
                        .append($('<td class="text-center">')
                            .append($('<button class="btn btn-default btn-sm has-popover info_button"' +
                                ' data-toggle="popover" title="Client & Asset Information. <button type=\'button\' id=\'close\' ' +
                                'class=\'close\'>&times;</button>" data-container="body"' +
                                ' id="info_button' + data.hca_id + '"' +
                                ' data-id="' + data.deal_id + '" data-html="true" data-placement="top"' +
                                ' data-dummy-content="' + dummy_content.html().replace(/"/g, "\'") + '"' +
                                ' data-content="' + getInfoContent(data) + '">' +
                                '<span class="glyphicon glyphicon-info-sign"></span></button>')))

                        // Phone number drop down.
                        .append($('<td class="text-center">')
                            .append($('<button class="btn btn-default btn-sm" data-toggle="modal" ' +
                                'data-target="#view-ccm-dialog" data-id="' + data.hca_id + '">' +
                                '<span class="glyphicon glyphicon-phone-alt"></span></button>')))

                        // Portfolio row.
                        .append($('<td class="text-center">')
                            .append($('<select style="width: 100%"' +
                                ' name="prop' + data.deal_id + '" class="form-control property"' +
                                ' data-id="' + data.deal_id + '"' +
                                ' data-current-status="' + data.ds_status + '">' +
                                '<option disabled selected>Portfolio</option>' +
                                '<option>' + data.up_name + '</option></select>')))

                        .append($('<td class="text-center">')
                            .append(row_data))

                        .append($('<td class="text-center">')
                            .append($(getBuyerLayout(buyer_array, data))))

                        .append($('<td class="text-center">')
                            .append($('<span class="has-tooltip" title="Next Action Manager">' +
                                '<button class="btn btn-default btn-sm has-popover"' +
                                ' data-content="' + getNotesContent(data) + '"' +
                                ' id="notes" data-id="' + data.deal_id + '" data-container="body" data-html="true"' +
                                ' data-toggle="popover" data-placement="top" data-title="Next Action Manager">' +
                                '<span class="glyphicon glyphicon-list"></span></button></span>')))

                        .append($('<td class="text-center">')
                            .append($('<button class="btn btn-default btn-sm has-popover component_button"' +
                                ' data-toggle="popover" title="Build Components." data-container="body"' +
                                ' data-id="' + data.deal_id + '" data-html="true" data-placement="top"' +
                                ' data-content="' + getBuildComponentLayout(data) + '">' +
                                '<span class="glyphicon glyphicon-send"></span></button>')))

                        .append($('<td class="text-center">')
                            .append($('<button class="btn btn-default btn-sm has-popover component_button" ' +
                                'data-toggle="popover" title="Sent Components." data-container="body"' +
                                ' data-id="' + data.deal_id + '" data-html="true" data-placement="top"' +
                                ' data-content="' + getSentComponents(data) + '"' +
                                '><span class="glyphicon glyphicon-envelope"></span></button>')))

                        .append($('<td class="text-center">')
                            .append($('<button class="btn btn-default btn-sm coming-soon-tool" data-toggle="modal" ' +
                                'data-target="#view-alert-dialog" data-type="asset" data-id="' + data.hc_id + '"> ' +
                                '<span class="glyphicon glyphicon-book"></span></button>')))
                    )
                ;

                $('#portfolio-row' + data.hca_portfolio_id + ' td').eq(5).find('select').val(data.deal_rwt);
                $('#portfolio-row' + data.hca_portfolio_id + ' td').eq(13).find('select').val(data.deal_clu);

                $('#phone' + data.hc_id).val(data.hc_selected_phone);

                // Declare Variables and set default to 0.
                let total_doll = 0,
                    total_gross = 0,
                    total_price = 0;
                usePopover();

                total_doll += ((parseInt(data.deal_price) * parseInt(data.deal_rate) / 100) * (data.deal_gsplit / 100)
                    * (parseInt(data.deal_split)) / 100);

                total_price += parseInt(data.deal_price);

                total_gross += (parseInt(data.deal_price) * parseInt(data.deal_rate) / 100);

                // Updating Total Dollars.
                $('#cpd-deal-' + status + '-total-doll').val(
                    '$' + (
                    parseInt($('#cpd-deal-' + status + '-total-doll').val()
                        .replace('$', '').replace(',', '')) + total_doll).toLocaleString()
                );

                // Updating Total Gross.
                $('#cpd-deal-' + status + '-total-gross').val(
                    '$' + (
                    parseInt($('#cpd-deal-' + status + '-total-gross').val()
                        .replace('$', '').replace(',', '')) + total_gross).toLocaleString());

                // Update Total Value.
                $('#cpd-deal-' + status + '-total-value').val(
                    '$' + (
                    parseInt($('#cpd-deal-' + status + '-total-value').val()
                        .replace('$', '').replace(',', '')) + total_price).toLocaleString()
                );

            } else {
                let prop_name = 'No Name';

                (data.deal_property === null || data.deal_property === '') ? '' : prop_name = data.deal_property;

                // Check if table with this asset status exist.
                0 !== $('#cpd-deal-' + status + '-table').length ?
                    $('#portfolio-row' + data.hca_portfolio_id).find('td').eq(7).find('select')
                        .append('<option>' + prop_name + '</option>') : '';
            }

            disableComponent();

        });
        const deal_table_tr = $('#cpd-deal-' + status + '-table tbody tr');

        // Updating the total number of deals.
        $('#cpd-deal-' + status + '-total-deal').val(
            (0 !== deal_table_tr.length) ?
                deal_table_tr.length : 0
        );


        // Update Quarter Actual Value if deal has a status of complete.
        if ('complete' === status) {
            let qtr_total = 0;
            $.each(deal, function (index, data) {
                if ((quarter[current_quarter][0] <= data.ds_md) && (quarter[current_quarter][1] >= data.ds_md)) {
                    qtr_total += ((parseInt(data.deal_price) * parseInt(data.deal_rate) / 100) *
                        (data.deal_gsplit / 100) * (parseInt(data.deal_split)) / 100);
                }
            });

            qtr_total = numeral($('#qtr_actual').val())._value + qtr_total;
            $('#qtr_actual').val(numeral(qtr_total).format(('$0,0')));
        }
    });

    // Append Buyers.
    $.each(buyers, function (status, deal) {

        // if (undefined === $('#cpd-deal-' + status + '-table').find('tbody').find('tr').eq(0).attr('id')) {
        //     $('#cpd-deal-' + status + '-table').find('tbody').find('tr').eq(0).remove();
        // }

        $.each(deal, function (index, data) {
            //Fetch the HTML content for Popover and replace with values.
            setupBuySidePopoverContent(dummy_content, data);

            let p_type = '',
                row_data = '';

            // Check if deal status is complete, if so change Info button to ref button.
            row_data = getRowData(row_data, data);

            // if ($('#portfolio-row' + data.hca_portfolio_id).length === 0) {

            let text_status = status;
            const fetch_status = getStatusText(data.ds_status);
            text_status = ('' !== fetch_status) ? fetch_status : text_status;

            (data.upg_name !== '' && data.upg_name !== null) ?
                p_type = data.upg_name : p_type = data.u_product_name;

            let custom_style = '', custom_class = '';
            if (-1 !== $.inArray(data.ds_status, yellow_rows_array)) {
                custom_style = 'background-color: lightyellow;';
                custom_class = 'yellow_row';
            }
            //
            // // Check if buyer info is available.
            // let buyer_array = [];
            // (data.cc_name !== null)
            //     ? buyer_array = data.cc_name.split(',') : '';

            $('#cpd-deal-' + status + '-table').find('tbody')
                .append($('<tr id="portfolio-row' + data.hca_portfolio_id + '" data-status="' + status + '" ' +
                    'style="' + custom_style + '" class="' + custom_class + '">')
                    .append($('<td class="text-center">')
                        .append($('<button class = "btn btn-default btn-sm"' +
                            ' data-toggle = "modal" data-type="buyer"' +
                            ' data-target = "#move-deal-dialog" data-backdrop = "static"' +
                            ' data-id="' + data.deal_id + '" ' +
                            ' data-current-status="' + data.ds_status + '">' +
                            ' <span class = "glyphicon glyphicon-sort"></span></button>')))
                    .append($('<td class="text-center">')
                        .append($('<input type="text" ' +
                            ' data-id="' + data.deal_id + '" readonly value="' + (text_status).toUpperCase() + '"' +
                            ' style=" background-color: #fff"' +
                            ' class="form-control" readonly> ')))
                    .append($('<td class="text-center">')
                        .append($('<input type="text" class="form-control" data-id="' + data.deal_id + '"' +
                            ' value="' + data.hb_name + '">')))
                    .append($('<td class="text-center">')
                        .append($('<input type="text"' +
                            ' data-id="' + data.deal_id + '" value="' + data.hba_entity_name + '" ' +
                            ' class="form-control"> ')))
                    .append($('<td class="text-center">')
                        .append($('<input type="text" value="' + data.hba_entity_address + '" ' +
                            ' name="client' + data.deal_id + '" class="form-control"' +
                            ' data-id="' + data.hc_id + '" data-name="hba_entity_address" ' +
                            ' data-current-status="' + data.ds_status + '">')))

                    .append($('<td class="text-center">')
                        .append($('<input value="' + data.hba_city + '"' +
                            ' name="city' + data.deal_id + '" class="form-control buyer-info"' +
                            ' data-id="' + data.hba_id + '" data-name="hba_city" ' +
                            ' data-current-status="' + data.ds_status + '">')))

                    // State value box
                    .append($('<td class="text-center">')
                        .append($('<input type="text" class="form-control buyer-info" value="' + data.hba_state + '"' +
                            ' name="state' + data.deal_id + '" data-name="hba_state">')))

                    // ZIP value.
                    .append($('<td class="text-center">')
                        .append($('<input type="text" class="form-control buyer-info" value="' + data.hba_zip + '"' +
                            ' name="state' + data.deal_id + '" data-name="hba_zip">')))

                    // RWT drop down
                    .append($('<td class="text-center">')
                        .append($('<select' +
                            ' name="rwt' + data.deal_id + '" class="form-control buyer-info"' +
                            ' data-id="' + data.hb_id + '" data-name="hb_rwt" ' +
                            ' data-current-status="' + data.ds_status + '">' +
                            ' <option value="R" selected>R</option>' +
                            ' <option value="W">W</option>' +
                            ' <option value="T">T</option>' +
                            ' </select>')))

                    // Product Type.
                    .append($('<td class="text-center">')
                        .append($('<input type="text" class="form-control buyer-info" value="' + p_type + '"' +
                            ' name="state' + data.deal_id + '" data-name="hba_zip">')))

                    // Info popup.
                    .append($('<td class="text-center">')
                        .append($('<button class="btn btn-default btn-sm has-popover info_button"' +
                            ' data-toggle="popover" title="Buyer Information. <button type=\'button\' id=\'close\' ' +
                            'class=\'close\'>&times;</button>" data-container="body"' +
                            ' id="buyer_info_button' + data.hba_id + '"' +
                            ' data-id="' + data.deal_id + '" data-html="true" data-placement="top"' +
                            ' data-dummy-content="' + dummy_content.html().replace(/"/g, "\'") + '"' +
                            ' data-content="' + getBuyerInfoContent(data) + '">' +
                            '<span class="glyphicon glyphicon-info-sign"></span></button>')))

                    // Buyer Type
                    // .append($('<td class="text-center">')
                    //     .append($('<input type="text" class="form-control buyer-info" ' +
                    //         ' value="' + data.hba_buyer_type + '"' +
                    //         ' name="hba_buyer_type' + data.deal_id + '" data-name="hba_buyer_type">')))

                    // Dial Button.
                    .append($('<td class="text-center">')
                        .append($('<button class="btn btn-default btn-sm" data-toggle="modal" ' +
                            'data-target="#view-ccm-dialog" data-id="' + data.hba_id + '">' +
                            '<span class="glyphicon glyphicon-phone-alt"></span></button>')))

                    // Asset Name.
                    .append($('<td class="text-center">')
                        .append($(getDealLayout(individual_deals, data))))

                    // Asset CLU.
                    .append($('<td class="text-center">')
                        .append($('<select class="form-control buyer-info" ' +
                            ' name="hba_clu' + data.deal_id + '" data-name="hba_buyer_type">' +
                            '<option value="C">C</option>' +
                            '<option value="L">L</option>' +
                            '<option value="U">U</option>' +
                            '</select>')))

                    .append($('<td class="text-center">')
                        .append($('<span class="has-tooltip" title="Next Action Manager">' +
                            '<button class="btn btn-default btn-sm has-popover"' +
                            ' data-content="' + getNotesContent(data) + '"' +
                            ' id="notes" data-id="' + data.deal_id + '" data-container="body" data-html="true"' +
                            ' data-toggle="popover" data-placement="top" data-title="Next Action Manager">' +
                            '<span class="glyphicon glyphicon-list"></span></button></span>')))

                    .append($('<td class="text-center">')
                        .append($('<button class="btn btn-default btn-sm has-popover component_button"' +
                            ' data-toggle="popover" title="Build Components." data-container="body"' +
                            ' data-id="' + data.deal_id + '" data-html="true" data-placement="top"' +
                            ' data-content="' + getBuildComponentLayout(data) + '">' +
                            '<span class="glyphicon glyphicon-send"></span></button>')))

                    .append($('<td class="text-center">')
                        .append($('<button class="btn btn-default btn-sm has-popover component_button" ' +
                            'data-toggle="popover" title="Sent Components." data-container="body"' +
                            ' data-id="' + data.deal_id + '" data-html="true" data-placement="top"' +
                            ' data-content="' + getSentComponents(data) + '"' +
                            '><span class="glyphicon glyphicon-envelope"></span></button>')))

                    .append($('<td class="text-center">')
                        .append($('<button class="btn btn-default btn-sm coming-soon-tool" data-toggle="modal" ' +
                            'data-target="#view-alert-dialog" data-type="asset" data-id="' + data.hc_id + '"> ' +
                            '<span class="glyphicon glyphicon-book"></span></button>')))
                );
            $('#phone' + data.hc_id).val(data.hc_selected_phone);

            // Declare Variables and set default to 0.
            let total_doll = 0,
                total_gross = 0,
                total_price = 0;
            usePopover();

            total_doll += ((parseInt(data.deal_price) * parseInt(data.deal_rate) / 100) * (data.deal_gsplit / 100)
                * (parseInt(data.deal_split)) / 100);

            total_price += parseInt(data.deal_price);

            total_gross += (parseInt(data.deal_price) * parseInt(data.deal_rate) / 100);

            // Updating Total Dollars.
            $('#cpd-deal-' + status + '-total-doll').val(
                '$' + (
                parseInt($('#cpd-deal-' + status + '-total-doll').val()
                    .replace('$', '').replace(',', '')) + total_doll).toLocaleString()
            );

            // Updating Total Gross.
            $('#cpd-deal-' + status + '-total-gross').val(
                '$' + (
                parseInt($('#cpd-deal-' + status + '-total-gross').val()
                    .replace('$', '').replace(',', '')) + total_gross).toLocaleString());

            // Update Total Value.
            $('#cpd-deal-' + status + '-total-value').val(
                '$' + (
                parseInt($('#cpd-deal-' + status + '-total-value').val()
                    .replace('$', '').replace(',', '')) + total_price).toLocaleString()
            );
            disableComponent();

        });
        const deal_table_tr = $('#cpd-deal-' + status + '-table tbody tr');

        // Updating the total number of deals.
        $('#cpd-deal-' + status + '-total-deal').val(
            (0 !== deal_table_tr.length) ?
                deal_table_tr.length : 0
        );


        // Update Quarter Actual Value if deal has a status of complete.
        if ('complete' === status) {
            let qtr_total = 0;
            $.each(deal, function (index, data) {
                if ((quarter[current_quarter][0] <= data.ds_md) && (quarter[current_quarter][1] >= data.ds_md)) {
                    qtr_total += ((parseInt(data.deal_price) * parseInt(data.deal_rate) / 100) *
                        (data.deal_gsplit / 100) * (parseInt(data.deal_split)) / 100);
                }
            });

            qtr_total = numeral($('#qtr_actual').val())._value + qtr_total;
            $('#qtr_actual').val(numeral(qtr_total).format(('$0,0')));
        }
    });

    $('.select-picker').selectpicker();

    // Asset preference click handler.
    $(document).off('change', '.asset-preference');
    $(document).on('change', '.asset-preference', function () {
        const update_preference = makeAjaxCall('cpdv1/update_preference', {
            'deal_id': $(this).val(),
            'cpd_id': cpd_id
        });

        update_preference.then(function (data) {
            console.log(data);
        })
    });

    // Add a dummy row to each table if no row is present.
    $.each(deal_status, function (status, deal) {
        // Destroy any previous instance of data table.
        if ($('#cpd-deal-' + deal['code_abbrev'] + '-table > tbody tr').length === 0) {
            let sample_tr = $('.sample-tr').clone(),
                key = deal['code_abbrev'];

            // Check if this request is for buyer row.
            if (('b_active' === key) || ('tour' === key) || ('offer' === key)
                || ('diligence' === key) || ('b_contract' === key)) {
                sample_tr = $('.sample-buyer-tr').clone();

                $('#cpd-deal-' + deal['code_abbrev'] + '-table > tbody')
                    .append('<tr class="cpd_deal">' + sample_tr.html() + '</tr>');
            } else {

                try {
                    $('#cpd-deal-' + deal['code_abbrev'] + '-table').DataTable().destroy();
                } catch (er) {
                }

                $('#cpd-deal-' + deal['code_abbrev'] + '-table > tbody')
                    .append('<tr class="cpd_deal">' + sample_tr.html() + '</tr>');

                $('#cpd-deal-' + deal['code_abbrev'] + '-table').DataTable({
                    fixedHeader: {
                        header: true
                    },
                    responsive: true,
                    "aoColumnDefs": [
                        {"sWidth": "3vw", "aTargets": [0]},
                        {"sWidth": "5vw", "aTargets": [1]},
                        {"sWidth": "6vw", "aTargets": [2]},
                        {"sWidth": "11vw", "aTargets": [3]},
                        {"sWidth": "12vw", "aTargets": [4]},
                        {"sWidth": "4vw", "aTargets": [5]},
                        {"sWidth": "11vw", "aTargets": [6]},
                        {"sWidth": "3vw", "aTargets": [7]},
                        {"sWidth": "3vw", "aTargets": [8]},
                        {"sWidth": "15vw", "aTargets": [9]},
                        {"sWidth": "4vw", "aTargets": [10]},
                        {"sWidth": "12vw", "aTargets": [11]},
                        {"sWidth": "3vw", "aTargets": [12]},
                        {"sWidth": "3vw", "aTargets": [13]},
                        {"sWidth": "3vw", "aTargets": [14]},
                    ],
                    // "offsetTop": 12,
                    "paging": false,
                    "searching": false,
                    "info": false,
                    "bAutoWidth": false,
                    "ordering": false,
                    "order": [],
                    "columnDefs": [
                        {
                            "targets": [0, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
                            "orderable": false
                        }]
                })
            }
        }

        // Append row to maintain DMD tables width.
        let sample_tr = $('.sample-tr').clone(),
            key = deal['code_abbrev'];

        // Check if this request is for buyer row.
        // if (('b_active' === key) || ('tour' === key) || ('offer' === key)
        //     || ('diligence' === key) || ('b_contract' === key)) {
        //     sample_tr = $('.sample-buyer-tr').clone();
        // }
        // $('#cpd-deal-' + deal['code_abbrev'] + '-table > tbody .maintainer').remove();
        // $('#cpd-deal-' + deal['code_abbrev'] + '-table > tbody')
        //     .append('<tr class="maintainer" style="visibility: hidden">' + sample_tr.html() + '</tr>');

        $('.dummy_income').attr('data-dummy-content', dummy_content.html().replace(/"/g, "'"));
        $('.dummy_income').attr('data-id', 0);

        // Setup layout for promo sent popover.
        $('.dummy_promo').attr('data-content', '<div class=\'row col-xs-12\'>\n' +
            '<div class=\'col-xs-4\'>' +
            '<table style=\'width:100%\' class=\'table table-striped\'>\n' +
            '<tr>\n' +
            '<thead>\n' +
            '<th>MCO</th>\n' +
            '<th>SAP</th>\n' +
            '<th>TPC</th>\n' +
            '<th>DUR</th>\n' +
            '<th>DAL</th>\n' +
            '<th>GMT</th>\n' +
            '</thead>\n' +
            '</tr>\n' +
            '<tbody>\n' +
            '<tr>\n' +
            $.map(universal_components.components, function (component) {
                if ((component.uc_type === 'promo') && (component.uc_tool === 'cpdv1')) {
                    return ('<td class=\'text-center\'>' +
                        ' <a class=\'btn btn-default btn-sm dummy-component\' ' +
                        ' href=\'#\'>' +
                        '<span class=\'glyphicon glyphicon-list-alt\'></span></a>' +
                        '</td>');
                }
            }).join('') +
            '</tr>\n' +
            '</tbody>\n' +
            '</table>\n' +
            '</div>\n' +
            '</div>');

        // Setup layout for dummy sent popover.
        $('.dummy_sent').attr('data-content', '<div class=\'row col-xs-12\'>\n' +
            '<div class=\'col-xs-4\'>' +
            '<table style=\'width:100%\' class=\'table table-striped\'>\n' +
            '<tr>\n' +
            '<thead>\n' +
            '<th>MCO</th>\n' +
            '<th>SAP</th>\n' +
            '<th>TEM</th>\n' +
            '<th>DAR</th>\n' +
            '<th>WEB</th>\n' +
            '</thead>\n' +
            '</tr>\n' +
            '<tbody>\n' +
            '<tr>\n' +
            $.map(universal_components.components, function (component) {
                if ((component.uc_type === 'promo') && (component.uc_tool === 'cpdv1')) {
                    return ('<td class=\'text-center\'>' +
                        ' <a class=\'btn btn-default btn-sm dummy-component\' ' +
                        ' href=\'#\'>' +
                        '<span class=\'glyphicon glyphicon-list-alt\'></span></a>' +
                        '</td>');
                }
            }).join('') +
            '</tr>\n' +
            '</tbody>\n' +
            '</table>\n' +
            '</div>\n' +
            '</div>');
    });

// Event listener for select boxes.
    $(document).on('change', '.cpd_lead, .cpd_source, .cpd_action, .cpd_whom', function () {

        // If the change is to "new agent", show add agent dialog.
        if ($(this).val() === "0") {
            $('#' + $(this).attr('data-target')).modal('show');
            $('#lead-type').val($(this).data('type'));
            $(this).val('-1');
        }
    });

// Check if Deal columns are changed.
    $(document).on('change', '.cpd-info', function () {
        if ($(this).val() !== "0" && $(this).val() !== null) {
            const that = $(this);

            // Check if the asset is name is to be updated.
            if (that.data('name') === 'hca_name') {
                makeAjaxCall('homebase/update_asset_field', {
                    'id': that.data('id'),
                    'col_name': 'hca_name',
                    'val': that.val(),
                    'long': '',
                    'lat': ''
                })
            }
            // Check if client phone or name is to be updated.
            else if (that.data('name') === 'hc_selected_phone' || that.data('name') === 'hc_name') {
                makeAjaxCall('homebase/update_homebase_field', {
                    'id': that.data('id'),
                    'col': that.data('name'),
                    'val': that.val()
                });
            } else if ($(that).hasClass('hca-info')) {
                makeAjaxCall('homebase/update_asset_field', {
                    'id': that.data('id'),
                    'col_name': that.data('name'),
                    'val': that.val()
                });
            }
            else {
                updateDeal(that.data('id'), that.data('name'), that.val());
            }
        }
    });

    usePopover();
    setEditable('bottom');

    TabDoubleScroll();

    // Get the Floor Target Game values.
    const get_ftg = makeAjaxCall('cpdv1/get_ftg',
        {
            'year': year,
            'quarter': current_quarter,
            'cpd_id': cpd_id
        });

    get_ftg.then(function (data) {
        updateFTG(data.data[0]);
    });

    // Selector For Collapse button.
    const collapse_icon_selector = $('.collapse-icon');

    // Remove on click event form collapse button.
    collapse_icon_selector.off('click');

    // Add on click event to collapse button.
    collapse_icon_selector.click(function (e) {
        if (e.isTrigger === undefined) {
            let obj = {};
            const obj_len = localStorage.key_count;
            let count = 0;

            // Check if the value for tab is already present in local storage object.
            function hasValue(object, value) {
                let key_to_return = false;
                $.each(object, function (index, key) {
                    if (key === value) {
                        key_to_return = index;
                    }
                });
                return key_to_return;
            }

            // Parse the active tabs and assign to an object.
            if (undefined !== obj_len) {
                obj = JSON.parse(localStorage.active_tabs);
                const key = hasValue(obj, $(this).attr('id'));

                // Check If key is already present in Storage. If yes remove it.
                if (key) {
                    delete obj[key];
                    count = parseInt(obj_len) - 1;
                } else {
                    obj[obj_len] = $(this).attr('id');
                    count = parseInt(obj_len) + 1;
                }
            } else {
                obj[0] = $(this).attr('id');
                count = 1;
            }

            // Put the active tabs JSON to session storage.
            const data = JSON.stringify(obj);
            localStorage.setItem('active_tabs', data);
            localStorage.setItem('key_count', count);
        } else if ('undefined' === typeof localStorage.active_tabs) {
            let ids = $.map($('.collapse-icon'), function (n) {
                return n.id;
            });

            // Put the active tabs JSON to session storage.
            const count = ids.length,
                data = JSON.stringify(ids);
            localStorage.setItem('active_tabs', data);
            localStorage.setItem('key_count', count);
        }
        $(this).parents('table').find('tbody').find('tr.cpd_deal').toggle();
        $(this).toggleClass('fa-plus-circle', 'fa-minus-circle');
        $(this).toggleClass('fa-minus-circle', 'fa-plus-circle');
    });

    // Get all the active tabs.
    if ('undefined' !== typeof localStorage.active_tabs) {
        const obj = JSON.parse(localStorage.active_tabs);
        // Trigger click event on all the active tabs.
        $.each(obj, function (index, key) {
            $('#' + key).trigger('click');
        });
    } else {
        $('.collapse-icon').trigger('click');
    }


    $('.coming_soon_button').click(function () {
        $(this).popover({content: $(this).attr('data-dummy-content')});
        // $(this).popover('show');
    });

    // Set Income Popover content.
    $(document).on('click', '.buy-side-income-button', function () {
        $(this).popover({content: $('#buyer_info_button' + $(this).data('id')).attr('data-dummy-content')});
        $(this).popover('show');
        const deal_id = $(this).attr('data-id'),
            gnet_selector = $('#b_gnet' + deal_id);

        // Calculate and set Gross Net and Net income.
        gnet_selector.val(numeral((numeral($('#b_gross' + deal_id).val())._value)
            * (numeral($('#b_gsplit' + deal_id).val())._value)).format('$0,0'));

        $('#b_net' + deal_id).val('$' + (numeral(gnet_selector.val())._value
            * parseInt($('#b_split' + deal_id).val()) / 100).toLocaleString());
        updateBuySideAmount();
    });

    // Set Income Popover content.
    $(document).on('click', '.amount_button', function () {
        $(this).popover({content: $('#info_button' + $(this).data('id')).attr('data-dummy-content')});
        $(this).popover('show');
        const deal_id = $(this).attr('data-id'),
            gnet_selector = $('#gnet' + deal_id);

        // Calculate and set Gross Net and Net income.
        gnet_selector.val(numeral((numeral($('#gross' + deal_id).val())._value)
            * (numeral($('#gsplit' + deal_id).val())._value)).format('$0,0'));

        $('#net' + deal_id).val('$' + (numeral(gnet_selector.val())._value
            * parseInt($('#split' + deal_id).val()) / 100).toLocaleString());
        updateAmount();
    });

    // Set Value Popover content.
    $(document).on('click', '.value-button', function () {
        $(this).popover({content: $('#info_button' + $(this).data('id')).attr('data-value-content')});
        $(this).popover('show');
        updateValue();
    });

    // Set Value Popover content.
    $(document).on('click', '.revenue-button', function () {
        const asset_id = $(this).attr('data-id');
        $(this).popover({content: $('#info_button' + $(this).data('id')).attr('data-revenue-content')});
        $(this).popover('show');
        const get_revenue_data = makeAjaxCall('cpdv1/get_revenue_data', {
            'id': asset_id
        });
        let total_monthly_income = 0,
            total_units = 0,
            total_sq_ft = 0;

        get_revenue_data.then(function (data) {
            $.each(data.revenue_data, function (index, revenue) {
                total_monthly_income += parseInt(revenue.har_rent_per_mo);
                total_units += 1;
                total_sq_ft += parseInt(revenue.har_sq_ft);

                $.each(revenue, function (key, value) {
                    $('.revenue_detail[data-name="' + key + '"][data-type="' + revenue.har_type + '"]' +
                        '[data-id="' + asset_id + '"]')
                        .val((-1 === key.indexOf('per_mo')) ? value : numeral(value).format('$0,0'));
                });
            });

            // Set the total input fields.
            $('#total-monthly-gross-income' + asset_id).val(numeral(total_monthly_income).format('$0,0'));
            $('#total-number-units' + asset_id).val(total_units);
            $('#total-livable-sf' + asset_id).val(total_sq_ft);
            $('#total-yearly-gross-income' + asset_id).val(numeral(total_monthly_income * 12).format('$0,0'));
        });

        updateRevenue();
    });

    // Setup MCO popover content.
    $(document).on('click', '#mco', function (e) {
        let hca_id = $(this).attr('data-hca');

        e.preventDefault();
        e.stopPropagation();

        setupComponentPopover('mco', 'Marketing Campaign Overlay.', '<th>Promos</th><th>Messages</th>' +
            '<th style="white-space: nowrap">Call Scripts</th>\n' +
            '<th>Confirms<s/th>\n' +
            '<th>Stories</th>\n', '(component.uc_type === \'promo\') && (component.uc_tool === \'tsl\')',
            'tsl', hca_id);

        $(this).popover('show');
    });

    // Setup SAP popover Content.
    $(document).on('click', '#sap', function (e) {
        let hca_id = $(this).attr('data-hca');
        e.preventDefault();
        e.stopPropagation();

        setupComponentPopover('sap', 'Strategic Advisory Program.', '<th>Profiles</th>\n' +
            '<th>Worksheets</th>\n' +
            '<th>Analysis</th>\n' +
            '<th>Updates</th>\n' +
            '<th>Notices</th>\n', '(component.uc_type !== \'promo\') && (component.uc_tool === \'tsl\')',
            'tsl', hca_id);

        $(this).popover('show');
    });

    // Setup TEM popover content.
    $(document).on('click', '#tem', function (e) {
        let deal_id = $(this).attr('data-id');
        e.preventDefault();
        e.stopPropagation();

        setupComponentPopover('tem', ' Transaction Process Coordinator.', '<th>Survey</th>' +
            '<th>Emails</th>' +
            '<th>Events</th>' +
            '<th>Reviews</th>' +
            '<th>Specs</th>', '(component.uc_type === \'promo\') && (component.uc_tool === \'cpdv1\')',
            'cpdv1', deal_id);

        $(this).popover('show');
    });

    // Setup DOC popover content.
    $(document).on('click', '#doc', function (e) {
        let deal_id = $(this).attr('data-id');
        e.preventDefault();
        e.stopPropagation();

        setupComponentPopover('doc', 'Document Upload Room..',
            '<th>Info</th>\n' +
            '<th>Reports</th>\n' +
            '<th>Proposal</th>\n' +
            '<th>Agreement</th>\n' +
            '<th>Contracts</th>\n', '(component.uc_type !== \'promo\') && (component.uc_tool === \'cpdv1\')\n' +
            ' && (component.uc_type !== \'mgt\')',
            'cpdv1', deal_id);

        $(this).popover('show');
    });

    // Setup DAL popover Content.
    $(document).on('click', '#dal', function (e) {
        let hca_id = $(this).attr('data-hca');
        e.preventDefault();
        e.stopPropagation();

        setupComponentPopover('dal', 'Document Assembly Library.',
            '<th>Templates</th>\n' +
            '<th>Images</th>\n' +
            '<th>Text</th>\n' +
            '<th>Chart</th>\n' +
            '<th>Art</th>\n', 'component.uc_type === \'mgt\'',
            'cpdv1', hca_id);

        $(this).popover('show');
    });

    // Setup DAL popover Content.
    $(document).on('click', '#gmt', function (e) {
        let hca_id = $(this).attr('data-hca');
        e.preventDefault();
        e.stopPropagation();

        setupComponentPopover('gmt', 'Game Management Tools.',
            '<th>SGD</th>\n' +
            '<th>DTN</th>\n' +
            '<th>DST</th>\n' +
            '<th>DCB</th>\n' +
            '<th>ACM</th>\n', 'component.uc_type === \'mgt\'',
            'cpdv1', hca_id);

        $(this).popover('show');
    });

    // Setup DST popover content.
    $(document).on('click', '#DST', function (e) {
        let coming_soon_dst = $('#coming_soon_dst').clone();
        coming_soon_dst.removeClass('coming_soon_dst');
        coming_soon_dst.removeClass('hidden');

        e.preventDefault();
        e.stopPropagation();

        $('#DST')
            .popover({
                selector: '[rel=popover]',
                // trigger: 'hover',
                container: 'body',
                placement: 'top',
                html: true,
                title: 'Coming Soon!! ' +
                '<span id="close" class="close" onclick="$(this).popover(&quot;hide&quot;);">×</span>',
                content: function () {
                    return coming_soon_dst.html().replace(/"/g, "\'");
                }
            });
        $(this).popover('show');
    });

    // Setup DCB coming soon popover content.
    $(document).on('click', '#dcb-coming-soon', function (e) {
        let coming_soon_dst = $('#coming_soon_content').clone();
        coming_soon_dst.removeClass('coming_soon_dst');
        coming_soon_dst.removeClass('hidden');

        e.preventDefault();
        e.stopPropagation();

        $('#dcb-coming-soon')
            .popover({
                selector: '[rel=popover]',
                // trigger: 'hover',
                container: 'body',
                placement: 'left',
                html: true,
                title: 'Coming Soon!! ' +
                '<span id="close" class="close" onclick="$(this).popover(&quot;hide&quot;);">×</span>',
                content: function () {
                    return coming_soon_dst.html().replace(/"/g, "\'");
                }
            });
        $(this).popover('show');
    });

    // Setup Team popover content.
    $(document).on('click', '#team', function (e) {
        const leads = leads_info,
            hca_id = $(this).attr('data-hca'),
            deal_lead = $(this).attr('data-lead'),
            deal_gen = $(this).attr('data-gen');

        e.preventDefault();
        e.stopPropagation();

        $('#team')
            .popover({
                selector: '[rel=popover]',
                container: 'body',
                placement: 'top',
                html: true,
                title: 'Team Information ',
                content: function () {
                    return '<table class="table">' +
                        '<tbody>' +
                        '<tr>' +
                        '<td>Lead Agent ' +
                        '<span class="glyphicon glyphicon-info-sign" data-toggle="modal"' +
                        ' data-target="#view-agent-profile-dialog"></span>' +
                        '<input style="float: right; width: 2vw" type="text" id="split-percent"/>' +
                        '<span class="team-split-span">Split </span>' +
                        '</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td>' + getLeadDropDown(leads, hca_id, deal_lead) + '</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td>Source Agent ' +
                        '<span class="glyphicon glyphicon-info-sign" data-toggle="modal"' +
                        ' data-target="#view-agent-profile-dialog"></span>' +
                        '<input type="text" id="split-percent" style="float: right; width: 2vw"/>' +
                        '<span class="team-split-span">Split </span>' +
                        '</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td>' + getSourceLayout(leads, hca_id, deal_gen) + '</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td>Associate Agent ' +
                        '<span class="glyphicon glyphicon-info-sign"' +
                        ' data-toggle="modal" data-target="#view-agent-profile-dialog"></span>' +
                        '<input type="text" id="split-percent" style="float: right; width: 2vw"/>' +
                        '<span class="team-split-span">Split </span>' +
                        '</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<td>' + getSourceLayout(leads, hca_id, deal_gen) + '</td>' +
                        '</tr>' +
                        '</tbody>' +
                        '</table>';
                }

            });
        $(this).popover('show');
    });

    // Setup DCB popover content.
    $(document).on('click', '#DCB', function (e) {
        let hca_id = $(this).attr('data-hca'),
            coming_soon_content = $('#coming_soon_content').clone();

        coming_soon_content.removeClass('coming_soon_content');
        coming_soon_content.removeClass('hidden');

        e.preventDefault();
        e.stopPropagation();

        const quarter_array = ['Q1 ' + parseInt(moment().year()),
                'Q2 ' + parseInt(moment().year()),
                'Q3 ' + parseInt(moment().year()),
                'Q4 ' + parseInt(moment().year()),
                'Q1 ' + parseInt(moment().year() + 1),
                'Q2 ' + parseInt(moment().year() + 1),
                'Q3 ' + parseInt(moment().year() + 1),
                'Q4 ' + parseInt(moment().year() + 1),
            ],
            probability_array = [
                '50% Potential',
                '60% Possible',
                '70% Prospective',
                '80% Probable',
                '90% Predictable'
            ];

        // Set up Deal conversion board popover.
        $('#DCB')
            .popover({
                selector: '[rel=popover]',
                trigger: 'hover',
                container: 'body',
                placement: 'top',
                html: true,
                title: 'Deal Conversion Board' +
                ' <span class="glyphicon glyphicon-info-sign" id="dcb-coming-soon"></span>' +
                ' <span id=\'close\' class=\'close\' ' +
                'onclick=\'$(this).popover(&quot;hide&quot;);\'>&times;</span>',
                content: function () {
                    return '<div><div><label style="width: 7vw">DCB Quarter  </label>' +
                        '<select class="form-control" style="display: inline;" id="dcb-quarter" ' +
                        'data-hca="' + hca_id + '">' +
                        $.map(quarter_array, function (quarter, index) {
                            return '<option value="' + index + '">' + quarter + '</option>'
                        }).join('') +
                        '</select></div>' +
                        '<div style="margin-top: 20px"><label style="width: 7vw">Select Probability  </label>' +
                        '<select class="form-control" style="display: inline;" id="dcb-probability" ' +
                        'data-hca="' + hca_id + '">' +
                        $.map(probability_array, function (probability, index) {
                            return '<option value="' + index + '">' + probability + '</option>'
                        }).join('') +
                        '</select></div>' +
                        '</div>';
                }
            });

        $(this).popover('show');

        // DCB popover on show event.
        $('#DCB').on('shown.bs.popover', function () {

            // Fetch the DCB data for asset.
            const fetch_dcb = makeAjaxCall('cpdv1/fetch_dcb', {
                'id': hca_id
            });

            fetch_dcb.then(function (data) {
                // Create an array with quarter as key and probability as value.
                let dcb_qtr_prob = [0, 0, 0, 0, 0, 0, 0, 0];
                $.each(data.dcb, function (index, dcb) {
                    dcb_qtr_prob[dcb.cd_qtr] = dcb.cd_prob;
                });

                $('#dcb-probability').val(dcb_qtr_prob[0]);

                // Check if the quarter for DCB as changed.
                $(document).off('change', '#dcb-quarter');
                $(document).on('change', '#dcb-quarter', function () {
                    $('#dcb-probability').val(dcb_qtr_prob[$(this).val()]);
                })
            })
        })
    });

    //Check if DCB info is changed.
    $(document).off('change', '#dcb-probability');
    $(document).on('change', '#dcb-probability', function () {
        makeAjaxCall('cpdv1/update_dcb', {
            'id': $(this).attr('data-hca'),
            'qtr': $('#dcb-quarter').val(),
            'probability': $('#dcb-probability').val()
        });
    })
}


// // Set the content for component popover.
// function setupComponentPopover(target, title, table_head, condition, tool, hca_id) {
//
//     let arr = [];
//     arr.DST = 'Deal Status Tracker';
//     arr.DCB = 'Deal Conversion Board';
//     arr.SGD = 'Strategic Game Designer';
//     arr.QVC = 'Quick View Components';
//     arr.MPA = 'Model, Plan and Actual';
//     arr.Pages = 'Promo Builder';
//     arr.Vmail = 'Voice Mail Builder';
//     arr.Script = 'Initial Contact Script Builder';
//     arr.Email = 'Email Builder';
//     arr.Stories = 'Stories Builder';
//     arr.Profile = 'Client Profile Builder';
//     arr.Worksheet = 'Worksheet Builder';
//     arr.SAP = 'Strategic Advisory Program';
//     arr.Updates = 'Updates Builder';
//     arr.Notices = 'Notices Builder';
//     arr.Survey = 'Survey Builder';
//     arr.Email = 'Email Builder';
//     arr.Event = 'Event Manager';
//     arr.Review = 'Review Manager';
//     arr.Followup = 'Notices Builder';
//     arr.Assembly = 'Specification Documents';
//     arr.Media = 'Media Documents';
//     arr.Proposal = 'Proposal Documents';
//     arr.Agreement = 'Agreement Documents';
//     arr['Web Presentation'] = 'Web Presentation';
//
//     $('#' + target)
//         .popover({
//             selector: '[rel=popover]',
//             trigger: 'hover',
//             container: 'body',
//             placement: 'top',
//             html: true,
//             title: title,
//             content: function () {
//                 return '<div class=\'row col-xs-12\'>\n' +
//                     '<table style=\'width:100%\' class=\'table table-striped\'>\n' +
//                     '<tr>\n' +
//                     '<thead>\n' + table_head +
//                     '</thead>\n' +
//                     '</tr>\n' +
//                     '<tbody>\n' +
//                     '<tr>\n' +
//                     $.map(universal_components.components, function (component) {
//
//                         if (Function('return (function(component, condition){ return (eval(  condition ))})')()(
//                                 component, condition)) {
//
//                             return ('<td class=\'text-center\'>' +
//                                 '<span class=\'has-tooltip\' data-title=\'' + arr[component.uc_name] + '\'>' +
//                                 '<a class=\'btn btn-default btn-sm coming-soon-tool\' ' +
//                                 'href=' + jsglobals.base_url + tool + component.uc_controller
//                                 + hca_id + '?id=' + cpd_id + ' id=' + component.uc_name + '>' +
//                                 '<span class=\'glyphicon glyphicon-' + component.uc_icon + '\'></span></a></span>' +
//                                 '</td>');
//                         }
//                     }).join('') +
//                     '</tr>\n' +
//                     '</tbody>\n' +
//                     '</table>\n' +
//                     '</div>\n' +
//                     '</div>';
//             }
//         });
//
// }


/**
 * Check if deal status is complete, if so change Info button to ref button.
 *
 * @param row_data String Row HTML.
 * @param data     Object Data for button.
 *
 */
function getRowData(row_data, data) {
    // Check if deal status is complete, if so change Info button to ref button.
    if ('complete' === data.ds_status) {
        row_data = '<button class = "btn btn-default btn-sm has-popover buyer cpd-info"' +
            ' data-toggle="modal"' +
            ' data-target="#client-info-dialog"' +
            ' data-html="true" data-type="ref"' +
            ' data-id="' + data.deal_id + '" ' +
            ' data-current-status="' + data.ds_status + '">' +
            ' <span class = "glyphicon glyphicon-plus"></span></button>';
    } else {
        row_data = '<select' +
            ' name="clu' + data.deal_id + '" class="form-control cpd-info hca-info"' +
            ' data-id="' + data.hca_id + '" data-name="hca_clu" ' +
            ' data-current-status="' + data.ds_status + '">' +
            ' <option value="C">C</option>' +
            ' <option value="L">L</option>' +
            ' <option value="U" selected>U</option>' +
            ' </select>';
    }
    return row_data;
}


/**
 * Returns source popup layout.
 *
 * @param leads    Object Array with Lead object.
 * @param data     Object Data for client.
 *
 */
function getSourceLayout(leads, hca_id, deal_gen) {
    return ('<select' +
        ' name="source' + hca_id + '" class="form-control cpd-info cpd_source hca-info"' +
        ' data-id="' + hca_id + '" data-name="hca_gen" data-type="source"' +
        ' data-target="view-source-dialog">' +
        '<option disabled selected value="-1"> --Source-- </option><option value="0">Add</option>' +
        $.map(leads, function (lead) {
            if ('source' === lead.dl_type)
                return ('<option value="' + lead.dl_id + '" ' + (deal_gen === lead.dl_id
                    ? 'selected' : '') + '>' + lead.dl_name + '</option>');
        })
        + '</select>');
}

/**
 * Returns Buyer input group layout.
 *
 * @param buyer_array Object Buyer Array.
 * @param data        Object Data for button.
 *
 */
function getBuyerLayout(buyer_array, data) {
    return ('<div class="input-group">' +
        '<select name="buyer' + data.deal_id + '" class="form-control input-group-addon"' +
        ' style="min-width: 5vw; background-color: #fff !important;"' +
        ' id="buyer-drop' + data.deal_id + '">' +
        '<option disabled selected value="-1"> --Buyer-- </option>' +
        $.map(buyer_array, function (buyer) {
            let buyer_info = buyer.split('|'),
                buyer_name = buyer_info[0],
                buyer_id = buyer_info[1];

            return '<option value="' + buyer_id + '">' + buyer_name + '</option>'
        }).join('') +
        '</select><span class="input-group-btn">' +
        '<button class="btn btn-default btn-sm" data-id="' + data.deal_id + '"' +
        ' data-type="info" data-target="#client-info-dialog" data-toggle="modal"' +
        ' data-buyer="true">' +
        '<span class="glyphicon glyphicon-list"></span></button></span></div>');
}

/**
 * Returns Deal  layout.
 *
 * @param buyer_array Object Buyer Array.
 * @param data        Object Data for button.
 *
 */
function getDealLayout(deal_array, data) {
    return (
        // '<div class="input-group">' +
        '<select name="buyer' + data.deal_id + '" class="form-control asset-preference select-picker"' +
        ' style="min-width: 5vw; background-color: #fff !important;" multiple' +
        ' id="buyer-drop' + data.deal_id + '">' +
        '<optgroup label="Offers">' +
        $.map(deal_array.deal_pros, function (deal) {
            return '<option value="' + deal.deal_id + '">' + deal.deal_property + '</option>'
        }).join('') +
        ' </optgroup><optgroup label="Contract">' +
        $.map(deal_array.predictable, function (deal) {
            return '<option value="' + deal.deal_id + '">' + deal.deal_property + '</option>'
        }).join('') +
        '</optgroup><optgroup label="Complete">' +
        $.map(deal_array.complete, function (deal) {
            return '<option value="' + deal.deal_id + '">' + deal.deal_property + '</option>'
        }).join('') +
        '</optgroup>' +
        '</select>'
        // '<span class="input-group-btn">' +
        // '<button class="btn btn-default btn-sm" data-id="' + data.deal_id + '"' +
        // ' data-type="info" data-target="#client-info-dialog" data-toggle="modal"' +
        // ' data-buyer="true">' +
        // '<span class="glyphicon glyphicon-list"></span></button></span></div>'
    );
}

/**
 * Returns Sent popover layoutl
 *
 * @param data Object Data for client.
 *
 */
function getSentComponents(data) {
    return ('<div class=\'row col-xs-12\'>\n' +
        '<div class=\'col-xs-4\'>' +
        '<table style=\'width:100%\' class=\'table table-striped\'>\n' +
        '<tr>\n' +
        '<thead>\n' +
        '<th>MCO</th>\n' +
        '<th>SAP</th>\n' +
        '<th>TEM</th>\n' +
        '<th>DAR</th>\n' +
        '<th>WEB</th>\n' +
        '</thead>\n' +
        '</tr>\n' +
        '<tbody>\n' +
        '<tr>\n' +
        // Loop through components.
        $.map(universal_components.components, function (component) {
            if ((component.uc_type !== 'promo') &&
                (component.uc_tool === 'cpdv1') &&
                (component.uc_type !== 'mgt')) {

                return ('<td class=\'text-center\'><button class=\'btn btn-default btn-sm coming-soon-tool\' ' +
                    'data-toggle=\'modal\' data-target=\'#view-sent-dialog\'' +
                    ' data-id=' + data.deal_id + ' data-primary-key=' + component.uc_ref_primary_key +
                    ' data-name=' + component.uc_name + ' data-controller=' + component.uc_controller +
                    '' +
                    ' data-table=' + component.uc_table + ' data-col=' + component.uc_col_name + '>' +
                    '<span class=\'glyphicon glyphicon-' + component.uc_icon + '\'></span></button>' +
                    '</td>');
            }
        }).join('') +
        '</tr>\n' +
        '</tbody>\n' +
        '</table>\n' +
        '</div>\n' +
        '</div>');
}

/**
 * Get the text for Status.
 *
 * @param status Object Status for deal.
 */
function getStatusText(status) {
    let text_status = '';
    if (status.match(/_ph/g) != null) {
        text_status = 'Phone';
    } else if (status.match(/_per/g) != null) {
        text_status = 'Person';
    } else if ((status.match(/accepted/g) || status.match(/ref_acc/g)) != null) {
        text_status = 'Accepted';
    } else if (status.match(/submitted/g) != null) {
        text_status = 'Submitted';
    } else if ((status.match(/complete/g)) || (status.match(/ref_com/g)) != null) {
        text_status = 'Completed';
    } else if (status.match(/request/g) != null) {
        text_status = 'Requested';
    } else if (status.match(/deliver/g) != null) {
        text_status = 'Delivered';
    } else if (status.match(/schedule/g) != null) {
        text_status = 'Scheduled';
    } else if (status.match(/deal_pros_a/g) != null) {
        text_status = 'Accepted';
    } else if ((status.match(/receive/g) || status.match(/ref_rec/g)) != null || status.match(/deal_pros/g) != null) {
        text_status = 'Received';
    } else if (status.match(/count_offer/g)) {
        text_status = 'Counter Offer';
    } else if (status.match(/offer_acc/g)) {
        text_status = 'Offer Accepted';
    } else if (status.match(/subm_offer/g)) {
        text_status = 'Offer Submitted';
    } else if (((status.match(/offer/g)) || (status.match(/ref_off/g))) !== null) {
        text_status = 'Offered';
    } else if (status.match(/ref_pla/g)) {
        text_status = 'In-Play';
    } else if (status.match(/ref_pa/g)) {
        text_status = 'Paid Out';
    } else if (status.match(/b_inactive/g)) {
        text_status = 'Considered';
    } else if (status.match(/b_active/g)) {
        text_status = 'Contacted';
    } else if (status.match(/buyer_req/g)) {
        text_status = 'Requested';
    } else if (status.match(/sign_ca/g)) {
        text_status = 'Signed CA';
    } else if (status.match(/diligence/g)) {
        text_status = 'Due Diligence';
    } else if (status.match(/b_contr_c/g)) {
        text_status = 'Contract Closed';
    } else if (status.match(/contingency/g)) {
        text_status = 'Contingencies Removed';
    }

    return text_status;
}

/**
 * Setup the content for amount button popover.
 *
 * @param dummy_content   String Layout for content.
 * @param data            Object Data for popover.
 *
 */
function setupAmountPopoverContent(dummy_content, data) {
    //Fetch the HTML content for Popover and replace with values.
    dummy_content.find('.price').attr('id', 'price' + data.hca_id);
    dummy_content.find('.price').attr('data-id', data.hca_id);
    dummy_content.find('.price').attr('value', '$' + parseInt(data.deal_price).toLocaleString());

    dummy_content.find('.rate').attr('value', parseInt(data.deal_rate) + '%');
    dummy_content.find('.rate').attr('data-id', data.hca_id);
    dummy_content.find('.rate').attr('id', 'rate' + data.hca_id);

    dummy_content.find('.gross').attr('value', '$' +
        parseInt(data.deal_price * data.deal_rate / 100).toLocaleString());

    dummy_content.find('.gross').attr('data-id', data.hca_id);
    dummy_content.find('.gross').attr('id', 'gross' + data.hca_id);

    dummy_content.find('.csplit').attr('value', parseInt(data.deal_split) + '%');
    dummy_content.find('.csplit').attr('data-id', data.hca_id);
    dummy_content.find('.csplit').attr('id', 'split' + data.hca_id);

    dummy_content.find('.net').attr('value', '$' + parseInt(data.deal_net));
    dummy_content.find('.net').attr('data-id', data.hca_id);
    dummy_content.find('.net').attr('id', 'net' + data.hca_id);

    dummy_content.find('.gsplit').attr('value', parseInt(data.deal_gsplit) + '%');
    dummy_content.find('.gsplit').attr('data-id', data.hca_id);
    dummy_content.find('.gsplit').attr('id', 'gsplit' + data.hca_id);

    dummy_content.find('.gnet').attr('data-id', data.hca_id);
    dummy_content.find('.gnet').attr('id', 'gnet' + data.hca_id);
}

/**
 * Setup the content for amount button popover.
 *
 * @param dummy_content   String Layout for content.
 * @param data            Object Data for popover.
 *
 */
function setupBuySidePopoverContent(dummy_content, data) {
    //Fetch the HTML content for Popover and replace with values.
    dummy_content.find('.price').attr('id', 'b_price' + data.hba_id);
    dummy_content.find('.price').attr('data-name', 'hba_price');
    dummy_content.find('.price').attr('data-id', data.hba_id);
    dummy_content.find('.price').attr('value', '$' + parseInt(data.hba_price).toLocaleString());

    dummy_content.find('.rate').attr('value', parseInt(data.hba_rate) + '%');
    dummy_content.find('.rate').attr('data-id', data.hba_id);
    dummy_content.find('.rate').attr('data-name', 'hba_rate');
    dummy_content.find('.rate').attr('id', 'b_rate' + data.hba_id);

    dummy_content.find('.gross').attr('value', '$' +
        parseInt(data.hba_price * data.hba_rate / 100).toLocaleString());

    dummy_content.find('.gross').attr('data-id', data.hba_id);
    dummy_content.find('.gross').attr('data-name', 'hba_gross');
    dummy_content.find('.gross').attr('id', 'b_gross' + data.hba_id);

    dummy_content.find('.csplit').attr('value', parseInt(data.hba_split) + '%');
    dummy_content.find('.csplit').attr('data-id', data.hba_id);
    dummy_content.find('.csplit').attr('data-name', 'hba_split');
    dummy_content.find('.csplit').attr('id', 'b_split' + data.hba_id);

    dummy_content.find('.net').attr('value', '$' + parseInt(data.hba_net));
    dummy_content.find('.net').attr('data-id', data.hba_id);
    dummy_content.find('.net').attr('data-name', 'hba_net');
    dummy_content.find('.net').attr('id', 'b_net' + data.hba_id);

    dummy_content.find('.gsplit').attr('value', parseInt(data.hba_gsplit) + '%');
    dummy_content.find('.gsplit').attr('data-id', data.hba_id);
    dummy_content.find('.gsplit').attr('data-name', 'hba_gsplit');
    dummy_content.find('.gsplit').attr('id', 'b_gsplit' + data.hba_id);

    dummy_content.find('.gnet').attr('data-id', data.hba_id);
    dummy_content.find('.gnet').attr('data-name', 'hba_gnet');
    dummy_content.find('.gnet').attr('id', 'b_gnet' + data.hba_id);
}

/**
 * Setup the content for value button popover.
 *
 * @param dummy_value_content    Dummy Value Content
 * @param data                   Object Data for popover.
 *
 */
function setupValuePopoverContent(dummy_value_content, data) {
    //Fetch the HTML content for Popover and replace with values.
    dummy_value_content.find('.price').attr('id', 'value-price' + data.hca_id);
    dummy_value_content.find('.price').attr('data-id', data.hca_id);
    dummy_value_content.find('.price').attr('value', '$' + parseInt(data.deal_price).toLocaleString());

    //Fetch the HTML content for Popover and replace with values.
    dummy_value_content.find('.usable_sf').attr('id', 'usable-sf' + data.hca_id);
    dummy_value_content.find('.usable_sf').attr('data-id', data.hca_id);
    dummy_value_content.find('.usable_sf').attr('value', parseInt(data.hca_usable_sf).toLocaleString());

    //Fetch the HTML content for Popover and replace with values.
    dummy_value_content.find('.gross_sf').attr('id', 'gross-sf' + data.hca_id);
    dummy_value_content.find('.gross_sf').attr('data-id', data.hca_id);
    dummy_value_content.find('.gross_sf').attr('value', parseInt(data.hca_gross_sf).toLocaleString());

    dummy_value_content.find('.livable_sf_price').attr('id', 'livable-sf-price' + data.hca_id);
    dummy_value_content.find('.livable_sf_price').attr('data-id', data.hca_id);
    dummy_value_content.find('.livable_sf_price').attr('value', '$' +
        parseInt(data.deal_price / data.hca_usable_sf).toLocaleString());

    dummy_value_content.find('.gross_sf_price').attr('id', 'gross-sf-price' + data.hca_id);
    dummy_value_content.find('.gross_sf_price').attr('data-id', data.hca_id);
    dummy_value_content.find('.gross_sf_price').attr('value', '$' +
        parseInt(data.deal_price / data.hca_gross_sf).toLocaleString());
}

/**
 * Setup the content for Revenue button popover.
 *
 * @param dummy_revenue_content  Dummy Revenue Content.
 * @param data                   Object Data for popover.
 *
 */
function setupRevenuePopoverContent(dummy_revenue_content, data) {
    //Fetch the HTML content for Popover and replace with values.
    dummy_revenue_content.find('.number').attr('id', 'number' + data.hca_id);
    dummy_revenue_content.find('.number').attr('data-id', data.hca_id);

    //Fetch the HTML content for Popover and replace with values.
    dummy_revenue_content.find('.total_sq_ft').attr('id', 'total-sq-ft' + data.hca_id);
    dummy_revenue_content.find('.total_sq_ft').attr('data-id', data.hca_id);

    //Fetch the HTML content for Popover and replace with values.
    dummy_revenue_content.find('.sq_ft').attr('id', 'sf-ft' + data.hca_id);
    dummy_revenue_content.find('.sq_ft').attr('data-id', data.hca_id);

    //Fetch the HTML content for Popover and replace with values.
    dummy_revenue_content.find('.rent_per_mo').attr('id', 'rent-per-mo' + data.hca_id);
    dummy_revenue_content.find('.rent_per_mo').attr('data-id', data.hca_id);

    //Fetch the HTML content for Popover and replace with values.
    dummy_revenue_content.find('.total_rent_per_mo').attr('id', 'total-rent-per-mo' + data.hca_id);
    dummy_revenue_content.find('.total_rent_per_mo').attr('data-id', data.hca_id);

    //Fetch the HTML content for Popover and replace with values.
    dummy_revenue_content.find('.total_number_units').attr('id', 'total-number-units' + data.hca_id);
    dummy_revenue_content.find('.total_number_units').attr('data-id', data.hca_id);

    //Fetch the HTML content for Popover and replace with values.
    dummy_revenue_content.find('.total_livable_sf').attr('id', 'total-livable-sf' + data.hca_id);
    dummy_revenue_content.find('.total_livable_sf').attr('data-id', data.hca_id);

    //Fetch the HTML content for Popover and replace with values.
    dummy_revenue_content.find('.monthly_total_gross_income').attr('id', 'total-monthly-gross-income' + data.hca_id);
    dummy_revenue_content.find('.monthly_total_gross_income').attr('data-id', data.hca_id);

    //Fetch the HTML content for Popover and replace with values.
    dummy_revenue_content.find('.yearly_total_gross_income').attr('id', 'total-yearly-gross-income' + data.hca_id);
    dummy_revenue_content.find('.yearly_total_gross_income').attr('data-id', data.hca_id);
}


/**
 * Updates Deal fields in Database.
 *
 * @param id   Int    Deal ID
 * @param col  String Column Name
 * @param val  Mixed  Value
 * @param lat  Float  Latitude coordinates.
 * @param long Float  Longitude coordinates.
 *
 */
function updateDeal(id, col, val, lat = null, long = null) {
    makeAjaxCall("cpdv1/update_deal",
        {
            id: id,
            col: col,
            val: val,
            lat: lat,
            long: long
        });
}

/**
 * Returns Build popover layout,
 *
 * @param data Object Client data.
 *
 */
function getBuildComponentLayout(data) {
    return ('<div class=\'row col-xs-12\'>\n' +
        '<table style=\'width:100%\' class=\'table table-striped\'>\n' +
        '<tr>\n' +
        '<thead>\n' +
        '<th>MCO</th>\n' +
        '<th>SAP</th>\n' +
        '<th>TPC</th>\n' +
        '<th>DUR</th>\n' +
        '<th>DAL</th>\n' +
        '<th>GMT</th>\n' +
        '</thead>\n' +
        '</tr>\n' +
        '<tbody>\n' +
        '<tr>' +
        '<td>' +
        '<span class=\'has-tooltip\' data-title=\'Marketing Campaign Overlay.\'>' +
        '<a class=\'btn btn-sm btn-default btn-sm has-popover\' id=\'mco\' ' +
        'data-id=' + data.deal_id + ' data-hca=' + data.hca_id + ' data-toggle=\'popover\'>' +
        '<span class=\'glyphicon glyphicon-list-alt\'></span>' +
        '</a></span></td>' +
        '<td>' +
        '<span class=\'has-tooltip\' data-title=\'Strategic Advisory Program.\'>' +
        '<a class=\'btn btn-sm btn-default btn-sm has-popover\' id=\'sap\'' +
        'data-id=' + data.deal_id + ' data-hca=' + data.hca_id + ' data-toggle=\'popover\'>' +
        '<span class=\'glyphicon glyphicon-list-alt\'></span>' +
        '</a></span></td>' +
        '<td>' +
        '<span class=\'has-tooltip\' data-title=\'Transactional Event Management.\'>' +
        '<a class=\'btn btn-sm btn-default btn-sm has-popover\' id=\'tem\'' +
        'data-id=' + data.deal_id + ' data-hca=' + data.hca_id + ' data-toggle=\'popover\'>' +
        '<span class=\'glyphicon glyphicon-list-alt\'></span>' +
        '</a></span></td>' +
        '<td>' +
        '<span class=\'has-tooltip\' data-title=\'Document Components.\'>' +
        '<a class=\'btn btn-sm btn-default btn-sm has-popover\' id=\'doc\'' +
        'data-id=' + data.deal_id + ' data-hca=' + data.hca_id + ' data-toggle=\'popover\'>' +
        '<span class=\'glyphicon glyphicon-list-alt\'></span>' +
        '</a></span></td>' +
        '<td>' +
        '<span class=\'has-tooltip\' data-title=\'Document Assembly Library.\'>' +
        '<a class=\'btn btn-sm btn-default btn-sm has-popover\' id=\'dal\'' +
        'data-id=' + data.deal_id + ' data-hca=' + data.hca_id + ' data-toggle=\'popover\'>' +
        '<span class=\'glyphicon glyphicon-list-alt\'></span>' +
        '</a></span></td>' +
        '<td>' +
        '<span class=\'has-tooltip\' data-title=\'Game Management Tools.\'>' +
        '<a class=\'btn btn-sm btn-default btn-sm has-popover\' id=\'gmt\'' +
        'data-id=' + data.deal_id + ' data-hca=' + data.hca_id + ' data-toggle=\'popover\'>' +
        '<span class=\'glyphicon glyphicon-list-alt\'></span>' +
        '</a></span></td>' +
        '</tr></tbody></table></div>');
    useTooltip();
}

/**
 * Returns Notes popover layout,
 *
 * @param data Object Client data.
 *
 */
function getNotesContent(data) {
    return ('<div class=\'row col-xs-12\'>\n' +
        '<table style=\'width:100%\' class=\'table table-striped\'>\n' +
        '<tr>\n' +
        '<thead>\n' +
        '<th>Notes</th>\n' +
        '<th>Action</th>\n' +
        '<th>Calls</th>\n' +
        '<th>Team</th>\n' +
        '<th>DAA</th>\n' +
        '<th>DRP</th>\n' +
        '</thead>\n' +
        '</tr>\n' +
        '<tbody>\n' +
        '<tr>' +
        '<td>' +
        '<span class=\'has-tooltip\' title=\'Notes List\'>' +
        '<button class=\'btn btn-default btn-sm notes\'' +
        ' id=\'notes' + data.deal_id + '\' data-id=' + data.deal_id + '' +
        ' data-toggle=\'modal\' data-target=\'#view-notes-dialog\'>' +
        '<span class=\'glyphicon glyphicon-list\'></span></button></span></td>' +
        '<td>' +
        '<span class=\'has-tooltip\' title=\'Next Action List\'>' +
        '<button class=\'btn btn-default btn-sm notes\'' +
        ' id=\'action' + data.deal_id + '\' data-id=' + data.hca_id + '' +
        ' data-toggle=\'modal\' data-target=\'#view-action-dialog\'>' +
        '<span class=\'glyphicon glyphicon-list\'></span></button></span></td>' +
        '<td>' +
        '<span class=\'has-tooltip\' title=\'Call Control Manager\'>' +
        '<button class=\'btn btn-default btn-sm\' data-id=' + data.deal_id + ' ' +
        'data-toggle=\'modal\' data-target=\'#view-ccr-dialog\'>' +
        '<span class=\'glyphicon glyphicon-list-alt\'></span></button></span></td>' +
        '<td>' +
        '<span class=\'has-tooltip\' data-title=\'Team Information.\'>' +
        '<a class=\'btn btn-sm btn-default btn-sm has-popover\' id=\'team\' data-lead=' + data.deal_lead + ' ' +
        ' data-gen=' + data.deal_gen + ' ' +
        'data-id=' + data.deal_id + ' data-hca=' + data.hca_id + ' data-toggle=\'popover\'>' +
        '<span class=\'glyphicon glyphicon-list-alt\'></span>' +
        '</a></span></td>' +
        '<td class=\'text-center\'>' +
        '<button class=\'has-tooltip btn btn-default btn-sm\'' +
        ' data-title=\'Deal Status Tracker\' id=\'DST\'><span class=\'glyphicon glyphicon-file\'></span>' +
        '</button></td>' +
        '<td class=\'text-center\'>' +
        '<button class=\'has-tooltip btn btn-default btn-sm\'' +
        ' data-title=\'Deal Conversion Board\' id=\'DCB\'><span class=\'glyphicon glyphicon-file\'></span>' +
        '</button></td>' +
        '</tr></tbody></table></div>'
    )
        ;
    useTooltip();
}

/**
 * Returns layout for lead drop down.
 *
 * @param leads Object Lead Data.
 * @param data  Object Client data,
 *
 */
function getLeadDropDown(leads, hca_id, deal_lead) {
    return ('<select' +
        ' name="lead' + hca_id + '" class="form-control cpd-info cpd_lead hca-info"' +
        ' data-id="' + hca_id + '" data-name="hca_lead" data-type="lead"' +
        ' data-target="view-lead-dialog">' +
        '<option disabled selected value="-1"> --Lead-- </option>' +
        '<option value="0">Add</option>' +
        $.map(leads, function (lead) {
            if ('lead' === lead.dl_type)
                return ('<option value="' + lead.dl_id + '" ' + (deal_lead === lead.dl_id
                    ? 'selected' : '') + '>' + lead.dl_name + '</option>');
        }).join('') + '</select>');
}

/**
 * Returns info popover content.
 *
 * @param data Object Client data.
 *
 */
function getInfoContent(data) {
    return ('<div class=\'row col-xs-12\'>\n' +
        '<table style=\'width:100%\' class=\'table table-striped\'>\n' +
        '<tr>\n' +
        '<thead>\n' +
        '<th>Profile</th>\n' +
        '<th>Asset</th>\n' +
        '<th class=\'text-center\'>SA</th>\n' +
        '<th>Map</th>\n' +
        '<th>Comps</th>\n' +
        '<th>Rev</th>\n' +
        '<th>Value</th>\n' +
        '<th>Income</th>\n' +
        '</thead>\n' +
        '</tr>\n' +
        '<tbody>\n' +
        '<tr>' +
        '<td>' +
        '<span class=\'has-tooltip\' data-title=\'Client Profile Dialog.\'>' +
        '<button class=\'btn btn-sm btn-default has-popover\' ' +
        'data-id=' + data.hc_id + ' data-target=\'#view-owner-profile-dialog\'' +
        ' data-toggle=\'modal\' data-owner=\'' + data.hc_id + '\'>' +
        '<span class=\'glyphicon glyphicon-list-alt\'></span>' +
        '</button></span></td>' +
        '<td>' +
        '<span class=\'has-tooltip\' data-title=\'Asset Information Dialog.\'>' +
        '<button class = \'btn btn-default btn-sm\'' +
        ' data-toggle = \'modal\' data-buyer=\'false\' data-owner=\'' + data.hc_id + '\'' +
        ' data-target = \'#view-asset-info-dialog\' data-backdrop = \'static\'' +
        ' data-id=\'' + data.deal_id + '\' ' +
        ' data-current-status=\'' + data.ds_status + '\'>' +
        ' <span class = \'glyphicon glyphicon-info-sign\'></span></button></span></td>' +
        '<td>' +
        '<span class=\'has-tooltip\' data-title=\'Strategic Advisory Dialog.\'>' +
        '<button class = \'btn btn-default btn-sm\'' +
        ' data-toggle = \'modal\'' +
        ' data-target = \'#view-sa-dialog\' data-backdrop = \'static\'' +
        ' data-id=\'' + data.deal_id + '\' ' +
        ' data-current-status=\'' + data.ds_status + '\'>' +
        ' <span class = \'glyphicon glyphicon-list-alt\'></span></button></span></td>' +
        '<td>' +
        '<span class=\'has-tooltip\' data-title=\'Asset Address Map.\'>' +
        '<button class = \'btn btn-default btn-sm\'' +
        ' data-toggle = \'modal\'' +
        ' data-target = \'#map-dialog\' data-backdrop = \'static\'' +
        ' data-name = \'' + data.deal_property + '\'' +
        ' data-long = \'' + data.deal_property_long + '\'' +
        ' data-lat = \'' + data.deal_property_lat + '\'' +
        ' data-id=\'' + data.deal_id + '\' ' +
        ' data-current-status=\'' + data.ds_status + '\'>' +
        ' <span class = \'glyphicon glyphicon-map-marker\'></span></button></span></td>' +
        '<td>' +
        '<span class=\'has-tooltip\' data-title=\'Comparable.\'>' +
        '<button type=\'button\' data-title=\'Comparable\'' +
        ' class=\'btn btn-default btn-sm has-popover comps_button\'' +
        ' data-html=\'true\' id=\'comps' + data.hca_id + '\'' +
        ' data-toggle=\'modal\' data-placement=\'top\' data-container=\'body\' ' +
        ' data-id=\'' + data.hca_id + '\' data-target=\'#view-comps-dialog\'' +
        ' data-current-status=\'' + data.ds_status + '\'' +
        '><span class=\'glyphicon glyphicon-map-marker\'></span></button></span></td>' +
        '<td>' +
        '<span class=\'has-tooltip\' data-title=\'Revenue Calcultor.\'>' +
        '<button type=\'button\' data-container=\'body\' data-title=\'Revenue Calculator.\'' +
        ' class=\'btn btn-default btn-sm has-popover revenue-button\'' +
        ' data-html=\'true\' id=\'revenue_button' + data.hca_id + '\'' +
        ' data-toggle=\'popover\' data-placement=\'top\' data-container=\'body\' ' +
        ' data-id=\'' + data.hca_id + '\' ' +
        ' data-current-status=\'' + data.ds_status + '\'' +
        '><span class=\'glyphicon glyphicon-usd\'></span></button></span></td>' +
        '<td>' +
        '<span class=\'has-tooltip\' data-title=\'Value Calcultor.\'>' +
        '<button type=\'button\' data-container=\'body\' data-title=\'Value Calculator.\'' +
        ' class=\'btn btn-default btn-sm has-popover value-button\'' +
        ' data-html=\'true\' id=\'value_button' + data.hca_id + '\'' +
        ' data-toggle=\'popover\' data-placement=\'top\' data-container=\'body\' ' +
        ' data-id=\'' + data.hca_id + '\' ' +
        ' data-current-status=\'' + data.ds_status + '\'' +
        '><span class=\'glyphicon glyphicon-usd\'></span></button></span></td>' +
        '<td>' +
        '<span class=\'has-tooltip\' data-title=\'Asset Income Dialog.\'>' +
        '<button type=\'button\' data-container=\'body\' data-title=\'Income Information.\'' +
        ' class=\'btn btn-default btn-sm has-popover amount_button\'' +
        ' data-html=\'true\' id=\'new_amount_button' + data.hca_id + '\'' +
        ' data-toggle=\'popover\' data-placement=\'top\' data-container=\'body\' ' +
        ' data-id=\'' + data.hca_id + '\' ' +
        ' data-current-status=\'' + data.ds_status + '\'' +
        '><span class=\'glyphicon glyphicon-usd\'></span></button></span></td>' +
        '</tr></tbody></table></div>');
}

/**
 * Returns buyer info popover content.
 *
 * @param data Object Client data.
 *
 */
function getBuyerInfoContent(data) {
    return ('<div class=\'row col-xs-12\'>\n' +
        '<table style=\'width:100%\' class=\'table table-striped\'>\n' +
        '<tr>\n' +
        '<thead>\n' +
        '<th>Profile</th>\n' +
        '<th>Prefer</th>\n' +
        '<th>Offers</th>\n' +
        '<th>Provisions</th>\n' +
        '<th>Income</th>\n' +
        '</thead>\n' +
        '</tr>\n' +
        '<tbody>\n' +
        '<tr>' +
        '<td class=\'text-center\'>' +
        '<span class=\'has-tooltip\' data-title=\'Buyer Profile Dialog.\'>' +
        '<button class=\'btn btn-sm btn-default has-popover\' ' +
        'data-id=' + data.hb_id + ' data-target=\'#client-info-dialog\'' +
        ' data-toggle=\'modal\' data-buyer=\'true\'>' +
        '<span class=\'glyphicon glyphicon-list-alt\'></span>' +
        '</button></span></td>' +
        '<td class=\'text-center\'>' +
        '<span class=\'has-tooltip\' data-title=\'Asset Information Dialog.\'>' +
        '<button class = \'btn btn-default btn-sm\'' +
        ' data-toggle = \'modal\' data-owner=\'' + data.hb_id + '\'' +
        ' data-target = \'#view-buyer-preference-dialog\' data-backdrop = \'static\'' +
        ' data-id=\'' + data.deal_id + '\' data-type=\'prefer\'' +
        ' data-current-status=\'' + data.ds_status + '\' data-heading=\'Buyer Preferences\'>' +
        ' <span class = \'glyphicon glyphicon-info-sign\'></span></button></span></td>' +
        '<td class=\'text-center\'>' +
        '<span class=\'has-tooltip\' data-title=\'Buyer Offer Cycle.\'>' +
        '<button class = \'btn btn-default btn-sm\'' +
        ' data-toggle = \'modal\' data-type=\'offer\' data-heading=\'Buyer Offer Cycle\'' +
        ' data-target = \'#view-buyer-preference-dialog\' data-backdrop = \'static\'' +
        ' data-id=\'' + data.deal_id + '\' ' +
        ' data-current-status=\'' + data.ds_status + '\'>' +
        ' <span class = \'glyphicon glyphicon-list-alt\'></span></button></span></td>' +
        '<td class=\'text-center\'>' +
        '<span class=\'has-tooltip\' data-title=\'Owner Provisions.\'>' +
        '<button class = \'btn btn-default btn-sm\'' +
        ' data-toggle = \'modal\' data-heading=\'Owner Provisions\'' +
        ' data-target = \'#view-buyer-preference-dialog\' data-backdrop = \'static\'' +
        ' data-id=\'' + data.deal_id + '\' data-type=\'provision\'' +
        ' data-current-status=\'' + data.ds_status + '\'>' +
        ' <span class = \'glyphicon glyphicon-list-alt\'></span></button></span></td>' +
        '<td class=\'text-center\'>' +
        '<span class=\'has-tooltip\' data-title=\'Buy Side Income.\'>' +
        '<button type=\'button\' class = \'btn btn-default btn-sm buy-side-income-button\'' +
        ' data-toggle = \'popover\' data-html=\'true\' data-container=\'body\'' +
        ' data-title=\'Buy Side Income\' data-id=\'' + data.deal_id + '\' data-placement=\'top\'' +
        ' data-current-status=\'' + data.ds_status + '\'>' +
        ' <span class = \'glyphicon glyphicon-usd\'></span></button></span></td>' +
        '</tr></tbody></table></div>');
}

/**
 * Updates the values for FTG fields in the DMD view page.
 *
 * @param data Array Values for the FTG fields.
 */
function updateFTG(data) {

    // ID Selector
    const qtr_target_selector = $('#qtr_target'),
        year_target_selector = $('#year_target'),
        year_actual_selector = $('#year_actual');

    // Clear any previous FTG column values.
    $('#year_floor, #year-game, #year-target, #qtr-floor, #qtr-game, #qtr-target').val('');

    // Set the yearly value of the elements.
    $('#year_floor').val((undefined !== data ? numeral(data.uo_floor) : numeral(0)).format('$0,0'));
    year_target_selector.val((undefined !== data ? numeral(data.uo_target) : numeral(0)).format('$0,0'));
    $('#year_game').val((undefined !== data ? numeral(data.uo_game) : numeral(0)).format('$0,0'));

    // Set the quarterly value of the elements.
    $('#qtr_floor').val((undefined !== data ? numeral(data.uqo_floor) : numeral(0)).format('$0,0'));
    qtr_target_selector.val((undefined !== data ? numeral(data.uqo_target) : numeral(0)).format('$0,0'));
    $('#qtr_game').val((undefined !== data ? numeral(data.uqo_game) : numeral(0)).format('$0,0'));

    year_actual_selector.val(numeral($('#cpd-deal-complete-total-doll').val()).format('$0,0'));

    // Fetch the quarter actual, target and calculate percentage.
    const qtr_actual = numeral($('#qtr_actual').val())._value,
        qtr_target = numeral(qtr_target_selector.val())._value;
    let qtr_percentage = ((qtr_actual / qtr_target) * 100).toFixed(2);

    qtr_percentage = isNaN(qtr_percentage) ? 0 : qtr_percentage;

    $('#qtr_percent').val((Infinity !== Number(qtr_percentage)) ? qtr_percentage + '% of Target' : 0 + '% of Target');
    $('#qtr_needed').val(numeral(qtr_target - qtr_actual).format(('$0,0')));

    // Fetch the year actual, target and calculate percentage.
    const actual = numeral(year_actual_selector.val())._value,
        target = numeral(year_target_selector.val())._value;
    let percentage = ((actual / target) * 100).toFixed(2);

    percentage = isNaN(percentage) ? 0 : percentage;

    // Set the values for Percentage and needed.
    $('#year_percent').val((Infinity !== Number(percentage)) ? percentage + "% of Target" : 0 + "% of Target");
    $('#year_needed').val(numeral(target - actual).format('$0,0'));
}

// Double Scroll Bar.
function TabDoubleScroll() {
    enableDoubleScroll('.cpd-pane');
    document.getElementsByClassName('doubleScroll-scroll')[0].style.width =
        $('.active .table')[0].clientWidth + 'px';
}



