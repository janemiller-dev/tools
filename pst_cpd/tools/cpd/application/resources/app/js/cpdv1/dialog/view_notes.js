/**
 *
 * View DMD notes.
 *
 * @summary      Dialog used to display Notes for DMD clients.
 * @description  This file contains functions for Displaying DMD client Notes..
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */

$(document).ready(function () {

    $('#view-notes-dialog').on('show.bs.modal', function (event) {
        view_notes.init(event);
    });

    view_notes.add_event();
});

/**
 *
 * View Notes available.
 */

let view_notes = function () {

    let max_seq = 0,
        deal_id;

    // Initialize and get the DMD ID.
    let init = function (event) {
            deal_id = $(event.relatedTarget).data('id');

            // Clear any previous values.
            $('.additional-row').remove();
            $('.event-input').val('');
            $('.event-date').val('');
            $('.event-text').text('');

            // Fetch the Notes Content.
            const notes_data = makeAjaxCall('cpdv1/get_notes_content', {
                'id': deal_id
            });

            // Append Fetched data to the Notes popup.
            notes_data.then(function (data) {
                let dummy_content;
                $.each(data.notes_content, function (index, content) {
                    const notes_seq_selector = $('#dn_name' + content.dn_seq);

                    // Check if this notes row exist. If not clone and append it to dialog.
                    if (0 === notes_seq_selector.length) {
                        dummy_content = $('#event-notes-div').clone();
                        dummy_content.find('#dn_name0')
                            .attr('data-seq', (parseInt(content.dn_seq)))
                            .attr('id', 'dn_name' + (parseInt(content.dn_seq)));

                        dummy_content.find('textarea')
                            .attr('data-seq', (parseInt(content.dn_seq)))
                            .attr('id', 'dn_note' + (parseInt(content.dn_seq)));

                        dummy_content.find('#dn_date0')
                            .attr('data-seq', (parseInt(content.dn_seq)))
                            .attr('id', 'dn_date' + (parseInt(content.dn_seq)));

                        dummy_content.find('#delete-note0')
                            .attr('data-seq', (parseInt(content.dn_seq)))
                            .attr('data-id', deal_id)
                            .attr('id', '#delete-note' + (parseInt(content.dn_seq)));

                        $('#notes-row').append('<div class="col-xs-12 additional-row">'
                            + dummy_content.html() + '</div>');
                    }

                    $('#dn_name' + content.dn_seq).val(content.dn_name);
                    $('#dn_note' + content.dn_seq).text(content.dn_note);
                    $('#dn_date' + content.dn_seq).val(content.dn_when);
                    $('.delete-note').attr('data-id', deal_id)


                    max_seq = content.dn_seq;
                });
                store_data();
                enable_date_picker();
            });

            // Delete note click handler.
            $(document).off('click', '.delete-note');
            $(document).on('click', '.delete-note', function () {
                const delete_note = makeAjaxCall('cpdv1/delete_note', {
                    'id': $(this).attr('data-id'),
                    'seq': $(this).attr('data-seq')
                })

                delete_note.then(function (data) {
                    $('#view-notes-dialog').modal('hide');
                    toastr.success('Note Delted.', 'Success!!')
                })
            })
        },

        // Stores Notes data.
        store_data = function () {
            const event_selector = $('.event');

            event_selector.unbind('change');
            event_selector.change(function () {
                makeAjaxCall('cpdv1/update_notes_content', {
                    'id': deal_id,
                    'col': $(this).data('name'),
                    'val': $(this).val(),
                    'seq': $(this).data('seq')
                });
            });
        },

        enable_date_picker = function () {
            $('.action-date').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss'
            });
        },

        // Adds new column to the event List.
        add_event = function () {

            // Add a new event to the list.
            $('#add-event').click(function () {
                let dummy_content = $('#event-notes-div').clone();
                dummy_content.find('#dn_name0')
                    .attr('data-seq', (parseInt(max_seq) + 1))
                    .attr('id', 'dn_name' + (parseInt(max_seq) + 1))
                    .attr('value', '');

                dummy_content.find('#dn_date0')
                    .attr('data-seq', (parseInt(max_seq) + 1))
                    .attr('id', 'dn_date' + (parseInt(max_seq) + 1))
                    .attr('value', '');

                dummy_content.find('textarea')
                    .attr('data-seq', (parseInt(max_seq) + 1))
                    .attr('id', 'dn_note' + (parseInt(max_seq) + 1)).text('');


                dummy_content.find('#delete-note0')
                    .attr('data-seq', (parseInt(max_seq) + 1))
                    .attr('data-id', deal_id)
                    .attr('id', '#delete-note' + (parseInt(max_seq) + 1));

                // Append to the last of List.
                $('#notes-row').append('<div class="col-xs-12 additional-row">' + dummy_content.html() + '</div>');
                max_seq = parseInt(max_seq) + 1;
                enable_date_picker();
                store_data();
            });
        };

    return {
        init: init,
        add_event: add_event
    };
}();