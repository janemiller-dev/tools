var activeOutlineId, activeSAId, outline_added, sa_added, client_id, obj;

$(document).ready(function () {
    loadPrev();

    // Get the Client ID from the URL
    var path = window.location.pathname;
    var components = path.split('/');
    client_id = components[components.length - 1];

    // Upload background/Image to be embedded with SA doc.
    $('.upload-ppt').click(function () {

        var data = {
            'client_id': client_id,
            'format': $('#docs-format').val(),
            'type': $('#docs-type').val(),
            'category': $('#docs-category').val()
        };

        $('#fileupload').trigger('click');

        var file_type_allowed = /.\.(pptx|ppt|odt)/i;
        var target = 'fileupload';
        var upload_web = uploadFile(target, data, file_type_allowed);

        upload_web.then(function (data) {
            toastr.success('File Uploaded!!', 'Success!!');

            $('.upload-ppt').attr('data-name', data._response.result.name);
        });

        return false;
    });
});

