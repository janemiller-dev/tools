let client_id, ref, active_page = active_id = -1, page_added = 0;

$(document).ready(function () {
    loadPrev();

    const path = window.location.pathname,
        components = path.split('/');
    client_id = components[components.length - 1];
    ref = window.location.search.split('?ref=')[1];

    refreshPage();

    // Document upload.
    $('#upload-page').click(function () {

        const data = {
            'version_id': active_id,
            'client_type': $('#client-type').val(),
            'doc_del': $('#delivery-type').val(),
            'doc_format': $('#assembler-format').val(),
            'doc_type': 'page'
        };

        $('#page-upload').trigger('click');

        const target = 'page-upload',
            file_type_allowed = /.\.(pdf)/i;

        // Upload a Document file.
        const upload_doc = uploadFile(target, data, file_type_allowed);
        upload_doc.then(function (data) {
            page_added = 1;
            refreshPage();
        });
        return false;
    });

    // Adds a new Document to the assembler.
    $('#add_doc').click(function () {
        const add_doc = makeAjaxCall('cpdv1/add_assembler_instance', {
            client_id: client_id
        });

        add_doc.then(function (data) {
            toastr.success('Document Version Added.', 'Success!!');
            refreshPage();
        })
    });

    $('#assemble').click(function (data) {
        const assemble_pages = makeAjaxCall('cpdv1/merge_pages', {
            'version_id': active_id
        })

        assemble_pages.then(function (data) {
            toastr.success('Document Created.', 'Success!!');
            refreshPage();
        })
    });

    $('#move_up').click(function () {
        const move_page_up = makeAjaxCall('cpdv1/move_page_up', {
            'page_id': active_page,
            'order': $('#page_instance' + active_page).attr('data-order'),
            'version': active_id
        });

        move_page_up.then(function (data) {
            toastr.success('Page Order Updated');
            page_added = 1;
            refreshPage();
        })
    });

    $('#move_down').click(function () {
        const move_page_down = makeAjaxCall('cpdv1/move_page_down', {
            'page_id': active_page,
            'order': $('#page_instance' + active_page).attr('data-order'),
            'version': active_id
        });

        move_page_down.then(function (data) {
            toastr.success('Page Order Updated');
            page_added = 1;
            refreshPage();
        })
    })
});


function refreshPage() {

    const get_assembler_version = makeAjaxCall('cpdv1/get_assembler_instance', {
        client_id: client_id
    });

    get_assembler_version.then(function (data) {
        const assembler_version_selector = $('#version-list');
        assembler_version_selector.empty();

        // Update Docs list.
        $.each(data.instances, function (index, instance) {
            assembler_version_selector
                .append($('<li class="text-center" id="version_instance' + instance.cav_id + '">')
                    .append('<a href="#" class="edit-ref" onclick="previewVersion(this); return false;"' +
                        ' data-name="' + instance.cav_name + '"data-id="' + instance.cav_id + '">' +
                        '<span class="editable" data-type="text" data-pk = "' + instance.cav_id +
                        '" data-url = "' + jsglobals.base_url + 'cpdv1/update_assembler_name" data-placement="left">'
                        + instance.cav_name + '</span></a></li>'))
        });
        setEditable();

        if (-1 === active_id) {
            $('#version-list li').eq(0).find('a').click();
        } else if (1 === page_added) {
            $('#version_instance' + active_id).find('a').click();
            active_page = -1;
        }

    })
}

// Displays Assembler Version.
function previewVersion(obj) {
    active_id = $(obj).attr('data-id');
    $('.active').removeClass('active');
    $('#version_instance' + active_id).addClass('active');


    $('#delete-doc').attr('data-id', active_id);
    $('#delete-doc').attr('data-name', $('.active').text());
    const canvas_container_selector = $('#canvas-container');

    // URL to make request to.
    let url = window.location.origin + '/tools/cpdv1/preview_assembler/?id=' + $(obj).attr('data-id');

    // Display PDF in Canvas.
    canvas_container_selector.html('<h4 class="main-con-header">View Document</h4>' +
        '<canvas id="pdf-canvas"></canvas>');


    canvas_container_selector.after('<div align="center" class="attach_next_prev" ' +
        'style="position: absolute; top: .5em; left: 0; right: 0;">' +
        '<button id="prev" class="btn btn-secondary prev" style="float: left; width:2%; margin-left: 2em">' +
        '<i class="fa fa-angle-left"></i></button>\n' +
        '<button id="next" class="btn btn-secondary next" style="float: right; width: 2%;  margin-right: 3em">' +
        '<i class="fa fa-angle-right"></i>' +
        '</button>&nbsp; &nbsp;\n' +
        '</div>');

    const canvas = document.getElementById('pdf-canvas');
    preview_pdf(url, canvas);

    const fetch_pages = makeAjaxCall('cpdv1/fetch_assembler_pages', {
        doc_id: active_id
    });

    fetch_pages.then(function (data) {
        const assembler_page_selector = $('#pages-list');
        assembler_page_selector.empty();

        // Update Docs list.
        $.each(data.pages, function (index, instance) {
            assembler_page_selector
                .append($('<li class="text-center" id="page_instance' + instance.cap_id +
                    '" data-order="' + instance.cap_order + '">')
                    .append('<a href="#" class="edit-ref" onclick="previewPage(this); return false;"' +
                        ' data-name="' + instance.cap_name + '"data-id="' + instance.cap_id + '">' +
                        '<span class="editable" data-type="text" data-pk = "' + instance.cap_id +
                        '" data-url = "' + jsglobals.base_url + 'cpdv1/update_assembler_page_name">'
                        + instance.cap_name + '</span></a></li>'))
        });
        setEditable();

        if (-1 === active_page) {
            $('#pages-list li').eq(0).find('a').click();
        }
    });
}

// Display assembler Page.
function previewPage(obj) {
    active_page = $(obj).attr('data-id');

    $('.active_page').removeClass('active_page');
    $('#page_instance' + active_page).addClass('active_page');

    $('#delete-page').attr('data-id', active_page);
    $('#delete-page').attr('data-name', $('.active_page').text());
    const canvas_container_selector = $('#canvas-container');

    // URL to make request to.
    let url = window.location.origin + '/tools/cpdv1/preview_page/?id=' + $(obj).attr('data-id');

    // Display PDF in Canvas.
    canvas_container_selector.html('<h4 class="main-con-header">View Document</h4>' +
        '<canvas id="pdf-canvas"></canvas>');

    canvas_container_selector.after('<div align="center" class="attach_next_prev" ' +
        'style="position: absolute; top: .5em; left: 0; right: 0;">' +
        '<button id="prev" class="btn btn-secondary prev" style="float: left; width:2%; margin-left: 2em">' +
        '<i class="fa fa-angle-left"></i></button>\n' +
        '<button id="next" class="btn btn-secondary next" style="float: right; width: 2%;  margin-right: 3em">' +
        '<i class="fa fa-angle-right"></i>' +
        '</button>&nbsp; &nbsp;\n' +
        '</div>');

    const canvas = document.getElementById('pdf-canvas');
    preview_pdf(url, canvas);

}