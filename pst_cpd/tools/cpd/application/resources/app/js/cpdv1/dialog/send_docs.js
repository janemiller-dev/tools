// $(document).ready(function () {
//     let client_id, docs_id, random_name, origin, pdf_id,
//         path = window.location.pathname,
//         components = path.split('/');
//
//     client_id = components[components.length - 1];
//
//     // When the dialog is displayed, set the current instance ID.
//     $('#send-docs-dialog').on('show.bs.modal', function (event) {
//         tinymce.remove("#email-content");
//
//         docs_id = $(event.relatedTarget).data('id');
//         random_name = $(event.relatedTarget).data('random');
//         origin = $(event.relatedTarget).data('origin');
//
//         $(event.relatedTarget).removeData('id');
//         $(event.relatedTarget).removeData('type');
//         $(event.relatedTarget).removeData('random');
//
//         // Gets the client email.
//         $.ajax({
//             url: jsglobals.base_url + 'tsl/get_client_email',
//             dataType: 'json',
//             type: 'post',
//             data: {
//                 client_id: client_id,
//             },
//             error: ajaxError
//         }).done(function (data) {
//
//             if (data.status !== 'success') {
//                 toastr.error(data.message);
//                 return;
//             }
//             else if (data.status === 'redirect') {
//                 window.location.href = data.redirect;
//                 return;
//             }
//             $('#docs-client-email').text(data.client[0].tc_email);
//
//         }).fail(function (jqXHR, textStatus) {
//             toastr.error("Request failed: " + textStatus);
//         }).always(function () {
//         });
//
//         tinymce.init({
//             selector: '#email-content',
//             branding: false,
//             init_instance_callback: function (ed) {
//                 tinyMCE.DOM.setStyle(tinyMCE.DOM.get('email-content_ifr'), 'height', '55vh');
//             }
//         });
//
//         // Embed text to text area of Text editor.
//         const embed_content_selector = $('#embed-content');
//         embed_content_selector.unbind('click');
//         embed_content_selector.click(function () {
//             const content = tinyMCE.activeEditor.getContent() +
//                 $('#canvas-container_ifr').contents().find('#tinymce').html();
//             tinyMCE.activeEditor.setContent(content);
//         });
//
//         pdf_id = 0;
//         $('#attachment_name').text(' ' + $('a[data-id=' + docs_id + ']').text());
//     });
//
//     $(document).on('click', '#close-send-docs-dialog', function () {
//         $('#send-docs-dialog').modal('hide');
//     });
//
//     $(document).on('click', '#add-recipient', function () {
//         $('.client-email')
//             .after('<div class="col-xs-9 col-xs-offset-3" style="padding:0; margin-top:5px !important">' +
//                 '<input type="email" class="form-control docs-client-email"></div>');
//     });
//
//     // Sends the docstional document.
//     $(document).on('submit', '#send-docs-form', function (ev) {
//         let email_array = [];
//         $('.docs-client-email').each(function(){
//             ('' !== $(this).val()) ? email_array.push($(this).val()) : '';
//         });
//
//         enableLoader();
//         ev.preventDefault();
//         ev.stopPropagation();
//
//         // Get the text from editor.
//         const text = tinymce.activeEditor.getContent();
//         $.ajax({
//             url: jsglobals.base_url + 'tsl/send_attachment',
//             dataType: 'json',
//             type: 'post',
//             data: {
//                 client_id: client_id,
//                 client_email: email_array,
//                 id: docs_id,
//                 tool_id: window.location.search.split('?id=')[1],
//                 type: 'doc',
//                 origin: origin,
//                 random: random_name,
//                 subject: $('#docs-client-subject').val(),
//                 text: text,
//                 pdf_id: pdf_id,
//             },
//             error: ajaxError
//         }).done(function (data) {
//             $.unblockUI();
//             if (data.status != 'success') {
//                 toastr.error(data.message, 'Invalid Email ID')
//                 return;
//             }
//             else if (data.status == 'redirect') {
//
//                 window.location.href = data.redirect;
//                 return;
//             }
//             toastr.success('Email Sent!!');
//             $('#send-docs-dialog').modal('hide');
//             $('#docs-text').val('');
//         }).fail(function (jqXHR, textStatus) {
//             $.unblockUI();
//             toastr.error('Please check Email ID provided.','Unable to Send Email!!');
//             return false;
//         }).always(function () {
//         });
//
//         return false;
//     })
// });
