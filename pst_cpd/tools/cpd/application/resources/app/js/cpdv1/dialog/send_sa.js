$(document).ready(function () {
    let client_id, client_email, sa_id, type, random_name, origin, pdf_id,
        path = window.location.pathname,
        components = path.split('/');

    client_id = components[components.length - 1];

    // When the dialog is displayed, set the current instance ID.
    $('#send-sa-dialog').on('show.bs.modal', function (event) {
        tinymce.remove("#email-content");

        sa_id = $(event.relatedTarget).data('id');
        type = $(event.relatedTarget).data('type');

        random_name = $(event.relatedTarget).data('random');
        origin = $(event.relatedTarget).data('origin');

        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('type');
        $(event.relatedTarget).removeData('random');

        // Gets the client email.
        $.ajax({
            url: jsglobals.base_url + 'tsl/get_client_email',
            dataType: 'json',
            type: 'post',
            data: {
                client_id: client_id,
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            $('#sa-client-email').text(data.client[0].tc_email);

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });

        // Initialize Trix Editor.
        tinymce.init({
            selector: '#email-content',
            branding: false,
            init_instance_callback: function (ed) {
                tinyMCE.DOM.setStyle(tinyMCE.DOM.get('email-content_ifr'), 'height', '55vh');
            }
        });

        let pdf_id = $('.active').attr('id');
        if (null !== pdf_id) {
            $('#attachment_name').text(' ' + $('.active').text());
        }

        // Embed text to text area of Text editor.
        const embed_content_selector = $('#embed-content');
        embed_content_selector.unbind('click');
        embed_content_selector.click(function () {
            const content = tinyMCE.activeEditor.getContent() +
                $('#canvas-container_ifr').contents().find('#tinymce').html();
            tinyMCE.activeEditor.setContent(content);
        });
    });

    $(document).on('click', '#close-send-sa-dialog', function () {
        $('#send-sa-dialog').modal('hide');
    });

    $(document).on('click', '#add-recipient', function () {
        $('.client-email')
            .after('<div class="col-xs-9 col-xs-offset-3" style="padding:0; margin-top:5px !important">' +
                '<input type="email" class="form-control sa-client-email"></div>');
    });

    // Sends the sational document.
    $(document).on('submit', '#send-sa-form', function (ev) {
        let email_array = [];
        $('.sa-client-email').each(function () {
            ('' !== $(this).val()) ? email_array.push($(this).val()) : '';
        });

        enableLoader();
        ev.preventDefault();
        ev.stopPropagation();

        // Get the text from editor.
        const text = tinymce.activeEditor.getContent();
        $.ajax({
            url: jsglobals.base_url + 'tsl/send_attachment',
            dataType: 'json',
            type: 'post',
            data: {
                client_id: client_id,
                client_email: email_array,
                id: sa_id,
                type: type,
                origin: origin,
                random: random_name,
                subject: $('#sa-client-subject').val(),
                text: text,
                pdf_id: pdf_id,
                tool_id: window.location.search.split('?id=')[1]
            },
            error: ajaxError
        }).done(function (data) {
            $.unblockUI();

            if (data.status != 'success') {
                toastr.error(data.message, 'Invalid Email ID')
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('Email Sent!!');
            $('#send-sa-dialog').modal('hide');
            $('#sa-text').val('');
        }).fail(function (jqXHR, textStatus) {
            $.unblockUI();
            toastr.error('Please check EMail ID provided.', 'Unable to Send Email!!');
            return false;
        }).always(function () {
        });

        return false;
    })
});
