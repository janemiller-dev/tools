let client_id, ref, active_id = -1;

$(document).ready(function () {
    const path = window.location.pathname,
        components = path.split('/');
    client_id = components[components.length - 1];
    ref = window.location.search.split('?ref=')[1];
    loadPrev();
    refreshPage();

    // Document upload.
    $('#upload-docs').click(function () {

        const docs_format_selector = $('#docs-format');
        const data = {
            'client_id': client_id,
            'format': docs_format_selector.val(),
            'type': $('#docs-type').val(),
            'category': $('#docs-category').val()
        };

        // Trigger file upload click.
        $('#fileupload').trigger('click');

        const file_type = docs_format_selector.val();
        let file_type_allowed;

        // Check for the type of file to be uploaded.
        if ('pdf' === file_type) {
            file_type_allowed = /.\.(pdf)/i;
        } else if ('video' === file_type) {
            file_type_allowed = /.\.(mp4)/i;
        } else if ('audio' === file_type) {
            file_type_allowed = /.\.(mp3)/i;
        }

        const target = 'fileupload';

        // Upload a Document file.
        const upload_doc = uploadFile(target, data, file_type_allowed);
        upload_doc.then(function (data) {
            refreshPage()
        });
        return false;
    });

    // Check of doc type is changed.
    $('#docs-format, #docs-category, #docs-type').change(function () {
        $('#canvas-container').html('');
        active_id = 0;
        refreshPage();
    });

});

/**
 * Fetch list of Docs available.
 */

function refreshPage() {

    // Get the all docs for a client.
    const get_docs = makeAjaxCall('cpdv1/get_docs', {
        'client_id': client_id,
        'format': $('#docs-format').val(),
        'type': $('#docs-type').val(),
        'category': $('#docs-category').val()
    });
    get_docs.then(function (data) {
        updateDocsList(data.docs);
        // Check if the component is a dummy.
        check_dummy_component(client_id);
    });
}

/**
 * Fetches the docs list.
 */
function updateDocsList(docs) {

    const docs_listing_selector = $('#docs-listing');
    docs_listing_selector.html('');
    docs_listing_selector.html(
        ' <h4 class="main-con-header">Select Document </h4>' +
        ' <ul class="nav" id="docs-list-nav">\n' +
        ' </ul>');

    $('#docs-list-nav').empty();

    // Update Docs list.
    $.each(docs, function (index, doc) {
        $('#docs-list-nav')
            .append($('<li class="text-center" id="doc' + doc.cd_id + '">')
                .append('<a href="#" class="edit-ref" onclick="previewDoc(this); return false;"' +
                    ' data-name="' + doc.cd_name + '"data-id="' + doc.cd_id + '">' +
                    '<span class="editable" data-type="text" data-pk = "' + doc.cd_name +
                    '" data-url = "' + jsglobals.base_url + 'cpdv1/update_doc_name">' + doc.cd_name +
                    '</span></a></li>'))
    });
    setEditable();

    // Check If reference id is null
    if (undefined !== ref && -1 === active_id) {
        // Get a doc detail.
        const get_docs = makeAjaxCall('cpdv1/get_docs_details', {
                doc_id: ref,
            }),
            docs_format_selector = $('#docs-format');
        get_docs.then(function (data) {

            let docs_data = data.doc_details[0];
            $('#docs-category').val(docs_data.cd_category);
            $('#docs-type').val(docs_data.cd_type);
            docs_format_selector.val(docs_data.cd_format);
            docs_format_selector.trigger('change');
            $("a[data-id=" + ref + "]").trigger('click');
        });
    } else if (0 === active_id || -1 === active_id) {
        const obj = {"data-id": docs[0].cd_id};
        previewDoc(obj);
    }
}

/**
 * Preview/Play a documnet
 */
function previewDoc(obj) {

    active_id = $(obj).attr('data-id');

    const delete_doc_selector = $('#delete-docs');

    delete_doc_selector.attr('data-id', active_id);
    delete_doc_selector.attr('data-name', $(obj).attr('data-name'));

    const type = $('#docs-format').val(),
        canvas_container_selector = $('#canvas-container');
    let url;

    // Check if doc type is Video.
    if ('video' === type) {
        // URL to make request to.
        url = window.location.origin + '/tools/cpdv1/play_doc/?id=' + $(obj).attr('data-id') + '&format=video';

        // Display Video Document in Canvas.
        canvas_container_selector.html(
            '<h4 class="main-con-header">View Document</h4><video width="100%" height="100%" controls>\n' +
            '<source src="' + url + '" type="video/mp4" media="screen and (min-width:320px)">\n' +
            'Your browser does not support the video.\n' +
            '</video>'
        )
    }
    // Check if Type of documet is Audio.
    else if ('audio' === type) {
        // URL to make request to.
        url = window.location.origin + '/tools/cpdv1/play_doc/?id=' + $(obj).attr('data-id') + '&format=video';

        // Append audio player to Canvas.
        canvas_container_selector.html(
            '<h4 class="main-con-header">View Document</h4><audio style="margin: 25%" controls>\n' +
            '<source src="' + url + '" type="audio/mp3" media="screen and (min-width:320px)">\n' +
            'Your browser does not support the audio.\n' +
            '</audio>'
        )
    }
    // Check if type of document is PDF.
    else if ('pdf' === type) {
        // URL to make request to.
        url = window.location.origin + '/tools/cpdv1/play_doc/?id=' + $(obj).attr('data-id') + '&format=pdf';

        // Display PDF in Canvas.
        canvas_container_selector.html('<h4 class="main-con-header">View Document</h4>' +
            '<div align="center" class="attach_next_prev">\n' +
            '<button id="prev" class="btn btn-secondary prev"><i class="fa fa-arrow-left"></i>Prev</button>\n' +
            '<button id="next" class="btn btn-secondary next">Next<i class="fa fa-arrow-right"></i>' +
            '</button>&nbsp; &nbsp;\n' +
            '<span>Page: <span id="page_num"></span> / <span id="page_count"></span></span>\n' +
            '</div><canvas id="pdf-canvas"></canvas>');

        const canvas = document.getElementById('pdf-canvas');

        preview_pdf(url, canvas);

    }
    const send_docs_selector = $('#send-docs');

    // Set attribute for send button.
    send_docs_selector.attr('data-id', $(obj).attr('data-id'));
    send_docs_selector.attr('data-toggle', 'modal');
    send_docs_selector.attr('data-target', '#send-docs-dialog');
    send_docs_selector.attr('data-backdrop', 'static');
    send_docs_selector.attr('data-name', $(obj).attr('data-name'));
}