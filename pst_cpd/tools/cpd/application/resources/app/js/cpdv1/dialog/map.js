/**
 *
 * Google Map Dialog.
 *
 * @summary      DMD google map functions.
 * @description  This file contains functions for Displaying Google Map on a dialog,
 *               converting address to longitude and latitude coordinates,
 *               fetching and displaying route between source and destination.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */

$('#map-dialog').on('show.bs.modal', function (event) {

    // Set Default values.
    $('#map-start').val('');
    $('#map-end').val('');
    $('#map-mode').val('DRIVING');

    // Get the variable values.
    const long = $(event.relatedTarget).data('long'),
        lat = $(event.relatedTarget).data('lat');
    let origin_latlng;

    // Remove previously assigned data attributes.
    $(event.relatedTarget).removeData('name');
    $(event.relatedTarget).removeData('long');
    $(event.relatedTarget).removeData('lat');

    // Create DirectionService and DirectionRender class object.
    const directionsService = new google.maps.DirectionsService,
        directionsDisplay = new google.maps.DirectionsRenderer,

        myCenter = new google.maps.LatLng(lat, long),
        map = new google.maps.Map(document.getElementById('map'), {
            center: myCenter,
            zoom: 14,
        });

    // Add marker fro destination
    let marker = new google.maps.Marker({position: myCenter});
    marker.setMap(map);

    //  Get User's current location and set the address in the origin field.
    $('#map-current-location').click(function () {
        navigator.geolocation.getCurrentPosition(function (data) {
            const lat = data.coords.latitude,
                long = data.coords.longitude,
                map_start_selector = $('#map-start');

            origin_latlng = {lat: lat, lng: long};

            // Reverse geocoding.
            const geocoder = new google.maps.Geocoder;

            // Fetch Address from coordinates.
            geocoder.geocode({'location': origin_latlng}, function (results, status) {

                if ('OK' === status) {
                    map_start_selector.val(results[0].formatted_address);
                    map_start_selector.trigger('change');
                }
            });
        })
    });

    // Check if start or end position for map is changed.
    $('#map-start, #map-end, #map-mode').change(function () {
        directionsDisplay.setMap(map);

        // Display Directions.
        directionsService.route({
            origin: origin_latlng || $('#map-start').val(),
            destination: $('#map-end').val() || {lat: lat, lng: long},
            travelMode: $('#map-mode').val(),
        }, function (response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
            } else {
                toastr.error(status, 'No Route Found!!');
            }
        });
    })
});