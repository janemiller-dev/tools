/**
 *
 * Moves a Deal.
 *
 * @summary      Move a DMD Deal to Another Deal.
 * @description  This file moves a deal with one status to another deal status.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */

$(document).ready(function () {
    let current_status = '';

    $('#move-deal-dialog').on('show.bs.modal', function (event) {
        if ($(event.relatedTarget).attr('data-type')) {
            $('#deal-new-status').find('optgroup:not([label="Buyer Status"])').addClass('hidden');
            $('#deal-new-status').find('optgroup[label="Buyer Status"]').removeClass('hidden');
        } else {
            $('#deal-new-status').find('optgroup:not([label="Buyer Status"])').removeClass('hidden');
            $('#deal-new-status').find('optgroup[label="Buyer Status"]').addClass('hidden');
        }

        // Get the values and set appropriate fields.
        current_status = $(event.relatedTarget).data('current-status');
        $('#deal-current-status').val(current_status);
        $('#deal-new-status').val(current_status);
        $('#new-status-deal-id').val($(event.relatedTarget).data('id'));


    });

    // Validates fields and moves deal if valid.
    $('#move-deal-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            deal_new_status: {
                validators: {
                    different: {
                        field: 'deal_current_status',
                        message: "The current and new status cannot be same"
                    }
                }
            },
            deal_new_move_date: {
                validators: {
                    notZero: {
                        message: 'Required field'
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the form cpd
        const $form = $(e.target),
            fv = $form.data('formValidation'),

            // Get the form data and submit it.
            deal_status_data = $form.serialize(),

            // Moves a deal.
            move_deal = makeAjaxCall('cpdv1/move_deal', deal_status_data);
        move_deal.then(function (data) {
            $($form)[0].reset();
            fv.resetForm();
            $('#move-deal-dialog').modal('hide');
            refreshPage();
        });
    });
});