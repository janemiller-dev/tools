$(document).ready(function () {
    var doc_id = '';
    var ui_name = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-doc-dialog').on('show.bs.modal', function (event) {
        doc_id = $(event.relatedTarget).data('id');
        ui_name = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name')

        $('#doc-name').html(ui_name);
    });

    $(document).on('click', '#close-delete-doc-button', function () {
        $('#delete-doc-dialog').modal('hide');
    });

    // Removes selected document docuence.
    $(document).on('submit', '#delete-doc-form', function (e) {
        e.preventDefault();

        // Delete Document instance from the DB.
        const delete_doc = makeAjaxCall('cpdv1/delete_doc', {
            'doc_id': doc_id,
        });

        delete_doc.then(function (data) {
            toastr.success('Document Version Deleted!!', 'Success!!')
            $('#delete-doc-dialog').modal('hide');
            active_id = 0;
            refreshPage();
        });

        return false;
    });
});
