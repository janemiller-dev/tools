/**
 *
 * View Collaboration.
 *
 * @summary      Dialog used to display Collaboration for DMD clients.
 * @description  This file contains functions for Displaying Collaboration.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */

$(document).ready(function () {

    $('#view-collaboration-dialog').on('show.bs.modal', function (event) {
        viewCollaboration.init(event);
    });
});

/**
 *
 * View Notes available.
 */

let viewCollaboration = function () {
    // Initialize and get the DMD ID.
    let user_id,
        init = function () {
            const get_collaboration = makeAjaxCall('cpdv1/get_collaboration_data', {cpd_id: cpd_id});

            get_collaboration.then(function (data) {
                $('#collab-data-row').empty();

                // Format Drop down options.
                $.each(data.collab_data, function (index, ind_data) {
                    if ('format' === index) {
                        $('#collab-send-format, #collab-format-select').empty();
                        $.each(ind_data, function (key, format_data) {
                            $('#collab-send-format, #collab-format-select').append('<option ' +
                                'data-message="' + format_data.collab_format_message + '" ' +
                                'value="' + format_data.collab_format_id + '">'
                                + format_data.collab_format_name + '</option>')
                        })
                    }
                    // Recipient Drop down options
                    else if ('recipient' === index) {
                        $('#collab-' + index + ', #collab-send-recipient').empty();
                        $('#collab-' + index + ', #collab-send-recipient')
                            .append('<option disabled selected>--None--</option>');
                        $('#collab-' + index + ', #collab-send-recipient')
                            .append('<option value="-1">--Add new recipient--</option>');
                        $.each(ind_data, function (key, recipient_data) {
                            $('#collab-' + index + ', #collab-send-recipient').append('<option selected ' +
                                'data-message="' + recipient_data.collab_recipient_id + '" ' +
                                'value="' + recipient_data.collab_recipient_id + '">'
                                + recipient_data.collab_recipient_name + '</option>')
                        })
                    }
                    // Sender Drop down options.
                    else if ('sender' === index) {
                        $('#collab-' + index).empty();
                        $('#collab-sender').append('<option>' + ind_data[0].collab_sender_name + '</option>')
                        $('#send-collab-sender-name').val(ind_data[0].collab_sender_name);
                        user_id = ind_data[0].user_id;
                    }
                    // Drop down option for collaboration
                    else if ('collab' === index) {
                        $.each(ind_data, function (key, collab_data) {
                            let dummy_row = $('#dummy_collab_data').clone();
                            dummy_row.removeAttr('id');
                            dummy_row.removeClass('hidden');
                            dummy_row.find('#dummy-collab-label')
                                .text(collab_data.collab_sender_name + ' ' + collab_data.collab_created_at
                                    + ' ' + collab_data.collab_format_name);
                            dummy_row.find('#dummy-collab-text')
                                .addClass('collab-text')
                                .attr('data-id', collab_data.collab_id)
                                .text(collab_data.collab_data);
                            dummy_row.find('.delete-collab')
                                .attr('data-id', collab_data.collab_id);
                            dummy_row.find('.delete-collab')
                                .attr('data-name', collab_data.collab_sender_name);
                            dummy_row.find('.delete-collab')
                                .attr('data-format', collab_data.collab_format_name);

                            // Check if message is from collaborator or user.
                            if ('0' === collab_data.collab_collab_id) {
                                $('#collab-data-row').append(dummy_row);
                            } else {
                                dummy_row.css('background', '#fffbed');
                                dummy_row.find('#dummy-collab-text').removeClass('collab-text');

                                $('.collab-text[data-id="' + collab_data.collab_collab_id + '"]')
                                    .closest('.collab-box')
                                    .after(dummy_row);
                            }
                        })
                    }
                    // Topic Drop down options.
                    else {
                        $('#collab-' + index).empty();
                        $.each(ind_data, function (key, topic_data) {
                            $('#collab-' + index).append('<option ' +
                                'value="' + topic_data.collab_script_id + '">'
                                + topic_data.collab_script_name + '</option>')
                        })
                    }
                });

                // Append Dummy row if no row is present.
                if (0 === $('.collab-text').length) {
                    let dummy_row = $('#dummy_collab_data').clone();
                    $('.dummy-collab-view').remove();
                    dummy_row.removeAttr('id');
                    dummy_row.removeClass('hidden');
                    dummy_row.addClass('dummy-collab-view');
                    dummy_row.find('#dummy-collab-label')
                        .text('Sender Name - Datetime - Collaboration Format');
                    dummy_row.find('.delete-collab').removeClass('delete-collab');
                    $('#collab-data-row').append(dummy_row);
                }
            });

            //Add New recipient.
            $(document).off('change', '#collab-recipient');
            $(document).on('change', '#collab-recipient', function () {
                if ('-1' === $(this).val()) {
                    $('#add-recipient-dialog').modal('show');
                    $('#view-collaboration-dialog').modal('hide');
                }
            });

            // Add new collaboration.
            $(document).off('click', '#add-collab');
            $(document).on('click', '#add-collab', function () {
                const add_collab = makeAjaxCall('cpdv1/add_collab', {
                    id: cpd_id,
                    sender: $('#collab-sender').val(),
                    recipient: $('#collab-recipient').val(),
                    topic: $('#collab-topic').val(),
                    format: $('#collab-format-select').val(),
                    date_time: moment().format(" DD-MM-YY hh:mm A"),
                    order: $('.collab-box').length,
                    user_email: sessionStorage.userSigninName
                });

                // Append data to collaboration row.
                add_collab.then(function (data) {
                    $('.dummy-collab-view').remove();

                    let dummy_row = $('#dummy_collab_data').clone();
                    dummy_row.removeAttr('id');
                    dummy_row.removeClass('hidden');
                    dummy_row.find('#dummy-collab-label').text($('#collab-sender').val() + ' ' +
                        moment().format(" DD-MM-YY hh:mm A")
                        + ' ' + $('#collab-format-select option:selected').text());
                    dummy_row.find('#dummy-collab-text')
                        .addClass('collab-text')
                        .attr('data-id', data.collab_added);
                    $('#collab-data-row').append(dummy_row);
                })
            });

            // Update collaboration text.
            $(document).off('change', '.collab-text');
            $(document).on('change', '.collab-text', function () {
                const save_collab_text = makeAjaxCall('collab/save_collab_text', {
                    id: $(this).attr('data-id'),
                    val: $(this).val()
                });

                save_collab_text.then(function (data) {
                    console.log(data);
                })
            });

            // Send collaboration handler.
            $(document).off('click', '#send-collab-button');
            $(document).on('click', '#send-collab-button', function (data) {
                $('#collab-type').val('universal');

                getAccessToken(function (accessToken) {
                    if (accessToken) {
                        // Create a Graph client
                        var client = MicrosoftGraph.Client.init({
                            authProvider: (done) => {
                                // Just return the token
                                done(null, accessToken);
                            }
                        });

                        const token = (+new Date).toString(3);

                        const sendMail = {
                            message: {
                                subject: $('#collab-client-subject').val(),
                                body: {
                                    contentType: "HTML",
                                    content: $('#collab-client-content').val() +
                                    '<div style="text-align: center">' +
                                    '<a href="https://cpd.powersellingtools.com/tools/collab/univ?token=' + token +
                                    '&id=' + user_id + '&r=' + $('#collab-send-recipient').val() + '">' +
                                    'Click To Join Collaboration</a></div>'
                                },
                                toRecipients: [
                                    {
                                        emailAddress: {
                                            address: $('.collab-client-email').val()
                                        }
                                    }
                                ]
                            },
                            saveToSentItems: "true"
                        };

                        enableLoader();
                        client.api('/me/sendMail')
                            .post(sendMail)
                            .then(function () {

                                $('#send-collab-token').val(token);
                                const send_collab = makeAjaxCall('collab/send_collab', $('#collab-form').serialize());

                                send_collab.then(function (data) {
                                    $.unblockUI();

                                    toastr.success('Collaboration Email Sent!!');
                                    $('#send-univ-collab-dialog').modal('hide');
                                });
                            })
                            .catch(function (err) {
                                toastr.warning(err);
                            })

                    } else {
                        var error = {responseText: 'Could not retrieve access token'};
                        callback(null, error);
                    }
                });

            });

            $(document).on('click', '#add-univ-recipient', function () {
                $('.univ-client-collab-div')
                    .after('<div class="col-xs-10 col-xs-offset-2" style="padding:0; margin-top:5px !important">' +
                        '<input type="collab" class="form-control client-collab"></div>');
            });

            // Delete collaboration click handler.
            $(document).off('click', '.delete-collab');
            $(document).on('click', '.delete-collab', function () {
                const id = $(this).attr('data-id'),
                    name = $(this).attr('data-name'),
                    format = $(this).attr('data-format');

                $('#view-collaboration-dialog').modal('hide');
                $('#delete-univ-collab-dialog').modal('show');
                $('#delete-collab-name').text(name);
                $('#delete-format-name').text(format);

                $('#delete-collab-message').off('click');
                $('#delete-collab-message').click(function () {
                    const delete_collab = makeAjaxCall('collab/delete_collab', {
                        id: id
                    });

                    delete_collab.then(function () {
                        $('#delete-univ-collab-dialog').modal('hide');
                        toastr.success('Collaboration Message Deleted!', 'Success!!');
                    })
                })
            })
        };

    return {
        init: init,
    };
}();