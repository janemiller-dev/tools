let activeSeqId, sequence_added, story_added, activeStoryId, storyIdArray, isTriggered, text, client_id;

$(document).ready(function () {
    // Get the Client ID from the URL
    const path = window.location.pathname,
        components = path.split('/');
    client_id = components[components.length - 1];
    origin = components[2];

    loadPrev();

    sequence_added = 0;
    activeSeqId = 0;
    activeStoryId = 0;
    isTriggered = false;
    text = 'This is David from ______________. I am a Commercial Real Estate advisor.' +
        ' We are just checking to see if you have any challenges we might help you resolve.' +
        ' If not, we know how challenging keeping up with the changes in the market can become and' +
        ' whether or not you are in a position to take any action at this time,' +
        ' we are still here to help you think through the process so that when you do take action' +
        ' you are more prepared. Give us a call and we will schedule an advisory session in which' +
        ' we will see where you stand today, where you may want to be in the future and how we might help' +
        ' you get there. If we don’t hear from you we will endeavor to reach out until we do touch base.';

    // List of story sequence.
    const story_sequence_selector = $('#story-sequence'),
        story_version_selector = $('#story-version');

    if ('tsl' === origin) {
        $('#messsage_header').remove();

        $('#story_header').html(
            '<div class="col-xs-15 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Target Audience:</label>' +
            '<select class="form-control msg-menu" id="target-audience">' +
            '</select>' +
            '</div>' +
            '<div class="col-xs-15 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Suspect List Criteria</label>' +
            '<select class="form-control msg-menu" id="list-criteria">' +
            '</select>' +
            '</div>' +
            '<div class="col-xs-15 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Promotional Focus</label>' +
            '<select class="form-control msg-menu" id="promo-focus">' +
            '</select>' +
            '</div>' +
            '<div class="col-xs-15 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Client Type</label>' +
            '<select class="form-control" id="category-list">' +
            '<option value="r">Radish 1-3 M</option>' +
            '<option value="w">Wheat 3-5 M</option>' +
            '<option value="t">Tree 5+ M</option>' +
            '</select>' +
            '</div>' +
            '<div class="col-xs-15 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Event Confirmation</label>' +
            '<select class="form-control msg-menu" id="event-confirmation">' +
            '<option value="ics">Initial Contact Script</option>' +
            '<option value="ems">Exploratory Meeting Script</option>' +
            '<option value="pps">Proposal Presentation Script</option>' +
            '<option value="plms">Project Launch Meeting Script</option>' +
            '</select>' +
            '</div>');
    } else {
        console.log('Here');

        $('#story_header').html('<div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Client Type:</label>' +
            '<select class="form-control" id="category-list">' +
            '<option value="r">Radish</option>' +
            '<option value="w">Wheat</option>' +
            '<option value="t">Tree</option>' +
            '</select>' +
            '</div>' +
            '<div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Event Type:</label>' +
            '<select class="form-control col-xs-8" id="promo-focus">' +
            '<option value="exm">Exploratory Meeting</option>' +
            '<option value="prm">Presentation Meeting</option>' +
            '<option value="lam">Launch Meeting</option>' +
            '<option value="com">Contract Meeting</option>' +
            '<option value="clm">Closing Meeting</option>' +
            '<option value="clc">Closing Celebration</option>' +
            '</select>' +
            '</div>' +
            '<div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Story Type:</label>' +
            '<select class="form-control" id="story-type">' +
            '<option value="no">Negative Outcomes</option>' +
            '<option value="po">Positive Outcomes</option>' +
            '<option value="mo">Mixed Outcomes</option>' +
            '</select>' +
            '</div>' +
            '</div>');
    }

    // Check if the component is a dummy.
    check_dummy_component(client_id);

    // Check if request is from TSL page.
    if ('tsl' === origin) {
        refreshTSLdropdown().then(function () {
            refreshDMDDropdown().then(function (data) {
                refreshPage();
            })
        });
    } else {
        refreshPage();
    }

    story_sequence_selector.html('');
    story_sequence_selector.html(
        ' <h4 class="main-con-header"><b>Story Sequences</b></h4>' +
        ' <ul class="nav" id="msg-seq-nav">\n' +
        ' </ul>');

    // List of storys.
    story_version_selector.html('');
    story_version_selector.html(
        ' <h4 class="main-con-header"><b>Story Version</b></h4>' +
        ' <ul class="nav" id="msg-ver-nav">\n' +
        ' </ul>');

    $('#category-list, #promo-focus, #story-type').change(function () {
        activeSeqId = 0;
        $('.delete').removeAttr('data-toggle');
        $('#story-container').empty();
        $('#msg-ver-nav').empty();
        refreshPage();
    });


    // Adds a new story sequence.
    $('#create-story-sequence').click(function (e) {
        sequence_added = 1;
        e.preventDefault();
        let ta, criteria, focus;

        // Check if TA is present.
        if (null === $('#target-audience').val()) {
            toastr.error('No Target Audience Selected!!');
            return;
        } else
            (ta = (undefined !== $('#target-audience').val()) ? $('#target-audience').val() : 0)

        // Check if criteria is present.
        if (null === $('#list-criteria').val()) {
            toastr.error('No List Criteria Selected!!');
            return;
        } else
            (criteria = (undefined !== $('#list-criteria').val()) ? $('#list-criteria').val() : 0);

        // Check if focus is present.
        if (null === $('#promo-focus').val()) {
            toastr.error('No Promotional Focus Selected!!');
            return;
        } else
            focus = $('#promo-focus').val();


        $.ajax({
            url: jsglobals.base_url + 'cpdv1/create_story_sequence',
            dataType: 'json',
            data: {
                category: $('#category-list').val(),
                focus_type: focus,
                story_type: (undefined !== $('#story-type').val()) ? $('#story-type').val() : '',
                target_audience: ta,
                list_criteria: criteria,
                event_confirmation: (undefined !== $('#event-confirmation').val()) ? $('#event-confirmation').val() : ''
            },
            type: 'post',
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.story);
                return;
            } else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            refreshPage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });

    // Adds a new story.
    $('#create-story-version').click(function (e) {
        e.preventDefault();

        if (0 !== activeSeqId) {
            story_added = 1;
            $.ajax({
                url: jsglobals.base_url + 'cpdv1/create_story',
                dataType: 'json',
                data: {
                    id: activeSeqId,
                },
                type: 'post',
                error: ajaxError
            }).done(function (data) {

                if (data.status != 'success') {
                    toastr.error('To create one use Create Button.', 'No instance of Story Sequence Selected!!');
                    return;
                } else if (data.status == 'redirect') {
                    window.location.href = data.redirect;
                    return;
                }
                refreshPage();
            }).fail(function (jqXHR, textStatus) {
                toastr.error("Request failed: " + textStatus);
            }).always(function () {
            });
        } else {
            toastr.error('To create one use the Left Panel.', 'No Story Sequence Selected!!');
            return;
        }
    });

    // Updates the content of story.
    $('#save-msg-seq').click(function (e) {
        e.preventDefault();

        if (0 !== activeStoryId) {
            var msg_data = $('#story-form').serializeArray();
            msg_data.push({name: 'story_array', value: storyIdArray});

            $.ajax({
                url: jsglobals.base_url + 'cpdv1/update_story_content',
                dataType: 'json',
                type: 'post',
                data: msg_data,
                error: ajaxError
            }).done(function (data) {

                if (data.status !== 'success') {
                    toastr.error(data.story);
                    return;
                }
                else if (data.status === 'redirect') {
                    window.location.href = data.redirect;
                    return;
                } else if (data.content === false) {
                    toastr.error('No instance of Story version selected/Same Story Content', 'Unable to update content!!');
                    return;
                }
                toastr.success('Content Saved');
            }).fail(function (jqXHR, textStatus) {
                toastr.error("Request failed: " + textStatus);
            }).always(function () {
            });
        } else {
            toastr.error('To create one use Middle Panel.', 'No Story Version Selected!!')
        }
    });

    // Creates a new copy of story.
    $(document).on('click', '.copy_msg', function () {
        const id = $(this).attr('data-id');
        if (0 == id) {
            toastr.error('To create one use Compose button.', 'No Story Version Selected!!');
            return;
        }

        const copy_story = makeAjaxCall('cpdv1/add_copy_story', {
            id: id
        });

        copy_story.then(function (data) {
            refreshPage();
        });
    });

    // Deletes a copy of story.
    $(document).on('click', '.delete_msg', function () {
        const story_id = $(this).attr('data-id');

        if (0 == story_id) {
            toastr.error('To create one use Compose button.', 'No Story Version Selected!!');
            return;
        }

        const delete_story = makeAjaxCall('cpdv1/delete_story_copy', {
            id: story_id
        });

        delete_story.then(function (data) {
            refreshPage();
        }).catch(function (e) {
            console.log(e);
        })
    });
});

// Fetches the list of story sequence.
function refreshPage() {

    // Fetch the story sequence.
    $.ajax({
        url: jsglobals.base_url + 'cpdv1/get_story_seq',
        dataType: 'json',
        data: {
            category: $('#category-list').val(),
            focus_type: $('#promo-focus').val(),
            story_type: (undefined !== $('#story-type').val()) ? $('#story-type').val() : '',
            target_audience: (undefined !== $('#target-audience').val()) ? $('#target-audience').val() : 0,
            list_criteria: (undefined !== $('#list-criteria').val()) ? $('#list-criteria').val() : 0,
            event_confirmation: (undefined !== $('#event-confirmation').val()) ? $('#event-confirmation').val() : '',
        },
        type: 'post',
        error: ajaxError
    }).done(function (data) {

        if (data.status != 'success') {
            toastr.error(data.story);
            return;
        }
        else if (data.status == 'redirect') {
            window.location.href = data.redirect;
            return;
        }
        updateStorySeq(data.msg_seq);

    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
    });
}


// Updates the list of story sequence.
function updateStorySeq(msg_seqs) {
    $('#msg-seq-nav').empty();
    $('#delete-seq').removeAttr('data-toggle');

    $.each(msg_seqs, function (index, msg_seq) {
        $('#msg-seq-nav')
            .append($('<li class="text-center" id="seq' + msg_seq.tms_id + '">')
                .append('<a href="#" class="edit-ref" onclick="setupStorySequence(this); ' +
                    'return false;" data-name="' + msg_seq.tms_name + '"data-id="' + msg_seq.tms_id + '">' +
                    '<span class="editable" data-type="text" data-pk = "' + msg_seq.tms_id + '" ' +
                    'data-url = "' + jsglobals.base_url + 'cpdv1/update_msg_seq_name">' + msg_seq.tms_name +
                    '</span></a></li>'))
    });

    // Checks if no story sequence is selected.
    if (msg_seqs.length !== 0 && activeSeqId === 0) {
        var obj = {"data-id": msg_seqs[0].tms_id, "data-name": msg_seqs[0].tms_name};
        setupStorySequence(obj);
    }
    // Checks if new story is added.
    else if (story_added === 1) {
        story_added = 0;
        $('#story-form').html('');
        $('#seq' + activeSeqId).find('a:last').click()
    }
    // Checks if new story sequence is added.
    else if (sequence_added === 1) {
        sequence_added = 0;
        $('#msg-seq-nav li').last().children()[0].click();
        $('#story-form').html('');
    } else {

        // Set content for Story Container.
        const story_container_selector = $('#story-container');
        story_container_selector.html('');
        story_container_selector.html('<h4 class="main-con-header"><b>Story Content</b></h4>' +
            '<form id="story-form" style="margin:2%">' +
            '</form>');

        isTriggered = true;
        $('#msg-seq-nav').find('li a[data-id=' + activeSeqId + ']').trigger('click');

        for (let i = 0; i < 5; i++) {
            setContent(i, {
                'tmc_content': '',
                'tmc_id': 0
            }, text, 5);
        }
    }

    setEditable('bottom');
}

// Setup story list and delete sequence.
function setupStorySequence(obj) {
    // Check if this click event is triggered.
    false === isTriggered ? activeStoryId = 0 : '';

    isTriggered = false;
    activeSeqId = $(obj).attr('data-id');

    // Set attribute for delete button.
    const delete_seq_selector = $('#delete-seq');
    delete_seq_selector.attr('data-toggle', 'modal');
    delete_seq_selector.attr('data-target', '#delete-seq-dialog');
    delete_seq_selector.attr('data-backdrop', 'static');
    delete_seq_selector.attr('data-id', activeSeqId);
    delete_seq_selector.attr('data-name', $(obj).attr('data-name'));

    $('.active-seq').removeClass('active-seq');
    $('#seq' + activeSeqId).addClass('active-seq');

    // Adding content to the editor.
    $.ajax({
        url: jsglobals.base_url + 'cpdv1/get_story',
        dataType: 'json',
        type: 'post',
        data: {
            id: activeSeqId
        },
        error: ajaxError
    }).done(function (data) {

        if (data.status != 'success') {
            toastr.error(data.story);
            return;
        }
        else if (data.status == 'redirect') {
            window.location.href = data.redirect;
            return;
        }

        updateStory(data.story);
    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
    });
    $(".edit-ref").children().click(function (event) {
        event.stopPropagation();
    });
}

// Updates story list.
function updateStory(msgs) {
    $('#msg-ver-nav').empty();

    // Remove toggle data attribute for delete button.
    $('#delete-msg').removeAttr('data-toggle');
    $('#story-form').html('');

    // Updates the list of story sequence.
    $.each(msgs, function (index, msg) {
        $('#msg-ver-nav').append($('<li class="text-center" id="msg' + msg.tm_id + '">')
            .append('<a href="#" class="edit-ref" onclick="setupStory(this); return false;" ' +
                'data-name="' + msg.tm_name + '"data-id="' + msg.tm_id + '"><span class="editable"' +
                ' data-type="text" data-pk = "' + msg.tm_id + '" ' +
                'data-url = "' + jsglobals.base_url + 'cpdv1/update_msg_name">' + msg.tm_name + '</span></a></li>'))
    });

    // Checks if no story is selected.
    if (msgs.length != 0 && activeStoryId == 0) {
        var obj = {"data-id": msgs[0].tm_id, "data-name": msgs[0].tm_name};
        setupStory(obj);
    }
    // Checks if new story is added.
    else if (story_added == 1) {
        story_added = 0;
        $('#msg-ver-nav li').last().children()[0].click();
    } else {
        $('#msg-ver-nav').find('li a[data-id=' + activeStoryId + ']').trigger('click')
    }

    setEditable('bottom');
}

// Setup story content and delete button.
function setupStory(obj) {

    activeStoryId = $(obj).attr('data-id');
    $('.active-msg').removeClass('active-msg');
    $('#msg' + activeStoryId).addClass('active-msg');

    // Sets attribute for delete button.
    const delete_msg_selector = $('#delete-msg');

    if (0 !== activeStoryId) {
        delete_msg_selector.attr('data-toggle', 'modal');
        delete_msg_selector.attr('data-target', '#delete-msg-dialog');
        delete_msg_selector.attr('data-backdrop', 'static');
        delete_msg_selector.attr('data-id', activeStoryId);
        delete_msg_selector.attr('data-name', $(obj).attr('data-name'));
    }

    // Adding content to the editor.
    $.ajax({
        url: jsglobals.base_url + 'cpdv1/get_story_content',
        dataType: 'json',
        type: 'post',
        data: {
            id: activeStoryId
        },
        error: ajaxError
    }).done(function (data) {

        if (data.status !== 'success') {
            toastr.error(data.story);
            return;
        }
        else if (data.status === 'redirect') {
            window.location.href = data.redirect;
            return;
        }

        const story_container_selector = $('#story-container');
        story_container_selector.html('');
        story_container_selector.html('<h4 class="main-con-header"><b>Story Content</b></h4>' +
            '<form id="story-form" style="margin:2%">' +
            '</form>');

        const total_story = data.content.length;
        storyIdArray = [];

        $.each(data.content, function (index, content) {

            storyIdArray.push(content.tmc_id);

            if (content.tmc_content !== '') {
                text = content.tmc_content;
                setContent(index, content, text, total_story);
            } else {
                setContent(index, content, text, total_story);
            }
        });

    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
    });

    // Makes the content editable.
    $(".edit-ref").children().click(function (event) {
        event.stopPropagation();
    });
}

// Set the content for middle panel.
function setContent(index, content, text, total_story) {

    $('#story-form').append(
        $('<div>' +
            '<h5 style="margin-left: 1vw"><b>Story ' + (index + 1) +
            ' of ' + total_story + '</b></h5>' +
            '<div class="input-group">' +
            '<span class="btn btn-default input-group-addon copy_msg"' +
            ' data-id="' + activeStoryId + '"><i class="fa fa-files-o"></i></span>' +
            '<span class="input-group-addon btn btn-secondary tts">' +
            '<i class="fa fa-bullhorn"></i></span>' +
            '<textarea onkeyup="auto_grow(this)" class="form-control" ' +
            'name="text' + content.tmc_id + '">' + text + '</textarea>' +
            '<span class="input-group-addon btn btn-secondary stt">' +
            '<i class="fa fa-microphone"></i></span>' +
            '<span class="btn btn-default input-group-addon delete_msg"' +
            ' data-id="' + content.tmc_id + '">' +
            '<i class="fa fa-trash-o"></i></span>' +
            '</div>' +
            '</div>'));

    voiceRecognition();
    speechFunctionality();
}