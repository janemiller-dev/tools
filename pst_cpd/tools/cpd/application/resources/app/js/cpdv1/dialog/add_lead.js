/**
 *
 * Adds new Lead/Source for DMD.
 *
 * @summary       Adds new Lead/Source for an existing DMD instance.
 * @description  This file contains functions for adding new Lead/Source for an existing Client Project Dashboard Instance.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */

let type = '', name = 'Lead';
$(document).ready(function () {
    $('#add-lead-dialog').on('show.bs.modal', function (event) {

        // Hide the previous view modal.
        $('#view-lead-dialog').modal('hide');
        $('#view-source-dialog').modal('hide');
        $('#view-action-dialog').modal('hide');
        $('#view-whom-dialog').modal('hide');

        // Set DMD ID value.
        // $('#cpd-id').val(cpd_id);

        // Fetch the type of document.
        type = $(event.relatedTarget).data('type');
        $(event.relatedTarget).removeData('type');

        // Set the Title/Name.
        $('#add-lead-title').text('Add ' + type);
        $('#name-label').text(type + ' Name');
        $('#name-para').text('Enter the name of ' + type);
    });

    // Form validation.
    $('#add-lead-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            lead_name: {
                validators: {
                    notEmpty: {
                        message: "The name is required."
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the add new lead form.
        const $form = $(e.target),
            fv = $form.data('formValidation'),

            // Get the form data and submit it.
            lead_data = $form.serialize(),

            // Adds a new deal.
            add_lead = makeAjaxCall('cpdv1/add_lead', lead_data);

        add_lead.then(function (data) {
            toastr.success('Lead Added!!', 'Success!!');
            fv.resetForm();
            $($form)[0].reset();

            get_all_criteria();
            $("#add-client-form").formcache("outputCache");
            $('#add-lead-dialog, #view-lead-dialog').modal('hide');
            refreshPage();
        });
    });
});