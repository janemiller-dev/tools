$(document).ready(function () {
    $('#view-call-notes-dialog').on('show.bs.modal', function (event) {
        view_call_notes.init(event);
    });

    view_call_notes.add_event();
});

// View Notes available.
let view_call_notes = function () {

    var max_seq = 0, client_id;

    // Initialize and get the DMD ID.
    let init = function (event) {
            client_id = $(event.relatedTarget).data('id');

            if (0 === client_id) {
                $('#view-call-notes-dialog :button').attr('disabled', true);
                $('.close-button').attr('disabled', false);
            }

            max_seq = 0;

            // Clear any previous values.
            $('.additional-row').remove();
            $('.notes-input').val('');
            $('.notes-topic').val('');
            $('.call-notes-date').val('');

            // Fetch the Notes Content.
            const notes_data = makeAjaxCall('tsl/get_notes_content', {
                'id': client_id
            });

            notes_data.then(function (data) {
                let dummy_content;

                $.each(data.notes_content, function (index, note) {

                    // Check if this row exists or not.
                    if (0 === $('#notes_content' + note.tn_seq).length) {
                        dummy_content = $('#view-notes-div').clone();

                        dummy_content.find('textarea')
                            .attr('data-seq', note.tn_seq)
                            .attr('id', 'notes_content' + note.tn_seq);

                        dummy_content.find('.notes-label')
                            .text('Call Notes ');

                        dummy_content.find('#notes-topic0')
                            .attr('data-seq', note.tn_seq)
                            .attr('id', 'notes-topic' + note.tn_seq)
                            .val('');

                        dummy_content.find('#notes-date0')
                            .attr('data-seq', note.tn_seq)
                            .attr('id', 'notes-date' + note.tn_seq)
                            .val('');

                        dummy_content.find('#notes-action0')
                            .attr('data-seq', note.tn_seq)
                            .attr('id', 'notes-action' + note.tn_seq)
                            .val('');

                        dummy_content.find('#next-call-date0')
                            .attr('data-seq', note.tn_seq)
                            .attr('id', 'next-call-date' + note.tn_seq)
                            .val('');

                        dummy_content.find('.topic-label')
                            .text('Call Purpose ');

                        $('#call-notes-row').append('<div class="col-xs-12 additional-row" style="margin-top: 10px">'
                            + '<hr>' + dummy_content.html() + '</div>');

                        max_seq = note.tn_seq;
                    }

                    // Set the content.
                    $('#notes_content' + max_seq).val(note.tn_content);
                    $('#notes-topic' + max_seq).val(note.tn_topic);
                    $('#notes-action' + max_seq).val(note.tn_action);
                    $('#notes-date' + max_seq).val(moment(note.tn_datetime).format('DD-MMM-YY, h:mm A'));
                    $('#next-call-date' + max_seq).val(moment(note.tn_next_call).format('DD-MMM-YY, h:mm A'));
                });
                store_data();

                // Set format for Date time picker.
                const next_call_selector = $('.next-call');
                next_call_selector.datetimepicker({
                    format: 'DD-MMM-YY, h:mm A'
                });
            });
        },

        // Stores Notes data.
        store_data = function () {
            $('.notes').unbind('change');
            $('.notes').change(function () {
                let value = $(this).val();

                // Check if the field is for next call date.
                $(this).hasClass('next-call') ? value = moment(value).format('YYYY-MM-DD HH:mm:ss') : '';

                makeAjaxCall('tsl/update_notes_content', {
                    'id': client_id,
                    'col': $(this).data('name'),
                    'val': value,
                    'seq': parseInt($(this).data('seq'))
                });
            });
        },

        //     Adds new column to the event List.
        add_event = function () {
            // Add a new event to the list.
            $('#add-notes').click(function () {
                max_seq = parseInt(max_seq) + 1;

                dummy_content = $('#view-notes-div').clone();

                dummy_content.find('textarea')
                    .attr('data-seq', max_seq)
                    .attr('id', 'notes_content' + max_seq)
                    .val('');

                dummy_content.find('.notes-label')
                    .text('Call Notes ');

                dummy_content.find('#notes-topic0')
                    .attr('data-seq', max_seq)
                    .attr('id', 'notes_topic' + max_seq)
                    .val('');

                dummy_content.find('#notes-date0')
                    .attr('data-seq', max_seq)
                    .attr('id', 'notes-date' + max_seq)
                    .val('');

                dummy_content.find('#notes-action0')
                    .attr('data-seq', max_seq)
                    .attr('id', 'notes-action' + max_seq)
                    .val('');

                dummy_content.find('#next-call-date0')
                    .attr('data-seq', max_seq)
                    .attr('id', 'next-call-date' + max_seq)
                    .val('');

                dummy_content.find('.topic-label')
                    .text('Call Purpose ');

                // Append to the last of List.
                $('#call-notes-row').append('<div class="col-xs-12 additional-row" style="margin-top: 10px">'
                    + '<hr>' + dummy_content.html() + '</div>');

                // Set format for Date time picker.
                const next_call_selector = $('.next-call');
                next_call_selector.datetimepicker({
                    format: 'DD-MMM-YY, h:mm A'
                });

                store_data();
            });
        };

    return {
        init: init,
        add_event: add_event
    };
}();