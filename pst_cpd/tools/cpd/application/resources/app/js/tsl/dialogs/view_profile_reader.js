$(document).ready(function () {
    $('#view-profile-reader-dialog').on('show.bs.modal', function (event) {
        view_profile_reader.init();
    });
});

let view_profile_reader = function () {
    // Initialize and get the DMD ID.
    let init = () => {
            fetch_answer();

            $(document).off('click', '#profile-readout-type li');
            $(document).on('click', '#profile-readout-type li', function () {
                fetch_answer();
            })
        },
        fetch_answer = () => {
            $('.profile-readout-tab .active p').empty();
            $('#profile-readout-type li').eq(0).attr('data-id', $('#profile-phase option').eq(0).val());
            $('#profile-readout-type li').eq(1).attr('data-id', $('#profile-phase option').eq(1).val());

            const get_profile = makeAjaxCall('tsl/get_profile_data', {
                id: activeProfileId,
                section: $('#profile-readout-type li.active').attr('data-id')
            });

            get_profile.then(function (data) {
                let label_index = -1;
                $.each(data.profile_data, function (index, profile) {
                    (profile.tpq_content.match(/Question 1/) !== null) ? label_index++ : '';
                    $('.profile-readout-tab .active p').eq(label_index).append(profile.answer);
                });
            });

        };
    return {
        init: init
    };
}();