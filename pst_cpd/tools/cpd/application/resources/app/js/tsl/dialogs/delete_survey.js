$(document).ready(function () {
    var bus_id = '';
    var ui_name = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-bus-dialog').on('show.bs.modal', function (event) {
        bus_id = $(event.relatedTarget).data('id');
        ui_name = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name')

        $('#bus-name').html(ui_name);
        $('#view-bus-dialog').modal('hide');
    });

    $(document).on('click', '#close-delete-bus-dialog', function () {
        $('#delete-bus-dialog').modal('hide');
    });

    // Deletes Buyer Survey.
    $(document).on('submit', '#delete-bus-form', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_bus',
            dataType: 'json',
            type: 'post',
            data: {
                bus_id: bus_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            toastr.success('Survey Instance Deleted.', 'Success!!');
            $('#delete-bus-dialog').modal('hide');

            survey_added = 1;
            refreshBusPage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
        return false;
    });
});
