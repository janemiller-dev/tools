let client_id, active_id = -1;
$(document).ready(function () {

    // Get the Client ID from the URL
    const path = window.location.pathname,
        components = path.split('/');

    client_id = components[components.length - 1];
    origin = components[2];

    const searchParams = new URLSearchParams(window.location.search),
        tsl_id = searchParams.get('id');

    loadPrev();

    // Date Time Picker
    $('.datepicker').datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
        onSelectTime: function (dp, $input) {
            console.log($('.datepicker').val());
        }
    });

    // List of message sequence.
    const message_sequence_selector = $('#component-sequence'),
        message_version_selector = $('#component-version');

    // Check if the component is a dummy.
    check_dummy_component(client_id);

    $('#compose-notice').click(function () {
        const add_notice = makeAjaxCall('tsl/add_notice', {
            client_id: client_id,
            name: $('#notice-date').val() + '\n' +
            $('#notice-type option:selected').text() + ', ' +
            $('#notice-topic option:selected').text() + ', ' +
            $('#notice-delivery option:selected').text() + ', ' +
            $('#notice-format option:selected').text()
        });

        add_notice.then(function (data) {
            refreshPage();
            toastr.success('Notice Added.');
        })
    });

    $('#save-notice').click(function () {
        var data = $('#response-form').serializeArray();
        data.push({name: 'notice_id', value: active_id});
        data.push({name: 'count', value: max});
        data.push({name: 'client_id', value: client_id});

        const save_notice = makeAjaxCall('tsl/save_notice_content', data);
        save_notice.then(function (data) {
            toastr.success('Content Saved.', 'Success!!');
            console.log(data);
        })
    });

    refreshPage();
});

function refreshPage() {
    const get_outline = makeAjaxCall('tsl/get_notices_outline', {
        'client_id': client_id,
    });

    get_outline.then(function (data) {
        const content = (data.outlines[0].tno_content);

        // Display first outline, if no outline is selected.
        $('#outline-container').html('<h4 class="main-con-header"><b>Notice Outline</b></h4><br>' +
            '<form id="outline-form" style="word-break: break-word">' +
            '<input type="hidden" id="target_audience" name="target_audience" >' +
            '<input type="hidden" id="criteria" name="criteria" >' +
            '<input type="hidden" id="promo_focus" name="promo_focus">' +
            '<input type="hidden" id="promo_format" name="promo_format">' +
            '<input type="hidden" id="client_type" name="client_type">' +
            '<input id="y" value="' + content +
            '" type="hidden" name="outline_content">\n' +
            '<trix-editor id="trix-editor" class="editor" input="y" name="tsl_promo_editor"></trix-editor>' +
            '</form>');

        trixButtons();
        $('trix-editor').attr('contenteditable', false);

        // Set up the content for Notice Builder.
        let z = $('#y').val().replace(/\[\[/g, '<h4 class="main-con-header">').replace(/\]\]/g, '</h4>')
                .replace(/\(\(/g, '<label style="margin-left: 25px; margin-top: 10px">').replace(/\)\)/g, '</label>\n' +
                '<div class="input-group" style="margin-left: 30px;">' +
                '<span class="input-group-addon btn btn-secondary tts">' +
                '<i class="fa fa-bullhorn"></i></span><textarea onkeyup="auto_grow(this)"' +
                ' class="form-control custom-control ans" placeholder="Click Microphone icon and speak or start typing."' +
                ' rows="2" cols="10"></textarea>' +
                '<span class="input-group-addon btn btn-secondary stt">' +
                '<i class="fa fa-microphone"></i>' +
                ' </span></div>' +
                '<span id=\'span_ans\' class=\'hidden\'></span>')

                .replace(/{{/g, '<label style="margin-top: 10px; font-weight: normal">').replace(/}}/g, '</label>\n' +
                ' <div class="input-group">' +
                '<span class="input-group-addon btn btn-secondary tts">' +
                '<i class="fa fa-bullhorn"></i>' +
                '</span><textarea onkeyup="auto_grow(this)" ' +
                'class="form-control custom-control ans" placeholder="Click Microphone icon and speak or start typing."' +
                ' rows="2" cols="70"></textarea>' +
                '<span class="input-group-addon btn btn-secondary stt">' +
                '<i class="fa fa-microphone"></i>' +
                '</span></div>' +
                '<span id=\'span_ans\' class=\'hidden\'></span>').replace(/<br>/g, ''),

            x = z.trim().match(/(<label(.*?)<\/label>)|(<h4 class="main-con-header">(.*?)<\/h4>)|(<([^>]+)>)/g),
            t = '';
        $.each(x, function (index, text) {
            t = t + text;
        });

        const heading = t.match(/<h4 class="main-con-header">(.*?)<\/h4>/);
        t = t.replace(/<h4 class="main-con-header">(.*?)<\/h4>/, '');

        const canvas_container_selector = $('#notice-container');
        canvas_container_selector.html('');
        canvas_container_selector.html( heading[0] +
            '<form id="response-form" style="margin:5px; font-size: 17px" class="form-horizontal">' +
            t +
            '</form>'
        );

        max = 0;
        $.each($('.ans'), function (index, element) {
            $(this).attr('name', 'ans' + (index + 1));
            $(this).attr('id', 'ans' + (index + 1));
            max = index + 1;
        });

    });

    const get_notices = makeAjaxCall('tsl/get_notice_instance', {
        'client_id': client_id
    });

    get_notices.then(function (data) {
        $('#notice-instance').empty();
        $.each(data.notices, function (index, notice) {
            $('#notice-instance').append('<li class="text-center" id="li' + notice.tn_id + '"><a href="#" ' +
                'onclick="setupAnswer(this); return false;" data-id="' + notice.tn_id + '">' + notice.tn_name + '</a></li>')
        });

        // Check if there's any active.
        if (-1 === active_id) {
            $('#notice-instance li:first').find('a').click()
        }
    })
}

function setupAnswer(obj) {
    active_id = $(obj).attr('data-id');

    $('textarea').val('');
    $('.active').removeClass('active');
    $('#li' + active_id).addClass('active');

    const get_answers = makeAjaxCall('tsl/get_notice_answer', {
        'client_id': client_id,
        'notice_id': active_id
    });

    get_answers.then(function (data) {
        $.each(data.answer, function (index, answer) {
            $('#ans' + answer.tna_question_number).val(answer.tna_content);
        })
    })
}
