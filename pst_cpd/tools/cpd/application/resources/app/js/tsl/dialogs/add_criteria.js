var promo_deal_id = '';
$(document).ready(function () {

    $('#add-criteria-dialog').on('show.bs.modal', function (event) {
        $('#view-criteria-dialog').modal('hide');
        $('#view-tsl-criteria-dialog').modal('hide');
        $('#client-criteria-dialog').modal('hide');
        promo_deal_id = $(event.relatedTarget).data('id');
    });

    // Form validation.
    $('#add-criteria-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            criteria_name: {
                validators: {
                    notEmpty: {
                        message: "The Deal Name is required."
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the add new criteria form.
        var $form = $(e.target);
        var fv = $form.data('formValidation');

        // Check if TSL ID is empty.
        ('undefined' === typeof(tsl_id)) ? tsl_id = promo_deal_id : '';

        // Get the form data and submit it.
        var criteria_data = $form.serializeArray();
        criteria_data.push({'name': 'tsl_id', 'value': tsl_id});

        $.ajax({
            url: jsglobals.base_url + 'tsl/add_criteria',
            dataType: 'json',
            type: 'post',
            data: criteria_data,
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            } else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('Criteria Added.', 'Success!!');
            fv.resetForm();
            $('#add-criteria-dialog').modal('hide');
            refreshPage();
            get_all_criteria();
            $("#add-client-form").formcache("outputCache");

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });
});




