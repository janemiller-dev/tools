var id = '';
var ui_name = '';
$(document).ready(function () {
    // When the dialog is displayed, set the current instance ID.
    $('#delete-email-dialog').on('show.bs.modal', function (event) {
        id = $(event.relatedTarget).data('id');
        ui_name = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name')

        $('#email-name').html(ui_name);
    });

    $(document).on('click', '#close-delete-email-button', function () {
        $('#delete-email-dialog').modal('hide');
    });
});

// Removes selected  email instance.
$(document).on('submit', '#delete-email-form', function (e) {
    e.preventDefault();
    $.ajax({
        url: jsglobals.base_url + 'tsl/delete_email',
        dataType: 'json',
        type: 'post',
        data: {
            id: id
        },
        error: ajaxError
    }).done(function (data) {

        if (data.status != 'success') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
            window.location.href = data.redirect;
            return;
        }

        $('#delete-email-dialog').modal('hide');
        refreshPage();
    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
    });

    return false;
});
