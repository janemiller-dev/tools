$(document).ready(function () {
    var outline_id = '';
    var version = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-outline-dialog').on('show.bs.modal', function (event) {
        outline_id = $(event.relatedTarget).data('id');
        version = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name')

        $('#outline-id').val(outline_id);
        $('#delete-outline-version').html('Outline V' + version);
        $('#view-script-outline-dialog').modal('hide');
    });

    // Deletes Buyer's survey Outline.
    $(document).on('submit', '#delete-script-outline-form', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_script_outline',
            dataType: 'json',
            type: 'post',
            data: {
                outline_id: outline_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('Script Outline Deleted.', 'Success!!');
            $('#delete-outline-dialog').modal('hide');
            refreshPage();

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
        return false;
    });
});
