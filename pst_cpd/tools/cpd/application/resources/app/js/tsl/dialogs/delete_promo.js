$(document).ready(function () {
    let promo_id = '',
        ui_name = '',
        type = '',
        random_name = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-promo-dialog').on('show.bs.modal', function (event) {

        promo_id = $(event.relatedTarget).data('id');
        ui_name = $(event.relatedTarget).data('name');
        type = $(event.relatedTarget).data('type');
        random_name = $(event.relatedTarget).data('random');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name');

        if (type == 'sa') {
            $(event.relatedTarget).removeData('type');
            $(event.relatedTarget).removeData('random');
        }

        $('#promo-name').html(ui_name);
        $('#view-promo-dialog').modal('hide');
    });

    $(document).on('click', '#close-delete-promo-dialog', function () {
        $('#delete-promo-dialog').modal('hide');
    });

    // Removes promo document instance.
    $(document).on('submit', '#delete-promo-form', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_promo',
            dataType: 'json',
            type: 'post',
            data: {
                promo_id: promo_id,
                type: type,
                random_name: random_name
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Unable to Delete Document!')
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('Document Deleted.', 'Success!!');
            $('#delete-promo-dialog').modal('hide');
            try {
                promo_added = 1;
                refreshPage();
            } catch (e) {
                refreshSA();
            }
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
        return false;
    });
});
