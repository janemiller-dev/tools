$(document).ready(function () {
    var msg_id = '';
    var ui_name = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-msg-dialog').on('show.bs.modal', function (event) {
        msg_id = $(event.relatedTarget).data('id');
        ui_name = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name')

        $('#msg-name').html(ui_name);
    });

    $(document).on('click', '#close-delete-msg-button', function () {
        $('#delete-msg-dialog').modal('hide');
    });

    // Removes selected message msguence.
    $(document).on('submit', '#delete-msg-form', function (e) {
        e.preventDefault();

        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_msg',
            dataType: 'json',
            type: 'post',
            data: {
                msg_id: msg_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('Message Version Deleted!!', 'Success!!')
            $('#delete-msg-dialog').modal('hide');
            activeMessageId = 0;
            refreshPage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });

        return false;
    });
});
