var activeOutlineId, activeEmailId, outline_added, email_added, client_id, obj, email_content, email_content_updated,
    type_selected, version_selected, max, origin, ref, req_type, component_name;

$(document).ready(function () {

    // Get the Client ID from the URL
    const path = window.location.pathname,
        components = path.split('/');
    client_id = components[components.length - 1];
    origin = components[2];
    req_type = components[3];

    ref = window.location.search.split('?ref=')[1];

    // Check if ref is defined or not.
    if (undefined !== ref) {
        const ref_array = ref.split('?');
        ref = ref_array[0];
    }

    $('#email_header').empty();

    // Check if request is from TSL, if so change the header for Email.
    if ('tsl' === origin) {
        $('#tsl-email-heading').html('<b>CONFIRMATION BUILDER</b>');
        component_name = 'Confirm';

        $('#email_header').html(
            '<div class="col-xs-15 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Confirmation Purpose</label>' +
            // '<select class="form-control email-menu" id="target-audience">' +
            '<select class="form-control email-menu" >' +
            '<option>Confirm a Meeting</option>' +
            '<option>Confirm a Presentation</option>' +
            '<option>Confirm a Future Call</option>' +
            '<option>Confirm a Next Action</option>' +
            '<option>Confirm an Event</option>' +
            '</select>' +
            '</div>' +
            '<div class="col-xs-15 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Confirmation Assignment</label>' +
            // '<select class="form-control email-menu" id="list-criteria">' +
            '<select class="form-control email-menu" id="">' +
            '<option>Three Questions</option>' +
            '<option>Complete Survey</option>' +
            '<option>Complete Profile</option>' +
            '<option>Send Link</option>' +
            '<option>Review Document</option>' +
            '</select>' +
            '</div>' +
            // '<div class="col-md-3 script-box" id="promo-focus-parent" data-toggle="popover"' +
            // 'data-placement="bottom" data-html="true">' +
            // '<label class="col-xs-12 text-center">Confirmation Delivery</label>' +
            // '<input type="text" class="col-xs-12 form-control" id="promo-focus">' +
            // '</div>' +
            '<div class="col-xs-15 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Confirmation Delivery</label>' +
            '<select class="form-control email-menu" id="email-event">' +
            '<option value="mp">Meeting Prep</option>' +
            '<option value="pp">Presentation Prep</option>' +
            '<option value="lp">Launch Prep</option>' +
            '<option value="map">Marketing Prep</option>' +
            '<option value="cp">Contract Prep</option>' +
            '<option value="cmp">Completion Prep</option>' +
            '</select>' +
            '</div>' +
            '<div class="col-xs-15 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Confirmation Set</label>' +
            '<select class="form-control email-menu" id="email-type">' +
            '</select>' +
            '</div>'+
            '<div class="col-xs-15 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Confirmation Outline</label>' +
            '<select class="form-control email-menu" id="email-type">' +
            '</select>' +
            '</div>'
        )
    } else {
        $('#tsl-email-heading').html('<b>EMAIL BUILDER</b>');
        component_name = 'Email';

        $('#email_header').html(
            '<!--Select box for Client Type-->' +
            '<div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Client Type:</label>' +
            '<select class="form-control" id="client-type">' +
            '<option value="r">Radish</option>' +
            '<option value="w">Wheat</option>' +
            '<option value="t">Tree</option>' +
            '</select>' +
            '</div>' +

            '<!-- Select box for event type-->' +
            '<div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Event Type:</label>' +
            '<select class="form-control email-select" id="email-event">' +
            '<option value="em">Meeting</option>' +
            '<option value="pm">Presentation Meeting</option>' +
            '<option value="lm">Launch Meeting</option>' +
            '<option value="cm">Contract Meeting</option>' +
            '<option value="clm">Closing Meeting</option>' +
            '<option value="cc">Closing Celebration</option>' +
            '</select>' +
            '</div>' +

            '<!--Select box for email type-->' +
            '<div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Email Type:</label>' +
            '<select class="form-control" id="email-type">' +
            '</select>' +
            '</div>'
        )
    }

    // Check if request is from TSL page.
    if ('tsl' === origin) {
        refreshTSLdropdown().then(function () {
            refreshDMDDropdown().then(function (data) {
                refreshPage();
            })
        });
    } else {
        // refreshDMDDropdown().then(function (data) {
        refreshPage();
        // })
    }

    // Enable Voice Recognition.
    voiceRecognition();

    var id = window.location.search.split('?id=')[1];
    loadPrev();

    activeEmailId = 0;
    // Display outline for the email.
    $('#outline-container').html('');
    $('#canvas-container').html('');

    // To check if a new email is added.
    outline_added = 0;
    activeOutlineId = 0;
    type_selected = 0;
    version_selected = 0;

    // List of Buyer's Email
    const container_listing_selector = $('#container-listing');
    container_listing_selector.html('');
    container_listing_selector.html(
        ' <h4 class="main-con-header"><strong>' + component_name + ' Version</strong></h4>' +
        ' <ul class="nav" id="tsl-email-nav">\n' +
        ' </ul>');

    $('.email-select').change(function () {
        localStorage.setItem('active_email_event', $('#email-event').val());
        localStorage.setItem('active_email_purpose', $('#email-purpose').val());
        localStorage.setItem('active_email_outline_id', 0);

        activeOutlineId = 0;
        refreshPage();
    });

    // Creates a new instance of email based on email outline.
    $('#compose-email').click(function (e) {
        e.preventDefault();
        email_added = 1;

        $.ajax({
            url: jsglobals.base_url + 'tsl/add_email_instance',
            dataType: 'json',
            data: {
                outline_id: $('#email-type').val()
            },
            type: 'post',
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error('To create one use the save button on the left.', 'No Instance of Outline.')
                return;
            } else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            toastr.success('Email Instance Added!!', 'Success');
            refreshPage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });

    // Saves the outline and generates questions from outlines.
    $('#save-email-outline').click(function (e) {
        $('#email_type').val($('#email-type').val());
        $('#email_purpose').val($('#email-purpose').val());
        $('#email_event').val($('#email-event').val());
        outline_added = 0;
        activeOutlineId = 0;

        var tsl_update_data = $('#outline-form').serializeArray();
        // Check if TA is present.
        if (null === $('#target-audience').val()) {
            toastr.error('No Location Selected!!');
            return;
        }
        else
            tsl_update_data.push({name: 'ta', value: $('#target-audience').val()});

        // Check if criteria is present.
        if (null === $('#list-criteria').val()) {
            toastr.error('No List Criteria Selected!!');
            return;
        }
        else
            tsl_update_data.push({name: 'criteria', value: $('#list-criteria').val()});

        // Check if focus is present.
        if (null === $('#promo-focus').val()) {
            toastr.error('No Promotional Focus Selected!!');
            return;
        } else
            tsl_update_data.push({name: 'focus', value: $('#promo-focus').val()});

        e.preventDefault();

        $.ajax({
            url: jsglobals.base_url + 'tsl/save_email_outline',
            dataType: 'json',
            type: 'post',
            data: tsl_update_data,
            error: ajaxError
        }).done(function (data) {
            if (data.status !== 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            toastr.success('Outline Saved!');
            refreshPage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });

    // Saves client response.
    $('#save-email').off('click');
    $('#save-email').click(function (e) {
        if (0 !== parseInt(activeEmailId)) {
            var data = $('#response-form').serializeArray();
            data.push({name: 'client_id', value: client_id});
            data.push({name: 'email_id', value: activeEmailId});
            data.push({name: 'count', value: max});
            e.preventDefault();

            $.ajax({
                url: jsglobals.base_url + 'tsl/set_email_content',
                dataType: 'json',
                type: 'post',
                data: data,
                error: ajaxError
            }).done(function (data) {
                if (activeEmailId === 0) {
                    toastr.error('To create one use compose button.', 'No Instance of email.')
                    return;
                }
                else if (data.status !== 'success') {
                    toastr.error(data.message);
                    return;
                }
                else if (data.status === 'redirect') {
                    window.location.href = data.redirect;
                    return;
                }
                toastr.success('Response Saved!');
                email_content_updated = 1;
                refreshPage();
            }).fail(function (jqXHR, textStatus) {
                toastr.error("Request failed: " + textStatus);
            }).always(function () {
            });
        } else {
            toastr.error('Please create one using compose button', 'No Instance of email version selected.')
        }
    });

    // Check if any drop down menu is changed, fetch outline version again.
    $('.email-menu').change(function () {
        activeOutlineId = 0;
        refreshPage();
    });

    // Checks if no instance of email is present.
    $('#send-email, #delete-email').click(function (e) {
        if (($(this).attr('data-toggle') == undefined) || (0 == activeEmailId)) {
            toastr.error('To create one use compose button.', 'No Instance of email version selected.');
            e.stopPropagation();
        }
    });

    // Adds Promo version to archive list.
    $('#add-email-archive').click(function (data) {
        const add_promo_archive = makeAjaxCall('tsl/add_archive', {
            'promo_id': activeEmailId,
            'type': 'email',
            'asset_id': client_id
        });

        add_promo_archive.then(function (data) {
            toastr.success('Component Added to Archive!!');
        });
    });

    // Propagates email content across clients.
    $('#propagate-email').click(function () {
        const propagate_email = makeAjaxCall('tsl/propagate_email', {
            client_id: client_id,
            email_id: activeEmailId
        });

        propagate_email.then(function (data) {
            toastr.success('Content Propagated!!', 'Success!!');
        })
    })
});

// Fetches questions and puts them in a form.
function setupEmail(obj) {

    activeOutlineId = $(obj).attr('data-id');

    $version = $(obj).attr('data-version');
    let question_input = '<h3><b>Email Questions</b></h3>',

        z = $('#y').val().replace(/\[\[/g, '<h4 class="main-con-header">').replace(/\]\]/g, '</h4>')
            .replace(/\(\(/g, '<label style="margin-left: 25px; margin-top: 10px">').replace(/\)\)/g, '</label>\n' +
                '<div class="input-group" style="margin-left: 30px;">' +
                '<span class="input-group-addon btn btn-secondary tts">' +
                '<i class="fa fa-bullhorn"></i></span><textarea onkeyup="auto_grow(this)"' +
                ' class="form-control custom-control ans" placeholder="Click Microphone icon and speak or start typing."' +
                ' rows="1" cols="10"></textarea>' +
                '<span class="input-group-addon btn btn-secondary stt">' +
                '<i class="fa fa-microphone"></i>' +
                ' </span></div>' +
                '<span id=\'span_ans\' class=\'hidden\'></span>')

            .replace(/{{/g, '<label style="margin-top: 10px">').replace(/}}/g, '</label>\n' +
                ' <div class="input-group">' +
                '<span class="input-group-addon btn btn-secondary tts">' +
                '<i class="fa fa-bullhorn"></i>' +
                '</span><textarea onkeyup="auto_grow(this)" ' +
                'class="form-control custom-control ans" placeholder="Click Microphone icon and speak or start typing."' +
                ' rows="1" cols="70"></textarea>' +
                '<span class="input-group-addon btn btn-secondary stt">' +
                '<i class="fa fa-microphone"></i>' +
                '</span></div>' +
                '<span id=\'span_ans\' class=\'hidden\'></span>').replace(/<br>/g, ''),

        x = z.trim().match(/(<label(.*?)<\/label>)|(<h4 class="main-con-header">(.*?)<\/h4>)|(<([^>]+)>)/g),
        t = '';
    $.each(x, function (index, text) {
        t = t + text;
    });

    const heading = t.match(/<h4 class="main-con-header">(.*?)<\/h4>/);
    t = t.replace(/<h4 class="main-con-header">(.*?)<\/h4>/, '');

    const canvas_container_selector = $('#canvas-container');
    canvas_container_selector.html('');
    canvas_container_selector.html(heading[0] + '<form id="response-form" style="margin:5px; font-size: 17px" class="form-horizontal">' +
        t +
        '</form>'
    );

    max = 0;
    $.each($('.ans'), function (index, element) {
        $(this).attr('name', 'ans' + (index + 1));
        $(this).attr('id', 'ans' + (index + 1));
        max = index + 1;
    });

    // Fetches and displays list of emails.
    $.ajax({
        url: jsglobals.base_url + 'tsl/get_email_list',
        dataType: 'json',
        data: {
            outline_id: $('#email-type').val()
        },
        type: 'post',
        error: ajaxError
    }).done(function (data) {

        if (data.status != 'success') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
            window.location.href = data.redirect;
            return;
        }
        updateEmailList(data.content);

    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
    });
}

// Fetches Buyer email outline.
function refreshPage() {

    // Set the active email event and purpose if any.
    // $('#email-event').val((undefined !== localStorage.active_email_event) ? localStorage.active_email_event : 'mp');
    $('#email-purpose').val((undefined !== localStorage.active_email_purpose) ?
        localStorage.active_email_purpose : 'bb');

    $.ajax({
        url: jsglobals.base_url + 'tsl/get_email_default',
        dataType: 'json',
        type: 'post',
        data: {
            email_event: $('#email-event').val(),
        },
        error: ajaxError
    }).done(function (data) {

        if (data.status !== 'success') {
            toastr.error(data.message);
            return;
        }
        else if (data.status === 'redirect') {
            window.location.href = data.redirect;
            return;
        }

        const email_type_selector = $('#email-type');
        $('#email-select').empty();
        email_type_selector.empty();

        $.each(data.default_outline, function (index, outline) {
            $('#email-type').append('<option data-content="' + outline.ted_content + '" data-id="' + outline.ted_id +
                '"  value="' + outline.ted_id + '">' + outline.ted_type + '</option>')
        });

        // If no Email Type is present.
        if (0 === parseInt(type_selected)) {
            getEmailOutline();
        }


        // Append Email Version to the table.
        $('#email-outline-table tbody').find('tr').remove();

        $('#email-outline-table').find('tbody')
            .append($('<tr class="active_outline">')
                .append($('<td class="text-center">')
                    .append(1))
                .append($('<td class="text-center">')
                    .append($('<a href="#">')
                        .append('Example Outline</a>'))));


        // If email type is changed.
        email_type_selector.off('change');
        email_type_selector.change(function () {
            // localStorage.setItem('active_email_type', $('#email-type').val());
            getEmailOutline();
        });
    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
    });

}


// Fetches custom Email outline.
function getEmailOutline() {

    // Display buyer's email buttons.
    $.ajax({
        url: jsglobals.base_url + 'tsl/get_email_outline',
        dataType: 'json',
        type: 'post',
        data: {
            email_purpose: (undefined === $('#email-purpose').val() ? '' : $('#email-purpose').val()),
            email_event: (undefined === $('#email-event').val() ? '' : $('#email-event').val()),
            email_type: (undefined === $('#email-type').val() ? '' : $('#email-type').val()),
            email_ta: (undefined === $('#target-audience').val() ? '' : $('#target-audience').val()),
            email_criteria: (undefined === $('#list-criteria').val() ? '' : $('#list-criteria').val()),
            email_focus: (undefined === $('#promo-focus').val() ? '' : $('#promo-focus').val())
        },
        error: ajaxError
    }).done(function (data) {

        if (data.status != 'success') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
            window.location.href = data.redirect;
            return;
        }

        updateEmailOutline(data.email_outline);

    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
        // Check if the component is a dummy.
        check_dummy_component(client_id);
    });
}


// Display selected Buyer email outline.
function updateEmailOutline(outlines) {

    const email_outline_selector = $('#email-outline');
    email_outline_selector.empty();

    // List outline versions.
    // $('#email-outline-table').find('tr').remove();

    email_outline_selector.append($('<option id="' + 0 + '" value="0" data-content="' +
        $('#email-type').find('option:selected').attr('data-content') + '"> Example Outline </option>'));

    $.each(outlines, function (index, outline) {
        $('#email-outline')
            .append($('<option id="' + outline.teo_id + '" value="' + (index + 1) + '"> Outline V' +
                outline.teo_version + '</option>'));

        $('#email-outline-table').find('tbody')
            .append($('<tr>')
                .append($('<td class="text-center">')
                    .append(index + 1))
                .append($('<td class="text-center">')
                    .append($('<a class="email_outline_selector" data-id="' + outline.teo_id + '" ' +
                        'data-val="' + index + '">')
                        .append('Outline V' + outline.teo_version + '</a>')))
                .append($('<td class="text-center">')
                    .append($('<button class="btn btn-default btn-sm"' +
                        ' data-toggle="modal"' +
                        ' data-target="#delete-email-outline-dialog"' +
                        ' data-backdrop="static"' +
                        ' data-id="' + outline.teo_id +
                        '" data-name="' + outline.teo_version +
                        '"> <span class="glyphicon glyphicon-trash"></span>')))
            );
    });

    update_outline_content(outlines);
}

// Updates the Outline content.
function update_outline_content(outlines) {
    const email_outline_selector = $('#email-outline');

    let content, id = 0;

    // Check if email outline dropdown exist.
    if (email_outline_selector.length) {
        email_outline_selector.off('change');

        // Display selected outline text in editor.
        email_outline_selector.change(function () {
            // Set the active email outline ID value in the local storage.
            localStorage.setItem('active_email_outline_id', $('#email-outline').val());
            activeEmailId = 0;

            content = outlines[(email_outline_selector.val() - 1)].teo_content;
            id = outlines[(email_outline_selector.val() - 1)].teo_id;

            updateEmailContent(id, content)
        });

        // email_outline_selector.trigger('change');

    } else {
        const email_outline_selector = $('.email_outline_selector');

        email_outline_selector.unbind('click');
        email_outline_selector.click(function () {
            content = outlines[$(this).attr('data-val')].teo_content;
            id = $(this).attr('data-id');
            updateEmailContent(id, content)
        });

        // Click the first outline if none is active.
        if (0 === activeOutlineId) {
            $('#email-outline-table').find('tr:first').find('a').trigger('click')
        } else {
            $('a[data-id=' + activeOutlineId + ']').trigger('click')
        }
    }

    if (0 === id) {
        content = $('#email-type').find('option:selected').attr('data-content');
        updateEmailContent(id, content);
    }

    // Email Outline Viewer.
    const view_email_outline_selector = $('#view-email-outline');
    view_email_outline_selector.attr('data-toggle', 'modal');
    view_email_outline_selector.attr('data-target', '#view-email-outline-dialog');
    view_email_outline_selector.attr('data-backdrop', 'static');
}

// Update the Email Content.
function updateEmailContent(id, content) {
    const outline_container_selector = $('#outline-container');
    outline_container_selector.html('');

    // Append the content to Email outline container.
    outline_container_selector.html('<h4 class="main-con-header"><b>' + component_name + ' Outline</b></h4><hr>');
    outline_container_selector.append('<form id="outline-form">' +
        '<input type="hidden" id="email_type" name="email_type">' +
        '<input type="hidden" id="email_purpose" name="email_purpose">' +
        '<input type="hidden" id="email_event" name="email_event">' +
        '<input id="y" value="' + content + '" type="hidden" name="outline_content">\n' +
        '<trix-editor input="y" name="tsl_email_editor"></trix-editor>' +
        '</form>');

    trixButtons();
    obj = {"data-id": id};
    setupEmail(obj);
}

// Display list of Buyer's email instances.
function updateEmailList(emails) {

    $('#tsl-email-nav').empty();

    // Add Example element to the list.
    $('#tsl-email-nav')
        .append($('<li class="text-center" id="0">')
            .append('<a href="#" onclick="setupAnswer(this); return false;" data-id="0">Example Confirm</a></li>'))

    // get_email_default_answer();

    // Populate emails list
    $.each(emails, function (index, email) {
        $('#tsl-email-nav')
            .append($('<li class="text-center" id="' + email.te_id + '">')
                .append('<a href="#" class="edit-ref" onclick="setupAnswer(this); return false;" ' +
                    'data-name="' + email.te_name + '"data-id="' + email.te_id + '">' +
                    '<span class="editable" data-type="text" data-pk = "' + email.te_id + '" data-url = "' +
                    jsglobals.base_url + 'tsl/update_email_name">' + email.te_name + '</span></a></li>'))
    });

    $('#send-email').removeAttr('data-toggle');
    $('#delete-email').removeAttr('data-toggle');

    if (undefined === emails) {
        activeEmailId = 0;
    } else if (ref !== undefined) {
        const index = emails.findIndex(x => x.te_id === ref);

        obj = {"data-id": ref, "data-name": emails[index].te_name};
        ref = undefined;

        setupAnswer(obj);
    }
    else if (0 === parseInt(activeEmailId)) {
        obj = {"data-id": 0, "data-name": 'Example Version'};
        setupAnswer(obj);
    } else if (1 === parseInt(email_added)) {
        console.log('hre');
        email_added = 0;
        obj = {"data-id": emails[emails.length - 1].te_id, "data-name": emails[emails.length - 1].te_name};
        setupAnswer(obj);
    } else if (1 === parseInt(email_content_updated)) {
        $('#' + activeEmailId).children()[0].click()
    }

    setEditable('bottom');
}

// Fetch and display already answered questions for a email.
function setupAnswer(obj) {
    activeEmailId = $(obj).attr('data-id');

    // Displaying the active email name.
    $('.active').removeClass('active');
    $('#' + activeEmailId).addClass('active');

    // Set attribute for send button.
    const send_email_selector = $('#send-email');
    send_email_selector.attr('data-id', activeOutlineId);
    send_email_selector.attr('data-toggle', 'modal');
    send_email_selector.attr('data-email', activeEmailId);
    send_email_selector.attr('data-target', '#send-email-dialog');
    send_email_selector.attr('data-backdrop', 'static');
    send_email_selector.attr('data-name', $(obj).attr('data-name'));

    // Set attribute for delete button.
    const delete_email_selector = $('#delete-email');
    delete_email_selector.attr('data-id', activeEmailId);
    delete_email_selector.attr('data-toggle', 'modal');
    delete_email_selector.attr('data-target', '#delete-email-dialog');
    delete_email_selector.attr('data-backdrop', 'static');
    delete_email_selector.attr('data-name', $(obj).attr('data-name'));
    delete_email_selector.attr('data-backdrop', 'static');

    // Fetches the list of already answered questions.
    $.ajax({
        url: jsglobals.base_url + 'tsl/get_email_content',
        dataType: 'json',
        data: {
            email_id: activeEmailId,
            client_id: client_id
        },
        type: 'post',
        error: ajaxError
    }).done(function (data) {

        if (data.status != 'success') {
            toastr.error(data.message);
            return;
        }
        else if (data.status == 'redirect') {
            window.location.href = data.redirect;
            return;
        }

        $('.ans').val('');

        if (0 === data.answers.length) {
            get_email_default_answer();
        } else {
            // Clears answer field.
            $('.email-answer').val('');
            email_content = '';
            $.each(data.answers, function (index, answer) {
                email_content += '<p>' + answer.tea_content + '</p>';
                $('#ans' + answer.tea_question_number).val(answer.tea_content.replace('\\', ''));
            })
        }
    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
    });

    // Makes the names editable.
    $(".edit-ref").children().click(function (event) {
        event.stopPropagation();
    });

    speechFunctionality();
}

// Fetches default answers for email builder form DB.
function get_email_default_answer() {
    const get_email_default_answer = makeAjaxCall('tsl/get_email_default_answer', {'id': $('#email-type').val()});

    get_email_default_answer.then(function (data) {
        $.each(data.default_answers, function (index, ans) {
            $('#response-form textarea').eq(index).val(ans.teda_content);
        });

        // Auto grow script form text area size.
        $('#response-form textarea').each(function () {
            $(this).css('height', $(this)[0].scrollHeight + 'px');
        });
    });
}

