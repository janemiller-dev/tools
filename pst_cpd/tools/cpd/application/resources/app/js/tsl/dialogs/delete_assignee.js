$(document).ready(function () {
    var assignee_id = '';
    var ui_name = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-assignee-dialog').on('show.bs.modal', function (event) {
        assignee_id = $(event.relatedTarget).data('id');
        ui_name = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name')

        $('#assignee-name').html(ui_name);
        $('#view-assignee-dialog').modal('hide');
    });

    $(document).on('click', '#close-delete-assignee-dialog', function () {
        $('#delete-assignee-dialog').modal('hide');
    });

    // Deletes the assignee name.
    $(document).on('submit', '#delete-assignee-form', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_assignee',
            dataType: 'json',
            type: 'post',
            data: {
                assignee_id: assignee_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            toastr.success('Assignee Deleted.', 'Success!!');
            $('#delete-assignee-dialog').modal('hide');
            refreshDashboard();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
        return false;
    });
});
