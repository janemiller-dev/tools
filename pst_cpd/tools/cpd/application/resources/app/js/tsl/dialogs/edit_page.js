let data, sa_id;
$(document).ready(function () {

    let outline_id;

    $('#edit-page-dialog').on('show.bs.modal', function (event) {
        outline_id = $('#promotion-outline option:selected').attr('id');
        $('#compose-promo-dialog').modal('hide');

        let text_array = $('#compose-promo-form textarea'),
            textAreaCount = 0,
            dObj;

        // Sets the value from the text field  into span.
        $('#compose-promo-form span').each(function () {
            dObj = $(this);
            dObj.text(text_array.eq(textAreaCount).val());
            textAreaCount++;
        });


        // Generates html for PDF with labels.
        if ((type === 'sa') && ($('#template-version').val() === '5pl2pbl')) {
            data = $('#compose-promo-form').html().replace(/<blockquote>/g,
                "<blockquote style='margin-top: 1em; margin-bottom: 1em; margin-left: 40px; margin-right: 40px;'>")
                .replace(/<\/label>/g, '<\/label><br>').replace(/<textarea[\s\S]*?<\/textarea>/g, '')
                .replace(/<span id="span_ans" class="hidden">/g, "<span>").replace(/<\/span>/g, '<\/span><br>');

        } else {
            data = $('#compose-promo-form').html().replace(/<blockquote>/g,
                "<blockquote style='margin-top: 1em; margin-bottom: 1em; margin-left: 40px; margin-right: 40px;'>")
                .replace(/<label>[\s\S]*?<\/label>/g, '').replace(/<textarea[\s\S]*?<\/textarea>/g, '')
                .replace(/<span id="span_ans" class="hidden">/g, "<span>");

        }

        $('#edit-page-container').html('');

        tinymce.init({
            selector: '#edit-page-container',
            branding: false
        });
    });

    const path = window.location.pathname,
        components = path.split('/'),
        type = components[components.length - 2];


    // Form validation.
    $('#compose-page').click(function () {
        let url;
        if (type === 'sa') {
            url = jsglobals.base_url + 'cpdv1/save_sa_data';
            sa_id = activeSAId;
        } else {
            url = jsglobals.base_url + 'tsl/compose_promo';
        }
        $.ajax({
            url: url,
            dataType: 'json',
            type: 'post',
            data: {
                data: $('#z').val().replace(/&nbsp; /g, '').replace(/<blockquote>/g, '').replace(/<\/blockquote>/g, ''),
                compose: $('#compose-page').attr('data-compose'),
                bg_image: $('.upload-sa').attr('data-name'),
                sa: $('#sa-type').val(),
                sa_id: sa_id,
                page: $('#page-template').val(),
                template: $('#template-version').val(),
                outline_id: outline_id,
                template_id: $('#background-template').val()
            },
            error: ajaxError
        }).done(function (data) {
            if (data.status !== 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            toastr.success('Content Saved!!', 'Success');
            $('#edit-page-dialog').modal('hide');
            promo_added = 1;
            refreshPage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });
});