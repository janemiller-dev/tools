$(document).ready(function () {
    var update_id = '';
    var ui_name = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-update-dialog').on('show.bs.modal', function (event) {
        update_id = $(event.relatedTarget).data('id');
        ui_name = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name')

        $('#update-name').html(ui_name);
        $('#view-update-dialog').modal('hide');
    });

    $(document).on('click', '#close-delete-update-dialog', function () {
        $('#delete-update-dialog').modal('hide');
    });

    // Deletes Buyer Update.
    $(document).on('submit', '#delete-update-form', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_update',
            dataType: 'json',
            type: 'post',
            data: {
                update_id: update_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            toastr.success('Update Instance Deleted.', 'Success!!');
            $('#delete-update-dialog').modal('hide');

            update_added = 1;
            refreshUpdatePage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
        return false;
    });
});
