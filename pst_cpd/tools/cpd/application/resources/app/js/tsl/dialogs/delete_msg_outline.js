$(document).ready(function () {
    var msg_outline_id = '';
    var version = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-msg-outline-dialog').on('show.bs.modal', function (event) {
        $('#view-promo-outline-dialog').modal('hide')
        version = $(event.relatedTarget).data('name');
        msg_outline_id = $(event.relatedTarget).data('id');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name');

        $('#msg-outline-id').val(msg_outline_id);
        $('#delete-msg-outline-version').html(version);
        $('#view-msg-outline-dialog').modal('hide');
    });

    // Deletes Buyer's survey Outline.
    $(document).on('submit', '#delete-msg-outline-form', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_msg_outline',
            dataType: 'json',
            type: 'post',
            data: {
                msg_outline_id: msg_outline_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('Survey Outline Deleted.', 'Success!!');
            $('#delete-msg-outline-dialog').modal('hide');
            activeOutlineId = 0;
            refreshPage();

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
        return false;
    });
});
