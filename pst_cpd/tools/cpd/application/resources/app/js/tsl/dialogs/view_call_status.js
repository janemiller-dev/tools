$(document).ready(function () {
    // Fetches the performance score.
    $('#view-call-status-dialog').on('show.bs.modal', function (event) {
        view_call_status.init();
    });

    $('#view-call-status-dialog').on('shown.bs.modal', function (event) {
        $(document).off('focusin.modal');
    });

    $('#delete-call-status-dialog').on('show.bs.modal', function (event) {
        $('#view-call-status-dialog').modal('hide');

        const id = $(event.relatedTarget).attr('data-id'),
            name = $(event.relatedTarget).attr('data-name');

        $('#call-status-name').text(name);

        $('#delete-call-status-button').off('click');
        $('#delete-call-status-button').on('click', function () {
            const delete_call_status = makeAjaxCall('tsl/delete_call_status', {
                'id': id
            });

            delete_call_status.then(function (data) {
                toastr.success('Call Status Deleted!!');
                $('#delete-call-status-dialog').modal('hide');
                fetch_call_status();
                refreshPage();
            })
        })
    });
});

let view_call_status = function () {

    // Initialize and get the DMD ID.
    let init = () => {
        const client_id = $('#view-call-status-dialog').attr('data-id'),
            get_phone = new Promise((resolve, reject) => function get_data() {
                try {
                    get_call_status = makeAjaxCall('tsl/get_call_status', {
                        client_id: client_id
                    });

                    get_call_status.then(function (data) {
                        $('#add-call-status-button').attr('data-id', client_id);
                        const table_body_selector = $('#call-status-table tbody');

                        table_body_selector.empty();

                        // Append additional phone.
                        $.each(data.call_status, function (index, status) {
                            let is_editable = '',
                                is_disabled = '';

                            // Check if this is default call status.
                            ('0' === status.tcs_user_id) ?
                                is_disabled = 'disabled' :
                                is_editable = 'editable';

                            table_body_selector.append('<tr>' +
                                '<td>' + (1 + index) + '</td>' +
                                '<td><span class="' + is_editable + '" data-type="text" data-pk="' + status.tcs_id + '" ' +
                                ' data-url="' + jsglobals.base_url + 'tsl/update_call_status">' + status.tcs_name + '</td>' +
                                '<td><button class="btn-sm btn btn-default" data-toggle="modal" ' +
                                'data-id="' + status.tcs_id + '" data-name="' + status.tcs_name + '"' +
                                'data-target="#delete-call-status-dialog"' + is_disabled + '>' +
                                '<i class="fa fa-trash"></i></td>' +
                                '</tr>');
                        });
                        resolve(true);
                    })
                } catch (e) {
                    reject(e);
                }
            }());

        get_phone.then(function () {
            // Enable XEditable
            setEditable('top');
        });
    };
    return {
        init: init
    };
}();