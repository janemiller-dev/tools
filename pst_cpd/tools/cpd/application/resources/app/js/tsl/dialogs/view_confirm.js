$(document).ready(function () {
    $('#view-confirm-dialog').on('show.bs.modal', function (event) {
        $('#view-confirm-div input, #view-confirm-div textarea').val('');
        view_confirm.init(event);
    });

    view_confirm.add_confirm();
});

// View Notes available.
let view_confirm = function () {

    var confirm_max_seq = 0, client_id;

    // Initialize and get the DMD ID.
    let init = function (event) {
            client_id = $(event.relatedTarget).data('id');
            confirm_max_seq = 0;

            // Fetch confirm data.
            const get_confirm_data = makeAjaxCall('tsl/get_confirm_data', {
                client_id: client_id
            });

            get_confirm_data.then(function (data) {
                let dummy_content;
                $('.additional-confirm-row').remove();

                $.each(data.confirm_data, function (index, client) {
                    // Check if this row exists or not.
                    if (0 === $('#confirm-content' + client.tsl_confirm_seq).length) {
                        dummy_content = $('#view-confirm-div').clone();

                        dummy_content.find('#confirm-name0')
                            .attr('data-seq', (parseInt(client.tsl_confirm_seq)))
                            .attr('id', 'confirm-name' + (parseInt(client.tsl_confirm_seq)));

                        dummy_content.find('.confirm-site')
                            .attr('data-seq', (parseInt(client.tsl_confirm_seq)))
                            .attr('id', 'confirm-site' + (parseInt(client.tsl_confirm_seq)));

                        dummy_content.find('textarea')
                            .attr('data-seq', (parseInt(client.tsl_confirm_seq)))
                            .attr('id', 'confirm-content' + (parseInt(client.tsl_confirm_seq)));

                        dummy_content.find('#confirm-topic0')
                            .attr('data-seq', (parseInt(client.tsl_confirm_seq)))
                            .attr('id', 'confirm-topic' + (parseInt(client.tsl_confirm_seq)));

                        dummy_content.find('.confirm-when')
                            .attr('data-seq', (parseInt(client.tsl_confirm_seq)))
                            .attr('id', 'confirm-when' + (parseInt(client.tsl_confirm_seq)))
                            .addClass('confirm-date');

                        dummy_content.find('#delete-confirm0')
                            .attr('data-seq', (parseInt(client.tsl_confirm_seq)))
                            .attr('id', 'delete-confirm' + (parseInt(client.tsl_confirm_seq)))
                            .attr('data-id', client_id);

                        $('#confirm-row').append('<div class="col-xs-12 additional-confirm-row" style="margin-top: 10px">'
                            + '<hr>' + dummy_content.html() + '</div>');

                        confirm_max_seq = client.tsl_confirm_seq;
                    }

                    // Set the content.
                    $('#confirm-name' + confirm_max_seq).val(client.tsl_confirm_name);
                    $('#confirm-site' + confirm_max_seq).val(client.tsl_confirm_site);
                    $('#confirm-content' + confirm_max_seq).val(client.tsl_confirm_note);
                    $('#confirm-topic' + confirm_max_seq).val(client.tsl_confirm_topic);
                    $('#confirm-when' + confirm_max_seq).val(moment(client.tsl_confirm_when).format('DD-MMM-YY, h:mm A'));
                    $('.delete-confirm').attr('data-id', client_id);
                });

                enable_confirm_date_picker();
                store_confirm_data();
            });

            $(document).off('click', '.delete-confirm');
            $(document).on('click', '.delete-confirm', function () {
                const delete_action = makeAjaxCall('tsl/delete_client_confirm', {
                    id: $(this).attr('data-id'),
                    seq: $(this).attr('data-seq')
                });

                delete_action.then(function (data) {
                    toastr.success('Event Confirmation Deleted.', 'Success!!');
                    $('#view-confirm-dialog').modal('hide');
                })
            })
        },

        // Stores Confirm data.
        store_confirm_data = function () {
            $('.confirm-field').unbind('change');
            $('.confirm-field').change(function () {
                makeAjaxCall('tsl/update_confirm_data', {
                    'id': client_id,
                    'col': $(this).data('name'),
                    'val': $(this).val(),
                    'seq': $(this).data('seq')
                })
            });
        },
        // Adds new column to the Confirm List.
        add_confirm = function () {
            // Add a new event to the list.
            $('#add-confirm').click(function () {

                dummy_content = $('#view-confirm-div').clone();

                confirm_max_seq = parseInt(confirm_max_seq) + 1;


                dummy_content.find('#confirm-name0')
                    .attr('data-seq', confirm_max_seq)
                    .attr('id', 'confirm-name' + confirm_max_seq);

                dummy_content.find('.confirm-site')
                    .attr('data-seq', confirm_max_seq)
                    .attr('id', 'confirm-site' + confirm_max_seq);

                dummy_content.find('textarea')
                    .attr('data-seq', confirm_max_seq)
                    .attr('id', 'confirm-content' + confirm_max_seq);

                dummy_content.find('#confirm-topic0')
                    .attr('data-seq', confirm_max_seq)
                    .attr('id', 'confirm-topic' + confirm_max_seq);

                dummy_content.find('.confirm-when')
                    .attr('data-seq', confirm_max_seq)
                    .attr('id', 'confirm-when' + confirm_max_seq)
                    .addClass('confirm-date');

                dummy_content.find('#delete-confirm0')
                    .attr('data-seq', confirm_max_seq)
                    .attr('id', 'delete-confirm' + confirm_max_seq)
                    .attr('data-id', client_id);

                $('#confirm-row').append('<div class="col-xs-12 additional-confirm-row" style="margin-top: 10px">'
                    + '<hr>' + dummy_content.html() + '</div>');

                enable_confirm_date_picker();
                store_confirm_data();
            });
        },
        enable_confirm_date_picker = function () {
            $('.confirm-date').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss'
            });
        };

    return {
        init: init,
        add_confirm: add_confirm
    };
}();