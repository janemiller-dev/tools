$(document).ready(function () {
    var caller_id = '';
    var ui_name = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-caller-dialog').on('show.bs.modal', function (event) {
        caller_id = $(event.relatedTarget).data('id');
        ui_name = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name')

        $('#caller-name').html(ui_name);
        $('#view-caller-dialog').modal('hide');
    });

    $(document).on('click', '#close-delete-caller-dialog', function () {
        $('#delete-caller-dialog').modal('hide');
    });

    // Deletes the caller name.
    $(document).on('submit', '#delete-caller-form', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_caller',
            dataType: 'json',
            type: 'post',
            data: {
                caller_id: caller_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            toastr.success('Caller Name Deleted', 'Success');
            $('#delete-caller-dialog').modal('hide');
            refreshDashboard();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
        return false;
    });
});
