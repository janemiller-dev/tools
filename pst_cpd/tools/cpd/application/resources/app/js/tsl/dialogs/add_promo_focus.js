$(document).ready(function () {

    $('#add-promo-focus-dialog').on('show.bs.modal', function (event) {
        $('#view-promo-focus-dialog').modal('hide');
    });

    // Form validation.
    $('#add-promo-focus-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            promo_focus_name: {
                validators: {
                    notEmpty: {
                        message: "Promotional Focus name is required."
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the add new promo-focus form.
        const $form = $(e.target),
            fv = $form.data('formValidation');

        // Get the form data and submit it.
        let promo_focus_data = $form.serializeArray(),
            tool_id = window.location.search.split('?id=')[1];

        (tool_id === undefined) ? tool_id = tsl_id : '';

        promo_focus_data.push({name: 'tool_id', value: tool_id});
        promo_focus_data.push({name: 'origin', value: origin});

        $.ajax({
            url: jsglobals.base_url + 'tsl/add_promo_focus',
            dataType: 'json',
            type: 'post',
            data: promo_focus_data,
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            } else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('Promotional Focus Added!!', 'Success!!');
            fv.resetForm();
            $($form)[0].reset();
            $('#add-promo-focus-dialog').modal('hide');

            // Check if request is from TSL page.
            if ('tsl' === origin) {
                refreshTSLdropdown().then(function () {
                    refreshDMDDropdown().then(function (data) {
                        refreshPage();
                    })
                });
            } else {
                refreshDMDDropdown().then(function (data) {
                    refreshPage();
                });
            }

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });
});