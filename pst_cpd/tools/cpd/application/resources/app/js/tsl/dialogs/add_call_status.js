$(document).ready(function () {
    // Fetches the performance score.
    $('#add-call-status-dialog').on('show.bs.modal', function (event) {
        $('#view-call-status-dialog').modal('hide');
        add_call_status.init();
    });
});

let add_call_status = function () {

    // Initialize and get the DMD ID.
    let init = () => {

        $('#save-call-status').off('click');
        $('#save-call-status').click(function () {
            const add_call_status = makeAjaxCall('tsl/save_call_status', {
                'call_status': $('#call-status').val()
            });

            add_call_status.then(function (data) {
                $('#add-call-status-dialog').modal('hide');
                fetch_call_status();
                refreshPage();
            })
        });
    };
    return {
        init: init
    };
}();