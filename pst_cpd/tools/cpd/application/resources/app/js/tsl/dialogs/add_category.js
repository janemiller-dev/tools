// $(document).ready(function () {
//     var client_deal_id = '';
//
//     $('#add-client-dialog').on('show.bs.modal', function (event) {
//         $(document).on('click', '#close-add-client-dialog', function () {
//             $('#add-client-dialog').modal('hide');
//         });
//     });
//
//     $('#add-client-form').formValidation({
//         framework: 'bootstrap',
//         icon: {
//             valid: 'glyphicon glyphicon-ok',
//             invalid: 'glyphicon glyphicon-remove',
//             validating: 'glyphicon glyphicon-refresh',
//         },
//         fields: {
//             client_name: {
//                 validators: {
//                     notEmpty: {
//                         message: "The client name is required."
//                     }
//                 }
//             }
//         }
//     }).on('success.form.fv', function (e) {
//         e.preventDefault();
//         console.log('here');
//
//         // Get the form cpd
//         var $form = $(e.target);
//         var fv = $form.data('formValidation');
//
//         // Get the form data and submit it.
//         var client_data = $form.serialize();
//
//         $.ajax({
//             url: jsglobals.base_url + 'tsl/add_client',
//             dataType: 'json',
//             type: 'post',
//             data: client_data,
//             error: ajaxError
//         }).done(function (data) {
//
//             if (data.status != 'success') {
//                 toastr.error(data.message);
//                 return;
//             } else if (data.status == 'redirect') {
//                 window.location.href = data.redirect;
//                 return;
//             }
//
//             fv.resetForm();
//             $($form)[0].reset();
//
//             $('#add-client-dialog').modal('hide');
//             // var client_deal_id = $('#view-client-deal-id').val();
//             refreshPage();
//
//             // $('.deal-table span.glyphicon-edit.client[data-id="' + client_deal_id + '"]').trigger('click');
//
//
//         }).fail(function (jqXHR, textStatus) {
//             toastr.error("Request failed: " + textStatus);
//         }).always(function () {
//         });
//     });
//     refreshAgent();
// });
