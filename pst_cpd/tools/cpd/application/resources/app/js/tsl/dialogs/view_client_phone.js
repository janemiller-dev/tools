$(document).ready(function () {
    // Fetches the performance score.
    $('#view-client-phone-dialog').on('show.bs.modal', function (event) {
        view_client_phone.init();
    });

    $('#view-client-phone-dialog').on('shown.bs.modal', function (event) {
        $(document).off('focusin.modal');
    });

    $('#delete-additional-phone-dialog').on('show.bs.modal', function (event) {
        $('#view-client-phone-dialog').modal('hide');

        const id = $(event.relatedTarget).attr('data-id'),
            name = $(event.relatedTarget).attr('data-name');

        $('#additional-phone-name').text(name);

        $('#delete-additional-phone-button').off('click');
        $('#delete-additional-phone-button').on('click', function () {
            const delete_additional_phone = makeAjaxCall('tsl/delete_additional_phone', {
                'id': id
            });

            delete_additional_phone.then(function (data) {
                toastr.success('Phone Deleted!!');
                $('#delete-additional-phone-dialog').modal('hide');
                refreshPage();
            })
        })
    });
});

let view_client_phone = function () {

    // Initialize and get the DMD ID.
    let init = () => {
        const client_id = $('#view-client-phone-dialog').attr('data-id'),
            get_phone = new Promise((resolve, reject) => function get_data() {
                try {
                    get_client_phone = makeAjaxCall('tsl/get_client_phone', {
                        client_id: client_id
                    });

                    get_client_phone.then(function (data) {
                        $('#add-client-phone-button').attr('data-id', client_id);
                        const table_body_selector = $('#client-phone-table tbody');

                        table_body_selector.empty();

                        // Append main phone.
                        table_body_selector.append('<tr>' +
                            '<td>1</td>' +
                            '<td>Main Phone</td>' +
                            '<td><span class="editable" data-type="text" data-pk="' + client_id + '" ' +
                            'data-url="' + jsglobals.base_url + 'tsl/update_client_phone" data-name="hc_main_phone">' +
                            data.phones.main_phone[0].hc_main_phone + '</span></td>' +
                            '<td><button class="btn-sm btn btn-default" disabled><i class="fa fa-trash"></i></td>' +
                            '</tr>');

                        // Append Second phone.
                        table_body_selector.append('<tr>' +
                            '<td>2</td>' +
                            '<td>Second Phone</td>' +
                            '<td><span class="editable" data-type="text" data-pk="' + client_id + '" ' +
                            'data-url="' + jsglobals.base_url + 'tsl/update_client_phone" data-name="hc_second_phone">' +
                            data.phones.second_phone[0].hc_second_phone + '</span></td>' +
                            '<td><button class="btn-sm btn btn-default" disabled><i class="fa fa-trash"></i></td>' +
                            '</tr>');

                        // Append Mobile phone
                        table_body_selector.append('<tr>' +
                            '<td>3</td>' +
                            '<td>Mobile Phone</td>' +
                            '<td><span class="editable" data-type="text" data-pk="' + client_id + '" ' +
                            'data-url="' + jsglobals.base_url + 'tsl/update_client_phone" data-name="hc_mobile_phone">' +
                            data.phones.mobile_phone[0].hc_mobile_phone + '</span></td>' +
                            '<td><button class="btn-sm btn btn-default" disabled><i class="fa fa-trash"></i></td>' +
                            '</tr>');

                        // Append additional phone.
                        $.each(data.phones.additional, function (index, phone) {
                            table_body_selector.append('<tr>' +
                                '<td>' + (4 + index) + '</td>' +
                                '<td><span class="editable" data-type="text" data-pk="' + phone.hcp_id + '" ' +
                                ' data-url="' + jsglobals.base_url + 'tsl/update_additional_phone" ' +
                                'data-name="hcp_type">' + phone.hcp_type + '</td>' +
                                '<td><span class="editable" data-type="text" data-pk="' + phone.hcp_id + '" ' +
                                'data-url="' + jsglobals.base_url + 'tsl/update_additional_phone" ' +
                                'data-name="hcp_phone">' + phone.hcp_phone + '</span></td>' +
                                '<td><button class="btn-sm btn btn-default" data-toggle="modal" ' +
                                'data-id="' + phone.hcp_id + '" data-name="' + phone.hcp_type + '"' +
                                'data-target="#delete-additional-phone-dialog"><i class="fa fa-trash"></i></td>' +
                                '</tr>');
                        });
                        resolve(true);
                    })
                } catch (e) {
                    reject(e);
                }
            }());

        get_phone.then(function () {
            // Enable XEditable
            setEditable('top');
        });
    };
    return {
        init: init
    };
}();