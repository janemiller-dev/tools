$(document).ready(function () {
    let id;

    $('#add-target-audience-dialog').on('show.bs.modal', function (event) {
        $('#view-target-audience-dialog').modal('hide');
        id = $(event.relatedTarget).data('id');
    });

    // Form validation.
    $('#add-target-audience-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            target_audience_name: {
                validators: {
                    notEmpty: {
                        message: "Location is required."
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the add new target-audience form.
        var $form = $(e.target);
        var fv = $form.data('formValidation');

        // Check if TSL ID is empty.
        ('undefined' === typeof(tsl_id)) ? tsl_id = id : '';

        // Get the form data and submit it.
        var target_audience_data = $form.serializeArray();
        target_audience_data.push({name: 'tsl_id', value: tsl_id});

        $.ajax({
            url: jsglobals.base_url + 'tsl/add_target_audience',
            dataType: 'json',
            type: 'post',
            data: target_audience_data,
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            } else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('Location Added!!', 'Success!!')
            fv.resetForm();
            $($form)[0].reset();

            $('#add-target-audience-dialog').modal('hide');
            refreshPage();
            get_all_criteria();

            $('#add-tsl-form').formcache("outputCache");

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });
});