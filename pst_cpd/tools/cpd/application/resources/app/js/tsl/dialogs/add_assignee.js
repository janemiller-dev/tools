$(document).ready(function () {
    $('#add-assignee-dialog').on('show.bs.modal', function (event) {
        $('#view-assignee-dialog').modal('hide');
    });

    // Form validation.
    $('#add-assignee-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            assignee_name: {
                validators: {
                    notEmpty: {
                        message: "Agent name is required."
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the add new assignee form.
        var $form = $(e.target);
        var fv = $form.data('formValidation');

        // Get the form data and submit it.
        var assignee_data = $form.serializeArray();

        $.ajax({
            url: jsglobals.base_url + 'tsl/add_assignee',
            dataType: 'json',
            type: 'post',
            data: assignee_data,
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            } else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('Assignee Added!!', 'Success!!');
            fv.resetForm();
            $($form)[0].reset();
            $('#add-assignee-dialog, #view-assignee-dialog').modal('hide');

            (typeof(refreshDashboard) !== "undefined") ? refreshDashboard() : refreshPage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });
});