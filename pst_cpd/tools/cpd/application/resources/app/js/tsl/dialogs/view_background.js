$(document).ready(function()
{
    // Document upload button.
    $('.upload-background-template').click(function () {
        $('#image-upload').trigger('click');
        return false;
    });

    $('#background-template').change(function () {
        // Checks if add new criteria option is selected.
        if ($(this).val() == "0") {
            $('#view-background-dialog').modal('show');
            $(this).val(-1);
        }
    });

    // Jquery file upload.
    $('#image-upload').fileupload({
        dataType: 'json',
        done: function (e, data) {
            if (data.result.status != "success") {
                toastr.error(data.result.message);
                return;
            }
            $.each(data.result.files, function (index, file) {
                $('<p/>').text(file.name).appendTo(document.body);
            });
            toastr.success('Image uploaded successfully...');
            refreshPage();
        }
    });
});

