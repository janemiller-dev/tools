/**
 *
 * Choose a new location for TSL.
 *
 * @summary      Dialog used to select new location for TSL.
 * @description  This file contains functions for selecting new location for a TSL
 *               which will then fetch the assets from homebase.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */

$(document).ready(function () {

    $('#choose-location-dialog').on('show.bs.modal', function (event) {
        $('#view-tsl-location-dialog').modal('hide');
        choose_location.init(event);
    });

});

/**
 *
 * View Notes available.
 */
let choose_location = function () {

    // Initialize and get the User Data.
    let init = function (event) {

        // Fetch User Info from DB.
        const fetch_location = makeAjaxCall('tsl/get_user_tsl_data', {
            'tsl_id': tsl_id
        });

        // Append options to location menu.
        $('#choose-location').empty();
        $('#choose-location').append('<option disabled selected>Choose New TSL location</option>')

        fetch_location.then(function (data) {
            $.each(data.tsl_data.location, function (index, location) {
                $('#choose-location').append('<option value="' + location.ul_id + '">'
                    + location.ul_name + '</option>')
            });

            // location Change Event.
            $(document).off('change', '#choose-location');
            $(document).on('change', '#choose-location', function () {
                const update_tsl_data = makeAjaxCall('tsl/update_tsl_data', {
                    val: $(this).val(),
                    id: tsl_id,
                    type: 'location'
                });

                update_tsl_data.then(function (data) {
                    console.log(data);
                });
            })
        });

    };

    return {
        init: init,
    };
}();