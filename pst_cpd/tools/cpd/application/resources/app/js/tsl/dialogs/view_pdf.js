$(document).ready(function() {

    $('#view-dialog').on('show.bs.modal', function (event) {

        $('#view-title').text($(event.relatedTarget).data('name'));
        // console.log($(event.relatedTarget));
        $promo_id = $(event.relatedTarget).data('id');
        $is_pdf = $(event.relatedTarget).data('is-pdf');
        $(event.relatedTarget).removeData('id');

        $(event.relatedTarget).removeData('is-pdf');

        var url;
        url = window.location.origin + '/tools/tsl/view_promo/?id=' + $promo_id + '&is_pdf=' + $is_pdf;

        var pdfjsLib = window['pdfjs-dist/build/pdf'];
        pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

        $('.pdf-canvas-container').html('');
        $('.pdf-canvas-container').html('<canvas id="pdf-canvas"></canvas>');

        // Removing click event from the previous canvas.
        $('.prev').off('click');
        $('.next').off('click');

        // Adding click event to the current canvas.
        $('.prev').attr('id', 'prev' + $promo_id);
        $('.next').attr('id', 'next' + $promo_id);

        var pdfDoc = null,
            pageNum = 1,
            pageRendering = false,
            pageNumPending = null,
            num = null,
            scale = 0.9,
            canvas = document.getElementById('pdf-canvas'),
            ctx = canvas.getContext('2d'),
            prev = 'prev' + $promo_id,
            next = 'next' + $promo_id;
        /**
         * Get page info from document, resize canvas accordingly, and render page.
         * @param num Page number.
         */
        function renderPage(num) {

            pageRendering = true;
            // Using promise to fetch the page
            pdfDoc.getPage(num).then(function(page) {
                var viewport = page.getViewport({scale:1});
                canvas.height = viewport.height;
                canvas.width = viewport.width;
                // Render PDF page into canvas context
                var renderContext = {
                    canvasContext: ctx,
                    viewport: viewport
                };
                var renderTask = page.render(renderContext);
                // Wait for rendering to finish
                renderTask.promise.then(function () {
                    pageRendering = false;
                    if (pageNumPending !== null) {
                        // New page rendering is pending
                        renderPage(pageNumPending);
                        pageNumPending = null;
                    }
                });
            });
            // Update page counters
            document.getElementById('page_num').textContent = num;
        }
        /**
         * If another page rendering in progress, waits until the rendering is
         * finished. Otherwise, executes rendering immediately.
         */
        function queueRenderPage(num) {
            if (pageRendering) {
                pageNumPending = num;
            } else {
                renderPage(num);
            }
        }
        /**
         * Displays previous page.
         */
        function onPrevPage() {
            if (pageNum <= 1) {
                return;
            }
            pageNum--;
            queueRenderPage(pageNum);
        }
        $('.prev').bind('click', onPrevPage);
        /**
         * Displays next page.
         */
        function onNextPage() {

            if (pageNum >= pdfDoc.numPages) {
                return;
            }
            pageNum++;
            queueRenderPage(pageNum);
        }
        $('.next').bind('click', onNextPage);
        /**
         * Asynchronously downloads PDF.
         */
        pdfjsLib.getDocument(url).promise.then(function (pdfDoc_) {
            pdfDoc = null;
            pdfDoc = pdfDoc_;
            document.getElementById('page_count').textContent = pdfDoc.numPages;
            // Initial/first page rendering
            renderPage(pageNum);
        });
    });
});