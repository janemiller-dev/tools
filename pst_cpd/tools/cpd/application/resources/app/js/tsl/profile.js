let activeOutlineId, activeProfileId, outline_added, profile_added, questions_id = [], client_id, obj, ref, requester;

$(document).ready(function () {

    // Get the Client ID from the URL
    const path = window.location.pathname,
        components = path.split('/'),
        origin = components[2];
    requester = components[3];

    ref = window.location.search.split('?ref=')[1];

    // Check if ref is defined or not.
    if (undefined !== ref) {
        const ref_array = ref.split('?');
        ref = ref_array[0];
    }
    client_id = components[components.length - 1];

    // Initialize voice rrecgnisation Functionality.
    voiceRecognition();

    const id = window.location.search.split('?id=')[1];
    loadPrev();

    activeOutlineId = 0;
    activeProfileId = -1;

    // Display outline for the profile.
    $('#outline-container').html('');
    $('#canvas-container').html('');

    // To check if a new profile is added.
    outline_added = 0;
    activeOutlineId = 0;

    // List of Buyer's Profile
    const container_listing_selector = $('#container-listing');
    container_listing_selector.html('');
    container_listing_selector.html(
        ' <h4 align="center" class="main-con-header">Profile Version</h4>' +
        ' <ul class="nav" id="tsl-profile-nav">\n' +
        ' </ul>');

    refreshProfilePage();

    // Add ability to focus on elements inside x-editable.
    $('#view-outline-dialog').on('shown.bs.modal', function () {
        $(document).off('focusin.modal');
    });

    // Creates a new instance of profile based on profile outline.
    $('#compose-profile').click(function (e) {
        e.preventDefault();
        profile_added = 1;

        $.ajax({
            url: jsglobals.base_url + 'tsl/add_profile',
            dataType: 'json',
            data: {
                outline_id: activeOutlineId
            },
            type: 'post',
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error('Please create one using save button on the left.', 'No instance of outline selected!!');
                return;
            } else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            refreshProfilePage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });

    // Saves the outline and generates questions from outlines.
    $('#save-profile-outline').click(function (e) {
        $('#profile_phase').val($('#profile-phase').val());
        $('#requester').val('profile');
        outline_added = 0;
        activeOutlineId = 0;
        const tsl_update_data = $('#outline-form').serializeArray();

        // Push question ID to the form data.
        tsl_update_data
            .push({name: 'ques_id', value: questions_id});

        e.preventDefault();

        $.ajax({
            url: jsglobals.base_url + 'tsl/update_phase_content',
            dataType: 'json',
            type: 'post',
            data: tsl_update_data,
            error: ajaxError
        }).done(function (data) {
            if (data.status !== 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            toastr.success('Outline Saved!');
            refreshProfilePage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });

    // Saves client response.
    $('#save-profile').click(function (e) {

        // Check if active profile is set to example.
        if (0 === parseInt(activeProfileId)) {
            toastr.error('To create one use compose button.', 'No Instance of profile.');
            return;
        }
        else {
            const data = $('#profile-record-view').serializeArray();
            data.push({name: 'question_id', value: questions_id});
            data.push({name: 'client_id', value: client_id});
            data.push({name: 'profile_id', value: activeProfileId});
            data.push({name: 'source', value: 'user'});
            e.preventDefault();

            $.ajax({
                url: jsglobals.base_url + 'profile/set_ans',
                dataType: 'json',
                type: 'post',
                data: data,
                error: ajaxError
            }).done(function (data) {
                if (data.status !== 'success') {
                    toastr.error(data.message);
                    return;
                }
                else if (data.status === 'redirect') {
                    window.location.href = data.redirect;
                    return;
                }
                toastr.success('Response Saved!');
                refreshProfilePage();
            }).fail(function (jqXHR, textStatus) {
                toastr.error("Request failed: " + textStatus);
            }).always(function () {
            });
        }
    });

    // Display selected outline text in editor.
    $(document).on('click', '.profile-outline', function () {
        activeOutlineId = $(this).attr('data-id');
        get_phase();

        // Set the Outline Id to local storage.
        localStorage.setItem('active_profile_outline_id', activeOutlineId);
    });

    // Add a new Profile Outline Instance
    $('#add-outline').click(function (data) {
        const add_outline = makeAjaxCall('tsl/add_profile_outline', {
            client_type: $('#client-type').val(),
            profile: $('#profile-type').val(),
        });

        add_outline.then(function (data) {
            toastr.success('Profile Outline Added!!', 'Success');
            refreshProfilePage();
        })
    });

    // Check if any drop down menu is changed, fetch outline version again.
    $('.profile-menu').change(function () {
        activeOutlineId = 0;
        refreshProfilePage();
    });

    $('#profile-phase').change(function () {
        localStorage.setItem('active_profile_phase', $(this).val());
        put_content(outlines)
    });

    // Checks if no instance of profile is present.
    $('#send-profile, #delete-profile').click(function () {
        if ($(this).attr('data-toggle') === undefined) {
            toastr.error('To create one use compose button.', 'No Instance of profile.');
        }
    });
});

// Fetches questions and puts them in a form.
function setupProfile(obj) {

    // Display buyer's survey buttons.
    const profile_phase_selector = $('#profile-phase');
    // profile_phase_selector
    //     .val(undefined !== localStorage.active_profile_phase ? localStorage.active_profile_phase : 0);

    // Check if the survey phase is set to null.
    (profile_phase_selector.val() === null) ? profile_phase_selector.find('option:eq(0)').prop('selected', true) : '';

    // Set the button for trix buttons.
    trixButtons();

    activeOutlineId = $(obj).attr('data-id');
    $version = $(obj).attr('data-version');

    $('.active_outline').removeClass('active_outline');

    // Change the active Outline.
    $('a[data-id=' + activeOutlineId + ']').closest('tr').addClass('active_outline');

    let question_input = '';

    // Fetches and displays the outline questions.
    $.ajax({
        url: jsglobals.base_url + 'tsl/get_profile_questions',
        dataType: 'json',
        type: 'post',
        data: {
            outline_id: activeOutlineId,
            full_screen_enabled: $('.full-screen').length,
            phase: $('#profile-phase').val()
        },
        error: ajaxError
    }).done(function (data) {
            if (data.status !== 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            let count = 0,
                heading_index = 0;

            if (!$('.profile-viewer').hasClass('hidden')) {
                let count = 0, phase_number = 0,
                    phase_value = [
                        'History Question',
                        'Current Facts Questions',
                        'Accomplished Questions',
                        'Needed - Problems Questions',
                        'Possible - Project Questions'];
                // $.each(data.content, function (index, question) {
                //     let ques = question.tpq_content.replace(/(?={{[\s\S]*?Question 1}})/, function () {
                //         return '<h4><b>' + heading[heading_index++] + '</b></h4>';
                //     });
                $('#profile-record-view').empty();
                $('#profile-record-view')
                    .append(
                        $.map(data.content, function (question, index) {
                            questions_id[index] = question.tpq_id;
                            let phase_string = '';
                            count++;

                            if (((count - 1) === 3 * phase_number) || count === 1) {
                                phase_string = '<h3 style="font-weight: bold">' + phase_value[phase_number] + '.' + '</h3>';
                                phase_number++;
                            }

                            let str = question.tpq_content.replace('{{', '').replace('}}', '');
                            str = str + (question.tpq_question !== null ? ' : ' + question.tpq_question : '');

                            return '<div class="container" style="margin-top: 3vh"><label>'
                                + phase_string + str + '</label>' +
                                '<div class="input-group">' +
                                '<textarea rows="2" class="form-control" name="' + 'ans' + question.tpq_id +
                                '" type = "text" id="' + 'ans' + question.tpq_id + '" ></textarea>' +
                                '<span class="input-group-addon btn btn-secondary stt"><i class="fa fa-microphone"></i></span>' +
                                '</div></div>'
                        }));

                setupAnswer(obj);
                // });
            } else {
                const heading = $('#profile-phase option:selected').attr('data-value').match(/\[\[.*?\]\]/g);
                // Removes parenthesis and creates profile form for user input.
                $.each(data.content, function (index, question) {
                    let ques = question.tpq_content.replace(/(?={{[\s\S]*?Question 1}})/, function () {
                        return '<h4><b>' + heading[heading_index++] + '</b></h4>';
                    });

                    ques = ques
                        .replace(/{{|}}/g, '')
                        .replace(/\[\[|\]\]/g, '');
                    count++;

                    questions_id[index] = question.tpq_id;
                    question_input = question_input + '<div style="margin-top: 10px">' +
                        '<label style="font-weight: normal">' + ques + "</label>" +
                        "<div class='input-group'>" +
                        "<span class='input-group-addon btn btn-secondary tts'><i class='fa fa-bullhorn'></i></span>" +
                        "<textarea class='form-control profile-question' rows='1' cols='70' id=" + 'ques' + question.tpq_id +
                        " name=" + 'ques_' + question.tpq_id + " class='answer' onkeyup='auto_grow(this)'" +
                        " placeholder='Click on the microphone and speak or type text.'" +
                        " data-id=" + question.tpq_id + ">"
                        + (null !== question.tpq_question ? question.tpq_question : '') + "</textarea>" +
                        "<span class='input-group-addon btn btn-secondary stt'><i class='fa fa-microphone'></i></span>" +
                        "</div></div>";
                });

                const canvas_container_selector = $('#canvas-container');
                canvas_container_selector.html('');
                canvas_container_selector.html('<h4 class="main-con-header">Profile Content</h4><form id="response-form"' +
                    ' class="form-horizontal" style="margin: 5px">' + '<h3><b>' +
                    $('#profile-phase option:selected').text() +
                    '</b></h3><div style="margin-left: 2%">' + question_input + '</div></form>'
                );

                // Fetches and displays list of profiles.
                $.ajax({
                    url: jsglobals.base_url + 'tsl/get_profile_list',
                    dataType: 'json',
                    data: {
                        outline_id: activeOutlineId,
                    },
                    type: 'post',
                    error: ajaxError
                }).done(function (data) {

                    if (data.status !== 'success') {
                        toastr.error(data.message);
                        return;
                    }
                    else if (data.status === 'redirect') {
                        window.location.href = data.redirect;
                        return;
                    }
                    updateProfileList(data.profile);

                }).fail(function (jqXHR, textStatus) {
                    toastr.error("Request failed: " + textStatus);
                }).always(function () {
                });

            }
        }
    ).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
    });

    $('.collab-back-button').off('click');
    $('.collab-back-button').on('click', function () {
        $('.full_view_hide').toggle();
        $('.profile-viewer').toggleClass('hidden');
        $('#canvas-container').toggleClass('full-screen');
        $('#profile_buttons').find('div').eq(1).toggleClass('col-xs-60');
    });
    // Buyer Profile Full Screen View.
    const expand_profile_selector = $('#record-profile');
    expand_profile_selector.unbind('click');
    expand_profile_selector.click(function (e) {
        // $('.profile_container').toggleClass('hidden');
        $('.full_view_hide').toggle();
        $('.profile-viewer').toggleClass('hidden');
        $('#canvas-container').toggleClass('full-screen');
        $('#profile_buttons').find('div').eq(1).toggleClass('col-xs-60');

        const expand_profile_selector = $('#expand-profile');
        // (null !== expand_profile_selector.html().match(/Expand/g)) ?
        //     expand_profile_selector.html('Return <i class="fa fa-arrows""></i>') :
        //     expand_profile_selector.html('Expand <i class="fa fa-arrows""></i>');

        // Fetch Profile Questions and Answers.
        obj = {"data-id": activeProfileId};
        setupProfile(obj);
    });

    // Save profile click handler.
    $('#save-profile-question').off('click');
    $('#save-profile-question').click(function () {
        let data = $('#response-form').serializeArray(),
            id_array = [];
        $('#response-form textarea').each(function (index, text) {
            id_array.push($(text).attr('data-id'));
        });

        data.push({name: 'question_id', value: id_array});
        const update_profile_question = makeAjaxCall('tsl/update_profile_questions', data);
        update_profile_question.then(function (data) {
            toastr.success('Profile Questions Updated', 'Success!!');
        })
    })
}

// Fetches Buyer profile outline.
function refreshProfilePage() {
    // Display buyer's profile buttons.
    $('.email_attachment').css("display", "none");
    $('#profile_buttons').css("display", "block");

    $.ajax({
        url: jsglobals.base_url + 'tsl/get_profile_outline',
        dataType: 'json',
        type: 'post',
        data: {
            profile_phase: $('#profile-phase').val(),
            profile_type: $('#profile-type').val(),
            project_type: $('#project-type').val(),
            client_type: $('#client-type').val(),
        },
        error: ajaxError
    }).done(function (data) {

        if (data.status !== 'success') {
            toastr.error(data.message);
            return;
        }
        else if (data.status === 'redirect') {
            window.location.href = data.redirect;
            return;
        }

        outlines = data.profile_outline;
        updateOutline(data.profile_outline);
    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
        // Check if the component is a dummy.
        check_dummy_component(client_id);
    });
}

// Display selected Profile outline.
function updateOutline(outlines) {
    let disabled = '',
        is_editable = '';


    // List outline versions.
    $('#outline-table').find('tbody tr').remove();
    $.each(outlines, function (index, outline) {

        // Enable/Disable delete button.
        (outline.tpo_id === '0') ? disabled = 'disabled' : disabled = is_editable = 'editable';

        $('#outline-table').find('tbody')
            .append($('<tr>')
                .append($('<td class="text-center">')
                    .append(index + 1))
                .append($('<td class="text-center">')
                    .append($('<a class="profile-outline" data-id="' + outline.tpo_id + '" data-val="' + index + '">' +
                        '<span class="fa fa-eye"></span>' +
                        '<span class="' + is_editable + '" data-type="text" data-pk="' + outline.tpo_id + '" ' +
                        'data-url="' + jsglobals.base_url +
                        'tsl/update_profile_outline_name">' + outline.tpo_name + '</span></a>'
                    )))
                .append($('<td class="text-center">')
                    .append('<button class="btn btn-default btn-sm" data-toggle="modal"' +
                        ' data-target="#delete-bus-outline-dialog" data-backdrop="static"' +
                        ' data-id="' + outline.tpo_id + '" ' +
                        ' data-name="' + outline.tpo_name + '" ' + disabled + '>' +
                        ' <span class="glyphicon glyphicon-trash"></span></button>'))
            )
        ;
    });

    const active_profile_outline_selector =
        $('.profile-outline[data-id=' + localStorage.active_profile_outline_id + ']');

    // Check for the last used outline ID.
    (undefined !== localStorage.active_profile_outline_id &&
        active_profile_outline_selector.length !== 0) ?
        active_profile_outline_selector.trigger('click') :
        $('.profile-outline[data-id=0]').trigger('click');

}


// Fetches phases for TSL.
function get_phase() {
    const get_survey_phase = makeAjaxCall('tsl/get_phase', {
        'id': activeOutlineId,
        'type': 'profile'
    });

    get_survey_phase.then(function (data) {

        $('#profile-phase option').remove();
        $('#profile-phase').append(
            $.map(data.phases, function (phase, index) {
                return '<option value="' + phase.cp_id + '" data-value="' + phase.cp_content + '" ' +
                    'data-name="' + phase.cp_name + '" data-number="' + phase.cp_number + '">' + phase.cp_name + '</option>'
            }));
        put_content(outlines);
    });
}

// Puts up the content.
function put_content(outlines) {
    const profile_outline_selector = $('.profile-outline'),
        outline_container_selector = $('#outline-container');

    // Displays the default outline if no outline present.
    if (outlines.length === 0) {
        let default_outline;
        $.ajax({
            url: jsglobals.base_url + 'tsl/get_profile_default',
            dataType: 'json',
            type: 'post',
            data: {
                profile_type: $('#profile-type').val(),
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            // default_outline = data.default_outline[0].tpd_content;
            outline_container_selector.html('');
            outline_container_selector.html('<h4 class="main-con-header">Profile outline</h4><form id="outline-form">' +
                '<input type="hidden" id="profile_phase" name="phase">' +
                '<input type="hidden" id="requester" name="requester">' +
                '<input id="y" value="' + $('#profile-phase option:selected').data('value') + '" type="hidden" name="content">\n' +
                '<trix-editor input="y" name="tsl_profile_editor" id="trix_profile_editor"></trix-editor>' +
                '</form>');

            obj = {"data-id": 0};
            setupProfile(obj);
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });

    } else if (activeOutlineId === 0) {
        // Display first outline, if no outline is selected.
        outline_container_selector.html('');
        outline_container_selector.html('<h4 class="main-con-header">Profile outline</h4><form id="outline-form">' +
            '<input type="hidden" id="profile_phase" name="phase">' +
            '<input type="hidden" id="requester" name="requester">' +
            '<input id="y" value="' + $('#profile-phase option:selected').data('value') + '" type="hidden" name="content">\n' +
            '<trix-editor input="y" name="tsl_profile_editor" id="trix_profile_editor"></trix-editor>' +
            '</form>');

        obj = {"data-id": outlines[0].tpo_id};
        obj = {"data-id": outlines[0].tpo_id};
        setupProfile(obj);
    } else if (profile_added === 1) {
        // If new profile is added.
        obj = {"data-id": activeOutlineId};
        setupProfile(obj);
    } else if (outline_added === 1) {
        outline_added = 0;
        profile_outline_selector.last().click();
    } else {
        // Display first outline, if no outline is selected.
        outline_container_selector.html('');
        outline_container_selector.html('<h4 class="main-con-header">Profile outline</h4><form id="outline-form">' +
            '<input type="hidden" id="profile_phase" name="phase">' +
            '<input type="hidden" id="requester" name="requester">' +
            '<input id="y" value="' + $('#profile-phase option:selected').data('value') + '" type="hidden" name="content">\n' +
            '<trix-editor input="y" name="tsl_profile_editor" id="trix_profile_editor"></trix-editor>' +
            '</form>');

        obj = {"data-id": activeOutlineId};
        setupProfile(obj);
    }
}

// Display list of Buyer's profile instances.
function updateProfileList(profiles) {

    $('#tsl-profile-nav').empty();

    $('#tsl-profile-nav')
        .append($('<li class="text-center" id="0">')
            .append('<a href="#" onclick="setupAnswer(this); return false;" data-id="0">Example Profile</a></li>'))

    // Append to the profile list.
    $.each(profiles, function (index, profile) {
        $('#tsl-profile-nav')
            .append($('<li class="text-center" id="' + profile.tsl_profile_id + '">')
                .append('<a href="#" class="edit-ref" onclick="setupAnswer(this); return false;"' +
                    ' data-name="' + profile.tsl_profile_name + '"data-id="' + profile.tsl_profile_id + '">' +
                    '<span class="editable" data-type="text" data-pk = "' + profile.tsl_profile_id + '" ' +
                    'data-url = "' + jsglobals.base_url + 'tsl/update_profile_name">' + profile.tsl_profile_name
                    + '</span></a></li>'))
    });

    $('#send-profile').removeAttr('data-toggle');
    $('#delete-profile').removeAttr('data-toggle');


    // Check if reference if undefined (Request is not from sent popup)
    if (!(ref !== undefined && activeProfileId === -1)) {
        if (activeProfileId === 0 || activeProfileId === -1) {
            obj = {"data-id": 0, "data-name": 'Example Profile'};
            setupAnswer(obj);
        } else if (profile_added === 1) {
            $('#tsl-profile-nav li').last().children().click();
        } else if (ref !== undefined) {
            $("a[data-id=" + ref + "]").trigger('click');
        }
    } else {

        // Fetch the Profile Details.
        const profile_details = makeAjaxCall('cpdv1/get_profile_details', {profile_id: ref});

        profile_details.then(function (data) {
            const response = data.profile_details[0];
            $('#client-type').val(response.tpo_client_type);
            $('#profile-class').val(response.tpo_project_type);
            $('#profile-type').val(response.tpo_profile_type);
            $('#profile-phase').val(response.tpo_profile_phase);
            $('.profile-outline[data-id=' + response.tsl_profile_outline_id + ']').click();
        });
    }
    setEditable('bottom');
}

// Fetch and display already answered questions for a profile.
function setupAnswer(obj) {

    activeProfileId = $(obj).attr('data-id');

    // Displaying the active email name.
    // $('.active').removeClass('active');
    // $('#' + activeProfileId).addClass('active');

    // Fetch and display default answers for survey.
    if (0 === parseInt($(obj).attr('data-id'))) {
        // const fetch_default_answers = makeAjaxCall('tsl/get_survey_default_answer',
        //     {
        //         'type': 'profile',
        //         phase: $('#profile-phase').prop('selectedIndex') + 1
        //     });
        //
        // fetch_default_answers.then(function (data) {
        //     $.each(data.default_answers, function (index, answer) {
        //         $('#response-form textarea').eq(index).val(answer.tsda_content);
        //     });
        //
        //     // Auto grow script form text area size.
        //     $('#response-form textarea').each(function () {
        //         $(this).css('height', $(this)[0].scrollHeight + 'px');
        //     });
        // });

    } else {

        // Set attribute for send button.
        const send_profile_selector = $('#send-profile'),
            delete_profile_selector = $('#delete-profile');

        send_profile_selector.attr('data-id', activeOutlineId);
        send_profile_selector.attr('data-toggle', 'modal');
        send_profile_selector.attr('data-profile', activeProfileId);
        send_profile_selector.attr('data-target', '#send-bus-dialog');
        send_profile_selector.attr('data-backdrop', 'static');
        send_profile_selector.attr('data-name', $(obj).attr('data-name'));

        // Set attribute for delete button.
        delete_profile_selector.attr('data-id', activeProfileId);
        delete_profile_selector.attr('data-toggle', 'modal');
        delete_profile_selector.attr('data-target', '#delete-profile-dialog');
        delete_profile_selector.attr('data-backdrop', 'static');
        delete_profile_selector.attr('data-name', $(obj).attr('data-name'));

        // Fetches the list of already answered questions.
        $.ajax({
            url: jsglobals.base_url + 'profile/get_ans',
            dataType: 'json',
            data: {
                profile_id: activeProfileId,
                question_id: questions_id,
                client_id: client_id
            },
            type: 'post',
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            // Clears answer field.
            $('.answer').val('');
            $.each(data.answers, function (index, answer) {
                $('#ans' + answer.tsl_profile_ans_ques_id).val(answer.tsl_profile_ans_content.replace('\\', ''));
                // Auto grow script form text area size.
                $('#response-form textarea').each(function () {
                    $(this).css('height', $(this)[0].scrollHeight + 'px');
                });
            })
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    }

    // Makes the names editable.
    $(".edit-ref").children().click(function (event) {
        event.stopPropagation();
    });

    speechFunctionality();
}