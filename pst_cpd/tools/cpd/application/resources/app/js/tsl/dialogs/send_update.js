$(document).ready(function () {
    let client_id, client_email, outline_id, origin, type, update_id, profile_id;

    // Get client Id from url.
    const path = window.location.pathname;
    const components = path.split('/');
    client_id = components[components.length - 1];
    origin = components[2];
    type = components[3];

    // When the dialog is displayed, set the current instance ID.
    $('#send-update-dialog').on('show.bs.modal', function (event) {

        outline_id = $(event.relatedTarget).data('id');
        update_id = $(event.relatedTarget).data('update');
        profile_id = $(event.relatedTarget).data('profile');

        // Get the client email ID.
        $.ajax({
            url: jsglobals.base_url + 'tsl/get_client_email',
            dataType: 'json',
            type: 'post',
            data: {
                client_id: client_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            // Display client email Id.
            $('#client-email').text(data.client[0].tc_email);

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });

        // Embed text Button Click Handler.
        $('#embed-update-content').off('click');
        $('#embed-update-content').click(function () {
            $.each($('#response-form textarea'), function (index, element) {
                $('.span_ans').eq(index).text($(element).val());
            });

            let content = $('#response-form').html()
                .replace(/<label(.*?)<\/label>/g, '')
                .replace(/<textarea(.*?)<\/textarea>/g, '')
                .replace(/<span class="hidden span_ans">/g, '<span>')
                .replace(/<div class="input-group"><span class="input-group-addon btn btn-secondary tts"><i class="fa fa-bullhorn"><\/i><\/span><span class="input-group-addon btn btn-secondary stt"><i class="fa fa-microphone"><\/i><\/span><\/div>/g, '')
                .replace(/<\/span><br><span>/g, '')
                .replace(/<br><span>/g, '')
                .replace(/<h4>Part 1 – /, '<h4>')
                .replace(/<h4>Part 2 – /, '<h4>')
                .replace(/<h4>Part 3 – /, '<h4>')
                .replace(/<h4>Part 4 – /, '<h4>')
                .replace(/<h4>Part 5 – /, '<h4>');

            console.log(content);
            content = tinyMCE.activeEditor.getContent() + content;
            tinyMCE.activeEditor.setContent(content);
        });

        const update_update_selector = $('#update-email-content')
        update_update_selector.html('');

        tinymce.init({
            selector: '#update-email-content',
            branding: false,
            init_instance_callback: function (ed) {
                tinyMCE.DOM.setStyle(tinyMCE.DOM.get('update-email-content_ifr'), 'height', '55vh');
            },
            toolbar: "imageupload",
            setup: function (editor) {
                const inp =
                    $('<input id="tinymce-uploader" type="file" name="pic" accept="image/*" style="display:none">');
                $(editor.getElement()).parent().append(inp);

                inp.on("change", function () {
                    const input = inp.get(0),
                        file = input.files[0],
                        fr = new FileReader();

                    fr.onload = function () {
                        var img = new Image();
                        img.src = fr.result;
                        editor.insertContent('<img src="' + img.src + '"/>');
                        inp.val('');
                    };
                    fr.readAsDataURL(file);
                });

                editor.addButton('imageupload', {
                    text: "IMAGE",
                    icon: false,
                    onclick: function (e) {
                        inp.trigger('click');
                    }
                });
            }
        });
    });

    $(document).on('click', '#close-send-update-dialog', function () {
        $('#send-update-dialog').modal('hide');
    });

    // Sends email to the client .
    $(document).on('submit', '#send-update-form', function (ev) {

        ev.preventDefault();
        ev.stopPropagation();
        enableLoader();

        $.ajax({
            url: jsglobals.base_url + 'tsl/send_bus',
            dataType: 'json',
            type: 'post',
            data: {
                text: tinymce.activeEditor.getContent(),
                subject: $('#update-client-subject').val(),
                client_id: client_id,
                client_email: $('#update-client-email').val(),
                outline_id: outline_id,
                update_id: update_id,
                profile_id: profile_id,
                origin: origin,
                type: type,
                tool_id: window.location.search.split('?id=')[1]
            },
            error: ajaxError
        }).done(function (data) {
            $.unblockUI();

            if (data.status != 'success') {
                toastr.error(data.message, 'Invalid Email ID');
                return;
            }
            else if (data.status == 'redirect') {

                window.location.href = data.redirect;
                return;
            }

            toastr.success('Email Sent!!');
            $('#send-update-dialog').modal('hide');

        }).fail(function (jqXHR, textStatus) {
            $.unblockUI();
            toastr.error('Please check EMail ID provided.', 'Unable to Send Email!!');
            return false;
        }).always(function () {
        });

        return false;
    })
});
