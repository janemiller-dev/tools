$(document).ready(function () {
    var tsl_id = '';
    var ui_name = '';

    // When the dialog is displayed, tsl the current instance ID.
    $('#delete-tsl-dialog').on('show.bs.modal', function (event) {
        tsl_id = $(event.relatedTarget).data('id');
        ui_name = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name')

        $('#tsl-name').html(ui_name);
    });

    $(document).on('click', '#close-delete-tsl-button', function () {
        $('#delete-tsl-dialog').modal('hide');
    });

    // Removes selected message tsl.
    $(document).on('submit', '#delete-tsl-form', function (e) {
        e.preventDefault();

        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_tsl',
            dataType: 'json',
            type: 'post',
            data: {
                tsl_id: tsl_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('TSL instance Deleted', 'Success!!');
            $('#delete-tsl-dialog').modal('hide');
            refreshPage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });

        return false;
    });
});
