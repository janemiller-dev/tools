$(document).ready(function () {
    let client_id, client_email, outline_id, origin, type, survey_id, profile_id;

    // Get client Id from url.
    const path = window.location.pathname;
    const components = path.split('/');
    client_id = components[components.length - 1];
    origin = components[2];
    type = components[3];

    // When the dialog is displayed, set the current instance ID.
    $('#send-bus-dialog').on('show.bs.modal', function (event) {

        outline_id = $(event.relatedTarget).data('id');
        survey_id = $(event.relatedTarget).data('survey');
        profile_id = $(event.relatedTarget).data('profile');

        // Get the client email ID.
        $.ajax({
            url: jsglobals.base_url + 'tsl/get_client_email',
            dataType: 'json',
            type: 'post',
            data: {
                client_id: client_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            // Display client email Id.
            $('#client-email').text(data.client[0].tc_email);

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });


        const bus_survey_selector = $('#bus-email-content')
        bus_survey_selector.html('');

        tinymce.init({
            selector: '#bus-email-content',
            branding: false,
            init_instance_callback: function (ed) {
                tinyMCE.DOM.setStyle(tinyMCE.DOM.get('bus-email-content_ifr'), 'height', '55vh');
            },
            toolbar: "imageupload",
            setup: function (editor) {
                const inp =
                    $('<input id="tinymce-uploader" type="file" name="pic" accept="image/*" style="display:none">');
                $(editor.getElement()).parent().append(inp);

                inp.on("change", function () {
                    const input = inp.get(0),
                        file = input.files[0],
                        fr = new FileReader();
                    fr.onload = function () {
                        var img = new Image();
                        img.src = fr.result;
                        editor.insertContent('<img src="' + img.src + '"/>');
                        inp.val('');
                    };
                    fr.readAsDataURL(file);
                });

                editor.addButton('imageupload', {
                    text: "IMAGE",
                    icon: false,
                    onclick: function (e) {
                        inp.trigger('click');
                    }
                });
            }
        });
    });

    $(document).on('click', '#close-send-bus-dialog', function () {
        $('#send-bus-dialog').modal('hide');
    });

    // Sends email to the client .
    $(document).on('submit', '#send-bus-form', function (ev) {

        ev.preventDefault();
        ev.stopPropagation();
        enableLoader();

        $.ajax({
            url: jsglobals.base_url + 'tsl/send_bus',
            dataType: 'json',
            type: 'post',
            data: {
                text: tinymce.activeEditor.getContent(),
                subject: $('#bus-client-subject').val(),
                client_id: client_id,
                client_email: $('#bus-client-email').val(),
                outline_id: outline_id,
                survey_id: survey_id,
                profile_id: profile_id,
                origin: origin,
                type: type,
                phase: $('#profile-phase').val(),
                tool_id: window.location.search.split('?id=')[1]
            },
            error: ajaxError
        }).done(function (data) {
            $.unblockUI();

            if (data.status !== 'success') {
                toastr.error(data.message, 'Invalid Email ID');
                return;
            }
            else if (data.status === 'redirect') {

                window.location.href = data.redirect;
                return;
            }

            toastr.success('Email Sent!!');
            $('#send-bus-dialog').modal('hide');

        }).fail(function (jqXHR, textStatus) {
            $.unblockUI();
            toastr.error('Please check EMail ID provided.','Unable to Send Email!!');
            return false;
        }).always(function () {
        });

        return false;
    })
});
