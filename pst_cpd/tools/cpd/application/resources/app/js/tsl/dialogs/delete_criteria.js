$(document).ready(function () {
    let criteria_id = '', ui_name = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-criteria-dialog').on('show.bs.modal', function (event) {
        criteria_id = $(event.relatedTarget).data('id');
        ui_name = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name')

        $('#criteria-id').val(criteria_id);
        $('#delete-criteria-name').html(ui_name);
        $('#view-criteria-dialog').modal('hide');
    });

    // Deletes the criteria for a user.
    $(document).on('submit', '#delete-criteria-form', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_criteria',
            dataType: 'json',
            type: 'post',
            data: {
                criteria_id: criteria_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            toastr.success('Criteria Deleted.', 'Success!!');
            $('#delete-criteria-dialog').modal('hide');

            // Check if refreshDashboard is a function.
            get_all_criteria();
            refreshPage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error('You have Assets belonging to this Criteria.', 'Error!!');
        }).always(function () {
        });
        return false;
    });
});
