$(document).ready(function () {
    var promo_outline_id = '';
    var version = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-promo-outline-dialog').on('show.bs.modal', function (event) {
        promo_outline_id = $(event.relatedTarget).data('id');
        version = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name');

        $('#promo-outline-id').val(promo_outline_id);
        $('#delete-promo-outline-version').html(version);
        $('#view-promo-outline-dialog').modal('hide');
    });

    // Deletes Buyer's survey Outline.
    $(document).on('submit', '#delete-promo-outline-form', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_promo_outline',
            dataType: 'json',
            type: 'post',
            data: {
                promo_outline_id: promo_outline_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            activeOutlineId = 0;
            toastr.success('Survey Outline Deleted.', 'Success!!');
            $('#delete-promo-outline-dialog').modal('hide');
            refreshPage();

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
        return false;
    });
});
