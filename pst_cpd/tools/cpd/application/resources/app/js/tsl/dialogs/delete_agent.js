$(document).ready(function () {
    var agent_id = '';
    var ui_name = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-agent-dialog').on('show.bs.modal', function (event) {
        agent_id = $(event.relatedTarget).data('id');
        ui_name = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name')

        $('#agent-id').val(agent_id);
        $('#delete-agent-name').html(ui_name);
        $('#view-agent-dialog').modal('hide');
    });

    // Deletes the prepared by agent name.
    $(document).on('submit', '#delete-agent-form', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_agent',
            dataType: 'json',
            type: 'post',
            data: {
                agent_id: agent_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('Prepared By Name deleted.', 'Success!!');
            $('#delete-agent-dialog').modal('hide');
            refreshDashboard();

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
        return false;
    });
});
