$(document).ready(function () {
    var outline_id = '';
    var name = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-bus-outline-dialog').on('show.bs.modal', function (event) {
        outline_id = $(event.relatedTarget).data('id');
        name = $(event.relatedTarget).data('name');

        // Set the title for delete dialog.
        const title_selector = $('#delete-bus-outline-title');
        (requester === 'profile') ?
            title_selector.text('Delete Profile Outline?') :
            title_selector.text('Delete Survey Outline?');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name');

        $('#outline-id').val(outline_id);
        $('#delete-outline-version').html(name);
        $('#view-outline-dialog').modal('hide');
    });

    // Deletes Buyer's survey Outline.
    $(document).on('submit', '#delete-bus-outline-form', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_bus_outline',
            dataType: 'json',
            type: 'post',
            data: {
                outline_id: outline_id,
                type: requester
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('Survey Outline Deleted.', 'Success!!');
            $('#delete-bus-outline-dialog').modal('hide');

            // Check if the refreshBusPage method exist.
            (typeof(refreshBusPage) !== "undefined") ? refreshBusPage() : refreshProfilePage();

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
        return false;
    });
});
