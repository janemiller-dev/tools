$(document).ready(function () {
    let client_email, tac_id, tc_id,
        path = window.location.pathname,
        components = path.split('/');

    // When the dialog is displayed, set the current instance ID.
    $('#save-client-info-dialog').on('show.bs.modal', function (event) {
        tac_id = $(event.relatedTarget).data('id');
        tc_id = $(event.relatedTarget).data('tc-id');

        // Set the active DMD name.
        $('#active-cpd-name').html($('#linked-cpd').clone());
        $('.no-content').hide();

        if ($('#result' + tac_id).val() == null) {
            toastr.warning('Please select Result first.', 'Warning!!');
            event.stopPropagation();
            event.preventDefault();
            return;
        } else if ('maybe' === $('#result' + tac_id).val()) {
            // toastr.info('All Maybe Must remain on TSL.');
            // event.stopPropagation();
            // event.preventDefault();
            $('.maybe-content').show();
            $('#save-client-button').hide();
            return;
        } else if ('no' === $('#result' + tac_id).val()) {
            $('.no-content').show();
        }
    });

    // Revert to homebase click handler.
    $(document).on('click', '#revert-to-homebase', function (e) {
        e.preventDefault();
        e.stopPropagation();

        const remove_prev_instance = makeAjaxCall('homebase/delete_prev_instance', {
            'table_name': 'tsl_client',
            'tc_id': tc_id,
            'deal_id': ''
        });

        remove_prev_instance.then(function (data) {
            toastr.success('Asset reverted back to homebase!!', 'Succes!!');
            $('#save-client-info-dialog').modal('hide');
            refreshPage();
        })
    });

    $(document).on('click', '#move-identified', function (e) {
        e.preventDefault();
        e.stopPropagation();

        const move_identified = makeAjaxCall('tsl/move_to_identified', {
            tac_id: tac_id,
            tc_id: tc_id
        });

        move_identified.then(function (data) {
            toastr.success('Asset Moved to Identified!!', 'Succes!!');
            $('#save-client-info-dialog').modal('hide');
            refreshPage();
        })
    });

    // Move TSL client info to DMD.
    $(document).on('submit', '#save-client-form', function (ev) {
        ev.preventDefault();
        ev.stopPropagation();

        $.ajax({
            url: jsglobals.base_url + 'tsl/move_deal',
            dataType: 'json',
            type: 'post',
            data: {
                tac_id: tac_id,
                cpd_id: cpd_id,
                tc_id: tc_id
            },
            error: ajaxError
        }).done(function (data) {
            if (data.status != 'success') {
                console.log(data.message);
                return;
            } else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            } else if (data.deal_moved === false) {
                toastr.error('Please create one or mark as active.', 'No Active DMD instance present.');
                return;
            }

            toastr.success('Deal pushed to DMD!!');
            $('#save-client-info-dialog').modal('hide');
            $('.tsl-tabs li.active').trigger('click');
        }).fail(function (jqXHR, textStatus) {
            console.log("Request failed: ");
            console.log(textStatus);
        }).always(function () {
        });

        return false;
    })
});
