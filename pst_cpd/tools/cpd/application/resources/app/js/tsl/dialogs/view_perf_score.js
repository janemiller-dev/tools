var client_id;
var script_id;
$(document).ready(function () {
    // Get the client id and script id from url.
    var path = window.location.pathname;
    var components = path.split('/');
    client_id = components[components.length - 1];
    script_id = $('#perf-prof-score').data('id');

    // Fetches the performance score.
    $('#view-perf-score-dialog').on('show.bs.modal', function (event) {
        $.ajax({
            url: jsglobals.base_url + 'tsl/get_perf_score',
            dataType: 'json',
            data: {
                client_id: client_id,
                script_id: activeId,
            },
            type: 'post',
            error: ajaxError
        }).done(function (data) {
            if (data.status != 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            updatePrefScore(data.score);

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {

        });
        $('.active').removeClass('active');
        $('#performance').addClass('active');

    });
});

// Displays the performance score.
function updatePrefScore(scores) {
    $('#perf-score-table tbody').empty();

    // Array to store sum of performance score.
    // var sum = [];
    var sum = {'tspp_perf1':0, 'tspp_perf2' : 0, 'tspp_perf3' : 0, 'tspp_perf3a' : 0, 'tspp_perf3b' : 0, 'tspp_perf3c' : 0
    , 'tspp_perf3d' : 0, 'tspp_perf3e' : 0, 'tspp_perf4' : 0, 'tspp_perf5' : 0, 'total' : 0};

    // Calculate total for each column and appends the score to the performance table.
    $.each(scores, function (index, score) {

        var total = parseInt(score.tspp_perf1) + parseInt(score.tspp_perf2) + parseInt(score.tspp_perf3) + parseInt(score.tspp_perf3a) + parseInt(score.tspp_perf3b) + parseInt(score.tspp_perf3c) + parseInt(score.tspp_perf3d) + parseInt(score.tspp_perf3e) + parseInt(score.tspp_perf4) + parseInt(score.tspp_perf5);
        sum['tspp_perf1'] += parseInt(score.tspp_perf1);
        sum['tspp_perf2'] += parseInt(score.tspp_perf2);
        sum['tspp_perf3'] += parseInt(score.tspp_perf3);
        sum['tspp_perf3a'] += parseInt(score.tspp_perf3a);
        sum['tspp_perf3b'] += parseInt(score.tspp_perf3b);
        sum['tspp_perf3c'] += parseInt(score.tspp_perf3c);
        sum['tspp_perf3d'] += parseInt(score.tspp_perf3d);
        sum['tspp_perf3e'] += parseInt(score.tspp_perf3e);
        sum['tspp_perf4'] += parseInt(score.tspp_perf4);
        sum['tspp_perf5'] += parseInt(score.tspp_perf5);
        sum['total'] += total;

        $('#perf-score-table tbody')
            .append($('<tr>')
                .append($('<td class="text-center">')
                    .append(index + 1))
            .append($('<td class="text-center">')
                    .append(score.tspp_datetime))
            .append($('<td class="text-center">')
                    .append(score.tspp_perf1))
            .append($('<td class="text-center">')
                .append(score.tspp_perf2))
            .append($('<td class="text-center">')
                .append(score.tspp_perf3))
            .append($('<td class="text-center">')
                .append(score.tspp_perf3a))
            .append($('<td class="text-center">')
                .append(score.tspp_perf3b))
            .append($('<td class="text-center">')
                .append(score.tspp_perf3c))
            .append($('<td class="text-center">')
                .append(score.tspp_perf3d))
            .append($('<td class="text-center">')
                .append(score.tspp_perf3e))
            .append($('<td class="text-center">')
                .append(score.tspp_perf4))
            .append($('<td class="text-center">')
                .append(score.tspp_perf5))
            .append($('<td class="text-center">')
                .append(total))
            )
    });

    // Append total for each performnace column.
    $('#perf-score-table tbody')
        .append($('<tr>')
            .append($('<td class="text-center">')
                .append('Total'))
            .append($('<td class="text-center">')
                .append((20*(($('#perf-score-table tr').length)-1))))
            .append($('<td class="text-center">')
                .append(sum['tspp_perf1']))
            .append($('<td class="text-center">')
                .append(sum['tspp_perf2']))
            .append($('<td class="text-center">')
                .append(sum['tspp_perf3']))
            .append($('<td class="text-center">')
                .append(sum['tspp_perf3a']))
            .append($('<td class="text-center">')
                .append(sum['tspp_perf3b']))
            .append($('<td class="text-center">')
                .append(sum['tspp_perf3c']))
            .append($('<td class="text-center">')
                .append(sum['tspp_perf3d']))
            .append($('<td class="text-center">')
                .append(sum['tspp_perf3e']))
            .append($('<td class="text-center">')
                .append(sum['tspp_perf4']))
            .append($('<td class="text-center">')
                .append(sum['tspp_perf5']))
            .append($('<td class="text-center">')
                .append(sum['total']))

        );

    // Fetches score between two given dates.
    $('#perf-date-submit').click(function () {
        var to_date = $('#perf-to-date').val();
        var from_date = $('#perf-from-date').val();

        $.ajax({
            url: jsglobals.base_url + 'tsl/get_perf_score',
            dataType: 'json',
            data: {
                to_date: to_date,
                from_date: from_date,
                client_id: client_id,
                script_id: activeId,
            },
            type: 'post',
            error: ajaxError
        }).done(function (data) {
            if (data.status != 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            updatePrefScore(data.score);

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {

        });
    });

    // Clears proficiency score board.
    $('#prof-score-table tbody').empty();

    // Array to store sum of proficiency scores.
    var sum = {'tspp_prof1' : 0, 'tspp_prof2' : 0, 'tspp_prof3' : 0, 'tspp_prof3a' : 0, 'tspp_prof3b' : 0, 'tspp_prof3c' : 0,
     'tspp_prof3d' : 0, 'tspp_prof3e' : 0, 'tspp_prof4' : 0, 'tspp_prof5' : 0, 'total' : 0};

    // Calculate sum of each proficiency column and display proficiency scores.
    $.each(scores, function (index, score) {

        // Maximum possible score.
        var total = parseInt(score.tspp_prof1) + parseInt(score.tspp_prof2) + parseInt(score.tspp_prof3) + parseInt(score.tspp_prof3a) + parseInt(score.tspp_prof3b) + parseInt(score.tspp_prof3c) + parseInt(score.tspp_prof3d) + parseInt(score.tspp_prof3e) + parseInt(score.tspp_prof4) + parseInt(score.tspp_prof5);

        // Sum of each column
        sum['tspp_prof1'] += parseInt(score.tspp_prof1);
        sum['tspp_prof2'] += parseInt(score.tspp_prof2);
        sum['tspp_prof3'] += parseInt(score.tspp_prof3);
        sum['tspp_prof3a'] += parseInt(score.tspp_prof3a);
        sum['tspp_prof3b'] += parseInt(score.tspp_prof3b);
        sum['tspp_prof3c'] += parseInt(score.tspp_prof3c);
        sum['tspp_prof3d'] += parseInt(score.tspp_prof3d);
        sum['tspp_prof3e'] += parseInt(score.tspp_prof3e);
        sum['tspp_prof4'] += parseInt(score.tspp_prof4);
        sum['tspp_prof5'] += parseInt(score.tspp_prof5);
        sum['total'] += total;

        // Append proficiency scores.
        $('#prof-score-table tbody')
            .append($('<tr>')
                .append($('<td class="text-center">')
                    .append(index + 1))
                .append($('<td class="text-center">')
                    .append(score.tspp_datetime))
                .append($('<td class="text-center">')
                    .append(score.tspp_prof1))
                .append($('<td class="text-center">')
                    .append(score.tspp_prof2))
                .append($('<td class="text-center">')
                    .append(score.tspp_prof3))
                .append($('<td class="text-center">')
                    .append(score.tspp_prof3a))
                .append($('<td class="text-center">')
                    .append(score.tspp_prof3b))
                .append($('<td class="text-center">')
                    .append(score.tspp_prof3c))
                .append($('<td class="text-center">')
                    .append(score.tspp_prof3d))
                .append($('<td class="text-center">')
                    .append(score.tspp_prof3e))
                .append($('<td class="text-center">')
                    .append(score.tspp_prof4))
                .append($('<td class="text-center">')
                    .append(score.tspp_prof5))
                .append($('<td class="text-center">')
                    .append(total))
            )
    });

    // Append total for proficiency scores.
    $('#prof-score-table tbody')
        .append($('<tr>')
            .append($('<td class="text-center">')
                .append('Total'))
            .append($('<td class="text-center">')
                .append((50*(($('#prof-score-table tr').length)-1))))
            .append($('<td class="text-center">')
                .append(sum['tspp_prof1']))
            .append($('<td class="text-center">')
                .append(sum['tspp_prof2']))
            .append($('<td class="text-center">')
                .append(sum['tspp_prof3']))
            .append($('<td class="text-center">')
                .append(sum['tspp_prof3a']))
            .append($('<td class="text-center">')
                .append(sum['tspp_prof3b']))
            .append($('<td class="text-center">')
                .append(sum['tspp_prof3c']))
            .append($('<td class="text-center">')
                .append(sum['tspp_prof3d']))
            .append($('<td class="text-center">')
                .append(sum['tspp_prof3e']))
            .append($('<td class="text-center">')
                .append(sum['tspp_prof4']))
            .append($('<td class="text-center">')
                .append(sum['tspp_prof5']))
            .append($('<td class="text-center">')
                .append(sum['total']))

        );

    // Fetches score between two given dates.
    $('#perf-date-submit').click(function () {
        var to_date = $('#perf-to-date').val();
        var from_date = $('#perf-from-date').val();

        $.ajax({
            url: jsglobals.base_url + 'tsl/get_perf_score',
            dataType: 'json',
            data: {
                to_date: to_date,
                from_date: from_date,
                client_id: client_id,
                script_id: activeId,
            },
            type: 'post',
            error: ajaxError
        }).done(function (data) {
            if (data.status != 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            updatePrefScore(data.score);

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {

        });
    });

    // Set format for date time picker
    $('.tsl-onlydate').datetimepicker({
        format: 'YYYY-MM-DD',
        timepicker: false
    });
}