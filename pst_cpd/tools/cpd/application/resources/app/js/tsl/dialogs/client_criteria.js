$(document).ready(function () {

    // When the dialog is displayed, set the current instance ID.
    $('#client-criteria-dialog').on('show.bs.modal', function (event) {
        var asset_id = $(event.relatedTarget).data('id');

        // Make every of the buttons disabled for dummy content.
        if (-1 === asset_id) {
            $('#client-criteria-dialog :button').attr('disabled', true);
            $('.close-button').attr('disabled', false);
        }

        // Gets the list of criteria present for a client.
        $.ajax({
            url: jsglobals.base_url + 'tsl/get_client_criteria',
            dataType: 'json',
            data: {
                asset_id: asset_id
            },
            type: 'post',
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            updateCriteria(data.criteria);

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });

    // List the criteria selected for a client.
    function updateCriteria(criteria) {
        $('#client-criteria-table tbody').empty();

        // Show the criteria if there are any.
        if (criteria.length === 0) {
            $('#no-criteria').show();
            $('#criteria-div').hide();
        } else {
            $('#no-criteria').hide();
            $('#criteria-div').show();

            // Listing criteria.
            $('#client-criteria-table tbody')
                .append($('<tr>')
                    .append($('<td class="text-center">')
                        .append(1))
                    .append($('<td class="text-center">')
                        .append(criteria[0].uc_name))
                );

        }
    }
});

