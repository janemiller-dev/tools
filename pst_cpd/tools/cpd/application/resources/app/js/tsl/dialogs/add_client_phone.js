$(document).ready(function () {
    // Fetches the performance score.
    $('#add-client-phone-dialog').on('show.bs.modal', function (event) {
        $('#view-client-phone-dialog').modal('hide');
        const client_id = $(event.relatedTarget).attr('data-id');
        add_client_phone.init(client_id);
    });
});

let add_client_phone = function () {

    // Initialize and get the DMD ID.
    let init = (client_id) => {

        $('#save-phone').off('click');
        $('#save-phone').click(function () {
            const add_client_phone = makeAjaxCall('tsl/save_client_phone', {
                'id': client_id,
                'type': $('#client-phone-type').val(),
                'number': $('#client-phone-number').val()
            });

            add_client_phone.then(function (data) {
                $('#add-client-phone-dialog').modal('hide');
                refreshPage();
            })
        });
    };
    return {
        init: init
    };
}();