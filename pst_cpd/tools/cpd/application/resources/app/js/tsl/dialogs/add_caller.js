$(document).ready(function () {
    $('#add-caller-dialog').on('show.bs.modal', function (event) {
        $('#view-caller-dialog').modal('hide');
        $('#tsl-id').val(tsl_id);
    });

    // Form validation.
    $('#add-caller-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            caller_name: {
                validators: {
                    notEmpty: {
                        message: "The deal name is required."
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the add new caller form.
        var $form = $(e.target);
        var fv = $form.data('formValidation');

        // Get the form data and submit it.
        var caller_data = $form.serialize();

        $.ajax({
            url: jsglobals.base_url + 'tsl/add_caller',
            dataType: 'json',
            type: 'post',
            data: caller_data,
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            } else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('Caller Added!!', 'Success!!')
            fv.resetForm();
            $($form)[0].reset();

            $('#add-caller-dialog, #view-caller-dialog').modal('hide');
            refreshDashboard();

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });
});