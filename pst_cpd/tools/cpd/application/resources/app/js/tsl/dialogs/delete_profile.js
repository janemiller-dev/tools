$(document).ready(function () {
    var profile_id = '';
    var ui_name = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-profile-dialog').on('show.bs.modal', function (event) {
        profile_id = $(event.relatedTarget).data('id');
        ui_name = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name')

        $('#profile-name').html(ui_name);
        $('#view-profile-dialog').modal('hide');
    });

    $(document).on('click', '#close-delete-profile-dialog', function () {
        $('#delete-profile-dialog').modal('hide');
    });

    // Deletes Buyer Profile.
    $(document).on('submit', '#delete-profile-form', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_profile',
            dataType: 'json',
            type: 'post',
            data: {
                profile_id: profile_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            toastr.success('Profile Instance Deleted.', 'Success!!');
            $('#delete-profile-dialog').modal('hide');

            profile_added = 1;
            refreshProfilePage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
        return false;
    });
});
