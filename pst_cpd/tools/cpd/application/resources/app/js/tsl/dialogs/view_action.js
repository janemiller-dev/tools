$(document).ready(function () {
    $('#view-action-dialog').on('show.bs.modal', function (event) {
        $('#view-action-div input, #view-action-div textarea').val('');
        view_action.init(event);
    });

    view_action.add_event();
});

// View Notes available.
let view_action = function () {

    var max_seq = 0, client_id;

    // Initialize and get the DMD ID.
    let init = function (event) {
            client_id = $(event.relatedTarget).data('id');
            max_seq = 0;

            $('#view-alert-dialog').modal('hide');

            if (0 === client_id) {
                $('#view-action-dialog :button').attr('disabled', true);
                $('.close-button').attr('disabled', false);
            }

            // Clear any previous values.
            $('.additional-row').remove();
            $('.action-when').attr('value', '');
            $('.action-input').val('');

            add_whom();

            // Fetch the Notes Content.
            const action_data = makeAjaxCall('tsl/get_action_content', {
                'id': client_id
            });

            action_data.then(function (data) {
                let dummy_content;

                $.each(data.action_content, function (index, client) {
                    // Check if this row exists or not.
                    if (0 === $('#action_content' + client.ta_seq).length) {
                        dummy_content = $('#view-action-div').clone();

                        dummy_content.find('textarea')
                            .attr('data-seq', (parseInt(client.ta_seq)))
                            .attr('id', 'action_content' + (parseInt(client.ta_seq)));

                        dummy_content.find('.action-whom')
                            .attr('data-seq', (parseInt(client.ta_seq)))
                            .attr('id', 'action_whom' + (parseInt(client.ta_seq)));

                        dummy_content.find('.action-when')
                            .attr('data-seq', (parseInt(client.ta_seq)))
                            .attr('id', 'action_when' + (parseInt(client.ta_seq)))
                            .addClass('action-date');

                        dummy_content.find('#action_date0')
                            .attr('data-seq', (parseInt(client.ta_seq)))
                            .attr('id', 'action_date' + (parseInt(client.ta_seq)));

                        dummy_content.find('#delete-action0')
                            .attr('data-seq', (parseInt(client.ta_seq)))
                            .attr('id', 'delete-action' + (parseInt(client.ta_seq)))
                            .attr('data-id', client_id);

                        $('#action-row').append('<div class="col-xs-12 additional-row" style="margin-top: 10px">'
                            + '<hr>' + dummy_content.html() + '</div>');

                        max_seq = client.ta_seq;
                    }

                    // Set the content.
                    $('#action_whom' + max_seq).val((null !== client.ta_whom) ? client.ta_whom : -1);
                    $('#action_content' + max_seq).val(client.ta_content);
                    $('#action_when' + max_seq).val(moment(client.ta_when).format('DD-MMM-YY, h:mm A'));
                    $('#action_date' + max_seq).val(moment(client.ta_date).format('DD-MMM-YY, h:mm A'));
                    $('.delete-action').attr('data-id', client_id);
                });
                enable_date_picker();
                store_data();
            });

            $(document).off('click', '.delete-action');
            $(document).on('click', '.delete-action', function () {
                const delete_action = makeAjaxCall('tsl/delete_client_action', {
                    id: $(this).attr('data-id'),
                    seq: $(this).attr('data-seq')
                });

                delete_action.then(function (data) {
                    toastr.success('Action Deleted.', 'Success!!');
                    $('#view-action-dialog').modal('hide');
                })
            })
        },

        // Stores Notes data.
        store_data = function () {
            $('.action').unbind('change');
            $('.action').change(function () {

                if ('ta_whom' === $(this).data('name') && (0 === parseInt($(this).val()))) {
                    $(this).val(-1);
                    $('#view-action-dialog').modal('hide');
                    $('#view-assignee-dialog').modal('show');
                    return;
                }

                // Check if the value is set.
                if ($(this).val() !== '' && $(this).val() !== null) {
                    const update_action = makeAjaxCall('tsl/update_action_content', {
                        'id': client_id,
                        'col': $(this).data('name'),
                        'val': $(this).val(),
                        'seq': parseInt($(this).data('seq'))
                    });

                    update_action.then(function () {
                        $('#view-action-dialog').off('hidden.bs.modal');
                        $('#view-action-dialog').on('hidden.bs.modal', function () {
                            refreshPage();
                        });
                    })
                }
            });
        },

        //     Adds new column to the event List.
        add_event = function () {
            // Add a new event to the list.
            $('#add-action').click(function () {

                max_seq = parseInt(max_seq) + 1;

                dummy_content = $('#view-action-div').clone();

                dummy_content.find('textarea')
                    .attr('data-seq', max_seq)
                    .attr('id', 'action_content' + max_seq)
                    .val('');

                dummy_content.find('.action-whom')
                    .attr('data-seq', max_seq)
                    .attr('id', 'action_whom' + max_seq);

                dummy_content.find('#action_date0')
                    .attr('data-seq', max_seq)
                    .attr('id', 'action_date' + max_seq);

                dummy_content.find('.action-when')
                    .attr('data-seq', max_seq)
                    .attr('id', 'action_when' + max_seq)
                    .addClass('action-date')
                    .attr('value', '');

                dummy_content.find('#delete-action0')
                    .attr('data-seq', (max_seq))
                    .attr('id', 'delete-action' + max_seq)
                    .attr('data-id', client_id);

                // Append to the last of List.
                $('#action-row').append('<div class="col-xs-12 additional-row" style="margin-top: 10px">'
                    + '<hr>' + dummy_content.html() + '</div>');

                enable_date_picker();
                store_data();
            });
        },

        add_whom = function () {
            $('.action-whom').empty();

            $('.action-whom')
                .append($('<option disabled value="-1" selected> -- Select By Whom -- </option>'))
                .append($('<option value="0">Add Assignee</option>'));

            // Iterate over every instance of assignee.
            $.each(assignee, function (index, agent) {
                $('.action-whom')
                    .append('<option value="' + agent.tsl_assignee_id + '">'
                        + agent.tsl_assignee_name + '</option>');
            });
        },

        enable_date_picker = function () {
            $('.action-when').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss'
            });
        };

    return {
        init: init,
        add_event: add_event
    };
}();