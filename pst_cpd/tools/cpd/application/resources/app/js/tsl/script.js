var activeId, activeOutlineId, item_added, client_id, script_added, new_script_id,
    script_name_id, active_textarea, mediaRecorder, origin, req_type, max, content_name, outline_name,
    requester = 'Script';

$(document).ready(function () {

    const path = window.location.pathname;
    const components = path.split('/');
    client_id = components[components.length - 1];
    origin = components[2];
    req_type = components[3];

    // Disable buttons for Dummy component.
    (components[4] === '0') ?
        $('.script_button').attr('disabled', true) : $('.btn').attr('disabled', false);

    const id = window.location.search.split('?id=')[1];
    loadPrev();

    // Clear text click handler.
    $(document).on('click', '#clear-text', function () {
        ('0' !== activeId) ? $('#script-form').find('textarea').val('') : '';
    });

    // Remove roleplay click handler.
    $(document).on('click', '.remove-roleplay', function () {
        $(this).closest('.collaborator_row').remove()
    });

    $(document).on('click', '#prompt-script', function () {
        let condensed_content = '';
        // Set the content for prompter form.
        $.each($('#script-form textarea'), function (index, text) {
            condensed_content = condensed_content +
                '<b><u>' + $('#script-form').find('label').eq(index).text() + '</u></b>';
            let text_array = ($(text).val()).split('.');
            $.each(text_array, function (index, data) {
                condensed_content = condensed_content + '<p>' + data + '</p>';
            });
        });

        $('#credits').html('');

        $('#credits').html(condensed_content);

        $('#view-reader-dialog').modal('show');
    });

    // Adds Promo version to archive list.
    $('#add-script-archive').click(function (data) {
        const add_promo_archive = makeAjaxCall('tsl/add_archive', {
            'promo_id': activeId,
            'type': 'script',
            'asset_id': client_id,
            'datetime': moment().format("DD-MM-YY hh:mm A"),
            'topic': $('#list-criteria option:selected').text()
        });

        add_promo_archive.then(function (data) {
            toastr.success('Component Added to Archive!!');
        });
    });

    voiceRecognition();

    // Check if the request is from TSL or DMD.
    if ('tsl' === origin) {
        $('#script_header').empty();
        $('#script_header').html(
            '<div class="col-md-3 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Client Type</label>' +
            '<select class="form-control script-menu script-pre-menu client-location" id="target-audience">' +
            '<option>Owner</option>' +
            '<option>Buyer</option>' +
            '<option>Investor</option>' +
            '<option>Syndicator</option>' +
            '<option>Institutional</option>' +
            '</select>' +
            '</div>' +
            // '<div class="col-xs-15 script-box">' +
            // '<label class="col-xs-12" style="text-align: center">Promo Focus</label>' +
            // '<select class="form-control script-menu" id="promo-focus">' +
            // '<option value="rs">Recent Success</option>' +
            // '<option value="ld">Local Developments</option>' +
            // '<option value="rt">Regional Trends</option>' +
            // '<option value="mec">Macro Economic Challenges</option>' +
            // '<option value="ie">Industry Events</option>' +
            // '<option value="no">New Opportunity</option>' +
            // '</select>' +
            // '</div>' +
            '<div class="col-md-3 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Data Source</label>' +
            '<select class="form-control script-menu script-pre-menu client-criteria" id="list-criteria">' +
            '<option>Owner Profile</option>' +
            '<option>Asset Profile</option>' +
            '<option>Buyer Profile</option>' +
            '<option>Lender Profile</option>' +
            '<option>Staff Profile</option>' +
            '</select>' +
            '</div>' +
            '<div class="col-md-3 script-box" id="promo-focus-parent" data-toggle="popover"' +
            'data-placement="bottom" data-html="true">' +
            '<label class="col-xs-12 text-center">Analysis Type</label>' +
            '<select class="form-control">' +
            '<option>Initial Strategic Analysis</option>' +
            '<option>Detailed Analysis</option>' +
            '<option>Opinion of Value</option>' +
            '<option>Asset Underwriting</option>' +
            '<option>Marketing Proposal</option>' +
            '</select>'+
            '</div>' +
            '<div class="col-md-3 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Worksheet Section</label>' +
            '<select class="form-control script-menu" id="script-type">' +
            '<option value="ics">Cover Page -CNC</option>' +
            '<option value="ems">Welcome Page – Evoking Rapport–CTX</option>' +
            '<option value="pps">Strategic Page – Strategic Analysis - CNT</option>' +
            '<option value="plms">Recommendation Page – Forging Partnership - CNT</option>' +
            '<option value="plms">Table Of Contents Page - CNT</option>' +
            '<option value="plms">Program Back Page COM</option>' +
            '</select>' +
            '</div>'
        );

    } else {

        $('#script_header').html(
            '<div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Client Type</label>' +
            '<select class="form-control col-xs-8" id="client-type">' +
            '<option value="r">Radish</option>' +
            '<option value="w">Wheat</option>' +
            '<option value="t">Tree</option>' +
            '</select></div>' +
            '<div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">' +
            '<label class="col-xs-12" id="label-script-type" style="text-align: center">Event Type</label>' +
            '<select class="form-control col-xs-8" id="event-type">' +
            '<option value="ipm">In-Person Meeting</option>' +
            '<option value="pm">Phone Meeting</option>' +
            '<option value="wm">Web Meeting</option>' +
            '<option value="ie">Industry Event</option>' +
            '</select>' +
            '</div>' +
            '<div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Script Type</label>' +
            '<select class="form-control col-xs-8 script-menu" id="script-type"></select>' +
            '</div>'
        );

    }

    $('#script-type').change(function () {
        localStorage.setItem('active_script_type', $(this).val());
    })
    // Variables to check if a new script is added.
    item_added = 0;
    activeId = 0;
    activeOutlineId = 0;

    // Show/hide outlines/answers as the user click on view button.
    $('#view-script').click(function (e) {
        $('#outline-content').toggle();
        $('#outline-form').toggle();
        const view_script_selector = $('#view-script');

        // Set the Text for Outline button.
        view_script_selector.text().includes('Outline') ?
            view_script_selector.html('Description <i class="fa fa-eye"></i>') :
            view_script_selector.html('Outline <i class="fa fa-eye-slash"></i>');
    });


    // $('#upload_record').click(function (e) {
    //     $('#recording_upload').trigger('click');
    // });

    // Filter the collaboration data.
    $('#filter').off('click');
    $('#filter').click(function () {
        $('#filer-collab-dialog').modal('show')
    });

    // Check if request is from TSL page.
    // if ('tsl' === origin) {
    //     refreshTSLdropdown().then(function () {
    //         refreshDMDDropdown().then(function (data) {
    //             refreshPage();
    //         })
    //     });
    // } else {
        refreshPage();
    // }

    // Jquery file upload.
    $('#recording_upload').fileupload({
        dataType: 'json',
        // formData: {'outline_id': activeOutlineId},
        done: function (e, data) {
            if (data.result.status != "success") {
                toastr.error(data.result.message);
                return;
            }
            toastr.success('recording uploaded.', 'Success!!');
        }
    });

    // Records the User Playback.
    $('#record').click(function (e) {
        $('#record').toggle();
        $('#stop_record').toggle();
        navigator.mediaDevices.getUserMedia({
            audio: true
        })
            .then(stream => {
                mediaRecorder = new MediaRecorder(stream, {
                    mimeType: 'audio/webm\;codecs=opus'
                });
                mediaRecorder.start();

                const audioChunks = [];
                var data;
                channels = 1; //1 for mono or 2 for stereo
                sampleRate = 44100; //44.1khz (normal mp3 samplerate)
                kbps = 128; //encode 128kbps mp3
                mp3encoder = new lamejs.Mp3Encoder(channels, sampleRate, kbps);

                mediaRecorder.addEventListener("dataavailable", event => {
                    audioChunks.push(event.data);
                });

                mediaRecorder.addEventListener("stop", () => {
                    var audioBlob = new Blob(audioChunks, {
                        type: 'audio/webm\;codecs=opus'
                    });
                    const audioUrl = URL.createObjectURL(audioBlob);
                    const audio = new Audio(audioUrl);

                    var hf = document.createElement('a');
                    hf.href = audioUrl;
                    var name = 'recording' + $.now() + '.mp3';
                    hf.download = name;
                    hf.innerHTML = hf.download;
                    var click = document.createEvent("Event");
                    click.initEvent("click", true, true);

                    var data = new FormData();
                    data.append('file', audioBlob);
                    data.append('name', name);


                    // $.ajax({
                    //     url :  "",
                    //     type: 'POST',
                    //     data: data,
                    //     contentType: false,
                    //     processData: false,
                    //     success: function(data) {
                    //         alert("boa!");
                    //     },
                    //     error: function() {
                    //         alert("not so boa!");
                    //     }
                    // });

                    $.ajax({
                        url: jsglobals.base_url + 'tsl/upload_audio',
                        // dataType: "json",
                        type: 'post',
                        contentType: false,
                        processData: false,
                        data: data
                    }).done(function (data) {

                        if (data.status === 'failure') {
                            toastr.error(data.message);
                            // reject(data);
                            return;
                        }
                        else if (data.status === 'redirect') {
                            window.location.href = data.redirect;
                            // reject(data);
                            return;
                        }
                    }).fail(function (jqXHR, textStatus) {
                        // $.unblockUI();
                        // reject('Ajax Error');
                        toastr.error("Request failed: " + textStatus);
                    });


                    // Upload recording to the server.
                    // const upload_audio = makeAjaxCall('tsl/upload_audio', {
                    //     data
                    // });
                    //
                    // upload_audio.then(function (data) {
                    //     console.log(data);
                    // });

                    const play_record_selector = $('#play_record'),
                        download_record_selector = $('#download_record');

                    play_record_selector.toggle();
                    download_record_selector.toggle();

                    // Play Audio File.
                    play_record_selector.click(function () {
                        audio.play();
                    });

                    // Download Audio file.
                    download_record_selector.click(function () {
                        hf.dispatchEvent(click);
                        hf.click();
                    });
                });
            });
    });

    $('#stop_record').click(function () {
        mediaRecorder.stop();
        $('#stop_record').toggle();
        $('#record').toggle();
    });

    let version_name = "Call Script Version";
    outline_name = "Call Script Outline";
    content_name = "Call Script Content";
    example_name = 'Example Script Version';


    if (req_type === "wks") {
        $('#score_stretch').hide();
        example_name = 'Example Skill Version';
        version_name = "Worksheet Version";
        content_name = "Worksheet Content";
        outline_name = "Worksheet Outline";
    } else if (req_type === "event") {
        $('#score_stretch').hide();
        example_name = 'Example Meeting Version';
        version_name = "Meetings Version";
        content_name = "Meetings Name";
        outline_name = "Meetings Outline";
    }

    // Scripts list.
    $('#script-listing').html('');
    $('#script-listing').html(
        ' <h4 align="center" class="main-con-header">' + version_name +
        '</h4><ul class="nav" id="tsl-script-nav">\n' +
        ' </ul>');

    let heading = 'Call Script Builder';
    // Check if the request type is for worksheet.
    if (req_type == 'wks') {
        requester = 'Worksheet';
        heading = 'STRATEGIC WORKSHEET BUILDER';
        $('#label-script-type').text('Skills Type');
        $('#script-version').text('Skills Version');

        $('<div class="col-xs-6 col-sm-6 col-lg-3 col-md-3 client_name script-box">' +
            '<label class="col-xs-12" id="label-client-name" style="text-align: center">Client Name</label>' +
            '<input class="form-control col-xs-8 wks-info" id="client-name" data-name=""></div>')
            .insertAfter($('.type'));

        $('<div class="col-xs-6 col-sm-6 col-lg-3 col-md-3 client_project script-box">' +
            '<label class="col-xs-12" id="label-client-project" style="text-align: center">Client Project</label>' +
            '<input class="form-control col-xs-8 wks-info" id="client-project"></div>')
            .insertBefore($('.version'));

        $('.version').removeClass('col-md-offset-6')

        $('.wks-info').change(function () {
            $.ajax({
                url: jsglobals.base_url + 'tsl/update_wks_info',
                dataType: 'json',
                type: 'post',
                data: {
                    client_name: $('#client-name').val(),
                    client_project: $('#client-project').val(),
                    wks_id: activeId,
                    client_id: client_id
                },
                error: ajaxError
            }).done(function (data) {

                if (data.status != 'success') {
                    toastr.error(data.message);
                    return;
                } else if (data.status == 'redirect') {
                    window.location.href = data.redirect;
                    return;
                }
                toastr.success('Outline Saved!');
                refreshPage();
            }).fail(function (jqXHR, textStatus) {
                toastr.error("Request failed: " + textStatus);
            }).always(function () {
            });
        });

    } else if (req_type == 'event') {
        requester = 'Event';
        heading = 'Meeting Builder';
        $('#label-script-type').text('Meeting Type');
        $('#script-version').text('Meeting Version');

        $('<div class="col-xs-6 col-sm-6 col-lg-3 col-md-3 client_name script-box">' +
            '<label class="col-xs-12" id="label-client-name" style="text-align: center">Client Name</label>' +
            '<input class="form-control col-xs-8 wks-info" id="client-name" data-name=""></div>')
            .insertAfter($('.type'));

        $('<div class="col-xs-6 col-sm-6 col-lg-3 col-md-3 client_project script-box">' +
            '<label class="col-xs-12" id="label-client-project" style="text-align: center">Client Project</label>' +
            '<input class="form-control col-xs-8 wks-info" id="client-project"></div>')
            .insertBefore($('.version'));

        $('.version').removeClass('col-md-offset-6')

        $('.wks-info').change(function () {
            $.ajax({
                url: jsglobals.base_url + 'tsl/update_wks_info',
                dataType: 'json',
                type: 'post',
                data: {
                    client_name: $('#client-name').val(),
                    client_project: $('#client-project').val(),
                    wks_id: activeId,
                    client_id: client_id
                },
                error: ajaxError
            }).done(function (data) {

                if (data.status != 'success') {
                    toastr.error(data.message);
                    return;
                } else if (data.status == 'redirect') {
                    window.location.href = data.redirect;
                    return;
                }
                toastr.success('Outline Saved!');
                refreshPage();
            }).fail(function (jqXHR, textStatus) {
                toastr.error("Request failed: " + textStatus);
            }).always(function () {
            });
        });

    }

    $('#tsl-script-heading').text(heading);

    // refreshPage();

    // Hide/Show script score.
    $('#score_stretch').click(function (e) {
        $('.script_score').toggle();
        $('.script_container').toggleClass('col-md-5 col-lg-5', 'col-md-7 col-lg-7');
        $('.script_container').toggleClass('col-md-7 col-lg-7', 'col-md-5 col-lg-5');
        $('#cpd_full_view').toggle();
    });

    // Hide/Show DMD full screen view.
    $('#cpd_full_view').click(function (e) {
            $('.script_container').toggleClass(' col-xs-60', 'col-md-7 col-lg-7');
            $('.full-view-hide').toggle();

            const script_container_selector = $('#script-container');
            script_container_selector.toggleClass('full-screen');

            // Check if full screen class is set.
            if (script_container_selector.hasClass('full-screen')) {
                setupCollab();
            }

            // Add Collaboration and send Collaboration link button, if not exist.
            if (0 === $('.add_collab').length) {
                $('.tts')
                    .before('<span class="input-group-addon btn btn-secondary add_collab">' +
                        '<i class="fa fa-plus"></i></span>');

                //Remove click functionality from add and send collab button.
                $(document).off('click', '.add_collab, .send_collab');

                $(document).off('click', '.add_collab');
                // Add Collaboration click handler.
                $(document).on('click', '.add_collab', function () {
                    $('#add-collab-dialog').modal('show');

                    const ques_id = $(this).parent().find('textarea').attr('data-id'),
                        that = $(this),
                        parent_margin = $(this).parent().css('margin'),
                        order = $(this).parent().siblings('.collab_row').length,
                        get_collab_details = makeAjaxCall('collab/get_collab_details', {});

                    get_collab_details.then(function (data) {
                        $('#collab-action').empty();
                        // $('#collab-action').append('<optgroup label="Comment(s)"></optgroup>')

                        let optgroup = $("<optgroup label='Comment(s)'>");
                        // Iterate over each of the formats available
                        $.each(data.collab_details.format, function (index, format) {

                            if (format.collab_format_id === '2' || format.collab_format_id === '3') {
                                $('#collab-action').append('<option value="' + format.collab_format_id + '">'
                                    + format.collab_format_name + '</option>')
                            } else {
                                optgroup
                                    .append('<option value="' + format.collab_format_id + '">'
                                        + format.collab_format_name + '</option>')
                            }
                        });

                        $('#collab-action').append(optgroup);

                        // Append Sender Name.
                        $('#sender_name').val(data.collab_details.user);

                        const recipient_name_selector = $('#recipient_name');

                        recipient_name_selector.empty();
                        recipient_name_selector.append('<option value="-1" selected>-- Select Recipient --</option>');
                        recipient_name_selector.append('<option value="0">-- Add New Recipient --</option>');

                        //Iterate over the collaborator names.
                        $.each(data.collab_details.recipient, function (index, recipient) {
                            recipient_name_selector.append
                            ('<option value="' + recipient.collab_recipient_id + '">' + recipient.collab_recipient_name + '</option>')
                        });

                        // Recipient Name drop down change event.
                        recipient_name_selector.change(function () {
                            if ($(this).val() === '0') {
                                $('#add-collab-dialog').modal('hide')
                                $('#add-recipient-dialog').modal('show')
                            }
                        })
                    });


                    $('#save_collab_details').off('click');
                    $('#save_collab_details').click(function () {
                        const sender_name = $('#sender_name').val(),
                            action = $('#collab-action option:selected').text(),
                            today = new Date(),
                            date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate(),
                            time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds(),

                            // Create a collab instance in database.
                            save_collab_data = makeAjaxCall('collab/save_collab_data', {
                                type: 'script',
                                instance_id: activeId,
                                ques_id: ques_id,
                                order: order,
                                sender: sender_name,
                                recipient: $('#recipient_name').val(),
                                action: $('#collab-action').val(),
                                date: date,
                                time: time,
                                collaboration_id: 0,
                                category: 'sc'
                            });

                        // Add Row after collab data is saved.
                        save_collab_data.then(function (data) {
                            toastr.success('Collaboration Instance Added!!');
                            $('#add-collab-dialog').modal('hide');

                            // Format date and time
                            const formatted_date = moment(date).format('DD-MM-YY'),
                                formatted_time = moment(time, "HH:mm:ss").format('hh:mm A');

                            const collab_action_message = [
                                'Request for Comments.',
                                'Request for Examples.',
                                'Request for Roleplay.',
                                'Do you have any Questions?',
                                'Do you have any Recommendations?',
                                'Do you have any Requests?'
                            ];

                            // Append text box after DB record is added.
                            $(that).parent().after(
                                '<div style="margin: calc(1% + ' + parent_margin + ')" class="collab_row">' +
                                '<label>' + sender_name + '. ' +
                                formatted_date + ' ' + formatted_time + ' : ' + action + '</label>' +
                                '<div class="input-group">' +
                                '<span class="input-group-addon btn btn-secondary tts">' +
                                '<i class="fa fa-bullhorn"></i></span>' +
                                '<textarea onkeyup="auto_grow(this)" class="form-control custom-control" ' +
                                'rows="1" cols="70" style="height: 52px;" ' +
                                'data-id="' + ques_id + '" data-collab="' + data.collab_data_saved + '" ' +
                                'placeholder="Add an Email Message."></textarea>' +
                                '<span class="input-group-addon btn btn-secondary stt">' +
                                '<i class="fa fa-microphone"></i></span>' +
                                '<span class="input-group-addon btn btn-secondary send_collab" data-toggle="modal"' +
                                'data-action="' + action + '" data-target="#send-collab-dialog" ' +
                                'data-message="' + collab_action_message[$('#collab-action').val() - 1] + '">' +
                                '<i class="fa fa-envelope"></i></span></div></div>')
                        });
                    });
                });

                // Send Collaboration click handler.
                // $(document).on('click', '.send_collab', function () {
                //     $('#send-collab-dialog').modal('show');
                // });

                //Save Collaboration data to database.
                $(document).off('change', '.collab_row textarea');
                $(document).on('change', '.collab_row textarea', function () {
                    const that = $(this),
                        save_collab_data = makeAjaxCall('collab/update_collab_data', {
                            id: $(this).attr('data-collab'),
                            content: $(this).val(),
                        });

                    save_collab_data.then(function (data) {
                        // console.log($(that).siblings('.send_collab'));
                        // $(that).siblings('.send_collab').attr('data-message', $())
                    })
                })
            }
            else {
                $('.add_collab, .send_collab, .collab_row, .collaborator_row').remove();
            }
        }
    );

    // Saves the outline and generates questions from outlines.
    $('#save-script-outline').click(function (e) {
        $('#script_type').val($('#script-type').val());
        $('#requester').val('script');
        $('#client_type').val($('#client-type').val());

        // Check if TA is present.
        if (null === $('#target-audience').val()) {
            toastr.error('No Location Selected!!');
            return;
        }
        else
            $('#ta').val((undefined !== $('#target-audience').val()) ? $('#target-audience').val() : 0);

        // Check if criteria is present.
        if (null === $('#list-criteria').val()) {
            toastr.error('No List Criteria Selected!!');
            return;
        }
        else
            $('#criteria').val((undefined !== $('#list-criteria').val()) ? $('#list-criteria').val() : 0);

        // Check if focus is present.
        // if (null === $('#promo-focus').val()) {
        //     toastr.error('No Promotional Focus Selected!!');
        //     return;
        // } else
        //     $('#focus').val($('#promo-focus').val());

        var tsl_update_data = $('#outline-form').serialize();
        e.preventDefault();

        // Save a new Script Outline.
        $.ajax({
            url: jsglobals.base_url + 'tsl/save_outline',
            dataType: 'json',
            type: 'post',
            data: tsl_update_data,
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error(data.message);
                return;
            } else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            toastr.success('Outline Saved!');
            refreshPage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });

    // Compose a new script.
    $('#compose-script').click(function (e) {
        e.preventDefault();

        // if (0 !== activeOutlineId) {

        item_added = 1;

        // Adds a new script version.
        $.ajax({
            url: jsglobals.base_url + 'tsl/add_script',
            dataType: 'json',
            data: {
                type_id: $('#script-type option:selected').attr('data-id'),
                version_id: activeOutlineId,
                component: req_type
            },
            type: 'post',
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error(data.message);
                return;
            } else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            // activeOutlineId = 0;
            refreshPage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
        // } else {
        //     toastr.error('No Outline version Selected.', 'Error!!');
        //     return;
        // }
    });

    // Saves the content of a script and lists it as new version.
    $('#save-script').click(function (e) {

        $('#script_names_id').val(script_name_id);
        var script_data = $('#script-form').serializeArray();
        e.preventDefault();

        // Check if script form is empty. If so show an error message.
        if (0 === script_data.length) {
            toastr.error('No content available to save.', 'Error');
            return;
        } else if (0 === parseInt(activeId)) {
            toastr.error('To Create one use Compose button.', 'No Script Version Selected!!');
            return;
        }

        script_data.push({name: 'script_id', value: $('#script-type option:selected').attr('data-id')});
        script_data.push({
            name: 'outline_version',
            value: $('#script-type option:selected').attr('data-id')
        });
        script_data.push({name: 'active_id', value: activeId});
        script_data.push({name: 'count', value: max});
        script_data.push({name: 'type', value: req_type});
        script_data.push({name: 'client_id', value: client_id});

        // Updates Script Content.
        $.ajax({
            url: jsglobals.base_url + 'tsl/update_script',
            dataType: 'json',
            type: 'post',
            data: script_data,
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error('To create one use compose button.', 'No instance of Script Present!!');
                return;
            } else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            script_added = 1;
            new_script_id = data.update;
            toastr.success('Script Content Saved');
            // refreshPage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });

    // Saves the performance and proficiency score.
    $(document).on('click', '#perf-prof-submit', function (e) {
        e.preventDefault();

        if (0 === activeId) {
            toastr.error('To Create one use Compose button.', 'No Script Version Selected!!');
            return;
        }

        var cols = '';
        $('#client_id').val(client_id);
        $('#script_id').val(activeId);

        // Checks if score is equal to null.
        $.each($('.ynm-script-select'), function (index, script) {
            if (script.value !== -1) {
                cols += script.id + ' ';
            }
        });

        // Name of the columns to be updated/inserted in the database.
        $('#cols_name').val(cols);

        var data = $('#perf-prof-form').serialize();

        // Updates the performance proficiency scores.
        $.ajax({
            url: jsglobals.base_url + 'tsl/update_script_perf_prof',
            dataType: 'json',
            type: 'post',
            data: data,
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error('Invalid Score Value', 'Error');
                return;
            } else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            toastr.success('Score Saved');
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });

    // Check if drop down value is changed.
    $(document).on('change', '.script-menu', function () {

        if ((!$(this).hasClass('script-pre-menu')) || ($(this).hasClass('script-pre-menu') && ($(this).val() !== '0'))) {
            activeOutlineId = 0;
            activeId = 0;

            // Clear out Navbar and form.
            $('#script-form').empty();
            $('#tsl-script-nav').empty();

            refreshPage();
        }
    });

    // Displays screen steps
    $('#view-script-outline').click(function () {
        $('#view-script-outline-dialog').modal('show');
    });

    // Displays score board.
    $(document).on('click', '#perf-prof-score', function (e) {
        $('#perf-prof-score').attr('data-id', activeId);
        $('#view-perf-score-dialog').modal('show');
        e.preventDefault();
        $('#' + activeId).addClass('active');
    });

    // Clears the score.
    $('#perf-prof-clear').click(function (e) {
        $('.ynm-script-select').val(-1);
        e.preventDefault();
    });
});

function setupCollab() {
    const get_collab_data = makeAjaxCall('collab/get_collab_data', {
        type: 'script',
        id: activeId,
        category: 'sc',
        filter_recipient: $('#filter-recipient-name').val(),
        filter_action: $('#filter-collab-action').val()
    });

    // Append Text for collaboration.
    get_collab_data.then(function (data) {
        $.each(data.collab_data, function (index, collab_data) {
            const formatted_date = moment(collab_data.sc_date).format('DD-MM-YY'),
                formatted_time = moment(collab_data.sc_time, "HH:mm:ss").format('hh:mm A');

            $('#ans' + collab_data.sc_ques_id).parent()
                .after(
                    '<div style="margin: calc(1% + ' + $('#ans' + collab_data.sc_ques_id).parent().css('margin')
                    + ')" class="collab_row"><label>' + collab_data.sc_sender + '. ' +
                    formatted_date + ' ' + formatted_time + ' : ' + collab_data.sc_action + '</label>' +
                    '<div class="input-group" id="collaboration' + collab_data.sc_id + '">' +
                    '<span class="input-group-addon btn btn-secondary tts">' +
                    '<i class="fa fa-bullhorn"></i></span>' +
                    '<textarea onkeyup="auto_grow(this)" class="form-control custom-control collab-user-row" rows="1" ' +
                    'cols="70" data-collab="' + collab_data.sc_id + '" ' +
                    'data-id="' + collab_data.sc_ques_id + '" placeholder="Add an Email Message." ' +
                    'id="collab' + collab_data.sc_id + '">' +
                    collab_data.sc_content + '</textarea>' +
                    '<span class="input-group-addon btn btn-secondary stt">' +
                    '<i class="fa fa-microphone"></i></span>' +
                    '<span class="input-group-addon btn btn-secondary send_collab" ' +
                    'data-message="' + collab_data.message + '" data-toggle="modal"' +
                    'data-target="#send-collab-dialog">' +
                    '<i class="fa fa-envelope"></i></span></div></div>')
        });

        // Fetch Collaborator data.
        const get_collaborator_data = makeAjaxCall('collab/get_collab_data', {
            type: 'script',
            id: activeId,
            category: 'scr',
            filter_recipient: $('#filter-recipient-name').val(),
            filter_action: $('#filter-collab-action').val()
        });

        // Append Text for collaborator.
        get_collaborator_data.then(function (data) {
            $.each(data.collab_data, function (index, collab_data) {
                const formatted_date = moment(collab_data.sc_date).format('DD-MM-YY'),
                    formatted_time = moment(collab_data.sc_time, "HH:mm:ss").format('hh:mm A'),
                    collab_selector = $('#collaboration' + collab_data.sc_collaboration_id).parent(),
                    parent_margin = collab_selector.css('margin');

                // Append Collaborator Responses.
                collab_selector
                    .after(
                        '<div style="margin: calc(1% + ' + parent_margin + ')" class="collaborator_row"><label>'
                        + collab_data.sc_sender + '. ' +
                        formatted_date + ' ' + formatted_time + ' : ' + collab_data.sc_action + '</label>' +
                        '<div class="input-group">' +
                        '<span class="input-group-addon btn btn-secondary tts">' +
                        '<i class="fa fa-bullhorn"></i></span>' +
                        '<textarea onkeyup="auto_grow(this)" class="form-control custom-control collborator_data" rows="1" ' +
                        'cols="70" data-collab="' + collab_data.sc_id + '" ' +
                        'data-id="' + collab_data.sc_ques_id + '" id="collab' + collab_data.sc_id + '">' +
                        collab_data.sc_content + '</textarea>' +
                        '<span class="input-group-addon btn btn-secondary stt">' +
                        '<i class="fa fa-microphone"></i></span></div></div>')
            });
            // TTS and STT functionality.
            speechFunctionality();

            // Check if roleplay is used for filtering collaboration data.
            if ($('#filter-collab-action').val() === '3') {
                // Append play roleplay button.
                $('#cpd_full_view')
                    .after('<button id="play_roleplay" class="btn btn-secondary full-view-hide"' +
                        ' style="margin-left:2px">Play Roleplay <i class="fa fa-play"></i></button>');

                // Append remove roleplay button.
                $('.collaborator_row')
                    .find('textarea')
                    .after('<span class="input-group-addon btn btn-secondary remove-roleplay">' +
                        '<i class="fa fa-minus"></i></span>')
            }
            else {
                $('#play_roleplay').remove();
            }
        });
    });
}

// Setup the yes email window.
function setupScript(obj) {
    script_name_id = $(obj).attr('data-name-id');
    item_added = 0;
    activeId = $(obj).attr('data-id');

    // Checks if active ID is not zero.
    if (0 !== parseInt(activeId) && req_type === 'script') {
        //Gets the Performance and Proficiency score.
        $.ajax({
            url: jsglobals.base_url + 'tsl/get_perf_prof',
            dataType: 'json',
            data: {
                client_id: client_id,
                script_id: activeId,
            },
            type: 'post',
            error: ajaxError
        }).done(function (data) {

            if ('success' !== data.status) {
                toastr.error(data.message);
                return;
            } else if ('redirect' === data.status) {
                window.location.href = data.redirect;
                return;
            }

            $('#client-name').val(data.cols['name-project'][0].tsc_name);
            $('#client-project').val(data.cols['name-project'][0].tsc_project);
            updatePerfProf(data.cols['perf-prof']);
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    }

    // Highlight active email name.
    $('.active').removeClass('active');
    $('#' + activeId).addClass('active');

    // Set attribute for delete button.
    const script_delete_selector = $('#delete-script');
    script_delete_selector.removeAttr('data-toggle');

    if (0 !== parseInt(activeId)) {
        script_delete_selector.attr('data-id', activeId);
        script_delete_selector.attr('data-toggle', 'modal');
        script_delete_selector.attr('data-target', '#delete-script-dialog');
        script_delete_selector.attr('data-backdrop', 'static');
        script_delete_selector.attr('data-name', $(obj).attr('data-name'));
    }

    $script_id = $(obj).attr('data-id');
    $('#script_names_id').val($(obj).attr('data-name-id'));

    setupContent();

    max = 0;
    $.each($('#script-form').find('textarea'), function (index, t) {
        count = (index + 1);
        $(t).attr('id', 'ans' + count);
        $(t).attr('name', 'ans' + count);
        $(t).attr('data-id', count);
        max = count;
    });

    // Fetch Scripts Answer.
    $.ajax({
        url: jsglobals.base_url + 'tsl/get_script_ans',
        dataType: 'json',
        type: 'post',
        data: {
            script_id: $script_id,
            type: req_type,
            client_id: client_id
        },
        error: ajaxError
    }).done(function (data) {

        if (data.status !== 'success') {
            toastr.error(data.message);
            return;
        } else if (data.status === 'redirect') {
            window.location.href = data.redirect;
            return;
        }

        if (data.answer.length === 0) {
            get_default_answers();
        } else {
            $.each(data.answer, function (index, answer) {
                $('#ans' + answer.question).val(answer.answer.replace('\\', ''));
            });
        }

    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {

        // Auto grow script form text area size.
        $('#script-form').find('textarea').each(function () {
            $(this).css('height', $(this)[0].scrollHeight + 'px');
        });

        speechFunctionality();
    });

    setEditable('bottom');

}

// Set up the content for script middle panel.
function setupContent() {
    // Append text area, headings, text to voice/voice to text buttons and indent sub-headings.
    let z = $('#y').val().replace(/\[\[/g, '<h4 style="margin-top: 15px">').replace(/\]\]/g, '</h4>')
            .replace(/\(\(/g, '<div class="user_row"><label style="margin-left: 30px; ">')
            .replace(/\)\)/g, '</label>\n' +
                '<div class="input-group" style="margin-left: 30px;">' +
                '<span class="input-group-addon btn btn-secondary tts">' +
                '<i class="fa fa-bullhorn"></i></span><textarea onkeyup="auto_grow(this)"' +
                ' class="form-control custom-control answer" placeholder="Click Microphone icon and speak or start typing."' +
                ' rows="1" cols="10"></textarea><span class="input-group-addon btn btn-secondary stt">' +
                '<i class="fa fa-microphone"></i>' +
                ' </span></div><span id=\'span_ans\' class=\'hidden\'></span></div>')
            .replace(/{{/g, '<div><label style="margin-top: 15px">').replace(/}}/g, '</label>\n' +
            ' <div class="input-group user_row"><span class="input-group-addon btn btn-secondary tts">' +
            '<i class="fa fa-bullhorn"></i></span><textarea onkeyup="auto_grow(this)" ' +
            'class="form-control custom-control" placeholder="Click Microphone icon and speak or start typing."' +
            ' rows="1" cols="70"></textarea><span class="input-group-addon btn btn-secondary stt">' +
            '<i class="fa fa-microphone"></i>' +
            ' </span></div><span id=\'span_ans\' class=\'hidden\'></span></div>').replace(/<br>/g, ''),

        x = z.trim().match(/(<label(.*?)<\/label>)|(<h4(.*?)<\/h4>)|(<([^>]+)>)/g),
        t = '';

    $.each(x, function (index, text) {
        t = t + text;
    });

   const script_container_selector = $('#script-container');
    script_container_selector.html('');
    script_container_selector.html('<h4 id="script_content" class="main-con-header">'
        + content_name +
        '</h4><form id="script-form" class="form-horizontal" style="padding: 10px">' +
        '<input type="hidden" name="script_name_id" id="script_names_id">' + t + '</form>');
}

// Fetches the Scripts Outline.
function refreshPage() {

    $.ajax({
        url: jsglobals.base_url + 'tsl/get_script_type',
        dataType: 'json',
        type: 'post',
        data: {
            origin: origin,
            req_type: req_type
        },
        error: ajaxError
    }).done(function (data) {

        if (data.status != 'success') {
            toastr.error(data.message);
            return;
        } else if (data.status == 'redirect') {
            window.location.href = data.redirect;
            return;
        }

        const script_type_selector = $('#script-type');
        script_type_selector.empty();
        $.each(data.type, function (index, type) {
            script_type_selector.append('<option data-content="' + type.tsl_script_content + '" data-id="' +
                type.tsl_script_default_id + '" value="' + index + '">' + type.tsl_script_type + '</option>')
        });

        // Set the active outline for scripts.
        script_type_selector.val((undefined !== localStorage.active_script_type) ? localStorage.active_script_type : 0);

        // Check if script type is set to null.
        (script_type_selector.val() === null) ?
            script_type_selector.val(0) : '';

        $('#script-outline').html('').html('<h4 id="script_outline" class="main-con-header">'
            + outline_name + '</h4><form id="outline-form">' +
            '<input type="hidden" id="script_type" name="type">' +
            '<input type="hidden" id="requester" name="requester">' +
            '<input type="hidden" id="ta" name="ta">' +
            '<input type="hidden" id="criteria" name="criteria">' +
            '<input type="hidden" id="focus" name="focus">' +
            '<input type="hidden" id="client_type" name="client_type">' +
            '<input id="y" value="' + $('#script-type option:selected').attr('data-content').replace(/\\/g, '')
            + '" type="hidden" name="outline_content">\n' +
            '<trix-editor id="trix-editor" input="y" name="tsl_bus_editor"></trix-editor>' +
            '</form>');
        trixButtons();

        updateScriptOutline();
    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
    });
}


//Fetches and displays script outlines versions.
function updateScriptOutline() {

    const script_outline_version_selector = $('#script-outline-version');
    script_outline_version_selector.empty();
    script_outline_version_selector.append($('<option id="0" data-id="0" value="0"> Example Script</option>'));

    // Set Default Outline in the outline listing table.
    $('#script-outline-table tbody').empty();
    $('#script-outline-table tbody')
        .append($('<tr>')
            .append($('<td class="text-center">')
                .append(1))
            .append($('<td class="text-center">')
                .append($('<a class="script-outline-selector" ' +
                    'id="' + $('#script-type option:selected').attr('data-id') + '" ' +
                    'data-val="' + $('#script-type').val() + '">Example Script</a>')))
        );

    obj = {
        "data-id": $('#script-type').val()
    };

    updateScriptList(obj);

    // $.ajax({
    //     url: jsglobals.base_url + 'tsl/get_script_outline',
    //     dataType: 'json',
    //     type: 'post',
    //     data: {
    //         script_type: $('#script-type').val(),
    //         ta: $('#target-audience').val(),
    //         criteria: $('#list-criteria').val(),
    //         focus: $('#promo-focus').val(),
    //         client_type: $('#client-type').val()
    //     },
    //     error: ajaxError
    // }).done(function (data) {
    //
    //     if (data.status !== 'success') {
    //         toastr.error(data.message);
    //         return;
    //     } else if (data.status === 'redirect') {
    //         window.location.href = data.redirect;
    //         return;
    //     }
    //
    //     // $('#script-outline-table tbody tr').remove();
    //
    //     // Check if the script outline is present or not.
    //     if (data.outline.length !== 0) {
    //         $.each(data.outline, function (index, outline) {
    //             $('#script-outline-version')
    //                 .append($('<option id="' + outline.tsl_script_outline_id + '" ' +
    //                     'data-id="' + outline.tsl_script_outline_id + '" value="' + (index + 1) + '"> Outline V'
    //                     + outline.tsl_script_outline_version + '</option>'))
    //
    //             $('#script-outline-table tbody')
    //                 .append($('<tr>')
    //                     .append($('<td class="text-center">')
    //                         .append(index + 1))
    //                     .append($('<td class="text-center">')
    //                         .append($('<a class="script-outline-selector" id="' + outline.tsl_script_outline_id + '" ' +
    //                             'data-val="' + index + '">' +
    //                             'Outline V' + outline.tsl_script_outline_version + '</a>')))
    //                     .append($('<td class="text-center">')
    //                         .append($('<button class="btn btn-default btn-sm"' +
    //                             ' data-toggle="modal"' +
    //                             ' data-target="#delete-outline-dialog"' +
    //                             ' data-backdrop="static"' +
    //                             ' data-id="' + outline.tsl_script_outline_id +
    //                             '" data-name="' + outline.tsl_script_outline_version +
    //                             '"> <span class="glyphicon glyphicon-trash"></span>')))
    //                 );
    //         });
    //
    //         $('#script-outline-table').find('tbody tr:first').addClass('active');
    //     }
    //
    //
    //     // Script Outline Click handler.
    //     $('.script-outline-selector').unbind('click');
    //     $('.script-outline-selector').click(function () {
    //
    //         const that = $(this);
    //         $('.active_outline').removeClass('active_outline');
    //         $(this).addClass('active_outline');
    //
    //         // Set the active Outline Value.
    //         localStorage.setItem('active_outline_id', that.attr('id'));
    //
    //         // Set the content for Outline Container.
    //         const script_outline_selector = $('#script-outline');
    //         script_outline_selector.html('');
    //         script_outline_selector.html('<form id="outline-form">' +
    //             '<input type="hidden" id="script_type" name="type">' +
    //             '<input type="hidden" id="requester" name="requester">' +
    //             '<input type="hidden" id="ta" name="ta">' +
    //             '<input type="hidden" id="criteria" name="criteria">' +
    //             '<input type="hidden" id="focus" name="focus">' +
    //             '<input type="hidden" id="client_type" name="client_type">' +
    //             '<input id="y" value="' + data.outline[(that.attr('data-val'))]
    //                 .tsl_script_outline_content + '" type="hidden" name="outline_content">\n' +
    //             '<trix-editor input="y" name="tsl_bus_editor"></trix-editor>' +
    //             '</form>');
    //
    //         trixButtons();
    //
    //         // Empty the middle and right panel.
    //         $('#script-container').html('');
    //         $('.ynm-script-select').val(-1);
    //         activeId = 0;
    //         obj = {
    //             "data-id": $('.active_outline').attr('data-val')
    //         };
    //         updateScriptList(obj);
    //     });
    //
    //     // Fetches active outline ID from local Storage.
    //     script_outline_version_selector.val((undefined === localStorage.active_outline_id) ?
    //         0 : localStorage.active_outline_id);
    //
    //     if (activeOutlineId === 0) {
    //         // Check if Outline for selected drop down combination is present.
    //         if (data.outline.length !== 0) {
    //             obj = {
    //                 "data-id": data.outline[0].tsl_script_outline_id
    //             };
    //             updateScriptList(obj);
    //         } else {
    //             // Remove open Modal from delete button.
    //             $('#delete-script').removeAttr('data-toggle');
    //             setupContent();
    //         }
    //     } else {
    //         obj = {
    //             "data-id": activeOutlineId
    //         };
    //         updateScriptList(obj);
    //     }
    //
    // }).fail(function (jqXHR, textStatus) {
    //     toastr.error("Request failed: " + textStatus);
    // }).always(function () {
    // });
}


// Updates the list of script outline and lists.
function updateScriptList(obj) {

    // Remove open Modal from delete button.
    $('#delete-script').removeAttr('data-toggle');

    let z = $('#y').val().replace(/{{/g, '<label>{{').replace(/}}/g, '}}</label>\n').replace(/<br>/g, '')
            .replace(/\(\(/g, '<label style="margin-left: 20px">((').replace(/\)\)/g, '))</label>\n')
            .replace(/\]\]/g, ']]</h3>\n').replace(/\[\[/g, '<h3>[['),
        x = z.trim().match(/(<label(.*?)<\/label>)|(<h3(.*?)<\/h3>)|(<([^>]+)>)|&nbsp;/g),
        t = '';


    $.each(x, function (index, text) {
        t = t + text;
    });

    t = t.replace(/<\/label>/g, '</label></br>');

    $('#script-outline').append('<div id="outline-content" style="display: none">' + t + '</div>');

    activeOutlineId =
        (undefined !== $('.active_outline').attr('id')) ? $('.active_outline').attr('id') : $(obj).attr('data-id');

    // Fetch Script List.
    $.ajax({
        url: jsglobals.base_url + 'tsl/get_script_list',
        dataType: 'json',
        data: {
            type_id: $('#script-type option:selected').attr('data-id'),
            version_id: activeOutlineId
        },
        type: 'post',
        error: ajaxError
    }).done(function (data) {

        if (data.status !== 'success') {
            toastr.error(data.message);
            return;
        } else if (data.status === 'redirect') {
            window.location.href = data.redirect;
            return;
        }

        // Populate Scripts
        $('#tsl-script-nav').empty();

        // if (data.scripts[0] === undefined) {
        //     toastr.info('Please create one using Compose Button', 'You have no instance of ' + requester + ' version')
        // } else {

        $('#tsl-script-nav')
            .append($('<li class="text-center" id="0">')
                .append('<a href="#" onclick="setupScript(this); return false;" data-id="0">' + example_name + '</a>'))

        // List scripts name and sets data attribute to make names editable.
        $.each(data.scripts, function (index, script) {
            $('#tsl-script-nav')
                .append($('<li class="text-center" id="' + script.ts_id + '">')
                    .append('<a href="#" class="edit-ref" onclick="setupScript(this); return false;" ' +
                        'data-name="' + script.tsn_name + ' v' + script.ts_version + '" ' +
                        ' data-index = "' + index + '" data-name-id = "' + script.tsn_id + '"' +
                        'data-id="' + script.ts_id + '">' +
                        '<span class="editable" data-type="text" data-pk = "' + script.tsn_id + '" data-url = "'
                        + jsglobals.base_url + 'tsl/update_script_name">' + script.tsn_name + '</span></a></li>'))
        });

        if (data.scripts.length !== 0) {

            var obj = {
                "data-id": data.scripts[0].ts_id,
                "data-name": data.scripts[0].tsn_name + ' v' + data.scripts[0].ts_version,
                "data-name-id": data.scripts[0].tsn_id
            };

            setupScript(obj);
        } else {
            setupContent();
            get_default_answers();
        }
        // }
    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
    });

    setEditable('bottom');
}


// Updates the latest Performance and Proficiency in the script.
function updatePerfProf(cols) {

    // Clear performance/proficiency score.
    $('.ynm-script-select').val(-1);

    $.each(cols[0], function (index, col) {
        if (col != null)
            $('#' + index.replace("tspp_", "")).val(col);
    });

    // Check if the component is a dummy.
    check_dummy_component(client_id);
}

// Fetches the default answers for scripts from the db.
function get_default_answers() {
    const get_script_answers = makeAjaxCall('tsl/get_script_default_answers',
        {'outline_id': $('#script-type option:selected').attr('data-id')});

    get_script_answers.then(function (data) {

        // Set the content for each textarea for script.
        $.each(data.default_answers, function (index, answer) {
            $('#script-form textarea').eq(index).val(answer.tsda_content);
        });

        // Auto grow script form text area size.
        $('#script-form textarea').each(function () {
            $(this).css('height', $(this)[0].scrollHeight + 'px');
        });

        speechFunctionality();

    });

}
