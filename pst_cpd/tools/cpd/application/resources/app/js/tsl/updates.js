let activeOutlineId, activeUpdateId, outline_added, update_added, questions_id = [], client_id, obj, ref,
    default_update;

$(document).ready(function () {

    // Get the Client ID from the URL
    const path = window.location.pathname,
        components = path.split('/'),
        origin = components[2];

    ref = window.location.search.split('?ref=')[1];

    // Check if ref is defined or not.
    if (undefined !== ref) {
        const ref_array = ref.split('?');
        ref = ref_array[0];
    }    client_id = components[components.length - 1];

    // Initialize voice rrecgnisation Functionality.
    voiceRecognition();

    const id = window.location.search.split('?id=')[1];
    loadPrev();

    activeOutlineId = 0;
    activeUpdateId = -1;

    // Display outline for the update.
    $('#outline-container').html('');
    $('#canvas-container').html('');

    // To check if a new update is added.
    outline_added = 0;
    activeOutlineId = 0;

    // List of Buyer's Update
    const container_listing_selector = $('#container-listing');
    container_listing_selector.html('');
    container_listing_selector.html(
        ' <h4 align="center" class="main-con-header">Update Version</h4>' +
        ' <ul class="nav" id="tsl-update-nav">\n' +
        ' </ul>');

    refreshUpdatePage();

    // Creates a new instance of update based on update outline.
    $('#compose-update').click(function (e) {
        e.preventDefault();
        update_added = 1;

        $.ajax({
            url: jsglobals.base_url + 'tsl/add_update',
            dataType: 'json',
            data: {
                outline_id: activeOutlineId
            },
            type: 'post',
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error('Please create one using save button on the left.', 'No instance of outline selected!!');
                return;
            } else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            refreshUpdatePage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });

    // Saves the outline and generates questions from outlines.
    $('#save-update-outline').click(function (e) {
        $('#outline_type').val($('#update-type').val());
        $('#frequency').val($('#update-frequency').val());
        $('#client_type').val($('#client-type').val());
        $('#update_phase').val($('#update-phase').val());
        $('#requester').val('update');
        outline_added = 0;
        activeOutlineId = 0;
        const tsl_update_data = $('#outline-form').serialize();
        e.preventDefault();

        $.ajax({
            url: jsglobals.base_url + 'tsl/save_outline',
            dataType: 'json',
            type: 'post',
            data: tsl_update_data,
            error: ajaxError
        }).done(function (data) {
            if (data.status !== 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            toastr.success('Outline Saved!');
            refreshUpdatePage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });

    // Saves client response.
    // $('#save-update').click(function (e) {
    //     const data = $('#response-form').serializeArray();
    //     data.push({name: 'question_id', value: questions_id});
    //     data.push({name: 'client_id', value: client_id});
    //     data.push({name: 'update_id', value: activeUpdateId});
    //     data.push({name: 'source', value: 'user'});
    //     e.preventDefault();
    //
    //     $.ajax({
    //         url: jsglobals.base_url + 'update/set_ans',
    //         dataType: 'json',
    //         type: 'post',
    //         data: data,
    //         error: ajaxError
    //     }).done(function (data) {
    //         if (activeUpdateId === 0) {
    //             toastr.error('To create one use compose button.', 'No Instance of update.');
    //             return;
    //         }
    //         else if (data.status !== 'success') {
    //             toastr.error(data.message);
    //             return;
    //         }
    //         else if (data.status === 'redirect') {
    //             window.location.href = data.redirect;
    //             return;
    //         }
    //         toastr.success('Response Saved!');
    //         refreshUpdatePage();
    //     }).fail(function (jqXHR, textStatus) {
    //         toastr.error("Request failed: " + textStatus);
    //     }).always(function () {
    //     });
    // });

    // Saves the content of a script Nand lists it as new version.
    $('#save-update').click(function (e) {
        $('#update_id').val(activeUpdateId);
        var update_data = $('#response-form').serializeArray();
        e.preventDefault();

        // Check if script form is empty. If so show an error message.
        if (0 === update_data.length) {
            toastr.error('No content available to save.', 'Error');
            return;
        } else if (0 === activeUpdateId) {
            toastr.error('To Create one use Compose button.', 'No Update Version Selected!!');
            return;
        }

        update_data.push({name: 'script_id', value: $('#script-type option:selected').attr('data-id')});
        update_data.push({
            name: 'outline_version',
            value: $('#script-outline-version option:selected').attr('data-id')
        });
        update_data.push({name: 'active_id', value: activeUpdateId});
        update_data.push({name: 'count', value: max});
        // update_data.push({name: 'type', value: req_type});

        $.ajax({
            url: jsglobals.base_url + 'tsl/update_update',
            dataType: 'json',
            type: 'post',
            data: update_data,
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error('To create one use compose button.', 'No instance of Update Present!!');
                return;
            } else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            script_added = 1;
            new_script_id = data.update;
            toastr.success('Update Content Saved');
            // refreshPage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });

    // Check if any drop down menu is changed, fetch outline version again.
    $('.update-menu').change(function () {
        activeOutlineId = 0;
        refreshUpdatePage();
    });

    // Checks if no instance of update is present.
    $('#send-update, #delete-update').click(function () {
        if ($(this).attr('data-toggle') === undefined) {
            toastr.error('To create one use compose button.', 'No Instance of update.');
        }
    });
});

// Fetches questions and puts them in a form.
function setupUpdate(obj) {

    // Set the button for trix buttons.
    trixButtons();

    activeOutlineId = $(obj).attr('data-id');
    $version = $(obj).attr('data-version');

    $('.active_outline').removeClass('active_outline');

    // Change the active Outline.
    $('tr [data-id=' + activeOutlineId + ']').closest('tr').addClass('active_outline');

    let question_input = '';

    // Fetches and displays the outline questions.
    // $.ajax({
    //     url: jsglobals.base_url + 'tsl/get_update_questions',
    //     dataType: 'json',
    //     type: 'post',
    //     data: {
    //         outline_id: activeOutlineId,
    //     },
    //     error: ajaxError
    // }).done(function (data) {
    //     if (data.status !== 'success') {
    //         toastr.error(data.message);
    //         return;
    //     }
    //     else if (data.status === 'redirect') {
    //         window.location.href = data.redirect;
    //         return;
    //     }
    //
    //     // Removes parenthesis and creates update form for user input.
    //     $.each(data.content, function (index, question) {
    //         const ques = question.tuq_content.replace(/{{|}}/g, function () {
    //             return '';
    //         });
    //
    //         questions_id[index] = question.tuq_id;
    //         question_input = question_input + '<div style="margin-top: 10px"><label><b>' + parseInt(index + 1) + '. ' + ques +
    //             "</b></label>" +
    //             "<div class='input-group'>" +
    //             "<span class='input-group-addon btn btn-secondary tts'><i class='fa fa-bullhorn'></i></span>" +
    //             "<textarea class='form-control answer' rows='1' cols='70' id=" + 'ans' + question.tuq_id +
    //             " name=" + 'ans' + question.tuq_id + " class='answer'" +
    //             " placeholder='Click on the microphone and speak or type text.'></textarea>" +
    //             "<span class='input-group-addon btn btn-secondary stt'><i class='fa fa-microphone'></i></span>" +
    //             "</div></div>";
    //     });
    //
    //     const canvas_container_selector = $('#canvas-container');
    //     canvas_container_selector.html('');
    //     canvas_container_selector.html('<h4 class="main-con-header">Updates Content</h4><form id="response-form"' +
    //         ' class="form-horizontal" style="margin: 5px">' +
    //         question_input +
    //         '</form>'
    //     );
    //

    let z = $('#y').val().replace(/\[\[/g, '<h4>').replace(/\]\]/g, '</h4>')
            .replace(/\(\(/g, '<label style="margin-left: 30px">').replace(/\)\)/g, '</label>\n' +
            '<div class="input-group" style="margin-left: 30px;">' +
            '<span class="input-group-addon btn btn-secondary tts">' +
            '<i class="fa fa-bullhorn"></i></span><textarea onkeyup="auto_grow(this)"' +
            ' class="form-control custom-control update_answer" placeholder="Click Microphone icon and speak or start typing."' +
            ' rows="1" cols="10"></textarea><span class="input-group-addon btn btn-secondary stt">' +
            '<i class="fa fa-microphone"></i>' +
            ' </span></div><span class=\'hidden span_ans\'></span>')
            .replace(/{{/g, '<label>').replace(/}}/g, '</label>\n' +
            ' <div class="input-group"><span class="input-group-addon btn btn-secondary tts">' +
            '<i class="fa fa-bullhorn"></i></span><textarea onkeyup="auto_grow(this)" ' +
            'class="form-control custom-control update_answer" placeholder="Click Microphone icon and speak or start typing."' +
            ' rows="1" cols="70"></textarea><span class="input-group-addon btn btn-secondary stt">' +
            '<i class="fa fa-microphone"></i>' +
            ' </span></div><span class=\'hidden span_ans\'></span>').replace(/<br><br>/g, '<br>')
            .replace(/<\/h4> <br>/g, '</h4>'),

        x = z.trim().match(/(<label(.*?)<\/label>)|(<h4>(.*?)<\/h4>)|(<([^>]+)>)/g),
        t = '';

    $.each(x, function (index, text) {
        t = t + text;
    });

    const canvas_container_selector = $('#canvas-container');
    canvas_container_selector.html('');
    canvas_container_selector.html('<h4 class="main-con-header">Updates Content</h4><form id="response-form"' +
        ' class="form-horizontal" style="margin: 14px">' +
        '<input type="hidden" name="update_id" id="update_id">' +
        t +
        '</form>'
    );

    max = 0;
    $.each($('#response-form textarea'), function (index, t) {
        count = (index + 1);
        $(t).attr('id', 'ans' + count);
        $(t).attr('name', 'ans' + count);
        $(t).val('');
        max = count;
    });

    // Fetches and displays list of updates.
    $.ajax({
        url: jsglobals.base_url + 'tsl/get_update_list',
        dataType: 'json',
        data: {
            outline_id: activeOutlineId,
        },
        type: 'post',
        error: ajaxError
    }).done(function (data) {

        if (data.status !== 'success') {
            toastr.error(data.message);
            return;
        }
        else if (data.status === 'redirect') {
            window.location.href = data.redirect;
            return;
        }
        updateUpdateList(data.update_list);

    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
    });

    // }).fail(function (jqXHR, textStatus) {
    //     toastr.error("Request failed: " + textStatus);
    // }).always(function () {
    // });


    // Buyer Update Full Screen View.
    const expand_update_selector = $('#expand-update');
    expand_update_selector.unbind('click');
    expand_update_selector.click(function (e) {
        $('.update_container').toggleClass('col-md-7', 'col-md-12');
        $('.full_view_hide').toggle();
        $('#canvas-container').toggleClass('full-screen');
        // $('#response-form div').toggleClass('container');

        const expand_update_selector = $('#expand-update');
        (null !== expand_update_selector.html().match(/Expand/g)) ?
            expand_update_selector.html('Return <i class="fa fa-arrows""></i>') :
            expand_update_selector.html('Expand <i class="fa fa-arrows""></i>');
    });
}

// Fetches Buyer update outline.
function refreshUpdatePage() {
    // Display buyer's update buttons.
    $('.email_attachment').css("display", "none");
    $('#update_buttons').css("display", "block");

    $.ajax({
        url: jsglobals.base_url + 'tsl/get_update_outline',
        dataType: 'json',
        type: 'post',
        data: {
            update_phase: $('#update-phase').val(),
            update_type: $('#update-type').val(),
            update_frequency: $('#update-frequency').val(),
            client_type: $('#client-type').val(),
        },
        error: ajaxError
    }).done(function (data) {

        if (data.status !== 'success') {
            toastr.error(data.message);
            return;
        }
        else if (data.status === 'redirect') {
            window.location.href = data.redirect;
            return;
        }

        updateOutline(data.update_outline);

    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
        // Check if the component is a dummy.
        check_dummy_component(client_id);
    });
}


// Display selected Update outline.
function updateOutline(outlines) {

    const update_outline_selector = $('.update-outline'),
        outline_container_selector = $('#outline-container');

    // List outline versions.
    $('#outline-table tbody.tr').remove();
    $.each(outlines, function (index, outline) {

        $('#outline-table tbody')
            .append($('<tr>')
                .append($('<td class="text-center">')
                    .append(index + 1))
                .append($('<td class="text-center">')
                    .append($('<a class="update-outline" data-id="' + outline.tuo_id + '" data-val="' + index + '">' +
                        'Outline V' + outline.tuo_version + '</a>')))
                .append($('<td class="text-center">')
                    .append($('<button class="btn btn-default btn-sm"' +
                        ' data-toggle="modal"' +
                        ' data-target="#delete-update-outline-dialog"' +
                        ' data-backdrop="static"' +
                        ' data-id="' + outline.tuo_id +
                        '" data-name="' + outline.tuo_version +
                        '"> <span class="glyphicon glyphicon-trash"></span>')))
            );
    });

    // Display selected outline text in editor.
    $('.update-outline').click(function () {

        outline_container_selector.html('');
        outline_container_selector.html('<h4 class="main-con-header">Update outline</h4><form id="outline-form">' +
            '<input type="hidden" id="outline_type" name="type">' +
            '<input type="hidden" id="frequency" name="frequency">' +
            '<input type="hidden" id="client_type" name="client">' +
            '<input type="hidden" id="update_phase" name="phase">' +
            '<input type="hidden" id="requester" name="requester">' +
            '<input id="y" value="' + outlines[$(this).data('val')].tuo_content + '" type="hidden" name="outline_content">\n' +
            '<trix-editor input="y" name="tsl_update_editor"></trix-editor>' +
            '</form>');

        obj = {"data-id": outlines[$(this).data('val')].tuo_id};
        activeUpdateId = ref > 0 ? ref : 0;
        setupUpdate(obj);

    });

    // Displays the default outline if no outline present.
    if (outlines.length === 0) {
        let default_outline;
        $.ajax({
            url: jsglobals.base_url + 'tsl/get_update_default',
            dataType: 'json',
            type: 'post',
            data: {
                update_type: $('#update-type').val(),
                update_phase: $('#update-phase').val()
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            default_outline = data.update_outline[0].tud_content;

            $('#outline-table tbody').empty();
            $.each(data.update_outline, function (index, outline) {

                $('#outline-table tbody')
                    .append($('<tr>')
                        .append($('<td class="text-center">')
                            .append(index + 1))
                        .append($('<td class="text-center">')
                            .append($('<a class="update-outline" data-id="0" data-val="' + index + '">' +
                                'Example Outline 1</a>')))
                    );
            });

            outline_container_selector.html('');
            outline_container_selector.html('<h4 class="main-con-header">Update outline</h4><form id="outline-form">' +
                '<input type="hidden" id="outline_type" name="type">' +
                '<input type="hidden" id="frequency" name="frequency">' +
                '<input type="hidden" id="client_type" name="client_type">' +
                '<input type="hidden" id="update_phase" name="phase">' +
                '<input type="hidden" id="requester" name="requester">' +
                '<input id="y" value="' + default_outline + '" type="hidden" name="outline_content">\n' +
                '<trix-editor input="y" name="tsl_update_editor"></trix-editor>' +
                '</form>');

            obj = {"data-id": 0};
            setupUpdate(obj);

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });

    } else if (activeOutlineId === 0) {

        // Display first outline, if no outline is selected.
        outline_container_selector.html('');
        outline_container_selector.html('<h4 class="main-con-header">Update outline</h4><form id="outline-form">' +
            '<input type="hidden" id="outline_type" name="type">' +
            '<input type="hidden" id="frequency" name="frequency">' +
            '<input type="hidden" id="client_type" name="client_type">' +
            '<input type="hidden" id="update_phase" name="phase">' +
            '<input type="hidden" id="requester" name="requester">' +
            '<input id="y" value="' + outlines[0].tuo_content + '" type="hidden" name="outline_content">\n' +
            '<trix-editor input="y" name="tsl_update_editor"></trix-editor>' +
            '</form>');

        obj = {"data-id": outlines[0].tuo_id};
        setupUpdate(obj);
    } else if (update_added === 1) {

        // If new update is added.
        obj = {"data-id": activeOutlineId};
        setupUpdate(obj);
    } else if (outline_added === 1) {
        outline_added = 0;
        update_outline_selector.last().click();
    }
}

// Display list of Buyer's update instances.
function updateUpdateList(updates) {

    $('#tsl-update-nav').empty();

    $('#tsl-update-nav')
        .append($('<li class="text-center" id="0">')
            .append('<a href="#" onclick="setupAnswer(this); return false;"' +
                'data-name="Example Update Version" data-id="0">Example Update Version</a>'));

    // Append to the update list.
    $.each(updates, function (index, update) {
        $('#tsl-update-nav')
            .append($('<li class="text-center" id="' + update.tu_id + '">')
                .append('<a href="#" class="edit-ref" onclick="setupAnswer(this); return false;"' +
                    ' data-name="' + update.tu_name + '"data-id="' + update.tu_id + '">' +
                    '<span class="editable" data-type="text" data-pk = "' + update.tu_id + '" ' +
                    'data-url = "' + jsglobals.base_url + 'tsl/update_update_name">' + update.tu_name
                    + '</span></a></li>'))
    });

    $('#send-update').removeAttr('data-toggle');
    $('#delete-update').removeAttr('data-toggle');


    // Check if reference if undefined (Request is not from sent popup)
    if (!(ref !== undefined && activeUpdateId === -1)) {
        if (updates.length === 0) {
            activeUpdateId = 0;
        } else if (activeUpdateId === 0 || activeUpdateId === -1) {
            obj = {"data-id": updates[0].tu_id, "data-name": updates[0].tu_name};
            setupAnswer(obj);
        } else if (update_added === 1) {
            $('#tsl-update-nav li').last().children().click();
        } else if (ref !== undefined) {
            $("a[data-id=" + ref + "]").trigger('click');
        }

    } else {

        // Fetch the Update Details.
        const update_details = makeAjaxCall('cpdv1/get_update_details', {update_id: ref});

        update_details.then(function (data) {

            $("a[data-id=" + ref + "]").trigger('click');
            // const response = data.update_details[0];
            // $('#client-type').val(response.tuo_client_type);
            // $('#update-frequency').val(response.tuo_frequency);
            // $('#update-type').val(response.tuo_update_type);
            // $('#update-phase').val(response.tuo_update_phase);
            // $('.update-outline[data-id=' + response.tsl_update_outline_id + ']').click();
        });
    }
    setEditable('bottom');
}

// Fetch and display already answered questions for a update.
function setupAnswer(obj) {

    activeUpdateId = $(obj).attr('data-id');

    // Displaying the active email name.
    $('.active').removeClass('active');
    $('#' + activeUpdateId).addClass('active');

    const default_answer = makeAjaxCall('tsl/get_default_update_answer', {});

    default_answer.then(function (data) {
        // console.log(data);
        $.each(data.default_answers, function (index, answer) {
            $('#ans' + answer.tuda_seq).val(answer.tuda_content);

        })
    });


    // Set attribute for send button.
    const send_update_selector = $('#send-update'),
        delete_update_selector = $('#delete-update');

    send_update_selector.attr('data-id', activeOutlineId);
    send_update_selector.attr('data-toggle', 'modal');
    send_update_selector.attr('data-update', activeUpdateId);
    send_update_selector.attr('data-target', '#send-update-dialog');
    send_update_selector.attr('data-backdrop', 'static');
    send_update_selector.attr('data-name', $(obj).attr('data-name'));

    // Set attribute for delete button.
    delete_update_selector.attr('data-id', activeUpdateId);
    delete_update_selector.attr('data-toggle', 'modal');
    delete_update_selector.attr('data-target', '#delete-update-dialog');
    delete_update_selector.attr('data-backdrop', 'static');
    delete_update_selector.attr('data-name', $(obj).attr('data-name'));

    // Fetches the list of already answered questions.
    $.ajax({
        url: jsglobals.base_url + 'tsl/get_update_ans',
        dataType: 'json',
        data: {
            update_id: activeUpdateId,
        },
        type: 'post',
        error: ajaxError
    }).done(function (data) {

        if (data.status !== 'success') {
            toastr.error(data.message);
            return;
        }
        else if (data.status === 'redirect') {
            window.location.href = data.redirect;
            return;
        }

        // Loop through the textarea.
        // $.each($('#response-form textarea'), function (index, t) {
        //     $(t).val('');
        //     $(t).val(default_update[index]);
        // });

        // Add the answers to each of the form text area.
        $.each(data.answer, function (index, answer) {
            // Check if the answer is present.
            ('' !== answer.tua_content) ?
                $('#ans' + answer.tua_question_id).val(answer.tua_content.replace('\\', '')) : '';
        });
    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
        $('#response-form textarea').each(function () {
            $(this).css('height', $(this)[0].scrollHeight + 'px');
        });
    });

    // Makes the names editable.
    $(".edit-ref").children().click(function (event) {
        event.stopPropagation();
    });

    speechFunctionality();
}