$(document).ready(function () {
    var email_outline_id = '';
    var version = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-email-outline-dialog').on('show.bs.modal', function (event) {
        email_outline_id = $(event.relatedTarget).data('id');
        version = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name');

        $('#email-outline-id').val(email_outline_id);
        $('#delete-email-outline-version').html('Outline V' + version);
        $('#view-email-outline-dialog').modal('hide');
    });

    // Deletes Buyer's survey Outline.
    $(document).on('submit', '#delete-email-outline-form', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_email_outline',
            dataType: 'json',
            type: 'post',
            data: {
                email_outline_id: email_outline_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('Survey Outline Deleted.', 'Success!!');
            $('#delete-email-outline-dialog').modal('hide');
            refreshPage();

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
        return false;
    });
});
