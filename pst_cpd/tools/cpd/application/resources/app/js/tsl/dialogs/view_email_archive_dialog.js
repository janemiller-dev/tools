$(document).ready(function () {
    // Fetches the performance score.
    $('#view-email-archive-dialog').on('show.bs.modal', function (event) {
        view_email_archive.init();
    });
});

let view_email_archive = function () {

    let max_seq = 0,
        deal_id;

    // Initialize and get the DMD ID.
    let init = () => {

        let email_archive_table = $('#email-archive-table').DataTable();

        // Destroy the datatable so that it can be rebuilt.
        email_archive_table.destroy();

        // Create the instance table using server-side data.
        email_archive_table = $('#email-archive-table').DataTable({
            "pageLength": 50,
            "serverSide": true,
            "processing": true,
            "ordering": true,
            "ajax": {
                "url": jsglobals.base_url + "tsl/get_email_archive",
                "type": "post",
                "data": {
                    id: (undefined === window.location.search.split('?id=')[1] ?
                        window.location.search.split('?ref=')[1] : window.location.search.split('?id=')[1]),
                    origin: origin,
                    component: req_type
                }
            },
            "columnDefs": [
                {"className": "text-center", "targets": "_all"}
            ],
            "autoWidth": false,
            "columns": [
                {"data": "te_id", "name": "#"},
                {"data": "te_name", "name": "email Name"},
                {"data": "hc_name", "name": "Client Name"},
                {"data": "hca_name", "name": "Asset Name"},
                {"data": "hca_location", "name": "Asset Location"},
            ],
            "rowCallback": function (row, data, index) {

                const content =
                    (data.tp_content != null ? data.tp_content.replace(/\\"/g, "'").replace(/\\/g, '') : null);

                $('td', row).eq(0).html(index + 1);

                $('td', row).eq(1).html(data.te_name + ' ' +
                    '<i class="fa fa-eye" id="view_archieved_email" data-id="' + data.te_id + '" ' +
                    'data-client="' + data.ca_asset_id + '"></i>');
            },
            "initComplete": function () {
                $(document).off('click', '#view_archieved_email');
                $(document).on('click', '#view_archieved_email', function (e) {
                    $('#view-email-archive-dialog').modal('hide');

                    const that = $(this);
                    // Set the content for Editor.

                    const get_email_content = makeAjaxCall('tsl/get_email_content', {
                        'email_id': $(this).attr('data-id'),
                        'client_id': $(this).attr('data-client')

                    });

                    get_email_content.then(function (data) {
                        let content = '';
                        $.each(data.answers, function (index, text) {
                            content += '<p>' + text.tea_content + '</p>';
                        });

                        if (data === 'null') {
                            $('#view-dialog').modal('show', that);
                        }
                        else {
                            $('#send-email-dialog').modal('show');
                            tinymce.remove('#content-container');

                            // Initialize Tiny MCE editor.
                            tinymce.init({
                                mode: 'exact',
                                selector: '#content-container',
                                branding: false,
                                setup: function (ed) {
                                    ed.on('init', function (args) {
                                        tinymce.activeEditor.setContent(content);
                                    });
                                },
                                init_instance_callback: function (ed) {
                                    tinyMCE.DOM.setStyle(tinyMCE.DOM.get('content-container_ifr'), 'height', '60vh');
                                }
                            });
                        }
                    });
                });
            }
        });

        // });
    };
    return {
        init: init
    };
}();