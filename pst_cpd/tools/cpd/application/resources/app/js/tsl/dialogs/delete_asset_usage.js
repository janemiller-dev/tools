$(document).ready(function () {
    let asset_id = '',
     ui_name = '';

    // When the dialog is displayed, asset-usage the current instance ID.
    $('#delete-asset-usage-dialog').on('show.bs.modal', function (event) {
        asset_id = $(event.relatedTarget).data('id');
        ui_name = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name')

        $('#asset-usage-name').html(ui_name);
    });

    $(document).on('click', '#close-delete-asset-usage-button', function () {
        $('#delete-asset-usage-dialog').modal('hide');
    });

    // Removes selected message asset-usage.
    $(document).on('submit', '#delete-asset-usage-form', function (e) {
        e.preventDefault();

        $.ajax({
            url: jsglobals.base_url + 'homebase/delete_prev_instance',
            dataType: 'json',
            type: 'post',
            data: {
                tc_id: asset_id,
                table_name: 'tsl_client'
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('Asset Instance Deleted', 'Success!!');
            fetchTSL();
            $('#delete-asset-usage-dialog').modal('hide');
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });

        return false;
    });
});
