var tsl_data;
$(document).ready(function () {

    $(document).on('click', '#add-location, #add-criteria', function () {
        $('#add-tsl-dialog').modal('hide')
    });

    $(document).on('change', '#tsl-product-id', function (e) {
        if ($(this).val() === '-1') {
            e.preventDefault();
            e.stopPropagation();

            $('#modal-id').val('add-tsl-dialog');
            $('#add-product-dialog').modal('show');
        }
    });

    // When the dialog is displayed, clear the values
    $('#add-tsl-dialog').on('show.bs.modal', function (event) {
        // Clear the select list.
        $('#tsl-year, #tsl-product-id, #tsl-focus-id').empty();

        // Add form cache.
        $('#add-tsl-form').formcache("clear");
        $('#add-tsl-form').formcache("destroy");

        // Initialize the year dropdown.
        updateYears('#tsl-year');
        init();
    });

    function init() {

        // Fetches the list of Products.
        const get_all_products = makeAjaxCall('industry/get_all_products', {});
        get_all_products.then(function (data) {
            updateTSLProducts(data.products);
        });

        // Click handler for TSL menu.
        $(document).off('change', '.tsl_ui_menu');
        $(document).on('change', '.tsl_ui_menu', function (e) {
            if ('0' === $(this).val()) {
                $(this).val('-1');
                $('#add-tsl-dialog').modal('hide');
                $('#' + $(this).attr('data-target')).modal('show');
                return;
            }
        });

        // Fetch the list of all TSL criteria/prepared by/target audience for this user.
        const get_tsl_info = makeAjaxCall('tsl/get_user_data', {});

        get_tsl_info.then(function (data) {

            // Clear out any previous Options.
            $('#tsl_ui_prepared_by, #tsl_ui_criteria, #tsl_ui_audience').empty();

            // Append Option for Location.
            $('#tsl_ui_audience')
                .append($.map(data.info.target_audience, function (audience, index) {
                    return '<option value="' + audience.ul_id + '">'
                        + audience.ul_name + '</option>';
                }));

            // Append Option for Category Name.
            $('#tsl_ui_criteria')
                .append($.map(data.info.criteria, function (criterion, index) {
                    return '<option value="' + criterion.uc_id + '">'
                        + criterion.uc_name + '</option>';
                }));

            $('.select-picker').selectpicker('refresh');
            $('.select-picker').selectpicker('deselectAll');
        });

        // Fetch info for assets.
        const get_asset_info = makeAjaxCall('homebase/get_asset_info', {});

        get_asset_info.then(function (data) {
            $('#tsl_ui_location').empty();

            $.each(data.asset_info, function (index, info) {
                if (info.length !== 0) {
                    $('#tsl_ui_' + index).empty();

                    $.each(info, function (key, data) {
                        ('desc' === index || 'cond' === index) ?
                            $('#tsl_ui_' + index)
                                .append('<option value="' + data.hai_id + '">' + data.hai_value + '</option>') :
                            $('#tsl_ui_location')
                                .append('<option value="' + data.hai_id + '">' + index + ' : ' + data.hai_value + '</option>')
                    })
                }
            });

            $('.select-picker').selectpicker('refresh');
            $('.select-picker').selectpicker('deselectAll');
        })
    }

    function updateTSLProducts(products) {

        $('#tsl-product-id').empty().attr('disabled', false);

        // Fetch and append criteria/location to view location/criteria popup.
        get_all_criteria();

        // Show the industries for selection.
        $('#tsl-product-id').append($('<option>')
            .attr({value: 0})
            .append("Select Product"));

        const product_selector = $('#tsl-product-id');

        // Append option to add new product.
        product_selector.append($('<option>')
            .attr({value: -1})
            .attr({'data-id': 'add-tsl-dialog'})
            .append("(( -- Add New Product -- ))"));

        $.each(products, function (i, product) {
            product_selector.append('<option style="font-weight: bold; font-style: italic;"' +
                ' data-id="' + product[0]['u_product_group_id'] + '"' +
                ' value="' + product[0]['u_product_group_id'] + '" data-type="group">' + i + '</option>');

            $.each(product, function (index, data) {
                product_selector.append($('<option>')
                    .attr({value: data.u_product_id})
                    .append(data.u_product_name));
            });
        });
    }

    // Validate the add tsl form and submit it if it is valid.
    $('#add-tsl-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            tsl_ui_name: {
                validators: {
                    notEmpty: {
                        message: "TSL name is required."
                    }
                }
            },
            tsl_ui_description: {
                validators: {
                    notEmpty: {
                        message: "TSL description is required."
                    }
                }
            },
            tsl_product_id: {
                validators: {
                    notZero: {
                        message: "Product type is required."
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        console.log('here');
        e.preventDefault();

        // Get the form tsl
        var $form = $(e.target),
            fv = $form.data('formValidation');

        // Get the form data and submit it.
        tsl_data = $form.serialize();
        addTSL();

        // fetchTSL();
    });

});

function fetchTSL() {
    $('#asset-used-table tbody').empty();

    // Push question ID to the form data.
    const fetch_assets = makeAjaxCall('tsl/fetch_assets_used', tsl_data);
    fetch_assets.then(function (data) {

        if (0 !== data.tsl_assets.length) {
            $('#view-asset-used-dialog').modal('show');

            $.each(data.tsl_assets, function (index, asset) {

                $('#asset-used-table tbody')
                    .append($('<tr>')
                        .append($('<td class="text-center">')
                            .append(index + 1))
                        .append($('<td class="text-center">')
                            .append(asset.hc_name))
                        .append($('<td class="text-center">')
                            .append(asset.hca_name))
                        .append($('<td class="text-center">')
                            .append(asset.tsl_name))
                        .append($('<td class="text-center">')
                            .append($('<button class="btn btn-default btn-sm"' +
                                ' data-toggle="modal" data-target="#delete-asset-usage-dialog"' +
                                ' data-backdrop="static" data-id="' + asset.tc_id + '"' +
                                ' data-name="' + asset.hca_name + '">' +
                                ' <span class="glyphicon glyphicon-trash"></span></button>')))
                    );

            });
        } else {
            addTSL();
        }
    });

    $(document).off('click', '#submit_add_tsl');
    $(document).on('click', '#submit_add_tsl', function () {
        $('#view-asset-used-dialog').modal('hide');
        addTSL();
    });
}

// Adds a new TSL instance.
function addTSL() {
    // Adds a new TSL instance.
    const add_tsl = makeAjaxCall('tsl/add_tsl', tsl_data);

    add_tsl.then(function (data) {
        $('#add-tsl-dialog').modal('hide');

        refreshProducts();
        refreshPage();
    });
}
