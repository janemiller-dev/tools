$(document).ready(function () {
    // Fetches the performance score.
    $('#view-script-archive-dialog').on('show.bs.modal', function (event) {
        view_script_archive.init();
    });
});

let view_script_archive = function () {

    let max_seq = 0,
        deal_id;

    // Initialize and get the DMD ID.
    let init = () => {

        let script_archive_table = $('#script-archive-table').DataTable();

        // Destroy the datatable so that it can be rebuilt.
        script_archive_table.destroy();

        // Create the instance table using server-side data.
        script_archive_table = $('#script-archive-table').DataTable({
            "pageLength": 50,
            "serverSide": true,
            "processing": true,
            "ordering": true,
            "ajax": {
                "url": jsglobals.base_url + "tsl/get_script_archive",
                "type": "post",
                "data": {
                    id: (undefined === window.location.search.split('?id=')[1] ?
                        window.location.search.split('?ref=')[1] : window.location.search.split('?id=')[1]),
                    origin: origin,
                    component: req_type
                }
            },
            "columnDefs": [
                {"className": "text-center", "targets": "_all"}
            ],
            "autoWidth": false,
            "columns": [
                {"data": "ts_id", "name": "#"},
                {"data": "tsn_name", "name": "Script Name"},
                {"data": "hc_name", "name": "Client Name"},
                {"data": "hca_name", "name": "Asset Name"},
                {"data": "hca_location", "name": "Asset Location"},
            ],
            "rowCallback": function (row, data, index) {

                const content =
                    (data.tp_content != null ? data.tp_content.replace(/\\"/g, "'").replace(/\\/g, '') : null);

                $('td', row).eq(0).html(index + 1);

                $('td', row).eq(1).html(data.tsn_name + ' ' +
                    '<i class="fa fa-eye" id="view_archieved_script" data-id="' + data.ts_id + '" ' +
                    'data-content="' + content + '"></i>');

                // $('td', row).eq(2).html(page_format);
            },
            "initComplete": function () {
                $(document).off('click', '#view_archieved_script');
                $(document).on('click', '#view_archieved_script', function (e) {
                    $('#view-script-archive-dialog').modal('hide');

                    const that = $(this);
                    // Set the content for Editor.
                    const content = $(this).attr('data-content');
                    if (content === 'null') {
                        $('#view-dialog').modal('show', that);
                    }
                    else {
                        $('#view-script-archive-instance-dialog').modal('show');
                        tinymce.remove('#archive-viewer-container');

                        // Initialize Tiny MCE editor.
                        tinymce.init({
                            mode: 'exact',
                            selector: '#archive-viewer-container',
                            branding: false,
                            setup: function (ed) {
                                ed.on('init', function (args) {
                                    tinymce.activeEditor.setContent(content);
                                });
                            },
                            init_instance_callback: function (ed) {
                                tinyMCE.DOM.setStyle(tinyMCE.DOM.get('archive-viewer-container_ifr'), 'height', '65vh');
                            }
                        });
                    }
                });
            }
        });

        // });
    };
    return {
        init: init
    };
}();