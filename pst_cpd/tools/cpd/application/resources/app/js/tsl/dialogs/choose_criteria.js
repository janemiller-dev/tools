/**
 *
 * Choose a new Criteria for TSL.
 *
 * @summary      Dialog used to select new criteria for TSL.
 * @description  This file contains functions for selecting new criteria for a TSL
 *               which will then fetch the assets from homebase.
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */

$(document).ready(function () {

    $('#choose-criteria-dialog').on('show.bs.modal', function (event) {
        $('#view-tsl-criteria-dialog').modal('hide');
        choose_criteria.init(event);
    });

});

/**
 *
 * View Notes available.
 */
let choose_criteria = function () {

    // Initialize and get the User Data.
    let init = function (event) {

        // Fetch User Info from DB.
        const fetch_criteria = makeAjaxCall('tsl/get_user_tsl_data', {
            'tsl_id' : tsl_id
        });

        // Append options to criteria menu.
        $('#choose-criteria').empty();
        $('#choose-criteria').append('<option disabled selected>Choose New TSL Criteria</option>')

        fetch_criteria.then(function (data) {
            $.each(data.tsl_data.criteria, function (index, criterion) {
                $('#choose-criteria').append('<option value="' + criterion.uc_id + '">'
                    + criterion.uc_name + '</option>')
            });

            // Criteria Change Event.
            $(document).off('change', '#choose-criteria');
            $(document).on('change','#choose-criteria' ,function () {
                const update_tsl_data = makeAjaxCall('tsl/update_tsl_data', {
                    val: $(this).val(),
                    id: tsl_id,
                    type: 'criteria'
                });

                update_tsl_data.then(function (data) {
                    console.log(data);
                });

            })
        });

    };

    return {
        init: init,
    };
}();