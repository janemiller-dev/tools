let caller, assignee, notes, active_sam, target_audience, tsl_criteria, cpd_id, tsl_id, tsl_agent, tsl_target_selector,
    call_status, call_duration, timeout_var, tsl_description, products, portfolio;

$(document).ready(function () {
    const path = window.location.pathname,
        components = path.split('/');

    tsl_target_selector = $('#tsl-list-target');

    tsl_id = components[components.length - 1];
    fetch_call_status();
    getComponents();
    refreshDashboard();

    get_all_products().then(function (data) {
        products = data;
    });

    // Reload TSL click Handler
    $(document).off('click', '#reload-tsl');
    $(document).on('click', '#reload-tsl', function (e) {
        const reload_tsl = makeAjaxCall('tsl/reload_tsl', {
            'tsl_id': tsl_id,
            'criteria': $('#tsl-list-criteria').val(),
            'area': $('#tsl-list-target').val(),
            'location': $('#tsl-list-loc').val(),
            'desc': $('#tsl-list-desc').val(),
            'cond': $('#tsl-list-cond').val()
        });

        reload_tsl.then(function (data) {
            toastr.success(data.tsl_reloaded.client_fetched.temp_updated ? 'Asset Added.' : 'No Asset Added.');
            refreshPage($('.tsl-tabs li.active').attr('data-type'))
        })
    });

    // Mark Bad Click handler.
    $(document).on('click', '.mark-bad', function () {
        const selected_option = $(this).parent().parent().siblings('select').find('option:selected'),
            id = $(selected_option).attr('data-id'),
            table = $(selected_option).attr('data-table'),
            col = $(selected_option).attr('data-name');

        if (!selected_option.val().includes('Number Needed')) {
            updateClientInfo(id, $(selected_option).attr('data-name'), 'Number Needed', table)
            refreshPage($('.tsl-tabs li.active').attr('data-type'))
        } else {

        }
    });

    // Tab change event handler.
    $('.tsl-tabs li').off('click');
    $('.tsl-tabs li').click(function () {
        refreshPage($(this).attr('data-type'));
    });

    // Sort By functionality.
    $(document).on('change', '#sort-by', function () {
        refreshPage($('.tsl-tabs li.active').attr('data-type'))
    });

    // Check for metric fields.
    $(document).on('change', '.metric-period', function () {
        get_metric_data($(this).val())
    });

    $('#view-sam-dialog').on('show.bs.modal', function (event) {
        $('#start-time, #end-time, #duration').val('')
        const get_active_sam = makeAjaxCall('tsl/get_active_sam', {
            tsl_id: tsl_id
        });

        get_active_sam.then(function (data) {
            if (0 < data.active_sam.length) {
                const sam_data = data.active_sam[0];
                $('#start-time').val(sam_data.ts_start_time);
                $('input[name="sam-control"][value="' + sam_data.ts_state + '"]').prop('checked', true);

                const duration = moment.utc(moment.utc(moment().format("hh:mm A"), "HH:mm") -
                    moment.utc(sam_data.ts_start_time, "HH:mm")).format('H:mm');

                $('#duration').val(duration);
            }
        })
    });

    // Start/Stop Call Session Change handler.
    $(document).on('change', 'input[name="sam-control"]', function () {
        if ('start' === $(this).val()) {
            if (null === active_sam) {
                const begin_session = makeAjaxCall('tsl/begin_call_session', {
                    'tsl_id': tsl_id,
                    'day': moment().format('dddd'),
                    'day_numeric': moment().format('d'),
                    'time': moment().format('h:mm a'),
                    'date': moment().format("MMM Do YY"),
                    'month': moment().format("M"),
                    'seq': 1,
                    'duration': $('#session-duration').val()
                });

                begin_session.then(function (data) {
                    $('#start-time').val(moment().format('h:mm a'));

                    refreshDashboard();
                    toastr.success('Call Session Started!');
                });
            } else {
                toastr.warning('Please end previous Call Session.', 'Already Active Session!!');
            }
        } else if ('stop' === $(this).val()) {
            if (null === active_sam) {
                toastr.warning('Please start a new call session.', 'No Active Session.')
            } else {
                const end_session = makeAjaxCall('tsl/end_call_session', {
                    'session_id': active_sam,
                    'time': moment().format('h:mm a')
                });

                end_session.then(function () {
                    $('#end-time').val(moment().format('h:mm a'));

                    refreshDashboard();
                    toastr.success('Call Session Ended!');
                })
            }
        } else {
            if (null === active_sam) {
                toastr.warning('Please start a new call session.', 'No Active Session.')
            } else {
                const update_session = makeAjaxCall('tsl/pause_restart_call_session', {
                    'session_id': active_sam,
                    'val': ('pause' === $(this).val() ? 'pause' : 'start')
                });
            }
        }
    });
    // Update TS field on data change.
    $(document).on('change', '.update_ts', function () {
        var that = $(this);

        if ((active_sam !== null) &&
            ('tac_result' === $(this).attr('data-name')) &&
            ('scheduled' !== $(this).find('option:selected').val())) {
            return;
        }
        const update_call_session = makeAjaxCall('tsl/update_call_session', {
            'session_id': active_sam,
            'col_name': $(this).attr('data-ts-name')
        });
    });

    // Remove TSL Client dialog modal show event handler.
    $('#remove-tsl-client-dialog').on('show.bs.modal', function (event) {
        const tc_id = $(event.relatedTarget).attr('data-id'),
            asset_id = $(event.relatedTarget).attr('data-hca');

        $('#delete-tsl-client-name').text($(event.relatedTarget).attr('data-name'));
        $('#delete-tsl-asset-name').text($(event.relatedTarget).attr('data-asset'));

        // Remove TSL click handler.
        $('#remove-tsl-client-button').off('click');
        $('#remove-tsl-client-button').click(function () {
            const remove_tsl_client = makeAjaxCall('tsl/remove_tsl_client', {
                tc_id: tc_id,
                asset_id: asset_id
            });

            remove_tsl_client.then(function () {
                toastr.success('Client Asset removed from TSL.', 'Success!!');
                $('#remove-tsl-client-dialog').modal('hide');
                refreshPage($('.tsl-tabs li.active').attr('data-type'))
            })
        });
    });

    // Remove TSL Past call dialog modal show event handler.
    $('#delete-past-call-dialog').on('show.bs.modal', function (event) {
        $('#delete-past-call-tsl-name').text($('#tsl-name').val());

        $('#delete-past-call-button').off('click');
        $('#delete-past-call-button').on('click', function () {
            const delete_past_calls = makeAjaxCall('tsl/delete_past_call', {
                id: tsl_id
            })

            delete_past_calls.then(function () {
                toastr.success('Past Calls Deleted.', 'Success!!');
                $('#delete-past-call-dialog').modal('hide');
                refreshPage($('.tsl-tabs li.active').attr('data-type'))
            })
        });
    });
});

// Fetch Caller and Assignee name List.
function refreshDashboard() {

    const get_tsl_dropdown = makeAjaxCall('homebase/get_asset_info', {});

    get_tsl_dropdown.then(function (data) {
        $.each(data.asset_info, function (key, info) {
            $('#tsl-list-' + key).append(
                $.map(info, function (ind_info, index) {
                    return '<option value="' + ind_info.hai_id + '">' + ind_info.hai_value + '</option>';
                }).join('')
            );

            // Check if city/state/zip is selected if so append it to location drop down.
            if (key === 'city' || key === 'state' || key === 'zip') {
                key = key.charAt(0).toUpperCase() + key.slice(1);

                $.each(info, function (index, ind_info) {
                    $('#tsl-list-loc')
                        .append('<option value="' + ind_info.hai_id + '">' + key + ' : ' + ind_info.hai_value + '</option>');
                });
            }
        })
    });

    // Fetch Caller List.
    $.ajax({
        url: jsglobals.base_url + 'tsl/get_tsl_data',
        dataType: 'json',
        type: 'post',
        data: {
            tsl_id: tsl_id
        },
        error: ajaxError
    }).done(function (data) {

        if (data.status !== 'success') {
            toastr.error(data.message);
            return;
        } else if (data.status === 'redirect') {
            window.location.href = data.redirect;
            return;
        }
        caller = data.tsl_data.caller;
        assignee = data.tsl_data.assignee;

        // notes = data.tsl_data.notes;
        target_audience = data.tsl_data.audience;
        tsl_criteria = data.tsl_data.criteria;
        tsl_agent = data.tsl_data.agent;
        // actions = data.tsl_data.actions;
        cpd_id = data.tsl_data.tsl[0].uact_cpd_id;
        active_sam = data.tsl_data.tsl[0].ts_id;
        call_duration = data.tsl_data.tsl[0].ts_duration;
        tsl_description = data.tsl_data.tsl[0].tsl_description;
        portfolio = data.tsl_data.portfolio;

        if (data.tsl_data.tsl[0].cpd_name !== null) {
            $('#linked-cpd').html(data.tsl_data.tsl[0].cpd_name.link(jsglobals.base_url + 'cpdv1/cpdv1/' + cpd_id));
        } else {
            $('#linked-cpd').html('No DMD')
        }

        $('#tsl-name').val(data.tsl_data.tsl[0].tsl_name);
        $('#tsl-date').val(data.tsl_data.tsl[0].tsl_modified);

        // Refresh drop down and select the promo focus.
        const refresh_dropdown = refreshDMDDropdown();
        refresh_dropdown.then(function (data) {
            $('#promo-focus').val(tsl_description);
        })

        updateAssignee();
        updateCaller();
        refreshPage($('.tsl-tabs li.active').attr('data-type'))
        get_metric_data();
        // get_sam_data();

    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
    });
}

function get_metric_data(period = '') {
    const get_metric_data = makeAjaxCall('tsl/get_metric_data',
        {
            'tsl_id': tsl_id,
            'period': period
        });

    get_metric_data.then(function (data) {
        // Metrices Popover Content.
        $('#tsl-metric').attr('data-content',
            '<div class="row">' +
            '<div class="col-xs-3" style="border-right: 1px solid #e1c8c8">' +
            '<span class="col-xs-12 text-center"><b>Dials </b></span>' +
            '<input type=\'text\' class=\'form-control\' style=\'display:inline-block;\'' +
            ' value="' + data.tsl_metric.dial[0].dial + '">' +
            '<span  class="col-xs-12 text-center"><b>No Answers </b></span>' +
            '<input type=\'text\' class=\'form-control\' style=\'display:inline-block;\'' +
            ' value="' + data.tsl_metric.na[0].na + '">' +
            '</div>' +
            '<div class="col-xs-3" style="border-right: 1px solid #e1c8c8">' +
            '<span class="col-xs-12 text-center"><b> Contacts </b></span>' +
            '<input type=\'text\' class=\'form-control\' style=\'display:inline-block;\'' +
            ' value="' + data.tsl_metric.cnt[0].contacts + '">' +
            '<span class="col-xs-12 text-center"><b>Left Messages </b></span>' +
            '<input type=\'text\' class=\'form-control\' style=\'display:inline-block;\'' +
            ' value="' + data.tsl_metric.lm[0].lm + '">' +
            '</div>' +
            '<div class="col-xs-3" style="border-right: 1px solid #e1c8c8">' +
            '<span class="col-xs-12 text-center"><b> Conversations </b></span>' +
            '<input type=\'text\' class=\'form-control\' style=\'display:inline-block;\'' +
            '  value="' + data.tsl_metric.cnv[0].conversation + '">' +
            '<span class="col-xs-12 text-center"><b>Call Backs </b></span>' +
            '<input type=\'text\' class=\'form-control\' style=\'display:inline-block;\'' +
            'value="' + data.tsl_metric.cb[0].cb + '">' +
            '</div>' +
            '<div class="col-xs-3" style="border-right: 1px solid #e1c8c8">' +
            '<span class="col-xs-12 text-center"><b> Meetings </b></span>' +
            '<input type=\'text\' class=\'form-control\' style=\'display:inline-block;\'' +
            '  value="' + data.tsl_metric.sch[0].scheduled + '">' +
            '<span class="col-xs-12 text-center"><b>Period </b></span>' +
            '<select class=\'form-control metric-period\' style=\display:inline-block;\'>' +
            '<option disabled selected value="">-- Select Period --</option>' +
            '<option value="">-- None --</option>' +
            '<option value="' + moment().subtract(1, "days").format("YYYY-MM-DD h:mm:ss") + '">Today</option>' +
            '<option value="' + moment().subtract(1, "weeks").format("YYYY-MM-DD h:mm:ss") + '">This Week</option>' +
            '<option value="' + moment().subtract(1, "months").format("YYYY-MM-DD h:mm:ss") + '">This Month</option>' +
            '<option value="' + moment().subtract(1, "quarters").format("YYYY-MM-DD h:mm:ss") + '">This Quarter</option>' +
            '<option value="' + moment().subtract(1, "years").format("YYYY-MM-DD h:mm:ss") + '">This Year</option>' +
            '</option>' +
            '</select>' +
            '</div>' +
            '</div>');

        $('#tsl-metric').data('bs.popover').setContent();
    });

    $('#promo-focus').val(tsl_description);
}

// Reloads the content for the page.
function refreshPage(buyer_type = '') {
    $.ajax({
        url: jsglobals.base_url + 'tsl/get_client_list',
        dataType: 'json',
        data: {
            tsl_id: tsl_id,
            sort_by: $('#sort-by').val(),
            buyer_type: buyer_type
        },
        type: 'post',
        error: ajaxError
    }).done(function (data) {

        if (data.status === 'failure') {
            toastr.error(data.message);
            return;
        } else if (data.status === 'redirect') {
            window.location.href = data.redirect;
            return;
        }

        updateClientList(data.client, buyer_type);

    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
    });
}

// Adds clients to TSL
function updateClientList(clients, buyer_type) {

    // Show the clients if there are any.
    if (clients === undefined || clients.length === 0) {
        $('#no-tsl-instance').show();

        // Displays a Dummy Row for TSL.
        var dummy_row = $('.sample-tr').clone();
        $('.additional-table').addClass('hidden');
        $('#tsl-instance-0-table > tbody').empty();
        $('#tsl-buyer-instance-0-table > tbody').empty();
        $('#tsl-instance-0-table > tbody').append('<tr>' + dummy_row.html() + '</tr>');

        $('.dummy_promo').attr('data-content', '<div class=\'row col-xs-12\'>\n' +
            '<table style=\'width:100%\' class=\'table table-striped\'>\n' +
            '<tr>\n' +
            '<thead>\n' +
            '<th>Promos</th>\n' +
            '<th>Messages</th>\n' +
            '<th>Scripts</th>\n' +
            '<th>Confirms</th>\n' +
            '<th>Stories</th>\n' +
            '</thead>\n' +
            '</tr>\n' +
            '<tbody>\n' +
            '<tr>\n' +
            $.map(universal_components.components, function (component, index) {

                // Append Dummy Promo components.
                if ((component.uc_type === 'promo') && (component.uc_tool === 'tsl')) {
                    return ('<td class=\'text-center\'>' +
                        '<a class=\'btn btn-default btn-sm coming-soon-tool\'  href=' + jsglobals.base_url + 'tsl'
                        + component.uc_controller
                        + 0 + '?id=' + tsl_id + ' id=' + component.uc_name + '>' +
                        '<span class=\'glyphicon glyphicon-' + component.uc_icon + '\'></span></a>' +
                        '</td>');
                }
            }).join('') +
            '</tr>\n' +
            '</tbody>\n' +
            '</table>\n' +
            '</div>\n' +
            '</div>');

        $('.dummy_sap').attr('data-content', '<div class=\'row col-xs-12\'>\n' +
            '<table style=\'width:100%\' class=\'table table-striped\'>\n' +
            '<tr>\n' +
            '<thead>\n' +
            '<th>Profiles</th>\n' +
            '<th>Worksheets</th>\n' +
            '<th>Analysis</th>\n' +
            '<th>Updates</th>\n' +
            '<th>Notices</th>\n' +
            '</thead>\n' +
            '</tr>\n' +
            '<tbody>\n' +
            '<tr>\n' +
            $.map(universal_components.components, function (component, index) {
                if ((component.uc_type !== 'promo') && (component.uc_tool === 'tsl')) {
                    return ('<td class=\'text-center\'>' +
                        '<a class=\'btn btn-default btn-sm coming-soon-tool\'  href=' + jsglobals.base_url + 'tsl'
                        + component.uc_controller
                        + 0 + '?id=' + tsl_id + ' id=' + component.uc_name + '>' +
                        '<span class=\'glyphicon glyphicon-' + component.uc_icon + '\'></span></a>' +
                        '</td>');
                }
            }).join('') +
            '</tr>\n' +
            '</tbody>\n' +
            '</table>\n' +
            '</div>\n' +
            '</div>');

        $('.dummy_send').attr('data-content', '<div class=\'row col-xs-12\'>\n' +
            '<div class=\'col-xs-4\'>' +
            '<table style=\'width:100%\' class=\'table table-striped\'>\n' +
            '<tr>\n' +
            '<thead>\n' +
            '<th>Confirm</th>\n' +
            '<th>Survey</th>\n' +
            '<th>Profile</th>\n' +
            '<th>Analysis</th>\n' +
            '<th>Updates</th>\n' +
            '<th>Docs</th>\n' +
            '</thead>\n' +
            '</tr>\n' +
            '<tbody>\n' +
            '<tr>\n' +
            $.map(universal_components.components, function (component, index) {
                if ((component.uc_type === 'promo') &&
                    (component.uc_name !== 'Review') &&
                    (component.uc_name !== 'Event') &&
                    (component.uc_name !== 'Worksheet') &&
                    (component.uc_name !== 'Followup') &&
                    (component.uc_tool === 'cpdv1')) {
                    return ('<td class=\'text-center\'><button class=\'btn btn-default btn-sm\'>' +
                        '<span class=\'glyphicon glyphicon-' + component.uc_icon + '\'></span></button>' +
                        '</td>');
                }
            }).join('') +
            $.map(universal_components.components, function (component, index) {
                if ((component.uc_type !== 'promo') && (component.uc_tool === 'cpdv1')) {
                    return ('<td class=\'text-center\'><button class=\'btn btn-default btn-sm\'>' +
                        '<span class=\'glyphicon glyphicon-' + component.uc_icon + '\'></span></button>' +
                        '</td>');
                }
            }).join('') +
            '</tr>\n' +
            '</tbody>\n' +
            '</table>\n' +
            '</div>\n' +
            '</div>');

    } else {

        $('#no-tsl-instance').hide();
        $('#tsl-instance-div').show();
        // Appends client's information.
        $.each(clients, function (i, level) {
            $('#tsl-' + $('.tsl-tabs li.active').attr('data-type') +
                'instance-' + level[0].tac_level + '-table tbody').empty();
            $('#tsl-' + $('.tsl-tabs li.active').attr('data-type') +
                'instance-' + level[0].tac_level + '-table').removeClass('hidden');

            // check if the table has more rows to display
            if (5 === level.length) {
                $('#tsl-' + $('.tsl-tabs li.active').attr('data-type') + 'instance-'
                    + level[0].tac_level + '-show-more').removeClass('hidden');
            }

            // Append rows to table.
            append_rows(level, 0, buyer_type);
        });

        // Disable components buttons which are under development.
        disableComponent();
    }

    $('[data-toggle="popover"]').popover();

    // Set format for Date time picker.
    const tsl_date_selector = $('.tsl-date');
    tsl_date_selector.datetimepicker({
        format: 'YYYY-MM-DD HH:mm:ss',
        onChangeDateTime: function (dp, $input) {
            if ($(this).val() === '') {
                $(this).val($input.val());
                return;
            }
            let value = $input.val(),
                table = $input.data('table');

            updateClientInfo($input.data('id'), $input.data('name'), value, table);
        }
    });
    $('.tsl-onlydate').datetimepicker({
        format: 'YYYY-MM-DD',
        timepicker: false
    });

    useTooltip();
    usePopover();

    // Disable all the Twice called clients row.
    disableRows();

    // Event listener for select boxes.
    const tsl_select_box_selector = $('#tsl-prepared-by, #tsl-list-criteria, .tsl-action, #tsl-target,' +
        ' #tsl-caller, .tsl-assignee');

    tsl_select_box_selector.unbind('click');

    tsl_select_box_selector.click(function () {
        // If the change is to "new agent", show add agent dialog.
        if ($(this).val() === "0") {
            $('#' + $(this).attr('data-target')).modal('show');
            $(this).val('-1');
        }
    });

    // TSL dial button functionality.
    $(document).off('click', '.tsl-dial');
    $(document).on('click', '.tsl-dial', function () {
        const that = $(this);
        $.ajax({
            url: jsglobals.base_url + 'tsl/push_client',
            dataType: 'json',
            data: {
                caller: $('#tsl-caller').find(':selected').text(),
                client_id: $(this).data('id'),
                sequence: $(this).data('sequence'),
                tac_id: $(this).data('tac'),
                tac_level: $(this).data('level')
            },
            type: 'post',
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            // Update TSL SAM data.
            if ('1' === $(that).attr('data-sequence')) {
                makeAjaxCall('tsl/update_call_session',
                    {
                        'session_id': active_sam,
                        'col_name': 'ts_dials'
                    });

                // Show Dial modal.
                $("#view-ccm-dialog").modal('show', $(that));
            }
            // refreshDashboard();
            refreshPage($('.tsl-tabs li.active').attr('data-type'));
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });

    // Add double scroll to TSL.
    enableDoubleScroll('#tsl-instance-div');
    enable_show();

    // Clear date time field
    tsl_date_selector.click(function () {
        $date_val = $(this).val();
        $(this).val('');
    });

    // Auto save client info as any field is changed.
    $(document).off('change', '.edit-client-info');
    $(document).on('change', '.edit-client-info', function () {
        let value = $(this).val(),
            table = $(this).attr('data-table'),
            that = $(this);

        if ($(this).attr('type') === "checkbox") {
            value = $('#' + $(this).data("name") + $(this).data('id')).prop('checked') ? 1 : 0;
        } else {
            value = $(this).val();
        }

        if ($(this).val() === '') {
            $(this).val($date_val);
            return;
        }

        let buyer_array = [];
        buyer_array['hca_name'] = 'hba_entity_name';
        buyer_array['hc_selected_phone'] = 'hb_selected_phone';

        console.log(table);
        if ('buyer-' !== $(this).attr('data-buyer-type') || table === 'tsl_about_client') {
            updateClientInfo($(this).attr('data-id'), $(this).attr('data-name'), value, table);
        } else {
            // Check for the table name.

            if (table === 'homebase_client_asset') {
                table = 'homebase_buyer_asset';
            } else {
                table = 'homebase_buyer';
            }

            updateClientInfo($(this).attr('data-id'), buyer_array[$(this).attr('data-name')], value, table);
        }
    });

    // View Phone Dialog.
    $(document).on('change', '.phone-field', function (e) {
        if ("-1" === $(this).val()) {
            $('#view-client-phone-dialog').attr('data-id', $(this).attr('data-id'));
            $('#view-client-phone-dialog').modal('show');
        }
    });

    // View Call Status Dialog
    $(document).on('change', '.tsl-call-status', function (e) {
        if ("-1" === $(this).val()) {
            e.preventDefault();
            e.stopPropagation();
            $('#view-call-status-dialog').modal('show');
        }
    });

    refreshAudience();
    refreshCriteria();
    $('.select-picker').selectpicker();
}

function setTimeOut() {
    // const timeout_duration = 60000 * call_duration;
    const timeout_duration = 10;
    // Check if time out variable is set.
    (null !== timeout_var) ? clearTimeout(timeout_var) : '';

    timeout_var = setTimeout(function () {
        // Your function here

        toastr.warning("<ol>" +
            "<li style='margin:10px 0 10px 0;'>Extend the limit for this call " +
            "<input type='text' id='extend_duration' style='min-width: 0; width: 3vw; display: inline'" +
            " class='form-control'> more minutes?</li>" +
            "<li>Pause this session until you return?" +
            "<button type='button' id='confirmationNo' " +
            "class='btn btn-default' style='margin-left: 5px'>Pause</button></li></ol>",
            'You have reached your set call duration limit, do you wish to:',
            {
                timeOut: 0,
                extendedTimeOut: 0,
                preventDuplicates: true,
                tapToDismiss: false,
                closeButton: true,
                allowHtml: true,
                onShown: function (toast) {
                    $("#confirmationYes").click(function () {
                        // Get the ID from the URL
                        var path = window.location.pathname;
                        var components = path.split('/');
                        var id = components[components.length - 1];
                        var lock_period = makeAjaxCall('tgd/lock_period', {id: id, leap: leap, year: tgd_year});

                        lock_period.then(function (data) {

                            // Get the newly inserted TGD ID.
                            const instance_id = data.tgd_data.tgd_instance_id;

                            // Checks if this was the last quarter of the year if 'YES' new TGD instance is opened in a new tab.
                            if (undefined !== instance_id) {
                                var address = jsglobals.base_url + "tgd/tgd/" + instance_id;
                                var newwin = window.open(address, '_blank');

                                // Check if poop ups are allowed or not.
                                newwin ? newwin.focus() : toastr.error('Please allow popups for this website');
                            } else {
                                refreshPage($('.tsl-tabs li.active').attr('data-type'))
                            }

                            // Close the Toastr.
                            $('.toast-close-button').trigger('click');
                        })
                    });

                    $('#confirmationNo').click(function () {
                        $('.toast-close-button').trigger('click');
                    })
                }
            });

    }, timeout_duration);

}

function updateClientInfo(id, name, value, table) {
    $.ajax({
        url: jsglobals.base_url + 'tsl/update_client_info',
        dataType: 'json',
        data: {
            id: id,
            col: name,
            val: value,
            table: table,
        },
        type: 'post',
        error: ajaxError
    }).done(function (data) {

        if (data.status !== 'success') {
            toastr.error(data.message);
            return;
        }
        else if (data.status === 'redirect') {
            window.location.href = data.redirect;
            return;
        }

        (('tsl_client' === table) || ('tsl_about_client' === table)) ? $('.tsl-tabs li.active').trigger('click') : '';

    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
    });
}

function fetch_call_status() {
    const fetch_call_status = makeAjaxCall('tsl/get_call_status', {});

    fetch_call_status.then(function (data) {
        call_status = data.call_status;
    })
}

// Enable/Disable extra rows.
function enable_show() {

    const tsl_show_more_selector = $('.tsl_show_more');
    let show_less = false;
    // Click Handler for show more button.
    tsl_show_more_selector.unbind('click');
    tsl_show_more_selector.click(function (e) {

        // Add on click event to collapse button.
        if (e.isTrigger === undefined) {
            let obj = {};
            const obj_len = localStorage.tsl_key_count;
            let count = 0;

            // Check if the value for tab is already present in local storage object.
            function hasValue(object, value) {
                let key_to_return = false;
                $.each(object, function (index, key) {
                    if (key == value) {
                        key_to_return = index;
                    }
                });
                return key_to_return;
            }

            // Parse the active tabs and assign to an object.
            if (undefined !== obj_len) {
                obj = JSON.parse(localStorage.active_tsl_levels);

                const key = hasValue(obj, $(this).attr('id'));

                // Check If key is already present in Storage. If yes remove it.
                if (key) {
                    delete obj[key];
                    count = parseInt(obj_len) - 1;
                    $("#tsl-instance-" + $(this).data('level') + "-table tbody").find("tr:gt(4)").remove();

                    $(this).find("i").toggleClass('fa-plus-circle').toggleClass('fa-minus-circle');

                    show_less = true;
                    // Add class to show more rows and remove class to show less.
                    $(this).addClass('tsl_show_more');
                    $(this).removeClass('tsl_show_less');
                } else {
                    obj[obj_len] = $(this).attr('id');
                    count = parseInt(obj_len) + 1;
                }
            } else {
                obj[0] = $(this).attr('id');
                count = 1;
            }

            // Put the active tabs JSON to session storage.
            const data = JSON.stringify(obj);
            localStorage.setItem('active_tsl_levels', data);
            localStorage.setItem('tsl_key_count', count);
        }

        // Check if this is a request to fetch less rows.
        if (show_less === false) {
            const get_more_rows = makeAjaxCall('tsl/get_rows', {
                'level': $(this).data('level'),
                'id': tsl_id,
                'sort_by': $('#sort-by').val()
            });

            get_more_rows.then(function (data) {
                append_rows(data.rows, 5);
            });

            // Add class to show less rows and remove class to show more.
            $(this).addClass('tsl_show_less').removeClass('tsl_show_more');
            $(this).find("i").addClass('fa-minus-circle').removeClass('fa-plus-circle');
        } else {
            // Add class to show more rows and remove class to show less.
            $(this).removeClass('tsl_show_less').addClass('tsl_show_more');
            $(this).find("i").removeClass('fa-minus-circle').addClass('fa-plus-circle');
            enable_show();
        }
    });

    // Get all the active tabs.
    const obj = localStorage.active_tsl_levels !== undefined
        ? JSON.parse(localStorage.active_tsl_levels) : {};

    // Trigger click event on all the active tabs.
    $.each(obj, function (index, key) {
        $('#' + key).trigger('click');
    });
}

// Appends rows to respective level TSL tables.
function append_rows(data, prev_count, buyer_type) {
    $.each(data, function (index, client) {
        let result_options = (buyer_type !== 'buyer-' ?
                '<option value="scheduled" ' + (client.tac_result == 'scheduled' ? 'selected' : '') + '>' +
                'Yes to meeting</option>' +
                '<option value="no"' + (client.tac_result == 'no' ? 'selected' : '') + '>' +
                'No to meeting</option>' +
                '<option value="maybe" ' + (client.tac_result == 'maybe' ? 'selected' : '') + '' +
                '>Maybe to meeting</option>'
                :
                '<option value="b_active" ' + (client.tac_result == 'b_active' ? 'selected' : '') + '>' +
                'Buyer Active</option>' +
                '<option value="b_inactive"' + (client.tac_result == 'b_inactive' ? 'selected' : '') + '>' +
                'Buyer Inactive</option>'
        );

        let background = "",
            button_background = 'rgba(146, 208, 130, 0.25)';
        if (2 == client.tac_sequence) {
            background = "style=\"background:  #ADD8E6;\" class=\"row-disabled\"";
            button_background = '';
        } else if (1 == client.tac_sequence) {
            background = "style=\"background: #FFFFE0;\"";
        } else if (client.tc_phone === 'Number Needed') {
            background = "style=\"background: #e9e3f4;\"";
        } else if (client.tac_result === 'maybe') {
            background = "style=\"background:  #fff1df;\""
        } else if (client.tac_result === 'no') {
            background = "style=\"background:  #fff2b8;\""
        }

        $('#tsl-' + $('.tsl-tabs li.active').attr('data-type') + 'instance-' + client.tac_level + '-table tbody')
            .append($('<tr  ' + background + '>')
                .append($('<td class="text-center">')
                    .append(prev_count + index + 1))

                .append($('<td class="text-center">')
                    .append($('<textarea rows="1" onkeyup="auto_grow(this)"\n' +
                        ' name="name' + client.tc_id + '" class="form-control' +
                        ' date-ref edit-client-info" data-name="hc_name" data-table="homebase_client"' +
                        ' data-id="' + client.hc_id + '" data-buyer-type="' + buyer_type + '">'
                        + client.tc_name +
                        '</textarea>')))

                .append($('<td class="text-center">')
                    .append($('<button class="btn btn-sm btn-default" data-id="' + client.hca_id + '" ' +
                        'data-target="#view-tsl-asset-criteria-dialog" data-toggle="modal"' +
                        ' data-buyer-type="' + buyer_type + '">' +
                        '<span class="glyphicon glyphicon-list"></span></button>')))

                .append($('<td class="text-center">')
                    .append('<input type="text" name="asset_name' + client.tc_id + '" ' +
                        'data-table="homebase_client_asset" data-buyer-type="' + buyer_type + '" ' +
                        'class="form-control date-ref edit-client-info" id="asset_name' + client.tc_id + '"' +
                        ' data-name="hca_name" data-id="' + client.hca_id + '" ' +
                        'value="' + (client.hca_name != null ? client.hca_name : '') + '">'))

                .append($('<td class="text-center" style="width: 12vw">')
                    .append('<div class="input-group">' +
                        '<select class="form-control date-ref edit-client-info phone-field"' +
                        ' style="min-width: 7vw; max-width: none" data-buyer-type="' + buyer_type + '" ' +
                        'data-name="hc_selected_phone" data-table="homebase_client" name="Phone' + client.tc_id + '"' +
                        'id="phone' + client.tc_id + '" data-id="' + client.hc_id + '">' +
                        '<option value="-1" data-id="' + client.tc_id + '">-- Add New Phone --</option>' +
                        '<option data-name="hc_main_phone" data-id="' + client.hc_id + '"  ' +
                        'data-table="homebase_client" value="hc_main_phone">' +
                        'Main: ' + (client.tc_phone != null ? client.tc_phone : '')
                        + '</option>' +
                        '<option data-name="hc_mobile_phone" data-id="' + client.hc_id + '" ' +
                        'data-table="homebase_client" value="hc_mobile_phone">' +
                        'Mobile: ' + (client.hc_mobile_phone != null ? client.hc_mobile_phone : '') + '</option>' +
                        '<option data-name="hc_second_phone" data-id="' + client.hc_id + '"' +
                        ' data-table="homebase_client" value="hc_second_phone"> ' +
                        'Second: ' + (client.hc_second_phone != null ? client.hc_second_phone : '') + '</option>' +
                        ((null != client.additional_phone) ?
                            $.map(client.additional_phone.split(','), function (phone, index) {
                                phone = phone.split('{{').pop().split('}}');
                                return '<option  data-table="homebase_client_phone" data-name="hcp_phone"' +
                                    ' data-id="' + phone[0] + '" value="' + phone[0] + '">' + phone[1] + '</option>';
                            }).join('') : "") +
                        '</select>' +
                        '<span class="input-group-btn">' +
                        '<span class="has-tooltip" title="Mark as Bad Number" >' +
                        '<button class="btn btn-default mark-bad" type="button" data-id="' + client.tc_id + '">' +
                        '<i class="fa fa-chain-broken"></i></button></span></span>' +
                        '</div>'))

                .append($('<td class="text-center">')
                    .append($('<button class="btn btn-default btn-sm component_button" data-owner="' + client.hc_id + '"' +
                        ' data-toggle="modal" title="Client-Asset Information" data-container="body"' +
                        ' data-id="' + data.hca_id + '" data-target="#client-info-dialog"' +
                        ' data-buyer-type="' + buyer_type + '">' +
                        '<span class="glyphicon glyphicon-list-alt"></span></button>')))

                .append($('<td class="text-center">')
                    .append('<select class="form-control date-ref" data-buyer-type="' + buyer_type + '">' +
                        '<option>-- None --</option>' +
                        $.map(products, function (product, index) {
                            return '<option style="font-weight: bold; font-style: italic" ' +
                                (client.hca_product_id == product[0].u_product_group_id
                                    ? 'selected' : '') + '>' + index + '</option>'
                                + $.map(product, function (ind_product, ind_key) {
                                    return '<option ' + (client.hca_product_id == ind_product.u_product_id ?
                                        'selected' : '') + '>' + ind_product.u_product_name + '</option>';
                                })
                        }).join('') +
                        '</select>'))

                .append($('<td class="text-center">')
                    .append('<button type="button" class="btn btn-default btn-sm tsl-dial" ' +
                        'data-level="' + client.tac_level + '" data-tac="' + client.tac_id + '"' +
                        'data-id="' + client.tc_id + '" data-number="' + client.tc_phone + '" ' +
                        ' data-hca="' + client.hca_id + '" data-buyer-type="' + buyer_type + '" ' +
                        ' id="dial' + client.tc_id + '" data-ts-name="ts_dials"' +
                        ' data-sequence="' + client.tac_sequence + '">' +
                        '<span class="glyphicon glyphicon-phone-alt"></span>' +
                        '</button>'))

                .append($('<td class="text-center">')
                    .append($('<input type="radio" name="cnt' + client.tac_id + '" ' +
                        'value="na" class="edit-client-info" data-table="tsl_about_client" ' +
                        'data-name="tac_valid_phone" data-id="' + client.tac_id + '" ' +
                        'data-buyer-type="' + buyer_type + '"'
                        + (client.tac_valid_phone == 'na' ? 'checked' : '') + '>')))

                .append($('<td class="text-center tsl_lm">')
                    .append($('<a href="#">' +
                        '<input type="text" name="lm' + client.tac_id + '" data-buyer-type="' + buyer_type + '"' +
                        'class="edit-client-info form-control" data-table="tsl_about_client" ' +
                        'data-name="tac_left_message" data-id="' + client.tac_id + '" ' +
                        'value="' + (null === client.tms_sequence_number ? 0 : client.tms_sequence_number) + '"' +
                        ' disabled style="width: 2vw; min-width:0"></a>')))

                .append($('<td class="text-center">')
                    .append('<input type="radio" name="cnt' + client.tac_id + '" value="cnt" data-ts-name="ts_contact"  ' +
                        'data-table="tsl_about_client" class="edit-client-info update_ts" ' +
                        'data-name="tac_valid_phone" data-id="' + client.tac_id + '" '
                        + (client.tac_valid_phone == 'cnt' ? 'checked' : '') + ' data-buyer-type="' + buyer_type + '">'))

                .append($('<td class="text-center">')
                    .append('<div class="input-group">' +
                        '<input disabled type="text" name="cbWhen' + client.tac_id + '" data-table="tsl_about_client" ' +
                        'class="form-control tsl-date date-ref edit-client-info" style="background: #fff"' +
                        ' data-name="tac_cb_when" data-id="' + client.tac_id + '" ' +
                        'data-buyer-type="' + buyer_type + '" value="' + (client.tn_next_call != null ?
                            moment(client.tn_next_call).format('DD-MMM-YY, h:mm A') : '') + '">' +
                        '<span class="input-group-btn"><span class="has-tooltip" title="Call Notes Dialog">' +
                        '<button class="btn btn-default btn-sm" data-toggle="modal"' +
                        ' data-target="#view-call-notes-dialog"' +
                        ' data-id="' + client.hca_id + '" >' +
                        '<span class="glyphicon glyphicon-list-alt"></span>' +
                        '</button></span></span>' +
                        '</div>'))

                .append($('<td class="text-center">')
                    .append('<input type="radio" id="tac_cnv' + client.tac_id +
                        '" name="cnt' + client.tac_id + '" ' + 'value="cnv" data-ts-name="ts_conv" ' +
                        'data-table="tsl_about_client" class="edit-client-info update_ts" ' +
                        'data-name="tac_valid_phone" data-id="' + client.tac_id + '"'
                        + (client.tac_valid_phone == 'cnv' ? 'checked' : '') + ' data-buyer-type="' + buyer_type + '">'))

                .append($('<td class="text-center">')
                    .append($('<select class="form-control date-ref edit-client-info update_ts" data-name="tac_result" ' +
                        'data-id="' + client.tac_id + '" id="result' + client.tac_id + '" ' +
                        'data-buyer-type="' + buyer_type + '"' +
                        'data-table="tsl_about_client" data-ts-name="ts_meet">' +
                        '<option disabled selected value="-1">--Result--</option>' +
                        result_options +
                        '</select>')))

                .append($('<td class="text-center">')
                    .append($('<button class="btn btn-default btn-sm has-popover"' +
                        ' data-toggle="popover" title="Next Action Manager" data-container="body"' +
                        ' data-id="' + data.deal_id + '" data-html="true" data-placement="top"' +
                        ' data-content="' + getNotesComponentLayout(client) + '" ' +
                        'data-buyer-type="' + buyer_type + '">' +
                        '<span class="glyphicon glyphicon-info-sign"></span></button>')))

                .append($('<td class="text-center">')
                    .append($('<button class="btn btn-default btn-sm has-popover component_button"' +
                        ' data-toggle="popover" title="Build Components" data-container="body"' +
                        ' data-id="' + data.deal_id + '" data-html="true" data-placement="top"' +
                        ' data-content="' + getInfoComponentLayout(client) + '">' +
                        '<span class="glyphicon glyphicon-info-sign"></span></button>')))

                .append($('<td class="text-center">')
                    .append('<button class="btn btn-primary submit-client-info" type="button" ' +
                        'data-id="' + client.tac_id + '" data-target="#save-client-info-dialog"' +
                        ' data-tc-id="' + client.tc_id + '" data-toggle="modal">' +
                        '<span class="glyphicon glyphicon-ok" style="color: #8ff1fd"></span></button>'))

                .append($('<td class="text-center">')
                    .append($('<button class="btn btn-primary remove-client" type="button" ' +
                        'data-id="' + client.tc_id + '" data-toggle="modal" data-hca="' + client.hca_id + '" ' +
                        'data-target="#remove-tsl-client-dialog" data-name="' + client.tc_name + '" ' +
                        'data-asset="' + client.hca_name + '">' +
                        '<span class="glyphicon glyphicon-remove" style="color: #8ff1fd"></span></button>'
                    )))
            )
        ;

        $('#phone' + client.tc_id).val(client.hc_selected_phone);
    });

    disableRows();
    speechFunctionality();
    usePopover();

    $(document).on('click', '#mco', function (e) {
        var hca_id = $(this).attr('data-hca'),
            deal_id = $(this).attr('data-id');
        e.preventDefault();
        e.stopPropagation();

        $('#mco')
            .popover({
                selector: '[rel=popover]',
                trigger: 'hover',
                container: 'body',
                placement: 'top',
                html: true,
                title: 'Marketing Campaign Overlay.',
                content: function () {
                    return '<div class=\'row col-xs-12\'>\n' +
                        '<table style=\'width:100%\' class=\'table table-striped\'>\n' +
                        '<tr>\n' +
                        '<thead>\n' +
                        '<th>Promos</th>\n' +
                        '<th>Messages</th>\n' +
                        '<th>Scripts</th>\n' +
                        '<th>Confirms<s/th>\n' +
                        '<th>Stories</th>\n' +
                        '</thead>\n' +
                        '</tr>\n' +
                        '<tbody>\n' +
                        '<tr>\n' +
                        $.map(universal_components.components, function (component, index) {
                            let coming_soon_class = '';

                            if ((component.uc_type === 'promo') && (component.uc_tool === 'tsl')) {

                                return ('<td class=\'text-center\'>' +
                                    '<a class=\'btn btn-default btn-sm coming-soon-tool\' ' +
                                    'href=' + jsglobals.base_url + 'tsl' + component.uc_controller
                                    + hca_id + '?id=' + cpd_id + ' id=' + component.uc_name + '>' +
                                    '<span class=\'glyphicon glyphicon-' + component.uc_icon + '\'></span></a>' +
                                    '</td>');
                            }
                        }).join('') +
                        '</tr>\n' +
                        '</tbody>\n' +
                        '</table>\n' +
                        '</div>';
                }
            });
        $(this).popover('show');
    });

    $(document).on('click', '#sent-popover', function (e) {
        var hca_id = $(this).attr('data-hca'),
            deal_id = $(this).attr('data-id');

        e.preventDefault();
        e.stopPropagation();

        $('#sent-popover')
            .popover({
                selector: '[rel=popover]',
                trigger: 'hover',
                container: 'body',
                placement: 'top',
                html: true,
                title: 'Sent Component',
                content: function () {
                    return '<div class=\'row col-xs-12\'>\n' +
                        '<table style=\'width:100%\' class=\'table table-striped\'>\n' +
                        '<tr>\n' +
                        '<thead>\n' +
                        '<th>MCO</th>\n' +
                        '<th>SAP</th>\n' +
                        '</thead>\n' +
                        '</tr>\n' +
                        '<tbody>\n' +
                        '<tr>\n' +
                        '<td><a class=\'btn btn-sm btn-default btn-sm has-popover\' id=\'mco\' ' +
                        'data-id=' + deal_id + ' data-hca=' + hca_id + ' data-toggle=\'popover\'>' +
                        '<span class=\'glyphicon glyphicon-list-alt\'></span>' +
                        '</a></td>' +
                        '<td><a class=\'btn btn-sm btn-default btn-sm has-popover\' id=\'sap\'' +
                        'data-id=' + deal_id + ' data-hca=' + hca_id + ' data-toggle=\'popover\'>' +
                        '<span class=\'glyphicon glyphicon-list-alt\'></span>' +
                        '</a></td></tr></tbody></table></div>'
                }
            });
        $(this).popover('show');
    });

    $(document).on('click', '#sap', function (e) {
        var hca_id = $(this).attr('data-hca'),
            deal_id = $(this).attr('data-id');
        e.preventDefault();
        e.stopPropagation();

        $('#sap')
            .popover({
                selector: '[rel=popover]',
                trigger: 'hover',
                container: 'body',
                placement: 'top',
                html: true,
                title: 'Strategic Advisory Program',
                content: function () {
                    return '<div class=\'row col-xs-12\'>\n' +
                        '<table style=\'width:100%\' class=\'table table-striped\'>\n' +
                        '<tr>\n' +
                        '<thead>\n' +
                        '<th>Profiles</th>\n' +
                        '<th>Worksheets</th>\n' +
                        '<th>Analysis</th>\n' +
                        '<th>Updates</th>\n' +
                        '<th>Notices</th>\n' +
                        '</thead>\n' +
                        '</tr>\n' +
                        '<tbody>\n' +
                        '<tr>\n' +
                        $.map(universal_components.components, function (component, index) {
                            if ((component.uc_type !== 'promo') && (component.uc_tool === 'tsl')) {
                                return ('<td class=\'text-center\'>' +
                                    '<a class=\'btn btn-default btn-sm coming-soon-tool\'  href=' + jsglobals.base_url + 'tsl'
                                    + component.uc_controller
                                    + hca_id + '?id=' + cpd_id + ' id=' + component.uc_name + '>' +
                                    '<span class=\'glyphicon glyphicon-' + component.uc_icon + '\'></span></a>' +
                                    '</td>');
                            }
                        }).join('') +
                        '</tr>\n' +
                        '</tbody>\n' +
                        '</table>\n' +
                        '</div>\n';
                }
            });
        $(this).popover('show');
    });
}

// Disable twice called clients.
function disableRows() {
    // Make the rows children disabled.
    $('.row-disabled').children().each(function (i, d) {
        $(this).children().attr('disabled', 'true');
        $(this).find('input').attr('disabled', 'true');
        $(this).find('.btn').attr('disabled', 'true');
    });

    // Make Delete button active.
    $('.row-disabled').find('.remove-client').attr('disabled', false)

}

function getInfoComponentLayout(client) {
    return ('<div class=\'row col-xs-12\'>\n' +
        '<table style=\'width:100%\' class=\'table table-striped\'>\n' +
        '<tr>\n' +
        '<thead>\n' +
        '<th>MCO</th>\n' +
        '<th>SAP</th>\n' +
        '<th>Sent</th>\n' +
        '</thead>\n' +
        '</tr>\n' +
        '<tbody>\n' +
        '<tr>' +
        '<td>' +
        '<span class=\'has-tooltip\' data-title=\'Marketing Campaign Overlay.\'>' +
        '<a class=\'btn btn-sm btn-default btn-sm has-popover\' id=\'mco\'' +
        'data-id=' + client.hc_id + ' data-hca=' + client.hca_id + ' data-toggle=\'popover\'>' +
        '<span class=\'glyphicon glyphicon-list-alt\'></span>' +
        '</a></span></td>' +
        '<td>' +
        '<span class=\'has-tooltip\' data-title=\'Strategic Advisory Program.\'>' +
        '<a class=\'btn btn-sm btn-default btn-sm has-popover\' id=\'sap\' ' +
        'data-id=' + client.hc_id + ' data-hca=' + client.hca_id + ' data-toggle=\'popover\'>' +
        '<span class=\'glyphicon glyphicon-list-alt\'></span>' +
        '</a></span></td>' +
        '<td>' +
        '<span class=\'has-tooltip\' data-title=\'Sent Components.\'>' +
        '<a class=\'btn btn-sm btn-default btn-sm has-popover\' id=\'sent-popover\' ' +
        'data-id=' + client.hc_id + ' data-hca=' + client.hca_id + ' data-toggle=\'popover\'>' +
        '<span class=\'glyphicon glyphicon-list-alt\'></span>' +
        '</a></span></td>' +
        '</tr></tbody></table></div>');
    useTooltip();
}

function getNotesComponentLayout(client) {
    return ('<div class=\'row col-xs-12\'>\n' +
        '<table style=\'width:100%\' class=\'table table-striped\'>\n' +
        '<tr>\n' +
        '<thead>\n' +
        '<th>Callbacks</th>\n' +
        '<th>Actions</th>\n' +
        '<th>Confirms</th>\n' +
        '</thead>\n' +
        '</tr>\n' +
        '<tbody>\n' +
        '<tr>' +
        '<td>' +
        '<span class=\'has-tooltip\' data-title=\'Callback Popover.\'>' +
        '<button class=\'btn btn-sm btn-default has-popover\' ' +
        'data-id=' + client.hca_id + ' data-target=\'#view-call-notes-dialog\'' +
        ' data-toggle=\'modal\' data-owner=\'' + client.hc_id + '\'>' +
        '<span class=\'glyphicon glyphicon-list-alt\'></span>' +
        '</button></span></td>' +
        '<td>' +
        '<span class=\'has-tooltip\' data-title=\'Next Action.\'>' +
        '<button class=\'btn btn-sm btn-default has-popover\' ' +
        'data-id=' + client.hca_id + ' data-target=\'#view-action-dialog\'' +
        ' data-toggle=\'modal\' data-owner=\'' + client.hc_id + '\'>' +
        '<span class=\'glyphicon glyphicon-list-alt\'></span>' +
        '</button></span></td>' +
        '<td>' +
        '<span class=\'has-tooltip\' data-title=\'Confirms.\'>' +
        '<button class=\'btn btn-sm btn-default has-popover\' ' +
        'data-id=' + client.hca_id + ' data-target=\'#view-confirm-dialog\'' +
        ' data-toggle=\'modal\' data-owner=\'' + client.hc_id + '\'>' +
        '<span class=\'glyphicon glyphicon-list-alt\'></span>' +
        '</button></span></td>' +
        '</tr></tbody></table></div>');
    useTooltip();
}

// Fetches and displays the list of agents.
function refreshAgent(refresh = null) {

    // Display agent.
    const tsl_prepared_by_selector = $('#tsl-prepared-by');
    tsl_prepared_by_selector.empty();
    tsl_prepared_by_selector.attr('data-id', tsl_id);
    tsl_prepared_by_selector.append($('<option disabled selected value="-1"> -- Select Prepared By -- </option>'));
    tsl_prepared_by_selector.append($('<option value="0" id="option1">Add New Name</option>'));

    $('#agent-table tbody').empty();


    // Show the agent if there are any.
    if (tsl_agent == 'undefined') {
        $('#no-agent').show();
        $('#agent-div').hide();
    } else {
        $('#no-agent').hide();
        $('#agent-div').show();

        $.each(tsl_agent, function (index, agent) {

            // Append options to TSL Prepared By Dropdown.
            tsl_prepared_by_selector.append(
                (1 === parseInt(agent.tsl_agent_selected)) ?
                    $('<option value="' + agent.tsl_agent_id + '" selected>'
                        + agent.tsl_agent_name + '</option>')
                    :
                    $('<option value="' + agent.tsl_agent_id + '">'
                        + agent.tsl_agent_name + '</option>')
            );

            // Append Prepared by to table
            $('#agent-table tbody')
                .append($('<tr>')
                    .append($('<td class="text-center">')
                        .append(index + 1))
                    .append($('<td class="text-center">')
                        .append(agent.tsl_agent_name))
                    .append($('<td class="text-center">')
                        .append($('<button class="btn btn-default btn-sm"' +
                            ' data-toggle="modal"' +
                            ' data-target="#delete-agent-dialog"' +
                            ' data-backdrop="static"' +
                            ' data-id="' + agent.tsl_agent_id +
                            '" data-name="' + agent.tsl_agent_name +
                            '"> <span class="glyphicon glyphicon-trash"></span>')))
                );

            $('.agent-category').append(
                $('<option value="' + agent.tcc_id + '">' + agent.tcc_name + '</option>'));
        });
    }
}

// Fetches and displays the list of criteria used.
function refreshCriteria(refresh = null) {

    const tsl_list_criteria_selector = $('#tsl-list-criteria');
    // Truncate criteria before appending.
    tsl_list_criteria_selector.empty();
    tsl_list_criteria_selector.attr('data-id', tsl_id);
    // tsl_list_criteria_selector.append($('<option disabled selected value="-1"> -- Criteria List -- </option>'));

    $.each(tsl_criteria, function (index, criteria) {
        // Append Criteria to TSL dropdown.
        tsl_list_criteria_selector.append(
            $('<option value="' + criteria.uc_id + '">'
                + criteria.uc_name + '</option>')
        );
    });

}

// Fetches and displays list of Audiences.
function refreshAudience() {

    tsl_target_selector.empty();
    // tsl_target_selector.append($('<option disabled selected value="-1"> -- Location List -- </option>'));
    tsl_target_selector.attr('data-id', tsl_id);
    $.each(target_audience, function (index, audience) {
        // Append Criteria to TSL dropdown.
        tsl_target_selector.append(
            $('<option value="' + audience.ul_id + '">'
                + audience.ul_name + '</option>')
        );
    });
    updateAudience();
}

// Displays list of callers.
function updateCaller() {
    const tsl_caller_selector = $('#tsl-caller');

    tsl_caller_selector.empty();
    tsl_caller_selector.append($('<option disabled selected value="-1"> -- Select Caller -- </option>'));
    tsl_caller_selector.append($('<option value="0">Add/Edit Caller</option>'));

    $('#caller-table tbody').empty();

    // If no caller is present, display no caller message.
    if (caller === 'undefined' || caller.length === 0) {
        $('#no-caller').show();
        $('#caller-div').hide();
    } else {
        $('#no-caller').hide();
        $('#caller-div').show();

        $.each(caller, function (index, call) {

            $('#tsl-caller').append($('<option value=" ' + call.tsl_caller_id + ' ">' + call.tsl_caller_name + ' </option>'));
            $('#caller-table tbody')
                .append($('<tr>')
                    .append($('<td class="text-center">')
                        .append(index + 1))
                    .append($('<td class="text-center">')
                        .append(call.tsl_caller_name))
                    .append($('<td class="text-center">')
                        .append($('<button class="btn btn-default btn-sm"' +
                            ' data-toggle="modal"' +
                            ' data-target="#delete-caller-dialog"' +
                            ' data-backdrop="static"' +
                            ' data-id="' + call.tsl_caller_id +
                            '" data-name="' + call.tsl_caller_name +
                            '"> <span class="glyphicon glyphicon-trash"></span>')))
                )
        })
    }

    let selectedItem = localStorage.getItem("SelectedItem");
    tsl_caller_selector.val(selectedItem);

    tsl_caller_selector.change(function () {
        localStorage.setItem("SelectedItem", $(this).val());
    });
}

// Displays list of Assignee.
function updateAssignee() {
    $('#assignee-table tbody').empty();

    // If no assignee is present, display no assignee message.
    if (assignee == 'undefined' || assignee.length == 0) {
        $('#no-assignee').show();
        $('#assignee-div').hide();
    } else {
        $('#no-assignee').hide();
        $('#assignee-div').show();

        $.each(assignee, function (index, agent) {
            $('#assignee-table tbody')
                .append($('<tr>')
                    .append($('<td class="text-center">')
                        .append(index + 1))
                    .append($('<td class="text-center">')
                        .append(agent.tsl_assignee_name))
                    .append($('<td class="text-center">')
                        .append($('<button class="btn btn-default btn-sm"' +
                            ' data-toggle="modal"' +
                            ' data-target="#delete-assignee-dialog"' +
                            ' data-backdrop="static"' +
                            ' data-id="' + agent.tsl_assignee_id +
                            '" data-name="' + agent.tsl_assignee_name +
                            '"> <span class="glyphicon glyphicon-trash"></span>')))
                )
        })
    }
}

// Displays list of target audience.
function updateAudience() {
    $('#location-table tbody').empty();
    tsl_target_selector.empty();

    // tsl_target_selector.append($('<option disabled selected value="-1"> -- Location List -- </option>'));
    // If no assignee is present, display no assignee message.

    const client_audience_selector = $('.client-audience');
    client_audience_selector.empty();
    // client_audience_selector.append($('<option value="-1" disabled selected>-- Select Location --</option>'));

    $.each(target_audience, function (index, audience) {
        // Append Criteria to TSL dropdown.
        tsl_target_selector.append(
            $('<option value="' + audience.ul_id + '">'
                + audience.ul_name + '</option>')
        );
    })
}
