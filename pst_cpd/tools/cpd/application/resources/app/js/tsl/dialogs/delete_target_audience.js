$(document).ready(function () {
    let target_audience_id = '',
        ui_name = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-target-audience-dialog').on('show.bs.modal', function (event) {
        target_audience_id = $(event.relatedTarget).data('id');
        ui_name = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name')

        $('#del-target-audience-name').html(ui_name);
        $('#view-target-audience-dialog').modal('hide');
    });

    $(document).on('click', '#close-delete-target-audience-dialog', function () {
        $('#delete-target-audience-dialog').modal('hide');
    });

    // Deletes the target-audience name.
    $(document).on('submit', '#delete-target-audience-form', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_target_audience',
            dataType: 'json',
            type: 'post',
            data: {
                target_audience_id: target_audience_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            toastr.success('Location Instance Deleted', 'Success');
            $('#delete-target-audience-dialog').modal('hide');
            get_all_criteria();
            refreshPage();

        }).fail(function (jqXHR, textStatus) {
            toastr.error('You have Assets belonging to this Location.', 'Error!!');
        }).always(function () {
        });
        return false;
    });
});
