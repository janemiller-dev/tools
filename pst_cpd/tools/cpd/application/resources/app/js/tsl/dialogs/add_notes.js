$(document).ready(function () {
    $('#add-notes-dialog').on('show.bs.modal', function (event) {
        $('#view-notes-dialog').modal('hide');
    });

    // Form validation.
    $('#add-notes-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            tsl_note: {
                validators: {
                    notEmpty: {
                        message: "Note is required."
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the add new caller form.
        var $form = $(e.target);
        var fv = $form.data('formValidation');

        // Get the form data and submit it.
        var data = $form.serializeArray();
        data.push({name: 'tsl_id', value: tsl_id})

        $.ajax({
            url: jsglobals.base_url + 'tsl/add_user_notes',
            dataType: 'json',
            type: 'post',
            data: data,
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            } else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('Note Added!!', 'Success!!')
            fv.resetForm();
            $($form)[0].reset();

            $('#add-notes-dialog').modal('hide');
            refreshDashboard();

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });
});



// else if ($(this).attr('data-name') == 'tsl_notes') {
//
//     $.ajax({
//         url: jsglobals.base_url + 'tsl/add_user_notes',
//         dataType: 'json',
//         data: {
//             val: value,
//         },
//         type: 'post',
//         error: ajaxError
//     }).done(function (data) {
//
//         if (data.status != 'success') {
//             toastr.error(data.message);
//             return;
//         }
//         else if (data.status == 'redirect') {
//             window.location.href = data.redirect;
//             return;
//         }
//
//         $(".notes_select").append('<option value="' + data.note_added + '" class="' + data.note_added + '">' + value + '</option>');
//         $(".notes_select").selectpicker("refresh");
//         $("option" + "." + data.note_added).hide();
//
//     }).fail(function (jqXHR, textStatus) {
//         toastr.error("Request failed: " + textStatus);
//     }).always(function () {
//     });
//
//     return;
// }