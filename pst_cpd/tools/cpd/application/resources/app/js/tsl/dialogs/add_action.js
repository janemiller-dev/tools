$(document).ready(function () {
    $('#add-action-dialog').on('show.bs.modal', function (event) {
        $('#view-action-dialog').modal('hide');
        $('#tsl_id').val(tsl_id);
    });

    // Form validation.
    $('#add-action-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            action_text: {
                validators: {
                    notEmpty: {
                        message: "The Action Text is required."
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        // Get the add new action form.
        var $form = $(e.target);
        var fv = $form.data('formValidation');

        // Get the form data and submit it.
        var action_data = $form.serialize();

        $.ajax({
            url: jsglobals.base_url + 'tsl/add_action',
            dataType: 'json',
            type: 'post',
            data: action_data,
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            } else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('Action Added!!', 'Success!!')
            fv.resetForm();
            $($form)[0].reset();

            $('#add-action-dialog, #view-action-dialog').modal('hide');
            refreshDashboard();

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });
});