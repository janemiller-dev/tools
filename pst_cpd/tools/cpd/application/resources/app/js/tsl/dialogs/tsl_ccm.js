/**
 *
 * View DMD ccm.
 *
 * @summary      Dialog used to display Notes for DMD clients.
 * @description  This file contains functions for Displaying DMD client Notes..
 * @author       Sumit K <sumitk@mindfiresolutions.com>
 *
 */

$(document).ready(function () {

    $('#view-ccm-dialog').on('show.bs.modal', function (event) {
        $('.start-end-call').attr('checked', false);
        viewCCM.init(event);
    });
});

/**
 *
 * View Notes available.
 */

let viewCCM = function () {

    let max_seq = 0,
        deal_id, tc_id, tac;

    // Initialize and get the DMD ID.
    let init = function (event) {

        $('#ccm-date-picker').val(moment().format("YYYY-MM-DD"));

        $('#ccm-date-picker').datetimepicker({
            timepicker: false,
            format: 'YYYY-MM-DD',
            onSelectTime: function (dp, $input) {
                console.log($(this).val());
            }
        });

        deal_id = $(event.relatedTarget).attr('data-id');
        tc_id = $(event.relatedTarget).attr('data-id'),
            tac = $(event.relatedTarget).attr('data-tac');

        $('.ccm-select').attr('data-id', deal_id);

        // Check if the request is from DMD/TSL.
        if ('cpdv1' === window.location.pathname.split('/')[2]) {
            $('.tsl-ccm-view').css('visibility', 'hidden')
        } else {
            $('.tsl-ccm-view').css('visibility', 'visible');
            deal_id = $(event.relatedTarget).data('hca');
            $('#ccm-na').prop('checked', false);
            $('#ccm-na').attr('data-id', tac);
        }

        const get_message_script = makeAjaxCall('cpdv1/get_message_script');

        // Append Message to message drop down.
        get_message_script.then(function (data) {
            $.each(data.message_script.message, function (index, message) {
                $('#ccm-message').append('<optgroup label="' + message[0].tm_name + '">');

                $.each(message, function (key, ind_message) {
                    $('#ccm-message').append('<option value="' + ind_message.tms_id + '">'
                        + ind_message.tms_name + '</option>')
                });

                $('#ccm-message').append('</optgroup>');
            });

            // Append Scripts to script drop down.
            $.each(data.message_script.script, function (index, script) {
                $('#ccm-script').append('<optgroup label="' + script[0].tsl_script_type + '">');

                $.each(script, function (key, ind_script) {
                    $('#ccm-script').append('<option value="' + ind_script.ts_id + '">'
                        + ind_script.tsn_name + '</option>')
                });

                $('#ccm-script').append('</optgroup>');
            });
        });

        // Show prompter click handler.
        $(document).off('click', '.show-prompter');
        $(document).on('click', '.show-prompter', function () {
            // $('#view-ccm-dialog').modal('hide');

            $('#credits').html('');

            const id = $(this).parent().siblings('select').val(),
                type = $(this).parent().siblings('select').attr('data-type'),
                deal_id = $(this).parent().siblings('select').attr('data-id');

            const get_message_script_content = makeAjaxCall('cpdv1/get_message_script_content', {
                'id': id,
                'type': type,
                'deal_id': deal_id
            });

            get_message_script_content.then(function (data) {
                let content = '',
                    outline = data.content[0].outline,
                    outline_array = outline.match(/({{(.*?)}})|(\(\((.*?)\)\))/g);

                $.each(data['content'], function (index, ret_content) {
                    content += '<h4>' + outline_array[index] + '</h4>';
                    content += '<p>' + ret_content.content + '</p>';
                });

                if (null !== content) {
                    $('#credits').html('<p>' + content + '</p>');
                }

                $('#view-reader-dialog').modal('show');
                $('#view-reader-dialog .modal-dialog').removeClass('modal-lg');
                $('#view-reader-dialog').removeClass('modal-lg');
                $('#view-reader-dialog').removeClass('modal-lg');
            });

        });

        const get_call_info = makeAjaxCall('cpdv1/get_call_info', {
                id: deal_id
            }),
            call_purpose_selector = $('#call-purpose'),
            start_end_selector = $('.start-end-call'),
            start_selector = $('#ccm-start-time'),
            duration_selector = $('#ccm-duration'),
            update_scoreboard_selector = $('.update-scoreboard');

        get_call_info.then(function (data) {

            call_purpose_selector.val('');
            start_end_selector.eq(1).removeAttr('checked');
            start_selector.val('');
            $('#ccm-end-time').val('');
            duration_selector.val('');

            if (data.call_info.length !== 0) {
                $('#ccm-start-time').val(data.call_info[0].ccm_start);

                const duration = moment.utc(moment.utc(moment().format("hh:mm A"), "HH:mm") -
                    moment.utc(data.call_info[0].ccm_start, "HH:mm")).format('H:mm');

                $('#ccm-duration').val(duration);
                start_end_selector.eq(0).prop('checked', true);
                start_end_selector.eq(1).attr('data-ccm', data.call_info[0].ccm_id);
                call_purpose_selector.attr('data-ccm', data.call_info[0].ccm_id);
                call_purpose_selector.val(data.call_info[0].ccm_purpose);
            }

            const get_call_scoreboard = makeAjaxCall('tsl/get_call_scoreboard', {
                when: $('#ccm-date-picker').val()
            });

            get_call_scoreboard.then(function (data) {
                $.each(data.call_scoreboard, function (index, score) {
                    if ('dial' === score.ucs_type) {
                        $('.scoreboard-span').eq(index)
                            .css('background-image', 'linear-gradient(to bottom right,' +
                                '  transparent calc(50% - 1px), #2F4F4F, transparent calc(50% + 1px))');
                    } else if ('contact' === score.ucs_type) {
                        $('.scoreboard-span').eq(index)
                            .css('background-image', 'linear-gradient(to bottom right,' +
                                '  transparent calc(50% - 1px), #2F4F4F, transparent calc(50% + 1px)), ' +
                                'linear-gradient(to bottom left,' +
                                '  transparent calc(50% - 1px), #2F4F4F, transparent calc(50% + 1px))');
                    } else if ('meeting' === score.ucs_type) {
                        $('.scoreboard-span').eq(index).html('<b style="color: #fff; font-size: 2em">M</b>')
                        $('.scoreboard-span').eq(index).css('background', '#696969');
                    } else if ('message' === score.ucs_type) {
                        $('.scoreboard-span').eq(index).html('<b style="color: #fff; font-size: 2em">LM</b>')
                        $('.scoreboard-span').eq(index).css('background', '#696969');
                    } else if ('conv' === score.ucs_type) {
                        $('.scoreboard-span').eq(index).html('<b style="color: #fff; font-size: 2em">C</b>')
                        $('.scoreboard-span').eq(index).css('background', '#696969');
                    }
                })
            })
        });

        // Start End Call handler.
        start_end_selector.off('change');
        start_end_selector.change(function () {
            const type = $(this).attr('data-type'),
                ccm_id = $(this).attr('data-ccm');

            (type === 'end') ? $('#ccm-end-time').val(moment().format("hh:mm A")) :
                start_selector.val(moment().format("hh:mm A"));

            // Ajax call to start end call.
            const start_call = makeAjaxCall('cpdv1/start_end_call', {
                type: type,
                deal_id: deal_id,
                date: moment().format("DD-MM-YYYY"),
                time: moment().format("hh:mm:ss A"),
                ccm: ccm_id
            });

            start_call.then(function (data) {
                call_purpose_selector.attr('data-ccm', data.call_started_ended);
                start_end_selector.attr('data-ccm', data.call_started_ended);
                toastr.success('Call ' + type + 'ed!!', 'Success!!');
            })
        });

        update_scoreboard_selector.off('change');
        update_scoreboard_selector.on('change', function () {
            const type = $(this).attr('data-type'),

                update_scoreboard = makeAjaxCall('tsl/update_scoreboard', {
                    type: type
                });

            update_scoreboard.then(function (data) {
                console.log(data);
            })
        });

        // Call Purpose change handler.
        call_purpose_selector.off('change');
        call_purpose_selector.change(function () {
            const ccm_id = $(this).attr('data-ccm'),
                val = $(this).val();

            // Update purpose for call.
            makeAjaxCall('cpdv1/update_call_purpose', {
                ccm: ccm_id,
                val: val
            });
        });

        // Mark Bad click handler.
        $('.ccm-mark-bad').off('change');
        $('.ccm-mark-bad').change(function () {
            $('.mark-bad[data-id="' + tc_id + '"]').trigger('click');
        });
    };

    return {
        init: init,
    };
}();