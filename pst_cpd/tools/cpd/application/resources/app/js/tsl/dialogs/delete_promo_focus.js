$(document).ready(function () {
    let promo_focus_id = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-promo-focus-dialog').on('show.bs.modal', function (event) {
        $('#view-promo-focus-dialog').modal('hide');

        // Fetch the Data attributes.
        promo_focus_name = $(event.relatedTarget).data('name');
        promo_focus_id = $(event.relatedTarget).data('id');

        $('#delete-promo-focus-name').text(promo_focus_name);

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name');

    });

    // Delete Promotional Focus.
    $(document).on('submit', '#delete-promo-focus-form', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_promo_focus',
            dataType: 'json',
            type: 'post',
            data: {
                promo_focus_id: promo_focus_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('Survey Outline Deleted.', 'Success!!');
            $('#delete-promo-focus-dialog').modal('hide');

            // Reload page content
            if ('tsl' === origin) {
                refreshTSLdropdown().then(function () {
                    refreshDMDDropdown().then(function (data) {
                        refreshPage();
                    })
                });
            } else {
                refreshDMDDropdown().then(function (data) {
                    refreshPage();
                });
            }

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
        return false;
    });
});
