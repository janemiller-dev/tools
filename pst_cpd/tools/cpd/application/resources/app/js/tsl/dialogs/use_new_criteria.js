$(document).ready(function () {
    $('#use-new-criteria-dialog').on('show.bs.modal', function (event) {
        $('#client-criteria-dialog').modal('hide');

        $(event.relatedTarget).removeData('id');
        var client_id = $(event.relatedTarget).data('id');
        $('#client-id').val(client_id);

        $('#use-new-criteria').empty();

        // Populate list of criteria.
        $.each(tsl_criteria, function (index, criterion) {
            $('#use-new-criteria').append(
                $('<option value="' + criterion.tsl_criteria_id + '">' + criterion.tsl_criteria_name + '</option>'));
        })


        // Fetch the list of already used criteria for a client.
        $.ajax({
            url: jsglobals.base_url + 'tsl/get_used_criteria',
            dataType: 'json',
            data: {
                client_id: client_id,
            },
            type: 'post',
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            // Remove the already sued criteria.
            $.each(data.used_criteria, function (index, used_criterion) {
                $("#use-new-criteria").find("option[value=" + used_criterion.tccrit_criteria_id + "]").remove();
            })

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });

    // Validate use new criteria form and submit it if it is valid.
    $('#use-criteria-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            use_new_criteria: {
                validators: {
                    notZero: {
                        message: "Criteria is required."
                    }
                }
            },
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();

        var $form = $(e.target);
        var fv = $form.data('formValidation');

        if (null !== $('#use-new-criteria').val()) {
            // Get the form data and submit it.
            var criteria_data = $form.serialize();
            $.ajax({
                url: jsglobals.base_url + 'tsl/use_new_criteria',
                dataType: 'json',
                type: 'post',
                data: criteria_data,
                error: ajaxError
            }).done(function (data) {

                if (data.status != 'success') {
                    toastr.error(data.message, 'Error!!');
                    return;
                }
                else if (data.status == 'redirect') {
                    window.location.href = data.redirect;
                    return;
                }

                toastr.success('New Client Criteria Added.', 'Success!!')
                // Clear the validation rule and form data.
                fv.resetForm();
                $($form)[0].reset();
                $('#use-new-criteria-dialog').modal('hide');

            }).fail(function (jqXHR, textStatus) {
                toastr.error("Request failed: " + textStatus);
            }).always(function () {
            });
        } else {
            fv.resetForm();
            $($form)[0].reset();
            toastr.error('To create one use Add new Criteria button.', 'No Unused Criteria Present!!');
        }
    });
});

