$(document).ready(function () {
    var outline_id = '';
    var version = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-update-outline-dialog').on('show.bs.modal', function (event) {
        outline_id = $(event.relatedTarget).data('id');
        version = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name')

        $('#outline-id').val(outline_id);
        $('#delete-update-outline-version').html('Outline V' + version);
        $('#view-outline-dialog').modal('hide');
    });

    // Deletes Update Outline.
    $(document).on('submit', '#delete-update-outline-form', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_update_outline',
            dataType: 'json',
            type: 'post',
            data: {
                outline_id: outline_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('Update Outline Deleted.', 'Success!!');
            $('#delete-update-outline-dialog').modal('hide');
            refreshUpdatePage();

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
        return false;
    });
});
