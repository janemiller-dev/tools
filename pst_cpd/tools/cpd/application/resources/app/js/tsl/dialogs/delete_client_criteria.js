$(document).ready(function () {
    var criteria_id = '';
    var ui_name = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-client-criteria-dialog').on('show.bs.modal', function (event) {
        criteria_id = $(event.relatedTarget).data('id');
        ui_name = $(event.relatedTarget).data('name');
        $('#criteria-id').val(criteria_id);
        $('#delete-client-criteria-name').html(ui_name);
        $('#client-criteria-dialog').modal('hide');
    });

    // Deletes the criteria for a client.
    $(document).on('click', '#delete-client-criteria-button', function (e) {
        e.preventDefault();
        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_client_criteria',
            dataType: 'json',
            type: 'post',
            data: {
                criteria_id: criteria_id
            },
            error: ajaxError
        }).done(function (data) {
            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            toastr.success('Criteria Deleted', 'Success!!')
            $('#delete-client-criteria-dialog').modal('hide');

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });
});
