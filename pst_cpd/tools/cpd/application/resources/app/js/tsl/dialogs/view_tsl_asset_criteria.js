$(document).ready(function () {
    // Fetches the performance score.
    $('#view-tsl-asset-criteria-dialog').on('show.bs.modal', function (event) {
        view_tsl_asset_criteria.init(event);
    });
});

let view_tsl_asset_criteria = function () {

    // Initialize and get the DMD ID.
    let init = (event) => {
        const asset_id = $(event.relatedTarget).attr('data-id');
        // $(event.relatedTarget).removeAttr('data-id');

        const get_asset_criteria = makeAjaxCall('tsl/get_asset_criteria',
            {'asset_id': asset_id}
        );

        get_asset_criteria.then(function (data) {
            data = data.asset_criteria;

            $('#asset-criteria-table tbody').empty();
            $('#asset-criteria-table tbody').append('<tr>' +
                '<td>' + (0 !== data.location_criteria.length ? data.location_criteria[0].ul_name : '') + '</td>' +
                '<td>' + (0 !== data.city.length ? 'City: ' + data.city[0].hai_value : '') +
                (0 !== data.state.length ? ', State: ' + data.state[0].hai_value : '') +
                (0 !== data.zip.length ? ', Zip: ' + data.zip[0].hai_value : '') + '</td>' +
                '<td>' + (0 !== data.desc.length ? data.desc[0].hai_value : '') + '</td>' +
                '<td>' + (0 !== data.cond.length ? data.cond[0].hai_value : '') + '</td>' +
                '<td>' + (0 !== data.location_criteria.length ? data.location_criteria[0].uc_name : '') + '</td>' +
                '</tr>')
        })

    };
    return {
        init: init
    };
}();