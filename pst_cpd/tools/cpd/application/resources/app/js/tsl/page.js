let activeOutlineId, activePromoId, outline_added, contentId, promo_added, client_id, obj, ref, promo_obj, pdf_added,
    req_type, activeId, cpd_id;

$(document).ready(function () {

    // Get the Client ID from the URL
    const path = window.location.pathname,
        components = path.split('/');

    client_id = components[components.length - 1];
    origin = components[2];
    ref = window.location.search.split('?ref=')[1];
    req_type = components[3];

    getComponents();

    // Check if ref is defined or not.
    if (undefined !== ref) {
        const ref_array = ref.split('?');
        ref = ref_array[0];
    }

    loadPrev();
    activePromoId = -1;
    // Display outline for the promo.
    $('#outline-container').html('');
    $('#canvas-container').html('');

    // To check if a new promo is added.
    outline_added = 0;
    activeOutlineId = 0;

    // List of Buyer's Survey
    const container_listing_selector = $('#container-listing');
    container_listing_selector.html('');
    container_listing_selector.html(
        ' <div class="promo_sidebar"><h4 class="main-con-header">Promo Versions</h4>' +
        ' <ul class="nav" id="tsl-promo-nav">\n' +
        ' </ul></div>');

    container_listing_selector.append($('<div class="promo_sidebar"><h4 class="main-con-header">PDF Versions</h4>' +
        '<ul class="nav" id="tsl-pdf-nav"></ul></div>'));

    $('#page-header').html('');
    if ('tsl' === origin) {
        $('#page-header').html(
            '<div class="col-md-3 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Promo Type</label>' +
            '<select class="form-control page-menu preselect" id="promo-type">' +
            '<option value="nf">News Flash</option>' +
            '<option value="ue">Upcoming Event</option>' +
            // '<option value="fc">Future Change</option>' +
            // '<option value="nfc">Negative Future Change</option>' +
            '<option value="ni">New Idea</option>' +
            '<option value="no">New Offer</option>' +
            '<option value="no">Policy Change</option>' +
            '</select>' +
            '</div>' +
            // '<div class="col-xs-15 script-box">' +
            // '<label class="col-xs-12" style="text-align: center">Promo Topic</label>' +
            // '<select class="form-control page-menu preselect topic" id="promo-topic">' +
            // '<option value="mei">Macro Economic Indicators</option>' +
            // '<option value="lmd">Local Market Developments</option>' +
            // '<option value="cwr">Catastrophic Weather Recovery</option>' +
            // '<option value="led">Looming Economic Downturn</option>' +
            // '<option value="sdc">Sudden Demographic Changes</option>' +
            // '</select>' +
            // '</div>' +
            '<div class="col-md-3 script-box" id="promo-focus-parent" data-toggle="popover"' +
            'data-placement="bottom" data-html="true">' +
            '<label class="col-xs-12 text-center">Promo Delivery</label>' +
            '<select class="form-control page-menu preselect" id="promo-delivery">' +
            '<option value="em">Email</option>' +
            '<option value="sm">Snail Mail</option>' +
            '<option value="eh">Event Handout</option>' +
            '<option value="lb">Leave Behind</option>' +
            '<option value="wd">Web Download</option>' +
            '</select>' +
            '</div>' +
            '<div class="col-md-3 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Promo Format</label>' +
            '<select class="form-control page-menu" id="promo-format">' +
            '<option value="fp">Full Page</option>\n' +
            '<option value="hp">Half Page</option>\n' +
            '<option value="po">Postcard</option>\n' +
            '<option value="eb">Electronic Banner</option>\n' +
            '<option value="ea">Embed or Attachment</option>\n' +
            '</select>' +
            '</div>' +
            '<div class="col-md-3 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Promo Series</label>' +
            '<select class="form-control" id="promo-section" >' +
            '<option value="ps">Promo Series 1</option>' +
            '<option value="cs">Promo Series 2</option>' +
            '<option value="fge">Promo Series 3</option>' +
            '<option value="fs">Promo Series 4</option>' +
            '<option value="ess">Promo Series 5</option>' +
            '</select>' +
            '</div>'           // '<div class="col-xs-15 script-box">' +
            // '<label class="col-xs-12" style="text-align: center">Promo Outline</label>' +
            // '<select class="form-control" id="promo-outline">' +
            // '</select>' +
            // '</div>'
        );

    } else {
        $('#page-header').html('<div class="col-xs-3 col-md-3 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Client Type:</label>' +
            '<select class="form-control page-menu" id="client-type">' +
            '<option value="r">Radish</option>\n' +
            '<option value="w">Wheat</option>\n' +
            '<option value="t">Tree</option>\n' +
            '</select>\n' +
            '</div>\n' +
            '<div class="col-xs-3 script-box col-md-3 ">\n' +
            '<label class="col-xs-12" style="text-align: center">Promo Focus:</label>\n' +
            '<select class="form-control page-menu" id="promo-focus">\n' +
            '<option value="rs">Recent Success</option>\n' +
            '<option value="ie">Industry Event</option>\n' +
            '<option value="le">Local Event</option>\n' +
            '<option value="cc">Client Challenge</option>\n' +
            '<option value="ni">New Information</option>\n' +
            '<option value="mc">Major Change</option>\n' +
            '</select>\n' +
            '</div>\n' +
            '<div class="col-xs-3 script-box col-md-3 ">\n' +
            '<label class="col-xs-12" style="text-align: center">Page Format:</label>\n' +
            '<select class="form-control page-menu" id="promo-format">\n' +
            '<option value="p">Page</option>\n' +
            '<option value="lp">Long Page</option>\n' +
            '<option value="wp">Wide Page</option>\n' +
            '<option value="hp">Half Page</option>\n' +
            '</select>\n' +
            '</div>\n' +
            '<div class="col-xs-3 script-box col-md-3 ">\n' +
            '<label class="col-xs-12" style="text-align: center">Background Template:</label>\n' +
            '<select class="form-control" id="background-template">\n' +
            '</select>\n' +
            '</div>');
    }

    // $(document).on('focus', ".when-date-picker", function () {
    //     $(this).datetimepicker({
    //         format: 'YYYY-MM-DD HH:mm:ss',
    //         onSelectTime: function (dp, $input) {
    //         }
    //     });
    // });
    refreshPage();


    // Check if request is from TSL page.
    // if ('tsl' === origin) {
    //     refreshTSLdropdown().then(function () {
    //         refreshDMDDropdown().then(function (data) {
    //             refreshPage();
    //         })
    //     });
    // } else {
    //     refreshDMDDropdown().then(function (data) {
    //         refreshPage();
    //     })
    // }

    // Document upload button.
    $('.upload-promo').click(function () {
        $('#fileupload').trigger('click');
        $('#message').html('');
        return false;
    });

    $('#send-promo').attr('data-origin', origin);

    // Saves the outline and generates questions from outlines.
    $('#save-promo-outline').click(function (e) {
        $('#promo_format').val($('#promo-format').val());
        $('#client_type').val((undefined !== $('#client-type').val()) ? $('#client-type').val() : '');

        // Check if TA is present.
        if (null === $('#target-audience').val()) {
            toastr.error('No Location Selected!!');
            return;
        }
        else
            $('#target_audience').val((undefined !== $('#target-audience').val()) ? $('#target-audience').val() : 0);

        // Check if criteria is present.
        if (null === $('#list-criteria').val()) {
            toastr.error('No List Criteria Selected!!');
            return;
        }
        else
            $('#criteria').val((undefined !== $('#list-criteria').val()) ? $('#list-criteria').val() : 0);

        // Check if focus is present.
        if (null === $('#promo-focus').val()) {
            toastr.error('No Promotional Focus Selected!!');
            return;
        } else
            $('#promo_focus').val($('#promo-focus').val());


        outline_added = 1;
        activeOutlineId = 0;
        var tsl_update_data = $('#outline-form').serialize();
        e.preventDefault();

        $.ajax({
            url: jsglobals.base_url + 'tsl/save_promo_outline',
            dataType: 'json',
            type: 'post',
            data: tsl_update_data,
            error: ajaxError
        }).done(function (data) {
            if (data.status !== 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            toastr.success('Outline Saved!');
            refreshPage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });


    // Check if any drop down menu is changed, fetch outline version again.
    $('.page-menu').change(function () {
        if ('0' !== $(this).val()) {
            $(this).attr('data-prev', $(this).val());
            $('#canvas-container').html('');
            activeOutlineId = 0;
            activePromoId = 0;
            refreshPage();
        }
    });


    // Checks if no instance of promo is present.
    $('#send-promo, #delete-promo, #view-promo-pdf').click(function () {
        if ($(this).attr('data-toggle') === undefined) {
            toastr.error('To create one use compose button.', 'No Instance of promo.');
        }
    });

    // Collaboration View Click handler.
    $(document).on('click', '#promo-collab', function () {
        $('.promo-container').toggleClass('col-xs-60 col-md-12 full-screen');
        $('#collab-button-row').toggleClass('col-xs-60 col-md-12');
        $('.full-view-hide').toggle();

        if (0 === $('.add_collab').length) {
            activeId = activePromoId;

            $('.promo-row').siblings('span.tts')
                .before('<span class="input-group-addon btn btn-secondary add_collab">' +
                    '<i class="fa fa-plus"></i></span>')

            $('#promo-collab').html('Composition  <i class="fa fa-expand"></i>');

            setupCollab();
        } else {
            $('#promo-collab').html('Collaborate <i class="fa fa-expand"></i>');

            $('.add_collab').remove();
            $('.collab_row').remove();
            $('.collaborator_row').remove();
        }
    });

    // Edit Outline click handler.
    $('#edit-outline').click(function () {
        $('trix-editor').attr('contenteditable', true);
    });

    // Clear button click handler.
    $(document).off('click', '#clear-promo');
    $(document).on('click', '#clear-promo', function () {
        $('#compose-promo-form textarea').val('');
    });
});

// Fetches questions and puts them in a form.
function setupPromo(obj) {
    promo_obj = obj;

    // Disable trix editor.
    $('trix-editor').attr('contenteditable', false);

    // Remove Extra Padding from outline container.
    const outline_container_first_heading_selector = $('#outline-container').find('h1:first');
    outline_container_first_heading_selector.html($.trim(outline_container_first_heading_selector.text()));

    activeOutlineId = $(obj).attr('data-id');
    $version = $(obj).attr('data-version');

    // Jquery file upload.
    $('#fileupload').fileupload({
        dataType: 'json',
        formData: {'outline_id': activeOutlineId},
        done: function (e, data) {
            if (data.result.status !== "success") {
                toastr.error(data.result.message);
                return;
            }
            $.each(data.result.files, function (index, file) {
                $('<p/>').text(file.name).appendTo(document.body);
            });
            toastr.success('Document uploaded successfully...');
            promo_added = 1;
            refreshPage();
        }
    });

    trixButtons();
    // $('#recompose-promo').attr('data-type', 'recompose');

    // ID selectors
    // const compose_promo_selector = $('#compose-promo, #recompose-promo'),
    const compose_promo_selector = $('#compose-promo'),
        compose_promo_title_selector = $('#compose-promo-title'),
        compose_promo_form_selector = $('#compose-promo-form'),
        save_promo_selector = $('#save-promo'),
        promo_type_selector = $('#promo_type');

    // Set the content for Promo Form.
    compose_promo_selector.unbind('click');
    compose_promo_selector.click(function () {
        const add_promo = makeAjaxCall('tsl/add_promo_instance', {
            outline_id: activeOutlineId,
            name: 'Custom Promo',
        });

        add_promo.then(function (data) {
            promo_added = 1;
            refreshPage();
        })
    });
    $('.override').remove();
    if (0 !== activeOutlineId) {

        // Set data attribute for compose promo.
        compose_promo_title_selector.text('Compose Promo');

        // Put text area and questions to compose promo form.
        compose_promo_form_selector.empty();

        compose_promo_form_selector.append($(obj)
            .attr('data-content')
            .replace(/{{/g, '<label style="padding-top: 10px;">').replace(/}}/g, '</label>\n' +
                '<div class="input-group">' +
                '<span class="input-group-addon btn btn-secondary tts"><i class="fa fa-bullhorn"></i></span>' +
                '<textarea class="form-control promo-row" rows="2" cols="70" onkeyup="auto_grow(this)"' +
                'id="ans' + Date.now() + '" ' +
                'placeholder="Click Microphone icon and speak or start typing."></textarea>' +
                '<span class="input-group-addon btn btn-secondary stt">' +
                '<i class="fa fa-microphone"></i></span>' +
                '<span id=\'span_ans\' class=\'hidden promo_ans\' name="ans"></span></div>')

            // <h4 class="main-con-header"></h4>
            .replace(/\[\[/g, '<h2><label style="padding-top: 10px;">')
            .replace(/\]\]/g, '</h2></label>\n' +
                '<div class="input-group">' +
                '<span class="input-group-addon btn btn-secondary tts"><i class="fa fa-bullhorn"></i></span>' +
                '<textarea class="form-control promo-row" id="ans' + Date.now() + '"' +
                ' placeholder="Click Microphone icon and speak or start typing." onkeyup="auto_grow(this)"' +
                ' rows="2" cols="70"></textarea>' +
                '<span class="input-group-addon btn btn-secondary stt">' +
                '<i class="fa fa-microphone"></i></span>' +
                '<h2><span id=\'span_ans\' class=\'hidden promo_ans\' name="ans"></span></h2></div>')

            .replace(/\(\(/g, '<label style="margin-left: 30px; padding-top: 10px;">')
            .replace(/\)\)/g, '</label>\n' +
                '<div class="input-group" style="margin-left: 30px;">' +
                '<span class="input-group-addon btn btn-secondary tts"><i class="fa fa-bullhorn"></i></span>' +
                '<textarea class="form-control promo-row" id="ans' + Date.now() + '"  onkeyup="auto_grow(this)"' +
                ' placeholder="Click Microphone icon and speak or start typing." rows="2" cols="70"></textarea>' +
                '<span class="input-group-addon btn btn-secondary stt">' +
                '<i class="fa fa-microphone"></i></span>' +
                '<span id=\'span_ans\' class=\'hidden promo_ans\' name="ans"></span></div>')
            .replace(/<\/div>[\s\S]*?<label/g, '</div><label')
            .replace(/– Knowing we stand by you to the end./, '')
            .replace(/A statement of the intended results./, '')
        );

        promo_type_selector.val('compose');
        $('#promo_id').val(activePromoId);

        fetchPromoAnswer();
        // $('#save-promo-content').before('<button class="btn btn-primary override"' +
        //     ' data-type="recompose">Override</button>');
        // compose_promo_title_selector.text('Recompose Promo');

        // Check if this is compose or recompose.
        promo_type_selector.val('compose');

        // Enable Voice Recognition.
        voiceRecognition();
        speechFunctionality();
    } else {
        toastr.error('You can create one using left panel.', 'No Outline Present!!');
    }
    // });

    // Save Promo content.
    save_promo_selector.unbind('click');
    save_promo_selector.click(function () {
        if (0 === parseInt(activePromoId)) {
            toastr.error('To create one use compose button.', 'No Promo version present!!');
            return;
        }

        const save_promo_content = makeAjaxCall('tsl/save_promo_content', {
            'content': tinymce.activeEditor.getContent(),
            'id': activePromoId,
            'client_id': client_id
        });

        save_promo_content.then(function (data) {
            toastr.success('Content Saved');
            ref = activePromoId;
            promo_added = 1;
            refreshPage();
        });
    });

    // Fetches and displays list of promos.
    const get_promo_list = makeAjaxCall('tsl/get_promo_list', {
        outline_id: activeOutlineId,
        client_id: client_id
    });
    get_promo_list.then(function (data) {
        updatePromoList(data.promo);
    });
}

function fetchPromoAnswer() {
    // Fetch the content for promo version.
    const get_promo = makeAjaxCall('tsl/get_promo_data', {
        id: activePromoId
    });

    // Populate the text area.
    get_promo.then(function (data) {
        $('#compose-promo-form textarea').val('');

        if ((data.data.length === 0)) {
            const get_default_answer = makeAjaxCall('tsl/get_default_promo_answers', {
                'outline_id': activeOutlineId
            });

            get_default_answer.then(function (data) {
                $.each(data.default_answers, function (index, answer) {
                    $('#compose-promo-form textarea')
                        .eq(index)
                        .val(answer.tpda_content)
                        .attr('data-id', index + 1)
                        .attr('id', 'ans' + (index + 1));
                });

            })
        } else {
            $.each(data.data, function (index, answer) {
                $('#compose-promo-form textarea')
                    .eq(index)
                    .val(answer.answer)
                    .attr('data-id', index + 1)
                    .attr('id', 'ans' + (index + 1));
            })

            // $('#compose-promo-form').find('textarea').each(function (index, textarea) {
            //     $(textarea).text(data.data[index].answer.replace(/\\/g, ''));
            // });
        }

        // Auto grow script form text area size.
        $('#compose-promo-form textarea').each(function () {
            // $(this).css('height', $(this)[0].scrollHeight + 'px');
        });
    });
}

// Fetches Buyer promo outline.
function refreshPage() {
    // Save from Send Email Instance.
    $(document).off('click', '#send_recompose, .save_as_new, #save-new-promo-content');
    $(document).on('click', '#send_recompose, .save_as_new, #save-new-promo-content', function (e) {
        const content = (null !== tinymce.activeEditor) ? tinymce.activeEditor.getContent() : null,
            type = $(this).attr('data-type'),
            update_content = makeAjaxCall('tsl/update_promo_content', {
                'id': activePromoId,
                'outline_id': activeOutlineId,
                'type': type,
                'content': content
            });

        update_content.then(function (data) {
            promo_added = 1;
            toastr.success('Action Completed.', 'Success!!')
            refreshPage();
            $('#send-promo-dialog').modal('hide');
            // $('#view-promo-archive-instance-dialog').modal('hide');
        })
    });

    // Adds Promo version to archive list.
    $('#add-promo-archive').click(function (data) {
        const add_promo_archive = makeAjaxCall('tsl/add_archive', {
            'promo_id': activePromoId,
            'type': 'page',
            'asset_id': client_id,
            'datetime': moment().format("DD-MM-YY hh:mm A"),
            'topic': $('.topic option:selected').text()
        });

        add_promo_archive.then(function (data) {
            toastr.success('Component Added to Archive!!');
        });
    });

    // Compose Promo Pop up click handler.
    compose_promo_form_selector = $('#compose-promo-form');

    // View PDF click handler.
    $(document).off('click', '#view-pdf');
    $(document).on('click', '#view-pdf', function () {
        if ((undefined == $(this).attr('data-id')) || (0 === parseInt(activePromoId))) {
            toastr.error('You can create one using Compose button.', 'No Promo Version Selected!!');
            return;
        } else {
            compose_promo(0);
            $("#view-dialog").modal('show', $(this));
        }
    });

    $(document).off('click', '#save-promo-content, .override');
    $(document).on('click', '#save-promo-content, .override', function (e) {

        // Check if the request is from recompose.
        ('recompose' === $(this).attr('data-type')) ? $('#promo_type').val('recompose') : '';
        let textAreaCount = 0,
            dObj;

        // Sets the value from the text field  into span.
        compose_promo_form_selector.find('span.promo_ans').each(function () {
            dObj = $(this);
            compose_promo_form_selector.find('textarea').eq(textAreaCount).attr('name', 'ans' + textAreaCount);
            dObj.text(compose_promo_form_selector.find('textarea').eq(textAreaCount).val());

            textAreaCount++;
        });

        $('#question_length').val(textAreaCount);

        // Fetch the html from for Compose Promo Form.
        const data = compose_promo_form_selector.html().replace(/<blockquote>/g,
            "<blockquote style='margin-top: 1em; margin-bottom: 1em; margin-left: 40px; margin-right: 40px;'>")
                .replace(/<label[\s\S]*?<\/label>/g, '').replace(/<textarea[\s\S]*?<\/textarea>/g, '')
                .replace(/<span id="span_ans" class="hidden">/g, "<span>"),

            // Get Form data.
            form_data = compose_promo_form_selector.serializeArray().reduce(function (obj, item) {
                obj[item.name] = item.value;
                return obj;
            }, {});

        // Add a new Promo instance.
        const update_promo = makeAjaxCall('tsl/update_promo', {
            outline: activeOutlineId,
            client_id: client_id,
            content: data,
            form_data: form_data,
            promo_id: activePromoId,
            questions_length: textAreaCount
        });

        // Fetch the promo list again and hide compose promo dialog.
        update_promo.then(function (data) {
            toastr.success('Promo Added.', 'Success!!')
            promo_added = 1;
            refreshPage();
            $('#compose-promo-dialog').modal('hide')
        });
    });

    // Fetches the list of background images belonging to a user.
    $.ajax({
        url: jsglobals.base_url + 'tsl/get_background_template',
        dataType: 'json',
        type: 'post',
        error: ajaxError
    }).done(function (data) {
        if (data.status !== 'success') {
            toastr.error(data.message);
            return;
        }
        else if (data.status === 'redirect') {
            window.location.href = data.redirect;
            return;
        }
        // Populate template drop down menu and background template listing table.
        const background_table_body_selector = $('#background-table tbody'),
            background_template_selector = $('#background-template');

        background_table_body_selector.empty();
        background_template_selector.empty();
        background_template_selector
            .append('<option disabled="" selected="" value="-1"> -- Select Background -- </option>');

        background_template_selector
            .append('<option value=0>Add/Edit Background Template.</option>');

        $.each(data.background_template, function (index, template) {
            $('#background-template')
                .append($('<option value="' + template.tpb_id + '">' + template.tpb_org_name + '</option>'));

            background_table_body_selector
                .append($('<tr>')
                    .append($('<td class="text-center">')
                        .append(index + 1))
                    .append($('<td class="text-center">')
                        .append('<img width="150" height="150" src=' + window.location.origin +
                            '/tools/tsl/view_thumbnail/?id=' + template.tpb_id + '" />'))
                    .append($('<td class="text-center">')
                        .append(template.tpb_org_name))
                    .append($('<td class="text-center">')
                        .append($('<button class="btn btn-default btn-sm"' +
                            ' data-id="' + template.tpb_id +
                            '" data-toggle ="modal' +
                            '" data-target ="#delete-background-dialog' +
                            '" data-backdrop ="static' +
                            '" data-name="' + template.tpb_org_name +
                            '"> <span class="glyphicon glyphicon-trash"></span>')))
                )
        });
    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
    });

    // Display buyer's promo buttons.
    $('.email_attachment').css("display", "none");
    $('#promo_buttons').css("display", "block");

    $.ajax({
        url: jsglobals.base_url + 'tsl/get_promo_outline',
        dataType: 'json',
        type: 'post',
        data: {
            promo_focus: $('#promo-focus').val(),
            promo_format: $('#promo-format').val(),
            client_type: (undefined !== $('#client-type').val()) ? $('#client-type').val() : '',
            criteria: (undefined !== $('#list-criteria').val()) ? $('#list-criteria').val() : 0,
            audience: (undefined !== $('#target-audience').val()) ? $('#target-audience').val() : 0,
            origin: origin
        },
        error: ajaxError
    }).done(function (data) {

        if (data.status !== 'success') {
            toastr.error(data.message);
            return;
        }
        else if (data.status === 'redirect') {
            window.location.href = data.redirect;
            return;
        }

        updateOutline(data.promo.outline, data.promo.active_id);

    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
    });
}


// Display selected Buyer promo outline.
function updateOutline(outlines, active) {
    (0 !== active.length) ?
        active = active[0].tpao_outline_id : active = 0;
    $('#promo-outline-table tbody tr').remove();
    let content = '<table class="table"><thead>' +
        '<th class="text-center" style="margin: 5px">#</th>' +
        '<th class="text-center" style="margin: 5px">Version</th>' +
        '<th class="text-center" style="margin: 5px">Default</th>' +
        '<th class="text-center" style="margin: 5px"><span class="glyphicon glyphicon-trash"></span></th>' +
        '</thead>' +
        '<tbody>';
    // List outline versions.
    $('#outline-table tr').remove();
    $('#promo-outline').empty();
    $.each(outlines, function (index, outline) {
        let disabled = '';
        (('0' === outline.tpo_id) || ('-1' === outline.tpo_id)) ? disabled = 'disabled' : '';

        $('#promo-outline').append('<option value="' + outline.tpo_id + '">' + outline.tpo_name + '</option>');

        // content += '<tr><td class="text-center" style="margin: 5px">' + (index + 1) + '</td>'
        //     + '<td class="text-center" style="margin: 5px">' +
        //     '<span class="editable" data-type="text" data-pk="' + outline.tpo_id + '" ' +
        //     'data-url="' + jsglobals.base_url + 'tsl/update_promo_outline_name' + '">'
        //     + outline.tpo_name + '</span><span><a class="promotion-outline" id="outline' + outline.tpo_id +
        //     '"' + ' data-value="' + index + '">' +
        //     '  <i class="fa fa-eye" style="color: black"></i><a></span></td>'
        //     + '<td class="text-center" style="margin: 5px">' +
        //     '<input type="radio" class="active_outline_id" data-name="outline_id" ' +
        //     ' id="active' + outline.tpo_id + '"' + (active === outline.tpo_id ? "checked" : "") +
        //     ' data-id="' + outline.tpo_id + '" name="active_outline" value="' + outline.tpo_id + '"' + '></td>'
        //     + '<td class="text-center" style="margin: 5px"><button class="btn btn-default btn-sm"' +
        //     ' data-toggle="modal" ' + disabled + '' +
        //     ' data-target="#delete-promo-outline-dialog"' +
        //     ' data-backdrop="static"' +
        //     ' data-id="' + outline.tpo_id +
        //     '" data-name="' + outline.tpo_name +
        //     '"> <span class="glyphicon glyphicon-trash"></span></button></td>';

        // $('#view-promo-outline').attr('data-content', '<table>' +
        //     '<thead>' +
        //     '<th>#</th>' +
        //     '<th>Version</th>' +
        //     '<th>Default</th>' +
        //     '<th><span class="glyphicon glyphicon-trash"></span></th>' +
        //     '</thead>' +
        //     '<tbody></tbody>' +
        //     '</table>');


        // $('#promo-outline-table tbody')
        //     .append($('<tr>')
        //         .append($('<td class="text-center">')
        //             .append(index + 1))
        //         .append($('<td class="text-center">')
        //             .append($('<span class="editable" data-type="text" data-pk="' + outline.tpo_id + '" ' +
        //                 'data-url="' + jsglobals.base_url + 'tsl/update_promo_outline_name' + '">'
        //                 + outline.tpo_name + '</span><span><a class="promotion-outline" id="outline' + outline.tpo_id +
        //                 '"' + ' data-value="' + index + '">' +
        //                 '  <i class="fa fa-eye" style="color: black"></i><a></span>')))
        //         .append($('<td class="text-center">')
        //             .append($('<input type="radio" class="active_outline_id" data-name="outline_id" ' +
        //                 ' id="active' + outline.tpo_id + '"' +
        //                 ' data-id="' + outline.tpo_id + '" name="active_outline" value="' + outline.tpo_id + '"' + '>')))
        //         .append($('<td class="text-center">')
        //             .append($('<button class="btn btn-default btn-sm"' +
        //                 ' data-toggle="modal" ' + disabled + '' +
        //                 ' data-target="#delete-promo-outline-dialog"' +
        //                 ' data-backdrop="static"' +
        //                 ' data-id="' + outline.tpo_id +
        //                 '" data-name="' + outline.tpo_name +
        //                 '"> <span class="glyphicon glyphicon-trash"></span>')))
        //     );
    });

    // $('#view-promo-outline').attr('data-content', content);

    // Check active Outline Id.
    // $('#active' + active).attr('checked', true);
    $('#promo-outline').val(active);

    $(document).off('change', '#promo-outline');
    $(document).on('change', '#promo-outline', function () {
        const change_active_outline = makeAjaxCall('tsl/change_active_outline', {
            'id': $('#promo-outline option:selected').attr('value'),
            'component': 'promo',
            'promo_focus': $('#promo-focus').val(),
            'promo_format': $('#promo-format').val(),
            'client_type': (undefined !== $('#client-type').val()) ? $('#client-type').val() : '',
            'criteria': (undefined !== $('#list-criteria').val()) ? $('#list-criteria').val() : 0,
            'audience': (undefined !== $('#target-audience').val()) ? $('#target-audience').val() : 0,
            'origin': origin
        });

        change_active_outline.then(function () {
            toastr.success('Default Outline updated!!');
            activeOutlineId = 0;
            $('#view-promo-outline-dialog').modal('hide');
            refreshPage();
        });
    });


    $(document).off('change', '.active_outline_id');
    $(document).on('change', '.active_outline_id', function () {
        const change_active_outline = makeAjaxCall('tsl/change_active_outline', {
            'id': $(this).attr('value'),
            'component': 'promo',
            'promo_focus': $('#promo-focus').val(),
            'promo_format': $('#promo-format').val(),
            'client_type': (undefined !== $('#client-type').val()) ? $('#client-type').val() : '',
            'criteria': (undefined !== $('#list-criteria').val()) ? $('#list-criteria').val() : 0,
            'audience': (undefined !== $('#target-audience').val()) ? $('#target-audience').val() : 0,
            'origin': origin
        });

        change_active_outline.then(function () {
            toastr.success('Default Outline updated!!');
            activeOutlineId = 0;
            $('#view-promo-outline-dialog').modal('hide');
            refreshPage();
        });
    });

    // Add ability to focus on elements inside x-editable.
    // $('#view-promo-outline-dialog').on('shown.bs.modal', function () {
    //     $(document).off('focusin.modal');
    // });

    const promo_outline_selector = $('.promotion-outline');
    promo_outline_selector.off('click');

    // Display selected outline text in editor.
    promo_outline_selector.click(function () {
        $('.active_outline').removeClass('active_outline');
        $(this).closest('tr').addClass('active_outline');

        let content = outlines[$(this).attr('data-value')].tpo_content
            .replace(/{{/g, '<span style=\'color: red\'>{{')
            .replace(/}}/g, '}}</span>')
            .replace(/\[\[/g, '<span style=\'color: yellow\'>\[\[')
            .replace(/\]\]/, '\]\]</span>')
            .replace(/\(\(/g, '<span style=\'green\'>\(\(')
            .replace(/\)\)/g, '\)\)</span>');

        $('#outline-container').html('<h4 class="main-con-header">Outlines</h4><br>' +
            '<form id="outline-form">' +
            '<input type="hidden" id="target_audience" name="target_audience" >' +
            '<input type="hidden" id="criteria" name="criteria" >' +
            '<input type="hidden" id="promo_focus" name="promo_focus">' +
            '<input type="hidden" id="promo_format" name="promo_format">' +
            '<input type="hidden" id="client_type" name="client_type">' +
            '<input id="y" value="' + content +
            '" type="hidden" name="outline_content">\n' +
            '<trix-editor id="trix-editor" class="editor" input="y" name="tsl_promo_editor"></trix-editor>' +
            '</form>');

        obj = {
            "data-id": outlines[$(this).attr('data-value')].tpo_id,
            "data-content": outlines[$(this).attr('data-value')].tpo_content
        };

        activePromoId = 0;
        setupPromo(obj);
        $('#view-promo-outline-dialog').modal('hide')
    });

    // Displays the default outline if no outline present.
    if (outlines.length === 0) {
        const default_outline = '<div><strong> {{ Big Headline }}</strong></div><div>' +
            '<strong> {{ Emotional Connection }}</strong></div><div><strong> {{ Inside Scoop }}</strong></div>' +
            '<div>&nbsp; &nbsp; <strong>    {{ Evidence }}</strong></div><div>&nbsp; &nbsp;' +
            ' <strong>     {{ Impact }}</strong></div><div>&nbsp; &nbsp; <strong>   {{ Challenge }}</strong>' +
            '</div><div>&nbsp; &nbsp; <strong>   {{ Remedy }}</strong></div><div>&nbsp; &nbsp;&nbsp;<strong>    ' +
            ' {{ Delivery }}</strong></div><div><strong> {{ Compelling Offer }}</strong></div>' +
            '<div><strong> {{ Bold Promise }}</strong></div>';

        $('#outline-container').html('<h4 class="main-con-header">Outline</h4><br>' +
            '<form id="outline-form">' +
            '<input type="hidden" id="target_audience" name="target_audience" >' +
            '<input type="hidden" id="criteria" name="criteria" >' +
            '<input type="hidden" id="promo_focus" name="promo_focus">' +
            '<input type="hidden" id="promo_format" name="promo_format">' +
            '<input type="hidden" id="client_type" name="client_type">' +
            '<input id="y" value="' + default_outline +
            '" type="hidden" name="outline_content">\n' +
            '<trix-editor id="trix-editor" class="editor" input="y" name="tsl_promo_editor"></trix-editor>' +
            '</form>');

        obj = {
            "data-id": 0,
            "data-content": default_outline
        };
        setupPromo(obj);

    } else if (outline_added === 1) {
        outline_added = 0;
        $('.promotion-outline')
            .eq($('.promoition-outline').length - 1)
            .trigger('click')
    } else if (activeOutlineId === 0) {

        let index = 0;
        $('#outline' + active).closest('tr').addClass('active_outline')
        index = outlines.findIndex(x => x.tpo_id === active);

        (-1 === index) ? index = 0 : '';
        let content = outlines[index].tpo_content
            .replace(/<blockquote>/g, '')
            .replace(/{{/g, '{{<span style=\'color: red\'>')
            .replace(/}}/g, '</span>}}')
            .replace(/\[\[/g, '<span style=\'color: yellow\'>\[\[')
            .replace(/\]\]/, '\]\]</span>')
            .replace(/\(\(/g, '<span style=\'green\'>\(\(')
            .replace(/\)\)/g, '\)\)</span>');

        // Display first outline, if no outline is selected.
        $('#outline-container').html('<h4 class="main-con-header">Outline</h4><br>' +
            '<form id="outline-form">' +
            '<input type="hidden" id="target_audience" name="target_audience" >' +
            '<input type="hidden" id="criteria" name="criteria" >' +
            '<input type="hidden" id="promo_focus" name="promo_focus">' +
            '<input type="hidden" id="promo_format" name="promo_format">' +
            '<input type="hidden" id="client_type" name="client_type">' +
            '<input id="y" value="' + content +
            '" type="hidden" name="outline_content">\n' +
            '<trix-editor id="trix-editor" class="editor" input="y" name="tsl_promo_editor"></trix-editor>' +
            '</form>');

        obj = {"data-id": outlines[index].tpo_id, "data-content": outlines[index].tpo_content};
        setupPromo(obj);
    } else if (promo_added === 1 || pdf_added === 1) {
        // If new promo is added.
        obj = {
            "data-id": activeOutlineId,
            "data-content": outlines.find(x => x.tpo_id === activeOutlineId).tpo_content
        };
        setupPromo(obj);
    }
}

// Display list of Buyer's promo instances.
function updatePromoList(promos) {
    $('#tsl-promo-nav').empty();
    $('#tsl-pdf-nav').empty();

    const content = $('#default_promo_content').html().replace(/"/g, "\'");

    $('#tsl-promo-nav')
        .append($('<li class="text-center" id="0">')
            .append('<a href="#" onclick="setupPDF(this); return false;" data-content="' + content + '" data-id="0">' +
                'Example Component</a>'));

    // Append Promos to promo list.
    $.each(promos.promo, function (index, promo) {
        const content = (promo.tp_content != null ? promo.tp_content.replace(/\\"/g, "'") : '').replace(/\\/g, '');

        $('#tsl-promo-nav')
            .append($('<li class="text-center" id="' + promo.tp_id + '">')
                .append('<a href="#" class="edit-ref" onclick="setupPDF(this); return false;" data-name="' +
                    promo.tp_org_name + '"data-id="' + promo.tp_id + '" data-content="' + content + '">' +
                    '</span><span style="float: left; color: black" onclick="setupPDF(this); return false;"' +
                    ' data-name="' + promo.tp_org_name + '" data-id="' + promo.tp_id + '"' +
                    ' data-content="' + content + '" data-content-id="' + promo.tpc_id + '">' +
                    '<i class="fa fa-eye"></i></span>' + '<span style="float: right; color: black" class="delete_promo"' +
                    ' data-id="' + promo.tp_id + '" data-target="#delete-promo-dialog" data-toggle="modal"' +
                    ' data-name="' + promo.tp_org_name + '">' +
                    '<i class="fa fa-trash"></i></span>' +
                    '<span class="editable" data-type="text" data-pk = "' + promo.tp_id + '" ' +
                    'data-url = "' + jsglobals.base_url + 'tsl/update_promo_name" style="word-break: break-word">' +
                    promo.tp_org_name + '</span></a></li>'))
    });

    // Append PDFs to pdf list.
    $.each(promos.pdf, function (index, pdf) {
        $('#tsl-pdf-nav')
            .append($('<li class="text-center" id="pdf' + pdf.tp_id + '" data-id="' + pdf.tp_id + '">')
                .append('<a href="#" class="edit-ref" data-name="' +
                    pdf.tp_org_name + '"data-id="' + pdf.tp_id + '">' +
                    '<span style="float: left; color: black" data-target="#view-dialog"' +
                    ' data-toggle="modal" data-id="' + pdf.tp_id + '" class="open_pdf" data-is-pdf="1">' +
                    '<i class="fa fa-eye"></i></span>' +
                    '<span class="editable" data-type="text" data-pk = "' + pdf.tp_id + '" ' +
                    'data-url = "' + jsglobals.base_url + 'tsl/update_pdf_name">' +
                    pdf.tp_org_name + '</span>' +
                    '<span style="float: right; color: black" data-id="' + pdf.tp_id + '" class="delete_pdf"' +
                    ' data-type="pdf" data-toggle="modal" data-target="#delete-promo-dialog"' +
                    ' data-name="' + pdf.tp_org_name + '">' +
                    '<i class="fa fa-trash"></i></span>' +
                    '</a></li>'))
    });

    $('.open_pdf, .delete_promo, .delete_pdf').click(function () {
        $(this).attr('data-target').modal('show');
    });

    const view_promo_pdf_selector = $('#view-promo-pdf');

    $('#send-promo').removeAttr('data-toggle');
    $('#delete-promo').removeAttr('data-toggle');
    view_promo_pdf_selector.removeAttr('data-toggle');

    // Set the default PDF ID.
    if (0 !== promos.pdf.length) {
        view_promo_pdf_selector.attr('data-id', promos.pdf[0].tp_id);
        view_promo_pdf_selector.attr('data-toggle', 'modal');
        $('#pdf' + promos.pdf[0].tp_id).addClass('active_pdf');
    }

    if ((activePromoId === 0 || activePromoId === -1) && 0 === promos.promo.length) {
        const content = $('#default_promo_content').html().replace(/"/g, "\'");

        obj = {"data-id": 0, "data-content-id": 0, "data-name": 'Example Component', "data-content": content};
        setupPDF(obj);
    }
    else if (ref !== undefined) {
        const index = promos.promo.findIndex(x => x.tp_id === ref),
            content = promos.promo[index].tp_content.replace(/\\"/g, "'").replace(/\\/g, '');

        obj = {"data-id": ref, "data-name": promos.promo[index].tp_org_name, "data-content": content};
        ref = undefined;

        setupPDF(obj);
    } else if ((activePromoId === 0 || activePromoId === -1) && 0 !== promos.promo.length) {
        obj = {
            "data-id": promos.promo[0].tp_id,
            "data-name": promos.promo[0].tp_org_name,
            "data-content": promos.promo[0].tp_content,
            "data-content-id": promos.promo[0].tpc_id
        };
        setupPDF(obj);
    } else if (promo_added === 1) {
        // Fetch data from DB and replace " with '.
        const content = (promos.promo[promos.promo.length - 1].tp_content != null ?
            promos.promo[promos.promo.length - 1].tp_content.replace(/\\"/g, "'").replace(/\\/g, '') : '');

        obj = {
            "data-id": promos.promo[promos.promo.length - 1].tp_id,
            "data-name": promos.promo[promos.promo.length - 1].tp_org_name,
            "data-content-id": promos.promo[promos.promo.length - 1].tpc_id,
            "data-content": content
        };
        setupPDF(obj);
    } else if (pdf_added === 1) {
        const index = promos.promo.findIndex(x => x.tp_id === activePromoId),
            content = promos.promo[index].tp_content.replace(/\\"/g, "'").replace(/\\/g, '');
        obj = {
            "data-id": promos.promo[index].tp_id,
            "data-name": promos.promo[index].tp_org_name,
            "data-content-id": promos.promo[index].tpc_id,
            "data-content": content
        };
        setupPDF(obj);
    }
    setEditable('bottom');
}

// Fetch and display already answered questions for a promo.
function setupPDF(obj) {

    activePromoId = $(obj).attr('data-id');
    contentId = $(obj).attr('data-content-id');


    $('#view-pdf').attr('data-id', activePromoId);
    // Displaying the active email name.
    $('.active').removeClass('active');
    $('#' + activePromoId).addClass('active');
    const send_promo_selector = $('.send-promo'),
        delete_promo_selector = $('#delete-promo');

    send_promo_selector.removeAttr('data-toggle');
    if (0 !== parseInt(activePromoId)) {
        // Set attribute for send button.
        send_promo_selector.attr('data-id', activePromoId);
        send_promo_selector.attr('data-toggle', 'modal');
        send_promo_selector.attr('data-target', '#send-promo-dialog');
        send_promo_selector.attr('data-content-id', contentId);
        send_promo_selector.attr('data-backdrop', 'static');
        send_promo_selector.attr('data-name', $(obj).attr('data-name'));
    }

    // Set attribute for delete button.
    delete_promo_selector.attr('data-id', activePromoId);
    delete_promo_selector.attr('data-toggle', 'modal');
    delete_promo_selector.attr('data-target', '#delete-promo-dialog');
    delete_promo_selector.attr('data-backdrop', 'static');
    delete_promo_selector.attr('data-name', $(obj).attr('data-name'));

    promo_id = $(obj).attr('data-id');

    //Set data id form alert button.
    $('.add-to-alert').attr('data-id', promo_id);

    // Setup content for promo reading view.
    $('#read-promo').off('click');
    $('#read-promo').click(function () {
        $('#email-container').html('');
        // Check if Send Email editor is up. If so remove it.
        (null !== tinymce.get('email-container')) ? tinymce.get('email-container').remove() : '';

        // Check if archive editor is up. If so remove it.
        (null !== tinymce.get('archive-viewer-container')) ? tinymce.get('archive-viewer-container').remove() : '';
        initEditor(obj);

        // Set the content for Editor.
        tinymce.activeEditor.setContent($(obj).attr('data-content'));
    });

    const compose_promo_textarea_selector = $('#compose-promo-form').find('textarea'),
        path = window.location.pathname,
        components = path.split('/'),
        type = components[components.length - 2];

    // Set the data attribute for PDF pop up on selecting an item form PDF list.
    $('#tsl-pdf-nav li').click(function () {

        const view_promo_pdf_selector = $('#view-promo-pdf');
        $('.active_pdf').removeClass('active_pdf');
        $(this).addClass('active_pdf');
        view_promo_pdf_selector.attr('data-id', $(this).attr('data-id'));
        view_promo_pdf_selector.attr('data-toggle', 'modal');
    });

    //On click handler for Create PDF.
    const create_pdf_selector = $('#create_promo_pdf');
    create_pdf_selector.unbind('click');
    create_pdf_selector.click(function () {

        // Check if Promo Version is present.
        if (0 === parseInt(activePromoId)) {
            toastr.error('You can create one using Compose button.', 'No Promo Version Selected!!');
            return;
        }

        compose_promo(1);
    });

    // Makes the names editable.
    $(".edit-ref").children().click(function (event) {
        event.stopPropagation();
    });

    fetchPromoAnswer();
}

// Create a new Promo PDF. is_pdf checks if the request is to view a PDF or to generate one.
function compose_promo(is_pdf) {
    let url = jsglobals.base_url + 'tsl/compose_promo';

    const compose_promo = makeAjaxCall('tsl/compose_promo', {
        data: tinymce.activeEditor.getContent(),
        id: activePromoId,
        compose: $('#compose-page').attr('data-compose'),
        bg_image: $('.upload-sa').attr('data-name'),
        sa: $('#sa-type').val(),
        page: $('#page-template').val(),
        template: $('#template-version').val(),
        template_id: $('#background-template').val(),
        is_pdf: is_pdf
    });

    // Event handler after promo is composed.
    compose_promo.then(function () {
        if (is_pdf === 1) {
            toastr.success('PDF Generated!!', 'Success')
            pdf_added = 1;
            refreshPage();
        }
        $('#edit-page-dialog').modal('hide');
    });
}