let activeSeqId, sequence_added, message_added, activeMessageId, messageIdArray, isTriggered, client_id,
    component_name, condensed_content, activeOutlineId = 0, tsl_id, req_type;

$(document).ready(function () {
    $('#copyright_text').empty();
    $('#copyright_text').append('<hr/>' +
        '<div class="col-xs-7 col-xs-offset-3">' +
        '<p class="text-center">Copyright &copy; ' + new Date().getFullYear() + ' RothMethods, Inc. All rights' +
        ' reserved</p>' +
        '</div>');

    // Get the Client ID from the URL
    const path = window.location.pathname,
        components = path.split('/');

    client_id = components[components.length - 1];
    origin = components[2];
    req_type = components[3];

    component_name = 'Message';

    if (components[3] === 'story') {
        component_name = 'Story';
    } else if (components[3] === 'review') {
        component_name = 'Debriefing';
    } else if (components[3] === 'followup') {
        component_name = 'Follow-Up';
    }

    $('#component-heading').html(component_name.toUpperCase() + ' BUILDER');

    const searchParams = new URLSearchParams(window.location.search);
    tsl_id = searchParams.get('id');

    loadPrev();

    sequence_added = 0;
    activeSeqId = 0;
    activeMessageId = 0;
    isTriggered = false;

    // List of message sequence.
    const message_version_selector = $('#component-version'),
        message_outline_selector = $('#outline-container');

    if ('tsl' === origin) {
        $('#messsage_header').remove();

        // Checks whether the request is for message or stories. And sets the drop down content appropriately.
        // let set_dropdown = 'Story' === component_name ?
        //     '<div class="col-xs-15 script-box">' +
        //     '<label class="col-xs-12" style="text-align: center">Sequence Topic</label>' +
        //     '<select class="form-control msg-menu" id="event-confirmation">' +
        //     '<option value="-1" disabled selected>-- Select Sequence Topic --</option>' +
        //     '<option value="0">Add a Sequence Topic</option>' +
        //     '</select>' +
        //     '</div>'
        //     :
        //     '<div class="col-xs-15 script-box">' +
        //     '<label class="col-xs-12" style="text-align: center">Event Invitation</label>' +
        //     '<select class="form-control msg-menu" id="event-confirmation">' +
        //     '<option value="ics">Initial Contact Script</option>' +
        //     '<option value="ems">Exploratory Meeting Script</option>' +
        //     '<option value="pps">Proposal Presentation Script</option>' +
        //     '<option value="plms">Project Launch Meeting Script</option>' +
        //     '</select>' +
        //     '</div>';

        if ('Story' === component_name) {
            $('#component_header').html(
                '<div class="col-md-3 script-box">' +
                '<label class="col-xs-12" style="text-align: center">Recipient Type</label>' +
                '<select class="form-control msg-menu" id="vmail-type">' +
                '<option value="rc">Radish Client</option>' +
                '<option value="wc">Wheat Client</option>' +
                '<option value="tc">Tree Client</option>' +
                '<option value="in">Institutional</option>' +
                '<option value="de">Development</option>' +
                '</select>' +
                '</div>' +
                '<div class="col-md-3 script-box">' +
                '<label class="col-xs-12" style="text-align: center">Story Topic</label>' +
                '<select class="form-control" id="vmail-delivery">' +
                '<option value="md">Market Downturn</option>' +
                '<option value="fta">Failure to Act</option>' +
                '<option value="cbs">Caught by Surprise</option>' +
                '<option value="mo">Missed Opportunity</option>' +
                '<option value="hte">Hesitant to Explore</option>' +
                '</select>' +
                '</div>' +
                '<div class="col-md-3 script-box">' +
                '<label class="col-xs-12" style="text-align: center">Story Delivery</label>' +
                '<select class="form-control" id="vmail-recurrence">' +
                '<option value="eip">Embed Into Promo</option>' +
                '<option value="eim">Embed Into Message</option>' +
                '<option value="eis">Embed Into Script</option>' +
                '<option value="eic">Embed Into Confirm</option>' +
                '<option value="ate">Attach To Email</option>' +
                '</select>' +
                '</div>' +
                '<div class="col-md-3 script-box" id="promo-focus-parent" data-toggle="popover"' +
                'data-placement="bottom" data-html="true">' +
                '<label class="col-xs-12 text-center">Story Sequence</label>' +
                '<select class="form-control msg-menu" id="vmail-sequence">' +
                '</select>' +
                '</div>'
            )
        } else {
            $('#component_header').html(
                '<div class="col-md-3 script-box">' +
                '<label class="col-xs-12" style="text-align: center">Message Type</label>' +
                '<select class="form-control msg-menu" id="vmail-type">' +
                '<option value="mo">Meeting Offer</option>' +
                '<option value="mc">Meeting Confirmation</option>' +
                '<option value="mr">Meeting Reminder</option>' +
                '<option value="tu">Transaction Update</option>' +
                '<option value="ra">Request Approval</option>' +
                '<option value="pr">Phase Reschedule</option>' +
                '</select>' +
                '</div>' +
                '<div class="col-md-3 script-box">' +
                '<label class="col-xs-12" style="text-align: center">Message Objective</label>' +
                '<select class="form-control msg-menu" id="vmail-objective">' +
                '<option value="sm">Schedule Meeting</option>' +
                '<option value="cm">Confirm Meeting</option>' +
                '<option value="mr">Meeting Reminder</option>' +
                '<option value="osa">Offer Strategic Analysis/option>' +
                '<option value="omu">Offer Marketing Update</option>' +
                '<option value="rd">Request Documents</option>' +
                '</select>' +
                '</div>' +
                '<div class="col-md-3 script-box">' +
                '<label class="col-xs-12" style="text-align: center">Message Delivery</label>' +
                '<select class="form-control" id="vmail-delivery">' +
                '<option value="pc">Phone Call</option>' +
                '<option value="wbc">Web Based Call</option>' +
                '<option value="vb">Vmail Blast</option>' +
                '<option value="tm">Text Message</option>' +
                '<option value="sm">Social Media</option>' +
                '</select>' +
                '</div>' +
                // '<div class="col-xs-15 script-box">' +
                // '<label class="col-xs-12" style="text-align: center">Message Recurrence</label>' +
                // '<select class="form-control" id="vmail-recurrence">' +
                // '<option value="pc">Daily</option>' +
                // '<option value="wbc">Three Days</option>' +
                // '<option value="vb">Weekly</option>' +
                // '<option value="tm">Monthly</option>' +
                // '<option value="sm">Quarterly</option>' +
                // '</select>' +
                // '</div>' +
                '<div class="col-md-3 script-box" id="promo-focus-parent" data-toggle="popover"' +
                'data-placement="bottom" data-html="true">' +
                '<label class="col-xs-12 text-center">Message Sequence</label>' +
                '<select class="form-control msg-menu" id="vmail-sequence">' +
                '</select>' +
                '</div>'
                // '<div class="col-xs-15 script-box" id="promo-focus-parent" data-toggle="popover"' +
                // 'data-placement="bottom" data-html="true">' +
                // '<label class="col-xs-12 text-center">Message Outline</label>' +
                // '<select class="form-control msg-menu" id="vmail-outline">' +
                // '</select>' +
                // '</div>'
            );
        }

        // Check for the component and set the Center Pane.
        // if ('Story' === component_name) {
        //     $('#component_header').append('<div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">' +
        //         '<label class="col-xs-12" style="text-align: center">Story Type:</label>' +
        //         '<select class="form-control" id="message-type">' +
        //         '<option value="no">Negative Outcomes</option>' +
        //         '<option value="po">Positive Outcomes</option>' +
        //         '<option value="mo">Mixed Outcomes</option>' +
        //         '</select>' +
        //         '</div>')
        // }
    } else {
        $('#component_header').html('<div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">' +
            '<label class="col-xs-12" style="text-align: center; text-transform:capitalize;">Client Type</label>' +
            '<select class="form-control" id="category-list">' +
            '<option value="r">Radish</option>' +
            '<option value="w">Wheat</option>' +
            '<option value="t">Tree</option>' +
            '</select>' +
            '</div>' +
            '<div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">' +
            '<label class="col-xs-12" style="text-align: center">Event Type:</label>' +
            '<select class="form-control col-xs-8" id="promo-focus">' +
            '<option value="exm">Exploratory Meeting</option>' +
            '<option value="prm">Presentation Meeting</option>' +
            '<option value="lam">Launch Meeting</option>' +
            '<option value="com">Contract Meeting</option>' +
            '<option value="clm">Closing Meeting</option>' +
            '<option value="clc">Closing Celebration</option>' +
            '</select>' +
            '</div>' +
            '</div>');

        // Check for the component and set the Center Pane.
        if ('Story' === component_name) {
            $('#component_header').append('<div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">' +
                '<label class="col-xs-12" style="text-align: center">Story Type:</label>' +
                '<select class="form-control" id="message-type">' +
                '<option value="no">Negative Outcomes</option>' +
                '<option value="po">Positive Outcomes</option>' +
                '<option value="mo">Mixed Outcomes</option>' +
                '</select>' +
                '</div>')
        } else if ('Debriefing' === component_name) {
            $('#component_header').append('<div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">' +
                '<label class="col-xs-12" style="text-align: center;">Debriefing Type:</label>' +
                '<select class="form-control" id="message-type">' +
                '<option value="od">Offer Document</option>' +
                '<option value="oc">Offer Conversation</option>' +
                '<option value="om">Offer Meeting</option>' +
                '<option value="op">Offer Presentation</option>' +
                '<option value="oe">Offer Event</option>' +
                '</select>' +
                '</div>')
        } else {
            $('#component_header').append('<div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">' +
                '<label class="col-xs-12" style="text-align: center;">Message Type:</label>' +
                '<select class="form-control" id="message-type">' +
                '<option value="od">Offer Document</option>' +
                '<option value="oc">Offer Conversation</option>' +
                '<option value="om">Offer Meeting</option>' +
                '<option value="op">Offer Presentation</option>' +
                '<option value="oe">Offer Event</option>' +
                '</select>' +
                '</div>')
        }
    }

    // Check if the component is a dummy.
    check_dummy_component(client_id);

    enableDatePicker();

    // Check if request is from TSL page.
    if ('tsl' === origin) {
        refreshTSLdropdown().then(function () {
            refreshDMDDropdown().then(function (data) {
                refreshPage();
            })
        });
    } else {
        refreshPage();
    }

    message_outline_selector.html(
        ' <h4 class="main-con-header">' + component_name + ' Outline</h4>' +
        ' <ul class="nav" id="msg-seq-nav">\n' +
        ' </ul>');

    // List of messages.
    message_version_selector.html('');
    message_version_selector.html(
        ' <h4 class="main-con-header">' + component_name + ' Version</h4>' +
        ' <ul class="nav" id="msg-ver-nav">\n' +
        ' </ul>');

    $('#category-list, #message-type').change(function () {
        activeSeqId = 0;
        // $('.delete').removeAttr('data-toggle');
        $('#component-container').empty();
        $('#msg-ver-nav').empty();
        refreshPage();
    });

    // Display Prompter for message.
    $('#prompt-msg').click(function () {
        condensed_content = '';

        // Set the content for prompter form.
        $.each($('#message-form textarea'), function (index, text) {
            condensed_content = condensed_content +
                '<b><u>' + $('#message-form').find('label').eq(index).text() + '</u></b>';
            let text_array = ($(text).val()).split('.');
            $.each(text_array, function (index, data) {
                condensed_content = condensed_content + '<p>' + data + '</p>';
            });
        });

        $('#credits').html('');

        $('#credits').html(condensed_content);
        $('#view-prompter-dialog').modal('show');
    });

    // Saves a message outline
    $('#save-msg-outline').click(function () {
        const save_msg_outline = makeAjaxCall('tsl/save_message_outline', {
            'component': component_name,
            'content': $('#y').val()
        });

        save_msg_outline.then(function (data) {
            toastr.success('Message Outline Saved.', 'Success!!');
            activeOutlineId = 0;
            refreshPage();
        })
    });


    // Adds a new message sequence.
    $('#create-message-sequence').click(function (e) {
        // Check if there's any active Message.
        if (0 === parseInt(activeMessageId)) {
            toastr.error('Create one or select one using left panel.', 'Example Message!!');
        } else {
            sequence_added = 1;
            e.preventDefault();

            $.ajax({
                url: jsglobals.base_url + 'tsl/create_message_sequence',
                dataType: 'json',
                data: {
                    'id': activeMessageId,
                    'component_name': component_name
                },
                type: 'post',
                error: ajaxError
            }).done(function (data) {

                if (data.status != 'success') {
                    toastr.error(data.message);
                    return;
                } else if (data.status == 'redirect') {
                    window.location.href = data.redirect;
                    return;
                }
                sequence_added = 1;
                refreshPage();
            }).fail(function (jqXHR, textStatus) {
                toastr.error("Request failed: " + textStatus);
            }).always(function () {
            });
        }
    });

    $('#spoken-sequence').click(function () {
        const save_spoken_message = makeAjaxCall('tsl/save_spoken_message', {
            'client_id': client_id,
            'seq_id': activeSeqId,
            'component_id': activeMessageId,
            'component_type': 'vmail'
        });

        save_spoken_message.then(function (data) {
            toastr.success('Message Added to Spoken List');
        })

    });

    // Adds a new message.
    $('#create-message-version').click(function (e) {
        e.preventDefault();
        let ta, criteria;
        ta = (null !== $('#target-audience').val()) ? $('#target-audience').val() : 0;
        criteria = (null !== $('#list-criteria').val()) ? $('#list-criteria').val() : 0;

        // Adds a new instance of message version to the database.
        const add_message = makeAjaxCall('tsl/create_message', {
            category: $('#category-list').val(),
            message_name: 'Custom Message',
            tool_id: tsl_id,
            component_name: component_name,
            client_id: client_id,
            outline_id: activeOutlineId
        });

        // Refresh Page after message version is added.
        add_message.then(function (data) {
            message_added = 1;
            refreshPage();
        });

    });

    // Updates the content of message.
    $('#save-msg-seq').click(function (e) {
        e.preventDefault();

        if (0 !== parseInt(activeMessageId)) {
            let msg_data = [],
                count = 0;

            $.each($('#message-form textarea'), function (index, textarea) {
                if ($(textarea).attr('id') !== 'condensed-text') {
                    msg_data.push({name: 'ans' + index, value: $(textarea).val()})
                    count++;
                }
            });

            msg_data.push({name: 'seq_count', value: count});
            msg_data.push({name: 'seq_id', value: activeSeqId});

            $.ajax({
                url: jsglobals.base_url + 'tsl/update_message_content',
                dataType: 'json',
                type: 'post',
                data: msg_data,
                error: ajaxError
            }).done(function (data) {

                if (data.status !== 'success') {
                    toastr.error(data.message);
                    return;
                }
                else if (data.status === 'redirect') {
                    window.location.href = data.redirect;
                    return;
                } else if (data.content === false) {
                    toastr.error('No instance of ' + component_name + ' version selected/Same '
                        + component_name + ' Content', 'Unable to update content!!');
                    return;
                }
                toastr.success('Content Saved');
            }).fail(function (jqXHR, textStatus) {
                toastr.error("Request failed: " + textStatus);
            }).always(function () {
            });
        } else {
            toastr.error('To create one use Left Panel.', 'No ' + component_name + ' Version Selected!!')
        }
    });

    // Creates a new copy of message.
    $(document).on('click', '.copy_msg', function () {
        const id = $(this).attr('data-id');
        if (0 == id) {
            toastr.error('To create one use Compose button.', 'No ' + component_name + ' Version Selected!!');
            return;
        }

        const copy_message = makeAjaxCall('tsl/add_copy_message', {
            id: id
        });

        copy_message.then(function (data) {
            refreshPage();
        });
    });

    // Deletes a copy of message.
    // $(document).on('click', '.delete_msg', function () {
    //     const message_id = $(this).attr('data-id');
    //
    //     if (0 == message_id) {
    //         toastr.error('To create one use Compose button.', 'No ' + component_name + ' Version Selected!!');
    //         return;
    //     }
    //
    //     const delete_message = makeAjaxCall('tsl/delete_message_copy', {
    //         id: message_id
    //     });
    //
    //     delete_message.then(function (data) {
    //         refreshPage();
    //     }).catch(function (e) {
    //         console.log(e);
    //     })
    // });

    $(document).on('change', '#vmail-sequence', function (data) {
        const obj = {
            "data-id": $(this).find('option:selected').attr('data-sequence'),
            "data-sequence": $(this).find('option:selected').attr('data-sequence')
        };
        setupMessageSequence(obj);
    });

    $(document).on('change', '.active_outline_id', function () {
        const change_active_outline = makeAjaxCall('tsl/change_active_outline', {
            'id': $(this).attr('value'),
            'component': 'message',
            'origin': origin
        });

        change_active_outline.then(function () {
            toastr.success('Default outline updated!!');
            activeOutlineId = 0;
            $('#view-promo-outline-dialog').modal('hide');
            refreshPage();
        });
    });

    // Save as new Message version.
    $('#save-as-new-msg-seq').click(function () {
        if (0 !== activeMessageId) {
            const save_as_new = makeAjaxCall('tsl/save_as_new_msg', {
                'msg_id': activeMessageId
            });

            save_as_new.then(function (data) {
                message_added = 1;
                refreshPage();
            })
        }
    });

    // Adds Promo version to archive list.
    $('#add-message-archive').click(function (data) {
        const add_promo_archive = makeAjaxCall('tsl/add_archive', {
            'promo_id': activeSeqId,
            'type': 'vmail',
            'asset_id': client_id,
            'datetime': moment().format("DD-MM-YY hh:mm A"),
            'topic': $('.topic option:selected').text()
        });

        add_promo_archive.then(function (data) {
            toastr.success('Component Added to Archive!!');
        });
    });

    // Collaboration View Click handler.
    $(document).on('click', '#collaborate-promo', function () {
        $('#component-container').toggleClass('col-xs-60 col-md-12 full-screen');
        $('#collab-button-row').toggleClass('col-xs-60 col-md-12');
        $('.full-view-hide').toggle();

        if (0 === $('.add_collab').length) {
            activeId = activeSeqId;

            $('.promo-row').siblings('span.tts')
                .before('<span class="input-group-addon btn btn-secondary add_collab">' +
                    '<i class="fa fa-plus"></i></span>')

            setupCollab();
        } else {
            $('.add_collab').remove();
            $('.collab_row').remove();
            $('.collaborator_row').remove();
        }
    });
});

// Fetches the list of message sequence.
function refreshPage() {

    const get_message_outline = makeAjaxCall('tsl/get_message_outline', {
        component: component_name

    });

    get_message_outline.then(function (data) {

        $('#promo-outline-table tbody tr').remove();
        const outlines = data.outlines.outline;
        let active_id = 1;

        // Check if active outline id is set.
        (0 !== data.outlines.active_id.length) ?
            active_id = data.outlines.active_id[0].tpao_outline_id : '';

        let content = '<table class="table"><thead>' +
            '<th class="text-center" style="margin: 5px">#</th>' +
            '<th class="text-center" style="margin: 5px">Version</th>' +
            '<th class="text-center" style="margin: 5px">Default</th>' +
            '<th class="text-center" style="margin: 5px"><span class="glyphicon glyphicon-trash"></span></th>' +
            '</thead>' +
            '<tbody>';

        $.each(outlines, function (index, outline) {
            $('#vmail-outline').append('<option value="' + outline.tmo_id + '">' + outline.tmo_name + '</option>')

            let disabled = '';

            content += '<tr><td class="text-center" style="margin: 5px">' + (index + 1) + '</td>'
                + '<td class="text-center" style="margin: 5px"><span class="editable" data-type="text" data-pk="' + outline.tmo_id + '" ' +
                'data-url="' + jsglobals.base_url + 'tsl/update_promo_outline_name' + '">'
                + outline.tmo_name + '</span><span><a class="promotion-outline" id="outline' + outline.tmo_id +
                '"' + ' data-value="' + index + '">' +
                '<i class="fa fa-eye" style="color: black"></i><a></span></td>'
                + '<td class="text-center" style="margin: 5px">' +
                '<input type="radio" class="active_outline_id" data-name="outline_id" ' +
                ' id="active' + outline.tmo_id + '"' +
                ' data-id="' + outline.tmo_id + '" name="active_outline" ' +
                ' value="' + outline.tmo_id + '"' + (active_id === outline.tmo_id ? "checked" : "") + '></td>'
                + '<td class="text-center" style="margin: 5px"><button class="btn btn-default btn-sm"' +
                ' data-toggle="modal" ' + disabled + '' +
                ' data-target="#delete-promo-outline-dialog"' +
                ' data-backdrop="static"' +
                ' data-id="' + outline.tmo_id +
                '" data-name="' + outline.tmo_name +
                '"> <span class="glyphicon glyphicon-trash"></span></button></td>';

            (('1' === outline.tmo_id)) ? disabled = 'disabled' : '';

            $('#promo-outline-table tbody')
                .append($('<tr>')
                    .append($('<td class="text-center">')
                        .append(index + 1))
                    .append($('<td class="text-center">')
                        .append($('<span class="editable" data-type="text" data-pk="' + outline.tmo_id + '" ' +
                            'data-url="' + jsglobals.base_url + 'tsl/update_promo_outline_name' + '">'
                            + outline.tmo_name + '</span><span><a class="promotion-outline" id="outline' + outline.tmo_id +
                            '"' + ' data-value="' + index + '">' +
                            '  <i class="fa fa-eye" style="color: black"></i></a></span>')))
                    .append($('<td class="text-center">')
                        .append($('<input type="radio" class="active_outline_id" data-name="outline_id" ' +
                            ' id="active' + outline.tmo_id + '"' +
                            ' data-id="' + outline.tmo_id + '" name="active_outline" value="' + outline.tmo_id + '"' + '>')))
                    .append($('<td class="text-center">')
                        .append($('<button class="btn btn-default btn-sm"' +
                            ' data-toggle="modal" ' + disabled +
                            ' data-name="' + outline.tmo_name + '"' +
                            ' data-target="#delete-msg-outline-dialog"' +
                            ' data-backdrop="static"' +
                            ' data-id="' + outline.tmo_id +
                            '"> <span class="glyphicon glyphicon-trash"></span>')))
                );
        });

        $('#view-msg-outline').attr('data-content', content);

        // Check active Outline Id.
        if (active_id.length !== 0) {
            $('#active' + active_id).attr('checked', true);
        }

        const message_outline_selector = $('#outline-container');

        if (activeOutlineId === 0) {

            let index = 0;
            if (active_id.length !== 0) {
                $('#outline' + active_id).closest('tr').addClass('active_outline');
                index = outlines.findIndex(x => x.tmo_id === active_id);
            } else {
                $('.promotion-outline').first().closest('tr').addClass('active_outline');
            }

            // Check if index is set to none.
            (-1 === index) ? index = 0 : '';
            message_outline_selector.html('');
            message_outline_selector.html('<h4 class="main-con-header">Message Outline</h4>' +
                '<form id="outline-form">' +
                '<input type="hidden" id="target_audience" name="target_audience" >' +
                '<input type="hidden" id="criteria" name="criteria" >' +
                '<input type="hidden" id="promo_focus" name="promo_focus">' +
                '<input type="hidden" id="promo_format" name="promo_format">' +
                '<input type="hidden" id="client_type" name="client_type">' +
                '<input id="y" value="' + outlines[index].tmo_content +
                '" type="hidden" name="outline_content">\n' +
                '<trix-editor id="trix-editor" class="editor" input="y" name="tsl_promo_editor"></trix-editor>' +
                '</form>');

            activeOutlineId = outlines[index].tmo_id;
        }

        // Outline Click handler.
        const promo_outline_selector = $('.promotion-outline');
        promo_outline_selector.off('click');

        // Display selected outline text in editor.
        promo_outline_selector.click(function () {
            $('.active_outline').removeClass('active_outline');
            $(this).closest('tr').addClass('active_outline');

            let content = outlines[$(this).attr('data-value')].tmo_content;

            message_outline_selector.html('<h4 class="main-con-header">Promo Outlines</h4><br>' +
                '<form id="outline-form">' +
                '<input type="hidden" id="target_audience" name="target_audience" >' +
                '<input type="hidden" id="criteria" name="criteria" >' +
                '<input type="hidden" id="promo_focus" name="promo_focus">' +
                '<input type="hidden" id="promo_format" name="promo_format">' +
                '<input type="hidden" id="client_type" name="client_type">' +
                '<input id="y" value="' + content +
                '" type="hidden" name="outline_content">\n' +
                '<trix-editor id="trix-editor" class="editor" input="y" name="tsl_promo_editor"></trix-editor>' +
                '</form>');

            obj = {
                "data-id": outlines[$(this).attr('data-value')].tmo_id,
                "data-content": outlines[$(this).attr('data-value')].tmo_content
            };

            activeOutlineId = outlines[$(this).attr('data-value')].tmo_id;
            // setupPromo(obj);
            trixButtons();
            $('#view-promo-outline-dialog').modal('hide')
            setupMessageList();
        });

        trixButtons();
        setupMessageList();
    });
}

function setupMessageList() {
    // Fetch the message version from DB.
    const get_message = makeAjaxCall('tsl/get_message', {
        tool_id: tsl_id,
        component_name: component_name,
        client_id: client_id,
        outline_id: activeOutlineId
    });

    // Append message version to list and fetch related message sequence.
    get_message.then(function (data) {
        updateMessage(data.message);
    });

    $(".edit-ref").children().click(function (event) {
        event.stopPropagation();
    });
}


// Updates the list of message sequence.
function updateMessageSeq(msg_seqs) {
    $('#delete-seq').removeAttr('data-toggle');

    $('#vmail-sequence').empty();

    $.each(msg_seqs, function (index, msg_seq) {
        $('#vmail-sequence')
            .append('<option data-sequence="' + msg_seq.tms_sequence_number + '" data-id="' + msg_seq.tms_id + '" ' +
                'data-name="' + msg_seq.tms_name + '">' + msg_seq.tms_name + '</option>');
    });

    // Checks if no message sequence is selected.
    const searchParams = new URLSearchParams(window.location.search),
        tsl_id = searchParams.get('id');

    // Search Parameters and see if this a request from LM.
    if (searchParams.has('seq')) {
        $('.msg_seq [data-id=' + searchParams.get('seq') + ']').click();
    } else if (msg_seqs.length !== 0 && activeSeqId === 0) {
        const obj = {
            "data-id": msg_seqs[0].tms_id,
            "data-name": msg_seqs[0].tms_name,
            "data-sequence": msg_seqs[0].tms_sequence_number
        };
        setupMessageSequence(obj);
    }
    // Checks if new message is added.
    else if (message_added === 1) {
        console.log('here');
        message_added = 0;
        // $('#message-form').html('');
        $('#msg-ver-nav li').last().find('a').click();
    }
    // Checks if new message sequence is added.
    else if (sequence_added === 1) {
        sequence_added = 0;
        $('#msg-seq-nav li').last().children()[0].click();
        $('#message-form').html('');
    }
    setEditable('bottom');
}

// Fetches message sequence.
function getMessageSeq(obj) {

    $('.delete_msg').off('click');
    $('.delete_msg').click(function () {
        $(this).attr('data-target').modal('show');
    });
    activeMessageId = $(obj).attr('data-id');
    $('.add-to-alert').attr('data-id', activeMessageId);

    activeSeqId = 0;

    // Sets attribute for delete button.
    const delete_msg_selector = $('#delete-msg');
    // Displaying the active email name.
    $('.active-msg').removeClass('active-msg');
    $('#msg' + activeMessageId).addClass('active-msg');

    // Check id active Message ID is set to Zero or not.
    if (0 !== parseInt(activeMessageId)) {
        // Set up data attribute for Delete Message Button.
        delete_msg_selector.attr('data-toggle', 'modal');
        delete_msg_selector.attr('data-target', '#delete-msg-dialog');
        delete_msg_selector.attr('data-backdrop', 'static');
        delete_msg_selector.attr('data-id', activeMessageId);
        delete_msg_selector.attr('data-name', $(obj).attr('data-name'));

        // Fetch Message Sequence for a message version.
        const get_message_seq = makeAjaxCall('tsl/get_message_seq', {
            'version_id': $(obj).attr('data-id')
        });

        get_message_seq.then(function (data) {
            updateMessageSeq(data.msg_seq);
        });
    } else {

        $('#msg-seq-nav').empty();
        // delete_msg_selector.removeAttr('data-toggle');
        $('#delete-seq').removeAttr('data-toggle');
        let example_version_array = [1, 2, 3, 4, 5];
        let default_version_name = 'Message';

        // Check Component and set version default version name.
        if ('Story' === component_name) {
            default_version_name = 'Story';
        } else if ('Follow-Up' === component_name) {
            default_version_name = 'Follow-Up';
        } else if ('Debriefing' === component_name) {
            default_version_name = 'Debriefing';
        }

        $('#vmail-sequence').empty();
        // Append Dummy Version to the List.
        $.each(example_version_array, function (index, val) {
            $('#vmail-sequence')
                .append('<option data-sequence="' + val + '" data-id="exam' + val + '">Example Sequence ' + val + '</option>')
        });

        // Checks if no message sequence is selected.
        const searchParams = new URLSearchParams(window.location.search),
            tsl_id = searchParams.get('id');

        // Search Parameters and see if this a request from LM.
        if (searchParams.has('ver') && searchParams.has('seq')) {
            $('.msg_ver [data-id=' + searchParams.get('ver') + ']').click();
            $('.msg_seq [data-id=' + searchParams.get('seq') + ']').click();
        } else {
            $('#vmail-sequence').trigger('change');
        }
    }
}

// Setup message list and delete sequence.
function setupMessageSequence(obj) {
    // Check if this click event is triggered.
    // false === isTriggered ? activeMessageId = 0 : '';

    isTriggered = false;
    activeSeqId = $(obj).attr('data-id');

    // Check if the Default Sequence is selected.
    if (0 !== parseInt(activeMessageId)) {
        // Set attribute for delete button.
        const delete_seq_selector = $('#delete-seq');
        delete_seq_selector.attr('data-toggle', 'modal');
        delete_seq_selector.attr('data-target', '#delete-seq-dialog');
        delete_seq_selector.attr('data-backdrop', 'static');
        delete_seq_selector.attr('data-id', activeSeqId);
        delete_seq_selector.attr('data-name', $(obj).attr('data-name'));
    }

    $('.active-seq').removeClass('active-seq');
    $('#seq' + activeSeqId).addClass('active-seq');

    setupMessage(obj);
}

// Updates message list.
function updateMessage(msgs) {
    let obj;
    $('#msg-ver-nav').empty();

    // Remove toggle data attribute for delete button.
    $('#message-form').html('');

    // Example Message Version.
    $('#msg-ver-nav').append($('<li class="text-center" id="msg0">')
        .append(
            '<a href="#" onclick="getMessageSeq(this); return false;" data-id="0" class="msg_ver">' +
            '<span style="float: left; color: black" onclick="getMessageSeq(this); return false;" ' +
            'data-id="0"><i class="fa fa-eye"></i></span>' +
            'Example Version' +
            '<span style="float: right; color: black" onclick="getMessageSeq(this); return false;" ' +
            'data-id="0">' +
            '<i class="fa fa-trash"></i></span></a></li>'))

    // Updates the list of message sequence.
    $.each(msgs, function (index, msg) {
        $('#msg-ver-nav').append($('<li class="text-center" id="msg' + msg.tm_id + '">')
            .append('<a href="#" class="edit-ref msg_ver" onclick="getMessageSeq(this); return false;" ' +
                'data-name="' + msg.tm_name + '" data-id="' + msg.tm_id + '">' +
                '<span style="float: left; color: black" onclick="getMessageSeq(this); return false;" ' +
                'data-name="' + msg.tm_name + '" data-id="' + msg.tm_id + '">' +
                '<i class="fa fa-eye"></i></span>' +
                '<span style="float: right; color: black" data-target="#delete-msg-dialog" data-toggle="modal"' +
                ' data-name="' + msg.tm_name + '" data-id="' + msg.tm_id + '" class="delete_msg">' +
                '<i class="fa fa-trash"></i></span>' +
                '<span class="editable" ' +
                'data-type="text" data-pk = "' + msg.tm_id + '" ' +
                'data-url = "' + jsglobals.base_url + 'tsl/update_msg_name">' + msg.tm_name + '</span>' +
                '</a></li>'))
    });

    // Checks if no message is selected.
    if (0 === parseInt(activeMessageId)) {
        obj = {"data-id": 0, "data-name": 'Example'};
        getMessageSeq(obj);
    } else if (sequence_added === 1) {
        const index = msgs.findIndex(x => x.tm_id === activeMessageId);
        sequence_added = 0;
        obj = {'data-id': activeMessageId, 'data-name': msgs[index].tm_name};
        getMessageSeq(obj);
    }

    setEditable('bottom');
}

// Setup message content and delete button.
function setupMessage(obj) {
    const get_message_content = makeAjaxCall('tsl/get_message_content', {
        'id': $(obj).attr('data-id')
    });

    get_message_content.then(function (data) {
        const message_container_selector = $('#component-container');
        message_container_selector.html('');
        message_container_selector.html('<h4 class="main-con-header" style="text-transform:capitalize;">' +
            component_name + ' Composition</h4>' +
            '<form id="message-form">' +
            '</form>');

        if (0 === data.data.length) {
            const seq_number = ($(obj).attr('data-sequence') > 5) ? 5 : $(obj).attr('data-sequence');
            const get_default_seq = makeAjaxCall('tsl/get_default_seq', {
                'seq': seq_number,
                'component': component_name
            });

            get_default_seq.then(function (data) {
                setContent($(obj).attr('data-sequence'), {
                    'tmc_content': '',
                    'tmc_id': 0
                }, data.default_seq, 5, 'Default');
            });
        } else {
            setContent($(obj).attr('data-sequence'), {
                'tmc_content': '',
                'tmc_id': 0
            }, data.data, 5, '');
        }
    });

    // Makes the Name field content editable.
    $(".edit-ref").children().click(function (event) {
        event.stopPropagation();
    });
}

// Set the content for middle panel.
function setContent(index, content, data, total_message, type) {

    let textarea_content = [], labels = [], form_heading = 'Message', col_names = [];

    // Check if the content type is set to default.
    if ('Default' === type) {
        $.each(data, function (index, data) {
            textarea_content.push(data.tmd_text);
        });
    } else {
        $.each(data, function (index, seq_data) {
            textarea_content.push(seq_data.tmc_content);
        });
    }

    col_names.push('intro');
    col_names.push('reason');
    col_names.push('message');
    col_names.push('invitation');
    col_names.push('promise');

    // Check if the component is story or message.
    if ('Story' === component_name) {
        form_heading = 'Deal Story';

        labels.push('Step 1: What happened, to whom and when did it happen?');
        labels.push('Step 2: Why did this happen and how did it happen?');
        labels.push('Step 3: What did you recommend and what did the client choose to do?');
        labels.push('Step 4: What was the overall impact on the client?');
        labels.push('Step 5: Where did the client end up as a result of their choice?');
    } else if ('Follow-Up' === component_name) {
        form_heading = 'Follow-Up';

        labels.push('Introduction');
        labels.push('Reason Why');
        labels.push('Message');
        labels.push('Invitation');
        labels.push('Promise');
    } else if ('Debriefing' === component_name) {
        form_heading = 'Debriefing';

        labels.push('Introduction');
        labels.push('Reason Why');
        labels.push('Message');
        labels.push('Invitation');
        labels.push('Promise');
    }
    else {
        labels.push('Introduction');
        labels.push('Reason Why');
        labels.push('Message');
        labels.push('Invitation');
        labels.push('Promise');
    }

    $('#message-form').append('<div id="main-form-content" style="margin:2%">' +
        $('#y').val().replace(/}}[\s\S]*?{{/g, '}}{{')
            .replace(/{{/g, '<label style="padding-top: 10px;">').replace(/}}/g, '</label>\n' +
            '<div class="input-group">' +
            '<span class="input-group-addon btn btn-secondary tts"><i class="fa fa-bullhorn"></i></span>' +
            '<textarea class="form-control promo-row" rows="2" cols="70" onkeyup="auto_grow(this)"' +
            'id="ans' + Date.now() + '" ' +
            'placeholder="Click Microphone icon and speak or start typing."></textarea>' +
            '<span class="input-group-addon btn btn-secondary stt">' +
            '<i class="fa fa-microphone"></i></span>' +
            '<span id=\'span_ans\' class=\'hidden promo_ans\' name="ans"></span></div>'))
    // .replace(/<\/strong>[\s\S]*?<\/div>/, '</div>') + '</div>');

    $.each($('#message-form textarea'), function (index, text) {
        $(text).val(textarea_content[index]).attr('data-id', index + 1).attr('id', 'ans' + (index + 1));
    });


    $('#message-form').append($('<div id="condensed-content" style="margin:2%" class="hidden">' +
        '<h3><b>' + form_heading + ' ' + index + '</b></h3>' +
        '<div class="input-group">' +
        '<span class="input-group-addon btn btn-secondary tts">' +
        '<i class="fa fa-bullhorn"></i></span>' +
        '<textarea onkeyup="auto_grow(this)"  rows="10" class="form-control" id="condensed-text" ' +
        '></textarea>' +
        '</div>' +
        '</div>'));

    // Display Message sequenece in the middle panel.
    $('#view-msg').off('click');
    $('#view-msg').click(function (e) {

        // Setup content for promo reading view.
        // $('#read-promo').off('click');
        // $('#read-promo').click(function () {
        $('#read-promo-dialog').modal('show');
        condensed_content = '';
        $.each($('#message-form textarea'), function (index, text) {
            condensed_content = condensed_content + $(text).val() + '\n';
        });

        let obj = {"data-content": condensed_content};

        // $('#condensed-text').val(condensed_content);

        $('#email-container').html('');
        // Check if Send Email editor is up. If so remove it.
        (null !== tinymce.get('email-container')) ? tinymce.get('email-container').remove() : '';

        // Check if archive Archive editor is up. If so remove it.
        (null !== tinymce.get('archive-viewer-container')) ? tinymce.get('archive-viewer-container').remove() : '';
        initEditor(obj);

        // Set the content for Editor.
        tinymce.activeEditor.setContent($(obj).attr('data-content'));
    });

    // if ($('#condensed-content').hasClass('hidden')) {
    //     condensed_content = '';
    //     $.each($('#message-form textarea'), function (index, text) {
    //         condensed_content = condensed_content + $(text).val() + '\n';
    //     });
    //
    //     $('#condensed-text').val(condensed_content);
    // }
    //
    // // Hide Save Button.
    // $('#save-msg-seq').toggleClass('hidden');
    // $('#clear-msg').toggleClass('hidden');
    // $('#main-form-content').toggleClass('hidden');
    // $('#condensed-content').toggleClass('hidden');
    // });

    // Check if Save Message Sequence button has class hidden.
    if ($('#save-msg-seq').hasClass('hidden')) {
        // Hide Save Button.
        $('#main-form-content').toggleClass('hidden');
        $('#condensed-content').toggleClass('hidden');
    }

    // Clears out Message Text area.
    $('#clear-msg').off('click');
    $('#clear-msg').click(function (e) {

        // Check if default version is being displayed.
        if (0 !== activeMessageId) {
            $.each($('#message-form textarea'), function (index, text) {
                $(this).val('');
            });
        }
    });

    voiceRecognition();
    speechFunctionality();
}