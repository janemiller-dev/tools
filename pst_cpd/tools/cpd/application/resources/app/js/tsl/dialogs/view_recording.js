$(document).ready(function () {
    // Fetches the performance score.
    $('#view-recording-dialog').on('show.bs.modal', function (event) {
        view_recording.init();
    });

    // Make Editable box clickable.
    $('#view-recording-dialog').on('shown.bs.modal', function () {
        $(document).off('focusin.modal');
    });
});

let view_recording = function () {
    // Initialize and get the DMD ID.
    let init = () => {
        const get_recording = makeAjaxCall('tsl/get_recording', {});

        // Append data to recording table.
        $('#recording-table tbody').empty()
        get_recording.then(function (data) {
            $.each(data.recording, function (index, recording) {
                const url = window.location.origin + '/tools/tsl/play_recording?name=' + recording.tsr_name;

                $('#recording-table').append('<tr><td class="text-center">' + (index + 1) + '</td>' +
                    '<td class="text-center"><span class="editable" data-type="text" ' +
                    'data-pk="' + recording.tsr_id + '" data-url="' + jsglobals.base_url +
                    'tsl/update_recording_name">' + recording.tsr_given_name + '</span></td>' +
                    '<td class="text-center">' +
                    '<audio class="player_audio hidden" src="' + url + '"></audio>' +
                    '<span class="btn btn-default btn-sm play-recording">' +
                    '<i class="fa fa-play play_icon"></i></span></td>' +
                    '<td class="text-center"><span class="btn btn-default btn-sm delete-recording"' +
                    ' data-id="' + recording.tsr_id + '" data-name="' + recording.tsr_name + '">' +
                    '<i class="fa fa-trash"></i></span></td></tr>');
            });

            // Makes the names editable.
            setEditable('bottom');
            $(".edit-ref").children().click(function (event) {
                event.stopPropagation();
            });
        });

        // recording Play button click handler.
        $(document).on('click', '.play-recording', function () {

            // Check if the audio is playing.
            if ($(this).children('i').hasClass('fa-play')) {
                $('.play_icon').removeClass('fa-pause').addClass('fa-play');
                $('.player_audio').trigger('pause');

                // Trigger audio click button.
                $(this).siblings('.player_audio').trigger('play');
                $(this).children('i').removeClass('fa-play').addClass('fa-pause');
            } else {

                // Trigger audio pause option.
                $(this).siblings('.player_audio').trigger('pause');
                $(this).children('i').removeClass('fa-pause').addClass('fa-play');
            }
        });


        $(document).on('click', '.delete-recording', function () {
            const delete_recording = makeAjaxCall('tsl/delete_recording', {
                id: $(this).attr('data-id'),
                name: $(this).attr('data-name')
            });

            delete_recording.then(function (data) {
                toastr.success('Recording Deleted!!', 'Success');
                $('#view-recording-dialog').modal('hide');
            });
        })

    };
    return {
        init: init
    };
}();

// <span class="input-group-addon btn btn-secondary stt"><i class="fa fa-microphone"></i></span>