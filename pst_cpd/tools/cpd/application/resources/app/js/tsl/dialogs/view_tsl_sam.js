$(document).ready(function () {
    // Fetches the performance score.
    $('#view-tsl-sam-dialog').on('show.bs.modal', function (event) {
        view_sam_data.init();
    });
});


let view_sam_data = function () {

    // Initialize and get the DMD ID.
    let init = () => {
        const get_sam_data = makeAjaxCall('tsl/get_sam_data', {
                tsl_id: tsl_id
            }),
            dummy_sessions = '<p>Call Session 1 - 00:00 am – 00:00 pm</p>' +
                '<p>Call Session 2 - 00:00 am – 00:00 pm</p>' +
                '<p>Call Session 3 - 00:00 am – 00:00 pm</p>',
            dummy_dials = '<p>0</p>' +
                '<p>0</p>' +
                '<p>0</p>',
            dummy_contact = '<p>0</p>' +
                '<p>0</p>' +
                '<p>0</p>',
            dummy_conversation = '<p>0</p>' +
                '<p>0</p>' +
                '<p>0</p>',
            dummy_meetings = '<p>0</p>' +
                '<p>0</p>' +
                '<p>0</p>';


        get_sam_data.then(function (data) {
                $('#sam_row').empty();

                const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
                let sam_data = [];

                $.each(days, function (index, day) {
                    sam_data[day] = data.sam_data[day];
                });

                for (key in sam_data) {

                    if (sam_data[key] === undefined) {
                        $('#sam_row').append(
                            '<div class="col-xs-12" style="border: 1px solid #e1c8c8; margin-top: 2px; padding-top: 10px">' +
                            '<div class="col-xs-4">' +
                            '<p><b>' + key + '</b></p>' +
                            dummy_sessions +
                            '</div>' +
                            '<div class="col-xs-2 text-center">' +
                            '<p><b>Dials</b></p>' +
                            dummy_dials +
                            '</div>' +
                            '<div class="col-xs-2 text-center">' +
                            '<p><b>Contacts</b></p>' +
                            dummy_contact +
                            '</div>' +
                            '<div class="col-xs-2 text-center">' +
                            '<p><b>Conversations</b></p>' +
                            dummy_conversation +
                            '</div>' +
                            '<div class="col-xs-2 text-center">' +
                            '<p><b>Meetings</b></p>' +
                            dummy_meetings +
                            '</div>' +
                            '</div>'
                        )
                    } else {
                        let session_col = '',
                            dials_col = '',
                            contact_col = '',
                            conv_col = '',
                            meeting_col = '';

                        $.each(sam_data[key], function (index, sam) {
                            session_col += '<p>Call Session ' + (index + 1) +
                                ' - ' + sam.ts_start_time + ' – ' + sam.ts_end_time + '</p>';
                            dials_col += '<p>' + sam.ts_dials + '</p>';
                            contact_col += '<p>' + sam.ts_contact + '</p>';
                            conv_col += '<p>' + sam.ts_conv + '</p>'
                            meeting_col += '<p>' + sam.ts_meet + '</p>'
                        });

                        $('#sam_row').append(
                            '<div class="col-xs-12" style="border: 1px solid #e1c8c8; margin-top: 2px;' +
                            ' padding-top: 10px">' +
                            '<div class="col-xs-4">' +
                            '<p><b>' + key + '</b></p>' +
                            session_col +
                            '</div>' +
                            '<div class="col-xs-2 text-center">' +
                            '<p><b>Dials</b></p>' +
                            dials_col +
                            '</div>' +
                            '<div class="col-xs-2 text-center">' +
                            '<p><b>Contacts</b></p>' +
                            contact_col +
                            '</div>' +
                            '<div class="col-xs-2 text-center">' +
                            '<p><b>Conversations</b></p>' +
                            conv_col +
                            '</div>' +
                            '<div class="col-xs-2 text-center">' +
                            '<p><b>Meetings</b></p>' +
                            meeting_col +
                            '</div>' +
                            '</div>'
                        )
                    }
                }
            }
        );
    };

    return {
        init: init
    };
}();