$(document).ready(function () {
    let client_id, client_email, promo_id, type, random_name, origin, pdf_id, content_id,
        path = window.location.pathname,
        components = path.split('/');

    client_id = components[components.length - 1];

    // When the dialog is displayed, set the current instance ID.
    $('#send-promo-dialog').on('show.bs.modal', function (event) {
        $('#read-promo-dialog').modal('hide');
        tinymce.remove("#email-content");

        promo_id = $(event.relatedTarget).data('id');
        type = $(event.relatedTarget).data('type');
        content_id = $(event.relatedTarget).data('content-id');

        random_name = $(event.relatedTarget).data('random');
        origin = $(event.relatedTarget).data('origin');

        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('type');
        $(event.relatedTarget).removeData('random');
        $(event.relatedTarget).removeData('content-id');

        // Gets the client email.
        $.ajax({
            url: jsglobals.base_url + 'tsl/get_client_email',
            dataType: 'json',
            type: 'post',
            data: {
                client_id: client_id,
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            $('#promo-client-email').text(data.client[0].tc_email);

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });

        tinymce.init({
            selector: '#email-content',
            branding: false,
            init_instance_callback: function (ed) {
                tinyMCE.DOM.setStyle(tinyMCE.DOM.get('email-content_ifr'), 'height', '55vh');
            }
        });

        // Embed text to text area of Text editor.
        const embed_content_selector = $('#embed-content');
        embed_content_selector.unbind('click');
        embed_content_selector.click(function () {
            let condensed_content = '';
            $.each($('#compose-promo-form textarea'), function (index, text) {
                condensed_content = condensed_content + '<p>' + $(text).val() + '</p>';
            });

            const content = tinyMCE.activeEditor.getContent() +
                condensed_content;

            tinyMCE.activeEditor.setContent(content);
        });

        pdf_id = 0;
        $('#attachment_span').addClass('hidden');
    });

    $(document).on('click', '#close-send-promo-dialog', function () {
        $('#send-promo-dialog').modal('hide');
    });

    $(document).on('click', '#add-recipient', function () {
        $('.client-email')
            .after('<div class="col-xs-9 col-xs-offset-3" style="padding:0; margin-top:5px !important">' +
                '<input type="email" class="form-control promo-client-email"></div>');
    });

    // Click handler for attach PDF button.
    $('#attach-pdf').click(function () {
        pdf_id = $('.active_pdf').attr('data-id');
        $('#attachment_name').text(' ' + $('.active_pdf').text());
        $('#attachment_span').removeClass('hidden');
    });

    // Sends the promotional document.
    $(document).on('submit', '#send-promo-form', function (ev) {
        let email_array = [];
        $('.promo-client-email').each(function(){
            ('' !== $(this).val()) ? email_array.push($(this).val()) : '';
        });

        enableLoader();
        ev.preventDefault();
        ev.stopPropagation();

        // Get the text from editor.
        const text = tinymce.activeEditor.getContent();
        $.ajax({
            url: jsglobals.base_url + 'tsl/send_attachment',
            dataType: 'json',
            type: 'post',
            data: {
                client_id: client_id,
                client_email: email_array,
                id: promo_id,
                tool_id: window.location.search.split('?id=')[1],
                type: type,
                origin: origin,
                random: random_name,
                subject: $('#promo-client-subject').val(),
                text: text,
                pdf_id: pdf_id,
                content_id: content_id
            },
            error: ajaxError
        }).done(function (data) {
            $.unblockUI();

            if (data.status != 'success') {
                toastr.error(data.message, 'Invalid Email ID')
                return;
            }
            else if (data.status == 'redirect') {

                window.location.href = data.redirect;
                return;
            }

            toastr.success('Email Sent!!');
            $('#send-promo-dialog').modal('hide');
            $('#promo-text').val('');
        }).fail(function (jqXHR, textStatus) {
            $.unblockUI();
            toastr.error('Please check EMail ID provided.','Unable to Send Email!!');
            return false;
        }).always(function () {
        });

        return false;
    })
});
