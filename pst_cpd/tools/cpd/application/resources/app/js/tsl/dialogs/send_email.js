$(document).ready(function () {
    let client_email, email_id, client_name, path = window.location.pathname, components = path.split('/');

    $(document).on('click', '#add-recipient', function () {
        $('.client-email')
            .after('<div class="col-xs-9 col-xs-offset-3" style="padding:0; margin-top:5px !important">' +
                '<input type="email" class="form-control email-client-email"></div>');
    });

    // When the dialog is displayed, set the current instance ID.
    $('#send-email-dialog').on('show.bs.modal', function (event) {
        toastr.info('Please format email with headings and paragraphs before sending.');

        email_id = $(event.relatedTarget).data('email');
        // Get client email ID.
        $.ajax({
            url: jsglobals.base_url + 'tsl/get_client_email',
            dataType: 'json',
            type: 'post',
            data: {
                client_id: client_id,
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            $('#client-email').text(data.client[0].tc_email);
            client_name = data.client[0].tc_name;

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });

        let email_text = '';
        $.each($('#response-form textarea'), function (index, textarea) {
            email_text += '<p>' + $(textarea).val() + '</p>';
        });

        tinymce.init({
            selector: '#content-container',
            branding: false,
            init_instance_callback: function (ed) {
                tinyMCE.DOM.setStyle(tinyMCE.DOM.get('content-container_ifr'), 'height', '55vh');
                tinyMCE.activeEditor.setContent(email_text.replace(/\\/g, ''));
            }
        });

    });

    $(document).on('click', '#close-send-email-dialog', function () {
        $('#send-email-dialog').modal('hide');
    });

    // Send the email content.
    $(document).on('submit', '#send-email-form', function (ev) {
        ev.preventDefault();
        ev.stopPropagation();

        let email_array = [];
        $('.email-client-email').each(function(){
            ('' !== $(this).val()) ? email_array.push($(this).val()) : '';
        });

        enableLoader();

        $.ajax({
            url: jsglobals.base_url + 'tsl/send_email',
            dataType: 'json',
            type: 'post',
            data: {
                client_id: client_id,
                client_email: email_array,
                client_name: client_name,
                content: tinymce.activeEditor.getContent(),
                id: email_id,
                subject: $('#subject').val(),
                origin: origin,
                type: 'Email',
                tool_id: window.location.search.split('?id=')[1]
            },
            error: ajaxError
        }).done(function (data) {
            $.unblockUI();

            if (data.status !== 'success') {
                console.log(data.message);
                return;
            }
            else if (data.status === 'redirect') {

                window.location.href = data.redirect;
                return;
            }
            $('#send-email-dialog').modal('hide');

            toastr.success('Email Sent!!');
        }).fail(function (jqXHR, textStatus) {
            $.unblockUI();
            toastr.error('Please check EMail ID provided.', 'Unable to Send Email!!');
            return false;
        }).always(function () {
        });

        return false;
    })
});
