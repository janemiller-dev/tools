// Form validation.
$(document).ready(function () {

    $('#add-criteria-category-form').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh',
        },
        fields: {
            criteria_category_name: {
                validators: {
                    notEmpty: {
                        message: "Category name is required."
                    }
                }
            }
        }
    }).on('success.form.fv', function (e) {
        e.preventDefault();
        // Get the add new Criteria Category form.
        var $form = $(e.target);
        var fv = $form.data('formValidation');
        // Get the form data and submit it.
        var form_data = $form.serializeArray();
        form_data.push({name: 'tsl_id', value: tsl_id});

        $.ajax({
            url: jsglobals.base_url + 'tsl/add_criteria_category',
            dataType: 'json',
            type: 'post',
            data: form_data,
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            } else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('Criteria Category Added', 'Success!!');
            fv.resetForm();
            $($form)[0].reset();

            $('#add-criteria-category-dialog').modal('hide');
            // $('#add-criteria-dialog').modal('show');
            refreshDashboard();

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });
});

