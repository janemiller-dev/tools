$(document).ready(function () {
    var action_id = '';
    var ui_name = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-action-dialog').on('show.bs.modal', function (event) {
        action_id = $(event.relatedTarget).data('id');
        ui_name = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name')

        $('#action-name').html(ui_name);
        $('#view-action-dialog').modal('hide');
    });

    $(document).on('click', '#close-delete-action-dialog', function () {
        $('#delete-action-dialog').modal('hide');
    });

    // Deletes the action name.
    $(document).on('submit', '#delete-action-form', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_action',
            dataType: 'json',
            type: 'post',
            data: {
                action_id: action_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            toastr.success('Action Deleted', 'Success');
            $('#delete-action-dialog').modal('hide');
            refreshDashboard();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
        return false;
    });
});
