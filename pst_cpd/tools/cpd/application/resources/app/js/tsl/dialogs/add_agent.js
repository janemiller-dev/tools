$(document).ready(() => {
    let client_deal_id = '';

    // Adds form validation.
    addAgent.validateForm();

    // On Pop up shown event.
    $('#add-agent-dialog').on('show.bs.modal', function (event) {
        addAgent.init(event);
    });
});

var addAgent = (() => {

    let id,
        init = (event) => {
            $('#view-agent-dialog').modal('hide');
            id = $(event.relatedTarget).data('id');
        },

        // Validates a Form.
        validateForm = function () {

            // Form validation
            $('#add-agent-form').formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh',
                },
                fields: {
                    agent_name: {
                        validators: {
                            notEmpty: {
                                message: "The agent name is required."
                            }
                        }
                    }
                }
            }).on('success.form.fv', function (e) {
                e.preventDefault();

                // Get agent form.
                var $form = $(e.target);
                var fv = $form.data('formValidation');

                // Get the form data.
                var agent_data = $form.serializeArray();

                // Check if TSL ID is empty.
                ('undefined' === typeof(tsl_id)) ? tsl_id = id : '';

                agent_data.push({name: 'tsl_id', value: tsl_id});

                let add_new_agent = makeAjaxCall('tsl/add_agent', agent_data);

                // Ajax call to add agent.
                add_new_agent.then(function (data) {
                    fv.resetForm();
                    $($form)[0].reset();
                    $('#add-agent-dialog').modal('hide');
                    toastr.success('Preparer Added', 'Success!!');
                    var refresh = true;
                    refreshDashboard();
                });
            });
        };

    return {
        init: init,
        validateForm: validateForm
    };
})();
