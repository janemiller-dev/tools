$(document).ready(function () {
    var background_id = '';
    var ui_name = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-background-dialog').on('show.bs.modal', function (event) {
        background_id = $(event.relatedTarget).data('id');
        ui_name = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name')

        $('#background-name').html(ui_name);
        $('#view-background-dialog').modal('hide');
    });

    $(document).on('click', '#close-delete-background-dialog', function () {
        $('#delete-background-dialog').modal('hide');
    });

    // Deletes the background name.
    $(document).on('submit', '#delete-background-form', function (e) {
        e.preventDefault();
        e.stopPropagation();

        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_background_Template',
            dataType: 'json',
            type: 'post',
            data: {
                background_id: background_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            toastr.success('Background Template Deleted.', 'Success!!');
            $('#delete-background-dialog').modal('hide');
            var promo_added = 1;
            refreshPage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
        return false;
    });
});
