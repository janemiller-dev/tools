$(document).ready(function ()
{
    var ui_name, script_id = '';

    // When the dialog is displayed, set the current instance ID.
    $('#delete-script-dialog').on('show.bs.modal', function (event) {

        script_id = $(event.relatedTarget).data('id');
        ui_name = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name')

        $('#script-name').html(ui_name);
    });

    // Removes the selected script instance.
    $(document).on('submit', '#delete-script-form', function (e) {
        e.preventDefault();
        e.stopPropagation();
        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_script',
            dataType: 'json',
            type: 'post',
            data: {
                script_id: script_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            toastr.success('Script Deleted!!', 'Success!!');
            refreshPage();
            $('#delete-script-dialog').modal('hide');

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
            // refreshPage();
        });
        return false;
    });
});
