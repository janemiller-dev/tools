let activeOutlineId, activeSurveyId, outline_added, survey_added, questions_id = [],
    client_id, obj, ref, outlines, requester;

$(document).ready(function () {

    // Get the Client ID from the URL
    const path = window.location.pathname,
        components = path.split('/'),
        origin = components[2];
    requester = components[3];

    ref = window.location.search.split('?ref=')[1];

    // Check if ref is defined or not.
    if (undefined !== ref) {
        const ref_array = ref.split('?');
        ref = ref_array[0];
    }
    client_id = components[components.length - 1];

    // Initialize voice rrecgnisation Functionality.
    voiceRecognition();

    const id = window.location.search.split('?id=')[1];
    loadPrev();

    activeOutlineId = 0;
    activeSurveyId = -1;

    // Display outline for the survey.
    $('#outline-container').html('');
    $('#canvas-container').html('');

    // To check if a new survey is added.
    outline_added = 0;
    activeOutlineId = 0;

    // List of Buyer's Survey
    const container_listing_selector = $('#container-listing');
    container_listing_selector.html('');
    container_listing_selector.html(
        ' <h4 align="center" class="main-con-header">' + $('#survey-class option:selected').text() + ' Version</h4>' +
        ' <ul class="nav" id="tsl-bus-nav">\n' +
        ' </ul>');

    // List of Buyer's Survey
    // $(document).on('change', container_listing_selector, function () {
    //     console.log('here');
    //     container_listing_selector.html('');
    //     container_listing_selector.html(
    //         ' <h4 align="center" class="main-con-header">' +
    //         $('#survey-class option:selected').text() + ' Version</h4>' +
    //         ' <ul class="nav" id="tsl-bus-nav">\n' +
    //         ' </ul>');
    // });

    refreshBusPage();


    // Display selected outline text in editor.
    $(document).on('click', '.survey-outline', function () {

        // outline_container_selector.html('');
        // outline_container_selector.html('<h4 class="main-con-header">Survey outline</h4><form id="outline-form">' +
        //     '<input type="hidden" id="outline_type" name="type">' +
        //     '<input type="hidden" id="outline_id" name="outline_id">' +
        //     '<input type="hidden" id="survey_class" name="class">' +
        //     '<input type="hidden" id="client_type" name="client">' +
        //     '<input type="hidden" id="requester" name="requester">' +
        //     '<input id="y" value="' + $('#survey-phase option:selected').data('value') + '" type="hidden" name="outline_content">\n' +
        //     '<trix-editor input="y" name="tsl_bus_editor"></trix-editor>' +
        //     '</form>');

        activeOutlineId = $(this).attr('data-id');
        get_phase();

        // Set the Outline Id to local storage.
        localStorage.setItem('active_survey_outline_id', activeOutlineId);
        // obj = {"data-id": outlines[$(this).data('val')].tso_id};
        // activeSurveyId = ref > 0 ? ref : 0;

    });

    // Creates a new instance of survey based on survey outline.
    $('#compose-bus').click(function (e) {
        e.preventDefault();
        survey_added = 1;

        $.ajax({
            url: jsglobals.base_url + 'tsl/add_survey',
            dataType: 'json',
            data: {
                outline_id: activeOutlineId
            },
            type: 'post',
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error('Please create one using save button on the left.', 'No instance of outline selected!!');
                return;
            } else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            refreshBusPage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    });

    // Add ability to focus on elements inside x-editable.
    $('#view-outline-dialog').on('shown.bs.modal', function () {
        $(document).off('focusin.modal');
    });

    // Add new Outline.
    $('#add-outline').off('click');
    $('#add-outline').click(function (data) {
        const add_outline = makeAjaxCall('tsl/add_survey_outline', {
            client_type: $('#client-type').val(),
            survey_type: $('#survey-class').val(),
        });

        add_outline.then(function (data) {
            toastr.success('Survey Outline Added!!', 'Success');
            refreshBusPage();
        })
    });

    // Saves the outline and generates questions from outlines.
    $('#save-bus-outline').click(function (e) {
        if ('0' === activeOutlineId) {
            toastr.error('You can\'t update default Outline.', 'Error!!');
        } else {
            outline_added = 0;
            activeOutlineId = 0;
            $('#survey_phase').val($('#survey-phase').val());

            const tsl_update_data = $('#outline-form').serializeArray();

            // Push question ID to the form data.
            tsl_update_data
                .push({name: 'ques_id', value: questions_id});

            e.preventDefault();

            $.ajax({
                url: jsglobals.base_url + 'tsl/update_phase_content',
                dataType: 'json',
                type: 'post',
                data: tsl_update_data,
                error: ajaxError
            }).done(function (data) {
                if (data.status !== 'success') {
                    toastr.error(data.message);
                    return;
                }
                else if (data.status === 'redirect') {
                    window.location.href = data.redirect;
                    return;
                }
                toastr.success('Outline Saved!');
                refreshBusPage();
            }).fail(function (jqXHR, textStatus) {
                toastr.error("Request failed: " + textStatus);
            }).always(function () {
            });
        }
    });

    // Saves client response.
    $('#save-bus').click(function (e) {

        // Check if the default survey is selected.
        if (0 === parseInt(activeSurveyId)) {
            toastr.error('To create one use compose button.', 'No Instance of survey.');
            return;
        }
        else {
            const data = $('#response-form').serializeArray();
            data.push({name: 'question_id', value: questions_id});
            data.push({name: 'client_id', value: client_id});
            data.push({name: 'survey_id', value: activeSurveyId});
            data.push({name: 'source', value: 'user'});
            e.preventDefault();

            $.ajax({
                url: jsglobals.base_url + 'survey/set_ans',
                dataType: 'json',
                type: 'post',
                data: data,
                error: ajaxError
            }).done(function (data) {
                if (data.status !== 'success') {
                    toastr.error(data.message);
                    return;
                }
                else if (data.status === 'redirect') {
                    window.location.href = data.redirect;
                    return;
                }
                toastr.success('Response Saved!');
                refreshBusPage();
            }).fail(function (jqXHR, textStatus) {
                toastr.error("Request failed: " + textStatus);
            }).always(function () {
            });
        }
    });


    $(document).on('change', '#survey-phase', function () {
        localStorage.setItem('active_survey_phase', $(this).val());
        put_content(outlines);
    });

    // Check if any drop down menu is changed, fetch outline version again.
    $('.survey-menu').change(function () {
        activeOutlineId = 0;
        const container_listing_selector = $('#container-listing');

        container_listing_selector.html('');
        container_listing_selector.html(
            ' <h4 align="center" class="main-con-header">' +
            $('#survey-class option:selected').text() + ' Version</h4>' +
            ' <ul class="nav" id="tsl-bus-nav">\n' +
            ' </ul>');

        refreshBusPage();
    });

    // Checks if no instance of survey is present.
    $('#send-bus, #delete-bus').click(function () {
        if ($(this).attr('data-toggle') === undefined) {
            toastr.error('To create one use compose button.', 'No Instance of survey.');
        }
    });

    // Buyer Survey Full Screen View.
    const expand_bus_selector = $('#expand-bus');
    expand_bus_selector.unbind('click');
    expand_bus_selector.click(function (e) {
        $('.survey_container').toggleClass('col-md-7', 'col-md-12');
        $('.full_view_hide').toggle();
        $('#canvas-container').toggleClass('full-screen');
        $('#response-form div').toggleClass('container');

        const expand_bus_selector = $('#expand-bus');
        (null !== expand_bus_selector.html().match(/Expand/g)) ?
            expand_bus_selector.html('Return <i class="fa fa-arrows""></i>') :
            expand_bus_selector.html('Expand <i class="fa fa-arrows""></i>');

        // survey_added = 1;
        // refreshBusPage();
        obj = {"data-id": activeOutlineId};
        setupBus(obj);
    });
});

// Fetches questions and puts them in a form.
function setupBus(obj) {

    // Display buyer's survey buttons.
    const survey_phase_selector = $('#survey-phase');
    survey_phase_selector
        .val(undefined !== localStorage.active_survey_phase ? localStorage.active_survey_phase : 0);

    // Check if the survey phase is set to null.
    (survey_phase_selector.val() === null) ? survey_phase_selector.find('option:eq(0)').prop('selected', true) : '';

    // Set the button for trix buttons.
    trixButtons();

    activeOutlineId = $(obj).attr('data-id');
    $version = $(obj).attr('data-version');

    $('.active_outline').removeClass('active_outline');

    // Change the active Outline.
    $('tr [data-id=' + activeOutlineId + ']').closest('tr').addClass('active_outline');

    let question_input = '';

    // Fetches and displays the outline questions.
    $.ajax({
        url: jsglobals.base_url + 'tsl/get_bus_questions',
        dataType: 'json',
        type: 'post',
        data: {
            outline_id: activeOutlineId,
            phase: $('#survey-phase option:selected').val(),
            full_screen_enabled: $('.full-screen').length
        },
        error: ajaxError
    }).done(function (data) {
        if (data.status !== 'success') {
            toastr.error(data.message);
            return;
        }
        else if (data.status === 'redirect') {
            window.location.href = data.redirect;
            return;
        }

        // Count and number to display phase number.
        // let count = 0, phase_number = 0;

        // Check if full screen width for page is set.
        const is_full_screen = $('.full_view_hide:visible').length;

        // Removes parenthesis and creates survey form for user input.
        let count = 0, phase_count = $('#survey-phase :selected').parent().attr('data-val'),
            phase_value = [
                'Phase 1 – Current Factors',
                'Phase 2 – Emerging Challenges',
                'Phase 3 – Future Implications',
                'Phase 4 – Known Options',
                'Phase 5 – Plan of Action'];

        // Check for current phase dropdown value.
        if ((0 !== is_full_screen)) {
            // Removes parenthesis and creates profile form for user input.
            $.each(data.content, function (index, question) {
                const ques = question.tsq_content.replace(/{{|}}/g, function () {
                    return '';
                });

                // Check if it's the end of a phase.
                if (0 === (count % 5)) {
                    // question_input += '<h4 style="margin-top: 25px"><b>' + phase_value[phase_count] + '</b></h4>';
                    phase_count++;
                    count = 0;
                }
                count++;

                questions_id[index] = question.tsq_id;
                question_input = question_input + '<div style="margin-top: 10px"><label><b>'
                    + parseInt(count) + '. ' + ques + "</b></label>" +
                    "<div class='input-group'>" +
                    "<span class='input-group-addon btn btn-secondary tts'><i class='fa fa-bullhorn'></i></span>" +
                    "<textarea class='form-control answer' rows='1' id=" + 'ans' + question.tsq_id +
                    " name=" + 'ans' + question.tsq_id + " data-id=" + question.tsq_id + "" +
                    " class='answer' onkeyup='auto_grow(this);'" +
                    " placeholder='Click on the microphone and speak or type text.'></textarea>" +
                    "<span class='input-group-addon btn btn-secondary stt'><i class='fa fa-microphone'></i></span>" +
                    "</div></div>";
            });
        } else if (0 === is_full_screen) {
            phase_count = 0;
            $.each(data.content, function (index, question) {
                const ques = question.tsq_content.replace(/{{|}}/g, function () {
                    return '';
                });

                // Check if it's the end of a phase.
                if (0 === (count % 5)) {
                    // question_input += '<h4 style="margin-top: 25px"><b>' + phase_value[phase_count] + '</b></h4>';
                    phase_count++;
                    count = 0;
                }
                count++;

                questions_id[index] = question.tsq_id;
                question_input = question_input + '<div style="margin-top: 10px"><label><b>'
                    + parseInt(count) + '. ' + ques + "</b></label>" +
                    "<div class='input-group'>" +
                    "<span class='input-group-addon btn btn-secondary tts'><i class='fa fa-bullhorn'></i></span>" +
                    "<textarea class='form-control answer' rows='1' cols='70' id=" + 'ans' + question.tsq_id +
                    " name=" + 'ans' + question.tsq_id + " class='answer' onkeyup='auto_grow(this);'" +
                    " placeholder='Click on the microphone and speak or type text.'></textarea>" +
                    "<span class='input-group-addon btn btn-secondary stt'><i class='fa fa-microphone'></i></span>" +
                    "</div></div>";
            });
        }

        // Append container class for full screen view.
        // (0 === is_full_screen) ? question_input = '<div class="container">' + question_input + '</div>' : '';

        const canvas_container_selector = $('#canvas-container');
        canvas_container_selector.html('');
        canvas_container_selector.html('<h4 class="main-con-header">Survey Content</h4><form id="response-form"' +
            ' class="form-horizontal" style="margin: 5px">' + '<h4><b>' +
            $('#survey-phase option:selected').text() +
            '</b></h4><div style="margin-left: 2%">' + question_input + '</div></form>'
        );

        // Fetches and displays list of surveys.
        $.ajax({
            url: jsglobals.base_url + 'tsl/get_survey_list',
            dataType: 'json',
            data: {
                outline_id: activeOutlineId,
            },
            type: 'post',
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }
            updateSurveyList(data.survey);

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });

    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
    });
}

// Fetches Buyer survey outline.
function refreshBusPage() {
    $('.email_attachment').css("display", "none");
    $('#bus_buttons').css("display", "block");

    $.ajax({
        url: jsglobals.base_url + 'tsl/get_bus_outline',
        dataType: 'json',
        type: 'post',
        data: {
            survey_phase: $('#survey-phase').val(),
            survey_type: $('#survey-type').val(),
            survey_class: $('#survey-class').val(),
            client_type: $('#client-type').val(),
        },
        error: ajaxError
    }).done(function (data) {

        if (data.status !== 'success') {
            toastr.error(data.message);
            return;
        }
        else if (data.status === 'redirect') {
            window.location.href = data.redirect;
            return;
        }

        outlines = data.bus;
        updateOutline(data.bus);

    }).fail(function (jqXHR, textStatus) {
        toastr.error("Request failed: " + textStatus);
    }).always(function () {
        // Check if the component is a dummy.
        check_dummy_component(client_id);
    });
}


// Display selected Survey outline.
function updateOutline(outlines) {
    // List outline versions.
    $('#outline-table tbody tr').remove();
    $.each(outlines, function (index, outline) {
        let delete_bus_outline = '',
            // outline_name = "Default Buyer Survey Series",
            is_editable = '';

        // Check if this is default outline;
        ('0' === outline.tso_id) ? delete_bus_outline = 'disabled' : is_editable = 'editable';

        $('#outline-table tbody')
            .append($('<tr>')
                .append($('<td class="text-center">')
                    .append(index + 1))
                .append($('<td class="text-center">')
                    .append($('<a class="survey-outline" data-id="' + outline.tso_id + '" data-val="' + index + '">'
                        + '<span class="fa fa-eye"></span>' +
                        '<span class="' + is_editable + '" data-type="text" data-pk = "' + outline.tso_id + '" ' +
                        'data-url = "' + jsglobals.base_url + 'tsl/update_bus_outline_name"> '
                        + outline.tso_name + '</span></a>')))
                .append($('<td class="text-center">')
                    .append($('<button class="btn btn-default btn-sm"' +
                        ' data-toggle="modal" data-target="#delete-bus-outline-dialog"' +
                        ' data-backdrop="static" data-id="' + outline.tso_id + '"' +
                        ' data-name="' + outline.tso_name + '"' + delete_bus_outline + '>' +
                        ' <span class="glyphicon glyphicon-trash"></span></button>')))
            );
    });

    const active_survey_outline_selector = $('.survey-outline[data-id=' + localStorage.active_survey_outline_id + ']');

    // Check for the last used outline ID.
    (undefined !== localStorage.active_survey_outline_id &&
        active_survey_outline_selector.length !== 0) ?
        active_survey_outline_selector.trigger('click') :
        $('.survey-outline[data-id=0]').trigger('click');
}

// Fetches phases for TSL.
function get_phase() {
    const get_survey_phase = makeAjaxCall('tsl/get_phase', {
        'id': activeOutlineId,
        'type': 'survey'
    });

    get_survey_phase.then(function (data) {

        $('#survey-phase option').remove();
        $('#survey-phase').append(
            $.map(data.phases, function (phase, index) {
                return '<option value="' + phase.cp_id + '" data-value="' + phase.cp_content + '" ' +
                    'data-name="' + phase.cp_name + '" data-number="' + phase.cp_number + '">Phase '
                    + phase.cp_number + ' - ' + phase.cp_name + '</option>'
            }));
        put_content(outlines);
    });
}


// Put Contents to Survey Builder.
function put_content(outlines) {
    const survey_outline_selector = $('.survey-outline'),
        outline_container_selector = $('#outline-container');

    // Displays the default outline if no outline present.
    if (outlines.length === 0) {
        let default_outline;
        $.ajax({
            url: jsglobals.base_url + 'tsl/get_survey_default',
            dataType: 'json',
            type: 'post',
            data: {
                survey_type: $('#survey-type').val(),
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            default_outline = data.content[0].tsd_content;
            outline_container_selector.html('');
            outline_container_selector.html('<h4 class="main-con-header">Survey outline</h4><form id="outline-form">' +
                '<input type="hidden" id="outline_type" name="type">' +
                '<input type="hidden" id="outline_id" name="outline_id">' +
                '<input type="hidden" id="survey_class" name="class">' +
                '<input type="hidden" id="client_type" name="client">' +
                '<input type="hidden" id="survey_phase" name="phase">' +
                '<input type="hidden" id="requester" name="requester">' +
                '<input id="y" value="' + $('#survey-phase option:selected').data('value') +
                '" type="hidden" name="outline_content">\n' +
                '<trix-editor input="y" name="tsl_bus_editor"></trix-editor>' +
                '</form>');

            obj = {"data-id": 0};
            setupBus(obj);

        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });

    }
    // else if(undefined !== localStorage.active_survey_outline_id) {
    //     // $('.survey-outline[data-id=' + localStorage.active_outline_id + ']').trigger('click');
    //     obj = {"data-id": localStorage.active_survey_outline_id};
    //
    //     setupBus(obj);
    // }
    else if (activeOutlineId === 0) {
        // Display first outline, if no outline is selected.
        outline_container_selector.html('');
        outline_container_selector.html('<h4 class="main-con-header">Survey outline</h4><form id="outline-form">' +
            '<input type="hidden" id="outline_type" name="type">' +
            '<input type="hidden" id="outline_id" name="outline_id">' +
            '<input type="hidden" id="survey_class" name="class">' +
            '<input type="hidden" id="client_type" name="client">' +
            '<input type="hidden" id="survey_phase" name="phase">' +
            '<input type="hidden" id="requester" name="requester">' +
            '<input id="y" value="' + $('#survey-phase option:selected').data('value') + '" type="hidden" name="outline_content">\n' +
            '<trix-editor input="y" name="tsl_bus_editor"></trix-editor>' +
            '</form>');

        obj = {"data-id": outlines[0].tso_id};
        setupBus(obj);
    } else if (survey_added === 1) {

        // If new survey is added.
        obj = {"data-id": activeOutlineId};
        setupBus(obj);
    } else if (outline_added === 1) {
        outline_added = 0;
        survey_outline_selector.last().click();
    } else {
        // Display first outline, if no outline is selected.
        outline_container_selector.html('');
        outline_container_selector.html('<h4 class="main-con-header">Survey outline</h4><form id="outline-form">' +
            '<input type="hidden" id="survey_phase" name="phase">' +
            '<input id="y" value="' + $('#survey-phase option:selected').data('value') +
            '" type="hidden" name="content">\n' +
            '<trix-editor input="y" name="tsl_bus_editor"></trix-editor>' +
            '</form>');

        obj = {"data-id": activeOutlineId};
        setupBus(obj);
    }
}

// Display list of Buyer's survey instances.
function updateSurveyList(surveys) {

    $('#tsl-bus-nav').empty();

    $('#tsl-bus-nav')
        .append($('<li class="text-center" id="0">')
            .append('<a href="#" onclick="setupAnswer(this); return false;"' +
                ' data-name="Example Version" data-id="0"><span class="fa fa-eye">' +
                '</span> Example Survey Version</a></li>'))

    // Append to the survey list.
    $.each(surveys, function (index, survey) {
        $('#tsl-bus-nav')
            .append($('<li class="text-center" id="' + survey.tsl_survey_id + '">')
                .append('<a href="#" class="edit-ref" onclick="setupAnswer(this); return false;"' +
                    ' data-name="' + survey.tsl_survey_name + '"data-id="' + survey.tsl_survey_id + '">' +
                    '<span class="editable" data-type="text" data-pk = "' + survey.tsl_survey_id + '" ' +
                    'data-url = "' + jsglobals.base_url + 'tsl/update_bus_name"><span class="fa fa-eye">' +
                    '</span> ' + survey.tsl_survey_name
                    + '</span></a></li>'))
    });


    // Check if reference if undefined (Request is not from sent popup)
    if (!(ref !== undefined && activeSurveyId === -1)) {
        if (activeSurveyId === 0 || activeSurveyId === -1) {
            obj = {"data-id": 0, "data-name": 'Example Survey'};
            setupAnswer(obj);
        } else if (survey_added === 1) {
            $('#tsl-bus-nav li').last().children().click();
        } else if (ref !== undefined) {
            $("a[data-id=" + ref + "]").trigger('click');
        } else {
            $("a[data-id=" + activeSurveyId + "]").trigger('click')
        }

    } else {

        // Fetch the Survey Details.
        const survey_details = makeAjaxCall('cpdv1/get_survey_details', {survey_id: ref});

        survey_details.then(function (data) {
            const response = data.survey_details[0];
            $('#client-type').val(response.tso_client_type);
            $('#survey-class').val(response.tso_survey_class);
            $('#survey-type').val(response.tso_survey_type);
            $('#survey-phase').val(response.tso_survey_phase);
            $('.survey-outline[data-id=' + response.tsl_survey_outline_id + ']').click();
        });
    }
    setEditable('bottom');
}

// Fetch and display already answered questions for a survey.
function setupAnswer(obj) {

    // Set Survey ID.
    activeSurveyId = $(obj).attr('data-id');

    // Remove data attribute for toggle.
    $('#send-bus').removeAttr('data-toggle');
    $('#delete-bus').removeAttr('data-toggle');

    // Displaying the active email name.
    $('.active').removeClass('active');
    $('#' + activeSurveyId).addClass('active');

    // Check Example version is selected.
    if (0 === parseInt($(obj).attr('data-id'))) {
        const fetch_default_answers = makeAjaxCall('tsl/get_survey_default_answer',
            {
                'type': 'survey',
                'phase': $('#survey-phase').prop('selectedIndex') + 1
            });

        fetch_default_answers.then(function (data) {
            $.each(data.default_answers, function (index, answer) {
                $('#response-form textarea').eq(index).val(answer.tsda_content);
            });

            // Auto grow script form text area size.
            $('#response-form textarea').each(function () {
                $(this).css('height', $(this)[0].scrollHeight + 'px');
            });
        });
    } else {
        // Set attribute for send button.
        const send_bus_selector = $('#send-bus'),
            delete_bus_selector = $('#delete-bus');
        send_bus_selector.attr('data-id', activeOutlineId);
        send_bus_selector.attr('data-toggle', 'modal');
        send_bus_selector.attr('data-survey', activeSurveyId);
        send_bus_selector.attr('data-target', '#send-bus-dialog');
        send_bus_selector.attr('data-backdrop', 'static');
        send_bus_selector.attr('data-name', $(obj).attr('data-name'));

        // Set attribute for delete button.
        delete_bus_selector.attr('data-id', activeSurveyId);
        delete_bus_selector.attr('data-toggle', 'modal');
        delete_bus_selector.attr('data-target', '#delete-bus-dialog');
        delete_bus_selector.attr('data-backdrop', 'static');
        delete_bus_selector.attr('data-name', $(obj).attr('data-name'));

        // Fetches the list of already answered questions.
        $.ajax({
            url: jsglobals.base_url + 'survey/get_ans',
            dataType: 'json',
            data: {
                survey_id: activeSurveyId,
                question_id: questions_id,
                client_id: client_id
            },
            type: 'post',
            error: ajaxError
        }).done(function (data) {

            if (data.status !== 'success') {
                toastr.error(data.message);
                return;
            }
            else if (data.status === 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            // Clears answer field.
            $('.answer').val('');
            $.each(data.answers, function (index, answer) {
                $('#ans' + answer.ts_ans_question_id).val(answer.ts_ans_content.replace('\\', ''));
                // Auto grow script form text area size.
                $('#response-form textarea').each(function () {
                    $(this).css('height', $(this)[0].scrollHeight + 'px');
                });
            })
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });
    }

    // Makes the names editable.
    $(".edit-ref").children().click(function (event) {
        event.stopPropagation();
    });

    speechFunctionality();
}

