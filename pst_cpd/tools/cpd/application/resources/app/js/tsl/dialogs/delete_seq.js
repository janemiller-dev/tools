$(document).ready(function () {
    var seq_id = '';
    var ui_name = '';

    // When the dialog is displayed, seq the current instance ID.
    $('#delete-seq-dialog').on('show.bs.modal', function (event) {
        seq_id = $(event.relatedTarget).data('id');
        ui_name = $(event.relatedTarget).data('name');

        // Remove previous data attribute.
        $(event.relatedTarget).removeData('id');
        $(event.relatedTarget).removeData('name')

        $('#seq-name').html(ui_name);
    });

    $(document).on('click', '#close-delete-seq-button', function () {
        $('#delete-seq-dialog').modal('hide');
    });

    // Removes selected message seq.
    $(document).on('submit', '#delete-seq-form', function (e) {
        e.preventDefault();

        $.ajax({
            url: jsglobals.base_url + 'tsl/delete_seq',
            dataType: 'json',
            type: 'post',
            data: {
                seq_id: seq_id
            },
            error: ajaxError
        }).done(function (data) {

            if (data.status != 'success') {
                toastr.error(data.message, 'Error!!');
                return;
            }
            else if (data.status == 'redirect') {
                window.location.href = data.redirect;
                return;
            }

            toastr.success('Message Sequence Deleted', 'Success!!');
            $('#delete-seq-dialog').modal('hide');
            activeMessageId = 0;
            refreshPage();
        }).fail(function (jqXHR, textStatus) {
            toastr.error("Request failed: " + textStatus);
        }).always(function () {
        });

        return false;
    });
});
