$(document).ready(function () {
    $(document).on('click', '.go-to-tsl', function () {
        const tsl_id = $(this).attr('data-id'),
            product_id = $(this).attr('data-product'),
            active_product = $('#current-product').val();

        if (product_id === active_product) {
            window.location.href = jsglobals.base_url + 'tsl/tsl/' + tsl_id;
        } else {
            toastr.error("To access this TSL, change active product type from white box or click 'Yes' below." +
                " <br /><button type='button' id='tsl-confirm-yes' class='btn btn-default'>Yes</button>" +
                "<button type='button' id='tsl-confirm-no' class='btn btn-default'" +
                " style='margin-left: 5px'>No</button>", "Do you want to update active product type?", {
                timeOut: 0,
                extendedTimeOut: 0,
                preventDuplicates: true,
                tapToDismiss: false,
                closeButton: true,
                allowHtml: true,
                onShown: function (toast) {
                    $("#tsl-confirm-yes").off('click');
                    $("#tsl-confirm-yes").click(function () {

                        // Update Active Product.
                        $('#current-product').val(product_id).trigger('change')

                        // Close the Toastr.
                        $('.toast-close-button').trigger('click');
                    });

                    $('#tsl-confirm-no').click(function () {
                        $('.toast-close-button').trigger('click');
                    })
                }
            });
        }
    });

    // Mark or Unmark a TSL instance as active.
    $(document).on('change', '.active-tsl', function () {
        makeAjaxCall('tsl/update_active_tsl', {
            'id': $(this).attr('data-id')
        });
    })
});

// Get the page data and display it.
function refreshPage() {
    updateInstances();
}

// Update the TSL instances
function updateInstances() {

    $.each($('#current-product option'), function (i, option) {

        if (i >= 2) {
            $('#tsl-instance-row' + i).remove()
            let demo_tsl_row = $('.tsl-instance-demo-row').clone();
            demo_tsl_row.removeClass('hidden tsl-instance-demo-row');
            demo_tsl_row.addClass('tsl-instance-row');
            demo_tsl_row.attr('id', 'tsl-instance-row' + i)
            demo_tsl_row.find('#tsl-instance-demo-table').attr('id', 'tsl-instance-table-' + i);
            demo_tsl_row.find('#table-demo-header').attr('id', 'table-header-' + i);

            $('.tsl-instance-row').last().after(demo_tsl_row);

        }

        var instance_datatable = $('#tsl-instance-table-' + i).DataTable();
        // Destroy the datatable so that it can be rebuilt.
        instance_datatable.destroy();

        $('#table-header-' + i).text($('#current-product option').eq(i).text());
        // Create the instance table using server-side data.
        instance_datatable = $('#tsl-instance-table-' + i).DataTable({
            "pageLength": 50,
            "serverSide": true,
            "processing": true,
            "ajax": {
                "url": jsglobals.base_url + "tsl/search_tsl_instances",
                "type": "post",
                "data": {
                    "product_id": $('#current-product option').eq(i).val()
                }
            },
            "columns": [
                {"data": "tsl_id", "name": "TSL ID"},
                {"data": "tsl_name", "name": "Name"},
                {"data": "tsl_description", "name": "Description"},
                // {"data": "cpd_name", "name": "DMD"},
                {"data": "tsl_id", "name": "Location"},
                {"data": "tsl_id", "name": "Suspect List Criteria"},
                {"data": "tsl_id", "name": "Prepared By"},
                {"data": "tsl_created", "name": "Created"},
                {"data": "tsl_modified", "name": "Modified"},
                {"data": "tsl_modified", "name": "Active"},
                {"data": null, "name": "Delete", "className": "text-center"},
            ],
            "rowCallback": function (row, data, index) {

                $('td', row).eq(1).html('<a class="go-to-tsl"' +
                    ' data-product="' + $('#current-product option').eq(i).val() + '"' +
                    ' data-id="' + data.tsl_id + '"  href="#">'
                    + data.tsl_name + "</a>");

                // Append associated Audiences to the List.
                $('td', row).eq(3).empty();
                $('td', row).eq(3).append($('<select class="form-control tsl_location tsl_dropdown"' +
                    ' data-id="' + data.tsl_id + '" data-table-name="tsl_target_audience"' +
                    ' data-target="view-target-audience-dialog" data-name="audience" data-table="target-audience-table" ' +
                    ' data-key="tta">' +
                    '<option disabled selected value="-1"> -- Location List -- </option>')
                    .append(
                        $.map(data.audience, function (audience, index) {
                            return '<option value="' + audience.ul_id + '">' + audience.ul_name + '</option>'
                        })));

                // Append associated Criteria to TSL
                $('td', row).eq(4).empty();
                $('td', row).eq(4).append($('<select class="form-control tsl_criteria tsl_dropdown"' +
                    ' data-id="' + data.tsl_id + '" data-table-name="tsl_criteria"' +
                    'data-target="view-criteria-dialog" data-name="criteria" data-table="criteria-table" data-key="tsl_criteria">' +
                    '<option disabled selected value="-1"> -- Criteria List -- </option>')
                    .append(
                        $.map(data.criteria, function (criterion, index) {
                            return '<option value="' + criterion.uc_id + '">'
                                + criterion.uc_name + '</option>'
                        })));


                // Append associated Prepared By to the List.
                $('td', row).eq(5).empty();
                $('td', row).eq(5).append($('<select class="form-control tsl_prepared_by tsl_dropdown"' +
                    ' data-id="' + data.tsl_id + '"  data-table-name="tsl_agent"' +
                    'data-target="view-agent-dialog" data-name="agent" data-table="agent-table" data-key="tsl_agent">' +
                    '<option disabled="" selected="" value="-1"> -- Prepared By -- </option>' +
                    '<option value="0">Add Prepared By</option>')
                    .append(
                        $.map(data.prepared_by, function (prepared_by, index) {
                            if (1 === parseInt(prepared_by.tsl_agent_selected))
                                return '<option value="' + prepared_by.tsl_agent_id + '" selected>' +
                                    prepared_by.tsl_agent_name + '</option>'
                            else
                                return '<option value="' + prepared_by.tsl_agent_id + '">' +
                                    prepared_by.tsl_agent_name + '</option>'
                        })));

                $('td', row).eq(8).html('<input type="checkbox" class="active-tsl"' +
                    ' data-id="' + data.tsl_id + '" ' + ((data.uat_id !== null) ? ' checked' : '') + '>');

                $('td', row).eq(9).html('<button class="btn btn-default btn-sm"' +
                    ' data-toggle="modal"' +
                    ' data-target="#delete-tsl-dialog" data-backdrop="static"' +
                    ' data-id="' + data.tsl_id +
                    '" data-name="' + data.tsl_name +
                    '"> <span class="glyphicon glyphicon-trash"></span></button>');
            },
            "initComplete": function () {
                if (instance_datatable.data().count() === 0) {
                    $('#no-tsl-instances').show();
                } else
                    $('#no-tsl-instances').hide();

                // Click handler for add new TSL Location/Prepared By/Criteria.
                const dropdown_selector = $('.tsl_location, .tsl_prepared_by, .tsl_criteria');
                dropdown_selector.click(function () {

                    if ('0' === $(this).val()) {
                        const tsl_id = $(this).attr('data-id'),
                            col = $(this).attr('data-name'),
                            table_selector = $('#' + $(this).attr('data-table') + ' tbody'),
                            key = $(this).attr('data-key');

                        $('#' + $(this).attr('data-target')).modal('show');
                        $(this).val('-1');
                        $(":button").attr('data-id', tsl_id);

                        const get_tsl_data = makeAjaxCall('tsl/get_tsl_data', {'tsl_id': tsl_id});
                        get_tsl_data.then(function (data) {
                            table_selector.empty();
                            $.each(data.tsl_data[col], function (index, value) {
                                table_selector
                                    .append($('<tr>')
                                        .append($('<td class="text-center">')
                                            .append(index + 1))
                                        .append($('<td class="text-center">')
                                            .append(value[key + '_name']))
                                        .append($('<td class="text-center">')
                                            .append($('<button class="btn btn-default btn-sm"' +
                                                ' data-toggle="modal"' +
                                                ' data-target="#delete-target-audience-dialog"' +
                                                ' data-backdrop="static"' +
                                                ' data-id="' + value[key + '_id'] +
                                                '" data-name="' + value[key + '_name'] +
                                                '"> <span class="glyphicon glyphicon-trash"></span>'))))
                            })
                        });
                    }
                });
            }
        });
    })
}
