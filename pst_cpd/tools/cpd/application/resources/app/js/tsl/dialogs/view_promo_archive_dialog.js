$(document).ready(function () {
    // Fetches the performance score.
    $('#view-promo-archive-dialog').on('show.bs.modal', function (event) {
        view_promo_archive.init();
    });
});

let view_promo_archive = function () {

    let max_seq = 0,
        deal_id;

    // Initialize and get the DMD ID.
    let init = () => {

        // Check for the type of request.
        if('vmail' === req_type) {
            $('#view-promo-focus-title').text('Message Archive');
            $('#component-name').text('Message Sequence');
            $('#component-topic').text('Message Topic');
            $('#component-response').text('Message Response');
            $('#view-title').text('View Message Content');
        }

        let promo_archive_table = $('#promo-archive-table').DataTable();

        // Destroy the datatable so that it can be rebuilt.
        promo_archive_table.destroy();

        // Create the instance table using server-side data.
        $('#promo-archive-table').DataTable({
            "pageLength": 50,
            "serverSide": true,
            "processing": true,
            "ordering": false,
            "ajax": {
                "url": jsglobals.base_url + "tsl/get_promo_archive",
                "type": "post",
                "data": {
                    id: (undefined === window.location.search.split('?id=')[1] ?
                        window.location.search.split('?ref=')[1] : window.location.search.split('?id=')[1]),
                    origin: origin,
                    component: req_type
                }
            },
            "columnDefs": [
                {"className": "text-center", "targets": "_all"}
            ],
            "autoWidth": false,
            "columns": [
                {"data": "tp_id", "name": "#"},
                {"data": "tp_org_name", "name": "Promo Name"},
                {"data": "ca_topic", "name": "Client Name"},
                {"data": "ca_datetime", "name": "Asset Name"},
                {"data": "ca_response", "name": "Asset Location"},
                {"data": "ca_response", "name": "Asset Location"},
                {"data": "hca_location", "name": "Asset Location"},
            ],
            "rowCallback": function (row, data, index) {

                const content =
                    (data.tp_content != null ? data.tp_content.replace(/\\"/g, "'").replace(/\\/g, '') : null);

                $('td', row).eq(0).html(index + 1);

                $('td', row).eq(1).html(data.tp_org_name);

                $('td', row).eq(4).html('<select class="form-control archive-select" data-id="' + data.ca_id + '">' +
                    '<option value="s" ' + (data.ca_response === 's' ? 'selected' : '') + '>Strong</option>' +
                    '<option value="w" ' + (data.ca_response === 'w' ? 'selected' : '') + '>Weak</option></select>');

                $('td', row).eq(5).html('<i class="fa fa-eye" id="view_archived_promo" data-id="' + data.tp_id + '" ' +
                    'data-content="' + content + '"></i>');

                $('td', row).eq(6).html('<i class="fa fa-trash" id="delete_archive" data-toggle="modal" ' +
                    'data-id="' + data.ca_id + '" data-target="#delete-archive-dialog" ' +
                    'data-name="' + data.tp_org_name + '"></i>');
            },
            "initComplete": function () {
                // View Archive click handler.
                $(document).off('click', '#view_archived_promo');
                $(document).on('click', '#view_archived_promo', function (e) {
                    $('#view-promo-archive-dialog').modal('hide');
                    const that = $(this);

                    // Set the content for Editor.
                    const content = $(this).attr('data-content');
                    if (content === 'null') {
                        $('#view-dialog').modal('show', that);
                    } else {
                        $('#view-promo-archive-instance-dialog').modal('show');
                        tinymce.remove('#archive-viewer-container');

                        // Initialize Tiny MCE editor.
                        tinymce.init({
                            mode: 'exact',
                            selector: '#archive-viewer-container',
                            branding: false,
                            setup: function (ed) {
                                ed.on('init', function (args) {
                                    tinymce.activeEditor.setContent(content);
                                });
                            },
                            init_instance_callback: function (ed) {
                                tinyMCE.DOM.setStyle(tinyMCE.DOM.get('archive-viewer-container_ifr'), 'height', '65vh');
                            }
                        });
                    }
                });
            }
        });

        // Archive selector change.
        $(document).off('change', '.archive-select');
        $(document).on('change', '.archive-select', function (data) {
            const update_archive_select = makeAjaxCall('tsl/update_archive_select', {
                'id': $(this).attr('data-id'),
                'val': $(this).val()
            });

            update_archive_select.then(function (data) {
                console.log(data);
            })
        })
    };
    return {
        init: init
    };
}();