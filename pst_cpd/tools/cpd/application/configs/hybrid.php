<?php

/**
 * HybridAuth
 * http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
 * (c) 2009-2015, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
 */
// ----------------------------------------------------------------------------------------
//	HybridAuth Config file: http://hybridauth.sourceforge.net/userguide/Configuration.html
// ----------------------------------------------------------------------------------------

return
	array(
		"base_url" => "http://cpd.powersellingtools.com/hybridauth_endpoint/",
		"providers" => array(
			// openid providers
			/*
			"OpenID" => array(
					  "enabled" => false
					  ),
			"Yahoo" => array(
					 "enabled" => false,
					 "keys" => array("key" => "", "secret" => ""),
					 ),
			"AOL" => array(
				   "enabled" => false
				   ),
			"Google" => array(
					  "enabled" => true,
					  "keys" => array("id" => "676138839662-faf7akc5819guqnoqsdd8heecg7lro8l.apps.googleusercontent.com", "secret" => "SFvbH1dkl7R5KIL3ywNsoHSP"),
					  ),
			*/
			"Facebook" => array(
				"enabled" => true,
				"keys" => array("id" => "107161893120722", "secret" => "6e4abe61dcd1684f40ea3feef18d21ac"),
				"trustForwarded" => false,
				"scope" => "email, user_about_me, user_birthday, user_hometown"
			),
			/*
			"Twitter" => array(
					   "enabled" => false,
					   "keys" => array("key" => "", "secret" => ""),
					   "includeEmail" => false
					   ),
			// windows live
			"Live" => array(
					"enabled" => false,
					"keys" => array("id" => "", "secret" => "")
					),
			*/
			"LinkedIn" => array(
				"enabled" => true,
				"keys" => array("key" => "78s7xjofaszhfe", "secret" => "vJ0Rl2YKnNiSCT8J")
			),
			/*
			"Foursquare" => array(
					  "enabled" => false,
					  "keys" => array("id" => "", "secret" => "")
					  ),
			*/
		),
		// If you want to enable logging, set 'debug_mode' to true.
		// You can also set it to
		// - "error" To log only error messages. Useful in production
		// - "info" To log info and error messages (ignore debug messages)
		"debug_mode" => true,
		// Path to file writable by the web server. Required if 'debug_mode' is not false
		"debug_file" => __DIR__ . '/../../logs/auth.log',
	);
