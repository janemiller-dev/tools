<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tgd/tgd.js"></script>

<!-- Load the custom css. -->
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/tgd/tgd.css"/>

<!-- Page Title -->
<div class="row">
    <div class="col-xs-12 text-center">
        <h1>Tactical Game Designer</h1>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-4 text-center">
        <span style="font-size: 1.2em;"><a href="" class="hidden" id="previous-year-tgd">Previous Year</a></span>
    </div>

    <div class="col-xs-12 col-sm-4 text-center">
        <span style="font-size: 1.2em;">Active Deal Management Dashboard:&nbsp;
            <span id="linked-cpd"></span>
        </span>
        <br/>
        <span style="font-size: 1.2em;" class="year-info">Year:&nbsp;<span
                    id="tgd-year"></span>
    </div>

    <div class="col-xs-12 col-sm-4 text-center">
        <span style="font-size: 1.2em;"><a href="" class="hidden" id="next-year-tgd">Next Year</a></span>
    </div>
</div>

<!-- Header section -->
<div class="row"
     style="border-bottom: 1px solid #e1c8c8; border-top: 1px solid #e1c8c8; padding-bottom: 5px; padding-top: 5px;">
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <div class="row" style="border-right: 1px solid #e1c8c8;">
            <h4 class="col-xs-12 text-center">Game Identification</h4>
            <div class="col-xs-12">
                <h5 class="col-xs-12 text-center">TGD Name & Product Type</h5>
                <input class="col-xs-12 bottom-buffer form-control" id="tgd-name" data-type="name"
                       placeholder="TGD Name" tabindex="1"
                       style="border: 1px solid" data-name="tgd_name">
                <input class="col-xs-12 bottom-buffer form-control" id="tgd-product-type" data-type="product_type"
                       placeholder="Product Type" tabindex="2"
                       style="border: 1px solid" data-name="product_type">
            </div>
        </div>
    </div>

    <!-- Objectives Columns-->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="border-right: 1px solid #e1c8c8;">
        <div class="row">
            <h4 class="col-xs-12 text-center">Annual Objectives</h4>
            <div class="col-xs-4">
                <h5 class="col-xs-12 text-center">Floor & Actual</h5>
                <input class="col-xs-12 bottom-buffer form-control tgd_year_ftg" id="year-floor" data-type="year"
                       placeholder="Floor" data-col="ui_floor" style="border: 1px solid" data-name="floor" tabindex="3">
                <input class="col-xs-12 bottom-buffer form-control" readonly id="year-actual" placeholder="Actual"
                       style="border: 1px solid">
            </div>
            <div class="col-xs-4">
                <h5 class="col-xs-12 text-center">Target & Percentage</h5>
                <input class="col-xs-12 bottom-buffer form-control tgd_year_ftg" id="year-target" data-type="year"
                       placeholder="Target" data-col="ui_target" style="border: 1px solid" data-name="target" tabindex="4">
                <input class="col-xs-12 form-control" id="year-percent" readonly placeholder="Percent"
                       style="border: 1px solid">
            </div>
            <div class="col-xs-4">
                <h5 class="col-xs-12 text-center">Game & Needed</h5>
                <input class="col-xs-12 bottom-buffer form-control tgd_year_ftg" id="year-game" data-type="year"
                       placeholder="Game" data-col="ui_game" style="border: 1px solid" data-name="game" tabindex="5">
                <input class="col-xs-12 form-control" id="year-needed" placeholder="Needed" readonly
                       style="border: 1px solid">
            </div>
        </div>
    </div>
    <!-- // Objectives Columns-->

    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <div class="row" style="border-right: 1px solid #e1c8c8;">
            <h4 class="col-xs-12 text-center">Game Formulation</h4>
            <div class="col-xs-12">
                <h5 class="col-xs-12 text-center">TGD Mission & Conditions</h5>
                <input class="col-xs-12 bottom-buffer form-control" id="tgd-mission" data-type="mission"
                       placeholder="Mission" tabindex="6"
                       style="border: 1px solid" data-name="tgd_mission">
                <input class="col-xs-12 bottom-buffer form-control" id="tgd-condition" data-type="tgd_conditions"
                       placeholder="Conditions" tabindex="7"
                       style="border: 1px solid" data-name="tgd_conditions">
            </div>
        </div>
    </div>

    <div class="col-xs-12 buffer-top text-center">
        <div id="tool-buttons" class="btn-toolbar">
        </div>
    </div>
</div>

<!-- Show the TGD Instance -->
<div class="row">
    <div class="col-xs-12">

        <!-- TGD instance table -->
        <div class="row">
            <div class="col-xs-12">
                <div id="tgd-div" class="top-buffer table-responsive">
                    <table class="table table-striped" id="tgd-table">
                        <thead class="main-table tgd-head">
                        </thead>
                        <tbody class="main-table">
                        </tbody>
                        <tfoot class="main-table">
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <!-- // TGD instance table -->

    </div>
</div>


<div style='width:710px;' class='dummy_ftg_content hidden'>
    <div class="row">
        <h4 class="col-xs-12" style="text-align: center; font-weight: bold" id="ftg-year"></h4>
        <div class="col-xs-6">
            <span><b> Floor </b></span>
            <input type='text' class='form-control tgd_ftg floor' data-name='tgd_floor'>
            <span><b> Target </b></span>
            <input type='text' class='form-control tgd_ftg target' data-name='tgd_target'>
            <span><b> Game </b></span>
            <input type='text' class='form-control tgd_ftg game' data-name='tgd_game'>
        </div>
        <div class="col-xs-6">
            <span><b> Actual </b></span>
            <input type='text' class='form-control tgd_ftg actual' data-name='tgd_actual'>
            <span><b> Percentage </b></span>
            <input type='text' class='form-control tgd_ftg percent' data-name='tgd_percent'>
            <span><b> Needed </b></span>
            <input type='text' class='form-control tgd_ftg needed' data-name='tgd_needed'>
        </div>
    </div>
</div>
<!-- Dialogs -->
<?php $this->partial('views/tgd/dialogs/copy_instance.php'); ?>
<?php $this->partial('views/tgd/dialogs/open_instance.php'); ?>
<?php $this->partial('views/tgd/dialogs/recalculate.php'); ?>
<?php $this->partial('views/tgd/dialogs/toggle_history.php'); ?>
<?php $this->partial('views/tgd/dialogs/delete_other_income.php'); ?>
<?php $this->partial('views/tgd/dialogs/propagate_other_income.php'); ?>
<?php $this->partial('views/tgd/dialogs/update_other_income.php'); ?>
<?php $this->partial('views/tgd/dialogs/view_linked_tgd.php'); ?>
<?php $this->partial('views/tgd/dialogs/view_tgd_metrics.php'); ?>
