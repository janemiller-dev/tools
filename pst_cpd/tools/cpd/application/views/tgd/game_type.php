<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tgd/game_type.js"></script>

<!-- Load the custom css. -->
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/tgd/game_type.css"/>

<!-- Page Title -->
<div class="row">
    <div class="col-xs-12">
        <div class="text-left">
            <h4>Tactical Game Designer - Industry</h4>
        </div>
    </div>
</div>

<!-- The game type -->
<div class="row">
    <div class="col-xs-12">
        <form id="heading-form" class="form-horizontal">

            <div class="form-group">
                <label class="col-xs-12 col-sm-4 control-label">
                    <h3>Industry:</h3>
                </label>
                <div class="col-xs-12 col-sm-8">
                    <h3 id="game-type-name" class="form-control-static has-tip"></h3>&nbsp;<span
                            class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover"
                            title="The Game Type Name" data-content="The name of the Game Type."></span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-xs-12 col-sm-4 control-label">
                    <h4>Description:</h4>
                </label>
                <div class="col-xs-12 col-sm-8">
                    <h4 id="game-type-description" class="form-control-static has-tip"></h4>&nbsp;<span
                            class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover"
                            title="The Game Type Description" data-content="The description of the Game Type."></span>
                </div>
            </div>

        </form>
    </div>
</div>

<!-- Show the Game Type conditions and multipliers -->
<div class="row">
    <div class="col-xs-12">

        <!-- Game Type condition table. -->
        <div class="row">
            <div class="col-xs-12">
                <p>The Game Type Conditions show the steps in the game, from the start of the sales process to the
                    ending objective.</p>
                <div id="condition-table-div" class="top-buffer">
                    <table class="table table-striped" id="condition-table">
                        <thead class="main-table">
                        <tr>
                            <th>Name</th>
                            <th class="text-center">Type</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody class="main-table">
                        </tbody>
                        <tfoot class="main-table">
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <!-- // Game Type condition table -->

        <!-- Game Type Multiplier table. -->
        <div class="row">
            <div class="col-xs-12">
                <div id="multiplier-table-div" class="top-buffer">
                    <table class="table table-striped" id="multiplier-table">
                        <thead class="main-table">
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Effect</th>
                        </tr>
                        </thead>
                        <tbody class="main-table">
                        </tbody>
                        <tfoot class="main-table">
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <!-- // Game Type condition table -->

    </div>
</div>
