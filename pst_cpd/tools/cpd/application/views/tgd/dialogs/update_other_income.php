<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tgd/dialog/update_other_income.js"></script>

<div class="modal fade" id="update-other-income-dialog" tabindex="-1" role="dialog"
     aria-labelledby="update-other-income-label" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form id="add-group-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-group-title" class="modal-title">Add/Edit Other Income</h2>
                </div>

                <div class="modal-body">

<!--                    <div class="col-xs-12 buffer-top text-center">-->
<!--                        <div id="oi-tool-buttons" class="btn-toolbar">-->
<!--                            <button type="button" class="btn btn-primary" id="add-other-income-btn" data-toggle="modal"-->
<!--                                    data-target="#add-other-income-dialog" data-id="61">-->
<!--                                <span class="has-tooltip" data-toggle="tooltip" title="Add a new Other Income"-->
<!--                                      data-placement="top">Add Other Income</span>-->
<!--                            </button>-->
<!--                        </div>-->
<!--                    </div>-->

                    <div id="tgd-other-income-div" class="top-buffer">
                        <table class="table table-striped" id="other-income-table">
                            <thead>
                            <tr>
                                <th class="text-center">Name</th>
                                <th class="text-center">Source</th>
                                <th class="text-center">Amount</th>
                                <th class="text-center">Propagate</th>
                                <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
<!--                    <button type="submit" class="btn btn-primary">Update</button>-->
                    <button type="button" class="btn btn-primary" id="add-other-income-btn" data-toggle="modal"
                            data-target="#add-other-income-dialog">
                                <span class="has-tooltip" data-toggle="tooltip" title="Add a new Other Income"
                                      data-placement="top">Add Other Income</span>
                    </button>
                </div>

            </form>
        </div>
    </div>
</div>

<!-- Dialogs -->
<?php $this->partial('views/tgd/dialogs/add_other_income.php'); ?>
<?php $this->partial('views/tgd/dialogs/add_income_source.php'); ?>
