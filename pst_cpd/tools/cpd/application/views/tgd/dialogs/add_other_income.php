<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tgd/dialog/add_other_income.js"></script>

<div class="modal fade" id="add-other-income-dialog" tabindex="-1" role="dialog"
     aria-labelledby="add-other-income-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="add-other-income-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-other-income-title" class="modal-title">Add Other Income</h2>
                </div>

                <div class="modal-body">

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="oi_type">Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <select class="form-control" id="oi-type" name="oi_type">
                                </select>
                            </div>
                            <p class="form-text text-muted">Select Name of the other income</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="name">Source</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="oi-name" name="name"/>
                            <p class="form-text text-muted">Enter the Source of other income.</p>
                        </div>
                    </div>

                    <div class="form-group" id="oi-type-amt">
                        <label class="col-xs-12 col-sm-3 control-label" for="name">Amount</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="number" class="form-control" id="oi-amount" name="amount"/>
                            <p class="form-text text-muted">Enter the quarterly amount of the other income.</p>
                        </div>
                    </div>

                    <input type="hidden" name="qtr" id="oi-qtr"></input>
                    <input type="hidden" name="tgd_id" id="oi-id"></input>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add Other Income</button>
                </div>

            </form>
        </div>
    </div>
</div>
