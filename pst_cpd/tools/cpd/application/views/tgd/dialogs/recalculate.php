<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tgd/dialog/recalculate.js"></script>

<div class="modal fade" id="recalculate-dialog" tabindex="-1" role="dialog" aria-labelledby="recalculate-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="recalculate-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="recalculate-title" class="modal-title">Confirm Recalculation</h2>
                </div>

                <div class="modal-body">

                    <div class="col-xs-12">
                        <p>This will recalcuation the Floor, Target and Game values based on the values you set in the
                            Q4 prior year, the proposals and conversion rates. All of the adjustments that you have made
                            will be lost.</p>
                        <p>If you would like to save the current version as a new instance use one of the following:</p>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="name">New Instance</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="recalc-save-as-name" name="save_as_name"/>
                            <p class="form-text text-muted">Enter the name of the new Tactical Game Designer
                                instance.</p>
                        </div>
                    </div>

                    <div id="recalc-select-existing-description">
                        <p>If you would like to over-write another existing instance, select the instance below.</p>
                    </div>

                    <div id="recalc-select-existing-div" class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="name">Existing Instance</label>
                        <div class="col-xs-12 col-sm-9">
                            <select class="form-control" id="recalc-save-as-id" name="save_as_id"/>
                            </select>
                        </div>
                    </div>

                    <div id="recalc-new-tab-checkbox" class="form-group">
                        <div class="checkbox col-xs-12">
                            <label>
                                <input type="checkbox" value="" name="new-tab" id="recalc-new-tab">
                                Open the saved instance in a new tab?
                            </label>
                        </div>
                    </div>

                </div>

                <input type="hidden" id="recalc-tgd-ui-id" name="ui_id" value=""/>
                <input type="hidden" id="recalc-tgd-locked-quarter" name="locked_quarter">

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Confirm</button>
                </div>

            </form>
        </div>
    </div>
</div>
