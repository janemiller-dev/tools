<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tgd/dialog/toggle_history.js"></script>

<div class="modal fade" id="toggle-history-dialog" tabindex="-1" role="dialog" aria-labelledby="toggle-history-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="toggle-history-title" class="modal-title">Toggles History</h2>
            </div>

            <div class="modal-body" style="max-height: 80vh; overflow-y: scroll">

                <div class="col-xs-12">
                    <p id="toggle-history-para"></p>
                </div>

                <!-- Toggles made table. -->
                <div class="row" id="notes-row">
                    <div class="col-xs-12">
                        <div id="note-div" class="top-buffer">
                            <table class="table" id="toggle-history-table" style="font-size: 18px">
                                <thead>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- // Toggles made Table. -->
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
