<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tgd/dialog/add_instance.js"></script>

<div class="modal fade" id="add-instance-dialog" tabindex="-1" role="dialog" aria-labelledby="add-instance-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="add-instance-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-instance-title" class="modal-title">Add Tactical Game Designer Instance</h2>
                </div>

                <div class="modal-body">

                    <input type="hidden" value="2" name="tgd_gt_id">
                    <!-- Select Year Drop Down. -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="year">Select Year</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <select class="form-control" id="tgd-ui-year" name="year">
                                </select>
                            </div>
                            <p class="form-text text-muted">Select the year this instance is for.</p>
                        </div>
                    </div>
                    <!-- //Select Year Drop Down. -->

                    <!-- Industry Drop down -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="tgd-product-id">Select
                            Industry</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <select class="form-control" id="tgd-industry" name="tgd_industry_id">
                                    <option> Commercial Real Estate </option>
                                </select>
                            </div>
                            <p class="form-text text-muted">Select Industry.</p>
                        </div>
                    </div>
                    <!-- // Industry Drop down -->

                    <!-- Profession Drop down -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="tgd-product-id">Select
                            Profession</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <select class="form-control" id="tgd-profession" name="tgd_professsion_id">
                                    <option> Investment Sales </option>
                                </select>
                            </div>
                            <p class="form-text text-muted">Select Profession Type.</p>
                        </div>
                    </div>
                    <!-- // Profession Drop down -->

                    <!-- Select Product Type Drop down -->
<!--                    <div class="form-group">-->
<!--                        <label class="col-xs-12 col-sm-3 control-label" for="product">Select Product Type</label>-->
<!--                        <div class="col-xs-12 col-sm-9">-->
<!--                            <div class="input-group">-->
<!--                                <select class="form-control" id="tgd-product" name="product">-->
<!--                                </select>-->
<!--                            </div>-->
<!--                            <p class="form-text text-muted">Select Product Type.</p>-->
<!--                        </div>-->
<!--                    </div>-->
                    <!-- //Select Product Type Drop down -->


                    <!-- TGD Name Text Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="name">Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="tgd-ui-name" name="name"/>
                            <p class="form-text text-muted">Enter the name of the new Tactical Game Designer
                                instance.</p>
                        </div>
                    </div>
                    <!-- //TGD Name Text Field -->

                    <!-- TGD Mission Text Area -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="name">Mission</label>
                        <div class="col-xs-12 col-sm-9">
                            <textarea class="form-control" rows="5" width="100%" name="mission"
                                      id="tgd-ui-mission"></textarea>
                            <p class="form-text text-muted">A bold statement about what you intend to accomplish in the
                                year to come.</p>
                        </div>
                    </div>
                    <!-- //TGD Mission Text Area -->


                    <!-- TGD Conditions Text Area -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="name">Conditions</label>
                        <div class="col-xs-12 col-sm-9">
                            <textarea type="text" class="form-control" rows="5" width="100%" name="condition"
                                      id="tgd-ui-condition"></textarea>
                            <p class="form-text text-muted">Describe the conditions needed to accomplish this Mission.</p>
                        </div>
                    </div>
                    <!-- //TGD Conditions Text Area -->

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Create Instance</button>
                </div>

            </form>
        </div>
    </div>
</div>
