<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<!--<script src="--><?php //echo $this->basepath; ?><!--resources/app/js/tsl/dialogs/add_income.js"></script>-->

<div class="modal fade" id="add-income-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="add-income-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="add-income-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-income-title" class="modal-title">Add Other Income Source</h2>
                </div>

                <div class="modal-body modal-small">

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="income-text">Income Source</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="income-source" name="income_source"/>
                            <p class="form-text text-muted">Enter Income Source.</p>
                            <input type="hidden" id="tsl_id" name="tsl_id">
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="close-add-income-dialog" data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary" id="add-income-source">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

