<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tgd/dialog/remove_group_instance.js"></script>

<div class="modal fade" id="remove-group-instance" tabindex="-1" role="dialog" aria-labelledby="remove-group-instance"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="remove-group-instance-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="remove-group-instance-title" class="modal-title">Remove Instance from Group?</h2>
                </div>

                <div class="modal-body">

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="name">Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <p class="form-control-static" id="tgd-rm-ui-name"></p>
                        </div>
                    </div>

                </div>

                <input type="hidden" id="tgd-rm-g-id" name="tgd_g_id"/>
                <input type="hidden" id="tgd-rm-ui-id" name="tgd_ui_id"/>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Remove</button>
                </div>

            </form>
        </div>
    </div>
</div>
