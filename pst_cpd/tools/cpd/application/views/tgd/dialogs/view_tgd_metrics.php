<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="view-tgd-metrics-dialog" tabindex="-1" role="dialog" aria-labelledby="linked-tgd-label"
     aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 class="modal-title">Activity Metrics Scoreboard</h2>
            </div>

            <div class="modal-body">
                <!-- Toggles made table. -->
                <div class="row" id="tgd-metrics-row">
                    <div class="col-xs-12">
                        <table class="table" id="tgd-metrics-table">
                            <tr>
                                <td class="text-center">
                                    <h5><b>TSL CALL ACTIVITY CALCULATOR</b></h5>
                                    <div class="row" style="text-align: left; border-top: 1px solid #e1c8c8;">
                                        <div class="col-xs-14">
                                            <label>Minimum Activity Benchmarks</label>
                                        </div>
                                        <div class="col-xs-14"
                                             style="border-right: 1px solid #e1c8c8; border-left: 1px solid #e1c8c8;">
                                            <span><b> Dials Range Minimum</b></span>
                                            <input type="text" class="form-control metrics-benchmark"
                                                   data-name="dial_range">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Broken Number Discovered </b></span>
                                            <input type="text" class="form-control metrics-benchmark"
                                                   data-name="broken_number">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Contacts Achieved Minimum </b></span>
                                            <input type="text" class="form-control metrics-benchmark"
                                                   data-name="contacts_achieved">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Left Messages Minimum </b></span>
                                            <input type="text" class="form-control metrics-benchmark"
                                                   data-name="left_messages">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Full Conversation Minimum </b></span>
                                            <input type="text" class="form-control metrics-benchmark"
                                                   data-name="full_conversation">
                                        </div>
                                        <div class="col-xs-14">
                                            <span><b> Meeting Scheduled Minimum </b></span>
                                            <input type="text" class="form-control metrics-benchmark"
                                                   data-name="meeting_scheduled">
                                        </div>
                                    </div>

                                    <div class="row"
                                         style="text-align: left; border-top: 1px solid #e1c8c8;
                                         ">
                                        <div class="col-xs-14">
                                            <label>Current Activity Totals</label>
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8;
                                        border-left: 1px solid #e1c8c8;">
                                            <span><b> Dials Range Current</b></span>
                                            <input type="text" class="form-control" id="dials_count">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Broken Number Current </b></span>
                                            <input type="text" class="form-control" id="broken_count">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Contacts Achieved Current </b></span>
                                            <input type="text" class="form-control" id="contacts_count">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Left Messages Current </b></span>
                                            <input type="text" class="form-control" id="message_count">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Full Conversation Current </b></span>
                                            <input type="text" class="form-control" id="conversation_count">
                                        </div>
                                        <div class="col-xs-14">
                                            <span><b> Meeting Scheduled Current </b></span>
                                            <input type="text" class="form-control" id="meetings_count">
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center">
                                    <h5><b>DMD DEAL GENERATION CALCULATOR</b></h5>

                                    <div class="row" style="text-align: left; border-top: 1px solid #e1c8c8;">
                                        <div class="col-xs-14">
                                            <label>Minimum Activity Benchmarks</label>
                                        </div>
                                        <div class="col-xs-14"
                                             style="border-right: 1px solid #e1c8c8; border-left: 1px solid #e1c8c8;">
                                            <span><b> Meetings Scheduled Minimum</b></span>
                                            <input type="text" class="form-control metrics-benchmark"
                                                   data-name="meeting_scheduled">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Meetings Completed Minimum </b></span>
                                            <input type="text" class="form-control metrics-benchmark"
                                                   data-name="meeting_completed">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Proposals Requested Minimum </b></span>
                                            <input type="text" class="form-control metrics-benchmark"
                                                   data-name="proposal_requested">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Proposals Presented Minimum </b></span>
                                            <input type="text" class="form-control metrics-benchmark"
                                                   data-name="proposal_presented">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Deals Listed Minimum </b></span>
                                            <input type="text" class="form-control metrics-benchmark"
                                                   data-name="deal_listed">
                                        </div>
                                        <div class="col-xs-14">
                                            <span><b> Deals Launched Minimum </b></span>
                                            <input type="text" class="form-control metrics-benchmark"
                                                   data-name="deal_launched">
                                        </div>
                                    </div>

                                    <div class="row"
                                         style="text-align: left; border-top: 1px solid #e1c8c8;
                                         ">
                                        <div class="col-xs-14">
                                            <label>Current Activity Totals</label>
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8;
                                        border-left: 1px solid #e1c8c8;">
                                            <span><b> Meetings Scheduled Current</b></span>
                                            <input type="text" class="form-control cpd-col" data-status="scheduled"
                                                   data-type="count">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Meetings Completed Current </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="count"
                                                   data-status="completed">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Proposals Requested Current </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="count"
                                                   data-status="requested">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Proposals Presented Current </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="count"
                                                   data-status="presented">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Deals Listed Current </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="count"
                                                   data-status="listed">
                                        </div>
                                        <div class="col-xs-14">
                                            <span><b> Deals Launched Current </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="count"
                                                   data-status="launched">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <!-- Deal Completion Data -->
                                <td class="text-center">
                                    <h5><b>DMD DEAL COMPLETION CALCULATOR</b></h5>
                                    <!-- Contract Activity Count -->
                                    <div class="row"
                                         style="text-align: left; border-top: 1px solid #e1c8c8;">
                                        <div class="col-xs-14">
                                            <label>Contract Activity Count</label>
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8;
                                        border-left: 1px solid #e1c8c8;">
                                            <span><b> Deals Contract Hard</b></span>
                                            <input type="text" class="form-control cpd-col" data-type="count"
                                                   data-status="contract">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Contracts Hard Count </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="count"
                                                   data-status="hard">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Closing Accepted Count </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="count"
                                                   data-status="complete">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Closing Completed Count </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="count"
                                                   data-status="complete">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Repeat Business Count </b></span>
                                            <input type="text" class="form-control cpd-col"  data-type="count"
                                                   data-status="repeat">
                                        </div>
                                        <div class="col-xs-14">
                                            <span><b> Referral Business Count </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="count"
                                                   data-status="ref_com">
                                        </div>
                                    </div>
                                    <!--// Contract Activity Count -->

                                    <!-- Contract Activity Total Value -->
                                    <div class="row"
                                         style="text-align: left; border-top: 1px solid #e1c8c8;
                                         ">
                                        <div class="col-xs-14">
                                            <label>Contract Activity Total Values</label>
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8;
                                        border-left: 1px solid #e1c8c8;">
                                            <span><b> Contracts Signed Value </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="price"
                                                   data-status="contract">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Contracts Hard Value </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="price"
                                                   data-status="hard" >
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Closing Accepted Value </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="price"
                                                   data-status="complete">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Closing Completed Value </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="price"
                                                   data-status="complete">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Repeat Business Value </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="price"
                                                   data-status="repeat">
                                        </div>
                                        <div class="col-xs-14">
                                            <span><b> Referral Business Value </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="price"
                                                   data-status="ref_com">
                                        </div>
                                    </div>
                                    <!-- Contract Activity Total Value -->

                                    <!-- Contract Activity Total Gross -->
                                    <div class="row"
                                         style="text-align: left; border-top: 1px solid #e1c8c8;
                                         ">
                                        <div class="col-xs-14">
                                            <label>Contract Signed Total Gross</label>
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8;
                                        border-left: 1px solid #e1c8c8;">
                                            <span><b> Contracts Signed Gross </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="gross"
                                                   data-status="contract">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Contracts Hard Gross </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="gross"
                                                   data-status="hard">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Closing Accepted Gross </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="gross"
                                                   data-status="complete">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Closing Completed Gross </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="gross"
                                                   data-status="complete">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Repeat Business Gross </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="gross"
                                                   data-status="repeat">
                                        </div>
                                        <div class="col-xs-14">
                                            <span><b> Referral Business Gross </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="gross"
                                                   data-status="ref_com">
                                        </div>
                                    </div>
                                    <!-- Contract Activity Total Gross -->

                                    <!-- Contract Activity Total Net -->
                                    <div class="row"
                                         style="text-align: left; border-top: 1px solid #e1c8c8;
                                         ">
                                        <div class="col-xs-14">
                                            <label>Contract Signed Total Net</label>
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8;
                                        border-left: 1px solid #e1c8c8;">
                                            <span><b> Contracts Signed Net </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="net"
                                                   data-status="contract">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Contracts Hard Net </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="net"
                                                   data-status="hard">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Closing Accepted Net </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="net"
                                                   data-status="complete">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Closing Completed Net </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="net"
                                                   data-status="complete">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Repeat Business Net </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="net"
                                                   data-status="repeat">
                                        </div>
                                        <div class="col-xs-14">
                                            <span><b> Referral Business Net </b></span>
                                            <input type="text" class="form-control cpd-col" data-type="net"
                                                   data-status="ref_com">
                                        </div>
                                    </div>
                                    <!-- Contract Activity Total Net -->

                                </td>
                                <!--// Deal Completion Data -->

                                <!-- Buyer Data -->
                                <td class="text-center">
                                    <h5><b>DMD BUYER CALCULATOR</b></h5>
                                    <!-- Contract Activity Count -->
                                    <div class="row"
                                         style="text-align: left; border-top: 1px solid #e1c8c8;">
                                        <div class="col-xs-14">
                                            <label>Minimum Activity Benchmarks</label>
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8;
                                        border-left: 1px solid #e1c8c8;">
                                            <span><b> Tours Scheduled Count </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Tours Completed Count </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Offer Submitted Count </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Offer Considered Count </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Contracts Submitted Count </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-xs-14">
                                            <span><b> Contracts Closed Count </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <!--// Contract Activity Count -->

                                    <!-- Contract Activity Total Value -->
                                    <div class="row"
                                         style="text-align: left; border-top: 1px solid #e1c8c8;
                                         ">
                                        <div class="col-xs-14">
                                            <label>Current Activity Total Values </label>
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8;
                                        border-left: 1px solid #e1c8c8;">
                                            <span><b> Tours Scheduled Value </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Tours Completed Value </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Offer Submitted Value </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Offer Considered Value </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Contracts Submitted Value </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-xs-14">
                                            <span><b> Contracts Closed Value </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <!-- Contract Activity Total Value -->

                                    <!-- Contract Activity Total Gross -->
                                    <div class="row"
                                         style="text-align: left; border-top: 1px solid #e1c8c8;
                                         ">
                                        <div class="col-xs-14">
                                            <label>Contract Activity Total Gross</label>
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8;
                                        border-left: 1px solid #e1c8c8;">
                                            <span><b> Contracts Signed Gross </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Contracts Hard Gross </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Offer Submitted Gross </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Offer Considered Gross </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Contracts Submitted Gross </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-xs-14">
                                            <span><b> Contracts Closed Gross </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <!-- Contract Activity Total Gross -->

                                    <!-- Contract Activity Total Net -->
                                    <div class="row"
                                         style="text-align: left; border-top: 1px solid #e1c8c8;
                                         ">
                                        <div class="col-xs-14">
                                            <label>Contract Activity Total Net</label>
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8;
                                        border-left: 1px solid #e1c8c8;">
                                            <span><b> Contracts Signed Net </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Contracts Hard Net </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Offers Submitted Net </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Offers Considered Net </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-xs-14" style="border-right: 1px solid #e1c8c8">
                                            <span><b> Contracts Submitted Net </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                        <div class="col-xs-14">
                                            <span><b> Contracts Closed Net </b></span>
                                            <input type="text" class="form-control">
                                        </div>
                                    </div>
                                    <!-- Contract Activity Total Net -->

                                </td>
                                <!--// Buyer Data -->
                            </tr>
                            <tr>
                                <td colspan="2" class="text-center" style="border: none">
                                    <button class="btn btn-default" data-toggle="popover" data-html="true"
                                            data-placement="top"
                                            data-content="<div class='row'>
                                            <div class='col-xs-4'>
                                            <label>Meeting Scheduled</label>
                                            <input class='form-control'/>
                                            </div>
                                            <div class='col-xs-4'>
                                            <label>Analysis Requested</label>
                                            <input class='form-control'/>
                                            </div>
                                            <div class='col-xs-4'>
                                            <label>Updates Scheduled</label>
                                            <input class='form-control'/>
                                            </div>
                                            <div class='col-xs-4'>
                                            <label>Meeting Completed</label>
                                            <input class='form-control'/>
                                            </div>
                                            <div class='col-xs-4'>
                                            <label>Analysis Delivered</label>
                                            <input class='form-control'/>
                                            </div>
                                            <div class='col-xs-4'>
                                            <label>Updates Completed</label>
                                            <input class='form-control'/>
                                            </div>
                                            </div>" data-title="STRATEGIC CLIENT SHARE CALCULATOR">
                                        View Strategic Calculator</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!-- // Toggles made Table. -->
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/tgd/view_tgd_metrics.css"/>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tgd/dialog/view_tgd_metrics.js"></script>

