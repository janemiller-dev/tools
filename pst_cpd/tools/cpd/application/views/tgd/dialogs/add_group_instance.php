<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tgd/dialog/add_group_instance.js"></script>

<div class="modal fade" id="add-group-instance" tabindex="-1" role="dialog" aria-labelledby="add-group-instance-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="add-group-instance-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-group-instance-title" class="modal-title">Add TGD Instance to Group</h2>
                </div>

                <div class="modal-body">

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="name">Instance</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <select class="form-control" id="add-group-ui-id" name="tgd_ui_id">
                                </select>
                            </div>
                            <p class="form-text text-muted">Select the TGD Instance that you want to add to the
                                group.</p>
                        </div>
                    </div>

                </div>

                <input type="hidden" id="add-group-g-id" name="tgd_g_id"/>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add to Group</button>
                </div>

            </form>
        </div>
    </div>
</div>
