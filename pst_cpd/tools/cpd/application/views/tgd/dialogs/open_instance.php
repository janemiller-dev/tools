<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tgd/dialog/open_instance.js"></script>

<div class="modal fade" id="open-instance-dialog" tabindex="-1" role="dialog" aria-labelledby="open-instance-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="open-instance-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="open-instance-title" class="modal-title">Open an Instance</h2>
                </div>

                <div class="modal-body">

                    <div id="open-instance-message">
                        <p>Select the instance that you would like to open and whether or not to open it in a new
                            tab.</p>
                    </div>

                    <div id="open-instance-div" class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="name">Instance to Open</label>
                        <div class="col-xs-12 col-sm-9">
                            <select class="form-control" id="open-instance-id" name="open-instance-id"/>
                            </select>
                        </div>
                    </div>

                    <div id="open-instance-checkbox" class="form-group">
                        <div class="checkbox col-xs-12">
                            <label>
                                <input type="checkbox" value="" id="open-instance-new-tab">
                                Open the instance in a new tab?
                            </label>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Open</button>
                </div>

            </form>
        </div>
    </div>
</div>
