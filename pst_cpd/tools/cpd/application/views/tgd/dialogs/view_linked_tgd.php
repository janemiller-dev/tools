<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tgd/dialog/view_linked_tgd.js"></script>

<div class="modal fade" id="view-linked-tgd-dialog" tabindex="-1" role="dialog" aria-labelledby="linked-tgd-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="linked-tgd-label" class="modal-title">Linked TGD</h2>
            </div>

            <div class="modal-body" style="max-height: 80vh; overflow-y: scroll">

                <div class="col-xs-12">
                    <p id="linked-tgd-par"></p>
                </div>

                <!-- Toggles made table. -->
                <div class="row" id="notes-row">
                    <div class="col-xs-12">
                        <div id="note-div" class="top-buffer">
                            <table class="table" id="linked-tgd-table">
                                <thead>
                                <th class="text-center">#</th>
                                <th class="text-center">Product</th>
                                <th class="text-center">Active TGD</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- // Toggles made Table. -->
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
