<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tgd/dialog/propagate_other_income.js"></script>

<div class="modal fade" id="propagate-other-income-dialog" tabindex="-1" role="dialog"
     aria-labelledby="propagate-other-income-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="propagate-other-income-form" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="propagate-other-income-title" class="modal-title">Forward Propagate Other Income.</h2>
                </div>

                <div class="modal-body modal-small">
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="name">Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <p class="form-control-static" id="prop-other-income-name"></p>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="close-propagate-other-income-dialog" data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary" id="propagate-other-income-button">Propagate</button>
                </div>
            </form>
        </div>
    </div>
</div>
