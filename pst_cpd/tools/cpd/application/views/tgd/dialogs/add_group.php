<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tgd/dialog/add_group.js"></script>

<div class="modal fade" id="add-group-dialog" tabindex="-1" role="dialog" aria-labelledby="add-group-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="add-group-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-group-title" class="modal-title">Add Tactical Game Designer Group</h2>
                </div>

                <div class="modal-body">

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="pool_id">Industry</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <select class="form-control" id="tgd-g-gt-id" name="tgd_gt_id">
                                </select>
                            </div>
                            <p class="form-text text-muted">Select your industry. For more information on the available
                                industriess, see <a href="/tgd/game_types">Industries</a></p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="year">Year</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <select class="form-control" id="tgd-g-year" name="year">
                                </select>
                            </div>
                            <p class="form-text text-muted">Select the year this group is for.</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="name">Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="tgd-g-name" name="name"/>
                            <p class="form-text text-muted">Enter the name of the new Tactical Game Designer group.</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="name">Description</label>
                        <div class="col-xs-12 col-sm-9">
                            <textarea class="form-control" rows="5" width="100%" name="description"
                                      id="tgd-g-description"></textarea>
                            <p class="form-text text-muted">A description of the group.</p>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Create Group</button>
                </div>

            </form>
        </div>
    </div>
</div>
