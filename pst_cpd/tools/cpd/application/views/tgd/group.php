<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tgd/group.js"></script>

<!-- Load the custom css. -->
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/tgd/group.css"/>

<!-- Page Title -->
<div class="row">
    <div class="col-xs-12 text-center">
        <h1>Tactical Game Designer Group</h1>
    </div>
</div>

<!-- Header section -->
<div class="row">
    <div class="col-xs-12 col-sm-6 col-lg-6 col-lg-push-6">
        <div class="text-center">
            <h4>Manage Group Members</h4>
            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-striped" id="member-table">
                        <thead>
                        <tr>
                            <th class="text-left">Instance Name</th>
                            <th class="text-center"><span class="glyphicon glyphicon-remove"></span></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-lg-6 col-lg-pull-6">

        <div class="col-xs-12">
            <form id="heading-form" class="form-horizontal">
                <div class="form-group">
                    <label class="col-xs-12 col-sm-4 control-label">
                        <h3>Name:</h3>
                    </label>
                    <div class="col-xs-12 col-sm-8"><h3 id="group-name" class="form-control-static"></h3>&nbsp;<span
                                class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover"
                                title="Name of the group" data-content="The name of the TGD Group."></span></div>
                </div>
                <div class="form-group">
                    <label class="col-xs-12 col-sm-4 control-label"><h4>Description:</h4></label>
                    <div class="col-xs-12 col-sm-8"><h4 id="group-description"
                                                        class="form-control-static"></h4>&nbsp;<span
                                class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover"
                                title="Group Description" data-content="The description of the group."></span></div>
                </div>
            </form>
        </div>

        <div class="col-xs-12 buffer-top">
            <div id="tool-buttons" class="btn-toolbar">
            </div>
        </div>

    </div>
</div>

<!-- Show the TGD Instance -->
<div class="row">
    <div class="col-xs-12">

        <!-- TGD instance table -->
        <div class="row">
            <div class="col-xs-12">
                <div id="tgd-div" class="top-buffer table-responsive">
                    <table class="table table-striped" id="group-table">
                        <thead class="main-table">
                        </thead>
                        <tbody class="main-table">
                        </tbody>
                        <tfoot class="main-table">
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
        <!-- // TGD instance table -->

    </div>
</div>

<!-- Dialogs -->
<?php $this->partial('views/tgd/dialogs/add_group_instance.php'); ?>
<?php $this->partial('views/tgd/dialogs/remove_group_instance.php'); ?>
