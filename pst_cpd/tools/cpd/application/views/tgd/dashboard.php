<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>


<!-- Page Title -->
<div class="row">
    <div class="col-xs-12">
        <h2>Tactical Game Designer Dashboard</h2>
    </div>
</div>

<!-- Show the list of my TGD instances. -->
<div class="row">
    <div class="col-xs-12">

        <!-- Tab panel. -->
        <div role="tabpanel">
            <div class="tab-content">

                <!-- TGD instance. -->
                <div role="tabpanel" class="tab-pane active" id="instances">
                    <div class="col-xs-12 text-left">
                        <button type="button" id="add-instance" class="btn btn-primary bottom-buffer"
                                data-toggle="modal" data-target="#add-instance-dialog">Add Instance
                        </button>
                    </div>

                    <div class="row tgd-instance-row" id="tgd-instance-row">
                        <div class="col-xs-12">
                            <div id="tgd-instance-div" class="top-buffer">
<!--                                <h4 id="table-header-1" style="font-weight: bold"></h4>-->
                                <table class="table table-striped" id="tgd-instance-table"
                                       style="border: 1px solid rgb(225, 200, 200);">
                                    <thead>
                                    <tr>
                                        <th>UI ID</th>
                                        <th>Owner</th>
                                        <th>Name</th>
<!--                                        <th>Linked TGD</th>-->
                                        <th>Mission</th>
                                        <th>Year</th>
                                        <th>Created</th>
                                        <th>Modified</th>
                                        <th>Active</th>
                                        <th class="text-center"><span class="fa fa-files-o"></span></th>
                                        <th class="text-center"><span class="fa fa-trash"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

<!--                            <div id="tgd-instance-demo-div" class="top-buffer hidden tgd-instance-demo-div">-->
<!--                                <hr>-->
<!--                                <h4 id="table-demo-header" style="font-weight: bold"></h4>-->
<!--                                <table class="table table-striped" id="tgd-instance-demo-table"-->
<!--                                       style="border: 1px solid rgb(225, 200, 200);">-->
<!--                                    <thead>-->
<!--                                    <tr>-->
<!--                                        <th>UI ID</th>-->
<!--                                        <th>Owner</th>-->
<!--                                        <th>Name</th>-->
<!--                                        <th>Linked TGD</th>-->
<!--                                        <th>Mission</th>-->
<!--                                        <th>Year</th>-->
<!--                                        <th>Created</th>-->
<!--                                        <th>Modified</th>-->
<!--                                        <th>Active</th>-->
<!--                                        <th class="text-center"><span class="fa fa-files-o"></span></th>-->
<!--                                        <th class="text-center"><span class="fa fa-trash"></span></th>-->
<!--                                    </tr>-->
<!--                                    </thead>-->
<!--                                    <tbody>-->
<!--                                    </tbody>-->
<!--                                </table>-->
<!--                            </div>-->
                        </div>
                    </div>
                </div>
                <!-- // TGD instance table -->
            </div>
        </div>
    </div>

    <!-- Dialogs -->
    <!--Load PHP files-->
    <?php $this->partial('views/tgd/dialogs/add_instance.php'); ?>
    <?php $this->partial('views/tgd/dialogs/delete_instance.php'); ?>
    <?php $this->partial('views/tgd/dialogs/copy_instance.php'); ?>
    <?php $this->partial('views/tgd/dialogs/add_group.php'); ?>
    <?php $this->partial('views/tgd/dialogs/delete_group.php'); ?>
    <!-- Allow unshares -->
    <?php $this->partial('views/dialogs/unshare.php'); ?>

    <!-- Load the javascript support. -->
    <script src="<?php echo $this->basepath; ?>resources/app/js/tgd/dashboard.js"></script>