<!-- Homebase dialog to move a client to DMD/TSL. -->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/homebase/dialogs/push_deal.js"></script>

<!--BS Modal-->
<div class="modal fade" id="push-deal-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="message-label" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="push-deal-title" class="modal-title">Move Deal.</h2>

            </div>

            <div class="modal-body">

                <!-- Show the list of TSL if `To be contacted` deal status is selected. -->
                <div class="row">
                    <div class="col-xs-12">
                        <!-- List TSL Names. -->
                        <div class="row" id="tsl-row">
                            <div class="col-xs-12">
                                <div id="sent-div" class="top-buffer hidden">
                                    <label class="col-xs-4"> Select TSL Instance</label>
                                    <div class="col-xs-8">
                                        <select id="tsl-instance" class="form-control">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!-- // List TSL Names -->
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="submit_deal">Submit</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!--// BS Modal-->