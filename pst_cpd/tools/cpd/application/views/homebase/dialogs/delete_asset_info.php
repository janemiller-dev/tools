<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/homebase/dialogs/delete_asset_info.js"></script>

<div class="modal fade" id="delete-asset-info-dialog" tabindex="-1" data-backdrop="static" role="dialog"
     aria-labelledby="delete-asset-info-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="delete-asset-info-form" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="delete-asset-info-title" class="modal-title"></h2>
                </div>

                <div class="modal-body modal-small">
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-3 text-center">
                            <label class="control-label" for="name">Name</label>
                        </div>
                        <div class="col-xs-12 col-sm-9">
                            <p class="form-control-static" id="asset-info-name"></p>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="close-delete-asset-info-dialog"
                            data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary" id="delete-asset-info-button">Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>
