<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/homebase/dialogs/add_client.js"></script>

<div class="modal fade" id="add-client-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="add-client-label" style="overflow-y: auto;" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-xl">
        <div class="modal-content">

            <form id="add-client-form" class="form-horizontal">
                <div class="modal-header">
                    <div class="col-xs-12">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h2 id="add-client-title" class="modal-title">Add Clients</h2>

                        <div style="float: right;" class="col-xs-7">
                            <!-- Number of Entries-->
                            <div class="col-xs-12">
                                <div class="col-xs-2" style="float: right;">
                                    <input type="number " id="client-count" name="client_count" value="1"
                                           class="form-control col-xs-3" style="display: inline">
                                </div>
                                <label for="client_count" class="control-label"
                                       style="font-size: 1em; float: right">Create Number of Entries: </label>
                            </div>
                            <!-- //Number of Entries-->
                        </div>
                    </div>
                </div>

                <div class="modal-body modal-small" style="margin-bottom: 10px; background: #eee">
                    <div class="row  add-client-modal">
                        <div class="col-xs-12 add-client-info client-info1 client-row" id="add-client-1">
                            <div class="row add-client-info-row">
                                <div class="col-xs-14">
                                    <label for="client-name">Client Name</label>
                                    <div>
                                        <input type="text" class="form-control client-name" id="client-name1"
                                               name="client_name1"
                                               placeholder="Enter the Name of the client." data-position="1"
                                               data-asset="2"/>
                                    </div>
                                </div>

                                <div class="col-xs-14">
                                    <label for="client-phone">Main Phone</label>
                                    <div>
                                        <input type="text" class="form-control" id="main-phone1"
                                               placeholder="Enter Main Phone" name="main_phone1"/>
                                    </div>
                                </div>

                                <div class="col-xs-14">
                                    <label for="client-phone">Second Phone</label>
                                    <div>
                                        <input type="text" class="form-control" id="second-phone1"
                                               placeholder="Enter Second Phone" name="second_phone1"/>
                                    </div>
                                </div>

                                <div class="col-xs-14">
                                    <label for="mobile-phone">Mobile Phone</label>
                                    <div>
                                        <input type="text" class="form-control" id="mobile-phone1"
                                               placeholder="Enter Mobile Phone" name="mobile_phone1"/>
                                    </div>
                                </div>

                                <div class="col-xs-14">
                                    <label for="client-email">Client Email</label>
                                    <div>
                                        <input type="text" class="form-control" id="client-email1"
                                               name="client_email1" placeholder="Enter Client Email"/>
                                    </div>
                                </div>

                                <div class="col-xs-14">
                                    <label for="entity-name">Entity Name</label>
                                    <div>
                                        <input type="text" class="form-control" id="entity-name1"
                                               name="entity_name1" placeholder="Enter Entity Name"/>
                                    </div>
                                </div>

                                <div class="col-xs-1">
                                    <div>
                                        <div>
                                            <label></label>
                                        </div>
                                        <button class="add-form-asset btn btn-primary" id="add-asset1">Add Asset
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 asset-info add-client-asset"
                                 style="border: 5px solid #008080cf; background: #eee; padding: 10px">
                                <div class="col-xs-14">
                                    <label for="client-asset">Asset Name</label>
                                    <div>
                                        <input type="text" class="form-control" id="asset-name1-1"
                                               placeholder="Enter Asset Name" name="asset_name1-1"/>
                                    </div>
                                </div>

                                <div class="col-xs-14">
                                    <label for="address">Asset Address</label>
                                    <div>
                                        <input type="text" class="form-control address" id="address1-1"
                                               placeholder="Enter Asset Address" name="asset_address1-1"/>
                                    </div>
                                </div>

                                <div class="col-xs-14">
                                    <label for="city">City</label>
                                    <div>
                                        <input type="text" class="form-control add-client-city" id="city1-1"
                                               placeholder="Enter Asset City" name="asset_city1-1"/>
                                    </div>
                                </div>
                                <div class="col-xs-14">
                                    <label for="state">State</label>
                                    <div>
                                        <input type="text" class="form-control add-client-state" id="state1-1"
                                               name="asset_state1-1" placeholder="Enter Asset City"/>
                                    </div>
                                </div>
                                <div class="col-xs-14">
                                    <label for="zip">ZIP</label>
                                    <div>
                                        <input type="text" class="form-control" id="zip1-1"
                                               placeholder="Enter Asset ZIP" name="asset_zip1-1"/>
                                    </div>
                                </div>

                                <div class="col-xs-14">
                                    <label for="asset-status">Select Status</label>
                                    <div>
                                        <select class="form-control client-status" id="asset-status1-1"
                                                name="asset_status1-1" style="width: 100%;"></select>
                                    </div>
                                </div>

                                <div class="col-xs-1">
                                    <div>
                                        <label></label>
                                    </div>
                                    <button class="btn btn-primary delete-form-asset" id="delete-asset-1-1">Delete Asset
                                    </button>
                                </div>

                                <div class="col-xs-30">
                                    <label for="client-asset">Product Type</label>
                                    <div>
                                        <select id="asset-product1-1" name="asset_product1-1"
                                                class="form-control col-xs-3 add-client-product"
                                                style="display: inline"></select>
                                    </div>
                                </div>

                                <div class="col-xs-30">
                                    <label for="address">Market Area</label>
                                    <div>
                                        <select class="form-control client-location" id="asset-location1-1"
                                                name="asset_location1-1"></select>
                                    </div>
                                </div>

                                <div class="col-xs-30">
                                    <label for="units">Units</label>
                                    <div>
                                        <input class="form-control add-client-units" id="asset-units1-1"
                                                name="asset_unit1-1"/>
                                    </div>
                                </div>


                                <div class="col-xs-30">
                                    <label for="asset-sq-ft1-1">Sq. Ft.</label>
                                    <div>
                                        <input class="form-control add-clients-sqft" id="asset-sq-ft1-1"
                                                name="asset_sq_ft1-1" style="width: 100%;">
                                    </div>
                                </div>

                                <div class="col-xs-30">
                                    <label for="asset-status">Condition</label>
                                    <div>
                                        <select class="form-control add-client-cond" id="asset-cond1-1"
                                                name="asset_cond1-1" style="width: 100%;"
                                                data-target="view-asset-cond-info-dialog"></select>
                                    </div>
                                </div>

                                <div class="col-xs-30">
                                    <div>
                                        <label>Criteria</label>
                                    </div>
                                    <select class="form-control client-criteria" id="asset-criteria1-1"
                                            name="asset_criteria1-1" style="width: 100%;"></select>
                                </div>

                                <div class="col-xs-30">
                                    <div>
                                        <label>Lead</label>
                                    </div>
                                    <select class="form-control add-client-lead" id="asset-lead1-1"
                                            name="asset_lead1-1" style="width: 100%;" data-type="lead"
                                            data-target="view-lead-dialog"></select>
                                </div>

                                <div class="col-xs-30">
                                    <div>
                                        <label>Source</label>
                                    </div>
                                    <select class="form-control add-client-source" id="asset-source1-1"
                                            name="asset_source1-1" style="width: 100%;" data-target="view-source-dialog"
                                            data-type="source"></select>
                                </div>
                            </div>

                        </div>

                        <div class="col-xs-12 dummy-asset-info add-client-asset hidden"
                             style="border: 5px solid #008080cf; background: #eee; padding: 10px">
                            <div class="col-xs-14">
                                <label for="client-asset">Asset Name</label>
                                <div>
                                    <input type="text" class="form-control" id="asset-name"
                                           placeholder="Enter Asset Name" name="asset_name"/>
                                </div>
                            </div>

                            <div class="col-xs-14">
                                <label for="address">Asset Address</label>
                                <div>
                                    <input type="text" class="form-control address" id="address"
                                           placeholder="Enter Asset Address" name="asset_address"/>
                                </div>
                            </div>

                            <div class="col-xs-14">
                                <label for="city">City</label>
                                <div>
                                    <input type="text" class="form-control add-client-city" id="asset-city"
                                           placeholder="Enter Asset City" name="asset_city"/>
                                </div>
                            </div>
                            <div class="col-xs-14">
                                <label for="state">State</label>
                                <div>
                                    <input type="text" class="form-control add-client-state" id="asset-state"
                                           placeholder="Enter Asset State" name="asset_state"/>
                                </div>
                            </div>
                            <div class="col-xs-14">
                                <label for="zip">ZIP</label>
                                <div>
                                    <input type="text" class="form-control" id="asset-zip"
                                           placeholder="Enter Asset Zip." name="asset_zip"/>
                                </div>
                            </div>
                            <div class="col-xs-14">
                                <label for="asset-status">Select Status</label>
                                <div>
                                    <select class="form-control client-status" id="asset-status"
                                            name="asset_status" style="width: 100%;"></select>
                                </div>
                            </div>

                            <div class="col-xs-1">
                                <div>
                                    <label></label>
                                </div>
                                <button class="btn btn-primary delete-form-asset" id="delete-asset">Delete Asset
                                </button>
                            </div>

                            <div class="col-xs-30">
                                <label for="client-asset">Product Type</label>
                                <div>
                                    <select id="asset-product" name="asset_product"
                                            class="form-control col-xs-3 add-client-product"
                                            style="display: inline"></select>
                                </div>
                            </div>

                            <div class="col-xs-30">
                                <label for="address">Market Area</label>
                                <div>
                                    <select class="form-control client-location" id="asset-location"
                                            name="asset_location"></select>
                                </div>
                            </div>

                            <div class="col-xs-30">
                                <label for="zip">Units</label>
                                <div>
                                    <input class="form-control add-client-units" id="asset-units"
                                            name="asset_units"/>
                                </div>
                            </div>


                            <div class="col-xs-30">
                                <label for="sq_ft">Sq. Ft.</label>
                                <div>
                                    <input class="form-control add-clients-sqft" id="asset-sq-ft"
                                            name="sq_ft" style="width: 100%;"/>
                                </div>
                            </div>

                            <div class="col-xs-30">
                                <label for="asset-status">Condition</label>
                                <div>
                                    <select class="form-control add-client-cond" id="asset-cond"
                                            name="asset_cond" style="width: 100%;"
                                            data-target="view-asset-cond-info-dialog"></select>
                                </div>
                            </div>

                            <div class="col-xs-30">
                                <div>
                                    <label>Criteria</label>
                                </div>
                                <select class="form-control client-criteria" id="asset-criteria">
                                    name="asset_criteria" style="width: 100%;"></select>
                            </div>

                            <div class="col-xs-30">
                                <div>
                                    <label>Lead</label>
                                </div>
                                <select class="form-control add-client-lead" id="asset-lead"
                                        name="asset_lead" style="width: 100%;" data-type="lead"
                                        data-target="view-lead-dialog"></select>
                            </div>

                            <div class="col-xs-30">
                                <div>
                                    <label>Source</label>
                                </div>
                                <select class="form-control add-client-source" id="asset-source"
                                        name="asset_source" style="width: 100%;" data-type="source"
                                        data-target="view-source-dialog"></select>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="modal-footer" style="margin-top: 2vh">
                    <button type="button" class="btn btn-primary" data-target="#import-excel-dialog"
                            data-toggle="modal" onclick="$('#my_file_input').val('')">Import
                    </button>
                    <button type="submit" id="save_client" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-primary" id="close-add-client-dialog">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php $this->partial('views/homebase/dialogs/import_excel.php'); ?>
