<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/homebase/dialogs/add_buyer.js"></script>

<div class="modal fade" id="add-buyer-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="add-buyer-label" style="overflow-y: auto;" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-xl">
        <div class="modal-content">

            <form id="add-buyer-form" class="form-horizontal">
                <div class="modal-header">
                    <div class="col-xs-12">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h2 id="add-buyer-title" class="modal-title">Add Buyers</h2>

                        <div style="float: right;" class="col-xs-7">
                            <!-- Number of Entries-->
                            <div class="col-xs-12">
                                <div class="col-xs-2" style="float: right;">
                                    <input type="number " id="buyer-count" name="buyer_count" value="1"
                                           class="form-control col-xs-3" style="display: inline">
                                </div>
                                <label for="buyer_count" class="control-label"
                                       style="font-size: 1em; float: right">Create Number of Entries: </label>
                            </div>
                            <!-- //Number of Entries-->
                        </div>
                    </div>
                </div>

                <div class="modal-body modal-small" style="margin-bottom: 10px; background: #eee">
                    <div class="row  add-buyer-modal">
                        <div class="col-xs-12 add-buyer-info buyer-info1 buyer-row" id="add-buyer-1">
                            <div class="row add-buyer-info-row">
                                <div class="col-xs-14">
                                    <label for="buyer-name">Buyer Name</label>
                                    <div>
                                        <input type="text" class="form-control buyer-name" id="buyer-name1"
                                               name="buyer_name1"
                                               placeholder="Enter the Name of the buyer." data-position="1"
                                               data-asset="2"/>
                                    </div>
                                </div>

                                <div class="col-xs-14">
                                    <label for="buyer-phone">Main Phone</label>
                                    <div>
                                        <input type="text" class="form-control" id="buyer-main-phone1"
                                               placeholder="Enter Main Phone" name="main_phone1"/>
                                    </div>
                                </div>

                                <div class="col-xs-14">
                                    <label for="buyer-phone">Second Phone</label>
                                    <div>
                                        <input type="text" class="form-control" id="second-phone1"
                                               placeholder="Enter Second Phone" name="second_phone1"/>
                                    </div>
                                </div>

                                <div class="col-xs-14">
                                    <label for="buyer-mobile-phone">Mobile Phone</label>
                                    <div>
                                        <input type="text" class="form-control" id="buyer-mobile-phone1"
                                               placeholder="Enter Mobile Phone" name="mobile_phone1"/>
                                    </div>
                                </div>

                                <div class="col-xs-14">
                                    <label for="buyer-email">Buyer Email</label>
                                    <div>
                                        <input type="text" class="form-control" id="buyer-email1"
                                               name="buyer_email1" placeholder="Enter Client Email"/>
                                    </div>
                                </div>

                                <div class="col-xs-14">
                                    <label for="entity-name">Entity Name</label>
                                    <div>
                                        <input type="text" class="form-control" id="entity-name1"
                                               name="entity_name1" placeholder="Enter Entity Name"/>
                                    </div>
                                </div>

                                <div class="col-xs-1">
                                    <div>
                                        <div>
                                            <label></label>
                                        </div>
                                        <button class="add-form-asset btn btn-primary" id="add-asset1">Add Asset
                                        </button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 add-buyer-asset"
                                 style="border: 5px solid #008080cf; background: #eee; padding: 10px">
                                <div class="col-xs-14">
                                    <label for="buyer-asset">Entity Name</label>
                                    <div>
                                        <input type="text" class="form-control" id="entity-name1-1"
                                               placeholder="Enter Entity Name" name="entity_name1-1"/>
                                    </div>
                                </div>

                                <div class="col-xs-14">
                                    <label for="address">Entity Address</label>
                                    <div>
                                        <input type="text" class="form-control address" id="entity-address1-1"
                                               placeholder="Enter Entity Address" name="entity_address1-1"/>
                                    </div>
                                </div>

                                <div class="col-xs-14">
                                    <label for="city">City</label>
                                    <div>
                                        <input type="text" class="form-control add-buyer-city" id="city1-1"
                                               placeholder="Enter City" name="entity_city1-1"/>
                                    </div>
                                </div>
                                <div class="col-xs-14">
                                    <label for="state">State</label>
                                    <div>
                                        <input type="text" class="form-control add-buyer-state" id="state1-1"
                                               name="entity_state1-1" placeholder="Enter State"/>
                                    </div>
                                </div>
                                <div class="col-xs-14">
                                    <label for="zip">ZIP</label>
                                    <div>
                                        <input type="text" class="form-control" id="zip1-1"
                                               placeholder="Enter ZIP" name="entity_zip1-1"/>
                                    </div>
                                </div>

                                <div class="col-xs-14">
                                    <label for="buyer-asset-status">Select Status</label>
                                    <div>
                                        <select class="form-control buyer-buyer-asset-status" id="status"
                                                name="status1-1" style="width: 100%;"></select>
                                    </div>
                                </div>

                                <div class="col-xs-1">
                                    <div>
                                        <label></label>
                                    </div>
                                    <button class="btn btn-primary delete-form-asset" id="delete-buyer-entity">Delete Asset
                                    </button>
                                </div>

                                <div class="col-xs-30">
                                    <label for="buyer-asset">Product Type</label>
                                    <div>
                                        <select id="entity-product" name="entity_product1-1"
                                                class="form-control col-xs-3 add-buyer-product"
                                                style="display: inline"></select>
                                    </div>
                                </div>

                                <div class="col-xs-30">
                                    <label for="address">Market Area</label>
                                    <div>
                                        <select class="form-control client-location" id="entity-location1-1"
                                                name="entity_location1-1"></select>
                                    </div>
                                </div>

                                <div class="col-xs-30">
                                    <label for="units">Units</label>
                                    <div>
                                        <input class="form-control add-buyer-units" id="entity-units1-1"
                                               name="entity_unit1-1"/>
                                    </div>
                                </div>


                                <div class="col-xs-30">
                                    <label for="buyer-asset-sq-ft1-1">Sq. Ft.</label>
                                    <div>
                                        <input class="form-control add-buyers-sqft" id="entity-sq-ft1-1"
                                               name="entity_sq_ft1-1" style="width: 100%;">
                                    </div>
                                </div>

                                <div class="col-xs-30">
                                    <label for="buyer-asset-status">Condition</label>
                                    <div>
                                        <select class="form-control add-client-cond" id="entity-cond1-1"
                                                name="entity_cond1-1" style="width: 100%;"
                                                data-target="view-buyer-asset-cond-info-dialog"></select>
                                    </div>
                                </div>

                                <div class="col-xs-30">
                                    <div>
                                        <label>Criteria</label>
                                    </div>
                                    <select class="form-control client-criteria" id="entity-criteria1-1"
                                            name="entity_criteria1-1" style="width: 100%;"></select>
                                </div>

                                <div class="col-xs-30">
                                    <div>
                                        <label>Lead</label>
                                    </div>
                                    <select class="form-control add-client-lead" id="entity-lead"
                                            name="entity_lead1-1" style="width: 100%;" data-type="lead"
                                            data-target="view-lead-dialog"></select>
                                </div>

                                <div class="col-xs-30">
                                    <div>
                                        <label>Source</label>
                                    </div>
                                    <select class="form-control add-client-source" id="entity-source"
                                            name="entity_source1-1" style="width: 100%;" data-target="view-source-dialog"
                                            data-type="source"></select>
                                </div>
                            </div>

                        </div>


                        <div class="col-xs-12 dummy-buyer-asset hidden"
                             style="border: 5px solid #008080cf; background: #eee; padding: 10px">
                            <div class="col-xs-14">
                                <label for="buyer-asset">Entity Name</label>
                                <div>
                                    <input type="text" class="form-control" id="entity-name"
                                           placeholder="Enter Entity Name" name="entity_name"/>
                                </div>
                            </div>

                            <div class="col-xs-14">
                                <label for="address">Entity Address</label>
                                <div>
                                    <input type="text" class="form-control address" id="entity-address"
                                           placeholder="Enter Entity Address" name="entity_address"/>
                                </div>
                            </div>

                            <div class="col-xs-14">
                                <label for="city">City</label>
                                <div>
                                    <input type="text" class="form-control add-buyer-city" id="city"
                                           placeholder="Enter City" name="entity_city"/>
                                </div>
                            </div>
                            <div class="col-xs-14">
                                <label for="state">State</label>
                                <div>
                                    <input type="text" class="form-control add-buyer-state" id="state"
                                           name="entity_state" placeholder="Enter State"/>
                                </div>
                            </div>
                            <div class="col-xs-14">
                                <label for="zip">ZIP</label>
                                <div>
                                    <input type="text" class="form-control" id="zip"
                                           placeholder="Enter ZIP" name="entity_zip"/>
                                </div>
                            </div>

                            <div class="col-xs-14">
                                <label for="buyer-asset-status">Select Status</label>
                                <div>
                                    <select class="form-control buyer-buyer-asset-status" id="status"
                                            name="status" style="width: 100%;"></select>
                                </div>
                            </div>

                            <div class="col-xs-1">
                                <div>
                                    <label></label>
                                </div>
                                <button class="btn btn-primary delete-form-asset" id="delete-buyer-entity">Delete Asset
                                </button>
                            </div>

                            <div class="col-xs-30">
                                <label for="buyer-asset">Product Type</label>
                                <div>
                                    <select id="entity-product" name="entity_product"
                                            class="form-control col-xs-3 add-buyer-product"
                                            style="display: inline"></select>
                                </div>
                            </div>

                            <div class="col-xs-30">
                                <label for="address">Market Area</label>
                                <div>
                                    <select class="form-control client-location" id="entity-location"
                                            name="entity_location"></select>
                                </div>
                            </div>

                            <div class="col-xs-30">
                                <label for="units">Units</label>
                                <div>
                                    <input class="form-control add-buyer-units" id="entity-units"
                                           name="entity_unit"/>
                                </div>
                            </div>

                            <div class="col-xs-30">
                                <label for="buyer-asset-sq-ft1-1">Sq. Ft.</label>
                                <div>
                                    <input class="form-control add-buyers-sqft" id="entity-sq-ft"
                                           name="entity_sq_ft" style="width: 100%;">
                                </div>
                            </div>

                            <div class="col-xs-30">
                                <label for="buyer-asset-status">Condition</label>
                                <div>
                                    <select class="form-control add-client-cond" id="entity-cond"
                                            name="entity_cond" style="width: 100%;"
                                            data-target="view-buyer-asset-cond-info-dialog"></select>
                                </div>
                            </div>

                            <div class="col-xs-30">
                                <div>
                                    <label>Criteria</label>
                                </div>
                                <select class="form-control client-criteria" id="entity-criteria"
                                        name="entity_criteria" style="width: 100%;"></select>
                            </div>

                            <div class="col-xs-30">
                                <div>
                                    <label>Lead</label>
                                </div>
                                <select class="form-control add-client-lead" id="entity-lead"
                                        name="entity_lead" style="width: 100%;" data-type="lead"
                                        data-target="view-lead-dialog"></select>
                            </div>

                            <div class="col-xs-30">
                                <div>
                                    <label>Source</label>
                                </div>
                                <select class="form-control add-client-source" id="entity-source"
                                        name="entity_source" style="width: 100%;" data-target="view-source-dialog"
                                        data-type="source"></select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer" style="margin-top: 2vh">
                    <button type="button" class="btn btn-primary" data-target="#import-buyer-excel-dialog"
                            data-toggle="modal" onclick="$('#my_file_input').val('')">Import
                    </button>
                    <button type="submit" id="save_buyer" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-primary" id="close-add-buyer-dialog">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php $this->partial('views/homebase/dialogs/import_buyer_excel.php'); ?>
