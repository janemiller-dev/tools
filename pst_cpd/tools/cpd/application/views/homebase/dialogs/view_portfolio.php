<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<div class="modal fade" id="view-portfolio-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="message-label"
     aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="view-portfolio-title" class="modal-title">View/Edit Portfolio</h2>
                <button type="button" id="add-portfolio-button" class="btn btn-primary bottom-buffer"
                        data-target="#add-portfolio-dialog" data-toggle="modal" data-backdrop="static">Add New Portfolio
                </button>

            </div>

            <div class="modal-body">

                <!-- Show the list of portfolios. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- List Portfolio table -->
                        <div class="row" id="portfolio-row">
                            <div class="col-xs-12">
                                <p id="no-portfolio">You have no instance of Portfolio. Use the add portfolio to add a new
                                    portfolio.</p>
                                <div id="portfolio-div" class="top-buffer">
                                    <table class="table" id="portfolio-dialog-table">
                                        <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Portfolio Name</th>
                                            <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- List Portfolio table -->
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php $this->partial('views/homebase/dialogs/delete_portfolio.php'); ?>
<?php $this->partial('views/homebase/dialogs/add_portfolio.php'); ?>
