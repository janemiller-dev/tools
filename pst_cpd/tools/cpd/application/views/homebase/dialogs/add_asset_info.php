<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/homebase/dialogs/add_asset_info.js"></script>

<div class="modal fade" id="add-asset-info-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="add-asset-info-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="add-asset-info-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-asset-info-title" class="modal-title"></h2>
                </div>

                <div class="modal-body modal-small">

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="asset-info-name" id="label-asset-info-name"></label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="asset-info-name" name="asset_info_name"/>
                            <p class="form-text text-muted" id="para-asset-info">Enter the Name of the Portfolio.</p>
                            <input type="hidden" name="info_type" id="asset-info-type">
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="close-add-asset-info-dialog" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
