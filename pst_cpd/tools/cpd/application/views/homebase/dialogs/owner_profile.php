<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

    <!-- Load the javascript support. -->
    <script src="<?php echo $this->basepath; ?>resources/app/js/homebase/dialogs/owner_profile.js"></script>

    <div class="modal fade" id="view-owner-profile-dialog" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="message-label" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <h2 id="view-owner-profile-title" class="modal-title">Owner Profile</h2>
                    <button type="button" id="edit-owner-profile-button" class="btn btn-primary bottom-buffer"
                            data-target="#edit-owner-profile-dialog" data-toggle="modal" data-backdrop="static">
                        Edit Profile
                    </button>

                </div>

                <div class="modal-body">

                    <!-- Show the list of owner-profiles. -->
                    <div class="row">
                        <div class="col-xs-12">

                            <!-- List Portfolio table -->
                            <div class="row" id="owner-profile-row">
                                <div class="col-xs-12">
                                    <div id="owner-profile-div" class="top-buffer">
                                        <div class="col-xs-12">
                                            <div class="col-xs-5">
                                                <div class="col-xs-4 text-center" id="image-div">
                                                    <img src="<?php echo $this->basepath; ?>resources/app/media/user-avatar.svg"
                                                         id="owner-avatar" style="width: 6.5vw">
                                                    <div class="upload">
                                                        <input id="owner-img-upload" type="file" name="files[]"
                                                               data-url= <?= "https://" . $_SERVER['SERVER_NAME'] .
                                                               "/tools/homebase/upload_owner_image" ?>
                                                               style="visibility: hidden; float:left; width:0;">
                                                        <a id="upload-owner-img">Change</a>
                                                    </div>
                                                </div>
                                                <div class="col-xs-4" id="owner-info">
                                                </div>
                                                <div class="col-xs-4" id="owner-contact">
                                                </div>
                                            </div>

                                            <!-- Drop Down boxes-->
                                            <div class="col-xs-7">
                                                <!-- Partner Drop down. -->
                                                <div class="col-xs-6">
                                                    <label class="info-label">Partners</label>
                                                    <select class="form-control owner-info-select"
                                                            id="owner-info-Partner"
                                                            data-name="Partner">
                                                        <option value="-1">-- Select Partner --</option>
                                                        <option value="0">--Add New--</option>
                                                    </select>
                                                </div>
                                                <!--// Partner Drop down. -->

                                                <!-- Staff Drop down. -->
                                                <div class="col-xs-6">
                                                    <label class="info-label">Staff</label>
                                                    <select class="form-control owner-info-select" id="owner-info-Staff"
                                                            data-name="Staff">
                                                        <option value="-1">-- Select Staff --</option>
                                                        <option value="0">--Add New--</option>
                                                    </select>
                                                </div>
                                                <!-- // Staff Drop down. -->

                                                <!-- Advisor Drop down. -->
                                                <div class="col-xs-6">
                                                    <label class="info-label" id="info-advisor-label">Advisors</label>
                                                    <select class="form-control owner-info-select"
                                                            id="owner-info-Advisor"
                                                            data-name="Advisor">
                                                        <option value="-1">-- Select Advisor --</option>
                                                        <option value="0">--Add New--</option>
                                                    </select>
                                                </div>
                                                <!-- // Advisor Drop down. -->

                                                <!-- Vendor Drop down. -->
                                                <div class="col-xs-6">
                                                    <label class="info-label" id="info-vendor-label">Vendors</label>
                                                    <select class="form-control owner-info-select"
                                                            id="owner-info-Vendor"
                                                            data-name="Vendor">
                                                        <option value="-1">-- Select Vendor --</option>
                                                        <option value="0">--Add New--</option>
                                                    </select>
                                                </div>
                                                <!-- // Vendor Drop down. -->
                                            </div>
                                            <!--// Drop Down boxes-->

                                        </div>

                                        <div class="col-xs-12" id="buttons-div" style="margin: 35px 0 35px 0">
                                            <div class="col-xs-2 text-center">
                                                <button id="owner-project" data-toggle="popover" data-type="project"
                                                        data-html="true" data-placement="bottom"
                                                        class="btn btn-sm btn-default profile-btn"
                                                        data-title="<h3>Owner Project</h3>"
                                                        data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>History:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info' data-type='history'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Facts:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                owner-popup-info' data-type='facts'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Accomplished:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                owner-popup-info'
                                                                                data-type='accomplished'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Problems:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                owner-popup-info' data-type='problem'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Project:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                owner-popup-info' data-type='project'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>">
                                                    <span class="glyphicon glyphicon-list-alt"></span>
                                                </button>
                                                <label>Owner Project</label>
                                            </div>

                                            <div class="col-xs-2 text-center">
                                                <button id="owner-challenge" data-type="challenge"
                                                        data-toggle="popover" data-type="project"
                                                        class="btn btn-sm btn-default profile-btn"
                                                        data-html="true" data-placement="bottom"
                                                        data-title="<h3>Owner Challenges</h3>"
                                                        data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Asset Challenges:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                owner-popup-info'
                                                                                data-type='asset-challenge'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>People Challenges:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                owner-popup-info'
                                                                                data-type='people-challenge'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Financial Challenges:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                owner-popup-info'
                                                                                data-type='financial-challenge'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Legal Challenges:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                owner-popup-info'
                                                                                data-type='legal-challenge'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Market Challenges:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                owner-popup-info'
                                                                                data-type='market-challenge'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>">
                                                    <span class="glyphicon glyphicon-list-alt"></span>
                                                </button>
                                                <label>Owner Challenges</label>
                                            </div>

                                            <div class="col-xs-2 text-center">
                                                <button id="strategic-objective"
                                                        data-type="objective" data-toggle="popover"
                                                        class="btn btn-sm btn-default profile-btn"
                                                        data-html="true" data-placement="bottom"
                                                        data-title="<h3>Strategic Objectives</h3>"
                                                        data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Strategic Objectives:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                owner-popup-info'
                                                                                 data-type='strategic-objective'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Disposition Objectives:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='disposition-objective'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Acquisition Objectives:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='acquisition-objective'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Exit Strategy:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='exit-strategy'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Beneficiaries:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='beneficiaries'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>">
                                                    <span class="glyphicon glyphicon-list-alt"></span>
                                                </button>
                                                <label>Strategic Objectives</label>
                                            </div>

                                            <div class="col-xs-2 text-center">
                                                <button id="transaction-history" data-toggle="popover"
                                                        data-html="true" data-placement="bottom" data-type="history"
                                                        data-title="<h3>Transaction History</h3>"
                                                        class="btn btn-sm btn-default profile-btn"
                                                        data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Disposition History:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='disposition-history'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Acquisition History:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='acquisition-history'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Partnership History:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='partnership-history'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Foreclosure History:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='foreclosure-history'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Bankruptcy History:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='bankruptcy-history'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>">
                                                    <span class="glyphicon glyphicon-list-alt"></span>
                                                </button>
                                                <label>Transaction History</label>
                                            </div>

                                            <div class="col-xs-2 text-center">
                                                <button id="acquistion-criteria" data-toggle="popover"
                                                        data-html="true" data-placement="bottom" data-type="history"
                                                        data-title="<h3>Acquisition Criteria</h3>"
                                                        class="btn btn-sm btn-default profile-btn"
                                                        data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Locations:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='locations'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Product Types:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='product-type'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Asset Specifications:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='asset-specifications'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Financing Methods:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='financing-method'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Offer Stipulations:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='offer-stipulations'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>">
                                                    <span class="glyphicon glyphicon-list-alt"></span>
                                                </button>
                                                <label>Acquisition Criteria</label>
                                            </div>

                                            <div class="col-xs-2 text-center">
                                                <button id="owner-project" data-toggle="popover"
                                                        data-html="true" data-placement="bottom" data-type="history"
                                                        data-title="<h3>Other Research</h3>"
                                                        class="btn btn-sm btn-default profile-btn"
                                                        data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Websites:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='website'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Media Exposure:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='media-exposure'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Social Media Profiles:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='social-media-profile'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>People’s Comments:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='people-comment'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Public Records:</b>
                                                                                </label>
                                                                                <div class='input-group'>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='public-record'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                 <span class='input-group-addon btn
                                                                                btn-secondary stt'>
                                                                                <i class='fa fa-microphone'></i>
                                                                                </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>">
                                                    <span class="glyphicon glyphicon-list-alt"></span>
                                                </button>
                                                <label>Other Research</label>
                                            </div>
                                        </div>
                                        <div style="margin: 10px">
                                            <div class="col-xs-offset-4 col-xs-4">
                                                <label style="color: #5A5A5A; font-size: 25px">Deal Management
                                                    Dashboard</label>
                                            </div>

                                            <div class="col-xs-4" style="text-align: right">
                                                <button class="btn btn-primary hidden" id="enable-poi">Show POI</button>
                                                <button class="btn btn-primary hidden" id="disable-poi">Hide POI
                                                </button>
                                                <button class="btn btn-primary" id="owner-asset-pics">DEALS</button>
                                                <button class="btn btn-primary" id="owner-asset-map">MAP</button>
                                                <button class="btn btn-primary" id="owner-asset-info">INFO</button>
                                            </div>
                                        </div>
                                        <div class="col-xs-12" id="owner-pics-div" style="border: 5px solid #A4A4A4">
                                            <ul class="nav nav-tabs top-buffer" role="tablist"
                                                style="padding: 10px; margin: auto; display: flex;
                                                 justify-content: center;" id="owner-pics-nav">
                                                <li role="presentation" data-type="property" class="active">
                                                    <a href="#owner-property-view" role="tab" data-toggle="tab">Deals In Play</a>
                                                </li>
                                                <li role="presentation" data-type="market">
                                                    <a href="#owner-market-view" role="tab" data-toggle="tab">Deals Inactive</a>
                                                </li>
                                            </ul>

                                            <div class="col-xs-4 text-center">
                                                <h4><b>Asset Listed or In Contract</b></h4>
                                                <table id="owner-contract-table" class="table"
                                                       style="border-collapse: separate; border-spacing: 0 0.5em;"></table>
                                            </div>
                                            <div class="col-xs-4 text-center">
                                                <h4><b>Assets Proposed or Strategic</b></h4>
                                                <table id="owner-proposed-table" class="table"
                                                       style="border-collapse: separate; border-spacing: 0 0.5em;"></table>
                                            </div>
                                            <div class="col-xs-4 text-center">
                                                <h4><b>Assets Closed or Inactive</b></h4>
                                                <table id="owner-complete-table" class="table"
                                                       style="border-collapse: separate; border-spacing: 0 0.5em;"></table>
                                            </div>
                                        </div>

                                        <div class="col-xs-12 hidden" id="owners-map-div"
                                             style="border: 5px solid #A4A4A4; height: 70vh; padding: 10px">
                                        </div>

                                        <div class="col-xs-12 hidden" id="owners-info-div"
                                             style="border: 5px solid #A4A4A4; height: 70vh; padding: 10px">
                                            <div class="row" id="owner-profile-info">
                                                <ul class="nav nav-tabs top-buffer" role="tablist" style="padding: 10px; margin: auto;
                 display: flex; justify-content: center;" id="owner-profile-nav">
                                                    <li role="presentation" class="active"
                                                        data-table="homebase_client_asset" data-key="hca_id">
                                                        <a href="#owner-property-view" role="tab"
                                                           data-toggle="tab">Asset</a>
                                                    </li>
                                                    <li role="presentation"
                                                        data-table="agent_asset_info" data-key="aai_hca_id">
                                                        <a href="#owner-asset-view" role="tab"
                                                           data-toggle="tab">Details</a>
                                                    </li>
                                                    <li role="presentation"
                                                        data-table="agent_asset_info" data-key="aai_hca_id">
                                                        <a href="#owner-people-view" role="tab"
                                                           data-toggle="tab">People</a>
                                                    </li>
                                                    <li role="presentation"
                                                        data-table="agent_asset_info" data-key="aai_hca_id">
                                                        <a href="#owner-financial-view" role="tab" data-toggle="tab">Financial</a>
                                                    </li>
                                                    <li role="presentation"
                                                        data-table="agent_asset_info" data-key="aai_hca_id">
                                                        <a href="#owner-legal-view" role="tab"
                                                           data-toggle="tab">Legal</a>
                                                    </li>
                                                    <li role="presentation"
                                                        data-table="agent_asset_info" data-key="aai_hca_id">
                                                        <a href="#owner-market-view" role="tab"
                                                           data-toggle="tab">Market</a>
                                                    </li>
                                                </ul>

                                                <div class="tab-content profile-readout-tab">

                                                    <!-- Property Tab-->
                                                    <div role="tabpanel" class="tab-pane col-xs-4 col-xs-offset-4 active"
                                                         id="owner-property-view">
                                                        <h3 style="text-align: center; padding: 5px"><b>Property
                                                                Overview</b></h3>
                                                        <div class="col-xs-12">
                                                            <div>
                                                                <h4 class="col-xs-5">Property Type</h4>
                                                                <div class="col-xs-7" style="padding: 5px;">
                                                                    <input type="text" class="form-control owner-asset-input"
                                                                           data-col="hca_type"/>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <h4 class="col-xs-5">Property Name</h4>
                                                                <div class="col-xs-7" style="padding: 5px;">
                                                                    <input type="text" class="form-control owner-asset-input"
                                                                           data-col="hca_name"/>
                                                                </div>
                                                            </div>
                                                            <h4 class="col-xs-5">Region/MSA</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="hca_location"/>
                                                            </div>
                                                            <div>
                                                                <h4 class="col-xs-5">Address</h4>
                                                                <div class="col-xs-7" style="padding: 5px;">
                                                                    <input type="text" class="form-control owner-asset-input"
                                                                           data-col="hca_address"/>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <h4 class="col-xs-5">City</h4>
                                                                <div class="col-xs-7" style="padding: 5px;">
                                                                    <input type="text" class="form-control owner-asset-input"
                                                                           data-col="hca_city"/>
                                                                </div>
                                                            </div>
                                                            <h4 class="col-xs-5">State</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="hca_state"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Zip</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="hca_zip"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--// Property Tab-->

                                                    <!-- Assets tab -->
                                                    <div role="tabpanel"
                                                         class="tab-pane col-xs-8 col-xs-offset-2 "
                                                         id="owner-asset-view">
                                                        <h3 style="text-align: center; padding: 5px"><b>Asset
                                                                Details</b></h3>
                                                        <div class="col-xs-6">
                                                            <div>
                                                                <h4 class="col-xs-5">Year Built</h4>
                                                                <div class="col-xs-7" style="padding: 5px;">
                                                                    <input type="text" class="form-control owner-asset-input"
                                                                           data-col="aai_built_year"/>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <h4 class="col-xs-5">Building Size</h4>
                                                                <div class="col-xs-7" style="padding: 5px;">
                                                                    <input type="text" class="form-control owner-asset-input"
                                                                           data-col="aai_building_size"/>
                                                                </div>
                                                            </div>
                                                            <h4 class="col-xs-5">Lot Size</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_lot_size"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Square Footage</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_avg_sf"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Rentable SF</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_rentable_sf"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Useable SF</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_usable_sf"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Amenities Inside</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_amenities_inside"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Amenities Outside</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_amenities_outside"/>
                                                            </div>
                                                        </div>

                                                        <div class="col-xs-6">

                                                            <h4 class="col-xs-5">Units</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_units"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Interstate Access</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_interstate_access"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Parking Ratio</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_parking_ratio"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Floors</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_floors"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Sprinkler System</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_sprinkler"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Expansion</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_expansion"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Unit Condition</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_unit_condition"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Grounds Condition</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_ground_condition"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--// Assets tab -->

                                                    <!-- People Tab-->
                                                    <div role="tabpanel" class="tab-pane col-xs-4 col-xs-offset-4"
                                                         id="owner-people-view">
                                                        <h3 style="text-align: center; padding: 5px"><b>People
                                                                Involved</b></h3>
                                                        <div class="col-xs-12">
                                                            <div>
                                                                <h4 class="col-xs-5">Owners</h4>
                                                                <div class="col-xs-7" style="padding: 5px;">
                                                                    <input type="text" class="form-control owner-asset-input"
                                                                           data-col="aai_owner"/>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <h4 class="col-xs-5">Ownership</h4>
                                                                <div class="col-xs-7" style="padding: 5px;">
                                                                    <input type="text" class="form-control owner-asset-input"
                                                                           data-col="aai_ownership"/>
                                                                </div>
                                                            </div>
                                                            <h4 class="col-xs-5">Management</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_management"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Attorneys</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_attorney"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Accountants</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_accountant"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Investors</h4>
                                                            <div class="col-xs-7" style="padding:5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_investor"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--// People Tab-->

                                                    <!-- Financial Tab-->
                                                    <div role="tabpanel" class="tab-pane col-xs-4 col-xs-offset-4"
                                                         id="owner-financial-view">
                                                        <h3 style="text-align: center; padding: 5px"><b>Financial
                                                                Overview</b></h3>
                                                        <div class="col-xs-12">
                                                            <div>
                                                                <h4 class="col-xs-5">Occupancy</h4>
                                                                <div class="col-xs-7" style="padding: 5px;">
                                                                    <input type="text" class="form-control owner-asset-input"
                                                                           data-col="aai_occupancy"/>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <h4 class="col-xs-5">Lease Type</h4>
                                                                <div class="col-xs-7" style="padding: 5px;">
                                                                    <input type="text" class="form-control owner-asset-input"
                                                                           data-col="aai_lease_type"/>
                                                                </div>
                                                            </div>
                                                            <h4 class="col-xs-5">Rental Rate</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_rental_rate"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Expenses</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_expenses"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Cash Flow</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_cash_flow"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Lender</h4>
                                                            <div class="col-xs-7" style="padding:5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_lender"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Debt Service</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_debt_service"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Total Debt</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_total_debt"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Prepayment</h4>
                                                            <div class="col-xs-7" style="padding:5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_prepayment"/>
                                                            </div>
                                                            <h4 class="col-xs-5">CAP Rate</h4>
                                                            <div class="col-xs-7" style="padding:5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_cap"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--// Financial Tab-->

                                                    <!-- Legal Tab-->
                                                    <div role="tabpanel" class="tab-pane col-xs-4 col-xs-offset-4"
                                                         id="owner-legal-view">
                                                        <h3 style="text-align: center; padding: 5px"><b>Legal Status</b>
                                                        </h3>
                                                        <div class="col-xs-12">
                                                            <div>
                                                                <h4 class="col-xs-5">Tenant Status</h4>
                                                                <div class="col-xs-7" style="padding: 5px;">
                                                                    <input type="text" class="form-control owner-asset-input"
                                                                           data-col="aai_tenant_status"/>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <h4 class="col-xs-5">Easements</h4>
                                                                <div class="col-xs-7" style="padding: 5px;">
                                                                    <input type="text" class="form-control owner-asset-input"
                                                                           data-col="aai_easement"/>
                                                                </div>
                                                            </div>
                                                            <h4 class="col-xs-5">Environmental</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_environmental"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--// Legal Tab-->

                                                    <!-- Market Tab-->
                                                    <div role="tabpanel" class="tab-pane col-xs-4 col-xs-offset-4"
                                                         id="owner-market-view">
                                                        <h3 style="text-align: center; padding: 5px"><b>Market
                                                                Issues</b></h3>
                                                        <div class="col-xs-12">
                                                            <h4 class="col-xs-5">Neighborhood</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_neighborhood"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Location Rating</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_location_rating"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Competition</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_competition"/>
                                                            </div>
                                                            <div>
                                                                <h4 class="col-xs-5">Demographics</h4>
                                                                <div class="col-xs-7" style="padding: 5px;">
                                                                    <input type="text" class="form-control owner-asset-input"
                                                                           data-col="aai_demographics"/>
                                                                </div>
                                                            </div>
                                                            <div>
                                                                <h4 class="col-xs-5">Crime</h4>
                                                                <div class="col-xs-7" style="padding: 5px;">
                                                                    <input type="text" class="form-control owner-asset-input"
                                                                           data-col="aai_crime"/>
                                                                </div>
                                                            </div>
                                                            <h4 class="col-xs-5">Proximity</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_proximity"/>
                                                            </div>
                                                            <h4 class="col-xs-5">Access</h4>
                                                            <div class="col-xs-7" style="padding: 5px;">
                                                                <input type="text" class="form-control owner-asset-input"
                                                                       data-col="aai_access"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--// Market Tab-->
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- List Portfolio table -->
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php $this->partial('views/homebase/dialogs/edit_owner_profile.php'); ?>