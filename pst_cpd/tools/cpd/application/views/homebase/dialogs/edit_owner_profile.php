<!-- Add Client Info/reference Instance.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/homebase/dialogs/edit_owner_profile.js"></script>

<div class="modal fade" id="edit-owner-profile-dialog" tabindex="10" role="dialog"
     aria-labelledby="edit-owner-profile-label"
     aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <form id="edit-owner-profile-form" class="form-horizontal">

                <!-- Modal Header section-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="modal-title">Edit Owner Profile</h2>

                </div>
                <!-- Modal Header section-->

                <div class="modal-body" style="max-height:70vh; overflow-y: scroll">
                    <!-- Name Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="owner-name">Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="owner-name" name="hc_name"/>
                            <p class="form-text text-muted">Enter Owner Name.</p>
                        </div>
                    </div>
                    <!--// Name Field -->

                    <!-- Title Field-->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="owner-title">Title</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" id="owner-title" name="hc_title">
                            <p class="form-text text-muted">Enter Owner Title.</p>
                        </div>
                    </div>
                    <!--// Title Field -->

                    <!-- Entity Field-->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="entity-name">Entity Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="entity-name" name="hc_entity_name"/>
                            <p class="form-text text-muted">Enter Entity Name.</p>
                        </div>
                    </div>
                    <!--// Entity Field-->

                    <!-- Address Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="owner-address">Address</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="owner-address" name="hc_entity_address"/>
                            <p class="form-text text-muted">Enter address</p>
                        </div>
                    </div>
                    <!-- // Address Field -->

                    <!-- Office Phone Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="owner-office-phone">Office Phone</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="owner-office-phone" name="hc_main_phone"/>
                            <p class="form-text text-muted">Enter Office Phone</p>
                        </div>
                    </div>
                    <!--// Office Phone Field -->

                    <!-- Cell Phone Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="owner-cell-phone">Cell Phone</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="owner-cell-phone" name="hc_second_phone"/>
                            <p class="form-text text-muted">Enter Cell Phone.</p>
                        </div>
                    </div>
                    <!-- // Cell Phone Field -->

                    <!-- Email Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="owner-email">Email</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="email" class="form-control" id="owner-email" name="hc_client_email"/>
                            <p class="form-text text-muted">Enter Email.</p>
                        </div>
                    </div>
                    <!-- // Email Field -->

                    <!-- Website Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="owner-website">Website</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="url" class="form-control" id="owner-website" name="hc_website"/>
                            <p class="form-text text-muted">Enter Website</p>
                        </div>
                    </div>
                    <!-- // Website Field -->

                    <!-- Messaging Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="owner-messaging">Messaging</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="owner-messaging" name="hc_messaging"/>
                            <p class="form-text text-muted">Enter Messaging</p>
                        </div>
                    </div>
                    <!-- // Messaging Field -->

                    <!-- Social Media Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="owner-social-media">Social Media</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="owner-social-media" name="hc_social"/>
                            <p class="form-text text-muted">Enter Social Media</p>
                        </div>
                    </div>
                    <!-- // Social Media Field -->

                    <input type="hidden" name="hc_id" id="owner-id"/>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="button" id="save-owner-info" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
