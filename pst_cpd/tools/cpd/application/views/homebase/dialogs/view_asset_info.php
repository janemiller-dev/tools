<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/homebase/dialogs/view_asset_info.js"></script>

<div class="modal fade" id="view-asset-cond-info-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="message-label"
     aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="view-asset-info-title" class="modal-title"></h2>
                <button type="button" id="add-asset-info-button" class="btn btn-primary bottom-buffer"
                        data-target="#add-asset-info-dialog" data-toggle="modal" data-backdrop="static">
                </button>

            </div>

            <div class="modal-body">

                <!-- Show the list of asset-infos. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- List Portfolio table -->
                        <div class="row" id="asset-info-row">
                            <div class="col-xs-12">
                                <div id="asset-info-div" class="top-buffer">
                                    <table class="table" id="asset-info-dialog-table">
                                        <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center" id="name-col-head"></th>
                                            <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- List Portfolio table -->
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php $this->partial('views/homebase/dialogs/delete_asset_info.php'); ?>
<?php $this->partial('views/homebase/dialogs/add_asset_info.php'); ?>
