<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>
<script src="<?php echo $this->basepath; ?>resources/app/js/homebase/dialogs/import_excel.js"></script>
<script src="https://unpkg.com/xlsx/dist/xlsx.full.min.js"></script>

<div class="modal fade" id="import-excel-dialog" tabindex="-1" data-backdrop="static" role="dialog"
     aria-labelledby="message-label" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="view-assignee-title" class="modal-title">Import Excel</h2>

            </div>

            <div class="modal-body">

                <!-- Show the list of assignees. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- List Assignee table -->
                        <div class="row" id="assignee-row">
                            <div class="col-xs-12">
                                <p id="no-assignee">You can import clients from excel sheets here.
                                    To import clients select Excel form using Choose File button.</p>
                                <p>You may download the Excel Form using<b> Download Form </b>Button.</p>
                                <input type="file" id="my_file_input" accept=".xlsx, .xls, .csv" class="hidden"/>
                                <button class="btn btn-default" onclick="$('#my_file_input').trigger('click')">Choose
                                    File
                                </button>

                                <div id="assignee-div" class="top-buffer">
                                    <table id='my_file_output'></table>
                                </div>
                            </div>
                        </div>
                        <!-- List Assignee table -->
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary has-popover" id="view_sample" data-toggle="popover"
                        data-container="body" data-title="<h3>View Import Form.</h3>" data-html="true"
                        data-placement="bottom">View Form
                </button>
                <button type="button" class="btn btn-primary" id="download_sample">Download Form</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
