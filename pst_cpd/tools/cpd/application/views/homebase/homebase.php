<!-- Homebase Deal Listing Page -->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>
<!-- Load the custom css. -->
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/homebase/homebase.css?v=1"/>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/homebase/homebase.js"></script>

<!-- Page Title -->
<div class="row">
    <div class="col-xs-12 text-center" style="margin-bottom: 2vh">
        <h1>HOMEBASE DATA CENTER</h1>
        <span style="font-size: 1.2em;">Active Deal Management Dashboard:&nbsp;
            <span id="linked-cpd"></span>
        </span>
    </div>
</div>

<!-- Show the add Objective button. -->
<div class="row"
     style="border-bottom: 1px solid #e1c8c8; border-top: 1px solid #e1c8c8; padding-bottom: 5px; padding-top: 5px;">
    <div class="col-lg-12">
        <div class="col-lg-2 text-center"
             style="border-right: 1px solid #e1c8c8; border-left: 1px solid #e1c8c8; background: #8ff1fd17">
            <h5 class="col-lg-12"><br></h5>
            <button type="button" id="add-homebase-client" class="btn btn-primary bottom-buffer"
                    data-toggle="modal" data-target="#add-client-dialog" data-backdrop="static">Add Client
            </button>
            <button type="button" id="add-homebase-buyer" class="btn btn-primary bottom-buffer hidden"
                    data-toggle="modal" data-target="#add-buyer-dialog" data-backdrop="static">Add Buyer
            </button>
        </div>
        <div class="col-lg-8" style="border-right: 1px solid #e1c8c8;">
            <div class="col-xs-20">
                <h5 class="text-center col-lg-12">Sort By Client Name</h5>
                <select class="form-control client-sort col-xs-6 filter-homebase" id="filter-sort">
                    <option value="-1">-- None --</option>
                    <option value="hc_name" data-order="asc">Names Ascending</option>
                    <option value="hc_name" data-order="desc">Names Descending</option>
                </select>
            </div>

            <div class="col-xs-20">
                <h5 class="text-center col-lg-12">Sort By City</h5>
                <select class="form-control client-city col-xs-6 filter-homebase" id="filter-city">
                    <option value="-1">-- None --</option>
                </select>
            </div>

            <div class="col-xs-20">
                <h5 class="text-center col-lg-12">Sort By State</h5>
                <select class="form-control client-state col-xs-6 filter-homebase" id="filter-state">
                    <option value="-1">-- None --</option>
                </select>
            </div>

            <div class="col-xs-20">
                <h5 class="text-center col-lg-12">Sort By Zip</h5>
                <select class="form-control client-zip col-xs-6 filter-homebase" id="filter-zip">
                    <option value="-1">-- None --</option>
                </select>
            </div>

            <div class="col-xs-30">
                <h5 class="text-center col-lg-12">Sort By Unit</h5>
                <select class="form-control col-xs-6 filter-homebase" id="filter-unit">
                    <option value="-1">-- None --</option>
                </select>
            </div>

            <div class="col-xs-30">
                <h5 class="text-center col-lg-12">Sort Sq. Ft.</h5>
                <select class="form-control col-xs-6 filter-homebase" id="filter-ft">
                    <option value="-1">-- None --</option>
                </select>
            </div>
        </div>

        <div class="col-lg-2">
            <h5 class="col-lg-12 text-center">Sort By Condition</h5>
            <select class="form-control client-cond col-xs-6 filter-homebase" id="filter-cond">
                <option value="-1">-- None --</option>
            </select>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="col-xs-14">
            <h5 class="text-center col-lg-12">Sort By Product Type</h5>
            <select class="form-control client-product col-xs-6 filter-homebase"
                    data-key="hca_product_id" id="filter-product"></select>
        </div>
        <div class="col-xs-14">
            <h5 class="text-center col-lg-12">Sort By Market Area</h5>
            <select class="form-control client-location filter-homebase"
                    data-key="hca_location" id="filter-location"></select>
        </div>
        <div class="col-xs-14">
            <h5 class="text-center col-lg-12">Sort By Criteria</h5>
            <select class="form-control client-criteria filter-homebase"
                    data-key="hca_criteria_used" id="filter-criteria"></select>
        </div>
        <div class="col-xs-14">
            <h5 class="text-center col-lg-12">Sort By Tool</h5>
            <select class="form-control client-tool filter-homebase"
                    data-key="hca_tool" id="filter-tool"></select>
        </div>
        <div class="col-xs-14">
            <h5 class="text-center col-lg-12">Sort By Status</h5>
            <select class="form-control client-status col-xs-6 filter-homebase"
                    data-key="has_status" id="filter-status"></select>
        </div>
        <div class="col-xs-14">
            <h5 class="text-center col-lg-12">Sort By RWT</h5>
            <select class="form-control col-xs-6 filter-homebase"
                    data-key="hca_rwt" id="filter-rwt">
                <option value="">--None--</option>
                <option value="R">R</option>
                <option value="W">W</option>
                <option value="T">T</option>
            </select>
        </div>
        <div class="col-xs-14">
            <h5 class="col-lg-12 text-center">Search Client or Entity</h5>
            <div class="input-group">
                <input type="text" id="search-homebase-text" class="form-control"
                       placeholder="Search Client or Entity..">
                <span class="input-group-btn btn btn-default" id="search-homebase-btn" style="font-size: smaller">
                    <i class="fa fa-search"></i>
                </span>
            </div>
        </div>
    </div>
</div>

<!-- Show the list of my Profession Objectives. -->
<div class="row">
    <div class="col-xs-12">
        <!-- Objective table -->
        <div class="row" id="homebase-row">
            <div class="col-xs-12">
                <ul class="nav nav-tabs top-buffer" role="tablist">
                    <li role="presentation" class="active" id="assets_tab">
                        <a href="#deal" role="tab" data-toggle="tab">Owners Tab</a>
                    </li>
                    <li role="presentation" id="portfolio_tab">
                        <a href="#portfolio" role="tab" data-toggle="tab">Portfolio Tab</a>
                    </li>
                    <!--                    <li role="presentation" id="owner_tab" style="float: right;">-->
                    <!--                        <a href="#owner" role="tab" data-toggle="tab">Owner Tab</a>-->
                    <!--                    </li>-->
                    <li role="presentation" id="buyer_tab" style="float: right;">
                        <a href="#buyer" role="tab" data-toggle="tab" id="buyer-tab">Buyer Tab</a>
                    </li>
                </ul>
            </div>

            <div class="tab-content">

                <!--Deals Tab-->
                <div role="tabpanel" class="tab-pane col-xs-12 active" id="deal">
                    <div class="col-xs-12">
                        <div id="homebase-div" class="top-buffer">
                            <table class="table" id="homebase-table" style="background: #8ff1fd17;">
                                <thead>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <!-- Dummy Table for Assets-->
                            <table class="hidden dummy_table">
                            </table>
                            <!-- // Dummy Table for Assets-->
                        </div>
                    </div>
                </div>
                <!--// Deals Tab-->

                <!--Portfolio Tab-->
                <div role="tabpanel" class="tab-pane col-xs-12" id="portfolio">
                    <div class="col-xs-12">
                        <div id="portfolio-div" class="top-buffer">
                            <table class="table" id="portfolio-table" style="background: #8ff1fd17;">
                                <thead>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--Portfolio Tab-->

                <!--Buyer Tab-->
                <div role="tabpanel" class="tab-pane col-xs-12" id="buyer">
                    <div class="col-xs-12">
                        <div id="buyer-div" class="top-buffer">
                            <table class="table" id="buyer-table" style="background: #8ff1fd17;">
                                <thead>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!--Buyer Tab-->

                <!--Pagination-->
                <div class="col-xs-12">
                    <div class="col-xs-6">
                        <label>Number of clients per page: </label>
                        <select id="page-offet" class="form-control"
                                style="display: inline-block; width: auto !important;">
                            <option>10</option>
                            <option>20</option>
                            <option>30</option>
                            <option>40</option>
                            <option>50</option>
                            <option>60</option>
                            <option>70</option>
                            <option>80</option>
                        </select>
                    </div>

                    <div class="col-xs-6">
                        <nav aria-label="Homebase Pagination." style="float: right; margin: 0px 10px 0 0">
                            <ul class="pagination">
                                <li class="page-item"><a class="page-link" href="#"
                                                         id="prev-page">Previous</a></li>
                                <li class="page-item"><span id="page-count" style="color: black"></span>
                                </li>
                                <li class="page-item"><a class="page-link" href="#" id="next-page">Next</a>
                                </li>
                            </ul>
                        </nav>
                        <button type="button" class="btn btn-primary hidden" id="delete-check"
                                style="float: right; margin: 20px 10px 0 0">
                            <span class="has-tooltip" title="Delete multiple Clients.">Delete</span>
                            <i class="fa fa-trash"></i></button>
                        <button id="remove-multi-delete" style="float: right; margin: 20px 10px 0 0;"
                                class="btn btn-sm btn-default hidden">
                            <span class="has-tooltip" title="Clear delete client check box.">
                                <i class="fa fa-times"></i>
                            </span>
                        </button>
                        <button type="button" class="btn btn-primary hidden move-client" id="submit-check"
                                style="float: right; margin: 20px 10px 0 0">
                            <span class="has-tooltip" title="Submit multiple clients.">Submit</span>
                        </button>
                        <button id="remove-multi-submit" style="float: right; margin: 20px 10px 0 0;"
                                class="btn btn-sm btn-default hidden">
                            <span class="has-tooltip" title="Clear Submit client check box.">
                                <i class="fa fa-times"></i>
                            </span>
                        </button>
                    </div>
                </div>
                <!--// Pagination-->
            </div>
            <!-- // Home Base table -->
        </div>
    </div>

    <?php $this->partial('views/cpdv1/dialogs/client_info.php'); ?>
    <?php $this->partial('views/cpdv1/dialogs/view_sa.php'); ?>
    <?php $this->partial('views/homebase/dialogs/push_deal.php'); ?>
    <?php $this->partial('views/homebase/dialogs/view_portfolio.php'); ?>
    <?php $this->partial('views/homebase/dialogs/add_buyer.php'); ?>
    <?php $this->partial('views/homebase/dialogs/view_asset_info.php'); ?>
    <?php $this->partial('views/cpdv1/dialogs/view_notes.php'); ?>

