<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="/resources/app/js/am/dialog/copy_instance.js"></script>

<div class="modal fade" id="copy-instance-dialog" tabindex="-1" role="dialog" aria-labelledby="copy-instance-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<form id="copy-instance-form" class="form-horizontal">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h2 id="copy-instance-title" class="modal-title">Save as New Instance</h2>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">New Instance</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="save-as-name" name="save_as_name"/>
							<p class="form-text text-muted">Enter the name of the new Activity Metrics instance.</p>
						</div>
					</div>

					<div id="select-existing-description">
						<p>If you would like to over-write an existing instance, select the instance below.</p>
					</div>

					<div id="select-existing-div" class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Existing Instance</label>
						<div class="col-xs-12 col-sm-9">
							<select class="form-control" id="save-as-id" name="save_as_id"/>
							</select>
						</div>
					</div>

				</div>

				<input type="hidden" id="copy-am-ui-id" name="am_ui_id" value=""/>

				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>

			</form>
		</div>
	</div>
</div>
