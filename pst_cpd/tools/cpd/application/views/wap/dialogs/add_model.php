<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="/resources/app/js/wap/dialog/add_model.js"></script>

<div class="modal fade" id="add-model-dialog" tabindex="-1" role="dialog" aria-labelledby="add-model-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<form id="add-model-form" class="form-horizontal">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h2 id="add-model-title" class="modal-title">Add Weekly Activity Model</h2>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="type">Type</label>
						<div class="col-xs-12 col-sm-9">
							<div class="input-group">
								<select class="form-control" id="wap-model-type-id" name="wap_type_id">
								</select>
							</div>
							<p class="form-text text-muted">Select the type of weekly activity type you would like to
								use. For more information on the available weekly activity types, see <a
										href="/wap/types">Weekly Activity Types</a></p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Name</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="wap_um_name" name="name"/>
							<p class="form-text text-muted">Enter the name of the new Activity Metrics model.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Description</label>
						<div class="col-xs-12 col-sm-9">
							<textarea class="form-control" rows="5" width="100%" name="description"
							          id="wap_um_description"></textarea>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>

			</form>
		</div>
	</div>
</div>
