<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="/resources/app/js/wap/dialog/add_instance.js"></script>

<div class="modal fade" id="add-instance-dialog" tabindex="-1" role="dialog" aria-labelledby="add-instance-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<form id="add-instance-form" class="form-horizontal">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h2 id="add-instance-title" class="modal-title">Add Activity Metrics Instance</h2>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="pool_id">Game Type</label>
						<div class="col-xs-12 col-sm-9">
							<div class="input-group">
								<select class="form-control" id="wap-type-id" name="wap_type_id">
								</select>
							</div>
							<p class="form-text text-muted">Select the type of Activity Metrics you would like to use.
								For more information on the available Activity Metrics types, see <a
										href="/wap/game_types">Game Types</a></p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="year">Year</label>
						<div class="col-xs-12 col-sm-9">
							<div class="input-group">
								<select class="form-control" id="wap-ui-quarter" name="quarter">
								</select>
							</div>
							<p class="form-text text-muted">Select the quarter and year this instance is for.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Name</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="wap_ui_name" name="name"/>
							<p class="form-text text-muted">Enter the name of the new Activity Metrics instance.</p>
						</div>
					</div>

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>

			</form>
		</div>
	</div>
</div>
