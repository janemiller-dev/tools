<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="/resources/app/js/wap/wap.js"></script>

<!-- Load the custom css. -->
<link rel="stylesheet" href="/resources/app/css/wap/wap.css"/>

<div class="row">
	<div class="col-xs-12 col-sm-6">
		<form id="heading-form" class="form-horizontal">
			<div class="form-group">
				<label class="col-xs-12 col-sm-4 control-label">
					<h3>Activity Metrics:</h3>
				</label>
				<div class="col-xs-12 col-sm-8"><h3 id="wap-name" class="form-control-static"></h3>&nbsp;<span
							class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover"
							title="Your Instance"
							data-content="The name you give each instance. To change the name click on the current name."></span>
				</div>
			</div>
		</form>
	</div>
	<div class="col-xs-12 col-sm-3">
		<div class="text-right">
			<h4>Create Your Flight Plan</h4>
			<p>If you know destination and are equipped with the skills, equipment and controls then all you need is a
				flight plan. Pilots always know exactly how to get to where they want to go and they also know how to
				make adjustments along the way. Watch this video an learn how this tactical game designer will give you
				the means of creating a flight plan that will get you to your performance objectives with a very high
				degree of certainty.</p>
			<span class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover" data-placement="bottom"
			      title="Instructional Video" data-content="Video tutorial explains how the tool is used."></span>
		</div>
	</div>
	<div class="col-xs-12 col-sm-3">
		<img style="max-width: 100%" src="/resources/app/media/tgd/TGDVideoImage.png"
		     alt="Activity Metrics Instructional Video">
		<!-- When video available.
		<div align="center" class="embed-responsive embed-responsive-16by9">
		  <div class="embed-responsive-item">
		<iframe src="https://player.vimeo.com/video/130995015" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		  </div>
		</div>
		-->
	</div>
</div>

<!-- Show the list of my WAP instances. -->
<div class="row">
	<div class="col-xs-12">

		<!-- WAP instance table -->
		<div class="row">
			<div class="col-xs-12">
				<div id="wap-div" class="top-buffer">
					<table class="table table-striped" id="wap-table">
						<thead class="main-table">
						</thead>
						<tbody class="main-table">
						</tbody>
						<tfoot class="main-table">
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		<!-- // WAP instance table -->

	</div>
</div>

<!-- Dialogs -->
<?php $this->partial('views/wap/dialogs/copy_instance.php'); ?>
