<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="/resources/app/js/wap/dashboard.js"></script>

<!-- Page Title -->
<div class="row">
	<div class="col-xs-12">
		<h3>Weekly Activity Planner Dashboard</h2>
	</div>
</div>

<!-- Show the list of my WAP instances. -->
<div class="row">
	<div class="col-xs-12">

		<!-- Nav tabs -->
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active"><a href="#instances" role="tab" data-toggle="tab">Instances</a></li>
			<li role="presentation"><a href="#models" role="tab" data-toggle="tab">Models</a></li>
		</ul>

		<!-- Tab panes -->
		<div class="tab-content">

			<!-- Instances -->
			<div role="tabpanel" class="tab-pane active" id="instances">

				<div class="row">
					<div class="col-xs-12 text-center top-buffer">
						<button type="button" id="add-instance" class="btn btn-primary bottom-buffer"
						        data-toggle="modal" data-target="#add-instance-dialog">Add Weekly Activity Planner
						</button>
					</div>
				</div>

				<div class="row" id="wap-instance-row">
					<div class="col-xs-12">
						<p id="no-wap-instances">You do not have any Weekly Activity Planner Instances. To create one,
							use the Added Weekly Activity Planner button.</p>
						<div id="wap-instance-div" class="top-buffer">
							<table class="table table-striped" id="wap-instance-table">
								<thead>
								<tr>
									<th>Name</th>
									<th>Type</th>
									<th>Description</th>
									<th>First Week</th>
									<th>Last Week</th>
									<th class="text-center"><span class="glyphicon glyphicon-duplicate"></span></th>
									<th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
								</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- // WAP instance table -->

			<!-- Models  -->
			<div role="tabpanel" class="tab-pane" id="models">

				<div class="row">
					<div class="col-xs-12 text-center top-buffer">
						<button type="button" id="add-instance" class="btn btn-primary bottom-buffer"
						        data-toggle="modal" data-target="#add-model-dialog">Add Weekly Activity Model
						</button>
					</div>
				</div>

				<div class="row" id="wap-model-row">
					<div class="col-xs-12">
						<p id="no-wap-models">You do not have any Weekly Activity Models. To create one, use the Add
							Weekly Activity Model button.</p>
						<div id="wap-model-div" class="top-buffer">
							<table class="table table-striped" id="wap-model-table">
								<thead>
								<tr>
									<th>Name</th>
									<th>Type</th>
									<th>Description</th>
									<th class="text-center"><span class="glyphicon glyphicon-duplicate"></span></th>
									<th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
								</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<!-- // WAP model table -->

		</div>
	</div>

	<!-- Instance Dialogs -->
	<?php $this->partial('views/wap/dialogs/add_instance.php'); ?>
	<?php $this->partial('views/wap/dialogs/delete_instance.php'); ?>
	<?php //$this->partial('views/wap/dialogs/copy_instance.php'); ?>

	<!-- Model Dialogs -->
	<?php $this->partial('views/wap/dialogs/add_model.php'); ?>
	<?php //$this->partial('views/wap/dialogs/delete_model.php'); ?>
	<?php //$this->partial('views/wap/dialogs/copy_model.php'); ?>
