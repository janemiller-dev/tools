<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="/resources/app/js/wap/model.js"></script>

<div class="row">

	<div class="col-xs-12"><h3>Weekly Activity Model</h3></div>

	<div class="col-xs-12 col-md-6 col-md-push-6 col-lg-7 col-lg-push-5">
		<div class="col-xs-12 col-lg-6">
			<div class="text-right">
				<h4>Create Your Flight Plan</h4>
				<p>If you know destination and are equipped with the skills, equipment and controls then all you need is
					a flight plan. Pilots always know exactly how to get to where they want to go and they also know how
					to make adjustments along the way. Watch this video an learn how this tactical game designer will
					give you the means of creating a flight plan that will get you to your performance objectives with a
					very high degree of certainty.</p>
				<span class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover" data-placement="bottom"
				      title="Instructional Video" data-content="Video tutorial explains how the tool is used."></span>
			</div>
		</div>
		<div class="col-xs-12 col-lg-6">
			<img style="max-width: 100%" src="/resources/app/media/tgd/TGDVideoImage.png"
			     alt="Activity Metrics Instructional Video">
		</div>
	</div>

	<div class="col-xs-12 col-md-6 col-md-pull-6 col-lg-5 col-lg-pull-7">
		<form id="heading-form" class="form-horizontal">
			<div class="form-group">
				<label class="col-xs-12 col-sm-5 control-label">
					<h4>Model Name:</h4>
				</label>
				<div class="col-xs-12 col-sm-7"><h4 id="wap-model-name" class="form-control-static"></h4>&nbsp;<span
							class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover" title="Your Model"
							data-content="The name you give each model. To change the name click on the current name."></span>
				</div>
			</div>
		</form>

		<!-- Show the model. -->
		<div class="row">
			<div class="col-xs-12">

				<!-- WAP model table -->
				<div id="wap-model-div" class="top-buffer">
					<table class="table table-striped" id="wap-model-table">
						<thead class="main-table">
						</thead>
						<tbody class="main-table">
						</tbody>
						<tfoot class="main-table">
						</tfoot>
					</table>
				</div>
				<!-- // WAP model table -->

			</div>
		</div>
	</div>


</div>

<!-- Dialogs -->
<?php //$this->partial('views/wap/dialogs/copy_model.php'); ?>
