<!-- Deal Management Dashboard Instance Listing Page -->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Page Title -->
<div class="row">
    <div class="col-xs-12">
        <h2>Deal Management Dashboard</h2>
    </div>
</div>

<!-- Show the add cpd button. -->
<div class="row">
    <div class="col-xs-12 text-left">
        <button type="button" id="add-cpd" class="btn btn-primary bottom-buffer" data-toggle="modal"
                data-target="#add-cpd-dialog" data-backdrop="static">Add New Deal Management Dashboard
        </button>
    </div>
</div>

<!-- Show the list of my DMD instances. -->
<div class="row">
    <div class="col-xs-12">

        <!-- DMD instance table -->
        <div class="row" id="cpd-instance-row">
            <div class="col-xs-12">
                <p id="no-cpd-instances">You do not have a Deal Management Dashboard Instance setup. To create one, use
                    the Add Deal Management Dashboard Instance button.</p>
                <div id="cpd-instance-div" class="top-buffer cpd-instance-row">

                    <div>
                        <table class="table table-striped" id="cpd-instance-table-1" style="border: 1px solid #e1c8c8">
                            <thead>
                            <tr>
                                <th>DMD ID</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Created</th>
                                <th>Modified</th>
                                <th>Active</th>
                                <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                    <div class="cpd-instance-demo-row hidden">
                        <hr>
                        <h4 id="table-demo-header" style="font-weight: bold">
                        </h4>
                        <table class="table table-striped" id="cpd-instance-demo-table"
                               style="border: 1px solid #e1c8c8">
                            <thead>
                            <tr>
                                <th>DMD ID</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Created</th>
                                <th>Modified</th>
                                <th>Active</th>
                                <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- // DMD instance table -->

    </div>
</div>

<!-- Dialogs -->
<?php $this->partial('views/cpdv1/dialogs/add_cpd.php'); ?>
<?php $this->partial('views/cpdv1/dialogs/delete_dialog.php'); ?>


<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/cpdv1/dashboard.js"></script>
