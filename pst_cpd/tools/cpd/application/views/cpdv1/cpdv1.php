<!-- Deal Management Dashboard Deals View -->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
}

$token = base64_encode(hash('sha256', time()));
$_SESSION['token'] = $token;
?>
<!-- Load the custom css. -->
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/cpdv1/cpd.css?v=1"/>
<input type="hidden" name="csrf-token" value="<?php echo $token; ?>">

<!-- Page Title -->
<div class="row">
    <div class="col-xs-12 text-center">
        <h1>Deal Manager Dashboard</h1>
        <label class="control-label" id="cpd-name" style="font-size: 16px"></label>
        <label class="control-label" id="cpd-desc" style="font-size: 16px"></label>
        <div class="row">
            <!-- Previous Quarter Link-->
            <div class="col-xs-12 col-sm-4">
                <span style="font-size: 1.3em; font-style:normal;">
                    <span class="has-tooltip"
                          data-title="Go-To Previous Quarter.">
                    <a href="" id="previous-quarter-cpd">Previous Quarter</a>
                    </span>
                </span>
            </div>
            <!-- // Previous Quarter Link-->

            <div class="col-xs-12 col-sm-4">
                <!-- Active TGD Link -->
                <span style="font-size: 1.3em; font-style:normal;">Active Tactical Game Designer:&nbsp;
                    <span class="has-tooltip"
                          data-title="Navigate to Active TGD instance">
                        <a href="" id="linked-tgd"></a>
                    </span>
                </span>
                <!-- // Active TGD Link -->
                <br/>

                <!--Current Quarter and Year-->
                <span style="font-size: 1.3em; font-style:normal;" class="quarter-info">Quarter:&nbsp;Q
                    <span id="quarter-number"></span>
                    <span id="quarter-year"></span>
                </span>
                <!--// Current Quarter and Year-->
            </div>

            <!-- Next Quarter Link-->
            <div class="col-xs-12 col-sm-4">
                <span style="font-size: 1.3em; font-style:normal;">
                    <span class="has-tooltip"
                          data-title="Go-To Next Quarter.">
                    <a href="" id="next-quarter-cpd">Next Quarter</a>
                    </span>
                </span>
            </div>
            <!-- // Next Quarter Link-->
        </div>
    </div>
</div>
<!-- // Page Title -->

<!-- Header section -->
<div class="row"
     style="border-bottom: 1px solid #e1c8c8; border-top: 1px solid #e1c8c8; padding-bottom: 5px; padding-top: 5px;">

    <!-- Deal Priority Row-->
    <div class="col-lg-3 col-md-3">
        <div class="row">
            <h4 class="col-xs-12 text-center">Deal Priority</h4>
            <h5 class="col-xs-12 text-center">View By CLU & RWT</h5>
            <select class="form-control filter-cpd" data-key="hca_clu" style="margin: 10px 10%; width: 80%">
                <option value="">-- None --</option>
                <option value="C">C</option>
                <option value="L">L</option>
                <option value="U">U</option>
            </select>
            <select class="form-control filter-cpd" data-key="hca_rwt" style="margin: 10px 10%; width: 80%">
                <option value="">-- None --</option>
                <option value="R">R</option>
                <option value="W">W</option>
                <option value="T">T</option>
            </select>
        </div>
    </div>
    <!--// Deal Priority Row-->

    <!-- Objectives Row -->
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="row" style="border-right: 1px solid #e1c8c8; border-left: 1px solid #e1c8c8;">
            <div class="col-xs-12">
                <select class="form-control" id="cpd-objective" style="margin: auto; font-size: 18px">
                    <option value="annual-objective">Annual Objectives</option>
                    <option value="quarter-objective">Quarterly Objectives</option>
                </select>
            </div>
            <!-- Quarter Objectives row.-->
            <div id="quarter-objective" class="hidden">
                <div class="col-xs-4">
                    <h5 class="col-xs-12 text-center">Floor & Actual</h5>
                    <input class="col-xs-12 bottom-buffer form-control cpd_ftg" id="qtr_floor" data-type="quarter"
                           placeholder="Floor" style="border: 1px solid" data-name="floor">
                    <input class="col-xs-12 bottom-buffer form-control" readonly id="qtr_actual" placeholder="Actual"
                           style="border: 1px solid">
                </div>
                <div class="col-xs-4">
                    <h5 class="col-xs-12 text-center">Target & Percentage</h5>
                    <input class="col-xs-12 bottom-buffer form-control cpd_ftg" id="qtr_target" data-type="quarter"
                           placeholder="Target" style="border: 1px solid" data-name="target">
                    <input class="col-xs-12 form-control" id="qtr_percent" readonly placeholder="Percent"
                           style="border: 1px solid">
                </div>
                <div class="col-xs-4">
                    <h5 class="col-xs-12 text-center">Game & Needed</h5>
                    <input class="col-xs-12 bottom-buffer form-control cpd_ftg" id="qtr_game" data-type="quarter"
                           placeholder="Game" style="border: 1px solid" data-name="game">
                    <input class="col-xs-12 form-control" id="qtr_needed" readonly placeholder="Needed"
                           style="border: 1px solid">
                </div>
            </div>
            <!--// Quarter Objectives row.-->

            <!-- Annual Objectives Row -->
            <div id="annual-objective">
                <div class="col-xs-4">
                    <h5 class="col-xs-12 text-center">Floor & Actual</h5>
                    <input class="col-xs-12 bottom-buffer form-control cpd_ftg" id="year_floor" data-type="year"
                           placeholder="Floor" style="border: 1px solid" data-name="floor">
                    <input class="col-xs-12 bottom-buffer form-control" readonly id="year_actual" placeholder="Actual"
                           style="border: 1px solid">
                </div>
                <div class="col-xs-4">
                    <h5 class="col-xs-12 text-center">Target & Percentage</h5>
                    <input class="col-xs-12 bottom-buffer form-control cpd_ftg" id="year_target" data-type="year"
                           placeholder="Target" style="border: 1px solid" data-name="target">
                    <input class="col-xs-12 form-control" id="year_percent" readonly placeholder="Percent"
                           style="border: 1px solid">
                </div>
                <div class="col-xs-4">
                    <h5 class="col-xs-12 text-center">Game & Needed</h5>
                    <input class="col-xs-12 bottom-buffer form-control cpd_ftg" id="year_game" data-type="year"
                           placeholder="Game" style="border: 1px solid" data-name="game">
                    <input class="col-xs-12 form-control" id="year_needed" placeholder="Needed" readonly
                           style="border: 1px solid">
                </div>
            </div>
            <!-- // Annual Objectives Row -->
        </div>
    </div>
    <!--// Objectives Row -->

    <!-- Sort By Product Type row -->
    <div class="col-lg-3 col-md-3">
        <div class="row">
            <h4 class="col-xs-12 text-center"><span class="has-tooltip" data-title="Current product type for deal.">Product Type</span>
            </h4>
            <h5 class="col-xs-12 text-center">Sort & View By Product Type</h5>
            <select class="form-control sort-cpd" data-type="product-type" data-key="hca_product_id"
                    style="margin: 10px 10%; width: 80%">
            </select>
            <select class="form-control filter-cpd" data-type="product-type" data-key="hca_product_id"
                    style="margin: 10px 10%; width: 80%">
            </select>
        </div>
    </div>
    <!-- Sort By Product Type row -->
</div>
<!-- // Header section -->


<!-- Buttons Row. -->
<div class="row top-buffer">

    <!-- Add Client and Colors Button. -->
    <div class="col-xs-6 text-left">
        <button type="button" id="add-deal" class="btn btn-primary bottom-buffer" data-toggle="modal"
                data-target="#add-client-dialog" data-backdrop="static">
                    <span class="has-tooltip" data-title="Add New Clients.">
                        Add Clients
                    </span>
        </button>
        <button type="button" class="btn btn-primary bottom-buffer" data-html="true" data-toggle="popover"
                data-placement="top" id="cpd-colors" data-title="DMD colors" data-content=".">
            <span class="has-tooltip" data-title="Colors Dictionary for DMD.">
                Colors
            </span>
        </button>
        <button type="button" class="btn btn-primary bottom-buffer" data-html="true" data-toggle="modal"
                data-target="#view-deal-identified-dialog" data-title="Deals Identified">
            <span class="has-tooltip" data-title="Deals Identified">
                Deals
            </span>
        </button>
    </div>
    <!--// Add Client and Colors Button. -->

    <!-- Tool specific buttons -->
    <div class="col-xs-6 text-right">
        <button type="button" class="btn btn-primary bottom-buffer" data-html="true" data-toggle="popover"
                data-placement="top" id="cpd-metrics" data-content="">
            <span class="has-tooltip" data-title="Deal Metrics for current DMD tab.">
                Metrics
            </span>
        </button>
        <button type="button" id="user-acm" style="width: 5em" class="btn btn-primary bottom-buffer"
                data-toggle="modal" data-target="#view-alert-dialog">
            <span class="has-tooltip" data-title="Accountability Manager.">
                ACM
            </span>
        </button>
        <button type="button" id="add-deal" class="btn btn-primary bottom-buffer" data-toggle="modal"
                data-target="#view-collaboration-dialog" data-backdrop="static">
            <span class="has-tooltip" data-title="General Collaboration.">
                Collaborate
            </span>
        </button>
    </div>
    <!--// Tool specific buttons -->
</div>
<!-- // Buttons Row. -->


<!-- Show the DMD Instance -->
<div class="row" style="overflow: hidden">
    <div class="col-xs-12">
        <!-- Tab List. -->
        <ul class="nav nav-tabs top-buffer cpd-tabs" role="tablist">
            <li role="presentation" class="active" id="generation_tab" data-type="gds">
                <a href="#creation" role="tab" data-toggle="tab">Deal Generation</a>
            </li>
            <li role="presentation" id="completion_tab" data-type="cds">
                <a href="#completion" role="tab" data-toggle="tab">Deal Management</a>
            </li>
            <li role="presentation" style="float: left;" id="referral_tab" data-type="rds">
                <a href="#referral" role="tab" data-toggle="tab">Deal Referrals</a>
            </li>
            <li role="presentation" style="float: right;" id="buyer_tab" data-type="bds">
                <a href="#buyer" role="tab" data-toggle="tab">
                    <span class="has-tooltip" data-title="Buyer Names List.">Buyer Management</span></a>
            </li>
            <li role="presentation" style="float: right;" id="strategic_tab" data-type="str_ds">
                <a href="#strategic" role="tab" data-toggle="tab">Strategic Program</a>
            </li>
        </ul>
        <!--// Tab List. -->

        <div class="tab-content">
            <!-- Deal Generation tab -->
            <div role="tabpanel" class="tab-pane cpd-pane col-xs-12 active" id="creation">
                <div class="row">
                    <!-- Potential column -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <!-- Potential table -->
                            <table class="table deal-table" align="center" id="cpd-deal-possible-table">
                                <thead>
                                <!-- Table Name and Definition -->
                                <tr class="table-name">
                                    <th colspan="17">
                                        <span style="font-size: 1.3em; font-style:normal;">Potential:&nbsp;
                                            <span class="editable" id="possible-name"></span>
                                        </span>
                                        <span style="font-size: 1.3em; font-style:normal; margin-left: 100px"
                                              class="definition">Definition:&nbsp;
                                            <span class="editable" id="possible-description"></span>
                                        </span>
                                        <span style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dmc"></i>
                                        </span>
                                        <span style="float: right;margin-right: 6vw;color: #C0C0C0;">
                                        </span>
                                    </th>
                                </tr>
                                <!--// Table Name and Definition -->
                                <!-- Table Header -->
                                <tr class="table-heading">
                                    <th class="text-center"><span class="glyphicon glyphicon-sort has-tooltip"
                                                                  data-title="Move Deal."></span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Move Month.">Move By</span>
                                    </th>
                                    <th class="text-center">
                                        <span class="has-tooltip" data-title="Current Deal Status.">Status</span>
                                    </th>
                                    <th class="text-center">
                                        <span class="has-tooltip"
                                              data-title="Current product type for deal.">Product Type</span>
                                    </th>
                                    <th style="text-align:center">
                                        <span class="has-tooltip" data-title="Name of Client.">Owner Name</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Radish, Wheat or Tree.">RWT</span></th>
                                    <th style="text-align:center"><span class="has-tooltip"
                                                                        data-title="Office, cell and mobile phone numbers.">Numbers</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Asset, client and income information.">Info</span>
                                    </th>
                                    <th class="text-center">Dial</th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Asset Name.">Asset Name</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Is deal Certain, Likely or Unlikely to happen.">CLU</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Buyer Names List.">Buyer Name</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Event Notes.">Notes</span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Build Components.">Build</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="List of sent components.">Sent</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Accountability Manager">ACM</span></th>
                                </tr>
                                </thead>
                                <!--// Table Header -->
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <!-- Input fields and buttons-->
                        <strong>
                            <div style="float: right;">

                                <div style="display: inline-block"><span class="total-head">
                           Total Deals&nbsp;</span>
                                    <label style="margin-right:5px"">
                                    <input readonly id="cpd-deal-possible-total-deal"
                                           class="deal-count form-control"
                                           style="border: 1px solid; width: 100px !important">
                                    </label>
                                </div>

                                <div style="display: inline-block"><span class="total-head">
                           Total Value </span><label>
                                        <input readonly id="cpd-deal-possible-total-value"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block;" class="total-gross-head">
                           <span class="total-head">
                           Total Gross </span><label style="margin-right:5px">
                                        <input readonly
                                               id="cpd-deal-possible-total-gross"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block;" class="total-doll-head">
                           <span class="total-head">
                           Total Net </span>
                                    <label style="margin-right:5px">
                                        <input readonly id="cpd-deal-possible-total-doll"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important">
                                    </label>
                                </div>
                                <div style="display: inline-block">
                                    <button type="button" class="btn btn-primary" id="cpd-deal-possible-submit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </strong>
                        <!-- // Input fields and buttons-->
                        <!-- // Potential table -->
                    </div>
                    <!-- // Potential Column -->

                    <!-- Possible column -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <!-- Possible table -->
                            <table class="table deal-table" id="cpd-deal-prospective-table">
                                <thead>

                                <!-- Table Name and Definition -->
                                <tr class="table-name">
                                    <th colspan="17">
                                        <span style="font-size: 1.3em; font-style:normal;">Possible:&nbsp;
                                            <span class="editable" id="prospective-name"></span>
                                        </span>

                                        <span style="font-size: 1.3em; font-style:normal; margin-left: 100px"
                                              class="definition">Definition:&nbsp;
                                            <span class="editable" id="prospective-description"></span>
                                        </span>

                                        <span style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dr"></i>
                                        </span>

                                        <span style="float: right;margin-right: 6vw;color: #C0C0C0;">
                                        </span>
                                    </th>
                                </tr>
                                <!--// Table Name and Definition -->

                                <!-- Table Header.-->
                                <tr class="table-heading">
                                    <th class="text-center"><span class="glyphicon glyphicon-sort has-tooltip"
                                                                  data-title="Move Deal."></span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Move Month.">Move By</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Current Deal Status.">Status</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Current product type for deal.">Product Type</span>
                                    </th>
                                    <th style="text-align:center"><span class="has-tooltip"
                                                                        data-title="Name of Client.">Owner Name</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Radish, Wheat or Tree.">RWT</span></th>
                                    <th style="text-align:center"><span class="has-tooltip"
                                                                        data-title="Office, cell and mobile phone numbers.">Numbers</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Asset, client and income information.">Info</span>
                                    </th>
                                    <th>Dial</th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Asset Name.">Asset Name</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Is deal Certain, Likely or Unlikely to happen.">CLU</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Buyer Names List.">Buyer Name</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Event Notes.">Notes</span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Build Components.">Build</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="List of sent components.">Sent</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Accountability Manager">ACM</span></th>
                                </tr>
                                <!--// Table Header.-->

                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <!-- Input fields and buttons-->
                        <strong>
                            <div style="float: right;">
                                <div style="display: inline-block">
                                    <span class="total-head"> Total Deals&nbsp;</span>
                                    <label style="margin-right:5px"">
                                    <input readonly id="cpd-deal-prospective-total-deal"
                                           class="deal-count form-control"
                                           style="border: 1px solid; width: 100px !important">
                                    </label>
                                </div>

                                <div style="display: inline-block">
                                    <span class="total-head">Total Value </span>
                                    <label>
                                        <input readonly id="cpd-deal-prospective-total-value"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important">
                                    </label>
                                </div>

                                <div style="display: inline-block;" class="total-gross-head">
                                    <span class="total-head">Total Gross </span>
                                    <label style="margin-right:5px">
                                        <input readonly
                                               id="cpd-deal-prospective-total-gross"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important">
                                    </label>
                                </div>

                                <div style="display: inline-block;" class="total-doll-head">
                                    <span class="total-head">Total Net </span>
                                    <label style="margin-right:5px">
                                        <input readonly id="cpd-deal-prospective-total-doll"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important">
                                    </label>
                                </div>
                                <div style="display: inline-block">
                                    <button type="button" class="btn btn-primary" id="cpd-deal-prospective-submit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </strong>
                        <!-- // Input fields and buttons-->
                        <!-- // Possible table -->
                    </div>
                    <!-- // Possible column -->

                    <div class="row">

                        <!-- Probable Column-->
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">

                            <!-- Probable table -->
                            <table class="table deal-table" id="cpd-deal-probable-table"
                                   data-deal-type="probable">
                                <thead>

                                <!-- Table Name and Definition -->
                                <tr class="table-name">
                                    <th colspan="17">
                                 <span style="font-size: 1.3em; font-style:normal;">Probable:&nbsp;
                                 <span class="editable" id="probable-name"></span>
                                 </span>
                                        <span style="font-size: 1.3em; font-style:normal; margin-left: 100px"
                                              class="definition">Definition:&nbsp;
                                 <span class="editable" id="probable-description"></span>
                                 </span>
                                        <span style="float: right;">
                                 <i class="collapse-icon fa fa-plus-circle" id="collapse_des"></i>
                                 </span>
                                        <span style="float: right;margin-right: 6vw;color: #C0C0C0;">

                                </span>
                                    </th>
                                </tr>
                                <!--// Table Name and Definition -->

                                <!-- Table Header -->
                                <tr class="table-heading">
                                    <th class="text-center"><span class="glyphicon glyphicon-sort has-tooltip"
                                                                  data-title="Move Deal."></span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Move Month.">Move By</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Current Deal Status.">Status</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Current product type for deal.">Product Type</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Name of Client.">Owner Name</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Radish, Wheat or Tree.">RWT</span></th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Office, cell and mobile phone numbers.">Numbers</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Asset, client and income information.">Info</span>
                                    </th>
                                    <th>Dial</th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Asset Name.">Asset Name</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Is deal Certain, Likely or Unlikely to happen.">CLU</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Buyer Names List.">Buyer Name</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Event Notes.">Notes</span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Build Components.">Build</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="List of sent components.">Sent</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Accountability Manager">ACM</span></th>
                                </tr>
                                <!--// Table Header -->
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <!-- Input fields and buttons-->
                            <strong>
                                <div style="float: right;">
                                    <div style="display: inline-block">
                                        <span class="total-head">Total Deals&nbsp;</span>
                                        <label style="margin-right:5px"">
                                        <input readonly id="cpd-deal-probable-total-deal"
                                               class="deal-count form-control"
                                               style="border: 1px solid; width: 100px !important">
                                        </label>
                                    </div>

                                    <div style="display: inline-block">
                                        <span class="total-head">Total Value </span>
                                        <label>
                                            <input readonly id="cpd-deal-probable-total-value"
                                                   class="deal-dollar form-control"
                                                   style="border: 1px solid; width: 100px !important">
                                        </label>
                                    </div>

                                    <div style="display: inline-block;" class="total-gross-head">
                                        <span class="total-head">Total Gross </span>
                                        <label style="margin-right:5px">
                                            <input readonly id="cpd-deal-probable-total-gross"
                                                   class="deal-dollar form-control"
                                                   style="border: 1px solid; width: 100px !important">
                                        </label>
                                    </div>

                                    <div style="display: inline-block;" class="total-doll-head">
                                        <span class="total-head">Total Net </span>
                                        <label style="margin-right:5px">
                                            <input readonly
                                                   id="cpd-deal-probable-total-doll"
                                                   class="deal-dollar form-control"
                                                   style="border: 1px solid; width: 100px !important">
                                        </label>
                                    </div>
                                    <div style="display: inline-block">
                                        <button type="button" class="btn btn-primary" id="cpd-deal-probable-submit">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </strong>
                            <!-- // Input fields and buttons-->
                        </div>
                        <!-- //Probable Column -->
                    </div>
                </div>
            </div>
            <!-- // Deal Generation tab -->

            <!-- Deal Completion Tab -->
            <div role="tabpanel" class="tab-pane cpd-pane col-xs-12" id="completion">
                <div class="row">


                    <!-- Prospective column -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        <!-- Prospective table -->
                        <div class="row">
                            <table class="table deal-table" id="cpd-deal-deal_pros-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="17">
                                 <span style="font-size: 1.3em; font-style:normal;">Prospective:&nbsp;
                                 <span class="editable" id="deal_pros-name"></span>
                                 </span>
                                        <span style="font-size: 1.3em; font-style:normal; margin-left: 100px"
                                              class="definition">Definition:&nbsp;
                                 <span class="editable" id="deal_pros-description"></span>
                                 </span>
                                        <span style="float: right;">
                                 <i class="collapse-icon fa fa-plus-circle" id="collapse_rep"></i>
                                 </span>
                                        <span style="float: right;margin-right: 6vw;color: #C0C0C0;">

                                </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th class="text-center"><span class="glyphicon glyphicon-sort has-tooltip"
                                                                  data-title="Move Deal."></span></th>
                                    <th class="text-center"><span class="has-tooltip" title="Close Month"
                                                                  data-placement="bottom">CM</span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Current Deal Status.">Status</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Current product type for deal.">Product Type</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Name of Client.">Owner Name</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Radish, Wheat or Tree.">RWT</span></th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Office, cell and mobile phone numbers.">Numbers</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Asset, client and income information.">Info</span>
                                    </th>
                                    <th>Dial</th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Asset Name.">Asset Name</span>
                                    </th>
                                    <th class="text-center">REF</th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Buyer Names List.">Buyer Name</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Event Notes.">Notes</span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Build Components.">Build</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="List of sent components.">Sent</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Accountability Manager">ACM</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <!-- Input fields and buttons-->
                            <strong>
                                <div style="float: right;">
                                    <div style="display: inline-block"><span class="total-head">
                              Total Deals&nbsp;</span><label style="margin-right:5px"">
                                        <input readonly id="cpd-deal-deal_pros-total-deal"
                                               class="deal-count form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                    </div>

                                    <div style="display: inline-block"><span class="total-head">
                              Total Value </span><label><input readonly id="cpd-deal-deal_pros-total-value"
                                                               class="deal-dollar form-control"
                                                               style="border: 1px solid; width: 100px !important"></label>
                                    </div>


                                    <div style="display: inline-block;" class="total-gross-head">
                           <span class="total-head">
                           Total Gross </span><label style="margin-right:5px">
                                            <input readonly
                                                   id="cpd-deal-deal_pros-total-gross"
                                                   class="deal-dollar form-control"
                                                   style="border: 1px solid; width: 100px !important"></label>
                                    </div>

                                    <div style="display: inline-block;" class="total-doll-head">
                              <span class="total-head">
                              Total Net </span><label style="margin-right:5px">
                                            <input readonly id="cpd-deal-deal_pros-total-doll"
                                                   class="deal-dollar form-control"
                                                   style="border: 1px solid; width: 100px !important"></label>
                                    </div>
                                    <div style="display: inline-block">
                                        <button type="button" class="btn btn-primary" id="cpd-deal-deal_pros-submit">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </strong>
                            <!-- // Input fields and buttons-->
                            <!-- // Prospective table -->
                        </div>
                    </div>
                    <!-- // Prospective column -->

                    <!-- Predictable Column -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <!-- Hard table-->
                            <table class="table deal-table" id="cpd-deal-predictable-table">
                                <thead>
                                <!-- Table name and defination. -->
                                <tr class="table-name">
                                    <th colspan="17">
                                 <span style="font-size: 1.3em; font-style:normal;">Predictable:&nbsp;
                                 <span class="editable" id="predictable-name"></span>
                                 </span>
                                        <span style="font-size: 1.3em; font-style:normal; margin-left: 100px"
                                              class="definition">Definition:&nbsp;
                                 <span class="editable" id="predictable-description"></span>
                                 </span>
                                        <span style="float: right;">
                                 <i class="collapse-icon fa fa-plus-circle" id="collapse_dep"></i>
                                 </span>
                                        <span style="float: right;margin-right: 6vw;color: #C0C0C0;">
                                </span>
                                    </th>
                                </tr>
                                <!--// Table name and defination. -->

                                <!-- Table Header-->
                                <tr class="table-heading">
                                    <th class="text-center"><span class="glyphicon glyphicon-sort has-tooltip"
                                                                  data-title="Move Deal."></span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Move Month.">Move By</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Current Deal Status.">Status</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Current product type for deal.">Product Type</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Name of Client.">Owner Name</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Radish, Wheat or Tree.">RWT</span></th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Office, cell and mobile phone numbers.">Numbers</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Asset, client and income information.">Info</span>
                                    </th>
                                    <th>Dial</th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Asset Name.">Asset Name</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Is deal Certain, Likely or Unlikely to happen.">CLU</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Buyer Names List.">Buyer Name</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Event Notes.">Notes</span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Build Components.">Build</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="List of sent components.">Sent</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Accountability Manager">ACM</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <!-- Input fields and buttons-->
                        <strong>
                            <div style="float: right;">
                                <div style="display: inline-block"><span class="total-head">
                           Total Deals&nbsp;</span><label style="margin-right:5px"">
                                    <input readonly
                                           id="cpd-deal-predictable-total-deal"
                                           class="deal-count form-control"
                                           style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block"><span class="total-head">
                           Total Value </span><label>
                                        <input readonly
                                               id="cpd-deal-predictable-total-value"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block;" class="total-gross-head">
                           <span class="total-head">
                           Total Gross </span><label style="margin-right:5px">
                                        <input readonly
                                               id="cpd-deal-predictable-total-gross"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block;" class="total-doll-head">
                           <span class="total-head">
                           Total Net </span><label style="margin-right:5px">
                                        <input readonly
                                               id="cpd-deal-predictable-total-doll"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>
                                <div style="display: inline-block">
                                    <button type="button" class="btn btn-primary" id="cpd-deal-predictable-submit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </strong>
                        <!-- // Input fields and buttons-->
                        <!-- // Predictable table -->
                    </div>
                    <!-- // Predictable Column -->

                    <!-- Actual column -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                        <!-- Actual table -->
                        <div class="row">
                            <table class="table deal-table" id="cpd-deal-complete-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="17">
                                 <span style="font-size: 1.3em; font-style:normal;">Actual Income:&nbsp;
                                 <span class="editable" id="complete-name"></span>
                                 </span>
                                        <span style="font-size: 1.3em; font-style:normal; margin-left: 100px"
                                              class="definition">Definition:&nbsp;
                                 <span class="editable" id="complete-description"></span>
                                 </span>
                                        <span style="float: right;">
                                 <i class="collapse-icon fa fa-plus-circle" id="collapse_com"></i>
                                 </span>
                                        <span style="float: right;margin-right: 6vw;color: #C0C0C0;">

                                </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th class="text-center"><span class="glyphicon glyphicon-sort has-tooltip"
                                                                  data-title="Move Deal."></span></th>
                                    <th class="text-center"><span class="has-tooltip" title="Close Month"
                                                                  data-placement="bottom">CM</span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Current Deal Status.">Status</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Current product type for deal.">Product Type</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Name of Client.">Owner Name</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Radish, Wheat or Tree.">RWT</span></th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Office, cell and mobile phone numbers.">Numbers</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Asset, client and income information.">Info</span>
                                    </th>
                                    <th>Dial</th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Asset Name.">Asset Name</span>
                                    </th>
                                    <th class="text-center">REF</th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Buyer Names List.">Buyer Name</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Event Notes.">Notes</span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Build Components.">Build</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="List of sent components.">Sent</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Accountability Manager">ACM</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <!-- Input fields and buttons-->
                            <strong>
                                <div style="float: right;">
                                    <div style="display: inline-block"><span class="total-head">
                              Total Deals&nbsp;</span><label style="margin-right:5px"">
                                        <input readonly id="cpd-deal-complete-total-deal"
                                               class="deal-count form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                    </div>

                                    <div style="display: inline-block"><span class="total-head">
                              Total Value </span><label><input readonly id="cpd-deal-complete-total-value"
                                                               class="deal-dollar form-control"
                                                               style="border: 1px solid; width: 100px !important"></label>
                                    </div>

                                    <div style="display: inline-block;" class="total-gross-head">
                           <span class="total-head">
                           Total Gross </span><label style="margin-right:5px">
                                            <input readonly
                                                   id="cpd-deal-complete-total-gross"
                                                   class="deal-dollar form-control"
                                                   style="border: 1px solid; width: 100px !important"></label>
                                    </div>

                                    <div style="display: inline-block;" class="total-doll-head">
                              <span class="total-head">
                              Total Net </span><label style="margin-right:5px">
                                            <input readonly id="cpd-deal-complete-total-doll"
                                                   class="deal-dollar form-control"
                                                   style="border: 1px solid; width: 100px !important"></label>
                                    </div>
                                    <div style="display: inline-block">
                                        <button type="button" class="btn btn-primary" id="cpd-deal-complete-submit">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </strong>
                            <!-- // Input fields and buttons-->
                            <!-- // Actual table -->
                        </div>
                    </div>
                    <!-- // Actual column -->
                </div>
            </div>
            <!-- // Deal Completion tab -->

            <!-- Strategic tab -->
            <div role="tabpanel" class="tab-pane cpd-pane col-xs-12" id="strategic">
                <div class="row">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <!-- // Potential Column -->
                        <div class="row">
                            <!-- Potential table -->
                            <table class="table deal-table" id="cpd-deal-potential-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="17">
                                 <span style="font-size: 1.3em; font-style:normal;">Strategic Owner Name:&nbsp;
                                 <span class="editable" id="potential-name"></span>
                                 </span>
                                        <span style="font-size: 1.3em; font-style:normal; margin-left: 100px"
                                              class="definition">Definition:&nbsp;
                                 <span class="editable" id="potential-description"></span>
                                 </span>
                                        <span style="float: right;">
                                 <i class="collapse-icon fa fa-plus-circle" id="collapse_dms"></i>
                                 </span>
                                        <span style="float: right;margin-right: 6vw;color: #C0C0C0;">

                                </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th class="text-center"><span class="glyphicon glyphicon-sort has-tooltip"
                                                                  data-title="Move Deal."></span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Move Month.">Move By</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Current Deal Status.">Status</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Current product type for deal.">Product Type</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Name of Client.">Owner Name</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Radish, Wheat or Tree.">RWT</span></th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Office, cell and mobile phone numbers.">Numbers</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Asset, client and income information.">Info</span>
                                    </th>
                                    <th>Dial</th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Asset Name.">Asset Name</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Is deal Certain, Likely or Unlikely to happen.">CLU</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Buyer Names List.">Buyer Name</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Event Notes.">Notes</span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Build Components.">Build</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="List of sent components.">Sent</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Accountability Manager">ACM</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <!-- Potential Column -->
                        <!-- Input fields and buttons-->
                        <strong>
                            <div style="float: right;">
                                <div style="display: inline-block"><span class="total-head">
                           Total Deals&nbsp;</span><label style="margin-right:5px"">
                                    <input readonly
                                           id="cpd-deal-potential-total-deal"
                                           class="deal-count form-control"
                                           style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block"><span class="total-head">
                           Total Value </span><label>
                                        <input readonly
                                               id="cpd-deal-potential-total-value"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block;" class="total-gross-head">
                           <span class="total-head">
                           Total Gross </span><label style="margin-right:5px">
                                        <input readonly
                                               id="cpd-deal-potential-total-gross"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block;" class="total-doll-head">
                           <span class="total-head">
                           Total Net </span><label style="margin-right:5px">
                                        <input readonly
                                               id="cpd-deal-potential-total-doll"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>
                                <div style="display: inline-block">
                                    <button type="button" class="btn btn-primary" id="cpd-deal-potential-submit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </strong>
                        <!-- // Input fields and buttons-->
                        <!-- // Potential table -->
                    </div>


                    <!-- Invited column-->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <!-- Invited in person table -->
                            <table class="table deal-table" id="cpd-deal-invitation-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="17">
                                 <span style="font-size: 1.3em; font-style:normal;">Strategic Packages:&nbsp;
                                 <span class="editable" id="invitation-name"></span>
                                 </span>
                                        <span style="font-size: 1.3em; font-style:normal; margin-left: 100px"
                                              class="definition">Definition:&nbsp;
                                 <span class="editable" id="inv_per-description"></span>
                                 </span>
                                        <span style="float: right;">
                                 <i class="collapse-icon fa fa-plus-circle" id="collapse_d_inv_per"></i>
                                 </span>
                                        <span style="float: right;margin-right: 6vw;color: #C0C0C0;">

                                </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th class="text-center"><span class="glyphicon glyphicon-sort has-tooltip"
                                                                  data-title="Move Deal."></span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Move Month.">Move By</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Current Deal Status.">Status</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Current product type for deal.">Product Type</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Name of Client.">Owner Name</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Radish, Wheat or Tree.">RWT</span></th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Office, cell and mobile phone numbers.">Numbers</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Asset, client and income information.">Info</span>
                                    </th>
                                    <th>Dial</th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Asset Name.">Asset Name</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Is deal Certain, Likely or Unlikely to happen.">CLU</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Buyer Names List.">Buyer Name</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Event Notes.">Notes</span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Build Components.">Build</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="List of sent components.">Sent</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Accountability Manager">ACM</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <!-- Input fields and buttons-->
                        <strong>
                            <div style="float: right;">
                                <div style="display: inline-block"><span class="total-head">
                           Total Deals&nbsp;</span><label style="margin-right:5px"">
                                    <input readonly
                                           id="cpd-deal-invitation-total-deal"
                                           class="deal-count form-control"
                                           style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block"><span class="total-head">
                           Total Value </span><label>
                                        <input readonly id="cpd-deal-invitation-total-value"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block;" class="total-gross-head">
                           <span class="total-head">
                           Total Gross </span><label style="margin-right:5px">
                                        <input readonly
                                               id="cpd-deal-invitation-total-gross"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block;" class="total-doll-head">
                           <span class="total-head">
                           Total Net </span><label style="margin-right:5px">
                                        <input readonly
                                               id="cpd-deal-invitation-total-doll"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>
                                <div style="display: inline-block">
                                    <button type="button" class="btn btn-primary" id="cpd-deal-invitation-submit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </strong>
                        <!-- // Input fields and buttons-->
                    </div>
                    <!-- // Invited column -->

                    <!-- Exploratory Meeting column -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <!-- Meetings table -->
                            <table class="table deal-table" id="cpd-deal-delivered-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="17">
                                 <span style="font-size: 1.3em; font-style:normal;">Strategic Updates:&nbsp;
                                 <span class="editable" id="delivered-name"></span>
                                 </span>
                                        <span style="font-size: 1.3em; font-style:normal; margin-left: 100px"
                                              class="definition">Definition:&nbsp;
                                 <span class="editable" id="expl_per-description"></span>
                                 </span>
                                        <span style="float: right;">
                                 <i class="collapse-icon fa fa-plus-circle" id="collapse_dexpl"></i>
                                 </span>
                                        <span style="float: right;margin-right: 6vw;color: #C0C0C0;">

                                </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th class="text-center"><span class="glyphicon glyphicon-sort has-tooltip"
                                                                  data-title="Move Deal."></span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Move Month.">Move By</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Current Deal Status.">Status</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Current product type for deal.">Product Type</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Name of Client.">Owner Name</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Radish, Wheat or Tree.">RWT</span></th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Office, cell and mobile phone numbers.">Numbers</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Asset, client and income information.">Info</span>
                                    </th>
                                    <th>Dial</th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Asset Name.">Asset Name</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Is deal Certain, Likely or Unlikely to happen.">CLU</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Buyer Names List.">Buyer Name</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Event Notes.">Notes</span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Build Components.">Build</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="List of sent components.">Sent</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Accountability Manager">ACM</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <!-- Input fields and buttons-->
                        <strong>
                            <div style="float: right;">

                                <div style="display: inline-block"><span class="total-head">
                           Total Deals&nbsp;</span><label style="margin-right:5px""><input readonly
                                                                                           id="cpd-deal-delivered-total-deal"
                                                                                           class="deal-count form-control"
                                                                                           style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block"><span class="total-head">
                           Total Value </span><label><input readonly id="cpd-deal-delivered-total-value"
                                                            class="deal-dollar form-control"
                                                            style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block;" class="total-gross-head">
                           <span class="total-head">
                           Total Gross </span><label style="margin-right:5px">
                                        <input readonly
                                               id="cpd-deal-expl_per-total-gross"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block;" class="total-doll-head">
                           <span class="total-head">
                           Total Net </span><label style="margin-right:5px"><input readonly
                                                                                   id="cpd-deal-delivered-total-doll"
                                                                                   class="deal-dollar form-control"
                                                                                   style="border: 1px solid; width: 100px !important"></label>
                                </div>
                                <div style="display: inline-block">
                                    <button type="button" class="btn btn-primary" id="cpd-deal-delivered-submit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </strong>
                        <!-- // Input fields and buttons-->
                    </div>
                    <!-- // Exploratory Meeting column -->

                </div>
                <!-- // Invited column -->
            </div>
            <!-- // Strategic tab -->


            <!-- Buyer Name Tab -->
            <div role="tabpanel" class="tab-pane cpd-pane col-xs-12" id="buyer">
                <div class="row">

                    <!-- Buyer Name Potential Table-->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <!-- Potential table -->
                            <table class="table deal-table" id="cpd-deal-b_active-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="20">
                                 <span style="font-size: 1.3em; font-style:normal;">Potential:
                                 <span class="editable" id="b_active-name"></span>
                                 </span>
                                        <span style="font-size: 1.3em; font-style:normal; margin-left: 100px"
                                              class="definition">Definition:&nbsp;
                                 <span class="editable" id="b_active-description"></span>
                                 </span>
                                        <span style="float: right;">
                                 <i class="collapse-icon fa fa-plus-circle" id="collapse_d_subm"></i>
                                 </span>
                                        <span style="float: right;margin-right: 6vw;color: #C0C0C0;">

                                </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th class="text-center"><span class="glyphicon glyphicon-sort has-tooltip"
                                                                  data-title="Move Deal."></span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Current Deal Status.">Status</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Name of Client.">Buyer Name</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Entity Name.">Entity Name</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Entity Address.">Entity Address</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="City.">City</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="State.">State</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="ZIP.">ZIP</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Radish, Wheat or Tree.">RWT</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Current product type for deal.">Product Type</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Asset, client and income information.">Info</span>
                                    </th>
                                    <th>Dial</th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Asset Name.">Asset Preferences</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Is deal Certain, Likely or Unlikely to happen.">CLU</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Event Notes.">Notes</span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Build Components.">Build</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="List of sent components.">Sent</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Accountability Manager">ACM</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <!-- Input fields and buttons-->
                            <strong>
                                <div style="float: right;">
                                    <div style="display: inline-block">
                           <span class="total-head">
                           Total Deals&nbsp;</span> <label style="margin-right:5px"">
                                        <input readonly
                                               id="cpd-deal-b_active-total-deal"
                                               class="deal-count form-control"
                                               style="border: 1px solid; width: 100px !important">
                                        </label>
                                    </div>

                                    <div style="display: inline-block">
                           <span class="total-head">
                           Total Value </span><label>
                                            <input readonly id="cpd-deal-b_active-total-value"
                                                   class="deal-dollar form-control"
                                                   style="border: 1px solid; width: 100px !important"></label>
                                    </div>

                                    <div style="display: inline-block;" class="total-gross-head">
                           <span class="total-head">
                           Total Gross </span><label style="margin-right:5px">
                                            <input readonly
                                                   id="cpd-deal-b_active-total-gross"
                                                   class="deal-dollar form-control"
                                                   style="border: 1px solid; width: 100px !important"></label>
                                    </div>

                                    <div style="display: inline-block;" class="total-doll-head">
                           <span class="total-head">
                           Total Net </span>
                                        <label style="margin-right:5px">
                                            <input readonly
                                                   id="cpd-deal-b_active-total-doll"
                                                   class="deal-dollar form-control"
                                                   style="border: 1px solid; width: 100px !important">
                                        </label>
                                    </div>
                                    <div style="display: inline-block">
                                        <button type="button" class="btn btn-primary" id="cpd-deal-b_active-submit">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </strong>
                            <!--// Input fields and buttons-->
                        </div>
                        <!-- // Buyer Name Potential Table -->
                    </div>
                    <!--// Buyer Name Potential Table-->


                    <!-- Buyer Name Possible Table-->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <!-- Tour table -->
                            <table class="table deal-table" id="cpd-deal-tour-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="20">
                                 <span style="font-size: 1.3em; font-style:normal;">Possible:
                                 <span class="editable" id="tour-name"></span>
                                 </span>
                                        <span style="font-size: 1.3em; font-style:normal; margin-left: 100px"
                                              class="definition">Definition:&nbsp;
                                 <span class="editable" id="tour-description"></span>
                                 </span>
                                        <span style="float: right;">
                                 <i class="collapse-icon fa fa-plus-circle" id="collapse_d_poss"></i>
                                 </span>
                                        <span style="float: right;margin-right: 6vw;color: #C0C0C0;">

                                </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th class="text-center"><span class="glyphicon glyphicon-sort has-tooltip"
                                                                  data-title="Move Deal."></span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Current Deal Status.">Status</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Name of Client.">Buyer Name</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Entity Name.">Entity Name</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Entity Address.">Entity Address</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="City.">City</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="State.">State</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="ZIP.">ZIP</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Radish, Wheat or Tree.">RWT</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Current product type for deal.">Product Type</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Asset, client and income information.">Info</span>
                                    </th>
                                    <th>Dial</th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Asset Name.">Asset Preferences</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Is deal Certain, Likely or Unlikely to happen.">CLU</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Event Notes.">Notes</span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Build Components.">Build</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="List of sent components.">Sent</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Accountability Manager">ACM</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                            <!-- Input fields and buttons-->
                            <strong>
                                <div style="float: right;">
                                    <div style="display: inline-block">
                           <span class="total-head">
                           Total Deals&nbsp;</span> <label style="margin-right:5px"">
                                        <input readonly
                                               id="cpd-deal-tour-total-deal"
                                               class="deal-count form-control"
                                               style="border: 1px solid; width: 100px !important">
                                        </label>
                                    </div>

                                    <div style="display: inline-block">
                           <span class="total-head">
                           Total Value </span><label>
                                            <input readonly id="cpd-deal-tour-total-value"
                                                   class="deal-dollar form-control"
                                                   style="border: 1px solid; width: 100px !important"></label>
                                    </div>

                                    <div style="display: inline-block;" class="total-gross-head">
                           <span class="total-head">
                           Total Gross </span><label style="margin-right:5px">
                                            <input readonly
                                                   id="cpd-deal-tour-total-gross"
                                                   class="deal-dollar form-control"
                                                   style="border: 1px solid; width: 100px !important"></label>
                                    </div>

                                    <div style="display: inline-block;" class="total-doll-head">
                           <span class="total-head">
                           Total Net </span>
                                        <label style="margin-right:5px">
                                            <input readonly
                                                   id="cpd-deal-tour-total-doll"
                                                   class="deal-dollar form-control"
                                                   style="border: 1px solid; width: 100px !important">
                                        </label>
                                    </div>
                                    <div style="display: inline-block">
                                        <button type="button" class="btn btn-primary" id="cpd-deal-tour-submit">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </strong>
                            <!--// Input fields and buttons-->
                        </div>
                        <!-- // Buyer Name Potential Table -->
                    </div>
                    <!--// Buyer Name Possible Table-->

                    <!-- Buyer Name Prospective: Table-->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <!-- Offers table -->
                            <table class="table deal-table" id="cpd-deal-offer-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="20">
                                 <span style="font-size: 1.3em; font-style:normal;">Prospective::&nbsp;
                                 <span class="editable" id="offer-name"></span>
                                 </span>
                                        <span style="font-size: 1.3em; font-style:normal; margin-left: 100px"
                                              class="definition">Definition:&nbsp;
                                 <span class="editable" id="offer-description"></span>
                                 </span>
                                        <span style="float: right;">
                                 <i class="collapse-icon fa fa-plus-circle" id="collapse_d_propo"></i>
                                 </span>
                                        <span style="float: right;margin-right: 6vw;color: #C0C0C0;">

                                </span>

                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th class="text-center"><span class="glyphicon glyphicon-sort has-tooltip"
                                                                  data-title="Move Deal."></span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Current Deal Status.">Status</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Name of Client.">Buyer Name</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Entity Name.">Entity Name</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Entity Address.">Entity Address</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="City.">City</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="State.">State</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="ZIP.">ZIP</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Radish, Wheat or Tree.">RWT</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Current product type for deal.">Product Type</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Asset, client and income information.">Info</span>
                                    </th>
                                    <th>Dial</th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Asset Name.">Asset Preferences</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Is deal Certain, Likely or Unlikely to happen.">CLU</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Event Notes.">Notes</span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Build Components.">Build</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="List of sent components.">Sent</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Accountability Manager">ACM</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <!-- Input fields and buttons-->
                        <strong>
                            <div style="float: right;">
                                <div style="display: inline-block">
                           <span class="total-head">
                           Total Deals&nbsp;</span> <label style="margin-right:5px"">
                                    <input readonly
                                           id="cpd-deal-offer-total-deal"
                                           class="deal-count form-control"
                                           style="border: 1px solid; width: 100px !important">
                                    </label>
                                </div>

                                <div style="display: inline-block">
                           <span class="total-head">
                           Total Value </span><label>
                                        <input readonly id="cpd-deal-offer-total-value"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block;" class="total-gross-head">
                           <span class="total-head">
                           Total Gross </span><label style="margin-right:5px">
                                        <input readonly
                                               id="cpd-deal-offer-total-gross"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block;" class="total-doll-head">
                           <span class="total-head">
                           Total Net </span>
                                    <label style="margin-right:5px">
                                        <input readonly
                                               id="cpd-deal-offer-total-doll"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important">
                                    </label>
                                </div>
                                <div style="display: inline-block">
                                    <button type="button" class="btn btn-primary" id="cpd-deal-offer-submit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </strong>
                        <!-- // Input fields and buttons-->
                    </div>
                    <!-- // Buyer Name Prospective: Table -->

                    <!-- Buyer Names Probable Table -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <!-- Contract table -->
                            <table class="table deal-table" id="cpd-deal-diligence-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="20">
                                 <span style="font-size: 1.3em; font-style:normal;">Probable:&nbsp;
                                 <span class="editable" id="diligence-name"></span>
                                 </span>
                                        <span style="font-size: 1.3em; font-style:normal; margin-left: 100px"
                                              class="definition">Definition:&nbsp;
                                 <span class="editable" id="diligence-description"></span>
                                 </span>
                                        <span style="float: right;">
                                 <i class="collapse-icon fa fa-plus-circle" id="collapse_b_prob"></i>
                                 </span>
                                        <span style="float: right;margin-right: 6vw;color: #C0C0C0;">

                                </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th class="text-center"><span class="glyphicon glyphicon-sort has-tooltip"
                                                                  data-title="Move Deal."></span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Current Deal Status.">Status</span>
                                    </th>

                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Name of Client.">Buyer Name</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Entity Name.">Entity Name</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Entity Address.">Entity Address</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="City.">City</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="State.">State</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="ZIP.">ZIP</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Radish, Wheat or Tree.">RWT</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Current product type for deal.">Product Type</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Asset, client and income information.">Info</span>
                                    </th>
                                    <th>Dial</th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Asset Name.">Asset Preferences</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Is deal Certain, Likely or Unlikely to happen.">CLU</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Event Notes.">Notes</span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Build Components.">Build</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="List of sent components.">Sent</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Accountability Manager">ACM</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <!-- Input fields and buttons-->
                        <strong>
                            <div style="float: right;">
                                <div style="display: inline-block">
                           <span class="total-head">
                           Total Deals&nbsp;</span> <label style="margin-right:5px"">
                                    <input readonly
                                           id="cpd-deal-diligence-total-deal"
                                           class="deal-count form-control"
                                           style="border: 1px solid; width: 100px !important">
                                    </label>
                                </div>

                                <div style="display: inline-block">
                           <span class="total-head">
                           Total Value </span><label>
                                        <input readonly id="cpd-deal-diligence-total-value"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block;" class="total-gross-head">
                           <span class="total-head">
                           Total Gross </span><label style="margin-right:5px">
                                        <input readonly
                                               id="cpd-deal-diligence-total-gross"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block;" class="total-doll-head">
                           <span class="total-head">
                           Total Net </span>
                                    <label style="margin-right:5px">
                                        <input readonly
                                               id="cpd-deal-diligence-total-doll"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important">
                                    </label>
                                </div>
                                <div style="display: inline-block">
                                    <button type="button" class="btn btn-primary" id="cpd-deal-diligence-submit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </strong>
                        <!-- // Input fields and buttons-->
                    </div>
                    <!-- // Buyer Names Probable Table -->


                    <!-- Buyer Names Predictable Table -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <!-- Predictable table -->
                            <table class="table deal-table" id="cpd-deal-b_contract-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="20">
                                 <span style="font-size: 1.3em; font-style:normal;">Predictable:
                                 <span class="editable" id="b_contract-name"></span>
                                 </span>
                                        <span style="font-size: 1.3em; font-style:normal; margin-left: 100px"
                                              class="definition">Definition:&nbsp;
                                 <span class="editable" id="b_contract-description"></span>
                                 </span>
                                        <span style="float: right;">
                                 <i class="collapse-icon fa fa-plus-circle" id="collapse_b_contr"></i>
                                 </span>
                                        <span style="float: right;margin-right: 6vw;color: #C0C0C0;">

                                </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th class="text-center"><span class="glyphicon glyphicon-sort has-tooltip"
                                                                  data-title="Move Deal."></span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Current Deal Status.">Status</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Name of Client.">Buyer Name</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Entity Name.">Entity Name</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Entity Address.">Entity Address</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="City.">City</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="State.">State</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="ZIP.">ZIP</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Radish, Wheat or Tree.">RWT</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Current product type for deal.">Product Type</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Asset, client and income information.">Info</span>
                                    </th>
                                    <th>Dial</th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Asset Name.">Asset Preferences</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Is deal Certain, Likely or Unlikely to happen.">CLU</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Event Notes.">Notes</span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Build Components.">Build</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="List of sent components.">Sent</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Accountability Manager">ACM</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <!-- Input fields and buttons-->
                        <strong>
                            <div style="float: right;">
                                <div style="display: inline-block">
                           <span class="total-head">
                           Total Deals&nbsp;</span> <label style="margin-right:5px"">
                                    <input readonly
                                           id="cpd-deal-b_contract-total-deal"
                                           class="deal-count form-control"
                                           style="border: 1px solid; width: 100px !important">
                                    </label>
                                </div>

                                <div style="display: inline-block">
                           <span class="total-head">
                           Total Value </span><label>
                                        <input readonly id="cpd-deal-b_contract-total-value"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block;" class="total-gross-head">
                           <span class="total-head">
                           Total Gross </span><label style="margin-right:5px">
                                        <input readonly
                                               id="cpd-deal-b_contract-total-gross"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block;" class="total-doll-head">
                           <span class="total-head">
                           Total Net </span>
                                    <label style="margin-right:5px">
                                        <input readonly
                                               id="cpd-deal-b_contract-total-doll"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important">
                                    </label>
                                </div>
                                <div style="display: inline-block">
                                    <button type="button" class="btn btn-primary" id="cpd-deal-b_contract-submit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </strong>
                        <!-- // Input fields and buttons-->
                    </div>
                    <!-- // Buyer Names Predictable Table -->

                </div>
                <!-- // Buyer Name Tab -->
            </div>

            <!-- Referral tab -->
            <div role="tabpanel" class="tab-pane cpd-pane col-xs-12" id="referral">
                <div class="row">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <!-- Received and Offered Column -->
                        <div class="row">
                            <!-- Potential table -->
                            <table class="table deal-table" id="cpd-deal-ref_rece-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="17">
                                 <span style="font-size: 1.3em; font-style:normal;">Referrals:&nbsp;
                                 <span class="editable" id="ref_rece-name"></span>
                                 </span>
                                        <span style="font-size: 1.3em; font-style:normal; margin-left: 100px"
                                              class="definition">Definition:&nbsp;
                                 <span class="editable" id="ref_rece-description"></span>
                                 </span>
                                        <span style="float: right;">
                                 <i class="collapse-icon fa fa-plus-circle" id="collapse_ref_rece"></i>
                                 </span>
                                        <span style="float: right;margin-right: 6vw;color: #C0C0C0;">

                                </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th class="text-center"><span class="glyphicon glyphicon-sort has-tooltip"
                                                                  data-title="Move Deal."></span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Move Month.">Move By</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Current Deal Status.">Status</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Current product type for deal.">Product Type</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Name of Client.">Owner Name</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Radish, Wheat or Tree.">RWT</span></th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Office, cell and mobile phone numbers.">Numbers</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Asset, client and income information.">Info</span>
                                    </th>
                                    <th>Dial</th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Asset Name.">Asset Name</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Is deal Certain, Likely or Unlikely to happen.">CLU</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Buyer Names List.">Buyer Name</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Event Notes.">Notes</span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Build Components.">Build</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="List of sent components.">Sent</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Accountability Manager">ACM</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <!-- Potential Column -->
                        <!-- Input fields and buttons-->
                        <strong>
                            <div style="float: right;">
                                <div style="display: inline-block"><span class="total-head">
                           Total Deals&nbsp;</span><label style="margin-right:5px"">
                                    <input readonly
                                           id="cpd-deal-ref_rece-total-deal"
                                           class="deal-count form-control"
                                           style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block"><span class="total-head">
                           Total Value </span><label>
                                        <input readonly
                                               id="cpd-deal-ref_rece-total-value"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block;" class="total-gross-head">
                           <span class="total-head">
                           Total Gross </span><label style="margin-right:5px">
                                        <input readonly
                                               id="cpd-deal-ref_rece-total-gross"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block;" class="total-doll-head">
                           <span class="total-head">
                           Total Net </span><label style="margin-right:5px">
                                        <input readonly
                                               id="cpd-deal-ref_rece-total-doll"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>
                                <div style="display: inline-block">
                                    <button type="button" class="btn btn-primary" id="cpd-deal-ref_rece-submit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </strong>
                        <!-- // Input fields and buttons-->
                        <!-- // Potential table -->
                    </div>


                    <!-- Invited column-->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <!-- Invited in person table -->
                            <table class="table deal-table" id="cpd-deal-ref_acce-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="17">
                                 <span style="font-size: 1.3em; font-style:normal;">Referrals:&nbsp;
                                 <span class="editable" id="ref_acce-name"></span>
                                 </span>
                                        <span style="font-size: 1.3em; font-style:normal; margin-left: 100px"
                                              class="definition">Definition:&nbsp;
                                 <span class="editable" id="ref_acce-description"></span>
                                 </span>
                                        <span style="float: right;">
                                 <i class="collapse-icon fa fa-plus-circle" id="collapse_d_ref_acce"></i>
                                 </span>
                                        <span style="float: right;margin-right: 6vw;color: #C0C0C0;">

                                </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th class="text-center"><span class="glyphicon glyphicon-sort has-tooltip"
                                                                  data-title="Move Deal."></span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Move Month.">Move By</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Current Deal Status.">Status</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Current product type for deal.">Product Type</span>
                                    </th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Name of Client.">Owner Name</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Radish, Wheat or Tree.">RWT</span></th>
                                    <th style=" text-align:center"><span class="has-tooltip"
                                                                         data-title="Office, cell and mobile phone numbers.">Numbers</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Asset, client and income information.">Info</span>
                                    </th>
                                    <th>Dial</th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Asset Name.">Asset Name</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Is deal Certain, Likely or Unlikely to happen.">CLU</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Buyer Names List.">Buyer Name</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Event Notes.">Notes</span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Build Components.">Build</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="List of sent components.">Sent</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Accountability Manager">ACM</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <!-- Input fields and buttons-->
                        <strong>
                            <div style="float: right;">
                                <div style="display: inline-block"><span class="total-head">
                           Total Deals&nbsp;</span><label style="margin-right:5px"">
                                    <input readonly
                                           id="cpd-deal-ref_acce-total-deal"
                                           class="deal-count form-control"
                                           style="border: 1px solid; width: 100px !important"></label>
                                </div>
                                <div style="display: inline-block"><span class="total-head">
                           Total Value </span><label>
                                        <input readonly id="cpd-deal-ref_acce-total-value"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>


                                <div style="display: inline-block;" class="total-gross-head">
                           <span class="total-head">
                           Total Gross </span><label style="margin-right:5px">
                                        <input readonly
                                               id="cpd-deal-ref_acce-total-gross"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block;" class="total-doll-head">
                           <span class="total-head">
                           Total Net </span><label style="margin-right:5px">
                                        <input readonly
                                               id="cpd-deal-ref_acce-total-doll"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>
                                <div style="display: inline-block">
                                    <button type="button" class="btn btn-primary" id="cpd-deal-ref_acce-submit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </strong>
                        <!-- // Input fields and buttons-->
                    </div>
                    <!-- // Invited column -->

                    <!-- Exploratory Meeting column -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="row">
                            <!-- TEM table -->
                            <table class="table deal-table" id="cpd-deal-ref_compl-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="17">
                                 <span style="font-size: 1.3em; font-style:normal;">Referrals:&nbsp;
                                 <span class="editable" id="ref_compl-name"></span>
                                 </span>
                                        <span style="font-size: 1.3em; font-style:normal; margin-left: 100px"
                                              class="definition">Definition:&nbsp;
                                 <span class="editable" id="ref_compl-description"></span>
                                 </span>
                                        <span style="float: right;">
                                 <i class="collapse-icon fa fa-plus-circle" id="collapse_ref_compl"></i>
                                 </span>
                                        <span style="float: right;margin-right: 6vw;color: #C0C0C0;">

                                </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th class="text-center">
                                        <span class="glyphicon glyphicon-sort has-tooltip"
                                              data-title="Move Deal."></span></th>
                                    <th class="text-center">
                                        <span class="has-tooltip" data-title="Move Month.">Move By</span>
                                    </th>
                                    <th class="text-center">
                                        <span class="has-tooltip" data-title="Current Deal Status.">Status</span>
                                    </th>
                                    <th class="text-center">
                                        <span class="has-tooltip"
                                              data-title="Current product type for deal.">Product Type</span>
                                    </th>
                                    <th style="text-align:center">
                                        <span class="has-tooltip"
                                              data-title="Name of Client.">Owner Name</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Radish, Wheat or Tree.">RWT</span></th>
                                    <th style="text-align:center">
                                        <span class="has-tooltip"
                                              data-title="Office, cell and mobile phone numbers.">Numbers</span>
                                    </th>
                                    <th class="text-center">
                                        <span class="has-tooltip"
                                              data-title="Asset, client and income information.">Info</span>
                                    </th>
                                    <th>Dial</th>
                                    <th class="text-center">
                                        <span class="has-tooltip" data-title="Asset Name.">Asset Name</span>
                                    </th>
                                    <th class="text-center">
                                        <span class="has-tooltip"
                                              data-title="Is deal Certain, Likely or Unlikely to happen.">CLU</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Buyer Names List.">Buyer Name</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Event Notes.">Notes</span></th>
                                    <th class="text-center"><span class="has-tooltip" data-title="Build Components.">Build</span>
                                    </th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="List of sent components.">Sent</span></th>
                                    <th class="text-center"><span class="has-tooltip"
                                                                  data-title="Accountability Manager">ACM</span></th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <!-- Input fields and buttons-->
                        <strong>
                            <div style="float: right;">
                                <div style="display: inline-block"><span class="total-head">
                           Total Deals&nbsp;</span><label style="margin-right:5px"">
                                    <input readonly
                                           id="cpd-deal-ref_compl-total-deal"
                                           class="deal-count form-control"
                                           style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block"><span class="total-head">
                           Total Value </span><label><input readonly id="cpd-deal-ref_compl-total-value"
                                                            class="deal-dollar form-control"
                                                            style="border: 1px solid; width: 100px !important"></label>
                                </div>


                                <div style="display: inline-block;" class="total-gross-head">
                           <span class="total-head">
                           Total Gross </span><label style="margin-right:5px">
                                        <input readonly
                                               id="cpd-deal-ref_compl-total-gross"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important"></label>
                                </div>

                                <div style="display: inline-block;" class="total-doll-head">
                           <span class="total-head">
                           Total Net </span><label style="margin-right:5px">
                                        <input readonly
                                               id="cpd-deal-ref_compl-total-doll"
                                               class="deal-dollar form-control"
                                               style="border: 1px solid; width: 100px !important;"></label>
                                </div>
                                <div style="display: inline-block">
                                    <button type="button" class="btn btn-primary" id="cpd-deal-ref_compl-submit">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </strong>
                        <!-- // Input fields and buttons-->
                    </div>
                    <!-- // Exploratory Meeting column -->

                </div>
                <!-- // Invited column -->
            </div>
            <!-- // Referral tab -->
        </div>
    </div>

    <!-- Dummy DMD row. -->
    <table class="">
        <tr class="maintainer sample-tr" style="visibility: hidden">
            <td class="text-center">
                <button class="btn btn-default btn-sm has-popover sample_button" data-toggle="popover" title="Dummy Row"
                        data-html="true" data-placement="top"
                        data-content="This is Dummy button.
                        Please Add new clients using Add Clients Button to access full features of the application..">
                    <span class="glyphicon glyphicon-sort has-tooltip" data-title="Move Deal."></span>
                </button>
            </td>
            <td class="text-center">
                <input type="text" class="form-control" value="Month" style="width: 4vw !important">
            </td>
            <td class="text-center">
                <input type="text" class="form-control" value="Example">
            </td>
            <td class="text-center">
                <input type="text" class="form-control" value="Product Type">
            </td>
            <td class="text-center">
                <input type="text" class="form-control" value="Not a Owner">
            </td>
            <td class="text-center">
                <select class="form-control">
                    <option>R</option>
                    <option>W</option>
                    <option>T</option>
                </select>
            </td>
            <td class="text-center">
                <select class="form-control">
                    <option>xxx-xxx-xxxx</option>
                </select></td>
            <td class="text-center">
                <button class="btn btn-default btn-sm has-popover" data-toggle="popover" data-html="true"
                        data-id="0" title="Dummy Button." data-container="body" data-placement="top"
                        data-content="This is Dummy button.
                        Please Add new clients using Add Clients Button to access full features..">
                    <span class="glyphicon glyphicon-info-sign"></span>
                </button>
            </td>
            <td class="text-center">
                <button class="btn btn-default btn-sm has-popover" data-toggle="popover" data-id="0" data-html="true"
                        data-content="This is Dummy button.Please Add new clients using
                            Add Clients Button to access full features.."
                        data-placement="top" data-container="body"
                        title="Dummy Button"><span class="glyphicon glyphicon-phone-alt"></span></button>
            </td>
            <td class="text-center">
                <textarea cols="15" class="form-control" rows="1">Asset Name</textarea>
            </td>
            <td class="text-center">
                <select class="form-control">
                    <option>C</option>
                    <option>L</option>
                    <option>U</option>
                </select>
            </td>
            <td class="text-center">
                <div class="input-group">
                    <select class="form-control input-group-addon"
                            style="min-width: 5vw; background-color: #fff !important;">
                        <option>--Buyer--</option>
                    </select>
                    <span class="input-group-btn">
                        <button class="btn btn-default btn-sm dummy_notes has-popover" data-toggle="popover" data-id="0"
                                data-content="This is Dummy button.Please Add new clients using
                            Add Clients Button to access full features.." data-html="true"
                                data-placement="top" data-container="body"
                                title="Dummy Button">
                            <span class="glyphicon glyphicon-list"></span>
                        </button>
                    </span>
                </div>
            </td>
            <td class="text-center">
                <button class="btn btn-default btn-sm dummy_notes has-popover" data-toggle="popover" data-id="0"
                        data-content="This is Dummy button.Please Add new clients using
                        Add Clients Button to access full features.." data-html="true"
                        data-placement="top" data-container="body"
                        title="Dummy Button">
                    <span class="glyphicon glyphicon-list"></span>
                </button>
            </td>
            <td class="text-center">
                <button class="btn btn-default btn-sm has-popover dummy_promo" data-toggle="popover"
                        title="Build Components." data-container="body" data-html="true" data-placement="top">
                    <span class="glyphicon glyphicon-send"></span>
                </button>
            </td>
            <td class="text-center">
                <button class="btn btn-default btn-sm has-popover dummy_sent" data-toggle="popover"
                        title="Dummy Sent Components" data-container="body" data-html="true" data-placement="top">
                    <span class="glyphicon glyphicon-envelope"></span>
                </button>
            </td>
            <td class="text-center">
                <button class="btn btn-default btn-sm dummy_acm has-popover" data-toggle="popover" data-id="0"
                        data-content="This is Dummy button.Please Add new clients using
                            Add Clients Button to access full features.." data-html="true"
                        data-placement="top" data-container="body"
                        title="Dummy Button">
                    <span class="glyphicon glyphicon-book"></span>
                </button>
            </td>
        </tr>
    </table>
    <!-- // Dummy DMD row. -->


    <!-- Dummy Buyer Row-->
    <table class="">
        <tr class="maintainer sample-buyer-tr" style="visibility: hidden">
            <td class="text-center">
                <button class="btn btn-default btn-sm has-popover sample_button" data-toggle="popover" title="Dummy Row"
                        data-html="true" data-placement="top"
                        data-content="This is Dummy button.
                        Please Add new clients using Add Clients Button to access full features of the application..">
                    <span class="glyphicon glyphicon-sort has-tooltip" data-title="Move Deal."></span>
                </button>
            </td>
            <td class="text-center">
                <input type="text" class="form-control" value="Example">
            </td>
            <td class="text-center">
                <input type="text" class="form-control" value="Not a buyer">
            </td>
            <td class="text-center">
                <input type="text" class="form-control" value="Not an entity">
            </td>
            <td class="text-center">
                <input type="text" class="form-control" value="Not an entity address">
            </td>
            <td class="text-center">
                <input type="text" class="form-control" value="Not a city">
            </td>
            <td class="text-center">
                <input type="text" class="form-control" value="Not a state">
            </td>
            <td class="text-center">
                <input type="text" class="form-control" value="Not a zip">
            </td>
            <td class="text-center">
                <select class="form-control">
                    <option>R</option>
                    <option>W</option>
                    <option>T</option>
                </select>
            </td>
            <td class="text-center">
                <input type="text" class="form-control" value="Product type">
            </td>
            <td class="text-center">
                <button class="btn btn-default btn-sm has-popover" data-toggle="popover" data-html="true"
                        data-id="0" title="Dummy Button." data-container="body" data-placement="top"
                        data-content="This is Dummy button.
                        Please Add new clients using Add Clients Button to access full features..">
                    <span class="glyphicon glyphicon-info-sign"></span>
                </button>
            </td>
            <!--            <td class="text-center">-->
            <!--                <input type="text" class="form-control" value="Buyer type">-->
            <!--            </td>-->
            <td class="text-center">
                <button class="btn btn-default btn-sm has-popover" data-toggle="popover" data-id="0" data-html="true"
                        data-content="This is Dummy button.Please Add new clients using
                            Add Clients Button to access full features.."
                        data-placement="top" data-container="body"
                        title="Dummy Button"><span class="glyphicon glyphicon-phone-alt"></span></button>
            </td>
            <td class="text-center">
                <textarea cols="15" class="form-control" rows="1">Asset Name</textarea>
            </td>
            <td class="text-center">
                <select class="form-control">
                    <option>C</option>
                    <option>L</option>
                    <option>U</option>
                </select>
            </td>
            <td class="text-center">
                <button class="btn btn-default btn-sm dummy_notes has-popover" data-toggle="popover" data-id="0"
                        data-content="This is Dummy button.Please Add new clients using
                        Add Clients Button to access full features.." data-html="true"
                        data-placement="top" data-container="body"
                        title="Dummy Button">
                    <span class="glyphicon glyphicon-list"></span>
                </button>
            </td>
            <td class="text-center">
                <button class="btn btn-default btn-sm has-popover dummy_promo" data-toggle="popover"
                        title="Build Components." data-container="body" data-html="true" data-placement="top">
                    <span class="glyphicon glyphicon-send"></span>
                </button>
            </td>
            <td class="text-center">
                <button class="btn btn-default btn-sm has-popover dummy_sent" data-toggle="popover"
                        title="Dummy Sent Components" data-container="body" data-html="true" data-placement="top">
                    <span class="glyphicon glyphicon-envelope"></span>
                </button>
            </td>
            <td class="text-center">
                <button class="btn btn-default btn-sm dummy_acm has-popover" data-toggle="popover" data-id="0"
                        data-content="This is Dummy button.Please Add new clients using
                            Add Clients Button to access full features.." data-html="true"
                        data-placement="top" data-container="body"
                        title="Dummy Button">
                    <span class="glyphicon glyphicon-book"></span>
                </button>
            </td>
        </tr>
    </table>
    <!-- // Dummy Buyer Row-->

    <div id="coming_soon_content" class="hidden"><a href="#" class="pull-left"><img
                    src="https://cpd.powersellingtools.com/wp-content/uploads/2019/07/dcg.jpg"
                    style="height: 800px; width: 1000px;"></a>
    </div>

    <div id="coming_soon_dst" class="hidden"><a href="#" class="pull-left"><img
                    src="https://cpd.powersellingtools.com/wp-content/uploads/2019/07/dst.jpg"
                    style="height: 350px; width: 500px;"></a>
    </div>

    <!-- Dialogs -->
    <?php $this->partial('views/cpdv1/dialogs/cpd_ccm.php'); ?>
    <?php $this->partial('views/cpdv1/dialogs/move_deal.php'); ?>
    <?php $this->partial('views/cpdv1/dialogs/map.php'); ?>
    <?php $this->partial('views/cpdv1/dialogs/view_notes.php'); ?>
    <?php $this->partial('views/cpdv1/dialogs/view_sa.php'); ?>
    <?php $this->partial('views/cpdv1/dialogs/view_buyer_preference.php'); ?>
    <?php $this->partial('views/cpdv1/dialogs/view_comps.php'); ?>
    <?php $this->partial('views/cpdv1/dialogs/client_info.php'); ?>
    <?php $this->partial('views/cpdv1/dialogs/view_ccr.php'); ?>
    <?php $this->partial('views/collab/dialogs/add_recipient.php'); ?>
    <?php $this->partial('views/tsl/dialogs/view_call_notes.php'); ?>
    <?php $this->partial('views/dialogs/view_deal_identified.php'); ?>

    <!-- Load the javascript support. -->
    <script src="<?php echo $this->basepath; ?>resources/app/js/cpdv1/cpdv1.js"></script>