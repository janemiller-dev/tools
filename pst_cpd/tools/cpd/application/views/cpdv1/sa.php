<!-- Strategic Advisory Document.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/trix/dist/trix.css">
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/minified_cpdv1/sa.css"/>

<!-- Header Section-->
<div class="row">
    <div class="col-xs-12">
        <!-- Back Button-->
        <div class="col-xs-4">
            <a class="btn btn-secondary back-button" style="float: right; padding-right: 25px">
                <img src="https://cpd.powersellingtools.com/wp-content/uploads/2020/01/Component-Arrow-Back.png"
                     style="height: 5vh; width: 70%;" alt="back"></a>
        </div>
        <!--// Back Button-->

        <div class="col-xs-4">
            <h2 id="tsl-survey-heading" style="text-align:center" class="heading">Analysis Builder</h2>
        </div>

        <!-- Collaboration Button-->
        <div class="col-xs-4">
            <a class="btn btn-secondary collab-button" style="float: right; padding-right: 25px">
                <img src="http://cpd.powersellingtools.com/wp-content/uploads/2020/01/Collaboration-Arrows-2.png"
                     style="height: 5vh; width: 70%;" alt="collaboration">
            </a>
        </div>
        <!--// Collaboration Button-->
    </div>

    <div class="col-xs-12">

        <!--Investor Type Drop down.-->
        <div class="col-lg-3 col-xs-12 col-sm-12 col-md-3 script-box">
            <label class="col-xs-12" style="text-align: center">Investor Type:</label>
            <select class="form-control sa-menu">
                <option value="r">Radish</option>
                <option value="w">Wheat</option>
                <option value="t">Tree</option>
                <option value="i">Institutional</option>
            </select>
        </div>
        <!--// Investor Type Drop down.-->

        <!-- SA Delivery Mode drop down -->
        <div class="col-lg-3 col-xs-12 col-sm-12 col-md-3 script-box">
            <label class="col-xs-12" style="text-align: center">Delivery Mode:</label>
            <select class="form-control sa-menu">
                <option value="e">Email</option>
                <option value="s">Snail Mail</option>
                <option value="p">Phone</option>
                <option value="w">Web Meeting</option>
                <option value="i">In-Person Meeting</option>
                <option value="c">Combo</option>
            </select>
        </div>
        <!--// SA Delivery Mode drop down -->

        <!-- SA Type drop down -->
        <div class="col-lg-3 col-xs-12 col-sm-12 col-md-3 script-box">
            <label class="col-xs-12" style="text-align: center">Program Step:</label>
            <select class="form-control sa-menu" id="sa-type">
                <option value="sa" data-name="Strategic Analysis">Step 1: Initial Strategic Analysis</option>
                <option value="ma" data-name="Market Analysis">Step 2: Current Market Analysis</option>
                <option value="va" data-name="Value Analysis">Step 3: Current Asset Analysis</option>
                <option value="da" data-name="Demand Analysis">Step 4: Current Comps Analysis</option>
                <option value="oa" data-name="Operations Analysis">Step 5: Current Demand Analysis</option>
                <option value="ia" data-name="Investment Analysis">Step 6: Current Operations Analysis</option>
                <option value="ia" data-name="Investment Analysis">Step 7: Current Financial Analysis</option>
                <option value="ia" data-name="Investment Analysis">Step 8: Current Value Analysis</option>
                <option value="ia" data-name="Investment Analysis">Step 9: Projected Investment Analysis</option>
                <option value="ia" data-name="Investment Analysis">Step 10: Extended Strategic Plan</option>
            </select>
        </div>
        <!-- // SA Type drop down -->

        <!-- SA Page Template Version drop down -->
        <div class="col-lg-3 col-xs-12 col-sm-12 col-md-3 script-box">
            <label class="col-xs-12" style="text-align: center">Document Page:</label>
            <select class="form-control sa-menu" id="template-version">
                <option value="lp">Lead Page</option>
                <option value="wp">Welcome Page</option>
                <option value="pp">Project Page</option>
                <option value="ap">Analysis Page</option>
                <option value="rp">Recommendation Page</option>
                <option value="amp">Last Page</option>
            </select>
        </div>
        <!-- // SA Page Template Version drop down -->
    </div>
</div>
<!--// Header Section-->

<hr>
<div class="row">
    <!--Container for outline.-->
    <div class="col-xs-3">
        <div class="sidebar" id="outline-container" style="padding-left: 2px">
            <p id="tsl-outline">
            </p>
        </div>
    </div>
    <!--// Container for outline.-->

    <!--Canvas Container.-->
    <div class="col-md-7" id="sa-container">
        <div class="row">
            <div class="col-md-12 container">
                <div class="sidebar" id="canvas-container"
                     style="height: 83vh; position: absolute; width: 97%;"></div>
            </div>
        </div>
    </div>
    <!--// Canvas Container.-->

    <!--Container for Listing.-->
    <div class="col-md-2">
        <div class="sidebar" id="container-listing" style="word-break: break-all;">
        </div>
    </div>
    <!--// Container for Listing.-->
</div>

<!-- Promotional Buttons -->
<div class="row" style="margin-top: 1vh">
    <div class="col-md-3" align="center" id="button-container">
        <button id="compose-sa" class="btn btn-secondary sa-menu">Compose <i
                    class="fa fa-plus"></i></button>
        <!--Upload team Logo Button-->
        <!--        <input id="logo_upload" type="file" name="files[]" data-name="team"-->
        <!--               data-url= -->
        <? //= "https://" . $_SERVER['SERVER_NAME'] . "/tools/cpdv1/upload_logo" ?><!-- style="visibility:-->
        <!--               hidden; float:left; width:0;">-->
        <!--        <button id="upload-team-logo" class="btn-secondary btn upload-logo sa-menu" data-name="team">Team Logo <i-->
        <!--                    class="fa fa-image"></i></button>-->
        <!-- // Upload Logo Button-->

        <!--Upload Company Logo Buttons-->
        <!--        <button id="upload-company-logo" class="btn-secondary btn upload-logo sa-menu" data-name="company">-->
        <!--            Company Logo-->
        <!--            <i class="fa fa-image"></i></button>-->
        <!-- // Upload Company Logo Buttons-->

        <!-- Upload Background Image-->
        <input id="fileupload" type="file" name="files[]"
               data-url= <?= "https://" . $_SERVER['SERVER_NAME'] . "/tools/cpdv1/upload_sa" ?> style="visibility:
               hidden; float:left; width:0;">

        <button class="btn btn-secondary upload-sa sa-menu" data-number="1">Upload <i class="fa fa-upload"></i></button>
        </button>
        <!-- Upload Background Image-->

        <!-- Upload Second backgroung image-->
        <button class="btn btn-secondary upload-sa-2 sa-menu" data-number="2" style="display: none">Upload <i
                    class="fa fa-upload"></i></button>
        </button>
        <!-- //Upload Second backgroung image-->

        <!--Compile SA button-->
        <button id="compile-sa" class="btn btn-secondary sa-menu">Compile <i class="fa fa-file-pdf-o"></i>
        </button>
        <!--// Compile SA button-->

        <!--Expand SA button-->
        <button id="expand-sa" class="btn btn-secondary sa-menu">Fullscreen <i class="fa fa-arrows"></i></button>
        <!--//Expand SA button-->
    </div>

    <div class="col-xs-2 col-xs-offset-7" align="center">
        <button id="compose-new-sa" class="btn btn-secondary">Add New <i class="fa fa-plus-circle"></i>
        </button>
        <button id="send-sa" class="btn-secondary btn sa-menu">Send <i class="fa fa-envelope"></i></button>
        <button id="delete-sa" class="btn btn-secondary sa-menu">Delete <i class="fa fa-trash"></i></button>
    </div>
</div>
<!-- File Upload Progress bar.-->
<div class="col-sm-10 progress fileupload-progress"
     style="float: unset; margin: auto; padding-right: 0; padding-left: 0; display: none">
    <!-- The global progress bar -->
    <div class="progress progress-striped" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="70">
        <div class="progress-bar progress-bar-success" style="width: 0%;"></div>
    </div>
    <!-- The extended global progress state -->
    <div class="progress-extended">&nbsp;</div>
</div>

<h5 class="text-center" id="progress" style="color: green"></h5>
<h5 id="error" class="text-center" style="color: red"></h5>
<!-- File Upload Progress bar.-->

<?php $this->partial('views/cpdv1/dialogs/send_sa.php'); ?>
<?php $this->partial('views/tsl/dialogs/compose_promo.php'); ?>
<?php $this->partial('views/tsl/dialogs/delete_promo.php'); ?>

<script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>
<script src="<?php echo $this->basepath; ?>resources/trix/dist/trix.js"></script>
<script src="<?php echo $this->basepath; ?>resources/app/js/minified_cpdv1/sa.js"></script>