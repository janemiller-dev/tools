<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/cpdv1/config/main.js"></script>

<!-- Page Title -->
<div class="row">
    <div class="col-xs-12">
        <h3>Deal Management Dashboard Configurations</h3>
    </div>
</div>

<!-- Description -->
<div class="row">
    <div class="col-xs-12">
        <p>You can have a number of different Client Project Configurations. Each configuration can have different
            Floors, Games, Targets, Columns, Styles, and Phases. To add a new configuration, click the button below. If
            there is an existing configuration that you would like to use to start a custom configuration, you can
            "clone" it using the <span class="glyphicon glyphicon-duplicate"></span> button to the right of the existing
            configuration</p>.
    </div>
</div>

<!-- Show the add config button. -->
<div class="row">
    <div class="col-xs-12 text-left">
        <button type="button" id="add-config" class="btn btn-primary bottom-buffer" data-toggle="modal"
                data-target="#add-config-dialog">Add Deal Management Dashboard Configuration
        </button>
    </div>
</div>

<!-- Show the list of my DMD configs. -->
<div class="row">
    <div class="col-xs-12">

        <!-- DMD config table -->
        <div class="row" id="cpd-config-row">
            <div class="col-xs-12">
                <p id="no-cpd-configs">You do not have a Deal Management Dashboard Config setup. To create one, use the
                    Add Deal Management Dashboard Config button.</p>
                <div id="cpd-config-div" class="top-buffer">
                    <table class="table table-striped" id="cpd-config-table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Created</th>
                            <th>Modified</th>
                            <th class="text-center"><span class="glyphicon glyphicon-duplicate"></span></th>
                            <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- // DMD config table -->

    </div>
</div>

<!-- Dialogs -->
<?php $this->partial('views/cpdv1/dialogs/add_config.php'); ?>
<?php $this->partial('views/cpdv1/dialogs/delete_config.php'); ?>
<?php $this->partial('views/cpdv1/dialogs/copy_config.php'); ?>
