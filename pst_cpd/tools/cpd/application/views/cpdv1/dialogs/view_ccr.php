<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="view-ccr-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="view-ccr-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="view-ccr-title" class="modal-title">Call Control Report</h2>
            </div>

            <div class="modal-body">
                <!-- Show the list of promos. -->
                <table class="table" id="ccr-table">
                    <thead>
                    <th class="text-center">Date / Day</th>
                    <th class="text-center">Call Duration</th>
                    <th class="text-center">Call Purpose</th>
                    <th class="text-center">Call Rating</th>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>

        </div>

    </div>
</div>

<script src="<?php echo $this->basepath; ?>resources/app/js/cpdv1/dialog/view_ccr.js"></script>
