<!-- View Notes dialog view.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="view-notes-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="message-label" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="view-notes-title" class="modal-title">Events Notes</h2>
            </div>
            <div class="modal-body">

                <!-- Show the list of notes. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- List Notes -->
                        <div class="row" id="notes-row">
                            <div class="col-xs-12">
                                <div id="event-notes-div" class="top-buffer">

                                    <!-- Event name field.-->
                                    <label class="control-label col-xs-3">Event Name</label>
                                    <div class="col-xs-9" style="margin-bottom: 5px">
                                        <input type="text" class="form-control event-input event" data-name="dn_name"
                                               data-seq="0" id="dn_name0">
                                    </div>
                                    <span class="fa fa-trash delete-note" id="delete-note0"
                                          style="position: absolute; right: 10px;" data-seq="0"></span>
                                    <!--// Event name field.-->

                                    <!-- Event notes field.-->
                                    <label class="control-label col-xs-3">Event Notes</label>
                                    <div class="col-xs-9" style="margin-bottom: 5px">
                                        <textarea class="form-control event-text event" rows="3" style="width: 100%;"
                                                  data-name="dn_note" data-seq="0" id="dn_note0"></textarea>
                                    </div>
                                    <!--// Event notes field.-->

                                    <!-- Event date field.-->
                                    <label class="control-label col-xs-3">Event Date</label>
                                    <div class="col-xs-9" style="margin-bottom: 5px">
                                        <input type="text" class="form-control action-date event event-date"
                                        data-name="dn_when" data-seq="0" id="dn_date0">
                                    </div>
                                    <!--// Event date field.-->
                                </div>
                            </div>
                        </div>
                        <!-- // List Notes -->

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="add-event">Add Event</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/cpdv1/dialog/view_notes.js"></script>
