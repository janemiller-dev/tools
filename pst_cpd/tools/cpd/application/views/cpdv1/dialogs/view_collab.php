<!-- View Collab dialog view.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="view-collaboration-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="message-label" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="view-collab-title" class="modal-title">Collaboration</h2>
            </div>
            <!--// Modal Header-->

            <div class="modal-body">

                <!-- Show the list of collabs. -->
                <div class="row">
                    <div class="col-xs-12">
                        <!-- List Collaboration table -->
                        <div class="row" id="collab-dialog-row">
                            <div class="col-xs-12">
                                <div id="collaboration-div" class="top-buffer">
                                    <div class="col-xs-3">
                                        <label>Select Sender</label>
                                        <select class="form-control" id="collab-sender"></select>
                                    </div>
                                    <div class="col-xs-3">
                                        <label>Select Recipient</label>
                                        <select class="form-control" id="collab-recipient"></select>
                                    </div>
                                    <div class="col-xs-3">
                                        <label>Select Topic</label>
                                        <select class="form-control" id="collab-topic"></select>
                                    </div>
                                    <div class="col-xs-3">
                                        <label>Select Format</label>
                                        <select class="form-control" id="collab-format-select"></select>
                                    </div>
                                </div>

                                <div class="col-xs-12" id="collab-data-row">
                                </div>
                                <div class="hidden collab-box" id="dummy_collab_data">
                                    <label id="dummy-collab-label"></label>
                                    <span class="fa fa-trash delete-collab" style="float: right"></span>
                                    <textarea id="dummy-collab-text" class="form-control"
                                              style="width: 100%;"></textarea>
                                </div>
                            </div>
                        </div>
                        <!-- List Collaboration table -->
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="add-collab">Add</button>
                <button type="button" class="btn btn-primary" id="send-collab" data-toggle="modal"
                        data-target="#send-univ-collab-dialog">Send
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php $this->partial('views/cpdv1/dialogs/delete_collab.php'); ?>

<script src="<?php echo $this->basepath; ?>resources/app/js/cpdv1/dialog/view_collab.js"></script>
