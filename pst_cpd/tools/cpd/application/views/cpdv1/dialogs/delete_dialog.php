<!-- Delete DMD components view.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="delete-dialog" tabindex="-1" role="dialog" aria-labelledby="delete-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="delete-form" class="form-horizontal">

                <!--Modal Header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="delete-title" class="modal-title"></h2>
                </div>
                <!--// Modal Header-->

                <div class="modal-body">
                    <div class="form-group">
                        <!-- Name of Component-->
                        <label class="col-xs-12 col-sm-3 control-label" for="name">Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <p class="form-control-static" id="dialog-instance-name"></p>
                        </div>
                        <!--// Name of Component-->
                    </div>
                </div>

                <input type="hidden" id="id" name="id"/>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Delete</button>
                </div>

            </form>
        </div>
    </div>
</div>


<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/minified_cpdv1/dialog/delete_dialog.js"></script>

