<!-- Move deal dialog view-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="move-deal-dialog" tabindex="-1" role="dialog" aria-labelledby="move-deal-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="move-deal-form" class="form-horizontal">

                <!-- Modal Header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="move-deal-title" class="modal-title">Choose New Deal Status</h2>
                </div>
                <!--// Modal Header-->

                <!-- Modal Body-->
                <div class="modal-body">

                    <!-- Status Drop down. -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="new-deal-status">Select Status</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <select class="form-control" id="deal-new-status" name="deal_new_status">
                                </select>
                            </div>
                            <p class="form-text text-muted">Select your New Status</p>
                        </div>
                    </div>
                    <!--// Status Drop down. -->

                    <!-- Move date drop down -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="deal-new-move-date">Move Date</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <select class="form-control" id="deal-new-move-date" name="deal_new_move_date">
                                </select>
                            </div>
                            <p class="form-text text-muted">Select the projected deal move date</p>
                        </div>
                    </div>
                    <!--// Move date drop down -->

                    <!-- Hidden Input fields-->
                    <input type="hidden" class="form-control" id="new-status-deal-id" name="new_status_deal_id"/>
                    <input type="hidden" class="form-control" id="deal-current-status" name="deal_current_status"/>
                    <!--// Hidden Input fields-->
                </div>
                <!--// Modal Header-->

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>

            </form>
        </div>
    </div>
</div>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/cpdv1/dialog/move_deal.js"></script>
