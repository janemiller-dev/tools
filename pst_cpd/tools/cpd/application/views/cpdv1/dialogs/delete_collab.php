<!-- Delete Collaboration view-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="delete-univ-collab-dialog" tabindex="-1" role="dialog" aria-labelledby="delete-collab-label"
     aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <form id="delete-collab-form" class="form-horizontal">

                <!-- Modal Header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="delete-collab-title" class="modal-title">Delete Collaboration?</h2>
                </div>
                <!-- // Modal Header-->

                <div class="modal-body">
                    <div class="form-group">
                        <!-- Collaboration name-->
                        <label class="col-xs-12 col-sm-3 control-label" for="delete-collab-name">From</label>
                        <div class="col-xs-12 col-sm-9">
                            <p class="form-control-static" id="delete-collab-name"></p>
                        </div>
                        <!--// Collaboration name-->
                        <!-- Format name-->
                        <label class="col-xs-12 col-sm-3 control-label" for="delete-collab-name">Format</label>
                        <div class="col-xs-12 col-sm-9">
                            <p class="form-control-static" id="delete-format-name"></p>
                        </div>
                        <!--// Format name-->
                    </div>
                </div>

                <input type="hidden" id="delete-collab-id" name="collab_id"/>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="button" id="delete-collab-message" class="btn btn-primary">Delete</button>
                </div>

            </form>
        </div>
    </div>
</div>
