<!-- Delete Lead view-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="delete-lead-dialog" tabindex="-1" role="dialog" aria-labelledby="delete-lead-label"
     aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <form id="delete-lead-form" class="form-horizontal">

                <!-- Modal Header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="delete-lead-title" class="modal-title">Delete Lead?</h2>
                </div>
                <!-- // Modal Header-->

                <div class="modal-body">
                    <div class="form-group">
                        <!-- Deal Lead name-->
                        <label class="col-xs-12 col-sm-3 control-label" for="delete-lead-name">Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <p class="form-control-static" id="delete-lead-name"></p>
                        </div>
                        <!--// Deal Lead name-->
                    </div>
                </div>

                <input type="hidden" id="delete-lead-id" name="lead_id"/>
                <input type="hidden" id="delete-lead-type" name="type"/>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Delete</button>
                </div>

            </form>
        </div>
    </div>
</div>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/minified_cpdv1/dialog/delete_lead.js"></script>
