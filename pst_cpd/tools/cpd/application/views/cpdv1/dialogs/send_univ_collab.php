<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="send-univ-collab-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="send-collab-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="collab-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="send-collab-title" class="modal-title">Collaboration Request</h2>
                </div>

                <div class="modal-body">

                    <div class="form-group col-xs-4 send-collab-div">
                        <label class="control-label">Sender Name:</label>
                        <div class="" style="padding: 0">
                            <input type="text" class="form-control" id="send-collab-sender-name" name="sender">
                        </div>
                    </div>

                    <div class="form-group col-xs-4 send-collab-div">
                        <label class="control-label" for="collab-client-email">Recipient Name:</label>
                        <select class="form-control" id="collab-send-recipient" name="recipient_id"></select>
                    </div>

                    <div class="form-group col-xs-4 send-collab-div">
                        <label class="control-label" for="collab-client-email">Format:</label>
                        <select class="form-control" id="collab-send-format" name="format_id"></select>
                    </div>

                    <div class="form-group" style="margin: 5px !important;">
                        <label class="col-sm-2" for="collab-client-email">Email:</label>
                        <div class="col-sm-10 input-group univ-client-collab-div">
                            <input type="email" class="form-control collab-client-email" name="client_email[]">
                            <span class="input-group-btn">
                                <span class="has-tooltip" title="Add Recipient" data-placement="bottom">
                                    <button class="btn btn-default" type="button" id="add-univ-recipient">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </span>
                            </span>
                        </div>
                    </div>

                    <div class="form-group" style="margin: 5px !important;">
                        <label class="col-xs-2" for="collab-client-subject">Subject:</label>
                        <div class="col-xs-10" style="padding: 0">
                            <input type="text" class="form-control" id="collab-client-subject" name="subject">
                        </div>
                    </div>

                    <div id="email-content" class="form-group" style="margin: 5px !important;">
                        <label class="col-xs-2" for="collab-client-email">Message:</label>
                        <div class="col-xs-10" style="padding: 0">
                        <textarea cols="2" placeholder="Email Text" name="text" class="form-control"
                                  style="width: 100%" id="collab-client-content"></textarea>
                        </div>
                    </div>

                    <input type="hidden" id="collab-type" name="type"/>
                    <input type="hidden" id="send-collab-user-email" name="user_email"/>
                    <input type="hidden" id="send-collab-token" name="token"/>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="close-send-collab-dialog" data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary" id="send-collab-button">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>
