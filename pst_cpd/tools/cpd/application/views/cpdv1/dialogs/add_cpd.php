<!-- Add Deal Management Dashboard Instance.-->

<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<div class="modal fade" id="add-cpd-dialog" tabindex="-1" role="dialog" aria-labelledby="add-cpd-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="add-cpd-form" class="form-horizontal">

                <!-- Modal header section. -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-cpd-title" class="modal-title">Add Deal Management Dashboard Instance</h2>
                </div>
                <!--// Modal header section. -->

                <!-- Modal body section. -->
                <div class="modal-body">

                    <!-- Year drop down -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="cpd-year">Select Year</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <select class="form-control" id="cpd-year" name="cpd_year">
                                </select>
                            </div>
                            <p class="form-text text-muted">Select your DMD Year</p>
                        </div>
                    </div>
                    <!-- // Year drop down -->

                    <!-- Industry Drop down -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="cpd-product-id">Select
                            Industry</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <select class="form-control" id="cpd-industry" name="cpd_industry_id">
                                    <option> Commercial Real Estate </option>
                                </select>
                            </div>
                            <p class="form-text text-muted">Select Industry.</p>
                        </div>
                    </div>
                    <!-- // Industry Drop down -->

                    <!-- Profession Drop down -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="cpd-product-id">Select
                            Profession</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <select class="form-control" id="cpd-profession" name="cpd_professsion_id">
                                    <option> Investment Sales </option>
                                </select>
                            </div>
                            <p class="form-text text-muted">Select Profession Type.</p>
                        </div>
                    </div>
                    <!-- // Profession Drop down -->

                    <!-- Product Type drop down -->
<!--                    <div class="form-group">-->
<!--                        <label class="col-xs-12 col-sm-3 control-label" for="cpd-product-id">Select Product Type</label>-->
<!--                        <div class="col-xs-12 col-sm-9">-->
<!--                            <div class="input-group">-->
<!--                                <select class="form-control" id="cpd-product-id" name="cpd_product_id">-->
<!--                                </select>-->
<!--                            </div>-->
<!--                            <p class="form-text text-muted">Select Product Type.</p>-->
<!--                        </div>-->
<!--                    </div>-->
                    <!--// Product Type drop down -->

                    <!-- DMD name field. -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="cpd_ui_name">DMD Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="cpd_ui_name" name="name"/>
                            <p class="form-text text-muted">Enter the name of the new Deal Management Dashboard
                                Instance.</p>
                        </div>
                    </div>
                    <!-- // DMD name field. -->

                    <!-- DMD description field. -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="cpd_ui_description">DMD Description</label>
                        <div class="col-xs-12 col-sm-9">
                            <textarea class="form-control" rows="5" width="100%" name="description"
                                      id="cpd_ui_description"></textarea>
                        </div>
                    </div>
                    <!-- // DMD description field. -->
                </div>
                <!-- // Modal body section. -->

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>

            </form>
        </div>
    </div>
</div>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/minified_cpdv1/dialog/add_cpd.js"></script>
