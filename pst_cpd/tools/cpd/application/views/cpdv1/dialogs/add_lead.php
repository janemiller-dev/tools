<!-- Add Deal Management Dashboard Lead Instance.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="add-lead-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="add-lead-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="add-lead-form" class="form-horizontal">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-lead-title" class="modal-title">Add Lead</h2>
                </div>
                <!-- // Modal Header -->

                <div class="modal-body modal-small">
                    <!-- Lead Name field. -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" id="name-label" for="lead-name">Lead
                            Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="lead-name" name="lead_name"/>
                            <p class="form-text text-muted" id="name-para">Enter the Name of the lead.</p>
                            <input type="hidden" id="cpd-id" name="cpd_id">
                            <input type="hidden" id="lead-type" name="lead_type">
                        </div>
                    </div>
                    <!--// Lead Name field. -->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="close-add-lead-dialog" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/cpdv1/dialog/add_lead.js"></script>

