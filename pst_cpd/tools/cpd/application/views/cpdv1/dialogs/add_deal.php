<!-- Add Deal Management Dashboard Deal Instance.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="add-deal-dialog" tabindex="-1" role="dialog" aria-labelledby="add-deal-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="add-deal-form" class="form-horizontal">

                <!-- Modal header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-deal-title" class="modal-title">Add Client</h2>
                </div>
                <!--// Modal header -->

                <div class="modal-body">

                    <!-- Client Name -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="deal-name">Client</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="deal-name" name="deal_name"/>
                            <p class="form-text text-muted">Enter the name of new Deal/Client.</p>
                        </div>
                    </div>
                    <!--// Client Name -->

                    <!-- Location field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="property-name">Location</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="property-name" name="property_name"/>
                            <p class="form-text text-muted">Enter the property location.</p>
                        </div>
                    </div>
                    <!-- // Location field -->

                    <!-- Email field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="email">Email</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="client-email" name="client_email"/>
                            <p class="form-text text-muted">Enter Client Email.</p>
                        </div>
                    </div>
                    <!--// Email field -->

                    <!-- Phone field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="property-name">Phone</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="client-phone" name="client_phone"/>
                            <p class="form-text text-muted">Enter Client Phone.</p>
                        </div>
                    </div>
                    <!-- // Phone field -->

                    <!-- Deal Status field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="deal-status">Deal Status</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <select class="form-control" id="deal-status" name="deal_status">
                                </select>
                            </div>
                            <p class="form-text text-muted">Select the deal Status</p>
                        </div>
                    </div>
                    <!--// Deal Status field -->

                    <!-- Hiddem Input fields.-->
                    <input type="hidden" id="add-deal-cpd-id" name="add_deal_cpd_id"/>
                    <input type="hidden" id="add-deal-status-type" name="add_deal_status_type"/>
                    <input type="hidden" id="add-deal-long" name="add_deal_long"/>
                    <input type="hidden" id="add-deal-lat" name="add_deal_lat"/>
                    <!--// Hiddem Input fields.-->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/minifed_cpdv1/dialog/add_deal.js"></script>
