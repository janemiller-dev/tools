<!-- View Lead dialog view.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="view-lead-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="message-label" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <!-- Modal Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="view-lead-title" class="modal-title">View/Edit Lead</h2>
                <button type="button" id="add-lead-button" class="btn btn-primary bottom-buffer" data-type="Lead"
                        data-target="#add-lead-dialog" data-toggle="modal" data-backdrop="static">Add New Lead
                </button>
            </div>
            <!--// Modal Header-->

            <div class="modal-body">

                <!-- Show the list of leads. -->
                <div class="row">
                    <div class="col-xs-12">
                        <!-- List Lead table -->
                        <div class="row" id="lead-row">
                            <div class="col-xs-12">
                                <p id="no-lead">You have no instance of Leads. Use the add lead to add a new lead.
                                </p>
                                <div id="lead-div" class="top-buffer">
                                    <table class="table" id="lead-table">
                                        <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Lead Name</th>
                                            <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- List Lead table -->
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php $this->partial('views/cpdv1/dialogs/delete_lead.php'); ?>
<?php $this->partial('views/cpdv1/dialogs/add_lead.php'); ?>