<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/cpdv1/dialog/view_ccm.js"></script>

<div class="modal fade" id="view-ccm-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="message-h4" aria-hidden="true">
    <div class="modal-dialog ccm-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row" style="border: solid 6px #A4A4A4; margin: 2px">
                    <div class="col-xs-12" style="padding: 0">
                        <table style="border-collapse: separate; border-spacing: 0 0.5em;">
                            <tr class="row">
                                <td rowspan="3" class="col-xs-4"
                                    style="vertical-align: top; padding-left: 15px; height: 1px">
                                    <div class="ccm-box" style="height: 100%">
                                        <div>
                                            <h4 id="view-reader-title" class="text-center"
                                                style="border-bottom: 1px solid #a4a4a463; padding-bottom: 10px">Prompter</h4>
                                        </div>
                                        <div style="overflow-y: hidden; scroll-behavior: smooth; height: 85%">
                                            <div class="row" id="titles">
                                                <div id="credits" class="col-xs-12" style="font-size: 20px">
                                                </div>
                                            </div>
                                        </div>
                                        <div style="border-top: 1px solid #e5e5e5; padding-top: 10px"
                                             class="row text-center">
                                            <div class="col-xs-3">
                                                <input type="range" min="12" max="20" value="15" id="speed-control"
                                                       style="width: 5vw; float: right"/>
                                            </div>
                                            <div class="col-xs-9" style="padding-bottom: 15px">
                                                <a class="btn btn-primary" href="#creditos" name="creditos"
                                                   style="margin-left: 1vw">Start
                                                    <i class=" fa fa-play"></i></a>
                                                <button type="button" class="btn btn-primary" id="stop">
                                                    Stop
                                                    <i class="fa fa-stop"></i></button>
                                                <button type="button" class="btn btn-primary" id="back">Back
                                                    <i class="fa fa-arrow-circle-down"></i>
                                                </button>
                                                <button type="button" class="btn btn-primary" id="next">Next
                                                    <i class="fa fa-arrow-circle-up"></i></button>
                                                <button type="button" class="btn btn-primary" id="reset"
                                                        style="margin-right: 1%">Reset <i class="fa fa-refresh"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </td>

                                <td rowspan="3" class="col-xs-4" style="vertical-align: top; height: 1px">
                                    <h1 class="text-center">Call Control Dashboard</h1>

                                    <!-- CCM Dialog -->
                                    <div class="ccm-box row" style="height: 70%;">
                                        <!--                                    <div class="col-xs-12 ccm-box">-->

                                        <div class="text-center">
                                            <div class="col-xs-4">
                                                <h4 style="display: inline-block">Start Call</h4>
                                                <input type="radio" class="start-end-call" name="start-end-call"
                                                       data-type="start">
                                            </div>
                                            <div class="col-xs-4 tsl-ccm-view">
                                                <h4 style="display: inline-block;">No Answer</h4>
                                                <input id="ccm-na" type="radio" class="start-end-call edit-client-info"
                                                       data-name="tac_valid_phone" data-type="end"
                                                       value="na" data-table="tsl_about_client">
                                            </div>
                                            <div class="col-xs-4">
                                                <h4 style="display: inline-block;">End Call</h4>
                                                <input type="radio" class="start-end-call" name="start-end-call"
                                                       data-type="end">
                                            </div>
                                        </div>
                                        <div>
                                            <h3 class="text-center"
                                                style="padding-top: 3%; margin-top: 7.6%;
                                                 border-top: solid 1px #a4a4a463">
                                                Call Control Panel</h3>
                                        </div>

                                        <div class="text-center" style="margin-top: 4%">
                                            <div class="col-xs-4">
                                                <h4>Call Start</h4>
                                                <input type="text" class="form-control" id="ccm-start-time">
                                            </div>
                                            <div class="col-xs-4">
                                                <h4>Call End</h4>
                                                <input type="text" class="form-control" id="ccm-end-time">
                                            </div>
                                            <div class="col-xs-4">
                                                <h4>Duration</h4>
                                                <input type="text" class="form-control" id="ccm-duration">
                                            </div>
                                        </div>

                                        <div class="text-center row" id="scoreboard-control" style="margin-top: 18%">
                                            <div class="col-xs-20">
                                                <h4 style="display: inline-block">Dial</h4>
                                                <input type="radio" class="update-scoreboard" name="update-score"
                                                       data-type="dial">
                                            </div>
                                            <div class="col-xs-20">
                                                <h4 style="display: inline-block">Contact</h4>
                                                <input type="radio" class="update-scoreboard" name="update-score"
                                                       data-type="contact">
                                            </div>
                                            <div class="col-xs-20">
                                                <h4 style="display: inline-block">Message</h4>
                                                <input type="radio" class="update-scoreboard" name="update-score"
                                                       data-type="message">
                                            </div>
                                            <div class="col-xs-20">
                                                <h4 style="display: inline-block">Conv</h4>
                                                <input type="radio" class="update-scoreboard" name="update-score"
                                                       data-type="conv">
                                            </div>
                                            <div class="col-xs-20">
                                                <h4 style="display: inline-block">Meeting</h4>
                                                <input type="radio" class="update-scoreboard" name="update-score"
                                                       data-type="meeting">
                                            </div>

                                        </div>

                                        <hr/>
                                        <div class="text-center row" style="margin: 0;">
                                            <div class="col-xs-6">
                                                <label>Select Script</label>
                                                <div class="input-group">
                                                    <select id="ccm-script"
                                                            class="form-control input-group-addon ccm-select"
                                                            data-type="script"
                                                            style="background-color: #fff !important;"></select>
                                                    <span class="input-group-btn">
                                    <button class="btn btn-default btn-sm show-prompter">
                                        <span class="glyphicon glyphicon-list"></span>
                                    </button>
                                </span>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <label>Select Message</label>
                                                <div class="input-group">
                                                    <select id="ccm-message"
                                                            class="form-control input-group-addon ccm-select"
                                                            data-type="message"
                                                            style="background-color: #fff !important;"></select>
                                                    <span class="input-group-btn">
                                    <button class="btn btn-default btn-sm show-prompter">
                                        <span class="glyphicon glyphicon-list"></span>
                                    </button>
                                </span>
                                                </div>
                                            </div>

                                            <!--                                            <div class="col-xs-12 text-center tsl-ccm-view" style="margin-top: 15px">-->
                                            <!--                                                <h4 style="display: inline-block;">Bad Number</h4>-->
                                            <!--                                                <input type="radio" class="ccm-mark-bad start-end-call"-->
                                            <!--                                                       name="start-end-call" data-type="end">-->
                                            <!--                                            </div>-->
                                        </div>
                                        <hr/>
                                        <div style="margin-right: 10px; text-align: right">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close
                                            </button>
                                        </div>
                                        <!--                                    </div>-->
                                        <!--// CCM Dialog -->
                                    </div>

                                    <div class="hidden ccm-box row"
                                         style="vertical-align: top; margin-top: 1em; height: 28%"
                                         id="control-panel">
                                        <!-- Conversation Control Panel-->
                                        <!--                                    <div class="col-xs-12 ccm-box" id="ccm-control-row">-->
                                        <h4 class="text-center" style="padding-bottom: 10px">
                                            Conversation Control Panel</h4>
                                        <hr style="margin: 0 40px 0 40px;"/>
                                        <div class="row" style="margin: 20px 12% 20px 12%">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th class="text-center">Profile</th>
                                                    <th class="text-center">Asset</th>
                                                    <th class="text-center">SA</th>
                                                    <th class="text-center">Maps</th>
                                                    <th class="text-center">Comps</th>
                                                    <th class="text-center">Value</th>
                                                    <th class="text-center">Income</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <button class="btn btn-sm btn-default" data-toggle="modal"
                                                                data-target="#client-info-dialog">
                                                            <span class="glyphicon glyphicon-list-alt"></span>
                                                        </button>
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-default btn-sm" data-toggle="modal"
                                                                data-target="#view-asset-info-dialog"
                                                                data-current-status="contract">
                                                            <span class="glyphicon glyphicon-info-sign"></span>
                                                        </button>
                                                    </td>
                                                    <td>
                                                        <button class="btn btn-default btn-sm" data-toggle="modal"
                                                                data-target="#view-sa-dialog">
                                                            <span class="glyphicon glyphicon-list-alt"></span>
                                                        </button>
                                                    </td>
                                                    <td>
                                                        <button type="button" data-container="body"
                                                                data-title="Income Information."
                                                                class="btn btn-default btn-sm has-popover amount_button"
                                                                data-html="true" data-toggle="popover"
                                                                data-placement="top">
                                                            <span class="glyphicon glyphicon-map-marker"></span>
                                                        </button>
                                                    </td>
                                                    <td>
                                                        <button type="button" data-container="body"
                                                                data-title="Income Information."
                                                                class="btn btn-default btn-sm has-popover amount_button"
                                                                data-html="true" data-toggle="popover"
                                                                data-placement="top">
                                                            <span class="glyphicon glyphicon-map-marker"></span>
                                                    </td>
                                                    <td>
                                                        <button type="button" data-container="body"
                                                                data-title="Income Information."
                                                                class="btn btn-default btn-sm has-popover"
                                                                data-html="true"
                                                                data-toggle="popover" data-placement="top">
                                                            <span class="glyphicon glyphicon-usd"></span>
                                                        </button>
                                                    </td>
                                                    <td>
                                                        <button type="button" data-container="body"
                                                                data-title="Income Information."
                                                                class="btn btn-default btn-sm has-popover"
                                                                data-html="true"
                                                                data-toggle="popover" data-placement="top">
                                                            <span class="glyphicon glyphicon-usd"></span>
                                                        </button>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!--                                    </div>-->
                                        <!--// Conversation Control Panel-->
                                    </div>

                                    <div class="ccm-box row"
                                         style="vertical-align: top; padding: 0; margin-top: 1em; height: 28%"
                                         id="call-dashboard">
                                        <h4 id="view-reader-title"
                                            style="border-bottom: 1px solid #a4a4a463; padding-bottom: 10px; margin: 0;">
                                            Call
                                            Scoreboard
                                            <span style="float: right;" id="ccm-next-day"><i
                                                        class="fa fa-arrow-circle-right"></i></span>
                                            <input style="float: right; margin-right: 10px;" id="ccm-date-picker"/>
                                            <span style="float: right; margin-right: 10px;" id="ccm-prev-day"><i
                                                        class="fa fa-arrow-circle-left"></i></span>
                                            <label style="float: right; margin-right: 10px;">Select Date: </label>
                                        </h4>
                                        <table class="table" style="height: 80%">
                                            <tbody>
                                            <?php for ($j = 0; $j < 5; $j++) { ?>
                                                <tr style="height: 20%">
                                                    <?php for ($i = 0; $i < 12; $i++) { ?>
                                                        <td class="scoreboard-span"></td>
                                                    <?php } ?>
                                                </tr>
                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </td>

                                <td class="col-xs-4" style="vertical-align: top">
                                    <!-- Notes division -->
                                        <div id="event-notes-div" class="col-xs-12 ccm-box" style="margin-top: 0">
                                            <div>
                                                <h4 class="text-center"
                                                    style="border-bottom: 1px solid #a4a4a463; padding-bottom: 10px">
                                                    Call
                                                    Back</h4>
                                            </div>

                                            <!-- Date Stamp Field -->
                                            <div>
                                                <label class="control-label col-xs-3 date-label">Date</label>
                                                <div class="col-xs-9" style="margin-bottom: 5px">
                                                    <input type="datetime" class="form-control date call-notes-date"
                                                           data-seq="0"
                                                           id="notes-date0" disabled>
                                                </div>
                                            </div>
                                            <!--// Date Stamp Field -->

                                            <!-- Topic Text Field -->
                                            <label class="control-label col-xs-3 topic-label">Topic</label>
                                            <div class="col-xs-9" style="margin-bottom: 5px">
                                                <input type="text" class="form-control notes notes-topic"
                                                       data-name="tn_topic"
                                                       data-seq="0" id="notes-topic0">
                                            </div>
                                            <!--// Topic Text Field -->

                                            <!-- Notes Text Area -->
                                            <label class="control-label col-xs-3 notes-label">Notes</label>
                                            <div class="col-xs-9" style="margin-bottom: 5px">
                                        <textarea class="form-control notes-input notes" data-name="tn_content"
                                                  data-seq="0" id="notes_content0" rows="2"></textarea>
                                            </div>
                                            <!--// Notes Text Area -->

                                            <!-- Action Field -->
                                            <div>
                                                <label class="control-label col-xs-3 date-label">Action</label>
                                                <div class="col-xs-9" style="margin-bottom: 5px">
                                                    <input type="datetime" class="form-control notes" data-seq="0"
                                                           id="notes-action0" data-name="tn_action">
                                                </div>
                                            </div>
                                            <!--// Action Field -->

                                            <!-- Date Stamp Field -->
                                            <div>
                                                <label class="control-label col-xs-3 date-label">By When</label>
                                                <div class="col-xs-9" style="margin-bottom: 5px">
                                                    <input type="datetime" class="form-control datetimepicker notes"
                                                           data-seq="0" id="next-call-date0" data-name="tn_next_call">
                                                </div>
                                            </div>
                                            <!--// Date Stamp Field -->

                                        </div>
                                    <!--// Notes division -->
                                </td>
                            </tr>
                            <tr class="row">
                                <td class="col-xs-4">
                                    <!-- Action Div -->
                                    <div class="col-xs-12 ccm-box" id="ccm-action-row">
                                        <div>
                                            <h4 class="text-center"
                                                style="border-bottom: 1px solid #a4a4a463; padding-bottom: 10px">Next
                                                Actions</h4>
                                        </div>
                                        <div class="col-xs-12">
                                            <div id="view-ccm-action-div" class="top-buffer row">
                                                <!-- Date field-->
                                                <label class="control-label col-xs-3">By When</label>
                                                <div class="col-xs-9" style="margin-bottom: 5px">
                                                    <input class="form-control action tsl-date" data-seq="0"
                                                           id="action_date0" disabled>
                                                </div>
                                                <!-- Date field-->

                                                <!-- Action Text Field -->
                                                <label class="control-label col-xs-3 action-label">Action</label>
                                                <div class="col-xs-9" style="margin-bottom: 5px">
                                        <textarea class="form-control action-input action" data-name="ta_content"
                                                  data-seq="0" id="action_content0" rows="2"
                                                  style="width: 100%;"></textarea>
                                                </div>
                                                <!--// Action Text Field -->

                                                <!-- By Whom Select-->
                                                <label class="control-label col-xs-3">By Whom</label>
                                                <div class="col-xs-9" style="margin-bottom: 5px">
                                                    <select class="form-control action-whom action" data-seq="0"
                                                            id="action_whom0"
                                                            data-name="ta_whom" style="width: 100%;"></select>
                                                </div>
                                                <!--// By Whom Select-->

                                                <!-- By When Select-->
                                                <label class="control-label col-xs-3">By When</label>
                                                <div class="col-xs-9" style="margin-bottom: 5px">
                                                    <input class="form-control action-when action datetimepicker"
                                                           data-seq="0"
                                                           id="action_when0"
                                                           data-name="ta_when">
                                                </div>
                                                <!-- By When Select-->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- // Action Division -->
                                </td>
                            </tr>
                            <tr class="row">
                                <td class="col-xs-4">

                                    <!-- Confirm Divison -->
                                    <div class="col-xs-12 ccm-box" id="ccm-confirm-row">
                                        <h4 class="text-center"
                                            style="border-bottom: 1px solid #a4a4a463; padding-bottom: 10px">Event
                                            Confirmation</h4>

                                        <!-- Event Name Field -->
                                        <label class="control-label col-xs-3">Event Type</label>
                                        <div class="col-xs-9" style="margin-bottom: 5px">
                                            <input type="text" class="form-control confirm" id="confirm-name0"
                                                   data-name="tsl_confirm_name" data-seq="0"/>
                                        </div>
                                        <!--// Event Name Field -->

                                        <!-- Event Topic Field-->
                                        <label class="control-label col-xs-3">Event Topic</label>
                                        <div class="col-xs-9" style=" margin-bottom: 5px">
                                            <input type="text" class="form-control confirm" data-seq="0"
                                                   id="confirm-topic0" data-name="tsl_confirm_topic"/>
                                        </div>
                                        <!--// Event Topic Field-->

                                        <!-- Notes Textarea-->
                                        <label class="control-label col-xs-3">Event Notes</label>
                                        <div class="col-xs-9" style="margin-bottom: 5px">
                                    <textarea class="form-control confirm-input confirm"
                                              data-seq="0" id="confirm-content0" rows="2" data-name="tsl_confirm_note"
                                              style="width: 100%;"></textarea>
                                        </div>
                                        <!--// Notes Textarea-->

                                        <!-- Event Site Input-->
                                        <label class="control-label col-xs-3">Event Site</label>
                                        <div class="col-xs-9" style="margin-bottom: 5px">
                                            <input class="form-control confirm-site confirm" data-seq="0"
                                                   id="confirm-site0" style="width: 100%;"
                                                   data-name="tsl_confirm_site"/>
                                        </div>
                                        <!--// Event Site Input-->

                                        <!-- By When-->
                                        <label class="control-label col-xs-3"> By When</label>
                                        <div class="col-xs-9" style=" margin-bottom: 5px">
                                            <input class="form-control confirm-when confirm datetimepicker" data-seq="0"
                                                   id="confirm-when0" data-name="tsl_confirm_when">
                                        </div>
                                        <!-- By When-->
                                    </div>
                                    <!--// Confirm Divison -->
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <!--            <div class="modal-footer">-->
            <!--                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>-->
            <!--            </div>-->
        </div>
    </div>
</div>