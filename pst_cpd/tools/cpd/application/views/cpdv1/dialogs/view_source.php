<!-- View Source Modal dialog-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="view-source-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="message-label"
     aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <!-- Modal Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="view-source-title" class="modal-title">View/Edit Source</h2>
                <button type="button" id="add-source-button" class="btn btn-primary bottom-buffer"
                        data-target="#add-lead-dialog" data-toggle="modal" data-backdrop="static" data-type="Source">
                    Add New Source</button>
            </div>
            <!-- // Modal Header-->

            <div class="modal-body">
                <!-- Show the list of sources. -->
                <div class="row">
                    <div class="col-xs-12">
                        <!-- List Source table -->
                        <div class="row" id="source-row">
                            <div class="col-xs-12">
                                <p id="no-source">You have no instance of Sources. Use the add source to add a new
                                    source.</p>
                                <div id="source-div" class="top-buffer">
                                    <table class="table" id="source-table">
                                        <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Source Name</th>
                                            <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- List Source table -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

