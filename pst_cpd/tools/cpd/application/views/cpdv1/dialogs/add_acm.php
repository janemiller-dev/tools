<!-- Add Accountablity Manager Requests/Promises view.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="add-acm-dialog" tabindex="-1" role="dialog" aria-labelledby="add-acm-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="add-acm-form" class="form-horizontal">

                <!-- Modal Header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-acm-title" class="modal-title">Add Accountability Manager</h2>
                </div>
                <!--// Modal Header-->

                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="acm-ceo-id">Select Critical, Essential,
                            Optional</label>
                        <div class="col-xs-12 col-sm-9">
                            <!-- Critical/Essential/Optional drop down selector. -->
                            <select class="form-control" id="acm-ceo-id" name="acm_ceo_id">
                                <option value=0>-- Select CEO --</option>
                                <option value="c">Critical</option>
                                <option value="e">Essential</option>
                                <option value="o">Optional</option>
                            </select>
                            <!-- Critical/Essential/Optional drop down selector. -->
                            <p class="form-text text-muted">Select CEO.</p>
                        </div>
                    </div>

                    <!-- Action Field.-->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="acm-action">Enter Action</label>
                        <div class="col-xs-12 col-sm-9">
                                <textarea class="form-control" rows="1" onkeyup="auto_grow(this)" id="acm-action"
                                          name="acm_action"></textarea>
                            <p class="form-text text-muted">Enter Future Action</p>
                        </div>
                    </div>
                    <!--// Action Field.-->

                    <!-- By whom Field.-->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="acm-whom">By Whom</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="acm-whom" name="acm_whom"/>
                            <p class="form-text text-muted">Enter By Whom.</p>
                        </div>
                    </div>
                    <!-- // By whom Field.-->

                    <!-- By when Field.-->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="acm_when">By When</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control datetimepicker" id="acm_when" name="acm_when"/>
                            <p class="form-text text-muted">Enter By When</p>
                        </div>
                        <input type="hidden" name="deal_id" id="deal-id"/>
                    </div>
                    <!--// By when Field.-->

                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/minified_cpdv1/dialog/add_acm.js"></script>

