<!-- Compose Strategic Advisory Document view.-->


<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="compose-sa-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="compose-sa-label"
     aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <!-- Modal Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="compose-sa-title" class="modal-title">Compose Promo</h2>
            </div>
            <!--// Modal Header-->

            <div class="modal-body">
                <!-- Compose SA form-->
                <form id="compose-sa-form" class="form-horizontal sa-form">
                <!--// Compose SA form-->
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" class="close">Close</button>
                <button type="button" id="save-sa" class="btn btn-primary" data-target="#edit-sa-dialog"
                        data-toggle="modal">View
                </button>
            </div>
            <input type="hidden" id="question_length" name="question_length">
            </form>
        </div>
    </div>
</div>


