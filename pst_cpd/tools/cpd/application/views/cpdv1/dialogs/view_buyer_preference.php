<!-- View Collab dialog view.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="view-buyer-preference-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="message-label" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 class="modal-title" id="prefer-title"></h2>
            </div>
            <!--// Modal Header-->

            <div class="modal-body" style="height: 75vh; overflow-y: scroll">
                <div class="hidden prefer-content" id="prefer-div">
                    <h4>Product Type</h4>
                    <textarea cols="2" class="form-control" data-name="bp_product"></textarea>
                    <h4>Market Area</h4>
                    <textarea cols="2" class="form-control" data-name="bp_market"></textarea>
                    <h4>Units Number</h4>
                    <textarea cols="2" class="form-control" data-name="bp_unit"></textarea>
                    <h4>Sq. Ft. Number</h4>
                    <textarea cols="2" class="form-control" data-name="bp_sq_ft"></textarea>
                    <h4>Condition</h4>
                    <textarea cols="2" class="form-control" data-name="bp_cond"></textarea>
                    <h4>Criteria</h4>
                    <textarea cols="2" class="form-control" data-name="bp_criteria"></textarea>
                    <h4>Price Range</h4>
                    <textarea cols="2" class="form-control" data-name="bp_price"></textarea>
                    <h4>CAP Rate</h4>
                    <textarea cols="2" class="form-control" data-name="bp_cap"></textarea>
                    <h4>Financing</h4>
                    <textarea cols="2" class="form-control" data-name="bp_financing"></textarea>
                    <h4>Contingencies</h4>
                    <textarea cols="2" class="form-control" data-name="bp_contingency"></textarea>
                    <h4>Terms</h4>
                    <textarea cols="2" class="form-control" data-name="bp_term"></textarea>
                    <h4>Strategy</h4>
                    <textarea cols="2" class="form-control" data-name="bp_strategy"></textarea>
                </div>

                <div class="hidden prefer-content" id="offer-div">
                    <h4>Initial Offer</h4>
                    <textarea cols="2" class="form-control" data-name="bp_initial"></textarea>
                    <h4>Counter Offer</h4>
                    <textarea cols="2" class="form-control" data-name="bp_counter"></textarea>
                    <h4>Second Offer</h4>
                    <textarea cols="2" class="form-control" data-name="bp_second"></textarea>
                    <h4>Discoveries</h4>
                    <textarea cols="2" class="form-control" data-name="bp_discovery"></textarea>
                    <h4>Final Offer</h4>
                    <textarea cols="2" class="form-control" data-name="bp_final"></textarea>
                    <h4>Retrade</h4>
                    <textarea cols="2" class="form-control" data-name="bp_retrade"></textarea>
                    <h4>Justifications</h4>
                    <textarea cols="2" class="form-control" data-name="bp_justification"></textarea>
                    <h4>Extensions</h4>
                    <textarea cols="2" class="form-control" data-name="bp_extension"></textarea>
                    <h4>Reasons</h4>
                    <textarea cols="2" class="form-control" data-name="bp_reason"></textarea>
                    <h4>Hard Money</h4>
                    <textarea cols="2" class="form-control" data-name="bp_hard"></textarea>
                    <h4>Additional Funds</h4>
                    <textarea cols="2" class="form-control" data-name="bp_additional"></textarea>
                    <h4>Accepted Price</h4>
                    <textarea cols="2" class="form-control" data-name="bp_accepted"></textarea>
                </div>

                <div class="hidden prefer-content" id="provision-div">
                    <h4>Owner Financing</h4>
                    <textarea cols="2" class="form-control" data-name="bp_owner"></textarea>
                    <h4>Unit Renovations</h4>
                    <textarea cols="2" class="form-control" data-name="bp_unit_renovation"></textarea>
                    <h4>External Renovations</h4>
                    <textarea cols="2" class="form-control" data-name="bp_external"></textarea>
                    <h4>Cost Coverage</h4>
                    <textarea cols="2" class="form-control" data-name="bp_cost"></textarea>
                    <h4>Property Management</h4>
                    <textarea cols="2" class="form-control" data-name="bp_property"></textarea>
                    <h4>Inspection Reimbursement</h4>
                    <textarea cols="2" class="form-control" data-name="bp_inspection"></textarea>
                    <h4>Appraisal Adjustments</h4>
                    <textarea cols="2" class="form-control" data-name="bp_appraisal"></textarea>
                    <h4>Environmental Adjustments</h4>
                    <textarea cols="2" class="form-control" data-name="bp_environmental"></textarea>
                    <h4>Title Adjustments</h4>
                    <textarea cols="2" class="form-control" data-name="bp_title"></textarea>
                    <h4>Tenant Arrangements</h4>
                    <textarea cols="2" class="form-control" data-name="bp_tenant_arrangement"></textarea>
                    <h4>Tenant Information</h4>
                    <textarea cols="2" class="form-control" data-name="bp_tenant_information"></textarea>
                    <h4>Legal Procedure</h4>
                    <textarea cols="2" class="form-control" data-name="bp_legal_procedure"></textarea>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo $this->basepath; ?>resources/app/js/cpdv1/dialog/view_buyer_preference.js"></script>
