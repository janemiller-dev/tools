<!-- Display Client Information.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/minified_cpdv1/client_info.css"/>

<div class="modal fade" id="client-info-dialog" tabindex="10" role="dialog" aria-labelledby="client-info-label"
     aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="client-info-title" class="modal-title"></h2>
            </div>
            <!--// Modal Header -->

            <div class="modal-body row">
                <div class="col-xs-12" id="owner-info-row">
                    <div class="col-xs-12">
                        <!-- User Info -->
                        <div class="col-xs-5">
                            <div class="col-xs-2 hidden" id="client-image-div">
                                <img src="<?php echo $this->basepath; ?>resources/app/media/user-avatar.svg"
                                     id="info-avatar" style="max-width: 6.5vw">
                                <div class="upload">
                                    <input id="info-img-upload" type="file" name="files[]"
                                           data-url= <?= "https://" . $_SERVER['SERVER_NAME'] . "/tools/cpdv1/upload_info_image" ?> style="visibility:
                                           hidden; float:left; width:0;">
                                    <a id="upload-info-img">Change</a>
                                </div>
                            </div>
                            <div class="col-xs-2 placeholder-div">
                                <div class="rectangle text-center"
                                     style="width: 6.5vw; height: 8vw; background-color: #eee; border: solid 1px">
                                    <p style="font-size: 20px; margin-top: 2.5vw">Upload Image</p>
                                </div>
                            </div>
                            <div class="col-xs-5 placeholder-div" id="name-placeholder-div" style="padding-left: 40px">
                                <p style="font-size: 30px">Client Name</p>
                                <p style="font-size: 20px"><b>Title</b></p>
                                <p style="font-size: 18px">Entity</p>
                                <p>Entity Address</p>
                            </div>
                            <div class="col-xs-5 placeholder-div">
                                <p>Entity Office Phone</p>
                                <p>Client Cell Phone</p>
                                <p>Client Email Address</p>
                                <p>Entity Website</p>
                                <p>Entity Messaging</p>
                                <p>Entity Social Media</p>
                            </div>

                            <div class="col-xs-5 hidden" id="client-info" style="padding-left: 40px">
                            </div>
                            <div class="col-xs-5 hidden" id="client-contact">
                            </div>
                        </div>
                        <!--// User Info -->

                        <!-- Drop Down boxes-->
                        <div class="col-xs-7">
                            <!-- Partner Drop down. -->
                            <div class="col-xs-6">
                                <label class="info-label">Partners</label>
                                <select class="form-control info-select" id="info-Partner" data-name="Partner">
                                </select>
                            </div>
                            <!--// Partner Drop down. -->

                            <!-- Staff Drop down. -->
                            <div class="col-xs-6">
                                <label class="info-label">Staff</label>
                                <select class="form-control info-select" id="info-Staff" data-name="Staff">
                                </select>
                            </div>
                            <!-- // Staff Drop down. -->

                            <!-- Advisor Drop down. -->
                            <div class="col-xs-6">
                                <label class="info-label" id="info-advisor-label">Advisors</label>
                                <select class="form-control info-select" id="info-Advisor" data-name="Advisor">
                                </select>
                            </div>
                            <!-- // Advisor Drop down. -->

                            <!-- Vendor Drop down. -->
                            <div class="col-xs-6">
                                <label class="info-label" id="info-contractor-label">Vendors</label>
                                <select class="form-control info-select" id="info-Buyer" data-name="Buyer">
                                </select>
                            </div>
                            <!-- // Vendor Drop down. -->
                        </div>
                        <!--// Drop Down boxes-->
                    </div>

                    <div class="col-xs-12" id="owner-profile-buttons-div" style="margin: 35px 0 35px 0">
                        <div class="col-xs-2 text-center">
                            <button id="owner-project" data-toggle="popover" data-type="project"
                                    data-html="true" data-placement="bottom"
                                    class="btn btn-sm btn-default profile-btn"
                                    data-title="<h3>Buyer Project</h3>"
                                    data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>History:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info' data-type='history'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Facts:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                owner-popup-info' data-type='facts'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Accomplished:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                owner-popup-info'
                                                                                data-type='accomplished'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Problems:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                owner-popup-info' data-type='problem'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Project:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                owner-popup-info' data-type='project'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                        </div>">
                                <span class="glyphicon glyphicon-list-alt"></span>
                            </button>
                            <label>Buyer Project</label>
                        </div>

                        <div class="col-xs-2 text-center">
                            <button id="owner-challenge" data-type="challenge"
                                    data-toggle="popover" data-type="project"
                                    class="btn btn-sm btn-default profile-btn"
                                    data-html="true" data-placement="bottom"
                                    data-title="<h3>Buyer Challenges</h3>"
                                    data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Asset Challenges:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                owner-popup-info'
                                                                                data-type='asset-challenge'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>People Challenges:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                owner-popup-info'
                                                                                data-type='people-challenge'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Financial Challenges:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                owner-popup-info'
                                                                                data-type='financial-challenge'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Legal Challenges:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                owner-popup-info'
                                                                                data-type='legal-challenge'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Market Challenges:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                owner-popup-info'
                                                                                data-type='market-challenge'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                        </div>">
                                <span class="glyphicon glyphicon-list-alt"></span>
                            </button>
                            <label>Buyer Challenges</label>
                        </div>

                        <div class="col-xs-2 text-center">
                            <button id="strategic-objective"
                                    data-type="objective" data-toggle="popover"
                                    class="btn btn-sm btn-default profile-btn"
                                    data-html="true" data-placement="bottom"
                                    data-title="<h3>Buyer Strategy</h3>"
                                    data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Strategic Objectives:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                owner-popup-info'
                                                                                 data-type='strategic-objective'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Disposition Objectives:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='disposition-objective'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Acquisition Objectives:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='acquisition-objective'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Exit Strategy:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='exit-strategy'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Beneficiaries:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='beneficiaries'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                        </div>">
                                <span class="glyphicon glyphicon-list-alt"></span>
                            </button>
                            <label>Buyer Strategy</label>
                        </div>

                        <div class="col-xs-2 text-center">
                            <button id="transaction-history" data-toggle="popover"
                                    data-html="true" data-placement="bottom" data-type="history"
                                    data-title="<h3>Buyer Preferences</h3>"
                                    class="btn btn-sm btn-default profile-btn"
                                    data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Disposition History:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='disposition-history'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Acquisition History:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='acquisition-history'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Partnership History:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='partnership-history'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Foreclosure History:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='foreclosure-history'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Bankruptcy History:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='bankruptcy-history'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                        </div>">
                                <span class="glyphicon glyphicon-list-alt"></span>
                            </button>
                            <label>Buyer Preferences</label>
                        </div>

                        <div class="col-xs-2 text-center">
                            <button id="acquistion-criteria" data-toggle="popover"
                                    data-html="true" data-placement="bottom" data-type="history"
                                    data-title="<h3>Buyer History</h3>"
                                    class="btn btn-sm btn-default profile-btn"
                                    data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Locations:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='locations'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Product Types:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='product-type'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Asset Specifications:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='asset-specifications'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Financing Methods:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='financing-method'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Offer Stipulations:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='offer-stipulations'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                        </div>">
                                <span class="glyphicon glyphicon-list-alt"></span>
                            </button>
                            <label>Buyer History</label>
                        </div>

                        <div class="col-xs-2 text-center">
                            <button id="owner-project" data-toggle="popover"
                                    data-html="true" data-placement="bottom" data-type="history"
                                    data-title="<h3>Other Research</h3>"
                                    class="btn btn-sm btn-default profile-btn"
                                    data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Websites:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='website'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Media Exposure:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='media-exposure'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Social Media Profiles:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='social-media-profile'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>People’s Comments:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='people-comment'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Public Records:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 owner-popup-info'
                                                                                 data-type='public-record'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                        </div>">
                                <span class="glyphicon glyphicon-list-alt"></span>
                            </button>
                            <label>Other Research</label>
                        </div>
                    </div>


                    <!--Client Buttons-->
                    <div class="col-xs-12 hidden" id="owner-buttons-div" style="margin: 35px 0 35px 0">
                        <div class="col-xs-2 text-center">
                            <button type="button" id="owner-project" data-toggle="popover" data-type="project"
                                    data-html="true" data-placement="bottom" data-container="#client-info-dialog"
                                    class="btn btn-sm btn-default client-info-btn has-popover"
                                    data-title="<h3>Client Project</h3>"
                                    data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>History:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info' data-type='history'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Facts:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                client-popup-info' data-type='facts'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Accomplished:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                client-popup-info'
                                                                                data-type='accomplished'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Problems:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                client-popup-info' data-type='problem's
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Project:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                client-popup-info' data-type='project'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                        </div>">
                                <span class="glyphicon glyphicon-list-alt"></span>
                            </button>
                            <label>Client Project</label>
                        </div>

                        <div class="col-xs-2 text-center">
                            <button type="button" id="owner-challenge" data-type="challenge"
                                    data-toggle="popover"
                                    class="btn btn-sm btn-default client-info-btn has-popover"
                                    data-html="true" data-placement="bottom" data-container="#client-info-dialog"
                                    data-title="<h3>Client Challenges</h3>"
                                    data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Asset Challenges:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                client-popup-info'
                                                                                data-type='asset-challenge'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>People Challenges:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                client-popup-info'
                                                                                data-type='people-challenge'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Financial Challenges:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                client-popup-info'
                                                                                data-type='financial-challenge'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Legal Challenges:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                client-popup-info'
                                                                                data-type='legal-challenge'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Market Challenges:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                client-popup-info'
                                                                                data-type='market-challenge'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                        </div>">
                                <span class="glyphicon glyphicon-list-alt"></span>
                            </button>
                            <label>Client Challenges</label>
                        </div>

                        <div class="col-xs-2 text-center">
                            <button type="button" id="strategic-objective"
                                    data-type="objective" data-toggle="popover"
                                    class="btn btn-sm btn-default client-info-btn has-popover"
                                    data-html="true" data-placement="bottom" data-container="#client-info-dialog"
                                    data-title="<h3>Strategic Objectives</h3>"
                                    data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Strategic Objectives:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                client-popup-info'
                                                                                 data-type='strategic-objective'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Disposition Objectives:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='disposition-objective'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Acquisition Objectives:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='acquisition-objective'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Exit Strategy:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='exit-strategy'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Beneficiaries:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='beneficiaries'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                        </div>">
                                <span class="glyphicon glyphicon-list-alt"></span>
                            </button>
                            <label>Strategic Objectives</label>
                        </div>

                        <div class="col-xs-2 text-center">
                            <button type="button" id="transaction-history" data-toggle="popover"
                                    data-html="true" data-placement="bottom" data-type="history"
                                    data-title="<h3>Transaction History</h3>" data-container="#client-info-dialog"
                                    class="btn btn-sm btn-default client-info-btn has-popover"
                                    data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Disposition History:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='disposition-history'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Acquisition History:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='acquisition-history'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Partnership History:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='partnership-history'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Foreclosure History:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='foreclosure-history'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Bankruptcy History:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='bankruptcy-history'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                        </div>">
                                <span class="glyphicon glyphicon-list-alt"></span>
                            </button>
                            <label>Transaction History</label>
                        </div>

                        <div class="col-xs-2 text-center">
                            <button type="button" id="acquistion-criteria" data-toggle="popover"
                                    data-html="true" data-placement="bottom" data-type="history"
                                    data-title="<h3>Acquisition Criteria</h3>" data-container="#client-info-dialog"
                                    class="btn btn-sm btn-default client-info-btn has-popover"
                                    data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Locations:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='locations'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Product Types:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='product-type'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Asset Specifications:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='asset-specifications'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Financing Methods:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='financing-method'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Offer Stipulations:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='offer-stipulations'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                        </div>">
                                <span class="glyphicon glyphicon-list-alt"></span>
                            </button>
                            <label>Acquisition Criteria</label>
                        </div>

                        <div class="col-xs-2 text-center">
                            <button type="button" id="owner-project" data-toggle="popover"
                                    data-html="true" data-placement="bottom" data-type="history"
                                    data-title="<h3>Other Research</h3>" data-container="#client-info-dialog"
                                    class="btn btn-sm btn-default client-info-btn has-popover"
                                    data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Websites:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='website'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Media Exposure:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='media-exposure'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Social Media Profiles:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='social-media-profile'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>People’s Comments:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='people-comment'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Public Records:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='public-record'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                        </div>">
                                <span class="glyphicon glyphicon-list-alt"></span>
                            </button>
                            <label>Other Research</label>
                        </div>
                    </div>
                    <!--// Client Buttons-->

                    <!--Buyer Buttons-->
                    <div class="col-xs-12 hidden" id="buyer-buttons-div" style="margin: 35px 0 35px 0">
                        <div class="col-xs-2 text-center">
                            <button type="button" id="buyer-project" data-toggle="popover" data-type="project"
                                    data-html="true" data-placement="bottom" data-container="#client-info-dialog"
                                    class="btn btn-sm btn-default client-info-btn has-popover"
                                    data-title="<h3>Acquisition History</h3>"
                                    data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Recent Acquisitions::</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='recent-acquisitions'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Previous Acquisitions::</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                client-popup-info'
                                                                                data-type='prev-acquisitions'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Disposition History::</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                client-popup-info'
                                                                                data-type='disposition-history'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Planned Acquisitions::</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                client-popup-info'
                                                                                data-type='planned-acquisition'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Future Acquisitions:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                client-popup-info'
                                                                                data-type='future-acquisition'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                        </div>">
                                <span class="glyphicon glyphicon-list-alt"></span>
                            </button>
                            <label>Acquisition History</label>
                        </div>

                        <div class="col-xs-2 text-center">
                            <button type="button" id="buyer-challenge" data-type="challenge"
                                    data-toggle="popover"
                                    class="btn btn-sm btn-default client-info-btn has-popover"
                                    data-html="true" data-placement="bottom" data-container="#client-info-dialog"
                                    data-title="<h3>Acquisition Criteria</h3>"
                                    data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Locations:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                client-popup-info'
                                                                                data-type='buyer-locations'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Product Types:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                client-popup-info'
                                                                                data-type='buyer-product-type'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Specifications:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                client-popup-info'
                                                                                data-type='buyer-specs'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Financing:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                client-popup-info'
                                                                                data-type='buyer-financing'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Stipulations:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                client-popup-info'
                                                                                data-type='buyer-stipulations'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                        </div>">
                                <span class="glyphicon glyphicon-list-alt"></span>
                            </button>
                            <label>Acquisition Criteria</label>
                        </div>

                        <div class="col-xs-2 text-center">
                            <button type="button" id="strategic-objective"
                                    data-type="objective" data-toggle="popover"
                                    class="btn btn-sm btn-default client-info-btn has-popover"
                                    data-html="true" data-placement="bottom" data-container="#client-info-dialog"
                                    data-title="<h3>Buyer Capability</h3>"
                                    data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Current Portfolio Size & Make-Up:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                client-popup-info'
                                                                                 data-type='buyer-portfolio-size
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Current Portfolio – Total Dollar Value:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='buyer-portfolio-dollar'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Current Product Type(s):</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='buyer-portfolio-current-product'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Deployable Equity Dollar Amount:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='buyer-portfolio-deployable'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Acquisition Plans & Target Date:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='buyer-acquisition-plan'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                        </div>">
                                <span class="glyphicon glyphicon-list-alt"></span>
                            </button>
                            <label>Buyer Capability</label>
                        </div>

                        <div class="col-xs-2 text-center">
                            <button type="button" id="transaction-history" data-toggle="popover"
                                    data-html="true" data-placement="bottom" data-type="history"
                                    data-title="<h3>Buyer Project</h3>" data-container="#client-info-dialog"
                                    class="btn btn-sm btn-default client-info-btn has-popover"
                                    data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Buyer History:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='buyer-history'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Current Facts:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='buyer-current-facts'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Accomplishments::</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='buyer-accomplishment'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Acquisition Needs:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='buyer-acquisition-need'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Acquisition Project:</b>
                                                                                </label>
                                                                                <textarea class='form-control
                                                                                 client-popup-info'
                                                                                 data-type='buyer-acquisition-project'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                            </div>
                                                                        </div>">
                                <span class="glyphicon glyphicon-list-alt"></span>
                            </button>
                            <label>Buyer Project</label>
                        </div>

                        <div class="col-xs-2 text-center">
                            <button type="button" id="acquistion-criteria" data-toggle="popover"
                                    data-html="true" data-placement="bottom" data-type="history"
                                    data-title="<h3>Buyer Concerns</h3>" data-container="#client-info-dialog"
                                    class="btn btn-sm btn-default client-info-btn has-popover"
                                    data-content="
                                    <div>
                                        <div>
                                            <label>
                                                <b>Assets Concerns::</b>
                                            </label>
                                            <textarea class='form-control
                                             client-popup-info'
                                             data-type='asset-concerns'
                                            data-container='body' rows='3'
                                            ></textarea>
                                        </div>
                                        <div>
                                            <label>
                                                <b>People Concerns:</b>
                                            </label>
                                            <textarea class='form-control
                                             client-popup-info'
                                             data-type='people-concern'
                                            data-container='body' rows='3'
                                            ></textarea>
                                        </div>
                                        <div>
                                            <label>
                                                <b>Financial Concerns:</b>
                                            </label>
                                            <textarea class='form-control
                                             client-popup-info'
                                             data-type='financial-concern'
                                            data-container='body' rows='3'
                                            ></textarea>
                                        </div>
                                        <div>
                                            <label>
                                                <b>Legal Concerns:</b>
                                            </label>
                                            <textarea class='form-control
                                             client-popup-info'
                                             data-type='legal-concern'
                                            data-container='body' rows='3'
                                            ></textarea>
                                        </div>
                                        <div>
                                            <label>
                                                <b>Market Concerns:</b>
                                            </label>
                                            <textarea class='form-control
                                             client-popup-info'
                                             data-type='market-concern'
                                            data-container='body' rows='3'
                                            ></textarea>
                                        </div>
                                    </div>">
                                <span class="glyphicon glyphicon-list-alt"></span>
                            </button>
                            <label>Buyer Concerns</label>
                        </div>

                        <div class="col-xs-2 text-center">
                            <button type="button" id="buyer-project" data-toggle="popover"
                                    data-html="true" data-placement="bottom" data-type="history"
                                    data-title="<h3>Buyer Research</h3>" data-container="#client-info-dialog"
                                    class="btn btn-sm btn-default client-info-btn has-popover"
                                    data-content=
                                    "<div>
                                        <div>
                                            <label>
                                                <b>Websites:</b>
                                            </label>
                                            <textarea class='form-control
                                             client-popup-info'
                                             data-type='buyer-website'
                                            data-container='body' rows='3'
                                            ></textarea>
                                        </div>
                                        <div>
                                            <label>
                                                <b>Media Exposure:</b>
                                            </label>
                                            <textarea class='form-control
                                             client-popup-info'
                                             data-type='buyer-media-exposure'
                                            data-container='body' rows='3'
                                            ></textarea>
                                        </div>
                                        <div>
                                            <label>
                                                <b>Social Media Profiles:</b>
                                            </label>
                                            <textarea class='form-control
                                             client-popup-info'
                                             data-type='buyer-social-media-profile'
                                            data-container='body' rows='3'
                                            ></textarea>
                                        </div>
                                        <div>
                                            <label>
                                                <b>People’s Comments:</b>
                                            </label>
                                            <textarea class='form-control
                                             client-popup-info'
                                             data-type='buyer-people-comment'
                                            data-container='body' rows='3'
                                            ></textarea>
                                        </div>
                                        <div>
                                            <label>
                                                <b>Public Records:</b>
                                            </label>
                                            <textarea class='form-control
                                             client-popup-info'
                                             data-type='buyer-public-record'
                                            data-container='body' rows='3'
                                            ></textarea>
                                        </div>
                                    </div>">
                                <span class="glyphicon glyphicon-list-alt"></span>
                            </button>
                            <label>Buyer Research</label>
                        </div>
                    </div>
                    <!--// Buyer Buttons-->

                    <div class="col-xs-12 table-responsive hidden" id="owner-info-div">
                        <table>
                            <tr>
                                <td id="owner-info-asset-table"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-xs-12">
                        <div id="owner-info-buyer-div"></div>
                    </div>
                    <div class="col-xs-12 hidden" id="agent-info-row" style="border: solid 1px #eee; padding: 10px;">
                        <div class="col-xs-12">
                            <div class="col-xs-2">
                                <h4>Staff Job Title</h4>
                            </div>
                            <div class="col-xs-10">
                                <input type="text" class="form-control job-field" id="job-title"
                                       data-col="cc_job_title">
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="col-xs-2">
                                <h4>Staff Job Description</h4>
                            </div>
                            <div class="col-xs-10">
                                <textarea cols="2" style="width: 100%" class="form-control job-field"
                                          id="job-desc" data-col="cc_job_desc"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12" id="notes-div">
                        <div class="col-xs-1">
                            <h4>Notes:</h4>
                        </div>
                        <div class="col-xs-11">
                            <div class="input-group">
                                <span class="input-group-addon" id="note-date">Date: </span>
                                <textarea class="form-control" id="owner-info-notes"></textarea>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <div class="hidden" id="buyer-offer-div">
                    <h5 id="buyer-name" style="font-weight: bold"></h5>
                    <table class="table" style="border: 1px solid #eee">
                        <thead>
                        <tr>
                            <th class="text-center">Offer</th>
                            <th class="text-center">CA</th>
                            <th class="text-center">Tour Date</th>
                            <th class="text-center">Offer Contingencies</th>
                            <th class="text-center">Proof of Funds</th>
                            <th class="text-center">Financing Method</th>
                            <th class="text-center">Terms of Purchase</th>
                            <th class="text-center">Fee Arrangement</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <input class="form-control" id="offer-text" data-col="hbi_offer"/>
                            </td>
                            <td>
                                <input class="form-control" id="ca-text" data-col="hbi_ca"/>
                            </td>
                            <td>
                                <input class="form-control" id="tour-date-text" data-col="hbi_tour_date"/>
                            </td>
                            <td>
                                <input class="form-control" id="offer-contingencies-text"
                                       data-col="hbi_offer_contingencies"/>
                            </td>
                            <td>
                                <input class="form-control" id="funds-proof-text" data-col="hbi_proof"/>
                            </td>
                            <td>
                                <input class="form-control" id="financing-method-text" data-col="hbi_financing"/>
                            </td>
                            <td>
                                <input class="form-control" id="purchase-term-text" data-col="hbi_terms"/>
                            </td>
                            <td>
                                <input class="form-control" id="fee-arrangement-text" data-col="hbi_fee"/>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <button type="button" class="btn btn-primary hidden add-info-offer" data-type="offer"
                        id="add-buyer-offer">Add Offer
                </button>
                <button type="button" class="btn btn-primary hidden add-info-offer" data-type="counter"
                        id="add-counter-offer">Add
                    Counter Offer
                </button>
                <button type="button" class="btn btn-primary hidden add-info-offer" data-type="retrade"
                        id="add-retrade-offer">Add
                    Retrade
                </button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php $this->partial('views/cpdv1/dialogs/add_client_info.php'); ?>
<?php $this->partial('views/dialogs/send_email.php'); ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/cpdv1/dialog/client_info.js"
        xmlns="http://www.w3.org/1999/html"></script>

