<!-- Add Client Info/reference Instance.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="add-client-info-dialog" tabindex="10" role="dialog" aria-labelledby="add-client-info-label"
     aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <form id="add-client-info-form" class="form-horizontal">

                <!-- Modal Header section-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-client-info-title" class="modal-title" style="text-transform: uppercase;">Add Info</h2>
                </div>
                <!--// Modal Header section-->

                <!-- Modal Body Section-->
                <div class="modal-body" style="max-height:70vh; overflow-y: scroll">
                    <!-- Name Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="name">Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="name" name="name"/>
                            <p class="form-text text-muted">Enter Name.</p>
                        </div>
                    </div>
                    <!--// Name Field -->

                    <!-- Title Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="title">Title</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="title" name="title"/>
                            <p class="form-text text-muted">Enter Title.</p>
                        </div>
                    </div>
                    <!--// Title Field -->

                    <!-- Entity Name Field-->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="entity-name">Entity Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="entity-name" name="entity_name"/>
                            <p class="form-text text-muted">Enter Entity Name.</p>
                        </div>
                    </div>
                    <!--// Entity Name Field-->

                    <!-- Address Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="address">Address</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="address" name="address"/>
                            <p class="form-text text-muted">Enter Address</p>
                        </div>
                    </div>
                    <!-- // Address Field -->

                    <!-- Office Phone Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="office-phone">Office Phone</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="office-phone" name="office_phone"/>
                            <p class="form-text text-muted">Enter Office Phone</p>
                        </div>
                    </div>
                    <!--// Office Phone Field -->

                    <!-- Cell Phone Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="cell-phone">Cell Phone</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="cell-phone" name="cell_phone"/>
                            <p class="form-text text-muted">Enter Cell Phone.</p>
                        </div>
                    </div>
                    <!-- // Cell Phone Field -->

                    <!-- Email Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="email">Email</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="email" class="form-control" id="email" name="email"/>
                            <p class="form-text text-muted">Enter Email.</p>
                        </div>
                    </div>
                    <!-- // Email Field -->

                    <!-- Website Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="website">Website</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="website" name="website"/>
                            <p class="form-text text-muted">Enter Website</p>
                        </div>
                    </div>
                    <!-- // Website Field -->

                    <!-- Messaging Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="messaging">Messaging</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="messaging" name="messaging"/>
                            <p class="form-text text-muted">Enter Messaging</p>
                        </div>
                    </div>
                    <!-- // Messaging Field -->

                    <!-- Social Media Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="social-media">Social Media</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="social-media" name="social_media"/>
                            <p class="form-text text-muted">Enter Social Media</p>
                        </div>
                    </div>
                    <!-- // Social Media Field -->

                    <input type="hidden" name="client_id" id="client-id"/>
                    <input type="hidden" name="relation_type" id="relation-type"/>
                    <input type="hidden" name="info_type" id="info-type"/>
                    <input type="hidden" name="is_buyer" id="is-buyer"/>
                    <input type="hidden" name="is_owner" id="is-owner"/>

                </div>
                <!--// Modal Body Section-->

                <!-- Modal Footer Section-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" id="save-client-info" class="btn btn-primary">Save</button>
                </div>
                <!--// Modal Footer Section-->
            </form>
        </div>
    </div>
</div>


<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/cpdv1/dialog/add_client_info.js"></script>
