<!-- View Collab dialog view.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="view-comps-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="message-label" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">

            <!-- Modal Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="view-comps-title" class="modal-title">Comps Dashboard</h2>
            </div>
            <!--// Modal Header-->

            <div class="modal-body">

                <!-- Show the list of comps. -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="row" id="comps-dialog-row">
                            <div class="col-xs-12">
                                <div id="comps-div" class="top-buffer">
                                    <div style="margin: 10px">
                                        <div class="col-xs-12" style="text-align: right">
                                            <button class="btn btn-primary hidden" id="enable-poi">Show POI</button>
                                            <button class="btn btn-primary hidden" id="disable-poi">Hide POI
                                            </button>
                                            <button class="btn btn-primary" id="owner-comps-pics">DEALS</button>
                                            <button class="btn btn-primary" id="owner-comps-map">MAP</button>
                                            <button class="btn btn-primary" id="owner-comps-info">INFO</button>
                                        </div>
                                    </div>
                                    <div class="col-xs-12" id="comps-deals-div">
                                        <table id="comps-table" class="table"
                                               style="border-collapse: separate; border-spacing: 0 0.5em;">
                                        </table>
                                    </div>

                                    <div class="col-xs-12 hidden" id="comps-map-div"
                                         style="border: 5px solid #A4A4A4; height: 70vh; padding: 10px">
                                    </div>

                                    <div class="col-xs-12 hidden container" id="comps-info-div"
                                         style="border: 5px solid #A4A4A4; height: 70vh; padding: 10px">
                                        <h3 style="text-align: center; padding: 5px"><b>Property Overview</b></h3>
                                        <div class="col-xs-4 col-xs-offset-4 ">
                                            <div>
                                                <h4 class="col-xs-5">Property Type</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-comps-input"
                                                           data-col="ac_prop_type"/>
                                                </div>
                                            </div>
                                            <div>
                                                <h4 class="col-xs-5">Property Name</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-comps-input"
                                                           data-col="ac_prop_name"/>
                                                </div>
                                            </div>
                                            <h4 class="col-xs-5">Region/MSA</h4>
                                            <div class="col-xs-7" style="padding: 5px;">
                                                <input type="text" class="form-control agent-comps-input"
                                                       data-col="ac_region"/>
                                            </div>
                                            <div>
                                                <h4 class="col-xs-5">Address</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-comps-input"
                                                           data-col="ac_address"/>
                                                </div>
                                            </div>
                                            <div>
                                                <h4 class="col-xs-5">City</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-comps-input"
                                                           data-col="ac_city"/>
                                                </div>
                                            </div>
                                            <h4 class="col-xs-5">State</h4>
                                            <div class="col-xs-7" style="padding: 5px;">
                                                <input type="text" class="form-control agent-comps-input"
                                                       data-col="ac_state"/>
                                            </div>
                                            <h4 class="col-xs-5">Zip</h4>
                                            <div class="col-xs-7" style="padding: 5px;">
                                                <input type="text" class="form-control agent-comps-input"
                                                       data-col="ac_zip"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="add-comps">Add</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo $this->basepath; ?>resources/app/js/cpdv1/dialog/view_comps.js"></script>
