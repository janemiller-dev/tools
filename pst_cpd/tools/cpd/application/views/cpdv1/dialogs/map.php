<!-- Google Map dialog view.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="map-dialog" tabindex="-1" role="dialog" aria-labelledby="map-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div id="map-header" class="text-center row">

                    <!-- Origin text field-->
                    <div class="col-xs-4">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Start" id="map-start">
                            <span class="input-group-btn">
                                <span class="has-tooltip" title="Use Current Location" data-placement="top">
                                    <button class="btn btn-default" type="button" id="map-current-location">
                                        <i class="fa fa-map-marker"></i>
                                    </button>
                                </span>
                            </span>
                        </div>
                    </div>
                    <!-- // Origin text field-->

                    <!-- Destination text field-->
                    <div class="col-xs-4">
                        <input type="text" class="form-control" id="map-end" placeholder="Destination"/>
                    </div>
                    <!-- // Destination text field-->

                    <!-- Mode Select Box-->
                    <div class="col-xs-4">
                        <span><b>Mode: </b></span>
                        <label>
                            <select id="map-mode" class="form-control">
                                <option value="DRIVING">Driving</option>
                                <option value="WALKING">Walking</option>
                                <option value="BICYCLING">Bicycling</option>
                                <option value="TRANSIT">Transit</option>
                            </select>
                        </label>
                    </div>
                    <!--// Mode Select Box-->

                </div>
                <div id="map" style="width:100%;height:400px;">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="close-delete-update-dialog">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Load Google Maps Script-->
<!--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBCwpF5xPvzTUNhXuKc-SKw655Pdty5qWs" async defer></script>-->
<script src="<?php echo $this->basepath; ?>resources/app/js/minified_cpdv1/dialog/map.js"></script>


