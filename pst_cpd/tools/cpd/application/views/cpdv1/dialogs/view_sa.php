<!-- View SA dialog view.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="view-sa-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="message-label" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="view-sa-title" class="modal-title">Current Strategic Analysis</h2>
            </div>
            <div class="modal-body" style="height: 75vh; overflow-y: scroll">

                <!-- Show the list of sa. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- List SA -->
                        <div class="row" id="sa-row">
                            <div class="col-xs-12">
                                <div id="sa-div">

                                    <!-- Factors Div-->
                                    <div class="col-xs-12" id="factors-div">
                                        <div>
                                            <h4>
                                                <b>Factor 1: <span class=""
                                                                   data-kind="factor" data-seq="1"> Name </span>
                                                </b>
                                                <i>
                                                    (Write a sentence or two on the challenge and implications of this
                                                    factor.)</i>
                                            </h4>
                                            <textarea cols="2" class="form-control sa-field"
                                                      data-type="factor" data-seq="1"></textarea>
                                        </div>
                                        <div>
                                            <h4>
                                                <b>Factor 2: <span class=""
                                                                   data-kind="factor" data-seq="2"> Name </span>
                                                </b><i>
                                                    (Write a sentence or two on the challenge and implications of this
                                                    factor.)</i>
                                            </h4>
                                            <textarea cols="2" class="form-control sa-field"
                                                      data-type="factor" data-seq="2"></textarea>
                                        </div>
                                        <div>
                                            <h4>
                                                <b>Factor 3: <span class=""
                                                                   data-kind="factor" data-seq="3"> Name </span>
                                                </b><i>
                                                    (Write a sentence or two on the challenge and implications of this
                                                    factor.)</i></h4>
                                            <textarea cols="2" class="form-control sa-field"
                                                      data-type="factor" data-seq="3"></textarea>
                                        </div>

                                        <div class="hidden" id="dummy_factor">
                                            <h4>
                                                <b>Factor 2: <span class="" data-kind="factor"> Name </span>
                                                </b><i>
                                                    (Write a sentence or two on the challenge and implications of this
                                                    factor.)</i></h4>
                                            <textarea cols="2" class="form-control sa-field"
                                                      data-type="factor"></textarea>
                                        </div>
                                        <button class="btn btn-default add-sa-info" data-type="factor">( Add Factor )
                                        </button>
                                    </div>
                                    <!--// Factors Div-->

                                    <!-- Options Div-->
                                    <div class="col-xs-12">
                                        <div>
                                            <h4>
                                                <b>Option 1: <span class=""
                                                                   data-kind="option" data-seq="1"> Name </span>
                                                </b><i>(Write a sentence or two describing the consequences of this
                                                    option.)</i></h4>
                                            <textarea cols="2" class="form-control sa-field"
                                                      data-type="option" data-seq="1"></textarea>
                                        </div>
                                        <div>
                                            <h4>
                                                <b>Option 2: <span class=""
                                                                   data-kind="option" data-seq="2"> Name </span>
                                                </b><i>(Write a sentence or two describing the consequences of this
                                                    option.)</i></h4>
                                            <textarea cols="2" class="form-control sa-field"
                                                      data-type="option" data-seq="2"></textarea>
                                        </div>
                                        <div>
                                            <h4>
                                                <b>Option 3: <span class=""
                                                                   data-kind="option" data-seq="3"> Name </span>
                                                </b><i>(Write a sentence or two describing the consequences of this
                                                    option.)</i></h4>
                                            <textarea cols="2" class="form-control sa-field"
                                                      data-type="option" data-seq="3"></textarea>
                                        </div>

                                        <div class="hidden" id="dummy_option">
                                            <h4>
                                                <b>
                                                    Option 2: <span class=""> Name </span>
                                                </b><i>(Write a sentence or two describing the consequences of this
                                                    option.)</i>
                                            </h4>
                                            <textarea cols="2" class="form-control sa-field"
                                                      data-type="option"></textarea>
                                        </div>
                                        <button class="btn btn-default add-sa-info" data-type="option">( Add Option )
                                        </button>
                                    </div>
                                    <!--// Options Div-->

                                    <!--Project Description-->
                                    <div class="col-xs-12">
                                        <h4><b>Project Description</b> [Optional]</h4>

                                        <label>History</label>
                                        <textarea class="form-control sa-field" cols="2" id="project-description"
                                                  data-type="desc_history"></textarea>

                                        <label>Facts</label>
                                        <textarea class="form-control sa-field" cols="2" id="project-description"
                                                  data-type="desc_fact"></textarea>

                                        <label>Accomplished</label>
                                        <textarea class="form-control sa-field" cols="2" id="project-description"
                                                  data-type="desc_accomplished"></textarea>

                                        <label>Needed</label>
                                        <textarea class="form-control sa-field" cols="2" id="project-description"
                                                  data-type="desc_needed"></textarea>

                                        <label>Possible</label>
                                        <textarea class="form-control sa-field" cols="2" id="project-description"
                                                  data-type="desc_possible"></textarea>
                                    </div>
                                    <!--// Project Description-->

                                    <!-- Recommendation-->
                                    <div class="col-xs-12">
                                        <div>
                                            <h4>Recommendations:</h4>
                                            <h4><b>Option 1:<span class=""
                                                                          data-kind="recommendation"
                                                                          data-seq="1"> Name</span></b>
                                            </h4>
                                            <textarea class="form-control sa-field" cols="2"
                                                      data-type="recommendation"
                                                      id="recommendation" data-seq="1"></textarea>
                                            <h4>Project Impact</h4>
                                            <textarea class="form-control sa-field" cols="2"
                                                      data-type="recommendation-impact"
                                                      id="recommendation" data-seq="1"></textarea>
                                            <h4>Next Action</h4>
                                            <textarea class="form-control sa-field" cols="2"
                                                      data-type="recommendation-action"
                                                      id="recommendation" data-seq="1"></textarea>
                                        </div>
                                        <div class="hidden" id="dummy_recommendation">
                                            <h4>Recommendations:</h4>
                                            <h4><b>Option 1:<span class=""
                                                                  data-kind="recommendation"
                                                                  data-seq="1"> Name</span></b>
                                            </h4>
                                            <textarea class="form-control sa-field" cols="2"
                                                      data-type="recommendation"
                                                      id="recommendation" data-seq="1"></textarea>
                                            <h4>Project Impact</h4>
                                            <textarea class="form-control sa-field" cols="2"
                                                      data-type="recommendation-impact"
                                                      id="recommendation" data-seq="1"></textarea>
                                            <h4>Next Action</h4>
                                            <textarea class="form-control sa-field" cols="2"
                                                      data-type="recommendation-action"
                                                      id="recommendation" data-seq="1"></textarea>

                                        </div>
                                        <button class="btn btn-default add-sa-info" data-type="recommendation">( Add
                                            Recommendation )
                                        </button>
                                    </div>
                                    <!--// Recommendation-->
                                </div>
                            </div>
                        </div>
                        <!-- // List SA -->

                    </div>
                </div>
            </div>
            <div class="modal-footer">

                <!-- Partnership Declaration-->
                <!--                                    <div class="col-xs-12">-->
                <!--                                        <h4><b>Partnership Declaration</b> [Optional]</h4>-->
                <!---->
                <!--                                        <label>Risks</label>-->
                <!--                                        <textarea class="form-control sa-field" id="partnership-declaration"-->
                <!--                                                  data-type="decl_risk" cols="2"></textarea>-->
                <!---->
                <!--                                        <label>Responsibility</label>-->
                <!--                                        <textarea class="form-control sa-field" id="partnership-declaration"-->
                <!--                                                  data-type="decl_responsibility" cols="2"></textarea>-->
                <!---->
                <!--                                        <label>Declaration</label>-->
                <!--                                        <textarea class="form-control sa-field" id="partnership-declaration"-->
                <!--                                                  data-type="decl_decl" cols="2"></textarea>-->
                <!---->
                <!--                                        <label>Invitation</label>-->
                <!--                                        <textarea class="form-control sa-field" id="partnership-declaration"-->
                <!--                                                  data-type="decl_invitation" cols="2"></textarea>-->
                <!---->
                <!--                                        <label>Promise</label>-->
                <!--                                        <textarea class="form-control sa-field" id="partnership-declaration"-->
                <!--                                                  data-type="decl_prom" cols="2"></textarea>-->
                <!--                                    </div>-->
                <!--// Partnership Declaration-->
                <button type="button" class="btn btn-primary" id="view-sa-pdf">View</button>
                <button type="button" class="btn btn-primary" id="make-sa-pdf">Make PDF</button>
                <button type="button" class="btn btn-primary" id="send-sa-pdf">Send PDF</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/cpdv1/dialog/view_sa.js"></script>
