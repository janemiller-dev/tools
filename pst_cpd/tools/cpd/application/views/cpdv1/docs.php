<!-- Document Builder Page -->
<?php if (!defined('SUBVIEW')) {
    exit('No direct docs access allowed');
} ?>

<!-- Header Section -->
<div class="row docs_head">
    <div class="col-xs-12">
        <!-- Back Button-->
        <div class="col-xs-4">
            <a class="btn btn-secondary back-button" style="float: right; padding-right: 25px">
                <img src="http://cpd.powersellingtools.com/wp-content/uploads/2020/01/Component-Arrow-Back.png"
                     style="height: 5vh; width: 70%;" alt="back"></a>
        </div>
        <!--// Back Button-->

        <div class="col-xs-4">
            <h2 id="tsl-docs-heading" style="text-align:center" class="heading">Documents Library</h2>
        </div>

        <!-- Collaboration Button-->
        <div class="col-xs-4">
            <a class="btn btn-secondary collab-button" style="float: right; padding-right: 25px">
                <img src="http://cpd.powersellingtools.com/wp-content/uploads/2020/01/Collaboration-Arrows-2.png"
                     style="height: 5vh; width: 70%;" alt="collaboration">
            </a>
        </div>
        <!--// Collaboration Button-->
    </div>
    <div class="col-xs-12">

        <!-- Document Category Drop down-->
        <div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">
            <label class="col-xs-12" style="text-align: center">Document Category:</label>
            <select class="form-control" id="docs-category">
                <option value="sc">Sales Campaign</option>
                <option value="ac">Advisory Campaign</option>
                <option value="bc>Branding Campaign</option>
                <option value=" pfc
                ">Product Focus Campaign</option>
                <option value="mfc">Market Focus Campaign</option>
            </select>
        </div>
        <!--// Document Category Drop down-->

        <!-- Document type Drop down-->
        <div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">
            <label class="col-xs-12" style="text-align: center">Document Type</label>
            <select class="form-control col-xs-8" id="docs-type">
                <option value="ma">Market Analysis</option>
                <option value="va">Value Analysis</option>
                <option value="da">Demand Analysis</option>
                <option value="oa">Operation Analysis</option>
                <option value="sa">Strategic Analysis</option>
            </select>
        </div>
        <!--// Document Type Drop down-->

        <!-- Document Format Drop down-->
        <div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">
            <label class="col-xs-12" style="text-align: center">Document Format</label>
            <select class="form-control col-xs-8" id="docs-format">
                <option value="pdf">PDF Document</option>
                <option value="video">Video Document</option>
                <option value="audio">Audio Document</option>
            </select>
        </div>
        <!-- // Document Format Drop down-->
    </div>
</div>
<!-- // Header Section -->

<hr>
<div class="row ynm-row">
    <!-- Docs Listing container-->
    <div class="col-xs-3 docs_outline">
        <div class="sidebar" style="padding-left: 2px">
            <p id="docs-listing">
            </p>
        </div>
    </div>
    <!-- // Docs Listing container-->

    <!-- Canvas container-->
    <div class="col-md-7 docs_container">
        <div class="row ynm-row">
            <div class="col-md-12 container">
                <div class="sidebar" id="canvas-container">
                </div>
            </div>
        </div>
    </div>
    <!--// Canvas container-->

    <!-- Sent Version-->
    <div class="col-md-2 docs_version">
        <div class="sidebar" id="docs-sent-version" style="word-break: break-all;">
            <h4 class="main-con-header">Sent Versions</h4>
        </div>
    </div>
    <!--// Sent Version-->

</div>

<!-- DOCS Buttons -->
<div class="col-md-12" id="docs_buttons" style="margin: 1%">
    <div class="class-col-xs-12" align="center">
        <input id="fileupload" type="file" name="files[]"
               data-url= <?= "https://" . $_SERVER['SERVER_NAME'] . "/tools/cpdv1/upload_docs" ?> style="visibility:
               hidden; float:left; width:0;">
        <button id="upload-docs" class="btn btn-secondary">Upload<i class="fa fa-upload"></i>
        </button>
        <button id="send-docs" type="button" class="btn btn-secondary">Send<i class="fa fa-envelope"></i></button>
        <button id="delete-docs" class="btn btn-secondary" data-toggle="modal" data-target="#delete-doc-dialog">
            Delete<i class="fa fa-trash"></i></button>
    </div>
</div>

<div class="col-lg-10 progress fileupload-progress"
     style="float: unset; margin: auto; padding-right: 0; padding-left: 0; display: none">
    <!-- The global progress bar -->
    <div class="progress progress-striped" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="70">
        <div class="progress-bar progress-bar-success" style="width: 0%;"></div>
    </div>
    <!-- The extended global progress state -->
    <div class="progress-extended">&nbsp;</div>
</div>

<h5 class="text-center" id="progress" style="color: green"></h5>
<h5 id="error" class="text-center" style="color: red"></h5>

<?php $this->partial('views/cpdv1/dialogs/send_docs.php'); ?>
<?php $this->partial('views/cpdv1/dialogs/delete_doc.php'); ?>

<script src="<?php echo $this->basepath; ?>resources/app/js/minified_cpdv1/docs.js"></script>
<script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>