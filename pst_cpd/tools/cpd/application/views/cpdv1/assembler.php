<!-- Document Builder Page -->
<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/minified_cpdv1/assembler.css?v=1"/>

<!-- Header Section -->
<div class="row assembler_head">
    <div class="col-xs-12">
        <!-- Back Button-->
        <div class="col-xs-4">
            <a class="btn btn-secondary back-button" style="float: right; padding-right: 25px">
                <img src="http://cpd.powersellingtools.com/wp-content/uploads/2020/01/Component-Arrow-Back.png"
                     style="height: 5vh; width: 70%;" alt="back"></a>
        </div>
        <!--// Back Button-->

        <div class="col-xs-4">
            <h2 id="tsl-assembler-heading" style="text-align:center" class="heading">Document Assembler</h2>
        </div>

        <!-- Collaboration Button-->
        <div class="col-xs-4">
            <a class="btn btn-secondary collab-button" style="float: right; padding-right: 25px">
                <img src="http://cpd.powersellingtools.com/wp-content/uploads/2020/01/Collaboration-Arrows-2.png"
                     style="height: 5vh; width: 70%;" alt="collaboration">
            </a>
        </div>
        <!--// Collaboration Button-->
    </div>
    <div class="col-xs-12">

        <!-- Client Type Drop down-->
        <div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">
            <label class="col-xs-12" style="text-align: center">Client Type:</label>
            <select class="form-control" id="client-type">
                <option value="r">Radish</option>
                <option value="w">Wheat</option>
                <option value="t">Tree</option>
                <option value="i">Institutional</option>
                <option value="o">Other</option>
            </select>
        </div>
        <!--// Client Type Drop down-->

        <!-- Document type Drop down-->
        <div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">
            <label class="col-xs-12" style="text-align: center">Document Delivery</label>
            <select class="form-control col-xs-8" id="delivery-type">
                <option value="do">Downloadable</option>
                <option value="wp">Web Presentation</option>
                <option value="vo">View Online Only</option>
                <option value="pf">Printer Friendly</option>
            </select>
        </div>
        <!--// Document Type Drop down-->

        <!-- Document Format Drop down-->
        <div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">
            <label class="col-xs-12" style="text-align: center">Document Format</label>
            <select class="form-control col-xs-8" id="assembler-format">
                <option value="da">Detailed Analysis</option>
                <option value="ov">Opinion of Value</option>
                <option value="uo">Underwriting Overview</option>
                <option value="mp">Marketing Proposal</option>
                <option value="la">Listing Agreement</option>
                <option value="era">Exclusive Rep Agreement</option>
                <option value="ca">Confidentiality Agreement</option>
                <option value="psa">Purchase Sale Agreement</option>
                <option value="ea">Escrow Agreement</option>
                <option value="psa">Repeat Business Agreement</option>
                <option value="ra">Referral Agreement</option>
                <option value="taa">Team Architecture Agreement</option>
                <option value="cba">Co-Brokerage Agreement</option>
            </select>
        </div>
        <!-- // Document Format Drop down-->
    </div>
</div>
<!-- // Header Section -->

<hr>
<div class="row ynm-row">
    <!-- assembler Listing container-->
    <div class="col-xs-3 assembler_outline">
        <div class="sidebar" style="padding-left: 2px">
            <p id="assembler-listing"></p>
            <h4 class="main-con-header">Page Order</h4>
            <ul id="pages-list" class="nav">
            </ul>
        </div>
    </div>
    <!-- // assembler Listing container-->

    <!-- Canvas container-->
    <div class="col-md-7 assembler_container">
        <div class="row ynm-row">
            <div class="col-md-12 container">
                <div class="sidebar" id="canvas-container">
                    <h4 class="main-con-header">Page Sequence Composer</h4>
                </div>
            </div>
        </div>
    </div>
    <!--// Canvas container-->

    <!-- Sent Version-->
    <div class="col-md-2 assembler_version">
        <div class="sidebar" id="assembler-sent-version" style="word-break: break-all;">
            <h4 class="main-con-header">Document Versions</h4>
            <ul id="version-list" class="nav">
            </ul>
        </div>
    </div>
    <!--// Sent Version-->

</div>

<!-- assembler Buttons -->
<div class="row" style="margin-top: 1%">
    <div class="col-md-3" align="center">
        <button id="move_up" class="btn btn-secondary">Move Up <i class="fa fa-arrow-up"></i></button>
        <button id="move_down" class="btn btn-secondary">Move Down <i class="fa fa-arrow-down"></i></button>
        <input id="page-upload" type="file" name="files[]"
               data-url= <?= "https://" . $_SERVER['SERVER_NAME'] . "/tools/cpdv1/upload_page" ?> style="visibility:
               hidden; float:left; width:0;">
        <button id="upload-page" class="btn btn-secondary">Upload <i class="fa fa-upload"></i>
        </button>
        <button id="delete-page" class="btn btn-secondary" data-title="Page" data-toggle="modal"
                data-target="#delete-dialog" data-controller="page">Delete <i class="fa fa-trash"></i></button>
    </div>

    <div class="col-md-7" id="assembler_buttons" align="center">
        <button id="assemble" class="btn btn-secondary">Assemble <i class="fa fa-file-pdf-o"></i></button>
        <button id="save_doc" class="btn btn-secondary">Save <i class="fa fa-check"></i></button>
        <button id="save_as_new_doc" class="btn btn-secondary">Save As New <i class="fa fa-check"></i></button>
        <button id="send-assembler" type="button" class="btn btn-secondary">Send <i class="fa fa-envelope"></i>
        </button>
    </div>

    <div class="col-md-2" align="center">
        <button id="add_doc" class="btn btn-secondary">Add <i class="fa fa-plus"></i></button>
        <input id="fileupload" type="file" name="files[]"
               data-url= <?= "https://" . $_SERVER['SERVER_NAME'] . "/tools/cpdv1/upload_doc" ?> style="visibility:
               hidden; float:left; width:0;">
        <button id="upload-doc" class="btn btn-secondary">Upload <i class="fa fa-upload"></i>
        </button>
        <button id="delete-doc" class="btn btn-secondary" data-title="Assembled Doc" data-toggle="modal"
                data-target="#delete-dialog" data-controller="assembled_doc">Delete <i class="fa fa-trash"></i></button>
    </div>
</div>

<!--Progress Bar-->
<div class="col-lg-10 progress fileupload-progress"
     style="float: unset; margin: auto; padding-right: 0; padding-left: 0; display: none">
    <!-- Global progress bar -->
    <div class="progress progress-striped" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="70">
        <div class="progress-bar progress-bar-success" style="width: 0%;"></div>
    </div>
    <!-- Extended global progress state -->
    <div class="progress-extended">&nbsp;</div>
</div>
<!-- // Progress Bar-->

<h5 class="text-center" id="progress" style="color: green"></h5>
<h5 id="error" class="text-center" style="color: red"></h5>

<?php $this->partial('views/cpdv1/dialogs/delete_dialog.php'); ?>
<script src="<?php echo $this->basepath; ?>resources/app/js/cpdv1/assembler.js"></script>
<script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>