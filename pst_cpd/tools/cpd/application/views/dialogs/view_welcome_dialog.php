<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support and CSS. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/dialogs/view_welcome_dialog.js"></script>

<div class="modal fade" id="view-welcome-dialog" tabindex="-1" role="dialog" aria-labelledby="welcome-label"
     aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content welcome-dialog-content">
            <div class="modal-body">
                <button type="button" style="position: absolute; right: 20px; opacity: 0.6" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                <img src="#" id="welcome-img" width="100%">
            </div>
        </div>
    </div>
</div>
