<!-- View SA dialog view.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/dialogs/view_asset_info.js"></script>

<div class="modal fade" id="view-asset-info-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="message-label" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="view-asset-info-title" class="modal-title">Asset Profile</h2>
            </div>
            <div class="modal-body" style="height: 75vh; overflow-y: scroll">
                <!-- Tab List. -->
                <ul class="nav nav-tabs top-buffer" role="tablist" style="padding: 10px; margin: auto;
                 display: flex; justify-content: center;" id="asset-info-type">
                    <li role="presentation" class="active" data-val="ai">
                        <a href="#" role="tab" data-toggle="tab">ASSET</a>
                    </li>
                    <li role="presentation" data-val="pi">
                        <a href="#" role="tab" data-toggle="tab">PEOPLE</a>
                    </li>
                    <li role="presentation" data-val="fi">
                        <a href="#" role="tab" data-toggle="tab">FINANCIAL</a>
                    </li>
                    <li role="presentation" data-val="li">
                        <a href="#" role="tab" data-toggle="tab">LEGAL</a>
                    </li>
                    <li role="presentation" data-val="mi">
                        <a href="#" role="tab" data-toggle="tab">MARKET</a>
                    </li>
                </ul>
                <!--// Tab List. -->

                <!-- Show the list of sa. -->
                <div class="row">
                    <div class="col-xs-12">

                        <h3 id="category-type"></h3>
                        <!-- List SA -->
                        <div class="row" id="asset-info-row">
                            <div class="col-xs-12">
                                <div id="asset-info-div" class="top-buffer">

                                    <!-- Categories Div-->
                                    <div class="col-xs-12" id="category-div">
                                        <div>
                                            <h4>
                                                Category: <span class="editable" data-sub-seq="0" id="category-name"
                                                                data-kind="category" data-seq="1"> Name </span>
                                            </h4>
                                            <textarea cols="2" class="form-control asset-info-field"
                                                      data-type="category" data-seq="1" data-sub-seq="0"></textarea>
                                        </div>
                                        <div class="col-xs-12" style="margin-left: 20px" id="sub-category-div">
                                            <span id="add-sub-category"
                                                  data-seq="1">(( Add Sub-Category ))</span>
                                            <div>
                                                <h4>
                                                    Sub-Category 1: <span class="editable sub-category-name"
                                                                          data-kind="sub-category"
                                                                          data-seq="1" data-sub-seq="1"> Product </span>

                                                    <span style="float: right" class="show-hide-feature" data-seq="1"><i
                                                                class="fa fa-plus-circle"></i></span>
                                                </h4>
                                                <textarea cols="2" class="form-control asset-info-field"
                                                          data-type="sub-category" data-seq="1"
                                                          data-sub-seq="1">
                                                </textarea>
                                            </div>
                                            <div>
                                                <h4>
                                                    Sub-Category 2: <span class="editable sub-category-name"
                                                                          data-kind="sub-category"
                                                                          data-seq="1" data-sub-seq="2"> Name </span>

                                                    <span style="float: right" class="show-hide-feature" data-seq="2"><i
                                                                class="fa fa-plus-circle"></i></span>
                                                </h4>
                                                <textarea cols="2" class="form-control asset-info-field"
                                                          data-type="sub-category" data-seq="1"
                                                          data-sub-seq="2"></textarea>
                                            </div>
                                            <div>
                                                <h4>
                                                    Sub-Category 3: <span class="editable sub-category-name"
                                                                          data-kind="category"
                                                                          data-seq="1" data-sub-seq="3"> Name </span>

                                                    <span style="float: right" class="show-hide-feature" data-seq="3"><i
                                                                class="fa fa-plus-circle"></i></span>
                                                </h4>
                                                <textarea cols="2" class="form-control asset-info-field"
                                                          data-type="sub-category" data-seq="1"
                                                          data-sub-seq="3"></textarea>
                                            </div>
                                            <div>
                                                <h4>
                                                    Sub-Category 4: <span class="editable sub-category-name"
                                                                          data-kind="sub-category"
                                                                          data-seq="1" data-sub-seq="4"> Name </span>

                                                    <span style="float: right" class="show-hide-feature" data-seq="4"><i
                                                                class="fa fa-plus-circle"></i></span>
                                                </h4>
                                                <textarea cols="2" class="form-control asset-info-field"
                                                          data-type="sub-category" data-seq="1"
                                                          data-sub-seq="4"></textarea>
                                            </div>
                                            <div>
                                                <h4>
                                                    Sub-Category 5: <span class="editable sub-category-name"
                                                                          data-kind="sub-category"
                                                                          data-seq="1" data-sub-seq="5"> Name </span>

                                                    <span style="float: right" class="show-hide-feature" data-seq="5">
                                                        <i class="fa fa-plus-circle"></i></span>
                                                </h4>
                                                <textarea cols="2" class="form-control asset-info-field"
                                                          data-type="sub-category" data-seq="1"
                                                          data-sub-seq="5"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- // List SA -->

                            <!-- Dummy Feature Division-->
                            <div class="input-group hidden" id="dummy-individual-feature">
                                                    <textarea class="form-control" rows="1"
                                                              placeholder="Feature 1: Property Type (Describe Feature)"></textarea>
                                <span class="input-group-btn">
                                                            <button class="btn btn-default delete-feature"><i
                                                                        class="fa fa-trash"></i></button></span>
                            </div>
                            <!--// Dummy Feature Division-->

                            <!-- Dummy Subcategory division-->
                            <div class="hidden" id="dummy-sub-category">
                                <h4>
                                    Sub-Category 1: <span class="editable sub-category-name"
                                                          data-kind="sub-category"
                                                          data-seq="1"> Product </span>

                                    <span style="float: right" class="show-hide-feature" data-seq="1"><i
                                                class="fa fa-plus-circle"></i></span>
                                </h4>
                                <textarea cols="2" class="form-control asset-info-field"
                                          data-type="sub-category" data-seq="1"></textarea>
                            </div>
                            <!--// Dummy Subcategory division-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
