<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support and CSS. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/dialogs/user_profile.js"></script>

<div class="modal fade" id="user-profile-dialog" tabindex="-1" role="dialog" aria-labelledby="user-profile-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="user-profile-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="user-profile-title" class="modal-title">Update User Profile</h2>
                </div>

                <div class="modal-body">

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="user_name">User Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <input class="input-group form-control" id="user-name" name="user_name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="user_email">User Email</label>
                        <div class="col-xs-12 col-sm-9">
                            <input class="input-group form-control" id="user-email" name="user_email">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="tsl-product-id">Select
                            Industry</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <select class="form-control" id="tsl-industry" name="tsl_industry_id">
                                    <option> Commercial Real Estate</option>
                                </select>
                            </div>
                            <p class="form-text text-muted">Select Industry.</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="tsl-product-id">Select
                            Profession</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <select class="form-control" id="tsl-profession" name="tsl_professsion_id">
                                    <option> Investment Sales</option>
                                </select>
                            </div>
                            <p class="form-text text-muted">Select Profession Type.</p>
                        </div>
                    </div>

                    <div class="form-group product-type">
                        <label class="col-xs-12 col-sm-3 control-label" for="primary-product-type">Select
                            Primary Product Type</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <select class="form-control profile-product-type" id="primary-product-type"
                                        name="primary_product"></select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group product-type">
                        <label class="col-xs-12 col-sm-3 control-label" for=secondary-product-type">Select
                            Second Product Type</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <select class="form-control profile-product-type" id="secondary-product-type"
                                        name="secondary_product"></select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group product-type">
                        <label class="col-xs-12 col-sm-3 control-label" for=secondary-product-type">Select
                            Third Product Type</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <select class="form-control profile-product-type" id="tertiary-product-type"
                                        name="tertiary_product"></select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group demo_product hidden">
                    <label class="col-xs-12 col-sm-3 control-label" id="demo-product-label"
                           for=demo-product-type"></label>
                    <div class="col-xs-12 col-sm-9">
                        <div class="input-group">
                            <select class="form-control" id="demo-product-type"></select>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" style="height: auto;">Close
                    </button>
                    <button type="button" class="btn btn-primary" style="height: auto;" id="add-product">
                        Add Product Type
                    </button>
                    <button type="button" class="btn btn-primary" id="save-profile" style="height: auto;">Update
                        Profile
                    </button>
                </div>

            </form>
        </div>
    </div>
</div>
