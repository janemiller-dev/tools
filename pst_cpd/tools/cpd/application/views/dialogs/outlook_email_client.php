<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support and CSS. -->
<script src="//kjur.github.io/jsrsasign/jsrsasign-latest-all-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.5/handlebars.min.js"></script>
<script src="<?php echo $this->basepath; ?>resources/app/js/dialogs/outlook_email_client.js"></script>
<script type="text/javascript"
        src="https://cdn.jsdelivr.net/npm/@microsoft/microsoft-graph-client/lib/graph-js-sdk.js"></script>


<div class="modal fade" id="outlook-email-client-dialog" tabindex="-1" role="dialog"
     aria-labelledby="email-support-label"
     aria-hidden="true">
    <div class="modal-dialog" style="width: fit-content;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="view-asset-info-title" class="modal-title">Quick Email</h2>
            </div>

            <div class="modal-body" style="height: auto; overflow-y: scroll">
                <div class="container">
                    <ul class="nav nav-tabs top-buffer authed-nav" role="tablist" style="padding: 10px; margin: auto;
                                     display: flex; justify-content: center; font-size: 16px">
                        <li role="presentation" id='home-nav'><a href="#">Home</a></li>
                        <li id='compose-nav'><a href="#compose">Compose</a></li>
                        <li role="presentation" id='inbox-nav'><a href="#inbox">Inbox</a></li>
                        <li id='sent-nav' role="presentation"><a href="#sent">Sent</a></li>
                        <li id='contacts-nav' role="presentation"><a href="#contacts">Contacts</a></li>
<!--                        <li id='calendar-nav' role="presentation"><a href="#calendar">Calendar</a></li>-->
                    </ul>
                </div>

                <div class="container main-container email-client-container">
                    <div id="signin-prompt" class="jumbotron page">
                        <a class="btn btn-lg btn-default" href="#" role="button" id="connect-button">Connect to
                            Outlook</a>
                        </p>
                    </div>
                    <!-- logged in user welcome -->
                    <div id="logged-in-welcome" class="jumbotron page">
                        <!--                        <h1>Outlook SPA Demo</h1>-->
                        <p style="display: inline">Welcome <span id="username"></span>! Please use the nav menu to
                            access your Outlook data.</p>

                        <a href="#signout" class="btn btn-default" style="float: right; display: inline-block">Sign
                            out</a>
                    </div>

                    <!-- unsupported browser message -->
                    <div id="unsupported" class="jumbotron page">
                        <h1>Oops....</h1>
                        <p>This page requires browser support for <a
                                    href="https://developer.mozilla.org/docs/Web/API/Web_Storage_API">session
                                storage</a> and <a
                                    href="https://developer.mozilla.org/docs/Web/API/RandomSource/getRandomValues">
                                <code>crypto.getRandomValues</code></a>.
                            Unfortunately, your browser does not support one or both features. Please visit this page
                            using a different browser.</p>
                    </div>
                    <!-- error message -->
                    <div id="error-display" class="page panel panel-danger">
                        <div class="panel-heading">
                            <h3 class="panel-title" id="error-name"></h3>
                        </div>
                        <div class="panel-body">
                            <pre><code id="error-desc"></code></pre>
                        </div>
                    </div>

                    <!-- inbox display -->
                    <div id="inbox" class="page panel panel-default">
                        <div class="panel-heading">
                            <h1 class="panel-title">Inbox</h1>
                        </div>
                        <div id="inbox-status" class="panel-body">
                        </div>
                        <div class="list-group" id="message-list">
                        </div>
                    </div>

                    <!-- Sent display -->
                    <div id="sent" class="page panel panel-default">
                        <div class="panel-heading">
                            <h1 class="panel-title">Sent Box</h1>
                        </div>
                        <div id="sent-status" class="panel-body">
                        </div>
                        <div class="list-group" id="sent-list">
                        </div>
                    </div>

                    <!-- contacts display -->
                    <div id="contacts" class="page panel panel-default">
                        <div class="panel-heading">
                            <h1 class="panel-title">Contacts</h1>
                        </div>
                        <div id="contacts-status" class="panel-body">
                        </div>
                        <div class="list-group" id="contact-list">
                        </div>
                    </div>

                    <!-- calendar display -->
<!--                    <div id="calendar" class="page panel panel-default">-->
<!--                        <div class="panel-heading">-->
<!--                            <h1 class="panel-title">Calendar</h1>-->
<!--                        </div>-->
<!--                        <div id="calendar-status" class="panel-body">-->
<!--                        </div>-->
<!--                        <div class="list-group" id="event-list">-->
<!--                        </div>-->
<!--                    </div>-->

                    <!-- Message Content display -->
                    <div id="message-content" class="page panel panel-default">
                        <div class="panel-heading">
                            <h1 class="panel-title">Message Content</h1>
                        </div>
                        <div id="message-body" class="panel-body">
                        </div>
                    </div>

                    <!-- Compose Message -->
                    <div id="compose" class="page panel panel-default">
                        <div class="panel-heading" style="padding: 10px">
                            <h1 class="panel-title" style="display: inline-block">Compose Email</h1>
                            <button class="btn btn-primary" id="send-outlook-email" style="float: right">Send
                                <i class="fa fa-paper-plane"></i></button>
                        </div>
                        <div>
                            <div style="margin: 10px">
                                <label class="col-xs-3">To: </label>
                                <div class="col-xs-9">
                                    <input type="text" class="form-control" id="recipient-email-id"
                                           placeholder="Recipent Email"/>
                                </div>
                            </div>

                            <div style="margin: 10px">
                                <label class="col-xs-3">Subject: </label>
                                <div class="col-xs-9">
                                    <input type="text" class="form-control" id="Subject" placeholder="Email Subject"/>
                                </div>
                            </div>
                            <div style="margin: 10px">
                                <label></label>
                                <div id="outlook-compose-body" class="panel-body">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Handlebars template for message list -->
<script id="msg-list-template" type="text/x-handlebars-template">
    {{#each messages}}
    <div class="list-group-item">
        <h3 id="msg-from" class="list-group-item-">
            <a href="#" class="getMessageContent" data-id={{this.id}}>
                {{this.from.emailAddress.name}} </a></h3>
        <h4 id="msg-subject" class="list-group-item-heading">{{this.subject}}</h4>
        <p id="msg-received" class="list-group-item-heading text-muted"><em>Received: {{formatDate
                this.receivedDateTime}}</em></p>
        <p id="msg-preview" class="list-group-item-text text-muted"><em>{{this.bodyPreview}}</em></p>
    </div>
    {{/each}}
</script>

<!-- Handlebars template for Sent message list -->
<script id="sent-msg-list-template" type="text/x-handlebars-template">
    {{#each messages}}
    <div class="list-group-item">
        <h3 id="msg-from" class="list-group-item-">
            <a href="#" class="getMessageContent" data-id={{this.id}}>
                {{this.from.emailAddress.name}} </a></h3>
        <h4 id="msg-subject" class="list-group-item-heading">{{this.subject}}</h4>
        <p id="msg-received" class="list-group-item-heading text-muted"><em>Received: {{formatDate
                this.receivedDateTime}}</em></p>
        <p id="msg-preview" class="list-group-item-text text-muted"><em>{{this.bodyPreview}}</em></p>
    </div>
    {{/each}}
</script>

<!-- Handlebars template for contact list -->
<script id="contact-list-template" type="text/x-handlebars-template">
    {{#each contacts}}
    <div class="list-group-item">
        <h4 id="contact-name" class="list-group-item-heading">{{this.givenName}} {{this.surname}}</h4>
        <p id="contact-email" class="list-group-item-heading">Email: {{this.emailAddresses.0.address}}</p>
    </div>
    {{/each}}
</script>

<!-- Handlebars template for event list -->
<script id="event-list-template" type="text/x-handlebars-template">
    {{#each events}}
    <div class="list-group-item">
        <h4 id="event-subject" class="list-group-item-heading">{{this.subject}}</h4>
        <p id="event-start" class="list-group-item-heading">Start: {{formatDate this.start.dateTime}}</p>
        <p id="event-end" class="list-group-item-heading">End: {{formatDate this.end.dateTime}}</p>
    </div>
    {{/each}}
</script>