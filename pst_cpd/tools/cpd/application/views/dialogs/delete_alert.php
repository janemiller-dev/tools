<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<div class="modal fade" id="delete-alert-dialog" tabindex="-1" data-backdrop="static" role="dialog"
     aria-labelledby="delete-alert-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="delete-alert-form" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="delete-alert-title" class="modal-title">Delete Alert?</h2>
                </div>

                <div class="modal-body modal-small">
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="name"> Client Name: </label>
                        <div class="col-xs-12 col-sm-9">
                            <p class="form-control-static" id="alert-name"></p>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="close-delete-alert-dialog" data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary" id="delete-alert-button">Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>
