<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/dialogs/send_email.js"></script>

<div class="modal fade" id="send-email-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="send-email-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="send-email-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="send-email-title" class="modal-title">Do you want to send this Email?</h2>
                </div>

                <div class="modal-body">

                    <div class="form-group" style="margin: 5px !important;">
                        <label class="col-xs-12 col-sm-3 control-label" for="client-email">To:</label>
                        <div class="col-xs-12 col-sm-9 input-group client-email-div">
                            <input type="email" class="form-control client-email" id="send-email-id">
                            <span class="input-group-btn">
                                <span class="has-tooltip" title="Add Recipient" data-placement="bottom">
                                    <button class="btn btn-default" type="button" id="add-recipient">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </span>
                            </span>
                        </div>
                    </div>

                    <div class="form-group" style="margin: 5px !important;">
                        <label class="col-xs-12 col-sm-3 control-label" for="client-subject">Subject:</label>
                        <div class="col-xs-12 col-sm-9" style="padding: 0">
                            <input type="text" class="form-control" id="client-subject">
                        </div>
                    </div>

                    <div id="email-content">
                    </div>
                </div>


                <input type="hidden" id="client-id" name="client_id"/>
                <div class="modal-footer">
                    <span class="hidden" style="float: left; font-size: 150%" id="attachment_span"><i
                                class="fa fa-paperclip"></i><span id="attachment_name"></span></span>
                    <button type="button" class="btn btn-primary" id="close-send-email-dialog" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary" id="send-email-button">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>
