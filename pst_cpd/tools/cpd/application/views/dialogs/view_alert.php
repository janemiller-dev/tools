
<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/dialogs/view_alert.js"></script>

<div class="modal fade" id="view-alert-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="message-label"
     aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="view-alert-title" class="modal-title">Accountability Manager.</h2>

                <select class="form-control" id="select-acm-duration" style="float: right; margin: 0 40px 0 10px">
                    <option value="3">3 Months</option>
                    <option value="6">6 Months</option>
                    <option value="9">9 Months</option>
                    <option value="12">12 Months</option>
                </select>
                <h4 style="float: right">Select Period: </h4>
            </div>

            <div class="modal-body" style="font-size: 18px; padding: 0 10px">
                <!-- Show the list of alerts. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- List Alert table -->
                        <div class="row" id="alert-row">
                            <div class="col-xs-12">
                                <div id="alert-div" class="top-buffer">
                                    <h4 id="alert-div-heading"></h4>
                                    <table class="table" id="alert-table">
                                        <thead>
                                        <tr style="background: #8ff1fd">
                                            <th></th>
                                            <th>DateTime Due</th>
                                            <th>Client</th>
                                            <th>Tool</th>
                                            <th>Objectives</th>
                                            <th>Status</th>
                                            <th>Done</th>
                                            <th>View</th>
                                            <th>Color</th>
                                            <th>Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- List Alert table -->
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="reset-acm-color">Reset Colors</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php $this->partial('views/dialogs/add_alert_objective.php'); ?>
