<!-- Add Accountablity Manager Requests/Promises view.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<!--<script src="--><?php //echo $this->basepath; ?><!--resources/app/js/cpdv1/dialog/add_alert_objective.js"></script>-->

<div class="modal fade" id="add-alert-objective-dialog" tabindex="-1" role="dialog" aria-labelledby="add-alert-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="add-alert-form" class="form-horizontal">

                <!-- Modal Header-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-alert-title" class="modal-title">Add Alert Objective</h2>
                </div>
                <!--// Modal Header-->

                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-xs-3 control-label" for="alert-ceo-id">Objective Title</label>
                        <div class="col-xs-9">
                            <input type="text" id="objective-title" name="objective_title" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="save-objective">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
