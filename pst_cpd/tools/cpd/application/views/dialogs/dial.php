<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/dial.css"/>

<div class="modal fade" id="dial-dialog" tabindex="-1" role="dialog" aria-labelledby="dial-label"
     aria-hidden="true">
    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="email-support-title" class="modal-title">Quick Phone Dialer</h2>
            </div>

            <div class="modal-body phone-dialog">

                <div id="controls">
                    <div id="info">
                        <div id="client-name"></div>
                        <div id="output-selection">
                            <label>Ringtone Devices</label>
                            <select id="ringtone-devices" multiple></select>
                            <label>Speaker Devices</label>
                            <select id="speaker-devices" multiple></select><br/>
                            <a id="get-devices">Seeing unknown devices?</a>
                        </div>
                    </div>
                    <div id="call-controls">
                        <p class="instructions">Make a Call:</p>
                        <input id="phone-number" type="text" placeholder="Enter a phone #"/>
                        <button id="button-call">Call</button>
                        <button id="button-hangup">Hangup</button>
                        <div id="volume-indicators">
                            <label>Mic Volume</label>
                            <div id="input-volume"></div>
                            <br/><br/>
                            <label>Speaker Volume</label>
                            <div id="output-volume"></div>
                        </div>
                        <div class="" id="log"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo $this->basepath; ?>resources/app/js/dialogs/dial.js"></script>
