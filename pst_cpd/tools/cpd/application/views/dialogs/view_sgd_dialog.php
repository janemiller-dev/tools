<!-- View SA dialog view.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/dialogs/view_sgd_dialog.js"></script>

<div class="modal fade" id="view-sgd-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="message-label" aria-hidden="true">
    <div class="modal-dialog modal-sgd">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="view-sgd-title" class="modal-title">Strategic Game Designer</h2>
            </div>
            <div class="modal-body" style="height: 75vh; overflow-y: scroll">
                <!-- Tab List. -->
                <ul class="nav nav-tabs top-buffer" role="tablist" style="padding: 10px; margin: auto;
                 display: flex; justify-content: center;" id="sgd-type">
                    <li role="presentation" class="active" data-val="accomplishment">
                        <a role="tab" data-toggle="tab">Accomplishment</a>
                    </li>
                    <li role="presentation" data-val="projects">
                        <a role="tab" data-toggle="tab">Projects</a>
                    </li>
                    <li role="presentation" data-val="possibilities">
                        <a role="tab" data-toggle="tab">Possibilities</a>
                    </li>
                    <li role="presentation" data-val="commitments">
                        <a role="tab" data-toggle="tab">Commitments</a>
                    </li>
                    <li role="presentation" data-val="actions">
                        <a role="tab" data-toggle="tab">Actions</a>
                    </li>
                    <li role="presentation" data-val="results">
                        <a role="tab" data-toggle="tab">Results</a>
                    </li>
                </ul>
                <!--// Tab List. -->

                <!-- SGD Tabs. -->
                <div id="sgd-tab-body">
                </div>


                <div class="tab-pane col-xs-12 hidden" id="accomplishment">
                    <h4><b>What has been accomplished in the past
                            <input type="text" id="month-count" class="form-control"
                                   style="width: 4vw; display: inline-block"/> months?</b></h4>

                    <div data-id="development">
                        <label style="margin: 2px; padding: 10px">Development</label>
                        <ol>
                            <li><textarea class="form-control" rows="2" data-seq="1"></textarea></li>
                            <li><textarea class="form-control" rows="2" data-seq="2"></textarea></li>
                            <li><textarea class="form-control" rows="2" data-seq="3"></textarea></li>
                        </ol>
                    </div>

                    <div data-id="performance">
                        <label style="margin: 2px; padding: 10px">Performance</label>
                        <ol>
                            <li><textarea class="form-control" rows="2" data-seq="1"></textarea></li>
                            <li><textarea class="form-control" rows="2" data-seq="2"></textarea></li>
                            <li><textarea class="form-control" rows="2" data-seq="3"></textarea></li>
                        </ol>
                    </div>

                    <div data-id="fulfillment">
                        <label style="margin: 2px; padding: 10px">Fulfillment</label>
                        <ol>
                            <li><textarea class="form-control" rows="2" data-seq="1"></textarea></li>
                            <li><textarea class="form-control" rows="2" data-seq="2"></textarea></li>
                            <li><textarea class="form-control" rows="2" data-seq="3"></textarea></li>
                        </ol>
                    </div>
                </div>

                <div class="tab-pane col-xs-12 hidden" id="projects">
                    <h4><b>What projects will I take on in the next
                            <input type="text" id="month-count" class="form-control"
                                   style="width: 4vw; display: inline-block"/> months?</b></h4>

                    <div style="border: none">
                        <div class="col-xs-8" data-id="development">
                            <label style="margin: 2px; padding: 10px">Development</label>
                            <ol>
                                <li><textarea class="form-control" rows="2" data-seq="1"></textarea></li>
                                <li><textarea class="form-control" rows="2" data-seq="2"></textarea></li>
                                <li><textarea class="form-control" rows="2" data-seq="3"></textarea></li>
                            </ol>
                        </div>
                        <div class="col-xs-3" data-id="development-date">
                            <label style="margin: 2px; padding: 10px">Date By When</label>
                            <ol>
                                <li><input class="form-control date-picker" data-id="development" data-seq="1"></li>
                                <li><input class="form-control date-picker" data-id="development" data-seq="2"></li>
                                <li><input class="form-control date-picker" data-id="development" data-seq="3"></li>
                            </ol>
                        </div>
                    </div>

                    <div style="border: none">
                        <div class="col-xs-8" data-id="performance">
                            <label style="margin: 2px; padding: 10px">Performance</label>
                            <ol>
                                <li><textarea class="form-control" rows="2" data-seq="1"></textarea></li>
                                <li><textarea class="form-control" rows="2" data-seq="2"></textarea></li>
                                <li><textarea class="form-control" rows="2" data-seq="3"></textarea></li>
                            </ol>
                        </div>

                        <div class="col-xs-3"  data-id="performance-date">
                            <label style="margin: 2px; padding: 10px">Date By When</label>
                            <ol>
                                <li><input class="form-control date-picker" data-id="performance" data-seq="1"></li>
                                <li><input class="form-control date-picker" data-id="performance" data-seq="2"></li>
                                <li><input class="form-control date-picker" data-id="performance" data-seq="3"></li>
                            </ol>
                        </div>
                    </div>

                    <div>
                        <div class="col-xs-8" data-id="fulfillment">
                            <label style="margin: 2px; padding: 10px">Fulfillment</label>
                            <ol>
                                <li><textarea class="form-control" rows="2" data-seq="1"></textarea></li>
                                <li><textarea class="form-control" rows="2" data-seq="2"></textarea></li>
                                <li><textarea class="form-control" rows="2" data-seq="3"></textarea></li>
                            </ol>
                        </div>
                        <div class="col-xs-3" data-id="fulfillment-date">
                            <label style="margin: 2px; padding: 10px">Date By When</label>
                            <ol>
                                <li><input class="form-control date-picker" data-id="fulfillment" data-seq="1"></li>
                                <li><input class="form-control date-picker" data-id="fulfillment" data-seq="2"></li>
                                <li><input class="form-control date-picker" data-id="fulfillment" data-seq="3"></li>
                            </ol>
                        </div>
                    </div>
                </div>

                <!--// SGD Tabs. -->

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
