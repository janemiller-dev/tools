<!-- Alerts Notification View -->
<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/dialogs/reminder.js"></script>

<div class="modal fade" id="view-reminder-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="send-email-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="send-email-title" class="modal-title">Quick Reminder.</h2>

                <span style="float: right; margin: 10px 40px 10px 10px" id="reminder-next-day"><i
                            class="fa fa-arrow-circle-right"></i></span>
                <input style="float: right; margin: 10px; width: 6vw" id="reminder-date-picker"/>
                <span style="float: right; margin: 10px" id="reminder-prev-day"><i class="fa fa-arrow-circle-left"></i></span>
                <label style="float: right; margin: 10px">Select Date: </label>
            </div>

            <div class="modal-body">
                <!-- Page Title -->
                <div class="row">
                    <div class="col-xs-12 text-center" style="height: 65vh; overflow-y: scroll">
                        <table class="table table-stripped" id="reminder-table">
                            <thead>
                            <tr>
                                <th class="text-center">Client</th>
                                <th class="text-center">Component</th>
                                <th class="text-center">Objective</th>
                                <!--                                <th class="text-center">Due By</th>-->
                                <th class="text-center">Due Date</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">X</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <label>Reminder Recurrence</label>
                <select class="form-control" id="reminder-snooze" style="width: 8vw; display: inline-block">
                    <option value="15">15 Minutes</option>
                    <option value="30">30 Minutes</option>
                    <option value="60">1 Hour</option>
                    <option value="120">2 Hour</option>
                    <option value="240">4 Hour</option>
                    <option value="480">8 Hour</option>
                </select>
                <button type="button" class="btn btn-primary" id="set-recurrence">
                    Set
                </button>

                <button type="button" class="btn btn-primary" id="close-delete-alert-dialog" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>