<!-- Add Client Info/reference Instance.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/dialogs/edit_agent_profile.js"></script>

<div class="modal fade" id="edit-agent-profile-dialog" tabindex="10" role="dialog"
     aria-labelledby="edit-agent-profile-label"
     aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <form id="edit-agent-profile-form" class="form-horizontal">

                <!-- Modal Header section-->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 class="modal-title">Edit Agent Profile</h2>

                </div>
                <!-- Modal Header section-->

                <div class="modal-body" style="max-height:70vh; overflow-y: scroll">
                    <!-- Name Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="agent-name">Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="agent-name" name="agent_name"/>
                            <p class="form-text text-muted">Enter Agent Name.</p>
                        </div>
                    </div>
                    <!--// Name Field -->

                    <!-- Title Field-->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="agent-title">Title</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" id="agent-title" name="agent_title" class="form-control">
                            <p class="form-text text-muted">Enter Agent Title.</p>
                        </div>
                    </div>
                    <!--// Title Field -->

                    <!-- Years of Experience Field-->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="entity-name">Years of Experience</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="agent-exp" name="agent_exp"/>
                            <p class="form-text text-muted">Enter Years of Experience.</p>
                        </div>
                    </div>
                    <!--// Years of Experience Field-->

                    <!-- Focus Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="agent-focus">Focus</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="agent-focus" name="agent_focus"/>
                            <p class="form-text text-muted">Enter Agent Focus</p>
                        </div>
                    </div>
                    <!-- // Focus Field -->

                    <!-- Company Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="agent-company">Company Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="agent-company-name" name="agent_company_name"/>
                            <p class="form-text text-muted">Enter Company Name</p>
                        </div>
                    </div>
                    <!--// Company Field -->


                    <!-- Office Address Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="agent-office-address">Office
                            Address</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="agent-office-address"
                                   name="agent_office_address"/>
                            <p class="form-text text-muted">Enter address</p>
                        </div>
                    </div>
                    <!-- // Office Address Field -->

                    <!-- Office Phone Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="agent-office-phone">Office Phone</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="agent-office-phone" name="agent_office_phone"/>
                            <p class="form-text text-muted">Enter Office Phone</p>
                        </div>
                    </div>
                    <!--// Office Phone Field -->

                    <!-- Second Phone Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="agent-Second-phone">Second Phone</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="agent-second-phone" name="agent_second_phone"/>
                            <p class="form-text text-muted">Enter Second Phone.</p>
                        </div>
                    </div>
                    <!-- // Second Phone Field -->

                    <!-- Cell Phone Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="agent-Second-phone">Cell Phone</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="agent-cell-phone" name="agent_cell_phone"/>
                            <p class="form-text text-muted">Enter Cell Phone.</p>
                        </div>
                    </div>
                    <!-- // Cell Phone Field -->

                    <!-- Email Field -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="agent-email">Email</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="email" class="form-control" id="agent-email" name="agent_client_email"/>
                            <p class="form-text text-muted">Enter Email.</p>
                        </div>
                    </div>
                    <!-- // Email Field -->

                    <input type="hidden" name="agent_type" id="agent-type" value="agent"/>
                    <input type="hidden" name="agent_id" id="agent-id"/>
                    <input type="hidden" name="add_agent" id="add-agent"/>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="button" id="save-agent-info" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
