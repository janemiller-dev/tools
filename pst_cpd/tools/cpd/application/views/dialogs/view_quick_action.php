<!-- View SA dialog view.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/dialogs/view_quick_action.js"></script>

<div class="modal fade" id="view-quick-action-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="message-label" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 class="modal-title">Quick Actions List</h2>

                <span style="float: right; margin: 10px 40px 10px 10px" id="action-next-day"><i
                            class="fa fa-arrow-circle-right"></i></span>
                <input style="float: right; margin: 10px; width: 6vw" id="action-date-picker"/>
                <span style="float: right; margin: 10px" id="action-prev-day"><i class="fa fa-arrow-circle-left"></i></span>
                <label style="float: right; margin: 10px">Select Date: </label>
            </div>
            <!-- Modal Header -->

            <!-- Modal Header -->
            <div class="modal-body" style="height: 75vh; overflow-y: scroll">
                <table class="table" id="quick-action-table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>CEO</th>
                        <th>Actions</th>
                        <th>Who</th>
                        <th>Tag</th>
                        <th>CAT</th>
                        <th><span class="fa fa-trash"></span></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
            <!--// Modal Header -->

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="add-quick-action">Add</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
