<?php if ( !defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support and CSS. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/dialogs/share.js"></script>

<div class="modal fade" id="share-dialog" tabindex="-1" role="dialog" aria-labelledby="share-label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

      <form id="share-form" class="form-horizontal">

    <div class="modal-header">
      <button  type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
          </button>
      <h2 id="email-support-title" class="modal-title">Share with others</h2>
    </div>

    <div class="modal-body">

      <div class="form-group">
            <div class="col-xs-12">
              <label class="control-label" for="names">People</label>
              <div class="input-group">
        <input type="text" class="form-control" id="share-names" name="emails" width="100%" placeholder="Enter names that you want to this resource share with.">
        <div class="input-group-btn bs-dropdown-to-select-group">
          <button type="button" class="btn btn-default dropdown-toggle as-is bs-dropdown-to-select" data-toggle="dropdown">
            <span data-bind="bs-drp-sel-label">Permission...</span>
            <input type="hidden" id="share-permission" name="permission" data-bind="bs-drp-sel-value" value="">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu" role="menu" style="">
            <li data-value="edit"><a href="#">Can edit</a></li>
            <li data-value="view"><a href="#">Can view</a></li>
          </ul>
        </div>
              </div>
          <p class="form-text text-muted">Enter the name or email address of the people you want to share with and select the type of sharing.</p>
            </div>
      </div>

      <div id="not-shared">
        <p class="form-text text-muted">This TGD is not currently shared with anyone.</p>
      </div>
      <div id="shared-list-div">
        <p class="form-text text-muted">Currently sharing with the following people:</p>
        <ul id="shared-list">
        </ul>
      </div>

    </div>

    <input type="hidden" name="object" id="share-object" value="" />
    <input type="hidden" name="id" id="share-id" value="" />

    <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Share</button>
        </div>

      </form>
    </div>
  </div>
</div>
