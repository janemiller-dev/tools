<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support and CSS. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/dialogs/email_support.js"></script>

<div class="modal fade" id="email-support-dialog" tabindex="-1" role="dialog" aria-labelledby="email-support-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="email-support-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="email-support-title" class="modal-title">Send a Message to Support</h2>
                </div>

                <div class="modal-body">

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="topic">Topic</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <select class="form-control" id="support-topic" name="topic">
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="name">Subject</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="support-subject" name="subject" width="100%"
                                   placeholder="Subject of your question">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="name">Message</label>
                        <div class="col-xs-12 col-sm-9">
							<textarea class="form-control" rows="2" id="support-question"
                                      placeholder="Support question" name="question"
                                      width="100%"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12 text-center">
                            <div class="g-recaptcha" data-sitekey="6LdCKi8UAAAAAKh9zAbVtXILksrbO0zff9B30jwn"></div>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Send Email</button>
                </div>

            </form>
        </div>
    </div>
</div>
