
<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/dialogs/view_sent.js"></script>

<div class="modal fade" id="view-sent-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="message-label"
     aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="view-sent-title" class="modal-title">View Sent Components</h2>

            </div>

            <div class="modal-body">

                <!-- Show the list of sents. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- List Sent table -->
                        <div class="row" id="sent-row">
                            <div class="col-xs-12">
                                <p id="no-sent">You have no instance of Sent Components.
                                    You can send one using Send Button.</p>
                                <div id="sent-div" class="top-buffer">
                                    <h4 id="sent-div-heading"></h4>
                                    <table class="table table-striped" id="sent-table">
                                        <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center" id="sent-name"></th>
                                            <th class="text-center">Date Time</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- List Sent table -->
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
