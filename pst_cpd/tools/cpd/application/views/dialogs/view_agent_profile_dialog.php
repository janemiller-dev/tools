<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/dialogs/view_agent_profile_dialog.js"></script>

<div class="modal fade" id="view-agent-profile-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="message-label" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="view-agent-profile-title" class="modal-title">Agent Profile</h2>
                <button type="button" id="edit-agent-profile-button" class="btn btn-primary bottom-buffer"
                        data-target="#edit-agent-profile-dialog" data-toggle="modal" data-backdrop="static">
                    Edit Profile
                </button>
            </div>

            <div class="modal-body">
                <!-- Show the list of agent-profiles. -->
                <div class="row" id="agent-profile-row">
                    <div id="agent-profile-div" class="top-buffer">
                        <div class="col-xs-12">
                            <div class="col-xs-5">
                                <div class="col-xs-4 text-center" id="image-div">
                                    <img src="<?php echo $this->basepath; ?>resources/app/media/user-avatar.svg"
                                         id="agent-avatar" style="width: 100%">
                                    <div class="upload">
                                        <input id="agent-img-upload" type="file" name="files[]"
                                               data-url= <?= "https://" . $_SERVER['SERVER_NAME'] .
                                               "/tools/common/upload_agent_image" ?>
                                               style="visibility: hidden; float:left; width:0;">
                                        <a id="upload-agent-img">Change</a>
                                    </div>
                                </div>
                                <div class="col-xs-4" id="agent-info">
                                    <h1 style="margin: 0">Agent</h1>
                                    <h3>Title</h3>
                                    <h4>Years Experience</h4>
                                    <h4>Focus</h4>
                                </div>
                                <div class="col-xs-4" id="agent-contact">
                                    <h4>Company Name</h4>
                                    <p>Office Address</p>
                                    <p>Main Phone</p>
                                    <p>Second Phone</p>
                                    <p>Cell Phone</p>
                                    <p>Email Address</p>
                                </div>
                            </div>

                            <!-- Drop Down boxes-->
                            <div class="col-xs-7">
                                <!-- Partner Drop down. -->
                                <div class="col-xs-6">
                                    <label class="info-label">Partners</label>
                                    <select class="form-control agent-info-select" id="info-partner"
                                            data-name="partner">
                                    </select>
                                </div>
                                <!--// Partner Drop down. -->

                                <!-- Staff Drop down. -->
                                <div class="col-xs-6">
                                    <label class="info-label">Associates</label>
                                    <select class="form-control agent-info-select" id="info-associate"
                                            data-name="associate">
                                    </select>
                                </div>
                                <!-- // Staff Drop down. -->

                                <!-- Advisor Drop down. -->
                                <div class="col-xs-6">
                                    <label class="info-label" id="info-advisor-label">Team Members</label>
                                    <select class="form-control agent-info-select" id="info-team"
                                            data-name="team">
                                    </select>
                                </div>
                                <!-- // Advisor Drop down. -->

                                <!-- Vendor Drop down. -->
                                <div class="col-xs-6">
                                    <label class="info-label" id="info-staff-label">Staff & Advisors</label>
                                    <select class="form-control agent-info-select" id="info-staff"
                                            data-name="staff">
                                    </select>
                                </div>
                                <!-- // Vendor Drop down. -->
                            </div>
                            <!--// Drop Down boxes-->

                        </div>

                        <div class="col-xs-12" id="buttons-div" style="margin: 35px 0 35px 0">
                            <div class="col-xs-2 text-center">
                                <button id="agent-track" data-toggle="popover"
                                        data-html="true" data-placement="bottom"
                                        data-title="<h3>Agent Game Designer</h3>
                                        <select class='form-control' style='display: inline-block;
                                         width: fit-content;'>
                                        <option disabled selected>-- Time Frame -- </option>
                                        <option> This Week </option>
                                        <option> This Quarter </option>
                                        <option> This Year </option>
                                        </select>"
                                        class="btn btn-sm btn-default agent-profile-btn"
                                        data-content="
                                                        <div class='col-xs-6'>
                                                            <div class='text-center' style='font-size: large'>
                                                                        <span style='margin-left: 10px'>
                                                                         Accomplished<input type='radio' value='accomplish' class='agd1-radio'
                                                                         name='agd1-radio' checked/>
                                                                        </span>
                                                                        <span style='margin-left: 10px'> Possibility
                                                                        <input type='radio' value='possible' class='agd1-radio'
                                                                         name='agd1-radio'/></span>
                                                                        <span style='margin-left: 10px'>
                                                                        Project <input type='radio' value='project' class='agd1-radio'
                                                                        name='agd1-radio'/>
                                                                        </span>
                                                                </div>
                                                            <div>
                                                            <div style='border: 3px solid #A4A4A4; padding: 5px'>
                                                                <div id='accomplish-div' class='agd1-tab'>
                                                                    <div>
                                                                        <label>
                                                                            <b>Accomplishment 1:
                                                                            <span class='editable'>Name the Accomplishment</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                          id='popover-text1' data-type='position-vision'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Accomplishment 2:
                                                                            <span class='editable'>Name the Accomplishment</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text2' data-type='position-commitment'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Accomplishment 3:
                                                                            <span class='editable'>Name the Accomplishment</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text3' data-type='position-mission'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Accomplishment 4:
                                                                            <span class='editable'>Name the Accomplishment</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text4' data-type='position-impact'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Accomplishment 5:
                                                                            <span class='editable'>Name the Accomplishment</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text5' data-type='position-declaration'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                               </div>

                                                               <div id='project-div' class='hidden agd1-tab'>
                                                                    <div>
                                                                        <label>
                                                                            <b>Project 1:
                                                                            <span class='editable'>Name the Project</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                          id='popover-text1' data-type='position-values'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Project 2:
                                                                            <span class='editable'>Name the Project</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text2' data-type='position-aesthetics'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Project 3:
                                                                            <span class='editable'>Name the Project</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text3'
                                                                        data-type='position-ethics'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Project 4:
                                                                            <span class='editable'>Name the Project</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text4' data-type='position-politic'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Project 5:
                                                                            <span class='editable'>Name the Project</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                          id='popover-text5' data-type='position-possibility'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div id='possible-div' class='hidden agd1-tab'>
                                                                    <div>
                                                                        <label>
                                                                            <b>Possibility 1:
                                                                            <span class='editable'>Name the Possibility</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                          id='popover-text1' data-type='position-viability'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Possibility 2:
                                                                            <span class='editable'>Name the Possibility</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text2' data-type='position-advantages'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Possibility 3:
                                                                            <span class='editable'>Name the Possibility</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                        id='popover-text3'
                                                                        data-type='position-features'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Possibility 4:
                                                                            <span class='editable'>Name the Possibility</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text4' data-type='position-benefits'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Possibility 5:
                                                                            <span class='editable'>Name the Possibility</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text5' data-type='position-positioning'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            </div>
                                                            </div>


                                                            <div class='col-xs-6'>
                                                            <div class='text-center' style=' font-size: large;'>
                                                                        <span style='margin-left: 10px'>
                                                                         Commitments<input type='radio' value='commit' class='agd2-radio'
                                                                         name='agd2-radio' checked/>
                                                                        </span>
                                                                        <span style='margin-left: 10px;'> Results
                                                                        <input type='radio' value='result' class='agd2-radio'
                                                                         name='agd2-radio'/></span>
                                                                        <span style='margin-left: 10px'>
                                                                        Actions <input type='radio' value='action' class='agd2-radio'
                                                                        name='agd2-radio'/>
                                                                        </span>
                                                                </div>
                                                            <div>
                                                            <div style='border: 3px solid #A4A4A4; padding: 5px;'>
                                                                <div id='commit-div' class='agd2-tab'>
                                                                    <div>
                                                                        <label>
                                                                            <b>Commitment 1:
                                                                            <span class='editable'>Name the Commitment</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                          id='popover-text1' data-type='position-vision'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Commitment 2:
                                                                            <span class='editable'>Name the Commitment</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text2' data-type='position-commitment'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Commitment 3:
                                                                            <span class='editable'>Name the Commitment</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text3' data-type='position-mission'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Commitment 4:
                                                                            <span class='editable'>Name the Commitment</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text4' data-type='position-impact'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Commitment 5:
                                                                            <span class='editable'>Name the Commitment</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text5' data-type='position-declaration'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                               </div>
                                                               <div id='action-div' class='hidden agd2-tab'>
                                                                    <div>
                                                                        <label>
                                                                            <b>Action 1:
                                                                            <span class='editable'>Name the Action</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                          id='popover-text1' data-type='position-values'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Action 2:
                                                                            <span class='editable'>Name the Action</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text2' data-type='position-aesthetics'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Action 3:
                                                                            <span class='editable'>Name the Action</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text3'
                                                                        data-type='position-ethics'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Action 4:
                                                                            <span class='editable'>Name the Action</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text4' data-type='position-politic'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Action 5:
                                                                            <span class='editable'>Name the Action</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                          id='popover-text5' data-type='position-possibility'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div id='result-div' class='hidden agd2-tab'>
                                                                    <div>
                                                                        <label>
                                                                            <b>Result 1:
                                                                            <span class='editable'>Name the Result</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                          id='popover-text1' data-type='position-viability'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Result 1:
                                                                            <span class='editable'>Name the Result</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text2' data-type='position-advantages'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Result 1:
                                                                            <span class='editable'>Name the Result</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                        id='popover-text3'
                                                                        data-type='position-features'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Result 1:
                                                                            <span class='editable'>Name the Result</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text4' data-type='position-benefits'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Result 1:
                                                                            <span class='editable'>Name the Result</span></b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text5' data-type='position-positioning'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            </div>
                                                            </div>
                                                            <div class='col-xs-12' style='margin-top: 10px; text-align: right'>
                                                                <button class='btn btn-primary'>Clear</button>
                                                                <button class='btn btn-primary'>Save</button>
                                                                <button class='btn btn-primary'>View</button>
                                                            </div>">
                                    <span class="glyphicon glyphicon-list-alt"></span>
                                </button>
                                <label>Agent Game Designer</label>
                            </div>

                            <div class="col-xs-2 text-center">
                                <button id="agent-position" data-toggle="popover" data-type="bio"
                                        data-html="true" data-placement="bottom" data-container="body"
                                        class="btn btn-sm btn-default agent-profile-btn"
                                        data-title="<h3>Agent Positioning</h3>
                                                <span style='float: right; margin-left: 10px'>Viability
                                                <input type='radio' value='viability' class='position-radio'
                                                 name='position-radio'/></span>
                                                <span style='float: right; margin-left: 10px'>
                                                Values <input type='radio' value='values' class='position-radio'
                                                name='position-radio'/>
                                                </span>
                                                <span style='float: right; margin-left: 10px'>
                                                Vision <input type='radio' value='vision' class='position-radio'
                                                 name='position-radio' checked/>
                                                </span>"
                                        data-content="<div>
                                                                <div id='vision-div' class='position-tab'>
                                                                    <div>
                                                                        <label>
                                                                            <b>Vision:</b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                          id='popover-text1' data-type='position-vision'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Commitment:</b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text2' data-type='position-commitment'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Mission:</b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text3' data-type='position-mission'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Impact:</b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text4' data-type='position-impact'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Declaration:</b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text5' data-type='position-declaration'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                               </div>
                                                               <div id='values-div' class='hidden position-tab'>
                                                                    <div>
                                                                        <label>
                                                                            <b>Values:</b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                          id='popover-text1' data-type='position-values'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Aesthetics:</b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text2' data-type='position-aesthetics'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Ethics:</b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text3'
                                                                        data-type='position-ethics'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Politic:</b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text4' data-type='position-politic'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Possibility:</b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                          id='popover-text5' data-type='position-possibility'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div id='viability-div' class='hidden position-tab'>
                                                                    <div>
                                                                        <label>
                                                                            <b>Viability:</b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                          id='popover-text1' data-type='position-viability'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Advantages:</b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text2' data-type='position-advantages'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Features:</b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                        id='popover-text3'
                                                                        data-type='position-features'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Benefits:</b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text4' data-type='position-benefits'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                    <div>
                                                                        <label>
                                                                            <b>Positioning:</b>
                                                                        </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                        <textarea class='form-control agent-popup-info'
                                                                         id='popover-text5' data-type='position-positioning'
                                                                        data-container='body' rows='3'
                                                                        ></textarea>
                                                                        <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>">
                                    <span class="glyphicon glyphicon-list-alt"></span>
                                </button>
                                <label>Agent Positioning</label>
                            </div>

                            <div class="col-xs-2 text-center">
                                <button id="agent-challenge"
                                        data-toggle="popover" data-type="project"
                                        class="btn btn-sm btn-default agent-profile-btn"
                                        data-html="true" data-placement="bottom"
                                        data-title="<h3>Agent Project Design</h3>"
                                        data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Mission:</b>
                                                                                </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                            <textarea class='form-control agent-popup-info'
                                                                             id='popover-text1'
                                                                            data-type='project-mission'
                                                                            data-container='body' rows='3'
                                                                            ></textarea>
                                                                            <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                        </div>
                                                                        <div>
                                                                            <label>
                                                                                <b>Objectives:</b>
                                                                            </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                                <textarea class='form-control
                                                                                agent-popup-info' id='popover-text2'
                                                                                data-type='project-objective'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Conditions:</b>
                                                                                </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                                <textarea class='form-control
                                                                                agent-popup-info' id='popover-text3'
                                                                                data-type='project-condition'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Metrics:</b>
                                                                                </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                                <textarea class='form-control
                                                                                agent-popup-info' id='popover-text4'
                                                                                data-type='project-metric'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Milestones:</b>
                                                                                </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                                <textarea class='form-control
                                                                                agent-popup-info' id='popover-text5'
                                                                                data-type='project-milestone'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                            </div>
                                                                        </div>">
                                    <span class="glyphicon glyphicon-list-alt"></span>
                                </button>
                                <label>Agent Project Design</label>
                            </div>

                            <div class="col-xs-2 text-center">
                                <button id="strategic-objective"
                                        data-type="business" data-toggle="popover"
                                        class="btn btn-sm btn-default agent-profile-btn"
                                        data-html="true" data-placement="bottom"
                                        data-title="<h3>Agent Business Plan</h3>"
                                        data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Client Focus:</b>
                                                                                </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                                <textarea class='form-control
                                                                                agent-popup-info' id='popover-text1'
                                                                                 data-type='business-focus'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Team Architecture:</b>
                                                                                </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                                <textarea class='form-control
                                                                                 agent-popup-info' id='popover-text2'
                                                                                 data-type='business-architecture'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Activity Metrics:</b>
                                                                                </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                                <textarea class='form-control
                                                                                 agent-popup-info' id='popover-text3'
                                                                                 data-type='business-activity'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Performance Metrics:</b>
                                                                                </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                                <textarea class='form-control
                                                                                 agent-popup-info' id='popover-text4'
                                                                                 data-type='business-performance'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Results In Time:</b>
                                                                                </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                                <textarea class='form-control
                                                                                 agent-popup-info' id='popover-text5'
                                                                                 data-type='business-result'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                            </div>
                                                                        </div>">
                                    <span class="glyphicon glyphicon-list-alt"></span>
                                </button>
                                <label>Agent Business Plan</label>
                            </div>

                            <div class="col-xs-2 text-center">
                                <button id="transaction-history" data-toggle="popover"
                                        data-html="true" data-placement="bottom" data-type="development"
                                        data-title="<h3>Agent Skill Development</h3>"
                                        class="btn btn-sm btn-default agent-profile-btn"
                                        data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Application Skills Development Plan:</b>
                                                                                </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                                <textarea class='form-control
                                                                                 agent-popup-info' id='popover-text1'
                                                                                 data-type='application-development'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Transactional Skills Development Plan:</b>
                                                                                </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                                <textarea class='form-control
                                                                                 agent-popup-info' id='popover-text2'
                                                                                 data-type='transaction-development'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Communication Skills Development Plan:</b>
                                                                                </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                                <textarea class='form-control
                                                                                 agent-popup-info' id='popover-text3'
                                                                                 data-type='communication-development'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Technology Skills Development Plan:</b>
                                                                                </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                                <textarea class='form-control
                                                                                 agent-popup-info' id='popover-text4'
                                                                                 data-type='technology-development'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Business Management Skills Development Plan:</b>
                                                                                </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                                <textarea class='form-control
                                                                                 agent-popup-info' id='popover-text5'
                                                                                 data-type='business-development'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                            </div>
                                                                        </div>">
                                    <span class="glyphicon glyphicon-list-alt"></span>
                                </button>
                                <label>Agent Skill Development Plan</label>
                            </div>

                            <div class="col-xs-2 text-center">
                                <button id="acquistion-criteria" data-toggle="popover"
                                        data-html="true" data-placement="bottom" data-type="bio"
                                        data-title="<h3>Agent Bio & Resume</h3>"
                                        class="btn btn-sm btn-default agent-profile-btn"
                                        data-content="<div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Biography:</b>
                                                                                </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                                <textarea class='form-control
                                                                                 agent-popup-info' id='popover-text1'
                                                                                 data-type='bio-biography'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Education:</b>
                                                                                </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                                <textarea class='form-control
                                                                                 agent-popup-info' id='popover-text2'
                                                                                 data-type='bio-education'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Experience:</b>
                                                                                </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                                <textarea class='form-control
                                                                                 agent-popup-info' id='popover-text3'
                                                                                 data-type='bio-experience'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Expertise:</b>
                                                                                </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                                <textarea class='form-control
                                                                                 agent-popup-info' id='popover-text4'
                                                                                 data-type='bio-expertise'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                            </div>
                                                                            <div>
                                                                                <label>
                                                                                    <b>Accomplishments:</b>
                                                                                </label>
                                                                        <div class='input-group' style='width: 23.5vw'>
                                                                                <textarea class='form-control
                                                                                 agent-popup-info' id='popover-text5'
                                                                                 data-type='bio-accomplishments'
                                                                                data-container='body' rows='3'
                                                                                ></textarea>
                                                                                <span class='input-group-addon btn
                                                                        btn-secondary stt'><i class='fa fa-microphone'></i>
                                                                        </span>
                                                                        </div>
                                                                            </div>
                                                                        </div>">
                                    <span class="glyphicon glyphicon-list-alt"></span>
                                </button>
                                <label>Agent Bio & Resume</label>
                            </div>
                        </div>
                        <div class="text-center">
                            <div style="margin: 10px">
                                <div class="col-xs-offset-4 col-xs-4">
                                    <label style="color: #5A5A5A; font-size: 25px">Deal Management Dashboard</label>
                                </div>

                                <div class="col-xs-4" style="text-align: right">
                                    <button class="btn btn-primary hidden" id="enable-poi">Show POI</button>
                                    <button class="btn btn-primary hidden" id="disable-poi">Hide POI</button>
                                    <button class="btn btn-primary" id="agent-asset-pics">DEALS</button>
                                    <button class="btn btn-primary" id="agent-asset-map">MAP</button>
                                    <button class="btn btn-primary" id="agent-asset-info">INFO</button>
                                </div>
                            </div>

                            <div class="col-xs-12" id="agent-pics-div" style="border: 5px solid #A4A4A4">
                                <ul class="nav nav-tabs top-buffer" role="tablist" style="padding: 10px; margin: auto;
                 display: flex; justify-content: center;" id="agent-pics-nav">
                                    <li role="presentation" class="active">
                                        <a href="#agent-assets-identified-view" role="tab" data-toggle="tab">Deals
                                            Identified</a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#agent-assets-prospective-view" role="tab" data-toggle="tab">Deals
                                            Proposed</a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#agent-assets-probable-view" role="tab" data-toggle="tab">Deals
                                            Listed</a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#agent-assets-predictable-view" role="tab"
                                           data-toggle="tab">Deals in Contract</a>
                                    </li>
                                    <li role="presentation" data-type="property">
                                        <a href="#agent-assets-completed-view" role="tab"
                                           data-toggle="tab">Deals Completed</a>
                                    </li>
                                </ul>

                                <div class="tab-content">

                                    <div role="tabpanel" class="tab-pane col-xs-12"
                                         id="agent-assets-completed-view">
                                        <div class="col-xs-12">
                                            <table id="agent-completed-table" class="table"
                                                   style="border-collapse: separate; border-spacing: 0 0.5em;"></table>
                                        </div>
                                    </div>

                                    <div role="tabpanel" class="tab-pane col-xs-12" id="agent-assets-predictable-view">
                                        <div class="col-xs-12">
                                            <table id="agent-predictable-table" class="table"
                                                   style="border-collapse: separate; border-spacing: 0 0.5em;"></table>
                                        </div>
                                    </div>

                                    <div role="tabpanel" class="tab-pane col-xs-12" id="agent-assets-probable-view">
                                        <div class="col-xs-12">
                                            <table id="agent-probable-table" class="table"
                                                   style="border-collapse: separate; border-spacing: 0 0.5em;"></table>
                                        </div>
                                    </div>

                                    <div role="tabpanel" class="tab-pane col-xs-12" id="agent-assets-prospective-view">
                                        <div class="col-xs-12">
                                            <table id="agent-prospective-table" class="table"
                                                   style="border-collapse: separate; border-spacing: 0 0.5em;"></table>
                                        </div>
                                    </div>

                                    <div role="tabpanel" class="tab-pane col-xs-12 active"
                                         id="agent-assets-identified-view">
                                        <div class="col-xs-12">
                                            <table id="agent-identified-table" class="table"
                                                   style="border-collapse: separate; border-spacing: 0 0.5em;"></table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 hidden" id="agents-map-div"
                                 style="border: 5px solid #A4A4A4; height: 70vh; padding: 10px">
                            </div>

                            <div class="col-xs-12 hidden" id="agents-info-div"
                                 style="border: 5px solid #A4A4A4; height: 70vh; padding: 10px">
                                <div class="row" id="agent-profile-info">
                                    <ul class="nav nav-tabs top-buffer" role="tablist" style="padding: 10px; margin: auto;
                 display: flex; justify-content: center;" id="agent-profile-nav">
                                        <li role="presentation" data-type="property" class="active"
                                            data-table="homebase_client_asset" data-key="hca_id">
                                            <a href="#agent-property-view" role="tab" data-toggle="tab">Asset</a>
                                        </li>
                                        <li role="presentation" data-type="asset"
                                            data-table="agent_asset_info" data-key="aai_hca_id">
                                            <a href="#agent-asset-view" role="tab" data-toggle="tab">Details</a>
                                        </li>
                                        <li role="presentation" data-type="people"
                                            data-table="agent_asset_info" data-key="aai_hca_id">
                                            <a href="#agent-people-view" role="tab" data-toggle="tab">People</a>
                                        </li>
                                        <li role="presentation" data-type="financial"
                                            data-table="agent_asset_info" data-key="aai_hca_id">
                                            <a href="#agent-financial-view" role="tab" data-toggle="tab">Financial</a>
                                        </li>
                                        <li role="presentation" data-type="legal"
                                            data-table="agent_asset_info" data-key="aai_hca_id">
                                            <a href="#agent-legal-view" role="tab" data-toggle="tab">Legal</a>
                                        </li>
                                        <li role="presentation" data-type="market"
                                            data-table="agent_asset_info" data-key="aai_hca_id">
                                            <a href="#agent-market-view" role="tab" data-toggle="tab">Market</a>
                                        </li>
                                    </ul>

                                    <div class="tab-content profile-readout-tab">

                                        <!-- Property Tab-->
                                        <div role="tabpanel" class="tab-pane col-xs-4 col-xs-offset-4 active"
                                             id="agent-property-view">
                                            <h3 style="text-align: center; padding: 5px"><b>Property Overview</b></h3>
                                            <div class="col-xs-12">
                                                <div>
                                                    <h4 class="col-xs-5">Property Type</h4>
                                                    <div class="col-xs-7" style="padding: 5px;">
                                                        <input type="text" class="form-control agent-asset-input"
                                                               data-col="hca_type"/>
                                                    </div>
                                                </div>
                                                <div>
                                                    <h4 class="col-xs-5">Property Name</h4>
                                                    <div class="col-xs-7" style="padding: 5px;">
                                                        <input type="text" class="form-control agent-asset-input"
                                                               data-col="hca_name"/>
                                                    </div>
                                                </div>
                                                <h4 class="col-xs-5">Region/MSA</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="hca_location"/>
                                                </div>
                                                <div>
                                                    <h4 class="col-xs-5">Address</h4>
                                                    <div class="col-xs-7" style="padding: 5px;">
                                                        <input type="text" class="form-control agent-asset-input"
                                                               data-col="hca_address"/>
                                                    </div>
                                                </div>
                                                <div>
                                                    <h4 class="col-xs-5">City</h4>
                                                    <div class="col-xs-7" style="padding: 5px;">
                                                        <input type="text" class="form-control agent-asset-input"
                                                               data-col="hca_city"/>
                                                    </div>
                                                </div>
                                                <h4 class="col-xs-5">State</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="hca_state"/>
                                                </div>
                                                <h4 class="col-xs-5">Zip</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="hca_zip"/>
                                                </div>
                                            </div>
                                        </div>
                                        <!--// Property Tab-->

                                        <!-- Assets tab -->
                                        <div role="tabpanel" class="tab-pane col-xs-8 col-xs-offset-2"
                                             id="agent-asset-view">
                                            <h3 style="text-align: center; padding: 5px"><b>Asset Details</b></h3>
                                            <div class="col-xs-6">
                                                <div>
                                                    <h4 class="col-xs-5">Year Built</h4>
                                                    <div class="col-xs-7" style="padding: 5px;">
                                                        <input type="text" class="form-control agent-asset-input"
                                                               data-col="aai_built_year"/>
                                                    </div>
                                                </div>
                                                <div>
                                                    <h4 class="col-xs-5">Building Size</h4>
                                                    <div class="col-xs-7" style="padding: 5px;">
                                                        <input type="text" class="form-control agent-asset-input"
                                                               data-col="aai_building_size"/>
                                                    </div>
                                                </div>
                                                <h4 class="col-xs-5">Lot Size</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_lot_size"/>
                                                </div>
                                                <h4 class="col-xs-5">Square Footage</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_avg_sf"/>
                                                </div>
                                                <h4 class="col-xs-5">Rentable SF</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_rentable_sf"/>
                                                </div>
                                                <h4 class="col-xs-5">Useable SF</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_usable_sf"/>
                                                </div>
                                                <h4 class="col-xs-5">Amenities Inside</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_amenities_inside"/>
                                                </div>
                                                <h4 class="col-xs-5">Amenities Outside</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_amenities_outside"/>
                                                </div>
                                            </div>

                                            <div class="col-xs-6">

                                                <h4 class="col-xs-5">Units</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_units"/>
                                                </div>
                                                <h4 class="col-xs-5">Interstate Access</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_interstate_access"/>
                                                </div>
                                                <h4 class="col-xs-5">Parking Ratio</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_parking_ratio"/>
                                                </div>
                                                <h4 class="col-xs-5">Floors</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_floors"/>
                                                </div>
                                                <h4 class="col-xs-5">Sprinkler System</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_sprinkler"/>
                                                </div>
                                                <h4 class="col-xs-5">Expansion</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_expansion"/>
                                                </div>
                                                <h4 class="col-xs-5">Unit Condition</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_unit_condition"/>
                                                </div>
                                                <h4 class="col-xs-5">Grounds Condition</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_ground_condition"/>
                                                </div>
                                            </div>
                                        </div>
                                        <!--// Assets tab -->

                                        <!-- People Tab-->
                                        <div role="tabpanel" class="tab-pane col-xs-4 col-xs-offset-4"
                                             id="agent-people-view">
                                            <h3 style="text-align: center; padding: 5px"><b>People Involved</b></h3>
                                            <div class="col-xs-12">
                                                <div>
                                                    <h4 class="col-xs-5">Owners</h4>
                                                    <div class="col-xs-7" style="padding: 5px;">
                                                        <input type="text" class="form-control agent-asset-input"
                                                               data-col="aai_owner"/>
                                                    </div>
                                                </div>
                                                <div>
                                                    <h4 class="col-xs-5">Ownership</h4>
                                                    <div class="col-xs-7" style="padding: 5px;">
                                                        <input type="text" class="form-control agent-asset-input"
                                                               data-col="aai_ownership"/>
                                                    </div>
                                                </div>
                                                <h4 class="col-xs-5">Management</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_management"/>
                                                </div>
                                                <h4 class="col-xs-5">Attorneys</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_attorney"/>
                                                </div>
                                                <h4 class="col-xs-5">Accountants</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_accountant"/>
                                                </div>
                                                <h4 class="col-xs-5">Investors</h4>
                                                <div class="col-xs-7" style="padding:5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_investor"/>
                                                </div>
                                            </div>
                                        </div>
                                        <!--// People Tab-->

                                        <!-- Financial Tab-->
                                        <div role="tabpanel" class="tab-pane col-xs-4 col-xs-offset-4"
                                             id="agent-financial-view">
                                            <h3 style="text-align: center; padding: 5px"><b>Financial Overview</b></h3>
                                            <div class="col-xs-12">
                                                <div>
                                                    <h4 class="col-xs-5">Occupancy</h4>
                                                    <div class="col-xs-7" style="padding: 5px;">
                                                        <input type="text" class="form-control agent-asset-input"
                                                               data-col="aai_occupancy"/>
                                                    </div>
                                                </div>
                                                <div>
                                                    <h4 class="col-xs-5">Lease Type</h4>
                                                    <div class="col-xs-7" style="padding: 5px;">
                                                        <input type="text" class="form-control agent-asset-input"
                                                               data-col="aai_lease_type"/>
                                                    </div>
                                                </div>
                                                <h4 class="col-xs-5">Rental Rate</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_rental_rate"/>
                                                </div>
                                                <h4 class="col-xs-5">Expenses</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_expenses"/>
                                                </div>
                                                <h4 class="col-xs-5">Cash Flow</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_cash_flow"/>
                                                </div>
                                                <h4 class="col-xs-5">Lender</h4>
                                                <div class="col-xs-7" style="padding:5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_lender"/>
                                                </div>
                                                <h4 class="col-xs-5">Debt Service</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_debt_service"/>
                                                </div>
                                                <h4 class="col-xs-5">Total Debt</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_total_debt"/>
                                                </div>
                                                <h4 class="col-xs-5">Prepayment</h4>
                                                <div class="col-xs-7" style="padding:5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_prepayment"/>
                                                </div>
                                                <h4 class="col-xs-5">CAP Rate</h4>
                                                <div class="col-xs-7" style="padding:5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_cap"/>
                                                </div>
                                            </div>
                                        </div>
                                        <!--// Financial Tab-->

                                        <!-- Legal Tab-->
                                        <div role="tabpanel" class="tab-pane col-xs-4 col-xs-offset-4"
                                             id="agent-legal-view">
                                            <h3 style="text-align: center; padding: 5px"><b>Legal Status</b></h3>
                                            <div class="col-xs-12">
                                                <div>
                                                    <h4 class="col-xs-5">Tenant Status</h4>
                                                    <div class="col-xs-7" style="padding: 5px;">
                                                        <input type="text" class="form-control agent-asset-input"
                                                               data-col="aai_tenant_status"/>
                                                    </div>
                                                </div>
                                                <div>
                                                    <h4 class="col-xs-5">Easements</h4>
                                                    <div class="col-xs-7" style="padding: 5px;">
                                                        <input type="text" class="form-control agent-asset-input"
                                                               data-col="aai_easement"/>
                                                    </div>
                                                </div>
                                                <h4 class="col-xs-5">Environmental</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_environmental"/>
                                                </div>
                                            </div>
                                        </div>
                                        <!--// Legal Tab-->

                                        <!-- Market Tab-->
                                        <div role="tabpanel" class="tab-pane col-xs-4 col-xs-offset-4"
                                             id="agent-market-view">
                                            <h3 style="text-align: center; padding: 5px"><b>Market Issues</b></h3>
                                            <div class="col-xs-12">
                                                <h4 class="col-xs-5">Neighborhood</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_neighborhood"/>
                                                </div>
                                                <h4 class="col-xs-5">Location Rating</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_location_rating"/>
                                                </div>
                                                <h4 class="col-xs-5">Competition</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_competition"/>
                                                </div>
                                                <div>
                                                    <h4 class="col-xs-5">Demographics</h4>
                                                    <div class="col-xs-7" style="padding: 5px;">
                                                        <input type="text" class="form-control agent-asset-input"
                                                               data-col="aai_demographics"/>
                                                    </div>
                                                </div>
                                                <div>
                                                    <h4 class="col-xs-5">Crime</h4>
                                                    <div class="col-xs-7" style="padding: 5px;">
                                                        <input type="text" class="form-control agent-asset-input"
                                                               data-col="aai_crime"/>
                                                    </div>
                                                </div>
                                                <h4 class="col-xs-5">Proximity</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_proximity"/>
                                                </div>
                                                <h4 class="col-xs-5">Access</h4>
                                                <div class="col-xs-7" style="padding: 5px;">
                                                    <input type="text" class="form-control agent-asset-input"
                                                           data-col="aai_access"/>
                                                </div>
                                            </div>
                                        </div>
                                        <!--// Market Tab-->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php $this->partial('views/dialogs/edit_agent_profile.php'); ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBCwpF5xPvzTUNhXuKc-SKw655Pdty5qWs" async
        defer></script>
