<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/setting/custom_fields.js"></script>

<div class="row">
	<div class="col-xs-12">
		<h3>Custom Fields</h3>
	</div>
</div>

<!-- Show the add button. -->
<div class="row">
	<div class="col-xs-12 text-left">
		<button type="button" id="add-cf" class="btn btn-primary bottom-buffer" data-toggle="modal"
		        data-target="#add-cf-dialog">Add Custom Field
		</button>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<!-- Custom fields. -->
		<div class="row" id="cf-list-row">
			<div class="col-xs-12">
				<div id="cf-table-div" class="top-buffer">
					<table class="table table-striped" id="cf-table">
						<thead>
						<tr>
							<th>ID</th>
							<th>Object</th>
							<th>Name</th>
							<th>System Name</th>
							<th>Type</th>
							<th>Type Extras</th>
							<!--
							<th><span class='glyphicon glyphicon-trash'></span></th>
							-->
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- // Custom field list row -->

	</div>
</div>

<!-- Dialogs -->
<?php $this->partial('views/setting/dialogs/add_cf.php'); ?>
<?php //$this->partial('views/setting/dialogs/delete_cf.php'); ?>
