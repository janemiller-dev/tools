<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/setting/dialog/add_cf.js"></script>

<div class="modal fade" id="add-cf-dialog" tabindex="-1" role="dialog" aria-labelledby="add-cf-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<form id="add-cf-form" class="form-horizontal">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h2 id="add-cf-title" class="modal-title">Add New Custom Field</h2>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="object_name">Base Object</label>
						<div class="col-xs-12 col-sm-9">
							<select class="form-control" id="cf-object-name" name="object_name">
								<option value="company">Company</option>
								<option value="contact">Contact</option>
								<option value="property">Property</option>
							</select>
							<p class="form-text text-muted">The object which will have the custom field.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Name</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="cf-name" name="name"/>
							<p class="form-text text-muted">The user visible name of the field.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="sys_name">System Name</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="cf-sys-name" name="sys_name"/>
							<p class="form-text text-muted">The system name field. Must be all lower case and can not
								have spaces.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="type">Type</label>
						<div class="col-xs-12 col-sm-9">
							<select class="form-control" id="cf-type" name="type">
							</select>
						</div>
					</div>

					<!-- Number type specific fields. -->
					<div class="form-group type-specific type-specific-number">
						<label class="col-xs-12 col-sm-3 control-label" for="type">Number of Decimals</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="number-dec" name="number_dec" default="0"/>
							<p class="form-text text-muted">The maximum number of decimals in the number.</p>
						</div>
					</div>
					<div class="form-group type-specific type-specific-number">
						<label class="col-xs-12 col-sm-3 control-label" for="type">Minimum Value</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="number-min" name="number_min"/>
							<p class="form-text text-muted">The minimum value of the number. Leave blank for no
								minimum.</p>
						</div>
					</div>
					<div class="form-group type-specific type-specific-number">
						<label class="col-xs-12 col-sm-3 control-label" for="type">Maximum Value</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="number-max" name="number_max"/>
							<p class="form-text text-muted">The maximum value of the number. Leave blank for no
								maximum.</p>
						</div>
					</div>
					<!-- // Number type specific fields. -->

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>

			</form>
		</div>
	</div>
</div>
