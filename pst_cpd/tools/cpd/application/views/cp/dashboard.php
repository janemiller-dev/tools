<!-- Deal Management Dashboard Instance Listing Page -->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/cp/dashboard.js"></script>

<!-- Page Title -->
<div class="row">
    <div class="col-xs-8 col-xs-offset-2">
        <h2>Content Provider</h2>
    </div>
</div>

<!-- Show the list of CP instances. -->
<div class="row">
    <div class="col-xs-12">

        <!-- DMD instance table -->
        <div class="row" id="cp-component-row">
            <div class="col-xs-8 col-xs-offset-2">
                <div id="cp-component-div" class="top-buffer">
                    <table class="table table-striped" id="cp-component-table">
                        <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th class="text-center">Component Name</th>
                            <th class="text-center">Component Description</th>
                            <th class="text-center">Last Updated</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- // CP instance table -->

    </div>
</div>

<!-- Dialogs -->
<?php $this->partial('views/cpdv1/dialogs/add_cpd.php'); ?>
<?php $this->partial('views/cpdv1/dialogs/delete_dialog.php'); ?>

