<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/tsl/email.css"
      xmlns="http://www.w3.org/1999/html"/>
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/trix/dist/trix.css">

<div class="row full_view_hide">
    <div class="col-xs-12">
        <!-- Back Button-->
        <div class="col-xs-4">
            <a class="btn btn-secondary back-button" style="float: right; padding-right: 25px">
                <img src="http://cpd.powersellingtools.com/wp-content/uploads/2020/01/Component-Arrow-Back.png"
                     style="height: 5vh; width: 70%;" alt="back"></a>
        </div>
        <!--// Back Button-->

        <!--// Collaboration Button-->
        <div class="col-xs-4">
            <h2 id="tsl-profile-heading" style="text-align:center" class="heading">Strategic Profile Builder</h2>
        </div>

        <!-- Collaboration Button-->
        <div class="col-xs-4">
            <a class="btn btn-secondary collab-button" style="float: right; padding-right: 25px">
                <img src="http://cpd.powersellingtools.com/wp-content/uploads/2020/01/Collaboration-Arrows-2.png"
                     style="height: 5vh; width: 70%;" alt="collaboration">
            </a>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="col-lg-3 col-xs-12 col-sm-12 col-md-3 script-box">
            <label class="col-xs-12" style="text-align: center">Client Type:</label>
            <select class="form-control profile-menu" id="client-type">
                <option value="r">Radish</option>
                <option value="w">Wheat</option>
                <option value="t">Tree</option>
                <option value="d">Developer</option>
                <option value="i">Institutional</option>
            </select>
        </div>
        <div class="col-lg-3 col-xs-12 col-sm-12 col-md-3 script-box">
            <label class="col-xs-12" style="text-align: center">Profile Delivery:</label>
            <select class="form-control profile-menu" id="client-type">
                <option value="r">Online Access Link</option>
                <option value="w">Email Attachment</option>
                <option value="t">Web Site Download</option>
                <option value="d">Live Phone Call – No Send</option>
                <option value="i">In-Person Meeting – No Send</option>
            </select>
        </div>
        <div class="col-lg-3 col-xs-12 col-sm-12 col-md-3 script-box">
            <label class="col-xs-12" style="text-align: center">Profile Type:</label>
            <select class="form-control profile-menu" id="profile-type">
                <option value="1">Type 1 – Owner Profile</option>
                <option value="2">Type 2 – Buyer Profile</option>
            </select>
        </div>
        <!--Select box for profile outlines-->
        <div class="col-lg-3 col-xs-12 col-sm-12 col-md-3 script-box">
            <label class="col-xs-12" style="text-align: center">Profile Section:</label>
            <select class="form-control" id="profile-phase">
            </select>
        </div>
    </div>
</div>

<hr>
<!--Container for outline.-->
<div class="row">
    <div class="col-xs-15 full_view_hide">
        <div class="sidebar" id="outline-container" style="padding-left: 2px">
            <p id="tsl-outline">
            </p>
        </div>
    </div>
    <div class="col-xs-60 profile_container full_view_hide">
        <div class="row">
            <div class="col-md-12 container">
                <div class="sidebar" id="canvas-container"></div>
            </div>
        </div>

    </div>
    <div class="col-xs-15 full_view_hide">
        <div class="sidebar" id="container-listing" style="word-break: break-all;">
        </div>
    </div>

    <div class="col-xs-12 hidden profile-viewer">
        <a class="btn btn-secondary collab-back-button" style="float: right; padding-right: 25px">
            <img src="https://cpd.powersellingtools.com/wp-content/uploads/2020/01/Component-Arrow-Back.png"
                 style="height: 5vh; width: 70%;" alt="back"></a>
        <div class="row">
            <h2 style="text-align: center"><b>STRATEGIC OWNER PROFILE RECORDER</b></h2>
            <p style="text-align: center">You may either compose in text or use speech to text.
                Just click on microphone and then speak.</p>
            <form id="profile-record-view">
            </form>
        </div>
    </div>
</div>

<!-- Buyer Profile Buttons -->
<div class="row" style="margin-top: 1vh" id="profile_buttons">
    <div class="col-xs-15" align="center">
        <button id="view-profile-outline" class="btn btn-secondary full_view_hide"
                data-toggle="modal" data-target="#view-outline-dialog">List
            <i class="fa fa-list"></i>
        </button>
        <button id="edit-profile-outline" class="btn btn-secondary full_view_hide">Edit
            <i class="fa fa-pencil"></i></button>
        <button id="save-profile-outline" class="btn btn-secondary full_view_hide">Save
            <i class="fa fa-check"></i></button>
    </div>

    <div class="col-xs-60" align="center">
        <button id="compose-profile" class="btn btn-secondary full_view_hide">Compose <i class="fa fa-plus"></i>
        </button>
        <button id="read-profile" data-toggle="modal" data-target="#view-profile-reader-dialog"
                class="btn btn-secondary profile-viewer hidden">Read <i class="fa fa-eye"></i>
        </button>
        <button id="clear-profile" class="btn btn-secondary">Clear <i class="fa fa-plus"></i>
        </button>
        <button id="save-profile-question" class="btn btn-secondary full_view_hide">Save<i class="fa fa-check"></i></button>
        <button id="save-profile" class="btn btn-secondary profile-viewer hidden">Save
            <i class="fa fa-check"></i></button>
        <button id="save-profile" class="btn btn-secondary profile-viewer hidden">Save As New
            <i class="fa fa-check"></i></button>
        <button id="record-profile" class="btn btn-secondary">Record <i class="fa fa-reply"></i></button>
        <button id="propagate-profile" class="btn btn-secondary">Propagate <i class="fa fa-arrows"></i></button>
        <button id="send-profile" class="btn-secondary btn">Send <i class="fa fa-paper-plane"></i></button>
    </div>

    <div class="col-xs-15" align="center">
        <button id="alert-profile" class="btn btn-secondary full_view_hide">Alert <i
                    class="fa fa-bell"></i></button>
        <button id="archive-profile" class="btn btn-secondary full_view_hide">Archive <i
                    class="fa fa-archive"></i></button>
        <button id="delete-profile" class="btn btn-secondary full_view_hide">Delete <i
                    class="fa fa-trash"></i></button>
    </div>
</div>

<script src="<?php echo $this->basepath; ?>resources/trix/dist/trix.js"></script>
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/profile.js"></script>

<?php $this->partial('views/tsl/dialogs/send_survey.php'); ?>
<?php //$this->partial('views/tsl/dialogs/view_promo_focus_dialog.php'); ?>
<?php $this->partial('views/tsl/dialogs/delete_profile.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_survey_outline.php'); ?>
<?php $this->partial('views/tsl/dialogs/delete_profile_outline.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_profile_reader.php'); ?>


