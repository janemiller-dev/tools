<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/tsl/page.css"/>
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/trix/dist/trix.css">

<div class="row full-view-hide">
    <div class="col-xs-12">
        <!-- Back Button-->
        <div class="col-xs-4">
            <a class="btn btn-secondary back-button" style="float: right; padding-right: 25px">
                <img src="http://cpd.powersellingtools.com/wp-content/uploads/2020/01/Component-Arrow-Back.png"
                     style="height: 5vh; width: 70%;" alt="back"></a>
        </div>
        <!--// Back Button-->

        <div class="col-xs-4">
            <h2 id="tsl-survey-heading" style="text-align:center" class="heading">Promo Builder</h2>
        </div>

        <!-- Collaboration Button-->
        <div class="col-xs-4">
            <button class="btn btn-secondary collab-button has-popover" style="float: right; padding-right: 25px"
                    id="collab-button" data-toggle="popover" data-html="true" data-placement="left"
                    data-title="Marketing Campaign Overlay.">
                <img src="https://cpd.powersellingtools.com/wp-content/uploads/2020/01/Collaboration-Arrows-2.png"
                     style="height: 5vh; width: 70%;" alt="collaboration">
            </button>
        </div>
        <!--// Collaboration Button-->
    </div>

    <!-- Top drop downs -->
    <div class="col-xs-12" id="page-header">
    </div>
    <!-- Top drop downs -->

</div>
<hr>
<!--Container for outline.-->
<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-15 full-view-hide">
            <div class="sidebar" id="outline-container" style="padding-left: 2px">
                <p id="tsl-outline">
                </p>
            </div>
        </div>
        <div class="col-xs-60 container sidebar promo-container"
             style="padding: 0px; overflow: hidden !important; margin: 0 1% 0 1%">
            <h4 class="main-con-header" style="margin-bottom: 0 !important;">Promo Composition</h4>
            <form id="compose-promo-form" class="form-horizontal promo-form" style="padding: 10px">
                <input type="hidden" id="question_length" name="question_length">
                <input type="hidden" id="promo_type" name="promo-type">
                <input type="hidden" id="promo_id" name="promo-id">
            </form>
        </div>
        <div class="col-xs-15 full-view-hide">
            <div id="container-listing" style="word-break: break-all;">
            </div>
        </div>
    </div>
</div>

<!-- Promotional Buttons -->
<div style="margin-top: 1vh">
    <div class="col-xs-15" align="center">
        <button id="view-promo-outline" class="btn btn-secondary has-tooltip full-view-hide" data-toggle="popover"
                data-container="body" data-html="true" title="Promo Outlines List." data-placement="top">List <i
                    class="fa fa-list"></i>
            </span>
        </button>
        <button id="edit-outline" class="btn btn-secondary full-view-hide">Edit <i class="fa fa-pencil"></i></button>
        <button id="save-promo-outline" class="btn btn-secondary full-view-hide">Save <i class="fa fa-check"></i>
        </button>
    </div>

    <div class="col-xs-60" align="center" id="collab-button-row">
        <!--        <div class="col-xs-6" align="left">-->
        <button id="compose-promo" class="btn btn-secondary has-tooltip" title="Compose a new Promo Version.">
            Compose
            <i class="fa fa-plus"></i></button>
        <button id="clear-promo" class="btn btn-secondary has-tooltip" title="Clear Promo textarea.">
            Clear
            <i class="fa fa-eraser"></i></button>
        <button type="button" id="save-promo-content" class="btn btn-secondary has-tooltip full-view-hide"
                title="Saves Promo Content.">Save <i class="fa fa-check"></i>
        </button>
        <button type="button" id="save-new-promo-content" class="btn btn-secondary has-tooltip full-view-hide"
                title="Saves Promo Content.">Save As New<i class="fa fa-check"></i>
        </button>

        <!--        <button id="read-promo" class="btn btn-secondary has-tooltip" title="Reading view for a Promo Version."-->
        <!--                data-toggle="modal" data-target="#read-promo-dialog">Read-->
        <!--            <i class="fa fa-eye"></i></button>-->
        <button id="promo-collab" class="btn btn-secondary">Collaborate <i class="fa fa-expand"></i></button>
        <button id="propagate-promo" class="btn btn-secondary promo-button has-tooltip"
                title="Propagates Promo Composer Content.">Propagate <i class="fa fa-exchange"></i></button>
        <button class="btn-secondary btn promo-button has-tooltip send-promo" id="send-promo" data-type="pages"
                title="Sends Promo Email.">Send <i class="fa fa-envelope"></i>
        </button>
    </div>

    <div class="col-xs-15" align="center">
<!--        <button class="btn btn-secondary has-tooltip full-view-hide add-to-alert" id="add-alert-promo" data-type="Promo"-->
        <!--                title="Adds Promo instance to alerts list.">Recurrence <i class="fa fa-plus-square-o"></i></button>-->
        <button class="btn btn-secondary has-tooltip full-view-hide add-to-alert" id="add-alert-promo" data-type="Promo"
                title="Adds Promo instance to alerts list.">Alert <i class="fa fa-plus-square-o"></i></button>
        <button class="btn btn-secondary has-tooltip full-view-hide" id="view-promo-archive"
                title="Lists all Archived Promo components." data-toggle="modal"
                data-target="#view-promo-archive-dialog">List <i class="fa fa-list"></i></button>
        <button class="btn btn-secondary has-tooltip full-view-hide" id="add-promo-archive"
                title="Adds a Promo version to Archive list.">Archive <i class="fa fa-archive"></i></button>
    </div>
    <!-- // Promotional Buttons -->

    <!-- Default Promo Content-->
    <div id="default_promo_content" class="hidden">
        <p align="center"><span style="font-family: 'Century Gothic'">
                <span><strong>Default Promo Builder</strong></span></span>
        </p>
        <p><span>
                <span style="font-family: gothicb;">
                    <strong>Market Downturn Looms. The Question is Are You Ready</strong>
                </span>
            </span>
        </p>
        <p><span>
                <span>For many property owners, a downsize in the economy can impact the
                    value of their property greatly. All this hurts people who, once in a panic, make choices that make
                    their financial situation worse. As cash flow shrinks the viability of an investment faces serious
                    risks. The chances of legal challenges emerge more readily as the entire complexion of the market
                    changes.
                </span>
            </span>
        </p>
        <p><span style="font-family: gothicb;"><strong>Emotional Connection</strong></span></p>
        <p>
            <span>
                <span>Owners riding high on the pre-downturn good times fail to get prepared
                    and easily fall into a tough situation unprepared to protect their ability to execute a strategy.
                    The problems they failed to address now bog them down so moving quickly to evade damage to value,
                    cash flow and ROI is difficult. In times of change, new opportunities emerge but these owners&rsquo;
                    hands are tied, and many miss out on what could have a great opportunity. The most devastating
                    result is the owner's ability to get from where they are currently to where they would like to be
                    in the future now elides them.
                </span>
            </span>
        </p>
        <p><span style="font-family: gothicb;"><strong>Inside Scoop</strong></span></p>
        <p>
            <span>
                <span>The evidence is overwhelming,
                    yet many fail to pull their head out of the sand in time. Currently, 60 % of executives of major
                    corporations foresee a downturn in twelve to eighteen months as do most major financial analysts.
                    They only argue over how deep and how soon. Many owners end up trapped in a post-downturn situation
                    that they cannot escape from financial and legal problems that can last more than a decade. The
                    challenge is getting ready and knowing how to minimize the downside, maximize one's ability to ride
                    it out and even take advantage of it. Our remedy is to give you the knowledge you need and help you
                    with a strategy for going forward. At this point, our advisory service comes into play. It involves
                    a series of meetings, an in-depth analysis of your situation and regular updates as things change.
                </span>
            </span>
        </p>
        <p>
            <span style="font-family: gothicb;">
                <strong>Compelling Offer</strong>
            </span>
        </p>
        <p>
            <span>
                <span>Our offer is simple, and it stands whether or not we work together now
                    or in the future. We will first give you information about the coming changes that you will not get
                    anywhere else. We will sit down with you and learn about your situation so we can help you fully
                    understand how our information applies to your specific situation and how the coming changes will
                    affect your situation. Then we will develop a Strategic Analysis that will become the basis of a
                    strategy for going forward, and on a continuing basis, we will give you regular updates on how
                    things are changing. Ultimately, if and when a transaction is required, we will both be more
                    prepared to step in and move quickly if that is necessary.
                </span>
            </span>
        </p>
        <p><span style="font-family: gothicb;"><strong>Bold Promise</strong></span></p>
        <p>
            <span>
                <span>We promise you will be better
                    prepared to face the effects of a downturn when it does come to fruition. You will have at all
                    times certainty about the process and the outcomes you can expect. We will make sure you have
                    clarity as to your available options and the consequences of each of these choices. The courage
                    that comes with a clear roadmap to accomplishing their project. Because we will be working in
                    full collaboration with you, you will have complete confidence in knowing we stand by you through
                    the process.
                </span>
            </span>
        </p>
    </div>
    <!-- // Default Promo Content-->

    <script src="<?php echo $this->basepath; ?>resources/trix/dist/trix.js"></script>
    <script src="<?php echo $this->basepath; ?>resources/app/js/tsl/page.js"></script>

    <?php $this->partial('views/tsl/dialogs/send_promo.php'); ?>
    <?php $this->partial('views/tsl/dialogs/view_pdf.php'); ?>
    <?php $this->partial('views/tsl/dialogs/delete_promo.php'); ?>
    <?php $this->partial('views/tsl/dialogs/view_promo_outline.php'); ?>
    <?php $this->partial('views/tsl/dialogs/view_background.php'); ?>
    <?php $this->partial('views/tsl/dialogs/view_promo_focus_dialog.php'); ?>
    <?php $this->partial('views/tsl/dialogs/view_criteria.php'); ?>
    <script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>
