<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/tsl/email.css"
      xmlns="http://www.w3.org/1999/html"/>
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/trix/dist/trix.css">

<div class="row full_view_hide">
    <div class="col-xs-12">
        <!-- Back Button-->
        <div class="col-xs-4">
            <a class="btn btn-secondary back-button" style="float: right; padding-right: 25px">
                <img src="http://cpd.powersellingtools.com/wp-content/uploads/2020/01/Component-Arrow-Back.png"
                     style="height: 5vh; width: 70%;" alt="back"></a>
        </div>
        <!--// Back Button-->

        <div class="col-xs-4">
            <h2 id="tsl-update-heading" style="text-align:center" class="heading">Update Builder</h2>
        </div>

        <!-- Collaboration Button-->
        <div class="col-xs-4">
            <a class="btn btn-secondary collab-button" style="float: right; padding-right: 25px">
                <img src="http://cpd.powersellingtools.com/wp-content/uploads/2020/01/Collaboration-Arrows-2.png"
                     style="height: 5vh; width: 70%;" alt="collaboration">
            </a>
        </div>
        <!--// Collaboration Button-->
    </div>
    <div class="col-xs-12">
        <div class="col-lg-3 col-xs-12 col-sm-12 col-md-3 script-box">
            <label class="col-xs-12" style="text-align: center">Update Frequency:</label>
            <select class="form-control update-menu" id="update-frequency">
                <option value="1">Frequency 1 – Weekly</option>
                <option value="2">Frequency 2 – Monthly</option>
                <option value="3">Frequency 3 – Quarterly</option>
                <option value="4">Frequency 4 – Yearly</option>
            </select>
        </div>
        <div class="col-lg-3 col-xs-12 col-sm-12 col-md-3 script-box">
            <label class="col-xs-12" style="text-align: center">Update Delivery:</label>
            <select class="form-control update-menu" id="client-type">
                <option value="v">Verbal</option>
                <option value="e">Email</option>
                <option value="s">Snail Mail</option>
                <option value="h">Handout</option>
                <option value="c">Combo</option>
            </select>
        </div>
        <div class="col-lg-3 col-xs-12 col-sm-12 col-md-3 script-box">
            <label class="col-xs-12" style="text-align: center">Update Type:</label>
            <select class="form-control update-menu" id="update-type">
                <option value="1">Market Update</option>
                <option value="2">Comps Update</option>
                <option value="3">Demand Update</option>
                <option value="4">Operations Update</option>
                <option value="5">Value Update</option>
            </select>
        </div>
        <!--Select box for update outlines-->
        <div class="col-lg-3 col-xs-12 col-sm-12 col-md-3 script-box">
            <label class="col-xs-12" style="text-align: center">Update Page:</label>
            <select class="form-control update-menu" id="update-phase">
                <option value="1">Page 1 - Lead Page</option>
                <optgroup label="Page 2 – Status Page">
                    <option value="2">Phase 1 - Macro Trends</option>
                    <option value="2">Phase 2 - Local Developments</option>
                    <option value="2">Phase 3 - Emerging Example</option>
                </optgroup>
                <optgroup label="Page 3 – Analysis Page">
                    <option value="3">Phase 4 - Evolving Options</option>
                    <option value="3">Phase 5 - Recommendations</option>
                </optgroup>
                <option value="4">Page 4 - Last Page</option>
            </select>
        </div>
    </div>
</div>

<hr>
<!--Container for outline.-->
<div class="row">
    <div class="col-xs-3 full_view_hide">
        <div class="sidebar" id="outline-container" style="padding-left: 2px">
            <p id="tsl-outline">
            </p>
        </div>
    </div>
    <div class="col-md-7 update_container">
        <div class="row">
            <div class="col-md-12 container">
                <div class="sidebar" id="canvas-container"></div>
            </div>
        </div>

    </div>
    <div class="col-md-2 full_view_hide">
        <div class="sidebar" id="container-listing" style="word-break: break-all;">
        </div>
    </div>
</div>

<!-- Buyer Update Buttons -->
<div class="row" style="margin-top: 1vh" id="update_buttons">
    <div class="col-xs-3" align="center">
        <button id="view-update-outline" class="btn btn-secondary full_view_hide"
                data-toggle="modal" data-target="#view-outline-dialog">List
            <i class="fa fa-list"></i>
        </button>
        <!--        <button id="save-update-outline" class="btn btn-secondary full_view_hide">Save-->
        <!--            <i class="fa fa-check"></i></button>-->
    </div>

    <div class="col-xs-7" align="center">
        <button id="compose-update" class="btn btn-secondary full_view_hide">Compose <i class="fa fa-plus"></i></button>
        <button id="expand-update" class="btn btn-secondary">Expand <i class="fa fa-arrows""></i></button>
        <button id="save-update" class="btn btn-secondary">Save <i class="fa fa-check"></i></button>
    </div>

    <div class="col-xs-2" align="center">
        <button id="delete-update" class="btn btn-secondary full_view_hide">Delete <i
                    class="fa fa-trash"></i></button>
        <button id="send-update" class="btn-secondary btn  full_view_hide">Send <i
                    class="fa fa-envelope"></i></button>
    </div>
</div>

<script src="<?php echo $this->basepath; ?>resources/trix/dist/trix.js"></script>
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/updates.js"></script>

<?php $this->partial('views/tsl/dialogs/send_update.php'); ?>
<?php ////$this->partial('views/tsl/dialogs/view_promo_focus_dialog.php'); ?>
<?php $this->partial('views/tsl/dialogs/delete_update.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_survey_outline.php'); ?>
<?php $this->partial('views/tsl/dialogs/delete_update_outline.php'); ?>


