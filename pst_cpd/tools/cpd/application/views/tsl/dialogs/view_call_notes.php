<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/view_call_notes.js"></script>

<div class="modal fade" id="view-call-notes-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="message-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="view-notes-title" class="modal-title">Call Back Records</h2>
            </div>
            <div class="modal-body">

                <!-- Show the list of notes. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- List Notes -->
                        <div class="row" id="call-notes-row">
                            <div class="col-xs-12">
                                <div id="view-notes-div" class="top-buffer">
                                    <!-- Date Stamp Field -->
                                    <div>
                                        <label class="control-label col-xs-2 date-label">Date</label>
                                        <div class="col-xs-10" style="margin-bottom: 5px">
                                            <input type="datetime" class="form-control date call-notes-date" data-seq="0"
                                                   id="notes-date0" disabled>
                                        </div>
                                    </div>
                                    <!--// Date Stamp Field -->

                                    <!-- Topic Text Field -->
                                    <label class="control-label col-xs-2 topic-label">Topic</label>
                                    <div class="col-xs-10" style="margin-bottom: 5px">
                                        <input type="text" class="form-control notes notes-topic" data-name="tn_topic"
                                               data-seq="0" id="notes-topic0">
                                    </div>
                                    <!--// Topic Text Field -->

                                    <!-- Notes Text Area -->
                                    <label class="control-label col-xs-2 notes-label">Notes</label>
                                    <div class="col-xs-10" style="margin-bottom: 5px">
                                        <textarea class="form-control notes-input notes" data-name="tn_content"
                                                  data-seq="0" id="notes_content0" rows="3"></textarea>
                                    </div>
                                    <!--// Notes Text Area -->

                                    <!-- Action Field -->
                                    <div>
                                        <label class="control-label col-xs-2 date-label">Action</label>
                                        <div class="col-xs-10" style="margin-bottom: 5px">
                                            <input type="datetime" class="form-control notes" data-seq="0"
                                                   id="notes-action0" data-name="tn_action">
                                        </div>
                                    </div>
                                    <!--// Action Field -->

                                    <!-- Date Stamp Field -->
                                    <div>
                                        <label class="control-label col-xs-2 date-label">By When</label>
                                        <div class="col-xs-10" style="margin-bottom: 5px">
                                            <input type="datetime" class="form-control next-call notes" data-seq="0"
                                                   id="next-call-date0" data-name="tn_next_call">
                                        </div>
                                    </div>
                                    <!--// Date Stamp Field -->
                                </div>
                            </div>
                        </div>
                        <!-- // List Notes -->

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="add-notes">Add Record</button>
                <button type="button" class="btn btn-primary close-button" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

