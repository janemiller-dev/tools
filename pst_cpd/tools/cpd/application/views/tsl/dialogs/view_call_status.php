<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

    <script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/view_call_status.js"></script>

    <div class="modal fade" id="view-call-status-dialog" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="view-call-status-label" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <h2 id="view-call-status-title" class="modal-title">Call Status</h2>
                    <button type="button" id="add-call-status-button" class="btn btn-primary bottom-buffer"
                            data-toggle="modal" data-target="#add-call-status-dialog" data-backdrop="static">
                        Add New Call Status
                    </button>

                </div>

                <div class="modal-body">

                    <!-- Show the list of promos. -->
                    <div class="row">
                        <div class="col-xs-12">

                            <!-- Deal Promo table -->
                            <div class="row" id="call-status-row">
                                <div class="col-xs-12">
                                    <div id="call-status-div" class="top-buffer">
                                        <table class="table table-striped" id="call-status-table">
                                            <thead>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Call Status</th>
                                                <th class="text-center"><span class="glyphicon glyphicon-trash"></span>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php $this->partial('views/tsl/dialogs/add_call_status.php'); ?>
<?php $this->partial('views/tsl/dialogs/delete_call_status.php'); ?>