<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/message.js"></script>

<div class="modal fade" id="send-message-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="message-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="view-promo-title" class="modal-title">Client Name</h2>
                <button type="button" id="add-message-button" class="btn btn-primary bottom-buffer" data-toggle="modal"
                        data-backdrop="static">Add New Message
                </button>

            </div>

            <div class="modal-body">

                <!-- Show the list of promos. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- Deal Promo table -->
                        <div class="row" id="deal-promo-row">
                            <div class="col-xs-12">
                                <p>These are scripted message. You can edit this message using the textbox below.</p>
                                <div id="deal-promo-div" class="top-buffer">
                                    <table class="table" id="message-table">
                                        <thead>
                                        <tr>
                                            <th class="text-center"><span class="glyphicon glyphicon-check"></span></th>
                                            <th>Message</th>
                                            <th>Last Used</th>
                                            <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><input type="checkbox" class="form-control"></td>
                                            <td class="editable" data-type="textarea">
                                            </td>
                                            <td>2018-05-29 15:41:01</td>
                                            <td>
                                                <button type="button" class="btn btn-default"><span
                                                            class="glyphicon glyphicon-trash"></span></button>
                                            </td>
                                        </tr>
                                        <tr bgcolor="#D3D3D3">
                                            <td><input type="checkbox" class="form-control"></td>
                                            <td class="editable" data-type="textarea">
                                            </td>
                                            <td>2018-05-29 15:41:01</td>
                                            <td>
                                                <button type="button" class="btn btn-default"><span
                                                            class="glyphicon glyphicon-trash"></span></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="form-control"></td>
                                            <td class="editable" data-type="textarea">
                                            </td>
                                            <td>2018-05-29 15:41:01</td>
                                            <td>
                                                <button type="button" class="btn btn-default"><span
                                                            class="glyphicon glyphicon-trash"></span></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="form-control"></td>
                                            <td class="editable" data-type="textarea">
                                            </td>
                                            <td>2018-05-29 15:41:01</td>
                                            <td>
                                                <button type="button" class="btn btn-default"><span
                                                            class="glyphicon glyphicon-trash"></span></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><input type="checkbox" class="form-control"></td>
                                            <td class="editable" data-type="textarea">
                                            </td>
                                            <td>2018-05-29 15:41:01</td>
                                            <td>
                                                <button type="button" class="btn btn-default"><span
                                                            class="glyphicon glyphicon-trash"></span></button>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- Deal Promo table -->
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>