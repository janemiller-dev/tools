<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/add_promo_focus.js"></script>

<div class="modal fade" id="add-promo-focus-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="add-promo-focus-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="add-promo-focus-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-promo-focus-title" class="modal-title">Add Promotional Focus</h2>
                </div>

                <div class="modal-body modal-small">

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-4 control-label" for="promo-focus-name">Promo Focus Name</label>
                        <div class="col-xs-12 col-sm-8">
                            <input type="text" class="form-control" id="promo-focus-name" name="promo_focus_name"/>
                            <p class="form-text text-muted">Enter the Name of the Promotional Focus.</p>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="close-add-promo-focus-dialog" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
