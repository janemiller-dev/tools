<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/view_action.js"></script>

<div class="modal fade" id="view-action-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="message-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="view-action-title" class="modal-title">Next Actions</h2>
            </div>
            <div class="modal-body">

                <!-- Show the list of action. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- List Notes -->
                        <div class="row" id="action-row">
                            <div class="col-xs-12">
                                <div id="view-action-div" class="top-buffer">
                                    <!-- Date field-->
                                    <label class="control-label col-xs-2">By When</label>
                                    <div class="col-xs-10" style="margin-bottom: 5px">
                                        <input class="form-control action tsl-date" data-seq="0"
                                               id="action_date0" disabled>
                                    </div>
                                    <!-- Date field-->

                                    <!-- Action Text Field -->
                                    <label class="control-label col-xs-2 action-label">Action</label>
                                    <div class="col-xs-10" style="margin-bottom: 5px">
                                        <textarea class="form-control action-input action" data-name="ta_content"
                                                  data-seq="0" id="action_content0" rows="3" style="width: 100%;"></textarea>
                                    </div>
                                    <span class="fa fa-trash delete-action" id="delete-action0"
                                          style="position: absolute; right: 10px;" data-seq="0"></span>
                                    <!--// Action Text Field -->

                                    <!-- By Whom Select-->
                                    <label class="control-label col-xs-2">By Whom</label>
                                    <div class="col-xs-10" style="margin-bottom: 5px">
                                        <select class="form-control action-whom action" data-seq="0" id="action_whom0"
                                                data-name="ta_whom" style="width: 100%;"></select>
                                    </div>
                                    <!--// By Whom Select-->

                                    <!-- By When Select-->
                                    <label class="control-label col-xs-2">By When</label>
                                    <div class="col-xs-10" style="margin-bottom: 5px">
                                        <input class="form-control action-when action tsl-date" data-seq="0" id="action_when0"
                                               data-name="ta_when">
                                    </div>
                                    <!-- By When Select-->
                                </div>
                            </div>
                        </div>
                        <!-- // List Notes -->

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="add-action">Add Action</button>
                <button type="button" class="btn btn-primary close-button" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

