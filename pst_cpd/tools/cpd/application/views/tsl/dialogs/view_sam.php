<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/tsl/view_sam.css"/>
<!--<script src="--><?php //echo $this->basepath; ?><!--resources/app/js/tsl/dialogs/view_sam.js"></script>-->

<div class="modal fade" id="view-sam-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-h4ledby="message-h4" aria-hidden="true">
    <div class="modal-dialog sam-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-h4="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h3 id="view-sam-title" class="modal-title">Call Session Manager</h3>
            </div>

            <div class="modal-body">
                <div class="text-center">
                    <div class="col-xs-4">
                        <h4>Start Time</h4>
                        <input type="text" disabled id="start-time" class="form-control"/>
                    </div>
                    <div class="col-xs-4">
                        <h4>End Time</h4>
                        <input type="text" disabled id="end-time" class="form-control"/>
                    </div>
                    <div class="col-xs-4">
                        <h4>Duration</h4>
                        <input type="text" disabled id="duration" class="form-control"/>
                    </div>
                </div>
                <div class="text-center sam-buttons">
                    <div class="col-xs-6">
                        <h4>Start Session</h4>
                        <input type="radio" name="sam-control" value="start">
                    </div>
                    <div class="col-xs-6">
                        <h4>End Session</h4>
                        <input type="radio" name="sam-control" value="stop">
                    </div>
                    <div class="col-xs-6">
                        <h4>Pause Session</h4>
                        <input type="radio" name="sam-control" value="pause">
                    </div>
                    <div class="col-xs-6">
                        <h4>Restart Session</h4>
                        <input type="radio" name="sam-control" value="restart">
                    </div>
                </div>
<!--                <div class='col-xs-4 text-center' style='margin-bottom: 2px'>-->
<!--                    <button class='btn btn-sm' id='start-call-session'><i class='fa fa-check'></i></button>-->
<!--                </div>-->
<!--                <div class='col-xs-8' style='margin-bottom: 2px; border-right: 1px solid #e1c8c8;'>-->
<!--                    <h4>End Call Session</h4>-->
<!--                </div>-->
<!--                <div class='col-xs-4 text-center' style='margin-bottom: 2px'>-->
<!--                    <button class='btn btn-sm' id='end-call-session'><i class='fa fa-times'></i></button>-->
<!--                </div>-->
<!--                <div class='col-xs-8' style='margin-bottom: 2px; border-right: 1px solid #e1c8c8;'>-->
<!--                    <h4>Default Per Call Duration (Mins)</h4>-->
<!--                </div>-->
<!--                <div class='col-xs-4 text-center' style='margin-bottom: 2px'>-->
<!--                    <input type='text' class='form-control' style='min-width: 0;' id='session-duration'>-->
<!--                </div>-->
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>