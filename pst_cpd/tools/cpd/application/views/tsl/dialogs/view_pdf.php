<!-- Script for pdf js -->
<script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/view_pdf.js"></script>
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/tsl/view_pdf.css"/>

<div class="modal fade" id="view-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="message-label" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="view-title" class="modal-title"></h2>
                <div id="progress">
                    <div class="bar" style="width: 0%;"></div>
                </div>
            </div>

            <!-- Previous and Next button.-->
            <div align="center">
                <button id="prev" class="btn btn-primary prev" style="background: #eee; color: black">Back</button>
                <button id="next" class="btn btn-primary next" style="background: #eee; color: black">Next</button>&nbsp; &nbsp;
                <span>Page: <span id="page_num"></span> / <span id="page_count"></span></span>
            </div>

            <div class="modal-body">

                <!-- Show the Promo Docs. -->
                <div class="row" align="center">
                    <div class="col-xs-12 pdf-canvas-container">
                        <!-- Canvas for the PDFs -->
                        <canvas id="pdf-canvas"></canvas>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
