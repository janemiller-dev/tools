<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

    <div class="modal fade" id="view-agent-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="view-agent-label"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <h2 id="view-agent-title" class="modal-title">Prepared By Name</h2>
                    <button type="button" id="add-agent-button" class="btn btn-primary bottom-buffer"
                            data-target="#add-agent-dialog" data-toggle="modal" data-backdrop="static">Add New Name
                    </button>

                </div>

                <div class="modal-body">

                    <!-- Show the list of promos. -->
                    <div class="row">
                        <div class="col-xs-12">

                            <!-- Deal Promo table -->
                            <div class="row" id="agent-row">
                                <div class="col-xs-12">
                                    <p id="no-agent">You have no prepared by name. To create one, use
                                        the Add Name button.</p>
                                    <div id="agent-div" class="top-buffer">
                                        <table class="table table-striped" id="agent-table">
                                            <thead>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Name</th>
                                                <th class="text-center"><span class="glyphicon glyphicon-trash"></span>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>

            </div>

        </div>
    </div>

<?php $this->partial('views/tsl/dialogs/add_agent.php'); ?>
<?php $this->partial('views/tsl/dialogs/delete_agent.php'); ?>