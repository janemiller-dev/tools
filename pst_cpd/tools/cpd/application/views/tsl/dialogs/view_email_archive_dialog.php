<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/view_email_archive_dialog.js"></script>

<div class="modal fade" id="view-email-archive-dialog" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="message-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="view-email-focus-title" class="modal-title">Email Archive</h2>
            </div>

            <div class="modal-body">

                <!-- Show the list of email-focuss. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- List Email Focus table -->
                        <div class="row" id="email-focus-row">
                            <div class="col-xs-12">
                                <div id="email-focus-div" class="top-buffer">
                                    <table class="table table-responsive table-striped" id="email-archive-table">
                                        <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Email Name</th>
                                            <th class="text-center">Client Name</th>
                                            <th class="text-center">Asset Name</th>
                                            <th class="text-center">Asset Location</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- List Email Focus table -->
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>