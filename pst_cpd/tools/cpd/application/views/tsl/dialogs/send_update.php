<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/send_update.js"></script>


<div class="modal fade" id="send-update-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="send-update-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="send-update-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="send-update-title" class="modal-title">Do you want to send Update?</h2>
                </div>

                <div class="modal-body">

                    <div class="form-group" style="margin-bottom: 5px !important;">
                        <label class="col-xs-12 col-sm-3 control-label" for="update-client-email">To:</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="update-client-email">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="update-client-subject">Subject:</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="update-client-subject">
                        </div>
                    </div>

                    <div id="update-email-content">
                    </div>

                </div>


                <input type="hidden" id="client-id" name="client_id"/>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="close-send-update-dialog" data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary" id="embed-update-content">Embed</button>
                    <button type="submit" class="btn btn-primary" id="send-update-button">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>
