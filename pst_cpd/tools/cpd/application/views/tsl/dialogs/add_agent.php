<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<div class="modal fade" id="add-agent-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="add-agent-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="add-agent-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <h2 id="add-agent-title" class="modal-title">Add Agent</h2>
                </div>

                <div class="modal-body modal-small">
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="agent-name">Agent Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="agent-name" name="agent_name"/>
                            <p class="form-text text-muted">Enter the Name of the Agent.</p>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="close-add-agent-dialog" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/add_agent.js"></script>