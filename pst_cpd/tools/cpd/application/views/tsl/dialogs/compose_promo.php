<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<div class="modal fade" id="compose-promo-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="compose-promo-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="compose-promo-title" class="modal-title">Compose Promo</h2>
            </div>
            <div class="modal-body">
<!--                <form id="compose-promo-form" class="form-horizontal promo-form"></form>-->

            </div>
            <div class="modal-footer">
                <button type="button" id="save-promo-content" class="btn btn-primary">Save As New</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
            <input type="hidden" id="question_length" name="question_length">
            <input type="hidden" id="promo_type" name="promo-type">
            <input type="hidden" id="promo_id" name="promo-id">
        </div>
    </div>
</div>

