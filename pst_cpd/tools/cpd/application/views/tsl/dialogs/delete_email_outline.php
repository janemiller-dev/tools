<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/delete_email_outline.js"></script>

<div class="modal fade" id="delete-email-outline-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="delete-email-outline-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="delete-email-outline-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="delete-email-outline-title" class="modal-title">Delete Email Outline?</h2>
                </div>

                <div class="modal-body modal-small">

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="delete-email-outline-version">Version:</label>
                        <div class="col-xs-12 col-sm-9">
                            <p class="form-control-static" id="delete-email-outline-version"></p>
                        </div>
                    </div>

                </div>

                <input type="hidden" id="email-outline-id" name="email_outline_id"/>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="delete-email-outline-button">Delete</button>
                </div>

            </form>
        </div>
    </div>
</div>
