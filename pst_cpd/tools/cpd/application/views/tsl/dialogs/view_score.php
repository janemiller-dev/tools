<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/view_perf_score.js"></script>

<div class="modal fade" id="view-perf-score-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="message-label" aria-hidden="true">
    <div class="modal-dialog modal-lg score-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h3 id="view-score-title" class="modal-title">Performance/Proficiency Score Board</h3>
            </div>

            <div class="row">
                <div class="col-xs-2 col-xs-offset-3">
                    <h5 class="col-xs-12 text-center">From Date:</h5>
                    <input class="col-xs-12 tsl-onlydate form-control" id="perf-from-date" style="border: 1px solid">
                </div>
                <div class="col-xs-2 col-xs-offset-2">
                    <h5 class="col-xs-12 text-center">To Date:</h5>
                    <input class="col-xs-12 tsl-onlydate form-control" id="perf-to-date" style="border: 1px solid">
                </div>
                <div>
                    <p style="margin-top: 4px">&nbsp</p>
                    <button class="btn btn-secondary" type="button" id="perf-date-submit">
                            <span class="has-tooltip" data-toggle="tooltip" title="Fetch Performance Proficiency." data-placement="bottom">
                                                    <i class="fa fa-check"></i>
                                                </span>
                    </button>
                </div>
            </div>
            <div class="modal-body score-body">

                <ul class="nav nav-tabs top-buffer" role="tablist">
                    <li role="presentation" id="performance-click">
                        <a href="#performance" role="tab" data-toggle="tab">Performance Score</a>
                    </li>
                    <li role="presentation">
                        <a href="#proficiency" role="tab" data-toggle="tab">Proficiency Score</a>
                    </li>
                </ul>

                <div class="tab-content">

                    <!-- Performance Tab -->
                    <div role="tabpanel" class="tab-pane col-xs-12" id="performance">

                        <!-- Score List-->
                        <div class="row">
                            <div class="col-xs-12">

                                <div class="row score-row">
                                    <div class="col-xs-12">

                                        <div id="perf-score-div" class="top-buffer">
                                            <table class="table" id="perf-score-table">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Outline
                                                        <br>(Initial Contact)
                                                    </th>
                                                    <th>Step1
                                                        <br>(Opening)
                                                    </th>
                                                    <th>Step2
                                                        <br>(Permission)
                                                    </th>
                                                    <th>Step3
                                                        <br>(Interest)
                                                    </th>
                                                    <th>Step3a
                                                        <br>(Generate)
                                                    </th>
                                                    <th>Step3b
                                                        <br>(Introduce)
                                                    </th>
                                                    <th>Step3c
                                                        <br>(Explore)
                                                    </th>
                                                    <th>Step3d
                                                        <br>(Listen)
                                                    </th>
                                                    <th>Step3e
                                                        <br>(Convert)
                                                    </th>
                                                    <th>Step4
                                                        <br>(Detached Offer)
                                                    </th>
                                                    <th>Step5
                                                        <br>(Schedule)
                                                    </th>
                                                    <th>Total</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Proficiency Tab -->
                    <div role="tabpanel" class="tab-pane col-xs-12" id="proficiency">

                        <!-- Score List-->
                        <div class="row">
                            <div class="col-xs-12">

                                <div class="row score-row">
                                    <div class="col-xs-12">

                                        <div id="prof-score-div" class="top-buffer">
                                            <table class="table" id="prof-score-table">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Outline
                                                        <br>(Initial Contact)
                                                    </th>
                                                    <th>Step1
                                                        <br>(Opening)
                                                    </th>
                                                    <th>Step2
                                                        <br>(Permission)
                                                    </th>
                                                    <th>Step3
                                                        <br>(Interest)
                                                    </th>
                                                    <th>Step3a
                                                        <br>(Generate)
                                                    </th>
                                                    <th>Step3b
                                                        <br>(Introduce)
                                                    </th>
                                                    <th>Step3c
                                                        <br>(Explore)
                                                    </th>
                                                    <th>Step3d
                                                        <br>(Listen)
                                                    </th>
                                                    <th>Step3e
                                                        <br>(Convert)
                                                    </th>
                                                    <th>Step4
                                                        <br>(Detached Offer)
                                                    </th>
                                                    <th>Step5
                                                        <br>(Schedule)
                                                    </th>
                                                    <th>Total</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>