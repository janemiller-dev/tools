<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/add_caller.js"></script>

<div class="modal fade" id="add-caller-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="add-caller-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="add-caller-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-caller-title" class="modal-title">Add Caller</h2>
                </div>

                <div class="modal-body modal-small">

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="caller-name">Caller Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="caller-name" name="caller_name"/>
                            <p class="form-text text-muted">Enter the Name of the caller.</p>
                            <input type="hidden" id="tsl-id" name="tsl_id">
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="close-add-caller-dialog" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
