<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/view_background.js"></script>

<div class="modal fade" id="view-background-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="message-label"
     aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="view-background-title" class="modal-title">View/Edit Background</h2>
                <input id="image-upload" type="file" name="files[]"
                       data-url= <?= "https://" . $_SERVER['SERVER_NAME'] . "/tools/tsl/upload_background_template" ?> style="visibility:
                       hidden; float:left; width:0;">
                <button class="btn btn-primary upload-background-template">Upload New Background <i class="fa fa-upload"></i></button>
                </button>

            </div>

            <div class="modal-body">

                <!-- Show the list of backgrounds. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- List Background table -->
                        <div class="row" id="background-row">
                            <div class="col-xs-12">
                                <p id="no-background">You have no instance of Backgrounds. Use the add background to add a new
                                    background.</p>
                                <div id="background-div" class="top-buffer">
                                    <table class="table" id="background-table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Background Thumbnail</th>
                                            <th>Background Name</th>
                                            <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- List Background table -->
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php $this->partial('views/tsl/dialogs/delete_background.php'); ?>
