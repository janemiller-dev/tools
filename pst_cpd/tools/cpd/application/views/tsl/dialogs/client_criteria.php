<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/client_criteria.js"></script>

<div class="modal fade" id="client-criteria-dialog" tabindex="-1" data-backdrop="static" role="dialog"
     aria-labelledby="criteria-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close close-button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="client-criteria-title" class="modal-title">Asset Criteria</h2>
            </div>

            <div class="modal-body">

                <!-- Show the list of criteria belonging to a client. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- Criteria table -->
                        <div class="row" id="criteria-row">
                            <div class="col-xs-12">
                                <p id="no-criteria">You have no criteria set for this asset. To add one, go to
                                    the Homebase.</p>
                                <div id="criteria-div" class="top-buffer">
                                    <table class="table table-striped" id="client-criteria-table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Criteria Name</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary close-button" data-dismiss="modal">Close</button>
            </div>

        </div>

    </div>
</div>