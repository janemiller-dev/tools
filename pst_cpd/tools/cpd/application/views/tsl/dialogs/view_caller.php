<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<div class="modal fade" id="view-caller-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="message-label"
     aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="view-caller-title" class="modal-title">View/Edit Caller</h2>
                <button type="button" id="add-caller-button" class="btn btn-primary bottom-buffer"
                        data-target="#add-caller-dialog" data-toggle="modal" data-backdrop="static">Add New Caller
                </button>

            </div>

            <div class="modal-body">

                <!-- Show the list of callers. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- List Caller table -->
                        <div class="row" id="caller-row">
                            <div class="col-xs-12">
                                <p id="no-caller">You have no instance of Callers. Use the add caller to add a new
                                    caller.</p>
                                <div id="caller-div" class="top-buffer">
                                    <table class="table" id="caller-table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Caller Name</th>
                                            <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- List Caller table -->
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php $this->partial('views/tsl/dialogs/delete_caller.php'); ?>
<?php $this->partial('views/tsl/dialogs/add_caller.php'); ?>
