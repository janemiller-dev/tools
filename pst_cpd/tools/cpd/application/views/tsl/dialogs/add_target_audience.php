<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/add_target_audience.js"></script>

<div class="modal fade" id="add-target-audience-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="add-target-audience-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="add-target-audience-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-target-audience-title" class="modal-title">Add Location</h2>
                </div>

                <div class="modal-body modal-small">

                    <!--Location Field-->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="target-audience-name">Location Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="target-audience-name"
                                   name="target_audience_name"/>
                            <p class="form-text text-muted">Enter Location Name.</p>
                        </div>
                    </div>
                    <!-- // Location Field-->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="close-add-target-audience-dialog"
                            data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
