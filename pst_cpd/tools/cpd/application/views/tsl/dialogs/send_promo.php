<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/send_promo.js"></script>

<div class="modal fade" id="send-promo-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="send-promo-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="send-promo-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="send-promo-title" class="modal-title">Do you want to send this Email?</h2>
                </div>

                <div class="modal-body">

                    <div class="form-group" style="margin: 5px !important;">
                        <label class="col-xs-12 col-sm-3 control-label" for="promo-client-email">To:</label>
                        <div class="col-xs-12 col-sm-9 input-group client-email">
                            <input type="email" class="form-control promo-client-email">
                            <span class="input-group-btn">
                                <span class="has-tooltip" title="Add Recipient" data-placement="bottom">
                                    <button class="btn btn-default" type="button" id="add-recipient">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </span>
                            </span>
                        </div>
                    </div>

                    <div class="form-group" style="margin: 5px !important;">
                        <label class="col-xs-12 col-sm-3 control-label" for="promo-client-subject">Subject:</label>
                        <div class="col-xs-12 col-sm-9" style="padding: 0">
                            <input type="text" class="form-control" id="promo-client-subject">
                        </div>
                    </div>

                    <div id="email-content">
                    </div>
                </div>


                <input type="hidden" id="client-id" name="client_id"/>
                <div class="modal-footer">
                    <span class="hidden" style="float: left; font-size: 150%" id="attachment_span"><i
                                class="fa fa-paperclip"></i><span id="attachment_name"></span></span>
                    <button type="button" class="btn btn-primary" id="close-send-promo-dialog" data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary" id="send_recompose" data-type="recompose">Override
                    </button>
                    <button type="button" class="btn btn-primary save_as_new" data-type="compose">Save As New</button>
                    <button type="button" class="btn btn-primary" id="embed-content">Embed</button>
                    <button type="button" class="btn btn-primary" id="attach-pdf">Attach</button>
                    <button type="submit" class="btn btn-primary" id="send-promo-button">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>
