<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/tsl_ccm.js"></script>

<div class="modal fade" id="view-ccm-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="message-h4" aria-hidden="true">
    <div class="modal-dialog ccm-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-h4="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h3 id="view-ccm-title" class="modal-title">Call Control Dashboard</h3>
            </div>

            <div class="modal-body">
                <div class="row" style="border: solid 4px #A4A4A4; margin: 2px">
                    <!--// Prompter Dialog -->
                    <div class="col-xs-4 ccm-box">
                        <div>
                            <h4 id="view-reader-title" class="text-center"
                                style="border-bottom: 1px solid #e5e5e5; padding-bottom: 10px">Message Prompter</h4>
                        </div>
                        <div style="height: 60vh; overflow-y: hidden; scroll-behavior: smooth">
                            <div class="row" id="titles">
                                <div id="credits" class="col-xs-12" style="font-size: 20px">
                                </div>
                            </div>
                        </div>

                        <div style="border-top: 1px solid #e5e5e5; padding-top: 10px">
                            <div class="col-xs-2">
                                <input type="range" min="12" max="20" value="15" id="speed-control"
                                       style="width: 5vw;"/>
                            </div>
                            <div class="col-xs-10" style="padding-bottom: 15px">
                                <a class="btn btn-primary" href="#creditos" name="creditos" style="margin-left: 1vw">Start
                                    <i class=" fa fa-play"></i></a>
                                <button type="button" class="btn btn-primary" id="stop">
                                    Stop
                                    <i class="fa fa-stop"></i></button>
                                <button type="button" class="btn btn-primary" id="back">Back
                                    <i class="fa fa-arrow-circle-down"></i>
                                </button>
                                <button type="button" class="btn btn-primary" id="next">Next
                                    <i class="fa fa-arrow-circle-up"></i></button>
                                <button type="button" class="btn btn-primary" id="reset">Reset <i
                                        class="fa fa-refresh"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <!--// Prompter Dialog -->

                    <div class="col-xs-8">
                        <!-- CCM Dialog -->
                        <div class="col-xs-6 ccm-box">
                            <div>
                                <h4 class="text-center"
                                    style="border-bottom: 1px solid #e5e5e5; padding-bottom: 10px">Call Control Manager</h4>
                            </div>
                            <div class="text-center">
                                <div class="col-xs-4">
                                    <h4 style="display: inline-block">Start Call</h4>
                                    <input type="radio" class="start-end-call" name="start-end-call" data-type="start">
                                </div>
                                <div class="col-xs-4 tsl-ccm-view">
                                    <h4 style="display: inline-block;">No Answer</h4>
                                    <input id="ccm-na" type="radio" class="start-end-call edit-client-info"
                                           data-name="tac_valid_phone" data-type="end"
                                           value="na" data-table="tsl_about_client">
                                </div>
                                <div class="col-xs-4">
                                    <h4 style="display: inline-block;">End Call</h4>
                                    <input type="radio" class="start-end-call" name="start-end-call" data-type="end">
                                </div>
                            </div>
                            <div class="text-center" style="margin-top: 15%">
                                <div class="col-xs-3">
                                    <h4>Call Status</h4>
                                </div>
                                <div class="col-xs-3">
                                    <h4>Call Start</h4>
                                    <input type="text" class="form-control" id="ccm-start-time">
                                </div>
                                <div class="col-xs-3">
                                    <h4>Call End</h4>
                                    <input type="text" class="form-control" id="ccm-end-time">
                                </div>
                                <div class="col-xs-3">
                                    <h4>Duration</h4>
                                    <input type="text" class="form-control" id="ccm-duration">
                                </div>
                            </div>

                            <div class="text-center row" id="scoreboard-control" style="margin-top: 39%">
                                <div class="col-xs-20">
                                    <h4 style="display: inline-block">Dial</h4>
                                    <input type="radio" class="update-scoreboard" name="update-score" data-type="dial">
                                </div>
                                <div class="col-xs-20">
                                    <h4 style="display: inline-block">Contact</h4>
                                    <input type="radio" class="update-scoreboard" name="update-score" data-type="contact">
                                </div>
                                <div class="col-xs-20">
                                    <h4 style="display: inline-block">Message</h4>
                                    <input type="radio" class="update-scoreboard" name="update-score" data-type="message">
                                </div>
                                <div class="col-xs-20">
                                    <h4 style="display: inline-block">Conv</h4>
                                    <input type="radio" class="update-scoreboard" name="update-score"
                                           data-type="conv">
                                </div>
                                <div class="col-xs-20">
                                    <h4 style="display: inline-block">Meeting</h4>
                                    <input type="radio" class="update-scoreboard" name="update-score"
                                           data-type="meeting">
                                </div>

                            </div>

                            <div class="text-center" style="margin-top: 8%">
                                <div class="col-xs-6">
                                    <label>Select Script</label>
                                    <div class="input-group">
                                        <select id="ccm-script" class="form-control input-group-addon ccm-select"
                                                data-type="script" style="background-color: #fff !important;"></select>
                                        <span class="input-group-btn">
                                    <button class="btn btn-default btn-sm show-prompter">
                                        <span class="glyphicon glyphicon-list"></span>
                                    </button>
                                </span>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <label>Select Message</label>
                                    <div class="input-group">
                                        <select id="ccm-message" class="form-control input-group-addon ccm-select"
                                                data-type="message" style="background-color: #fff !important;"></select>
                                        <span class="input-group-btn">
                                    <button class="btn btn-default btn-sm show-prompter">
                                        <span class="glyphicon glyphicon-list"></span>
                                    </button>
                                </span>
                                    </div>
                                </div>

                                <div class="col-xs-12 text-center tsl-ccm-view" style="margin-top: 15px">
                                    <h4 style="display: inline-block;">Bad Number</h4>
                                    <input type="radio" class="ccm-mark-bad start-end-call"
                                           name="start-end-call" data-type="end">
                                </div>
                            </div>
                        </div>
                        <!--// CCM Dialog -->

                        <!-- Notes division -->
                        <div>
                            <div id="event-notes-div" class="col-xs-6 ccm-box">
                                <div>
                                    <h4 class="text-center"
                                        style="border-bottom: 1px solid #e5e5e5; padding-bottom: 10px">Event Notes</h4>
                                </div>
                                <!-- Event name field.-->
                                <label class="control-label col-xs-3">Event Name</label>
                                <div class="col-xs-9" style="margin-bottom: 5px">
                                    <input type="text" class="form-control event-input event" data-name="dn_name"
                                           data-seq="0" id="dn_name0">
                                </div>
                                <span class="fa fa-trash delete-note" id="delete-note0"
                                      style="position: absolute; right: 10px;" data-seq="0"></span>
                                <!--// Event name field.-->

                                <!-- Event notes field.-->
                                <label class="control-label col-xs-3">Event Notes</label>
                                <div class="col-xs-9" style="margin-bottom: 5px">
                                        <textarea class="form-control event-text event" rows="3" style="width: 100%;"
                                                  data-name="dn_note" data-seq="0" id="dn_note0"></textarea>
                                </div>
                                <!--// Event notes field.-->

                                <!-- Event date field.-->
                                <label class="control-label col-xs-3">Event Date</label>
                                <div class="col-xs-9" style="margin-bottom: 5px">
                                    <input type="text" class="form-control ccm-action-date event event-date"
                                           data-name="dn_when" data-seq="0" id="dn_date0">
                                </div>
                                <!--// Event date field.-->
                            </div>

                            <!-- Action Div -->
                            <div class="col-xs-6 ccm-box" id="ccm-action-row">
                                <div>
                                    <h4 class="text-center"
                                        style="border-bottom: 1px solid #e5e5e5; padding-bottom: 10px">Next Actions</h4>
                                </div>
                                <div class="col-xs-12">
                                    <div id="view-ccm-action-div" class="top-buffer">

                                        <!-- Action Text Field -->
                                        <label class="control-label col-xs-3 ccm-action-label">Action 1</label>
                                        <div class="col-xs-9" style="margin-bottom: 5px">
                                        <textarea class="form-control ccm-action-input ccm-action" data-name="ta_content"
                                                  data-seq="0" id="ccm-action_content0" rows="3"
                                                  style="width: 100%;"></textarea>
                                        </div>
                                        <span class="fa fa-trash delete-ccm-action" id="delete-ccm-action0"
                                              style="position: absolute; right: 10px;" data-seq="0"></span>
                                        <!--// Action Text Field -->

                                        <!-- By Whom Select-->
                                        <label class="control-label col-xs-3">By Whom</label>
                                        <div class="col-xs-9" style="margin-bottom: 5px">
                                            <select class="form-control ccm-action-whom ccm-action" data-seq="0"
                                                    id="ccm-action_whom0"
                                                    data-name="ta_whom" style="width: 100%;"></select>
                                        </div>
                                        <!--// By Whom Select-->

                                        <!-- By When Select-->
                                        <label class="control-label col-xs-3">By When</label>
                                        <div class="col-xs-9" style="margin-bottom: 5px">
                                            <input class="form-control ccm-action-when ccm-action tsl-date" data-seq="0"
                                                   id="ccm-action_when0"
                                                   data-name="ta_when">
                                        </div>
                                        <!-- By When Select-->
                                    </div>
                                </div>
                            </div>
                            <!-- // Action Division -->
                        </div>
                        <!--// Notes division -->

                        <div class="col-xs-12 ccm-box">
                            <div>
                                <div>
                                    <h4 id="view-reader-title"
                                        style="border-bottom: 1px solid #e5e5e5; padding-bottom: 10px">Call Activity
                                        Scoreboard
                                        <span style="float: right;" id="ccm-next-day"><i
                                                class="fa fa-arrow-circle-right"></i></span>
                                        <input style="float: right; margin-right: 10px;" id="ccm-date-picker"/>
                                        <span style="float: right; margin-right: 10px;" id="ccm-prev-day"><i
                                                class="fa fa-arrow-circle-left"></i></span>
                                        <label style="float: right; margin-right: 10px;">Select Date: </label>
                                    </h4>

                                    <table>
                                        <tbody>
                                        <?php for ($j = 0; $j < 4; $j++) { ?>
                                            <tr>
                                                <?php for ($i = 0; $i < 18; $i++) { ?>
                                                    <td class="scoreboard-span"></td>
                                                <?php } ?>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>