<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/delete_script.js"></script>

<div class="modal fade" id="delete-script-dialog" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="delete-script-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="delete-script-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="delete-script-title" class="modal-title">Delete Script?</h2>
                </div>

                <div class="modal-body modal-small">

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="name">Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <p class="form-control-static" id="script-name"></p>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="close-delete-script-dialog" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary" id="delete-script-button">Delete</button>
                </div>

            </form>
        </div>
    </div>
</div>
