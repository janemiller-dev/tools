<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/view_promo_archive_dialog.js"></script>

<div class="modal fade" id="view-promo-archive-dialog" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="message-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="view-promo-focus-title" class="modal-title">Promo Archive</h2>
            </div>

            <div class="modal-body">

                <!-- Show the list of promo-focuss. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- List Promotional Focus table -->
                        <div class="row" id="promo-focus-row">
                            <div class="col-xs-12">
                                <div id="promo-focus-div" class="top-buffer">
                                    <table class="table table-responsive table-striped" id="promo-archive-table">
                                        <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center" id="component-name">Promo Name</th>
                                            <th class="text-center" id="component-topic">Promo Topic</th>
                                            <th class="text-center">Date/Time Sent</th>
                                            <th class="text-center" id="component-response">Promo Response</th>
                                            <th class="text-center"><i class="fa fa-eye"></i></th>
                                            <th class="text-center"><i class="fa fa-trash"></i></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- List Promotional Focus table -->
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>