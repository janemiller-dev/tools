<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/add_assignee.js"></script>

<div class="modal fade" id="add-assignee-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="add-assignee-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="add-assignee-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-assignee-title" class="modal-title">Add Assignee</h2>
                </div>

                <div class="modal-body modal-small">

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="assignee-name">Assignee Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="assignee-name" name="assignee_name"/>
                            <p class="form-text text-muted">Enter the Name of the Assignee.</p>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="close-add-assignee-dialog" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
