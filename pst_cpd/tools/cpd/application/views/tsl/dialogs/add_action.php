<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/add_action.js"></script>

<div class="modal fade" id="add-action-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="add-action-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="add-action-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-action-title" class="modal-title">Add Action</h2>
                </div>

                <div class="modal-body modal-small">

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="action-text">Action Text</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="action-text" name="action_text"/>
                            <p class="form-text text-muted">Enter Action Text.</p>
                            <input type="hidden" id="tsl_id" name="tsl_id">
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="close-add-action-dialog" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
