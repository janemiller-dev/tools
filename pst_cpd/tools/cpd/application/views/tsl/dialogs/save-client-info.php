<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/save_client_info.js"></script>

<div class="modal fade" id="save-client-info-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="save-client-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="save-client-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 id="save-client-title" class="modal-title">Link to
                        <span id="active-cpd-name"></span> Client
                        Project Dashboard?</h3>
                </div>
                <div class="modal-body no-content" style="display: none">
                    <p>This record can be reverted to Homebase, linked to Strategic or moved to Identified.</p>
                </div>
                <div class="modal-body maybe-content" style="display: none">
                    <p>All maybe must remain in TSL or moved to Identified.</p>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary no-content maybe-content" id="move-identified" style="display: none;">
                    <span class="has-tooltip" title="Move to Identified.">Identified</span></button>
                    <button class="btn btn-primary no-content" id="revert-to-homebase" style="display: none;">
                        <span class="has-tooltip" title="Reverts the Asset back to Homebase.">Revert</span></button>
                    <button type="submit" class="btn btn-primary" id="save-client-button">
                        <span class="has-tooltip" title="Moves Asset to Active DMD instance.">Link</span></button>
                    <button type="button" class="btn btn-primary" id="close-save-client-dialog" data-dismiss="modal">
                        Close
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
