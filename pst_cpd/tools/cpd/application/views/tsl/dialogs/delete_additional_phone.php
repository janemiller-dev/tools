<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="delete-additional-phone-dialog" tabindex="-1" data-backdrop="static" role="dialog"
     aria-labelledby="delete-additional_phone-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="delete-additional_phone-form" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="delete-additional_phone-title" class="modal-title">Delete Additional Phone?</h2>
                </div>

                <div class="modal-body modal-small">
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="name">Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <p class="form-control-static" id="additional-phone-name"></p>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary" id="delete-additional-phone-button">Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>
