<!-- Script for pdf js -->
<script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>
<div class="modal fade" id="view-promo-archive-instance-dialog" data-backdrop="static" tabindex="-1"
     role="dialog" aria-labelledby="message-label" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="view-title" class="modal-title">View Promo Content</h2>
            </div>

            <div class="modal-body">

                <!-- Show the Promo Docs. -->
                <div align="center">
                    <div class="col-xs-12" id="archive-viewer-container">
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary save_as_new">Save as New</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
