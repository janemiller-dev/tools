<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/view_tsl_asset_criteria.js"></script>

<div class="modal fade" id="view-tsl-asset-criteria-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="view-asset-criteria-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="view-asset-criteria-title" class="modal-title">Asset Criteria</h2>

            </div>

            <div class="modal-body">

                <!-- Show the list of promos. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- Deal Promo table -->
                        <div class="row" id="asset-criteria-row">
                            <div class="col-xs-12">
                                <div id="asset-criteria-div" class="top-buffer">
                                    <table class="table table-striped" id="asset-criteria-table">
                                        <thead>
                                        <tr>
                                            <th class="text-center">Market Area</th>
                                            <th class="text-center">Location</th>
                                            <th class="text-center">Description</th>
                                            <th class="text-center">Condition</th>
                                            <th class="text-center">Other Criteria</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
