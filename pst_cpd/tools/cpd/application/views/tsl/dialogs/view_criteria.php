<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

    <div class="modal fade" id="view-criteria-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="view-criteria-label"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <h2 id="view-criteria-title" class="modal-title">Suspect List Criteria</h2>
                    <button type="button" id="add-criteria-button" class="btn btn-primary bottom-buffer"
                            data-toggle="modal" data-target="#add-criteria-dialog" data-backdrop="static">Add New Criteria
                    </button>

                </div>

                <div class="modal-body">

                    <!-- Show the list of promos. -->
                    <div class="row">
                        <div class="col-xs-12">

                            <!-- Deal Promo table -->
                            <div class="row" id="criteria-row">
                                <div class="col-xs-12">
                                    <p id="no-criteria">You have no criteria for the clients. To create one, use
                                        the Add Criteria button.</p>
                                    <div id="criteria-div" class="top-buffer">
                                        <table class="table table-striped" id="criteria-table">
                                            <thead>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Criteria Name</th>
                                                <th class="text-center"><span class="glyphicon glyphicon-trash"></span>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>

            </div>

        </div>
    </div>

<?php $this->partial('views/tsl/dialogs/delete_criteria.php'); ?>