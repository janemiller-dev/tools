<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!--<link rel="stylesheet" href="--><?php //echo $this->basepath; ?><!--resources/app/css/tsl/page.css"/>-->

<div class="modal fade" id="send-email-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="send-email-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="send-email-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="send-email-title" class="modal-title">Do you want to send Email?</h2>
                </div>

                <div class="modal-body">

                    <!-- Recipients Email Address-->
                    <div class="form-group" style="margin: 5px !important;">
                        <label class="col-xs-12 col-sm-3 control-label" for="client-email">To:</label>
                        <div class="col-xs-12 col-sm-9 input-group client-email">
                            <input type="email" class="form-control email-client-email">
                            <span class="input-group-btn">
                                <span class="has-tooltip" title="" data-placement="bottom" data-original-title="Add Recipient">
                                    <button class="btn btn-default" type="button" id="add-recipient">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </span>
                            </span>
                        </div>
                    </div>
                    <!--// Recipients Email Address-->

                    <div class="form-group" style="margin: 5px !important;">
                        <label class="col-xs-12 col-sm-3 control-label" for="promo-client-subject">Subject:</label>
                        <div class="col-xs-12 col-sm-9" style="padding: 0">
                            <input type="text" class="form-control" id="subject">
                        </div>
                    </div>

                    <div id="content-container">
                    </div>
                </div>


                <input type="hidden" id="client-id" name="client_id"/>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="close-send-email-dialog" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary" id="send-email-button">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/send_email.js"></script>