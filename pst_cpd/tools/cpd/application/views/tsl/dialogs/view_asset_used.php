<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

    <div class="modal fade" id="view-asset-used-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="view-asset-used-label"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <h3 id="view-asset-used-title" class="modal-title">
                        These Client Assets are located in another TSL. Do you wish to move them from there to here?</h3>
                </div>

                <div class="modal-body">

                    <!-- Show the list of promos. -->
                    <div class="row">
                        <div class="col-xs-12">

                            <!-- Deal Promo table -->
                            <div class="row" id="asset-used-row">
                                <div class="col-xs-12">
                                    <div id="asset-used-div" class="top-buffer">
                                        <table class="table table-striped" id="asset-used-table">
                                            <thead>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Owner</th>
                                                <th class="text-center">Asset</th>
                                                <th class="text-center">TSL </th>
                                                <th class="text-center"><span class="glyphicon glyphicon-trash"></span>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="submit_add_tsl">Submit</button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>

            </div>

        </div>
    </div>
