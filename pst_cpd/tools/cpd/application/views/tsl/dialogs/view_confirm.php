<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/view_confirm.js"></script>

<div class="modal fade" id="view-confirm-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="message-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close-button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="view-confirm-title" class="modal-title">Event Confirmation</h2>
            </div>
            <div class="modal-body">

                <!-- Show the list of confirm. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- List Notes -->
                        <div class="row" id="confirm-row">
                            <div class="col-xs-12">
                                <div id="view-confirm-div" class="top-buffer">

                                    <!-- Event Name Field -->
                                    <label class="control-label col-xs-2">Event Type</label>
                                    <div class="col-xs-10" style="margin-bottom: 5px">
                                        <input type="text" class="form-control confirm-field" id="confirm-name0"
                                               data-name="tsl_confirm_name" data-seq="0"/>
                                    </div>
                                    <span class="fa fa-trash delete-confirm" id="delete-confirm0"
                                          style="position: absolute; right: 10px;" data-seq="0"></span>
                                    <!--// Event Name Field -->

                                    <!-- Event Topic Field-->
                                    <label class="control-label col-xs-2">Event Topic</label>
                                    <div class="col-xs-10" style=" margin-bottom: 5px">
                                        <input type="text" class="form-control confirm-field" data-seq="0"
                                               id="confirm-topic0" data-name="tsl_confirm_topic"/>
                                    </div>
                                    <!--// Event Topic Field-->

                                    <!-- Notes Textarea-->
                                    <label class="control-label col-xs-2">Event Notes</label>
                                    <div class="col-xs-10" style="margin-bottom: 5px">
                                    <textarea class="form-control confirm-input confirm-field"
                                              data-seq="0" id="confirm-content0" rows="3" data-name="tsl_confirm_note"
                                              style="width: 100%;"></textarea>
                                    </div>
                                    <!--// Notes Textarea-->

                                    <!-- Event Site Input-->
                                    <label class="control-label col-xs-2">Event Site</label>
                                    <div class="col-xs-10" style="margin-bottom: 5px">
                                        <input class="form-control confirm-site confirm-field" data-seq="0"
                                               id="confirm-site0" style="width: 100%;" data-name="tsl_confirm_site"/>
                                    </div>
                                    <!--// Event Site Input-->

                                    <!-- By When-->
                                    <label class="control-label col-xs-2"> By When</label>
                                    <div class="col-xs-10" style=" margin-bottom: 5px">
                                        <input class="form-control confirm-when confirm-field tsl-date" data-seq="0"
                                               id="confirm-when0" data-name="tsl_confirm_when">
                                    </div>
                                    <!-- By When-->
                                </div>
                            </div>
                        </div>
                        <!-- // List Notes -->
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="add-confirm">Add Confirm</button>
                <button type="button" class="btn btn-primary close-button" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

