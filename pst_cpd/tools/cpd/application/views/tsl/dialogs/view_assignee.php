<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<div class="modal fade" id="view-assignee-dialog" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="message-label" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="view-assignee-title" class="modal-title">View/Edit Assignee</h2>
                <button type="button" id="add-assignee-button" class="btn btn-primary bottom-buffer"
                        data-target="#add-assignee-dialog" data-toggle="modal" data-backdrop="static">Add New Assignee
                </button>

            </div>

            <div class="modal-body">

                <!-- Show the list of assignees. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- List Assignee table -->
                        <div class="row" id="assignee-row">
                            <div class="col-xs-12">
                                <p id="no-assignee">You have no instance of Assignees. Use the add assignee to add a new
                                    assignee.</p>
                                <div id="assignee-div" class="top-buffer">
                                    <table class="table" id="assignee-table">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Assignee Name</th>
                                            <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- List Assignee table -->
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php $this->partial('views/tsl/dialogs/delete_assignee.php'); ?>
<?php $this->partial('views/tsl/dialogs/add_assignee.php'); ?>
