<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/add_client_phone.js"></script>

<div class="modal fade" id="add-client-phone-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="add-category-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="add-client-phone-form" class="form-horizontal">

                <div class="modal-header">
                    <h2 id="add-client-phone-title" class="modal-title">Add Client Phone</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body modal-small">

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="client-phone-type">Phone Type</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="client-phone-type" name="client_phone_type"/>
                            <p class="form-text text-muted">Enter the Phone Number Type.</p>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="client-phone-number">Phone Number</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="client-phone-number" name="client_phone_number"/>
                            <p class="form-text text-muted">Enter the Phone Number.</p>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" class="close">Close</button>
                    <button type="button" id="save-phone" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

