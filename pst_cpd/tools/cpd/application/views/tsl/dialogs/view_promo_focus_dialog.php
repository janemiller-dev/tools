<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="view-promo-focus-dialog" tabindex="-1" data-backdrop="static" role="dialog"
     aria-labelledby="message-label"
     aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="view-promo-focus-title" class="modal-title">View/Edit Promo Focus</h2>
                <button type="button" id="add-promo-focus-button" class="btn btn-primary bottom-buffer"
                        data-target="#add-promo-focus-dialog" data-toggle="modal" data-backdrop="static">Add New
                </button>

            </div>

            <div class="modal-body">

                <!-- Show the list of promo-focuss. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- List Promotional Focus table -->
                        <div class="row" id="promo-focus-row">
                            <div class="col-xs-12">
                                <!--                                <p id="no-promo-focus">You have no instance of Promotional Focuss. Use the add promo-focus to add a new-->
                                <!--                                    promo-focus.</p>-->
                                <div id="promo-focus-div" class="top-buffer">
                                    <table class="table" id="promo-focus-table">
                                        <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Promotional Focus Name</th>
                                            <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- List Promotional Focus table -->
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php $this->partial('views/tsl/dialogs/delete_promo_focus.php'); ?>
<?php $this->partial('views/tsl/dialogs/add_promo_focus.php'); ?>
