<!-- Script for pdf js -->
<script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>

<div class="modal fade" id="read-promo-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="message-label" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="read_promo-title" class="modal-title" align="center">Promo Builder Reading View</h2>
            </div>

            <div class="modal-body">

                <!-- Show the Promo Docs. -->
                <div class="row" align="center" style="margin: 1px">
                    <div id="email-container" class="col-xs-12"></div>
                </div>
            </div>

            <div class="modal-footer">
                <button class="btn-secondary btn promo-button has-tooltip send-promo" data-type="pages"
                        data-origin="tsl" data-original-title="Sends Promo Email." data-toggle="modal"
                        data-target="#send-promo-dialog" data-backdrop="static"
                  >
                    Send <i class="fa fa-envelope"></i>
                </button>
                <button id="edit-promo" class="btn btn-secondary promo-button has-tooltip"
                        title="Enables Editing for Promo Composer.">Edit<i class="fa fa-pencil-square-o "></i>
                </button>
                <button id="save-promo" class="btn btn-secondary promo-button has-tooltip"
                        title="Saves Promo Composer Content.">Save <i class="fa fa-check"></i></button>
                <button id="recompose-promo" class="btn btn-secondary save_as_new has-tooltip"
                        title="Saves Promo content as a new version.">Save As New <i class="fa fa-check"></i>
                </button>

                <button id="view-pdf" class="btn btn-secondary promo-button has-tooltip" data-is-pdf="0"
                        title="View PDF for the current Promo content and selected background.">
                    View PDF <i class="fa fa-eye"></i></button>
                <button id="create_promo_pdf" class="btn btn-secondary promo-button has-tooltip"
                        title="Creates a new PDF from Promo content and background.">Create PDF <i
                            class="fa fa-check"></i>
                </button>

                <button class="btn btn-secondary" data-dismiss="modal">Close <i class="fa fa-times"></i></button>
            </div>
        </div>
    </div>
</div>
