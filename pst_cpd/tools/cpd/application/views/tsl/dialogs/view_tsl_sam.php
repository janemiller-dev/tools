<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/view_tsl_sam.js"></script>

<div class="modal fade" id="view-tsl-sam-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="view-sam-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="view-sam-title" class="modal-title">Call Activity Report</h2>
            </div>

            <div class="modal-body">
                <!-- Show the list of promos. -->
                <div id="sam_row">
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>

        </div>

    </div>
</div>