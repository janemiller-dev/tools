<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/add_call_status.js"></script>

<div class="modal fade" id="add-call-status-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="add-call-status-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="add-call-status-form" class="form-horizontal">

                <div class="modal-header">
                    <h2 id="add-call-status-title" class="modal-title">Add Call Status</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body modal-small">

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="call-status">Call Status</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="call-status" name="call_status"/>
                            <p class="form-text text-muted">Enter Call Status.</p>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" class="close">Close</button>
                    <button type="button" id="save-call-status" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

