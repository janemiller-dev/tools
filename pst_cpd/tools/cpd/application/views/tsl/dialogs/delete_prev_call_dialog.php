<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="delete-past-call-dialog" tabindex="-1" data-backdrop="static" role="dialog"
     aria-labelledby="delete-past-call-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="delete-past-call-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="delete-past-call-title" class="modal-title">Delete all the past calls from TSL?</h2>
                </div>

                <div class="modal-body modal-small">

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="delete-past-call-name">TSL Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <p class="form-control-static" id="delete-past-call-tsl-name"></p>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close
                    </button>
                    <button type="button" class="btn btn-primary" id="delete-past-call-button">Delete</button>
                </div>

            </form>
        </div>
    </div>
</div>
