<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="view-target-audience-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="message-label" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="view-target_audience-title" class="modal-title">View/Edit Location</h2>
                <button type="button" id="add-target-audience-button" class="btn btn-primary bottom-buffer"
                        data-target="#add-target-audience-dialog" data-toggle="modal" data-backdrop="static">
                    Add New Location
                </button>

            </div>

            <div class="modal-body">

                <!-- Show the list of target_audiences. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- List Location table -->
                        <div class="row" id="target-audience-row">
                            <div class="col-xs-12">
                                <p id="no-target-audience">You have no instance of Location. Use the Add Location button
                                    to add a new Location.</p>
                                <div id="target-audience-div" class="top-buffer">
                                    <table class="table" id="target-audience-table">
                                        <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Location Name</th>
                                            <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- List Location table -->
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php $this->partial('views/tsl/dialogs/delete_target_audience.php'); ?>
<?php $this->partial('views/tsl/dialogs/add_target_audience.php'); ?>
