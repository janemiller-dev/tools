<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/choose_criteria.js"></script>

<div class="modal fade" id="choose-criteria-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="choose-client-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="choose-criteria-form" class="form-horizontal">

                <div class="modal-header">
                    <h2 id="choose-criteria-title" class="modal-title">Choose Criteria</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body modal-small">
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="choose-criteria">Select Criteria</label>
                        <div class="col-xs-12 col-sm-9">
                            <select class="form-control" id="choose-criteria" name="criteria_id"></select>
                            <p class="form-text text-muted">Choose Criteria for TSL.</p>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" class="close">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php //$this->partial('views/tsl/dialogs/choose_criteria_category.php'); ?>
