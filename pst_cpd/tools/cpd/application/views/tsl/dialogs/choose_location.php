<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/choose_location.js"></script>

<div class="modal fade" id="choose-location-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="choose-client-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="choose-location-form" class="form-horizontal">

                <div class="modal-header">
                    <h2 id="choose-location-title" class="modal-title">Choose location</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body modal-small">
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="choose-location">Select Location</label>
                        <div class="col-xs-12 col-sm-9">
                            <select class="form-control" id="choose-location" name="location_id"></select>
                            <p class="form-text text-muted">Choose Location for TSL.</p>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" class="close">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
