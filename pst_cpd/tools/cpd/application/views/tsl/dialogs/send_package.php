<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/send_pkg.js"></script>

<div class="modal fade" id="send-pkg-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="send-pkg-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="send-pkg-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="send-pkg-title" class="modal-title">Do you want to send pkg Email?</h2>
                </div>

                <div class="modal-body modal-small">
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="pkg-client-email">To:</label>
                        <div class="col-xs-12 col-sm-9">
                            <p class="form-control-static" id="pkg-client-email"></p>
                        </div>
                        <label class="col-xs-12 col-sm-3 control-label" for="pkg-text">Email Text:</label>
                        <div class="col-xs-12 col-sm-9">
                            <textarea class="form-control" id="pkg-text" name="pkg_text"></textarea>
                        </div>
                    </div>

                    <div class="pkg_overlay" style="display: block">
                        <img src='../img' id="send-pkg-image" class="ajax-loader" style="display: none">
                    </div>
                </div>

                <input type="hidden" id="client-id" name="client_id"/>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="close-send-pkg-dialog" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary" id="send-pkg-button">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>
