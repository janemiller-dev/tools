<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/view_script_archive_dialog.js"></script>

<div class="modal fade" id="view-script-archive-dialog" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="message-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="view-script-focus-title" class="modal-title">Script Archive</h2>
            </div>

            <div class="modal-body">

                <!-- Show the list of script-focuss. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- List scripttional Focus table -->
                        <div class="row" id="script-focus-row">
                            <div class="col-xs-12">
                                <div id="script-focus-div" class="top-buffer">
                                    <table class="table table-responsive table-striped" id="script-archive-table">
                                        <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Script Name</th>
                                            <th class="text-center">Client Name</th>
                                            <th class="text-center">Asset Name</th>
                                            <th class="text-center">Asset Location</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!-- List scripttional Focus table -->
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>