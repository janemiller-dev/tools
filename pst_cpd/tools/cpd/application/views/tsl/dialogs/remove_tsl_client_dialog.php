<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="remove-tsl-client-dialog" tabindex="-1" data-backdrop="static" role="dialog"
     aria-labelledby="remove-tsl-client-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="remove-tsl-client-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="remove-tsl-client-title" class="modal-title">Remove Client-Asset From TSL?</h2>
                </div>

                <div class="modal-body modal-small">

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="delete-tsl-client-name">Client Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <p class="form-control-static" id="delete-tsl-client-name"></p>
                        </div>
                        <label class="col-xs-12 col-sm-3 control-label" for="delete-tsl-asset-name">Asset Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <p class="form-control-static" id="delete-tsl-asset-name"></p>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close
                    </button>
                    <button type="button" class="btn btn-primary" id="remove-tsl-client-button">Remove</button>
                </div>

            </form>
        </div>
    </div>
</div>
