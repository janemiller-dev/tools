<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/add_criteria.js"></script>

<div class="modal fade" id="add-criteria-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="add-client-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="add-criteria-form" class="form-horizontal">

                <div class="modal-header">
                    <h2 id="add-criteria-title" class="modal-title">Add Criteria</h2>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body modal-small">
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="criteria-name">Criteria Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="criteria-name" name="criteria_name"/>
                            <p class="form-text text-muted">Enter the Name of the criteria.</p>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" class="close">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php //$this->partial('views/tsl/dialogs/add_criteria_category.php'); ?>
