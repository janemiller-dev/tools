<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

    <div class="modal fade" id="view-tsl-location-dialog" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="view-location-label"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <h2 id="view-location-title" class="modal-title">Suspect List location</h2>
                    <button type="button" id="choose-location-button" class="btn btn-primary bottom-buffer"
                            data-toggle="modal" data-target="#choose-location-dialog" data-backdrop="static">
                        Choose New Location
                    </button>

                </div>

                <div class="modal-body">

                    <!-- Show the list of promos. -->
                    <div class="row">
                        <div class="col-xs-12">

                            <!-- Deal Promo table -->
                            <div class="row" id="location-row">
                                <div class="col-xs-12">
                                    <p id="no-location">You have no location for the clients. To create one, use
                                        the Add location button.</p>
                                    <div id="location-div" class="top-buffer">
                                        <table class="table table-striped" id="location-table">
                                            <thead>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Location Name</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>

            </div>

        </div>
    </div>

<?php $this->partial('views/tsl/dialogs/choose_location.php'); ?>
