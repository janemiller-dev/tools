<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/add_tsl.js"></script>

<div class="modal fade" id="add-tsl-dialog" tabindex="-1" role="dialog" aria-labelledby="add-tsl-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <form id="add-tsl-form" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-tsl-title" class="modal-title">Add Targeted Suspect List Instance</h2>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <div class="col-xs-6">
                            <div class="col-xs-12 text-center">
                                <label class="control-label" for="tsl-year">Select Year</label>
                            </div>
                            <div class="col-xs-12 text-center">
                                <div>
                                    <select class="form-control" id="tsl-year" name="tsl_year">
                                    </select>
                                </div>
                                <p class="form-text text-muted">Select your TSL Year</p>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="col-xs-12 text-center">
                                <label class="control-label" for="tsl-product-id">Select
                                    Industry</label>
                                <select class="form-control" id="tsl-industry" name="tsl_industry_id">
                                    <option> Commercial Real Estate</option>
                                </select>
                                <p class="form-text text-muted">Select Industry.</p>
                            </div>
                        </div>
                        <div class="col-xs-6">
                            <div class="col-xs-12 text-center">
                                <label class="control-label"
                                       for="tsl-product-id">Select Profession</label>
                            </div>
                            <div class="col-xs-12 text-center">
                                <div>
                                    <select class="form-control" id="tsl-profession" name="tsl_professsion_id">
                                        <option> Investment Sales</option>
                                    </select>
                                </div>
                                <p class="form-text text-muted">Select Profession Type.</p>
                            </div>
                        </div>
                        <div class="col-xs-6 text-center">
                            <div class="col-xs-12 text-center">
                                <label class="control-label" for="tsl-product-id">Select Product
                                    Type</label>
                            </div>
                            <div class="col-xs-12">
                                <div>
                                    <select class="form-control" id="tsl-product-id" name="tsl_product_id">
                                    </select>
                                </div>
                                <p class="form-text text-muted">Select Product Type.</p>
                            </div>
                        </div>
                    </div>

<!--                    <div class="form-group" style="border-bottom: solid 1px #eee">-->
<!--                        <div class="col-xs-6">-->
<!--                            <div class="col-xs-12 text-center">-->
<!--                                <label class="control-label text-center" for="tsl_ui_audience">Select Market-->
<!--                                    Area</label>-->
<!--                            </div>-->
<!--                            <div class="col-xs-12 text-center">-->
<!--                                <select class="select-picker " multiple id="tsl_ui_audience" name="audience[]"></select>-->
<!--                                <p class="form-text text-muted">Select Market Area.</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-xs-6">-->
<!--                            <div class="col-xs-12 text-center">-->
<!--                                <label class="control-label"-->
<!--                                       for="tsl_ui_desc">Select Description</label>-->
<!--                            </div>-->
<!--                            <div class="col-xs-12 text-center">-->
<!--                                <select class="tsl_ui_menu select-picker" id="tsl_ui_desc" name="desc[]"-->
<!--                                        multiple></select>-->
<!--                                <p class="form-text text-muted">Select Description.</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!---->
<!--                        <div class="col-xs-6">-->
<!--                            <div class="col-xs-12 text-center">-->
<!--                                <label class="control-label" for="tsl_ui_cond">Select-->
<!--                                    Condition</label>-->
<!--                            </div>-->
<!--                            <div class="col-xs-12 text-center">-->
<!--                                <select class="tsl_ui_menu select-picker" id="tsl_ui_cond" name="cond[]"-->
<!--                                        multiple></select>-->
<!--                                <p class="form-text text-muted">Select Condition.</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!---->
<!--                        <div class="col-xs-6">-->
<!--                            <div class="col-xs-12 text-center">-->
<!--                                <label class="control-label" for="tsl_ui_criteria">Select Other-->
<!--                                    Criteria</label>-->
<!--                            </div>-->
<!--                            <div class="col-xs-12 text-center">-->
<!--                                <select class="tsl_ui_menu select-picker" id="tsl_ui_criteria" name="criteria[]"-->
<!--                                        multiple></select>-->
<!--                                <p class="form-text text-muted">Select Criteria.</p>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->


                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="col-xs-12 text-center">
                                <label class="control-label" for="tsl_ui_name">Enter TSL Name</label>
                            </div>
                            <div class="col-xs-12 text-center">
                                <input type="text" name="tsl_ui_name" class="form-control" style="margin: auto">
                                <p class="form-text text-muted">Select TSL focus.</p>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="col-xs-12 text-center">
                                <label class="control-label" for="tsl_ui_description">Create TSL
                                    Promo Focus</label>
                            </div>
                            <div class="col-xs-12 text-center">
                                <textarea class="form-control" cols="2" id="tsl-ui-description"
                                          name="tsl_ui_description" style="font-size: 20px">
                                </textarea>
                                <p class="form-text text-muted">Enter TSL promo focus.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <!--                </div>-->

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="create-instance">Create Instance</button>
                </div>
            </form>

        </div>
    </div>
</div>
<?php $this->partial('views/tsl/dialogs/view_asset_used.php'); ?>
