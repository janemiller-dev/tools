<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/send_survey.js"></script>


<div class="modal fade" id="send-bus-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="send-bus-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="send-bus-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="send-bus-title" class="modal-title">Do you want to send Email?</h2>
                </div>

                <div class="modal-body">

                    <div class="form-group" style="margin-bottom: 5px !important;">
                        <label class="col-xs-12 col-sm-3 control-label" for="bus-client-email">To:</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="bus-client-email">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="bus-client-subject">Subject:</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="bus-client-subject">
                        </div>
                    </div>

                    <div id="bus-email-content">
                    </div>

                </div>


                <input type="hidden" id="client-id" name="client_id"/>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="close-send-bus-dialog" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary" id="send-bus-button">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>
