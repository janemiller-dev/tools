<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="view-script-outline-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="message-label" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

                <h2 id="view-outline-title" class="modal-title">View/Delete Outline</h2>

            </div>

            <div class="modal-body">

                <!-- Show the list of outlines. -->
                <div class="row">
                    <div class="col-xs-12">

                        <!-- List Outline -->
                        <div class="row" id="outline-row">
                            <div class="col-xs-12">
                                <div id="outline-div" class="top-buffer">
                                    <table class="table table-striped" id="script-outline-table">
                                        <thead>
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Name</th>
<!--                                            <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>-->
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                        <!-- List Caller table -->
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php $this->partial('views/tsl/dialogs/delete_script_outline.php'); ?>
