<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/view_profile_reader.js"></script>

<div class="modal fade" id="view-profile-reader-dialog" tabindex="-1" data-backdrop="static"
     role="dialog" aria-labelledby="message-label"
     aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="view-profile-reader-title" class="modal-title">SA Profile Readout</h2>
            </div>

            <div class="modal-body">
                <!-- Show the list of profile reader. -->
                <div class="row" id="profile-reader-content">
                    <ul class="nav nav-tabs top-buffer" role="tablist" style="padding: 10px; margin: auto;
                 display: flex; justify-content: center;" id="profile-readout-type">
                        <li role="presentation" class="active">
                            <a href="#project-view" role="tab" data-toggle="tab">Project</a>
                        </li>
                        <li role="presentation">
                            <a href="#challenge-view" role="tab" data-toggle="tab">Challenge</a>
                        </li>
                    </ul>

                    <div class="tab-content profile-readout-tab">
                        <!-- Deal Generation tab -->
                        <div role="tabpanel" class="tab-pane col-xs-12 active" id="project-view">
                            <h4><b>Client Project Section</b></h4>
                            <label>History</label>
                            <p id="history-answer"></p>
                            <label>Current Facts</label>
                            <p id="facts-answer"></p>
                            <label>Accomplished</label>
                            <p id="accomplished-answer"></p>
                            <label>Needed</label>
                            <p id="needed-answer"></p>
                            <label>Possible</label>
                            <p id="possible-answer"></p>
                        </div>
                        <div role="tabpanel" class="tab-pane col-xs-12" id="challenge-view">
                            <h4><b>Client Challenges Section</b></h4>
                            <label>Assets</label>
                            <p id="assets-answer"></p>
                            <label>People</label>
                            <p id="people-answer"></p>
                            <label>Financial</label>
                            <p id="financial-answer"></p>
                            <label>Legal</label>
                            <p id="legal-answer"></p>
                            <label>Market</label>
                            <p id="market-answer"></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>