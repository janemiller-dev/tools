<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="view-prompter-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="message-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <div align="center">
                    <h2 id="view-prompter-title" class="modal-title">Message Prompter</h2>
                </div>
            </div>

            <div class="modal-body" id="prompter-body"
                 style="height: 60vh; overflow-y: hidden; scroll-behavior: smooth">

                <!-- Show the list of promo outlines. -->
                <div class="row" id="titles">
                    <div id="credits" class="col-xs-12" style="font-size: 20px">
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <div class="col-xs-3">
                    <input type="range" min="8" max="40" value="25" id="speed-control" style="width: 6vw;">
                </div>
                <div class="col-xs-9">
                    <a class="btn btn-primary" href="#creditos" name="creditos">Start
                        <i class=" fa fa-play"></i></a>
                    <button type="button" class="btn btn-primary" id="stop" style="margin-right: 10px">
                        Stop <i class="fa fa-stop"></i></button>

                    <button type="button" class="btn btn-primary" id="back">Back
                        <i class="fa fa-arrow-circle-down"></i>
                    </button>
                    <button type="button" class="btn btn-primary" id="next" style="margin-right: 10px">Next
                        <i class="fa fa-arrow-circle-up"></i></button>
                    <button type="button" class="btn btn-primary" id="reset">
                        Reset <i class="fa fa-refresh"></i>
                    </button>
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close <i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>