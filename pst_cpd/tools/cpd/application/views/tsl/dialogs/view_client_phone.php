<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

    <script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/view_client_phone.js"></script>

    <div class="modal fade" id="view-client-phone-dialog" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="view-client-phone-label" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                    <h2 id="view-client-phone-title" class="modal-title">Client Phone</h2>
                    <button type="button" id="add-client-phone-button" class="btn btn-primary bottom-buffer"
                            data-toggle="modal" data-target="#add-client-phone-dialog" data-backdrop="static">
                        Add New Client Phone
                    </button>

                </div>

                <div class="modal-body">

                    <!-- Show the list of promos. -->
                    <div class="row">
                        <div class="col-xs-12">

                            <!-- Deal Promo table -->
                            <div class="row" id="client-phone-row">
                                <div class="col-xs-12">
                                    <div id="client-phone-div" class="top-buffer">
                                        <table class="table table-striped" id="client-phone-table">
                                            <thead>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Phone Type</th>
                                                <th class="text-center">Phone Number</th>
                                                <th class="text-center"><span class="glyphicon glyphicon-trash"></span>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

<?php $this->partial('views/tsl/dialogs/add_client_phone.php'); ?>
<?php $this->partial('views/tsl/dialogs/delete_additional_phone.php'); ?>