<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dialogs/delete_agent.js"></script>

<div class="modal fade" id="delete-agent-dialog" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="delete-agent-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="delete-agent-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="delete-agent-title" class="modal-title">Delete Agent?</h2>
                </div>

                <div class="modal-body modal-small">

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="delete-agent-name">Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <p class="form-control-static" id="delete-agent-name"></p>
                        </div>
                    </div>

                </div>

                <input type="hidden" id="agent-id" name="agent_id"/>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="delete-agent-button">Delete</button>
                </div>

            </form>
        </div>
    </div>
</div>
