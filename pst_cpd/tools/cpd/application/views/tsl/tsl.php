<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/tsl.js"></script>
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/tsl/dashboard.css"/>

<!-- Page Title -->
<div class="row">
    <div class="col-xs-12">
        <h1 style="text-align: center">Targeted Suspect List</h1>
    </div>
    <div class="text-center">
            <span style="font-size: 1.2em;">Active Deal Management Dashboard:&nbsp;
                    <span id="linked-cpd"></span>
    </div>
</div>
<div class="row" style=" border: 1px solid #e1c8c8; padding:10px">
    <div class="col-xs-12">
        <div class="col-sm-12">
            <div class="row">

                <!-- Prepared By text box -->
                <div class="tsl-col-xs-11">
                    <h5 class="col-xs-12 text-center">Last Updated</h5>
                    <input class="col-xs-12 tsl-onlydate form-control" id="tsl-date">
                </div>
                <!-- // Prepared By text box -->

                <!-- Date Text box -->
                <div class="tsl-col-xs-11">
                    <h5 class="col-xs-12 text-center">Market Area</h5>
                    <select class="col-xs-12 form-control tsl_dropdown select-picker" id="tsl-list-target"
                            data-table-name="tsl_target_audience" multiple data-key="tta">
                    </select>
                </div>

                <!-- Suspect List Criteria Text Box -->
                <!--                <div class="tsl-col-xs-11">-->
                <!--                    <h5 class="col-xs-12 text-center">Location</h5>-->
                <!--                    <select class="col-xs-12 form-control tsl_dropdown select-picker" id="tsl-list-loc"-->
                <!--                            data-table-name="tsl_criteria" multiple-->
                <!--                            data-target="view-tsl-criteria-dialog" data-key="tsl_criteria">-->
                <!--                    </select>-->
                <!--                </div>-->
                <!-- // Suspect List Criteria Text Box -->

                <!--Description select box-->
                <!--                <div class="tsl-col-xs-11">-->
                <!--                    <h5 class="col-xs-12 text-center">Description</h5>-->
                <!--                    <select class="col-xs-12 form-control tsl_dropdown select-picker" multiple id="tsl-list-desc"-->
                <!--                            data-table-name="tsl_criteria"-->
                <!--                            data-target="view-tsl-criteria-dialog" data-key="tsl_criteria">-->
                <!--                    </select>-->
                <!--                </div>-->
                <!-- // Description select box-->

                <!--Condition select box-->
                <div class="tsl-col-xs-11">
                    <h5 class="col-xs-12 text-center">Condition</h5>
                    <select class="col-xs-12 form-control tsl_dropdown select-picker" id="tsl-list-cond"
                            data-table-name="tsl_criteria" multiple
                            data-target="view-tsl-criteria-dialog" data-key="tsl_criteria">
                    </select>
                </div>
                <!-- // Condition select box-->

                <!--Criteria select box-->
                <div class="tsl-col-xs-11">
                    <h5 class="col-xs-12 text-center">Criteria</h5>
                    <select class="col-xs-12 form-control tsl_dropdown select-picker list-criteria"
                            id="tsl-list-criteria"
                            data-table-name="tsl_criteria" multiple
                            data-target="view-tsl-criteria-dialog" data-key="tsl_criteria">
                    </select>
                </div>
                <!-- // Criteria select box-->

                <!-- Promo Focus select box.-->
                <div class="tsl-col-xs-11" id="promo-focus-parent" data-toggle="popover"
                     data-placement="top" data-html="true">
                    <h5 class="col-xs-12 text-center">Promo Focus</h5>
                    <select class="col-xs-12 form-control" id="promo-focus"></select>
                </div>
                <!--// Promo Focus select box-->

                <!-- Suspect List Name -->
                <div class="tsl-col-xs-11">
                    <h5 class="col-xs-12 text-center">Suspect List Name</h5>
                    <input class="col-xs-12 form-control" id="tsl-name">
                </div>
                <!-- // Suspect List Name -->

                <!-- Sort By field.-->
                <div class="tsl-col-xs-11" id="promo-focus-parent" data-toggle="popover"
                     data-placement="top" data-html="true">
                    <h5 class="col-xs-12 text-center">Sort By</h5>
                    <select class="form-control" id="sort-by">
                        <option value="">-- None --</option>
                        <option value="cb">Call Back</option>
                        <option value="na">Not Answered</option>
                        <option value="bn">Bad Number</option>
                        <option value="cnt">Contacted</option>
                        <option value="cnv">Conversations</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Show the add tsl button. -->
<div class="row" style="padding: 10px">
    <div class="col-xs-6 ">
        <button type="button" class="btn btn-primary bottom-buffer" id="add-tsl" data-toggle="modal"
                data-target="#add-client-dialog" data-backdrop="static">Add Clients
        </button>

        <button type="button" class="btn btn-primary has-tooltip bottom-buffer" id="reload-tsl"
                title="Load Assets from Homebase."
                data-backdrop="static">Load
        </button>

        <button type="button" class="btn btn-primary bottom-buffer"
                data-html="true"
                data-placement="right" id="tsl-dictionary" data-toggle="popover"
                data-title="Dictionary of TSL Row Color."
                data-content="<div><ul style='list-style-type:none; padding:20px'>
                <li style='border-bottom: 1px solid #e1c8c8; padding: 3px'>
                <img src='https://cpd.powersellingtools.com/wp-content/uploads/2019/08/to_be_contacted.png'>
                : To be contacted
                </li>
                <li style='border-bottom: 1px solid #e1c8c8; padding: 3px'>
                <img src='https://cpd.powersellingtools.com/wp-content/uploads/2019/08/violet.png'>
                : Bad Number(New Number Needed)
                </li>
                <li style='border-bottom: 1px solid #e1c8c8; padding: 3px'>
                <img src='https://cpd.powersellingtools.com/wp-content/uploads/2019/08/yellow.png'>
                : Maybe to a meeting
                </li>
                <li style='border-bottom: 1px solid #e1c8c8; padding: 3px'>
                <img src='https://cpd.powersellingtools.com/wp-content/uploads/2019/08/no.png'>
                : No to a meeting
                </li>
                <li style='border-bottom: 1px solid #e1c8c8; padding: 3px'>
                <img src='https://cpd.powersellingtools.com/wp-content/uploads/2019/08/dialled.png'> : Dialled</li>
                <li style='border-bottom: 1px solid #e1c8c8; padding: 3px'>
                <img src='https://cpd.powersellingtools.com/wp-content/uploads/2019/08/in_progress.png'> : In Progress
                </li>
                </ul></div>">Colors
        </button>

        <button type="button" class="btn btn-primary bottom-buffer has-tooltip" id="begin_session" data-html="true"
                data-toggle="modal" data-container="body" data-title="Call Session Manager"
                data-target="#view-sam-dialog">CSM
        </button>

        <button type="button" class="btn btn-primary bottom-buffer has-tooltip" id="begin_session" data-html="true"
                data-toggle="modal" data-container="body" data-title="Deals Idenitified"
                data-target="#view-deal-identified-dialog">Deals
        </button>

    </div>
    <div class="col-xs-6">
        <button type="button" style="float: right;" class="btn btn-primary bottom-buffer has-tooltip"
                data-toggle="modal" data-target="#delete-past-call-dialog"
                data-title="Delete Past Calls from TSL">Delete Past Calls
        </button>

        <button type="button" style="float: right; margin-right: 4px" class="btn btn-primary bottom-buffer has-tooltip"
                data-html="true"
                data-toggle="modal" data-title="Universal Collaboration."
                data-placement="top" id="tsl-collaboration" data-target="#view-collaboration-dialog">Collaborate
        </button>
        <button type="button" style="float: right; margin-right: 4px" class="btn btn-primary bottom-buffer has-tooltip"
                data-html="true" data-toggle="modal" data-target="#view-alert-dialog"
                data-placement="top" id="tsl-acm" data-title="Accountability Manager"
                data-content="">ACM
        </button>

        <button type="button" style="float: right; margin-right: 4px" class="btn btn-primary bottom-buffer has-tooltip"
                data-html="true" data-title="TSL dials, contacts, conversations, meetings metrics."
                data-toggle="popover" data-placement="top" id="tsl-metric"
                data-content="">Metric
        </button>

        <button type="button" style="float: right; margin-right: 4px" class="btn btn-primary bottom-buffer has-tooltip"
                data-toggle="modal" id="tsl-sam" data-target="#view-tsl-sam-dialog"
                data-title="Call Activity Report">CAR
        </button>

    </div>
</div>


<!-- Show the list of my DMD instances. -->
<div class="row">
    <div class="container-fluid">
        <!-- DMD instance table -->
        <div class="row" class="tsl-instance-row">
            <div class="col-xs-12">
                <ul class="nav nav-tabs top-buffer tsl-tabs" role="tablist">
                    <li role="presentation" style="float: left;" id="referral_tab" data-type="" class="active">
                        <a href="#deal" role="tab" data-toggle="tab">Deals Tab</a>
                    </li>
                    <li role="presentation" style="float: right;" id="buyer_tab" data-type="buyer-">
                        <a href="#buyer" role="tab" data-toggle="tab">
                            <span class="has-tooltip" data-title="Buyer Names List.">Buyers Tab</span></a>
                    </li>
                </ul>

                <p id="no-tsl-instance">You do not have any client added to this TSL instance. To
                    add one, use the Add New Client Instance button.</p>

                <div class="tab-content">
                    <!-- Deal Generation tab -->
                    <div role="tabpanel" class="tab-pane col-xs-12 active" id="deal" style="background: #8ff1fd17;">
                        <div class=" top-buffer table-responsive" id="tsl-instance-div">

                            <table class="table" id="tsl-instance-0-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="21" rowspan="1">
                                    <span class="has-tooltip" style="float: left; font-size: 1.3em; font-style:normal;">
                                        Initial Call
                                    </span>
                                        <span class="tsl_show_more has-tooltip" data-level="0" data-toggle="tooltip"
                                              title="Show More/Less Rows." data-placement="top" id="tsl_show1"
                                              style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dms"></i>
                                    </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th>#</th>

                                    <th>
                                    <span class="has-tooltip" title="Name/Property of the Client"
                                          data-placement="top">Owner Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Criteria For Client" data-placement="top">
                                        Criteria</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Asset Name"
                                          data-placement="top">Asset Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client's Phone number"
                                          data-placement="top">Number</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client-Asset Profile"
                                          data-placement="top">Profile</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Asset Product Type"
                                              data-placement="top">Product Type</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Number of Dials/Contacts/Conversation/Meeting"
                                              data-placement="top">Dial</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">N/A</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">LM</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client Contacted"
                                          data-placement="top">CNT</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">Call Back</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">CNV</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Result of call."
                                              data-placement="top">Result</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Client Call Notes Popup"
                                              data-placement="top">Notes</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Build Components"
                                              data-placement="top">Build</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Push to DMD"
                                              data-placement="top">Submit</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Removes item from TSL."
                                              data-placement="top">Remove</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <table class="table hidden additional-table" id="tsl-instance-1-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="21" rowspan="1">
                                    <span class="has-tooltip" style="float: left; font-size: 1.3em; font-style:normal;">
                                        Second Call
                                    </span>
                                        <span class="tsl_show_more has-tooltip" data-level="1" data-toggle="tooltip"
                                              title="Show More/Less Rows." data-placement="top" id="tsl_show2"
                                              style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dms"></i>
                                    </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th>#</th>
                                    <th>
                                    <span class="has-tooltip" title="Name/Property of the Client"
                                          data-placement="top">Owner Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Criteria For Client" data-placement="top">
                                        Criteria</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Asset Name"
                                          data-placement="top">Asset Name</span>
                                    </th>
                                    <th><span class="has-tooltip" title="Client's Phone number"
                                              data-placement="top">Number</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client-Asset Profile"
                                          data-placement="top">Profile</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Asset Product Type"
                                              data-placement="top">Product Type</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Number of Dials/Contacts/Conversation/Meeting"
                                              data-placement="top">Dial</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">N/A</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">LM</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client Contacted"
                                          data-placement="top">CNT</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">Call Back</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">CNV</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Result of call."
                                              data-placement="top">Result</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Notes Pop-up"
                                              data-placement="top">Notes</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Build Components"
                                              data-placement="top">Build</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Push to DMD"
                                              data-placement="top">Submit</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Removes item from TSL."
                                              data-placement="top">Remove</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <table class="table hidden additional-table" id="tsl-instance-2-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="21" rowspan="1">
                                    <span class="has-tooltip" style="float: left; font-size: 1.3em; font-style:normal;">
                                        Third Call
                                    </span>
                                        <span class="tsl_show_more has-tooltip" data-level="2" data-toggle="tooltip"
                                              title="Show More/Less Rows." data-placement="top" id="tsl_show3"
                                              style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dms"></i>
                                    </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th>#</th>
                                    <th>
                                    <span class="has-tooltip" title="Name/Property of the Client"
                                          data-placement="top">Owner Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Criteria For Client" data-placement="top">
                                        Criteria</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Asset Name"
                                          data-placement="top">Asset Name</span>
                                    </th>
                                    <th><span class="has-tooltip" title="Client's Phone number"
                                              data-placement="top">Number</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client-Asset Profile"
                                          data-placement="top">Profile</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Asset Product Type"
                                              data-placement="top">Product Type</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Number of Dials/Contacts/Conversation/Meeting"
                                              data-placement="top">Dial</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">N/A</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">LM</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client Contacted"
                                          data-placement="top">CNT</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">Call Back</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">CNV</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Result of call."
                                              data-placement="top">Result</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Notes Pop-up"
                                              data-placement="top">Notes</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Build Components"
                                              data-placement="top">Build</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Push to DMD"
                                              data-placement="top">Submit</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Removes item from TSL."
                                              data-placement="top">Remove</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <table class="table hidden additional-table" id="tsl-instance-3-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="21" rowspan="1">
                                    <span class="has-tooltip" style="float: left; font-size: 1.3em; font-style:normal;">
                                        Fourth Call
                                    </span>
                                        <span class="tsl_show_more has-tooltip" data-level="3" data-toggle="tooltip"
                                              title="Show More/Less Rows." data-placement="top" id="tsl_show4"
                                              style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dms"></i>
                                    </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th>#</th>
                                    <th>
                                    <span class="has-tooltip" title="Name/Property of the Client"
                                          data-placement="top">Owner Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Criteria For Client" data-placement="top">
                                        Criteria</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Asset Name"
                                          data-placement="top">Asset Name</span>
                                    </th>
                                    <th><span class="has-tooltip" title="Client's Phone number"
                                              data-placement="top">Number</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client-Asset Profile"
                                          data-placement="top">Profile</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Asset Product Type"
                                              data-placement="top">Product Type</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Number of Dials/Contacts/Conversation/Meeting"
                                              data-placement="top">Dial</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">N/A</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">LM</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client Contacted"
                                          data-placement="top">CNT</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">Call Back</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">CNV</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Result of call."
                                              data-placement="top">Result</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Notes Pop-up"
                                              data-placement="top">Notes</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Build Components"
                                              data-placement="top">Build</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Push to DMD"
                                              data-placement="top">Submit</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Removes item from TSL."
                                              data-placement="top">Remove</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <table class="table hidden additional-table" id="tsl-instance-4-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="21" rowspan="1">
                                    <span class="has-tooltip" style="float: left; font-size: 1.3em; font-style:normal;">
                                        Fifth Call
                                    </span>
                                        <span class="tsl_show_more has-tooltip" data-level="4" data-toggle="tooltip"
                                              title="Show More/Less Rows." data-placement="top" id="tsl_show5"
                                              style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dms"></i>
                                    </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th>#</th>
                                    <th>
                                    <span class="has-tooltip" title="Name/Property of the Client"
                                          data-placement="top">Owner Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Criteria For Client" data-placement="top">
                                        Criteria</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Asset Name"
                                          data-placement="top">Asset Name</span>
                                    </th>
                                    <th><span class="has-tooltip" title="Client's Phone number"
                                              data-placement="top">Number</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client-Asset Profile"
                                          data-placement="top">Profile</span>
                                    </th>

                                    <th>
                                        <span class="has-tooltip" title="Asset Product Type"
                                              data-placement="top">Product Type</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Number of Dials/Contacts/Conversation/Meeting"
                                              data-placement="top">Dial</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">N/A</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">LM</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client Contacted"
                                          data-placement="top">CNT</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">Call Back</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">CNV</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Result of call."
                                              data-placement="top">Result</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Notes Pop-up"
                                              data-placement="top">Notes</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Build Components"
                                              data-placement="top">Build</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Push to DMD"
                                              data-placement="top">Submit</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Removes item from TSL."
                                              data-placement="top">Remove</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <table class="table hidden additional-table" id="tsl-instance-5-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="21" rowspan="1">
                                    <span class="has-tooltip" style="float: left; font-size: 1.3em; font-style:normal;">
                                        Sixth Call
                                    </span>
                                        <span class="tsl_show_more has-tooltip" data-level="5" data-toggle="tooltip"
                                              title="Show More/Less Rows." data-placement="top" id="tsl_show6"
                                              style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dms"></i>
                                    </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th>#</th>
                                    <th>
                                    <span class="has-tooltip" title="Name/Property of the Client"
                                          data-placement="top">Owner Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Criteria For Client" data-placement="top">
                                        Criteria</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Asset Name"
                                          data-placement="top">Asset Name</span>
                                    </th>
                                    <th><span class="has-tooltip" title="Client's Phone number"
                                              data-placement="top">Number</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client-Asset Profile"
                                          data-placement="top">Profile</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Asset Product Type"
                                              data-placement="top">Product Type</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Number of Dials/Contacts/Conversation/Meeting"
                                              data-placement="top">Dial</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">N/A</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">LM</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client Contacted"
                                          data-placement="top">CNT</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">Call Back</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">CNV</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Result of call."
                                              data-placement="top">Result</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Notes Pop-up"
                                              data-placement="top">Notes</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Build Components"
                                              data-placement="top">Build</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Push to DMD"
                                              data-placement="top">Submit</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Removes item from TSL."
                                              data-placement="top">Remove</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <table class="table hidden additional-table" id="tsl-instance-6-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="21" rowspan="1">
                                    <span class="has-tooltip" style="float: left; font-size: 1.3em; font-style:normal;">
                                        Seventh Call
                                    </span>
                                        <span class="tsl_show_more has-tooltip" data-level="6" data-toggle="tooltip"
                                              title="Show More/Less Rows." data-placement="top" id="tsl_show7"
                                              style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dms"></i>
                                    </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th>#</th>
                                    <th>
                                    <span class="has-tooltip" title="Name/Property of the Client"
                                          data-placement="top">Owner Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Criteria For Client" data-placement="top">
                                        Criteria</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Asset Name"
                                          data-placement="top">Asset Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client's Phone number"
                                          data-placement="top">Number</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client-Asset Profile"
                                          data-placement="top">Profile</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Asset Product Type"
                                              data-placement="top">Product Type</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Number of Dials/Contacts/Conversation/Meeting"
                                              data-placement="top">Dial</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">N/A</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">LM</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client Contacted"
                                          data-placement="top">CNT</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">Call Back</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">CNV</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Result of call."
                                              data-placement="top">Result</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Notes Pop-up"
                                              data-placement="top">Notes</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Build Components"
                                              data-placement="top">Build</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Push to DMD"
                                              data-placement="top">Submit</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Removes item from TSL."
                                              data-placement="top">Remove</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <table class="table hidden additional-table" id="tsl-instance-7-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="21" rowspan="1">
                                    <span class="has-tooltip" style="float: left; font-size: 1.3em; font-style:normal;">
                                        Eighth Call
                                    </span>
                                        <span class="tsl_show_more has-tooltip" data-level="7" data-toggle="tooltip"
                                              title="Show More/Less Rows." data-placement="top" id="tsl_show8"
                                              style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dms"></i>
                                    </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th>#</th>
                                    <th>
                                    <span class="has-tooltip" title="Name/Property of the Client"
                                          data-placement="top">Owner Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Criteria For Client" data-placement="top">
                                        Criteria</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Asset Name"
                                          data-placement="top">Asset Name</span>
                                    </th>
                                    <th><span class="has-tooltip" title="Client's Phone number"
                                              data-placement="top">Number</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client-Asset Profile"
                                          data-placement="top">Profile</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Asset Product Type"
                                              data-placement="top">Product Type</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Number of Dials/Contacts/Conversation/Meeting"
                                              data-placement="top">Dial</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">N/A</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">LM</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client Contacted"
                                          data-placement="top">CNT</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">Call Back</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">CNV</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Result of call."
                                              data-placement="top">Result</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Notes Pop-up"
                                              data-placement="top">Notes</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Build Components"
                                              data-placement="top">Build</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Push to DMD"
                                              data-placement="top">Submit</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Removes item from TSL."
                                              data-placement="top">Remove</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <table class="table hidden additional-table" id="tsl-instance-8-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="21" rowspan="1">
                                    <span class="has-tooltip" style="float: left; font-size: 1.3em; font-style:normal;">
                                        Ninth Call
                                    </span>
                                        <span class="tsl_show_more has-tooltip" data-level="8" data-toggle="tooltip"
                                              title="Show More/Less Rows." data-placement="top" id="tsl_show9"
                                              style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dms"></i>
                                    </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th>#</th>
                                    <th>
                                    <span class="has-tooltip" title="Name/Property of the Client"
                                          data-placement="top">Owner Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Criteria For Client" data-placement="top">
                                        Criteria</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Asset Name"
                                          data-placement="top">Asset Name</span>
                                    </th>
                                    <th><span class="has-tooltip" title="Client's Phone number"
                                              data-placement="top">Number</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client-Asset Profile"
                                          data-placement="top">Profile</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Asset Product Type"
                                              data-placement="top">Product Type</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Number of Dials/Contacts/Conversation/Meeting"
                                              data-placement="top">Dial</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">N/A</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">LM</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client Contacted"
                                          data-placement="top">CNT</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">Call Back</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">CNV</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Result of call."
                                              data-placement="top">Result</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Notes Pop-up"
                                              data-placement="top">Notes</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Build Components"
                                              data-placement="top">Build</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Push to DMD"
                                              data-placement="top">Submit</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Removes item from TSL."
                                              data-placement="top">Remove</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <table class="table hidden additional-table" id="tsl-instance-9-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="21" rowspan="1">
                                    <span class="has-tooltip" style="float: left; font-size: 1.3em; font-style:normal;">
                                        Tenth Call
                                    </span>
                                        <span class="tsl_show_more has-tooltip" data-level="9" data-toggle="tooltip"
                                              title="Show More/Less Rows." data-placement="top" id="tsl_show10"
                                              style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dms"></i>
                                    </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th>#</th>
                                    <th>
                                    <span class="has-tooltip" title="Name/Property of the Client"
                                          data-placement="top">Owner Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Criteria For Client" data-placement="top">
                                        Criteria</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Asset Name"
                                          data-placement="top">Asset Name</span>
                                    </th>
                                    <th><span class="has-tooltip" title="Client's Phone number"
                                              data-placement="top">Number</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client-Asset Profile"
                                          data-placement="top">Profile</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Asset Product Type"
                                              data-placement="top">Product Type</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Number of Dials/Contacts/Conversation/Meeting"
                                              data-placement="top">Dial</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">N/A</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">LM</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Client Contacted"
                                          data-placement="top">CNT</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">Call Back</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">CNV</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Result of call."
                                              data-placement="top">Result</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Notes Pop-up"
                                              data-placement="top">Notes</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Build Components"
                                              data-placement="top">Build</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Push to DMD"
                                              data-placement="top">Submit</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Removes item from TSL."
                                              data-placement="top">Remove</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane col-xs-12" id="buyer" style="background: #8ff1fd17;">
                        <div class=" top-buffer table-responsive" id="tsl-buyer-instance-div">

                            <table class="table" id="tsl-buyer-instance-0-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="21" rowspan="1">
                                    <span class="has-tooltip" style="float: left; font-size: 1.3em; font-style:normal;">
                                        Initial Call
                                    </span>
                                        <span class="tsl_show_more has-tooltip" data-level="0" data-toggle="tooltip"
                                              title="Show More/Less Rows." data-placement="top" id="tsl_show1"
                                              style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dms"></i>
                                    </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th>#</th>

                                    <th>
                                    <span class="has-tooltip" title="Name/Property of the Buyer"
                                          data-placement="top">Buyer Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Criteria For Buyer" data-placement="top">
                                        Criteria</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Entity Name"
                                          data-placement="top">Entity Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer's Phone number"
                                          data-placement="top">Number</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer-Entity Profile"
                                          data-placement="top">Profile</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Entity Product Type"
                                              data-placement="top">Product Type</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Number of Dials/Contacts/Conversation/Meeting"
                                              data-placement="top">Dial</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">N/A</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">LM</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer Contacted"
                                          data-placement="top">CNT</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">Call Back</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">CNV</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Result of call."
                                              data-placement="top">Result</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Buyer Call Notes Popup"
                                              data-placement="top">Notes</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Build Components"
                                              data-placement="top">Build</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Push to DMD"
                                              data-placement="top">Submit</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Removes item from TSL."
                                              data-placement="top">Remove</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <table class="table hidden additional-table" id="tsl-buyer-instance-1-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="21" rowspan="1">
                                    <span class="has-tooltip" style="float: left; font-size: 1.3em; font-style:normal;">
                                        Second Call
                                    </span>
                                        <span class="tsl_show_more has-tooltip" data-level="1" data-toggle="tooltip"
                                              title="Show More/Less Rows." data-placement="top" id="tsl_show2"
                                              style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dms"></i>
                                    </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th>#</th>
                                    <th>
                                    <span class="has-tooltip" title="Name/Property of the Buyer"
                                          data-placement="top">Buyer Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Criteria For Buyer" data-placement="top">
                                        Criteria</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Entity Name"
                                          data-placement="top">Entity Name</span>
                                    </th>
                                    <th><span class="has-tooltip" title="Buyer's Phone number"
                                              data-placement="top">Number</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer-Entity Profile"
                                          data-placement="top">Profile</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Entity Product Type"
                                              data-placement="top">Product Type</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Number of Dials/Contacts/Conversation/Meeting"
                                              data-placement="top">Dial</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">N/A</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">LM</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer Contacted"
                                          data-placement="top">CNT</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">Call Back</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">CNV</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Result of call."
                                              data-placement="top">Result</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Notes Pop-up"
                                              data-placement="top">Notes</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Build Components"
                                              data-placement="top">Build</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Push to DMD"
                                              data-placement="top">Submit</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Removes item from TSL."
                                              data-placement="top">Remove</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <table class="table hidden additional-table" id="tsl-buyer-instance-2-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="21" rowspan="1">
                                    <span class="has-tooltip" style="float: left; font-size: 1.3em; font-style:normal;">
                                        Third Call
                                    </span>
                                        <span class="tsl_show_more has-tooltip" data-level="2" data-toggle="tooltip"
                                              title="Show More/Less Rows." data-placement="top" id="tsl_show3"
                                              style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dms"></i>
                                    </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th>#</th>
                                    <th>
                                    <span class="has-tooltip" title="Name/Property of the Buyer"
                                          data-placement="top">Buyer Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Criteria For Buyer" data-placement="top">
                                        Criteria</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Entity Name"
                                          data-placement="top">Entity Name</span>
                                    </th>
                                    <th><span class="has-tooltip" title="Buyer's Phone number"
                                              data-placement="top">Number</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer-Entity Profile"
                                          data-placement="top">Profile</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Entity Product Type"
                                              data-placement="top">Product Type</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Number of Dials/Contacts/Conversation/Meeting"
                                              data-placement="top">Dial</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">N/A</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">LM</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer Contacted"
                                          data-placement="top">CNT</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">Call Back</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">CNV</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Result of call."
                                              data-placement="top">Result</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Notes Pop-up"
                                              data-placement="top">Notes</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Build Components"
                                              data-placement="top">Build</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Push to DMD"
                                              data-placement="top">Submit</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Removes item from TSL."
                                              data-placement="top">Remove</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <table class="table hidden additional-table" id="tsl-buyer-instance-3-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="21" rowspan="1">
                                    <span class="has-tooltip" style="float: left; font-size: 1.3em; font-style:normal;">
                                        Fourth Call
                                    </span>
                                        <span class="tsl_show_more has-tooltip" data-level="3" data-toggle="tooltip"
                                              title="Show More/Less Rows." data-placement="top" id="tsl_show4"
                                              style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dms"></i>
                                    </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th>#</th>
                                    <th>
                                    <span class="has-tooltip" title="Name/Property of the Buyer"
                                          data-placement="top">Buyer Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Criteria For Buyer" data-placement="top">
                                        Criteria</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Entity Name"
                                          data-placement="top">Entity Name</span>
                                    </th>
                                    <th><span class="has-tooltip" title="Buyer's Phone number"
                                              data-placement="top">Number</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer-Entity Profile"
                                          data-placement="top">Profile</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Entity Product Type"
                                              data-placement="top">Product Type</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Number of Dials/Contacts/Conversation/Meeting"
                                              data-placement="top">Dial</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">N/A</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">LM</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer Contacted"
                                          data-placement="top">CNT</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">Call Back</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">CNV</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Result of call."
                                              data-placement="top">Result</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Notes Pop-up"
                                              data-placement="top">Notes</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Build Components"
                                              data-placement="top">Build</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Push to DMD"
                                              data-placement="top">Submit</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Removes item from TSL."
                                              data-placement="top">Remove</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <table class="table hidden additional-table" id="tsl-buyer-instance-4-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="21" rowspan="1">
                                    <span class="has-tooltip" style="float: left; font-size: 1.3em; font-style:normal;">
                                        Fifth Call
                                    </span>
                                        <span class="tsl_show_more has-tooltip" data-level="4" data-toggle="tooltip"
                                              title="Show More/Less Rows." data-placement="top" id="tsl_show5"
                                              style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dms"></i>
                                    </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th>#</th>
                                    <th>
                                    <span class="has-tooltip" title="Name/Property of the Buyer"
                                          data-placement="top">Buyer Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Criteria For Buyer" data-placement="top">
                                        Criteria</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Entity Name"
                                          data-placement="top">Entity Name</span>
                                    </th>
                                    <th><span class="has-tooltip" title="Buyer's Phone number"
                                              data-placement="top">Number</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer-Entity Profile"
                                          data-placement="top">Profile</span>
                                    </th>

                                    <th>
                                        <span class="has-tooltip" title="Entity Product Type"
                                              data-placement="top">Product Type</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Number of Dials/Contacts/Conversation/Meeting"
                                              data-placement="top">Dial</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">N/A</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">LM</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer Contacted"
                                          data-placement="top">CNT</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">Call Back</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">CNV</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Result of call."
                                              data-placement="top">Result</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Notes Pop-up"
                                              data-placement="top">Notes</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Build Components"
                                              data-placement="top">Build</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Push to DMD"
                                              data-placement="top">Submit</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Removes item from TSL."
                                              data-placement="top">Remove</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <table class="table hidden additional-table" id="tsl-buyer-instance-5-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="21" rowspan="1">
                                    <span class="has-tooltip" style="float: left; font-size: 1.3em; font-style:normal;">
                                        Sixth Call
                                    </span>
                                        <span class="tsl_show_more has-tooltip" data-level="5" data-toggle="tooltip"
                                              title="Show More/Less Rows." data-placement="top" id="tsl_show6"
                                              style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dms"></i>
                                    </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th>#</th>
                                    <th>
                                    <span class="has-tooltip" title="Name/Property of the Buyer"
                                          data-placement="top">Buyer Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Criteria For Buyer" data-placement="top">
                                        Criteria</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Entity Name"
                                          data-placement="top">Entity Name</span>
                                    </th>
                                    <th><span class="has-tooltip" title="Buyer's Phone number"
                                              data-placement="top">Number</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer-Entity Profile"
                                          data-placement="top">Profile</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Entity Product Type"
                                              data-placement="top">Product Type</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Number of Dials/Contacts/Conversation/Meeting"
                                              data-placement="top">Dial</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">N/A</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">LM</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer Contacted"
                                          data-placement="top">CNT</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">Call Back</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">CNV</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Result of call."
                                              data-placement="top">Result</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Notes Pop-up"
                                              data-placement="top">Notes</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Build Components"
                                              data-placement="top">Build</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Push to DMD"
                                              data-placement="top">Submit</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Removes item from TSL."
                                              data-placement="top">Remove</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <table class="table hidden additional-table" id="tsl-buyer-instance-6-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="21" rowspan="1">
                                    <span class="has-tooltip" style="float: left; font-size: 1.3em; font-style:normal;">
                                        Seventh Call
                                    </span>
                                        <span class="tsl_show_more has-tooltip" data-level="6" data-toggle="tooltip"
                                              title="Show More/Less Rows." data-placement="top" id="tsl_show7"
                                              style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dms"></i>
                                    </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th>#</th>
                                    <th>
                                    <span class="has-tooltip" title="Name/Property of the Buyer"
                                          data-placement="top">Buyer Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Criteria For Buyer" data-placement="top">
                                        Criteria</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Entity Name"
                                          data-placement="top">Entity Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer's Phone number"
                                          data-placement="top">Number</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer-Entity Profile"
                                          data-placement="top">Profile</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Entity Product Type"
                                              data-placement="top">Product Type</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Number of Dials/Contacts/Conversation/Meeting"
                                              data-placement="top">Dial</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">N/A</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">LM</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer Contacted"
                                          data-placement="top">CNT</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">Call Back</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">CNV</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Result of call."
                                              data-placement="top">Result</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Notes Pop-up"
                                              data-placement="top">Notes</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Build Components"
                                              data-placement="top">Build</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Push to DMD"
                                              data-placement="top">Submit</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Removes item from TSL."
                                              data-placement="top">Remove</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <table class="table hidden additional-table" id="tsl-buyer-instance-7-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="21" rowspan="1">
                                    <span class="has-tooltip" style="float: left; font-size: 1.3em; font-style:normal;">
                                        Eighth Call
                                    </span>
                                        <span class="tsl_show_more has-tooltip" data-level="7" data-toggle="tooltip"
                                              title="Show More/Less Rows." data-placement="top" id="tsl_show8"
                                              style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dms"></i>
                                    </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th>#</th>
                                    <th>
                                    <span class="has-tooltip" title="Name of the Buyer"
                                          data-placement="top">Buyer Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Criteria For Buyer" data-placement="top">
                                        Criteria</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Entity Name"
                                          data-placement="top">Entity Name</span>
                                    </th>
                                    <th><span class="has-tooltip" title="Buyer's Phone number"
                                              data-placement="top">Number</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer-Entity Profile"
                                          data-placement="top">Profile</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Entity Product Type"
                                              data-placement="top">Product Type</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Number of Dials/Contacts/Conversation/Meeting"
                                              data-placement="top">Dial</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">N/A</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">LM</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer Contacted"
                                          data-placement="top">CNT</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">Call Back</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">CNV</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Result of call."
                                              data-placement="top">Result</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Notes Pop-up"
                                              data-placement="top">Notes</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Build Components"
                                              data-placement="top">Build</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Push to DMD"
                                              data-placement="top">Submit</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Removes item from TSL."
                                              data-placement="top">Remove</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <table class="table hidden additional-table" id="tsl-buyer-instance-8-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="21" rowspan="1">
                                    <span class="has-tooltip" style="float: left; font-size: 1.3em; font-style:normal;">
                                        Ninth Call
                                    </span>
                                        <span class="tsl_show_more has-tooltip" data-level="8" data-toggle="tooltip"
                                              title="Show More/Less Rows." data-placement="top" id="tsl_show9"
                                              style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dms"></i>
                                    </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th>#</th>
                                    <th>
                                    <span class="has-tooltip" title="Name of the Buyer"
                                          data-placement="top">Buyer Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Criteria For Buyer" data-placement="top">
                                        Criteria</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Entity Name"
                                          data-placement="top">Entity Name</span>
                                    </th>
                                    <th><span class="has-tooltip" title="Buyer's Phone number"
                                              data-placement="top">Number</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer-Entity Profile"
                                          data-placement="top">Profile</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Entity Product Type"
                                              data-placement="top">Product Type</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Number of Dials/Contacts/Conversation/Meeting"
                                              data-placement="top">Dial</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">N/A</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">LM</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer Contacted"
                                          data-placement="top">CNT</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">Call Back</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">CNV</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Result of call."
                                              data-placement="top">Result</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Notes Pop-up"
                                              data-placement="top">Notes</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Build Components"
                                              data-placement="top">Build</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Push to DMD"
                                              data-placement="top">Submit</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Removes item from TSL."
                                              data-placement="top">Remove</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <table class="table hidden additional-table" id="tsl-buyer-instance-9-table">
                                <thead>
                                <tr class="table-name">
                                    <th colspan="21" rowspan="1">
                                    <span class="has-tooltip" style="float: left; font-size: 1.3em; font-style:normal;">
                                        Tenth Call
                                    </span>
                                        <span class="tsl_show_more has-tooltip" data-level="9" data-toggle="tooltip"
                                              title="Show More/Less Rows." data-placement="top" id="tsl_show10"
                                              style="float: right;">
                                            <i class="collapse-icon fa fa-plus-circle" id="collapse_dms"></i>
                                    </span>
                                    </th>
                                </tr>
                                <tr class="table-heading">
                                    <th>#</th>
                                    <th>
                                    <span class="has-tooltip" title="Name of the Buyer"
                                          data-placement="top">Buyer Name</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Criteria For Buyer" data-placement="top">
                                        Criteria</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Entity Name"
                                          data-placement="top">Entity Name</span>
                                    </th>
                                    <th><span class="has-tooltip" title="Buyer's Phone number"
                                              data-placement="top">Number</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer-Entity Profile"
                                          data-placement="top">Profile</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Entity Product Type"
                                              data-placement="top">Product Type</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Number of Dials/Contacts/Conversation/Meeting"
                                              data-placement="top">Dial</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">N/A</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Phone Not Answered"
                                          data-placement="top">LM</span>
                                    </th>
                                    <th>
                                    <span class="has-tooltip" title="Buyer Contacted"
                                          data-placement="top">CNT</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">Call Back</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Conversation"
                                              data-placement="top">CNV</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Result of call."
                                              data-placement="top">Result</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Notes Pop-up"
                                              data-placement="top">Notes</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Build Components"
                                              data-placement="top">Build</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Push to DMD"
                                              data-placement="top">Submit</span>
                                    </th>
                                    <th>
                                        <span class="has-tooltip" title="Removes item from TSL."
                                              data-placement="top">Remove</span>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Dummy Table Content-->
<table class="">
    <tr class="maintainer sample-tr" style="visibility: hidden">
        <td>
            1
        </td>

        <td>
            <input type="text" class="form-control date-ref" value="Owner Name">
        </td>

        <td>
            <input type="text" class="form-control date-ref" value="Asset Name">
        </td>

        <td>
            <div class="input-group">
                <select class="form-control date-ref phone-field"
                        style="min-width: 7vw; max-width: none">
                    <option>-- Phone --</option>
                </select>
                <span class="input-group-btn">
                    <span class="has-tooltip" title="Mark as Bad Number">
                        <button class="btn btn-default" type="button"><i
                                    class="fa fa-chain-broken"></i>
                        </button>
                    </span>
                </span>
            </div>
        </td>

        <td>
            <button class="btn btn-default btn-sm" data-toggle="modal" title="Dummy Row" data-html="true"
                    data-target="#client-criteria-dialog" data-id="-1">
                <span class="glyphicon glyphicon-list"></span>
            </button>
        </td>

        <td>
            <select class="form-control date-ref">
                <option>--Product Type--</option>
            </select>
        </td>

        <td>
            <button class="btn btn-default btn-sm" data-toggle="popover" title="Dummy Row" data-html="true"
                    data-content="This is Dummy button.<br> Please Add new clients using Add New Clients Button
                        to access full features of the application..">
                <span class="glyphicon glyphicon-phone-alt"></span>
            </button>
        </td>

        <td>
            <input type="radio" name="cnt0" checked>
        </td>

        <td>
            <input type="text" class="form-control" disabled style="width: 2vw; min-width:0" value="0">
        </td>
        <td>
            <input type="radio" name="cnt0">
        </td>

        <td>
            <input type="radio" name="cnt0">
        </td>

        <td>
            <input type="text" class="form-control date-ref" value="By When Date">
        </td>

        <td>
            <input type="radio" name="cnt0">
        </td>

        <td>
            <select class="form-control date-ref">
                <option disabled="" selected="" value="-1">--Result--</option>
            </select>
        </td>
        <td>
            <button class="btn btn-default btn-sm" data-toggle="modal" data-target="#view-notes-dialog" data-id="0">
                <span class="glyphicon glyphicon-list-alt"></span>
            </button>
        </td>

        <td>
            <div class="input-group">
                <input class="form-control date-ref phone-field" disabled
                       style="min-width: 7vw; max-width: none" placeholder="Confirm Date">
                <span class="input-group-btn">
                    <span class="has-tooltip" title="Cofirm Status Prompter.">
                        <button class="btn btn-default" type="button"><i
                                    class="glyphicon glyphicon-list-alt"></i>
                        </button>
                    </span>
                </span>
            </div>
        </td>

        <td>
            <button class="btn btn-primary" type="button" data-toggle="popover" title="Dummy Row" data-html="true"
                    data-content="This is Dummy button.<br> Please Add new clients using Add New Clients Button
                        to access full features of the application.." data-placement="left">
                <span class="glyphicon glyphicon-ok"></span>
            </button>
        </td>
    </tr>
</table>
<!-- // Dummy Table Content-->

<!-- Dialogs -->
<?php $this->partial('views/cpdv1/dialogs/client_info.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_caller.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_client_phone.php'); ?>
<?php //$this->partial('views/homebase/dialogs/add_client.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_agent.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_confirm.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_call_status.php'); ?>
<?php //$this->partial('views/tsl/dialogs/client_criteria.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_tsl_sam.php'); ?>
<?php $this->partial('views/tsl/dialogs/save-client-info.php'); ?>
<?php $this->partial('views/dialogs/dial.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_promo_focus_dialog.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_tsl_location.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_sam.php'); ?>
<?php $this->partial('views/tsl/dialogs/remove_tsl_client_dialog.php'); ?>
<?php $this->partial('views/tsl/dialogs/delete_prev_call_dialog.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_tsl_asset_criteria.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_call_notes.php'); ?>
<?php $this->partial('views/cpdv1/dialogs/cpd_ccm.php'); ?>
<?php $this->partial('views/dialogs/view_deal_identified.php'); ?>
