<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/tsl/message.css"/>
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/trix/dist/trix.css">

<div class="row">
    <div class="col-xs-12">
        <!-- Back Button-->
        <div class="col-xs-4">
            <a class="btn btn-secondary back-button" style="float: right; padding-right: 25px">
                <img src="http://cpd.powersellingtools.com/wp-content/uploads/2020/01/Component-Arrow-Back.png"
                     style="height: 5vh; width: 70%;" alt="back"></a>
        </div>
        <!--// Back Button-->

        <!--// Collaboration Button-->
        <div class="col-xs-4">
            <h2 id="component-heading" style="text-align:center; text-transform:capitalize;" class="heading"></h2>
        </div>

        <!-- Collaboration Button-->
        <div class="col-xs-4">
            <a class="btn btn-secondary collab-button" style="float: right; padding-right: 25px">
                <img src="http://cpd.powersellingtools.com/wp-content/uploads/2020/01/Collaboration-Arrows-2.png"
                     style="height: 5vh; width: 70%;" alt="collaboration">
            </a>
        </div>
    </div>
    <div class="col-xs-12" id="component_header">
    </div>
</div>
<hr>
<div class="row">
    <div class="col-xs-12">
        <!-- Version Container   -->
        <div class="col-xs-15 full-view-hide">
            <div class="sidebar" id="outline-container" style="word-break: break-all; text-transform:capitalize;">
            </div>
        </div>
        <!--// Version Container   -->

        <!-- Component Container -->
        <div class="sidebar col-xs-60" id="component-container"
             style="padding: 0px; overflow: hidden !important; margin: 0 1% 0 1%"></div>
        <!--// Component Container -->

        <!-- Sequence Container -->
        <div class="col-xs-15 full-view-hide">
            <div class="sidebar" style="padding-left: 2px">
                <p id="component-version" style="text-transform:capitalize;">
                </p>
            </div>
        </div>
        <!--// Sequence Container -->
    </div>
</div>

<!-- Message Buttons -->
<div class="row" style="margin-top: 1vh">

    <!-- Version Buttons-->
    <div class="col-xs-15" id="promo_buttons full-view-hide" align="center">
        <button id="view-msg-outline" class="btn btn-secondary has-tooltip full-view-hide" data-toggle="popover"
                title="Message Outlines List." data-html="true" data-placement="top">List <i
                    class="fa fa-list"></i>
            </span>
        </button>
        <button id="edit-msg-outline" class="btn btn-secondary full-view-hide" title="Edit Message Outline">Edit <i
                    class="fa fa-pencil"></i></button>
        <button id="save-msg-outline" class="btn btn-secondary full-view-hide" title="Save message Outline as new one.">
            Save <i class="fa fa-check"></i>
        </button>
    </div>
    <!--// Version Buttons-->

    <!-- Content Buttons-->
    <div class="col-xs-60" id="collab-button-row" align="center">

        <button class="btn btn-secondary" id="create-message-version">Compose <i
                    class="fa fa-plus"></i></button>
        <button id="clear-msg" class="btn btn-secondary">Clear <i class="fa fa-eraser"></i></button>
        <button id="save-msg-seq" class="btn btn-secondary full-view-hide">Save
            <i class="fa fa-check"></i>
        </button>
        <button id="save-as-new-msg-seq" class="btn btn-secondary full-view-hide">Save as New
            <i class="fa fa-check"></i>
        </button>
<!--        <button id="view-msg" class="btn btn-secondary">Read <i class="fa fa-eye"></i></button>-->
        <button id="collaborate-promo" class="btn btn-secondary">Collaborate <i class="fa fa-expand"></i></button>
        <button class="btn btn-secondary" id="propagate-promo">Propagate <i class="fa fa-exchange"></i></button>
        <button class="btn btn-secondary" id="prompt-msg">Speak <i class="fa fa-envelope"></i></button>
        <!--                <button class="btn btn-secondary" id="spoken-sequence">Spoken <i class="fa fa-check"></i></button>-->
    </div>
    <!--// Content Buttons-->

    <!-- Sequence Buttons -->
    <div class="col-xs-15 full-view-hide" align="center">
        <!--        <button class="btn btn-secondary" id="create-message-sequence">Add <i class="fa fa-plus"></i></button>-->
        <button class="btn btn-secondary add-to-alert" data-type="Message">
            Alert <i class="fa fa-plus-square-o"></i></button>
        <button class="btn btn-secondary has-tooltip full-view-hide" id="view-message-archive" title=""
                data-toggle="modal" data-target="#view-promo-archive-dialog"
                data-original-title="Lists all Archived Promo components.">List <i
                    class="fa fa-list"></i></button>
        <button class="btn btn-secondary has-tooltip full-view-hide" id="add-message-archive" title=""
                data-original-title="Adds a Promo version to Archive list.">Archive <i class="fa fa-archive"></i>
        </button>
    </div>
    <!--// Sequence Buttons -->

</div>

<script src="<?php echo $this->basepath; ?>resources/trix/dist/trix.js"></script>
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/message.js"></script>

<?php $this->partial('views/tsl/dialogs/delete_seq.php'); ?>
<?php $this->partial('views/tsl/dialogs/delete_msg.php'); ?>
<?php $this->partial('views/tsl/dialogs/delete_msg_outline_dialog.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_promo_outline.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_criteria.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_prompter.php'); ?>
