<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/tsl/email.css"/>
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/trix/dist/trix.css">

<!--Top menu-->
<div class="row">
    <div class="col-xs-12">
        <!-- Back Button-->
        <div class="col-xs-4">
            <a class="btn btn-secondary back-button" style="float: right; padding-right: 25px">
                <img src="http://cpd.powersellingtools.com/wp-content/uploads/2020/01/Component-Arrow-Back.png"
                     style="height: 5vh; width: 70%;" alt="back"></a>
        </div>
        <!--// Back Button-->

        <div class="col-xs-4">
            <h2 id="tsl-email-heading" style="text-align:center" class="heading"></h2>
        </div>

        <!-- Collaboration Button-->
        <div class="col-xs-4">
            <a class="btn btn-secondary collab-button" style="float: right; padding-right: 25px">
                <img src="http://cpd.powersellingtools.com/wp-content/uploads/2020/01/Collaboration-Arrows-2.png"
                     style="height: 5vh; width: 70%;" alt="collaboration">
            </a>
        </div>
        <!--// Collaboration Button-->
    </div>
    <div class="col-xs-12" id="email_header">
    </div>
</div>
<hr>
<!--Container for outline.-->
<div class="row">
    <div class="col-xs-12">
        <div class="col-xs-15">
            <div class="sidebar" id="outline-container" style="padding-left: 2px">
                <p id="tsl-outline">
                </p>
            </div>
        </div>

        <div class="col-xs-60">
            <div class="row">
                <div class="col-md-12 container">
                    <div class="sidebar" id="canvas-container"></div>
                </div>
            </div>

        </div>

        <div class="col-xs-15">
            <div class="sidebar" id="container-listing" style="word-break: break-all;">
            </div>
        </div>
    </div>
</div>


<!--Email Buttons-->
<div class="row" style="margin-top:1vh">
    <div class="col-xs-15" align="center">
                <button id="view-email-outline" class="btn btn-secondary">List <i class="fa fa-list"></i>
                </button>
        <button id="edit-email-outline" class="btn btn-secondary">Edit <i class="fa fa fa-pencil"></i>
                </button>
                <button id="save-email-outline" class="btn btn-secondary">Save <i class="fa fa-check"></i></button>
    </div>
    <div class="col-xs-60" align="center">
        <button id="compose-email" class="btn btn-secondary">Compose <i
                    class="fa fa-plus"></i></button>
        <button id="clear-email" class="btn btn-secondary">Clear <i
                    class="fa fa-eraser"></i></button>
        <button id="save-email" class="btn btn-secondary">Save <i class="fa fa-check"></i></button>
        <button id="save-new-email" class="btn btn-secondary">Save As New <i class="fa fa-check"></i></button>
        <button id="propagate-email" class="btn btn-secondary">Collaborate <i class="fa fa-exchange"></i></button>
        <button id="propagate-email" class="btn btn-secondary">Propagate <i class="fa fa-exchange"></i></button>
        <button id="send-email" class="btn-secondary btn">Send <i class="fa fa-envelope"></i></button>
    </div>
    <div class="col-xs-15" align="center">
        <button id="list-script-archive" class="btn btn-secondary full-view-hide" data-toggle="modal"
                title="Lists archived Email Version" data-target="#view-email-archive-dialog">
            Alert <i class="fa fa-bell"></i>
        </button>
        <button id="add-email-archive" class="btn btn-secondary full-view-hide"
                title="Adds a Email version to Archive list.">
            Archive <i class="fa fa-trash"></i>
        </button>
        <button id="delete-email" class="btn btn-secondary">Delete <i class="fa fa-trash"></i></button>
    </div>
</div>
<!-- // Email Buttons-->


<?php $this->partial('views/tsl/dialogs/delete_email.php'); ?>
<?php $this->partial('views/tsl/dialogs/send_email.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_email_outline.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_promo_focus_dialog.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_email_archive_dialog.php'); ?>

<?php $this->partial('views/tsl/dialogs/view_criteria.php'); ?>

<script src="<?php echo $this->basepath; ?>resources/trix/dist/trix.js"></script>
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/email.js"></script>
<script src="//mozilla.github.io/pdf.js/build/pdf.js"></script>