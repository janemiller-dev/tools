<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/dashboard.js"></script>

<!-- Page Title -->
<div class="row">
    <div class="col-xs-6">
        <h2>Targeted Suspect List</h2>
    </div>

        <!-- Product Type Drop down -->
        <form class="navbar-form pull-right col-xs-6" style="z-index: 1">
            <label>Product Type: </label>
            <span class="has-tooltip" data-toggle="tooltip"
                  title="Change Product type to access tools for that Product type.."
                  data-placement="bottom"
                  id="product_tooltip">
            <select class="form-control" id="current-product" name="current_product">
            </select>
            </span>
        </form>
        <!-- // Product Type Drop down -->
</div>

<!-- Show the add tsl button. -->
<div class="row">
    <div class="col-xs-6 text-left">
        <button type="button" id="add-tsl" class="btn btn-primary bottom-buffer" data-toggle="modal"
                data-target="#add-tsl-dialog" data-backdrop="static">Add New Suspect List
        </button>
    </div>
</div>


<!-- Show the list of my TSL instances. -->
<div class="row">
    <div class="col-xs-12">

        <!-- TSL primary product instance table -->
        <div class="row tsl-instance-row">
            <div class="col-xs-12">
                <div id="tsl-instance-div" class="top-buffer">
                    <h4 id="table-header-1" style="font-weight: bold"></h4>
                    <table class="table table-striped" id="tsl-instance-table-1"
                           style="border: 1px solid rgb(225, 200, 200);">
                        <thead>
                        <tr>
                            <th>TSL ID</th>
                            <th>Name</th>
                            <th>Promo Focus</th>
                            <th>Location</th>
                            <th>Suspect List Criteria</th>
                            <th>Prepared By</th>
                            <th>Created</th>
                            <th>Modified</th>
                            <th>Active</th>
                            <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--// TSL primary product instance table -->

        <!-- TSL demo product instance table -->
        <div class="row tsl-instance-demo-row hidden">
            <hr>
            <div class="col-xs-12">
                <div id="tsl-instance-div" class="top-buffer">
                    <h4 id="table-demo-header" style="font-weight: bold"></h4>
                    <table class="table table-striped" id="tsl-instance-demo-table"
                           style="border: 1px solid rgb(225, 200, 200);">
                        <thead>
                        <tr>
                            <th>TSL ID</th>
                            <th>Name</th>
                            <th>Promo Focus</th>
                            <th>Location</th>
                            <th>Suspect List Criteria</th>
                            <th>Prepared By</th>
                            <th>Created</th>
                            <th>Modified</th>
                            <th>Active</th>
                            <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!--// TSL demo product instance table -->

    </div>
</div>

<!-- Dialogs -->
<?php $this->partial('views/tsl/dialogs/add_tsl.php'); ?>
<?php $this->partial('views/tsl/dialogs/delete_tsl.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_criteria.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_agent.php'); ?>
<?php $this->partial('views/tsl/dialogs/delete_asset_usage.php'); ?>
