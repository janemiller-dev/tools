<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/tsl/email.css"/>
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/trix/dist/trix.css">

<div class="row full_view_hide">
    <div class="col-xs-12">
        <!-- Back Button-->
        <div class="col-xs-4">
            <a class="btn btn-secondary back-button" style="float: right; padding-right: 25px">
                <img src="http://cpd.powersellingtools.com/wp-content/uploads/2020/01/Component-Arrow-Back.png"
                     style="height: 5vh; width: 70%;" alt="back"></a>
        </div>
        <!--// Back Button-->

        <!--// Collaboration Button-->
        <div class="col-xs-4">
            <h2 id="tsl-survey-heading" style="text-align:center" class="heading">Survey Builder</h2>
        </div>

        <!-- Collaboration Button-->
        <div class="col-xs-4">
            <a class="btn btn-secondary collab-button" style="float: right; padding-right: 25px">
                <img src="http://cpd.powersellingtools.com/wp-content/uploads/2020/01/Collaboration-Arrows-2.png"
                     style="height: 5vh; width: 70%;" alt="collaboration">
            </a>
        </div>
    </div>
    <div class="col-xs-12">
        <div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">
            <label class="col-xs-12" style="text-align: center">Client Type:</label>
            <select class="form-control survey-menu" id="client-type">
                <option value="r">Radish</option>
                <option value="w">Wheat</option>
                <option value="t">Tree</option>
            </select>
        </div>
        <div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">
            <label class="col-xs-12" style="text-align: center">Survey Type:</label>
            <select class="form-control survey-menu" id="survey-class">
                <option value="1">Market Survey</option>
                <option value="2">Comps Survey</option>
                <option value="3">Demand Survey</option>
                <option value="4">Operations Survey</option>
                <option value="4">Value Survey</option>
            </select>
        </div>
        <!--        <div class="col-lg-3 col-xs-12 col-sm-12 col-md-3 script-box">-->
        <!--            <label class="col-xs-12" style="text-align: center">Survey Type:</label>-->
        <!--            <select class="form-control survey-menu" id="survey-type">-->
        <!--                <option value="bus">Survey Type 1 – Product Distribution</option>-->
        <!--                <option value="ows">Survey Type 2 – Investor Demand</option>-->
        <!--                <option value="les">Survey Type 3 – Demographic Trends</option>-->
        <!--                <option value="ins">Survey Type 4 – Convenience Mix</option>-->
        <!--            </select>-->
        <!--        </div>-->
        <!--Select box for survey outlines-->
        <div class="col-lg-4 col-xs-12 col-sm-12 col-md-4 script-box">
            <label class="col-xs-12" style="text-align: center">Survey Phase:</label>
            <select class="form-control" id="survey-phase">
<!--                <optgroup data-val="0" label="Page 1 - Status Page">-->
<!--                    <option value="1">Phase 1 - Current Factors</option>-->
<!--                    <option value="1">Phase 2 - Emerging Challenges</option>-->
<!--                    <option value="1">Phase 3 – Future Implications</option>-->
<!--                </optgroup>-->
<!--                <optgroup data-val="3" label="Page 2 – Strategy Page">-->
<!--                    <option value="2">Phase 4 – Known Options</option>-->
<!--                    <option value="2">Phase 5 – Plan of Action</option>-->
<!--                </optgroup>-->
            </select>
        </div>
    </div>
</div>

<hr>
<!--Container for outline.-->
<div class="row">
    <div class="col-xs-3 full_view_hide">
        <div class="sidebar" id="outline-container" style="padding-left: 2px">
            <p id="tsl-outline">
            </p>
        </div>
    </div>
    <div class="col-md-7 survey_container">
        <div class="row">
            <div class="col-md-12 container">
                <div class="sidebar" id="canvas-container"></div>
            </div>
        </div>

    </div>
    <div class="col-md-2 full_view_hide">
        <div class="sidebar" id="container-listing" style="word-break: break-all;">
        </div>
    </div>
</div>

<!-- Buyer Survey Buttons -->
<div class="row" style="margin-top: 1vh" id="bus_buttons">
    <div class="col-xs-3" align="center">
        <button id="view-bus-outline" class="btn btn-secondary full_view_hide"
                data-toggle="modal" data-target="#view-outline-dialog">List
            <i class="fa fa-list"></i>
        </button>
        <button id="save-bus-outline" class="btn btn-secondary full_view_hide">Save
            <i class="fa fa-check"></i></button>
    </div>

    <div class="col-xs-7" align="center">
        <button id="compose-bus" class="btn btn-secondary full_view_hide">Compose <i class="fa fa-plus"></i></button>
        <button id="expand-bus" class="btn btn-secondary">Expand <i class="fa fa-arrows""></i></button>
        <button id="save-bus" class="btn btn-secondary">Save <i class="fa fa-check"></i></button>
    </div>

    <div class="col-xs-2" align="center">
        <button id="delete-bus" class="btn btn-secondary full_view_hide">Delete <i
                    class="fa fa-trash"></i></button>
        <button id="send-bus" class="btn-secondary btn  full_view_hide">Send <i
                    class="fa fa-envelope"></i></button>
    </div>
</div>

<script src="<?php echo $this->basepath; ?>resources/trix/dist/trix.js"></script>
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/survey.js"></script>

<?php $this->partial('views/tsl/dialogs/send_survey.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_promo_focus_dialog.php'); ?>
<?php $this->partial('views/tsl/dialogs/delete_survey.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_survey_outline.php'); ?>

