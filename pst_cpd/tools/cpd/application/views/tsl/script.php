<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/trix/dist/trix.css">
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/tsl/script.css">
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/script.js"></script>

<div class="row script_head full-view-hide">
    <div class="col-xs-12">
        <!-- Back Button-->
        <div class="col-xs-4">
            <a class="btn btn-secondary back-button" style="float: right; padding-right: 25px">
                <img src="http://cpd.powersellingtools.com/wp-content/uploads/2020/01/Component-Arrow-Back.png"
                     style="height: 5vh; width: 70%;" alt="back"></a>
        </div>
        <!--// Back Button-->

        <div class="col-xs-4">
            <h2 id="tsl-script-heading" class="heading" style="margin-left: -40px"></h2>
        </div>

        <!-- Collaboration Button-->
        <div class="col-xs-4">
            <a class="btn btn-secondary collab-button" style="float: right; padding-right: 25px">
                <img src="http://cpd.powersellingtools.com/wp-content/uploads/2020/01/Collaboration-Arrows-2.png"
                     style="height: 5vh; width: 70%;" alt="collaboration">
            </a>
        </div>
        <!--// Collaboration Button-->
    </div>
    <div class="col-xs-12 dropdown" id="script_header">
    </div>
</div>
<hr>
<div class="ynm-row">
    <div class="col-xs-15 script_outline full-view-hide">
        <div class="sidebar" style="padding-left: 2px">
            <p id="script-outline">
            </p>
        </div>

        <div class="col-xs-12" align="center" style="margin-top: 1vh">
            <button id="view-script-outline" class="btn btn-secondary full-view-hide">List <i class="fa fa-list"></i>
            </button>
            <button id="view-script" class="btn btn-secondary full-view-hide script_button" title="Swap between Outlines
             and Description.">Edit <i class="fa fa-pencil"></i>
            </button>
            <button id="save-script-outline" class="btn btn-secondary full-view-hide">Save <i
                        class="fa fa-check"></i>
            </button>
        </div>
    </div>
    <div class="col-xs-60 script_container">
        <div class="row ynm-row">
            <div class="col-md-12 container">
                <div class="sidebar" id="script-container"></div>
            </div>

            <div class="col-xs-12" align="center" style="margin-top: 1vh">
                <button id="compose-script" class="btn btn-secondary full-view-hide script_button"
                        title="Creates a new Script Version">Compose <i class="fa fa-plus"></i>
                </button>
                <button id="clear-text" class="btn btn-secondary script_button" title="Clears Out Script Content">
                    Clear <i class="fa fa-eraser"></i></button>
                <button id="save-script" class="btn btn-secondary col-xs-offset-0 script_button"
                        title="Saves Script Content">Save <i class="fa fa-check"></i>
                </button>
                <button id="save-script" class="btn btn-secondary col-xs-offset-0 script_button"
                        title="Saves Script Content">Save As New<i class="fa fa-check"></i>
                </button>
                <!--                <button id="propagate-script" class="btn btn-secondary script_button has-tooltip"-->
                <!--                        title="Propagates Script Content.">Propagate <i class="fa fa-exchange"></i></button>-->
<!--                <button id="view-recording" class="btn btn-secondary script_button" data-toggle="modal"-->
<!--                        data-target="#view-recording-dialog" title="Lists all the call recordings present">-->
<!--                    Recording <i class="fa fa-file-audio-o "></i>-->
<!--                </button>-->
<!--                <button id="filter" class="btn btn-secondary full-view-hide script_button"-->
<!--                        title="Filter out Collaboration requests." style="display: none;">Filter <i-->
<!--                            class="fa fa-filter"></i></button>-->
<!--                <button id="record" class="btn btn-secondary full-view-hide script_button" style="display: none;"-->
<!--                        title="Records the audio">Record <i class="fa fa-microphone"></i></button>-->
<!--                <button id="stop_record" class="btn btn-secondary script_button" style="display:none"-->
<!--                        title="Stops recording audio.">Stop <i class="fa fa-microphone-slash"></i></button>-->
<!--                <button id="play_record" class="btn btn-secondary script_button" style="display:none"-->
<!--                        title="Plays last recorded audio.">Play <i class="fa fa-play"></i></button>-->
<!--                <button id="download_record" class="btn btn-secondary script_button" style="display:none"-->
<!--                        title="Downloads the copy of last recorded audio">Download <i class="fa fa-download">-->
<!--                    </i></button>-->
                <button id="cpd_full_view" type="button" class="btn btn-secondary script_button"
                        title="Brings the Script fullscreen view up.">Collaborate <i class="fa fa-expand"></i></button>
                <button id="cpd_propagate" type="button" class="btn btn-secondary script_button"
                        title="Brings the Script fullscreen view up.">Propagate <i class="fa fa-exchange"></i></button>
                <button id="wks-profile" type="button" class="btn btn-secondary script_button">
                    Profile <i class="fa fa-eye"></i></button>
<!--                <button id="prompt-script" type="button" class="btn btn-secondary script_button"-->
<!--                        title="Script Prompter.">Send-->
<!--                    <i class="fa fa-envelope"></i></button>-->
<!--                <button id="score_stretch" type="button" class="btn btn-secondary full-view-hide script_button"-->
<!--                        title="Shows/Hide Scorecard for Script Steps."><i class="fa fa-arrows-h"></i></button>-->
            </div>
        </div>
    </div>
    <div class="col-xs-15 script_score" style="display: none">
        <form id="perf-prof-form">
            <div class="row ynm-row">
                <div class="col-md-5">
                    <p><b>Step No.</b></p>
                </div>
                <div class="col-md-3">
                    <p><span class="has-tooltip" data-toggle="tooltip" title="Performance measure Yes=2, Maybe=1, No=0"
                             data-placement="bottom"><b>PERF YNM</b></span></p>
                </div>
                <div class="col-md-4">
                    <p><span class="has-tooltip" data-toggle="tooltip"
                             title="Proficiency Measure 0=Asleep, 1=Aware, 2=Able, 3=Accountable, 4=Accomplished, 5=Authority"
                             data-placement="bottom"><b>PROF 0-5</b></span></p>
                </div>
            </div>
            <div class="row ynm-row">
                <div class="col-md-5">
                    <p><b>Step1</b></p>
                </div>
                <div class="col-md-3">
                    <select id="perf1" name="perf1" class="ynm-script-select form-control">
                        <option disabled selected value="-1"> ----</option>
                        <option value="2">Y</option>
                        <option value="1">M</option>
                        <option value="0">N</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <select id="prof1" name="prof1" class="ynm-script-select form-control">
                        <option disabled selected value="-1"> ----</option>
                        <option value="5">5</option>
                        <option value="4">4</option>
                        <option value="3">3</option>
                        <option value="2">2</option>
                        <option value="1">1</option>
                        <option value="0">0</option>
                    </select>
                </div>
            </div>
            <div class="row ynm-row">
                <div class="col-md-5">
                    <p><b>Step2</b></p>
                </div>
                <div class="col-md-3">
                    <select id="perf2" name="perf2" class="ynm-script-select form-control">
                        <option disabled selected value="-1"> ----</option>
                        <option value="2">Y</option>
                        <option value="1">M</option>
                        <option value="0">N</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <select id="prof2" name="prof2" class="ynm-script-select form-control">
                        <option disabled selected value="-1"> ----</option>
                        <option value="5">5</option>
                        <option value="4">4</option>
                        <option value="3">3</option>
                        <option value="2">2</option>
                        <option value="1">1</option>
                        <option value="0">0</option>
                    </select>
                </div>
            </div>
            <div class="row ynm-row">
                <div class="col-md-5">
                    <p><b>Step3</b></p>
                </div>
                <div class="col-md-3">
                    <select id="perf3" name="perf3" class="ynm-script-select form-control">
                        <option disabled selected value="-1"> ----</option>
                        <option value="2">Y</option>
                        <option value="1">M</option>
                        <option value="0">N</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <select id="prof3" name="prof3" class="ynm-script-select form-control">
                        <option disabled selected value="-1"> ----</option>
                        <option value="5">5</option>
                        <option value="4">4</option>
                        <option value="3">3</option>
                        <option value="2">2</option>
                        <option value="1">1</option>
                        <option value="0">0</option>
                    </select>
                </div>
            </div>
            <div class="row ynm-row">
                <div class="col-md-5">
                    <p><b>Step3a</b></p>
                </div>
                <div class="col-md-3">
                    <select id="perf3a" name="perf3a" class="ynm-script-select form-control">
                        <option disabled selected value="-1"> ----</option>
                        <option value="2">Y</option>
                        <option value="1">M</option>
                        <option value="0">N</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <select id="prof3a" name="prof3a" class="ynm-script-select form-control">
                        <option disabled selected value="-1"> ----</option>
                        <option value="5">5</option>
                        <option value="4">4</option>
                        <option value="3">3</option>
                        <option value="2">2</option>
                        <option value="1">1</option>
                        <option value="0">0</option>
                    </select>
                </div>
            </div>
            <div class="row ynm-row">
                <div class="col-md-5">
                    <p><b>Step3b</b></p>
                </div>
                <div class="col-md-3">
                    <select id="perf3b" name="perf3b" class="ynm-script-select form-control">
                        <option disabled selected value="-1"> ----</option>
                        <option value="2">Y</option>
                        <option value="1">M</option>
                        <option value="0">N</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <select id="prof3b" name="prof3b" class="ynm-script-select form-control">
                        <option disabled selected value="-1"> ----</option>
                        <option value="5">5</option>
                        <option value="4">4</option>
                        <option value="3">3</option>
                        <option value="2">2</option>
                        <option value="1">1</option>
                        <option value="0">0</option>
                    </select>
                </div>
            </div>
            <div class="row ynm-row">
                <div class="col-md-5">
                    <p><b>Step3c</b></p>
                </div>
                <div class="col-md-3">
                    <select id="perf3c" name="perf3c" class="ynm-script-select form-control">
                        <option disabled selected value="-1"> ----</option>
                        <option value="2">Y</option>
                        <option value="1">M</option>
                        <option value="0">N</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <select id="prof3c" name="prof3c" class="ynm-script-select form-control">
                        <option disabled selected value="-1"> ----</option>
                        <option value="5">5</option>
                        <option value="4">4</option>
                        <option value="3">3</option>
                        <option value="2">2</option>
                        <option value="1">1</option>
                        <option value="0">0</option>
                    </select>
                </div>
            </div>
            <div class="row ynm-row">
                <div class="col-md-5">
                    <p><b>Step3d</b></p>
                </div>
                <div class="col-md-3">
                    <select id="perf3d" name="perf3d" class="ynm-script-select form-control">
                        <option disabled selected value="-1"> ----</option>
                        <option value="2">Y</option>
                        <option value="1">M</option>
                        <option value="0">N</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <select id="prof3d" name="prof3d" class="ynm-script-select form-control">
                        <option disabled selected value="-1"> ----</option>
                        <option value="5">5</option>
                        <option value="4">4</option>
                        <option value="3">3</option>
                        <option value="2">2</option>
                        <option value="1">1</option>
                        <option value="0">0</option>
                    </select>
                </div>
            </div>
            <div class="row ynm-row">
                <div class="col-md-5">
                    <p><b>Step3e</b></p>
                </div>
                <div class="col-md-3">
                    <select id="perf3e" name="perf3e" class="ynm-script-select form-control">
                        <option disabled selected value="-1"> ----</option>
                        <option value="2">Y</option>
                        <option value="1">M</option>
                        <option value="0">N</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <select id="prof3e" name="prof3e" class="ynm-script-select form-control">
                        <option disabled selected value="-1"> ----</option>
                        <option value="5">5</option>
                        <option value="4">4</option>
                        <option value="3">3</option>
                        <option value="2">2</option>
                        <option value="1">1</option>
                        <option value="0">0</option>
                    </select>
                </div>
            </div>
            <div class="row ynm-row">
                <div class="col-md-5">
                    <p><b>Step4</b></p>
                </div>
                <div class="col-md-3">
                    <select id="perf4" name="perf4" class="ynm-script-select form-control">
                        <option disabled selected value="-1"> ----</option>
                        <option value="2">Y</option>
                        <option value="1">M</option>
                        <option value="0">N</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <select id="prof4" name="prof4" class="ynm-script-select form-control">
                        <option disabled selected value="-1"> ----</option>
                        <option value="5">5</option>
                        <option value="4">4</option>
                        <option value="3">3</option>
                        <option value="2">2</option>
                        <option value="1">1</option>
                        <option value="0">0</option>
                    </select>
                </div>
            </div>
            <div class="row ynm-row">
                <div class="col-md-5">
                    <p><b>Step5</b></p>
                </div>
                <div class="col-md-3">
                    <select id="perf5" name="perf5" class="ynm-script-select form-control">
                        <option disabled selected value="-1"> ----</option>
                        <option value="2">Y</option>
                        <option value="1">M</option>
                        <option value="0">N</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <select id="prof5" name="prof5" class="ynm-script-select form-control">
                        <option disabled selected value="-1"> ----</option>
                        <option value="5">5</option>
                        <option value="4">4</option>
                        <option value="3">3</option>
                        <option value="2">2</option>
                        <option value="1">1</option>
                        <option value="0">0</option>
                    </select>
                </div>
            </div>

            <input type="hidden" id="client_id" name="client-id"/>
            <input type="hidden" id="script_id" name="script-id"/>
            <input type="hidden" id="cols_name" name="cols-name"/>

            <div class="row ynm-row" style="margin-top: 1vh">
                <div class="col-md-5">
                    <button class="btn btn-secondary" id="perf-prof-score">
                            <span class="has-tooltip" data-toggle="tooltip" title="Cumulative Scoreboard"
                                  data-placement="bottom">
                            Score <i class="fa fa-list-alt"></i>
                        </span>
                    </button>
                </div>
                <div class="col-md-3">
                    <button class="btn btn-secondary" type="submit" id="perf-prof-submit">
                            <span class="has-tooltip" data-toggle="tooltip" title="Saves Performance/Proficiency values"
                                  data-placement="bottom">
                            <i class="fa fa-check"></i>
                        </span>
                    </button>
                </div>
                <div class="col-md-4">
                    <button class="btn btn-secondary" id="perf-prof-clear">
                            <span class="has-tooltip" data-toggle="tooltip"
                                  title="Clears Performance/Proficiency values" data-placement="bottom">
                            <i class="fa fa-times"></i>
                        </span>
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-xs-15 script_version full-view-hide" align="center">
        <div class="sidebar" id="script-listing" style="word-break: break-all;">
        </div>

        <button id="list-script-archive" class="btn btn-secondary full-view-hide script_button" data-toggle="popover"
                title="Opens Reader View" style="margin-top: 1vh" data-title="<h4><b>SA WORKSHEET EXAMPLE</b></h4>"
                data-html="true" data-placement="top"
                data-content="<div style='font-size: 20px'>
                <div style='margin: 20px'><h3><b>Cover Page Example</b></h2></div>
                <div style='margin: 20px'><label>Client Name</label><p>William Miller</p></div>
                <div style='margin: 20px'><label>Property Name</label><p>Dakota Arms</p></div>
                <div style='margin: 20px'><label>Property Location</label><p>7859 Dakota Way, Tucson, Arizona</p></div>
                <div style='margin: 20px'><label>Designated Advisor</label><p>James Chapman</p></div>
                <div style='margin: 20px'><label>Submission Date</label><p>May 7, 2020</p></div></div>">
            Reader<i class="fa fa-eye"></i>
        </button>

        <button id="list-script-archive" class="btn btn-secondary full-view-hide script_button" data-toggle="modal"
                title="Lists archived Script Version" style="margin-top: 1vh" data-target="#view-script-archive-dialog">
            Alert<i class="fa fa-bell"></i>
        </button>

        <button id="add-script-archive" class="btn btn-secondary full-view-hide script_button"
                style="margin-top: 1vh" title="Adds a Script version to Archive list.">
            Archive<i class="fa fa-trash"></i>
        </button>

        <button id="delete-script" class="btn btn-secondary full-view-hide script_button"
                style="margin-top: 1vh" title="Deletes a Script Version">
            Delete<i class="fa fa-trash"></i>
        </button>

    </div>
</div>

<?php $this->partial('views/tsl/dialogs/delete_script.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_script_outline.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_score.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_script_archive_dialog.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_promo_focus_dialog.php'); ?>
<?php $this->partial('views/collab/dialogs/filter_collab.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_recording.php'); ?>

<?php $this->partial('views/tsl/dialogs/view_criteria.php'); ?>

<script src="<?php echo $this->basepath; ?>resources/trix/dist/trix.js"></script>
<script src="<?php echo $this->basepath; ?>resources/lamejs/lame.all.js"></script>