<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/tsl/message.css"/>
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/trix/dist/trix.css">

<div class="row">
    <div class="col-xs-12">
        <!-- Back Button-->
        <div class="col-xs-4">
            <a class="btn btn-secondary back-button" style="float: right; padding-right: 25px">
                <img src="http://cpd.powersellingtools.com/wp-content/uploads/2020/01/Component-Arrow-Back.png"
                     style="height: 5vh; width: 70%;" alt="back"></a>
        </div>
        <!--// Back Button-->

        <!--// Collaboration Button-->
        <div class="col-xs-4">
            <h2 id="component-heading" style="text-align:center; text-transform:capitalize;" class="heading">
                Notice Builder</h2>
        </div>

        <!-- Collaboration Button-->
        <div class="col-xs-4">
            <a class="btn btn-secondary collab-button" style="float: right; padding-right: 25px">
                <img src="http://cpd.powersellingtools.com/wp-content/uploads/2020/01/Collaboration-Arrows-2.png"
                     style="height: 5vh; width: 70%;" alt="collaboration">
            </a>
        </div>
    </div>
    <div class="col-xs-12" id="component_header">

        <div class="col-xs-15 script-box">
            <label class="col-xs-12" style="text-align: center;">Notice Type</label>
            <select class="form-control" id="notice-type">
                <option value="nf">News Flash</option>
                <option value="ue">Upcoming Event</option>
                <option value="rem">Reminder</option>
                <option value="js">Just Sold</option>
                <option value="jl">Just Listed</option>
            </select>
        </div>

        <div class="col-xs-15 script-box">
            <label class="col-xs-12" style="text-align: center;">Notice Topic</label>
            <select class="form-control" id="notice-topic">
                <option value="mei">Macro Economic Indicators</option>
                <option value="lmd">Local Market Developments</option>
                <option value="cwr">Catastrophic Weather Recovery</option>
                <option value="led">Looming Economic Downturn</option>
                <option value="sdc">Sudden Demographic Changes</option>
            </select>
        </div>

        <div class="col-xs-15 script-box">
            <label class="col-xs-12" style="text-align: center;">Notice Delivery</label>
            <select class="form-control" id="notice-delivery">
                <option value="em">Email</option>
                <option value="sm">Snail Mail</option>
                <option value="eh">Event Handout</option>
                <option value="lb">Leave Behind</option>
                <option value="wd">Web Download</option>
            </select>
        </div>

        <div class="col-xs-15 script-box">
            <label class="col-xs-12" style="text-align: center;">Notice Format</label>
            <select class="form-control" id="notice-format">
                <option value="fp">Full Page</option>
                <option value="hp">Half Page</option>
                <option value="po">Postcard</option>
                <option value="eb">Electronic Banner</option>
                <option value="ea">Embed or Attachment</option>
            </select>
        </div>

        <div class="col-xs-15 script-box">
            <label class="col-xs-12" style="text-align: center;">Notice DateTime</label>
            <input class="datepicker form-control" id="notice-date">
        </div>

    </div>
</div>
<hr>
<div class="row">

    <!-- Version Container   -->
    <div class="col-md-3">
        <div class="sidebar" id="notice-outline" style="word-break: break-all; text-transform:capitalize;">
            <div id="outline-container"></div>
        </div>
    </div>
    <!--// Version Container   -->

    <!-- Component Container -->
    <div class="col-md-7">
        <div class="row">
            <div class="col-md-12 container">
                <div class="sidebar" id="notice-container">
                    <h4 class="main-con-header"><b>Notice Composition</b></h4>
                </div>
            </div>
        </div>
    </div>
    <!--// Component Container -->

    <!-- Instances Container -->
    <div class="col-xs-2">
        <div class="sidebar" style="padding-left: 2px">
            <h4 class="main-con-header"><b>Notice Instances</b></h4>
            <ul class="nav" id="notice-instance"></ul>
        </div>
    </div>
    <!--// Notice Container -->
</div>

<!-- Message Buttons -->
<div class="row" style="margin-top: 1vh">

    <!-- Version Buttons-->
    <div class="col-md-3" id="promo_buttons" align="center">
        <button class="btn btn-secondary" id="create-notice-version">List <i
                    class="fa fa-list"></i></button>
        <button id="delete-notice" class="btn btn-secondary delete">
            Edit <i class="fa fa-pencil"></i></button>
        <button id="save-outline" class="btn btn-secondary delete">
            Save <i class="fa fa-check"></i></button>
    </div>
    <!--// Version Buttons-->

    <!-- Content Buttons-->
    <div class="col-md-7" id="promo_buttons" align="center">
        <button id="compose-notice" class="btn btn-secondary">Compose <i class="fa fa-plus"></i></button>
        <button id="clear-notice" class="btn btn-secondary">Clear <i class="fa fa-eraser"></i></button>
        <button id="view-notice" class="btn btn-secondary">View <i class="fa fa-eye"></i></button>
        <button id="save-notice" class="btn btn-secondary">Save
            <i class="fa fa-check"></i>
        </button>
    </div>
    <!--// Content Buttons-->

    <!-- Notice Buttons -->
    <div class="col-md-2" align="center">
        <button class="btn btn-secondary" id="spoken-notice">Spoken <i class="fa fa-check"></i></button>
        <button id="delete-seq" class="btn btn-secondary delete">Delete
            <i class="fa fa-trash"></i></button>
    </div>
    <!--// Notice Buttons -->

</div>

<script src="<?php echo $this->basepath; ?>resources/trix/dist/trix.js"></script>
<script src="<?php echo $this->basepath; ?>resources/app/js/tsl/notice.js"></script>

<?php //$this->partial('views/tsl/dialogs/delete_seq.php'); ?>
<?php //$this->partial('views/tsl/dialogs/delete_notice.php'); ?>
<?php //$this->partial('views/tsl/dialogs/view_promo_focus_dialog.php'); ?>
<!---->
