<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/company/company.js"></script>

<div class="row">
	<div class="col-xs-12">
		<h3><span id="company-name"></span></h3>
	</div>
</div>

<div class="row">
	<div class="col-xs-12 col-md-8">
		<h4>Domain Name: <span id="company-domain"></span></h4>
	</div>
	<div class="col-xs-12 col-md-4">
	</div>
</div>

