<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Include the supporting javascript. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/company/dialog/delete_company.js"></script>

<!-- Update/insert company dialog -->
<div class="modal fade" id="delete-company-dialog" tabindex="-1" role="dialog" aria-labelledby="delete-company-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="delete-company-form" class="form-horizontal">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h2 id="ddd-message-title" class="modal-title">Delete Company</h2>
				</div>

				<div class="modal-body">

					<p>This will also delete all contacts associated with the company.</p>

					<div class="form-group">
						<label class="col-sm-4 control-label" for="name">Remove Company?</label>
						<div class="col-sm-8">
							<p id="confirm-msg" class="form-control-static"></p>
						</div>
					</div>

				</div>

				<input type="hidden" name="company_id" id="dm-company-id" value=""/>

				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Delete</button>
				</div>

			</form>
		</div>
	</div>
</div>
