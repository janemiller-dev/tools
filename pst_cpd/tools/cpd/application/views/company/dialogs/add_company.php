<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/company/dialog/add_company.js"></script>

<div class="modal fade" id="add-company-dialog" tabindex="-1" role="dialog" aria-labelledby="add-company-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<form id="add-company-form" class="form-horizontal">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h2 id="add-company-title" class="modal-title">Add New Company</h2>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Name</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="company_name" name="name"
							       placeholder="ABC Company, Inc." required="required"/>
							<p class="form-text text-muted">Enter the name of the new company.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="domain">Domain Name</label>
						<div class="col-xs-12 col-sm-9">
							<input type="url" class="form-control" id="company_domain" name="domain"
							       placeholder="http://powersellingtools.com"/>
							<p class="form-text text-muted">The company's domain name, including http://.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Description</label>
						<div class="col-xs-12 col-sm-9">
							<textarea class="form-control" rows="5" width="100%" id="company_description"
							          name="description"></textarea>
							<p class="form-text text-muted">A description of the company.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Contact</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="contact_first" name="contact_first"
							       placeholder="First Name"/>
							<input type="text" class="form-control" id="contact_last" name="contact_last"
							       placeholder="Last Name"/>
							<p class="form-text text-muted">Additional contacts can be added later.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Address</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="address" name="address"
							       placeholder="123 Main St, New York, NY 10001"/>
							<input type="hidden" name="street_1" address-data="name">
							<input type="hidden" name="city" address-data="locality">
							<input type="hidden" name="state" address-data="administrative_area_level_1_short">
							<input type="hidden" name="zipcode" address-data="postal_code">
							<input type="hidden" name="place_id" address-data="place_id">
							<input type="hidden" name="country_code" value="" address-data="country_short">
							<input type="hidden" name="lat" value="" address-data="lat">
							<input type="hidden" name="lng" value="" address-data="lng">
							<p class="form-text text-muted">Company address. The address will be automatically completed
								as you type.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Phone</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="phone" name="phone" placeholder="212-555-1212"/>
							<p class="form-text text-muted">Primary phone number. Additional numbers can be added
								later.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Email</label>
						<div class="col-xs-12 col-sm-9">
							<input type="email" class="form-control" id="email" name="email"
							       placeholder="name@domain.com"/>
							<p class="form-text text-muted">Primary phone number. Additional email addresses can be
								added later.</p>
						</div>
					</div>

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>

			</form>
		</div>
	</div>
</div>
