<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/company/companies.js"></script>

<div class="row">
	<div class="col-xs-12">
		<h3>Companies</h3>
	</div>
</div>

<!-- Show the add button. -->
<div class="row">
	<div class="col-xs-12 text-left">
		<button type="button" id="add-company" class="btn btn-primary bottom-buffer" data-toggle="modal"
		        data-target="#add-company-dialog">Add Company
		</button>
		<a class="btn btn-primary bottom-buffer" href="<?php echo $this->basepath; ?>setting/custom_fields">Custom
			Fields</a>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<!-- Company list row -->
		<div class="row" id="company-list-row">
			<div class="col-xs-12">
				<div id="company-table-div" class="top-buffer">
					<table class="table table-striped" id="company-table">
						<thead>
						<tr>
							<th>ID</th>
							<th>Name</th>
							<th>Domain</th>
							<th>Contacts</th>
							<th>Phones</th>
							<th>Addresses</th>
							<th>Emails</th>
							<th>Description</th>
							<th><span class='glyphicon glyphicon-trash'></span></th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- //Company list row -->

	</div>
</div>

<!-- Dialogs -->
<?php $this->partial('views/company/dialogs/add_company.php'); ?>
<?php $this->partial('views/company/dialogs/delete_company.php'); ?>
