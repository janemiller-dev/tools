<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="/resources/app/js/usermgt/users.js"></script>

<div class="row">
	<div class="col-xs-12">
		<h3>Users</h3>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<!-- User list row -->
		<div class="row" id="user-list-row">
			<div class="col-xs-12">
				<div id="user-table-div" class="top-buffer">
					<table class="table table-striped" id="user-table">
						<thead>
						<tr>
							<th>ID</th>
							<th>Avatar</th>
							<th>Name</th>
							<th>Email</th>
							<th>Admin?</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- /User list row -->

	</div>
</div>
