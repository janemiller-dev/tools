<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="/resources/app/js/usermgt/subscribe.js"></script>

<!-- Page Title -->
<div class="row">
	<div class="col-xs-12">
		<h3>User Management - Subscribe</h2>
	</div>
</div>

<!-- Show the current status of the subscription. -->
<div class="row">
	<div class="col-xs-12">
		<label class="col-xs-12 col-sm-3 control-label">Current Subscription Status</label>
		<div class="col-xs-12 col-sm-9" id="subscription-status"></div>
	</div>
</div>

<!-- Show the eligbility status. -->
<div class="row">
	<div class="col-xs-12">
		<label class="col-xs-12 col-sm-3 control-label">Subscription Eligibility</label>
		<div class="col-xs-12 col-sm-9" id="eligibility-status"></div>
	</div>
</div>

<!-- Subscribe button. -->
<div class="row">
	<div class="col-xs-12 text-center">
		<div class="btn btn-default" id="subscribe-toggle">Subscribe</div>
	</div>
</div>
