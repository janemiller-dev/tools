<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div>
    <table class="col-xs-12">
        <tr class="col-xs-offset-1" style="border: 1px solid rgb(225, 200, 200">
            <td class="col-xs-6">
                <h1 align="center">Getting Started</h1>
                <h3><b>To get started you must first take the following action:</b></h3>
                <h3><b>Complete your User Profile which includes selecting at a Primary Product Type.</b></h3>

                <div style="font-size: 180%"><h2>Step 1:</h2>
                    <p>Go to your Subscriber Profile and fill out the following:</p>
                    <ul>
                        <li> Name</li>
                        <li> Email</li>
                        <li> Industry (Only Commercial Real Estate is available at this time)</li>
                        <li> Profession (Only Investment Sales is available at this time.)</li>
                        <li><b> Primary Product Type – Must be done. </b></li>
                        <li> Secondary Product Type (Only needed is you have one.)</li>
                        <li> Tertiary Product Type (Only needed is you have one.)</li>
                    </ul>
                    You may add as many Product Types as needed.<br/>
                    Then click Update Profile.
                </div>
            </td>

            <td class="col-xs-4">
                <img src="https://cpd.powersellingtools.com/wp-content/uploads/2019/09/step1.png">
            </td>
        </tr>

        <tr class="col-xs-offset-1" style="border: 1px solid rgb(225, 200, 200">
            <td class="col-xs-6" style="font-size: 180%">
                <h2>Step 2:</h2>
                <p>
                    In the white dropdown box on the Green Bar your Primary Product Type will automatically appear. If
                    you have selected
                    additional Product Types they will be accessible in this white dropdown box. Your tools will be
                    organized according to
                    Product Type. This means selecting a product type in this white box will give you access to only
                    tools that belong that
                    that product type. Your Primary Product will always be the default that shows in the box until you
                    change it</p>
            </td>
            <td class="col-xs-">
                <img src="https://cpd.powersellingtools.com/wp-content/uploads/2019/09/step2.png">
            </td>
        </tr>

        <tr class="col-xs-offset-1" style="border: 1px solid rgb(225, 200, 200">
            <td class="col-xs-6" style="font-size: 180%">
                <h2>Step 3:</h2>
                <h3><b> Then click the Sales Game Navigator dropdown on the Green Bar and select one of the following
                        tools:</b></h3>
                <ol>
                    <li>Targeted Suspect List</li>
                    <li>Deal Management Dashboard</li>
                    <li>Tactical Game Designer</li>
                </ol>
                <p> You will then be shown the Dashboard for the tool you select. The Tool Dashboard will show all
                    Instances of the tool grouped into Primary, Second, Third and so on Product Types.</p>
            </td>
            <td class="col-xs-">
                <img src="https://cpd.powersellingtools.com/wp-content/uploads/2019/10/SalesGameNavigator.png">
            </td>
        </tr>

        <tr class="col-xs-offset-1" style="border: 1px solid rgb(225, 200, 200">
            <td class="col-xs-6" style="font-size: 180%">
                <h2>Step 4: Go To HomeBase</h2>
                <p> Then on the Green Bar and select HomeBase into which you may either add Names of Owners and their
                    Assets or
                    click Import to view the Download Form. Then Download the form and import the records you want from
                    your
                    CRM into the Excel Form or enter them by hand into the Form:</p>
            </td>
            <td class="col-xs-">
                <img src="https://cpd.powersellingtools.com/wp-content/uploads/2019/09/step4.png">
            </td>
        </tr>

        <tr class="col-xs-offset-1" style="border: 1px solid rgb(225, 200, 200">
            <td class="col-xs-6" style="font-size: 180%">
                <h2>Step 4a: Building a Targeted Suspect List (TSL).</h2>
                <h3><b>Here are the steps:</b></h3>
                <ol>
                    <li>Select the Year.</li>
                    <li>Select the Product Type.</li>
                    <li>Select the Industry.</li>
                    <li>Select the Profession.</li>
                    <li>Select the Market Area. There can be more than one Market Area.</li>
                    <li>Select the Description. There can be more than one Description.</li>
                    <li>Select the Condition. There can be more than one Condition.</li>
                    <li>Select the Other Criteria. There can be more than one Other Criteria.</li>
                    <li>Client Records will populate the list based on the Criteria selected.</li>
                    <li>Give the List a Name.</li>
                    <li>Give the List a Promotional Focus.</li>
                    <li>The Name will lock in the TSL.</li>
                    <li>The Reload button adds records with the selected Market Area, Description, Condition & Other
                        Criteria that may be added to HomeBase after launch of the TSL.
                    </li>
                    <li>You may add additional Records by selecting different Criteria.</li>
                    <li>Additional Records may also be Submitted directly from HomeBase into the TSL of your choice.
                    </li>
                    <li>You may have as many TSL as you wish because each one will carry its own distinct name.</li>
                    <li>The results of your Call Campaign will be fed into a single Active DMD regardless of the Name,
                        Location,
                        Criteria or Promotional Focus of the TSL
                    </li>
                    <li>Besides the basic TSL Tool functionalities there are two sets of Components which include five
                        Promotional (Promo) Components and five Strategic Advisory Program (SAP) Components.
                    </li>
                </ol>

            </td>
            <td class="col-xs-">
                <img src="https://cpd.powersellingtools.com/wp-content/uploads/2019/11/add_tsl.png">
            </td>
        </tr>

        <tr class="col-xs-offset-1" style="border: 1px solid rgb(225, 200, 200">
            <td class="col-xs-6" style="font-size: 180%">
                <h2>Step 4b: Building a Deal Management Dashboard (DMD).</h2>
                <h3><b>Here are the steps:</b></h3>
                <ol>
                    <li>Select the Year.</li>
                    <li>Select the Industry.</li>
                    <li>Select the Profession.</li>
                    <li>Give the instance a Name.</li>
                    <li>Give the instance a Description.</li>
                    <li>Set the Floor, Target Game (FTG) for the year and quarter.</li>
                    <li>Different that the TSL which can have many Instances, the DMD can only have one Active DMD.
                    </li>
                    <li>DMD refreshes on a Quarterly basis and feeds actual results during each quarter into the
                        Tactical Game Designer Tool.
                    </li>
                    <li>Records from a TSL may have been submitted from a TSL into active DMD's Meetings Scheduled.</li>
                    <li>Before moving Records further through the transactional process continuum you will be directed
                        back to HomeBase.
                    </li>
                    <li>In HomeBase you will review and adjust the status of assets, combine assets into a portfolio.
                    </li>
                    <li>If assets within a Portfolio cut across Product Type the Records included in the Portfolio will
                        be marked
                        with a P1.
                    </li>
                    <li>A second and third Portfolio will be marked P2 & P3.</li>
                    <li>A portfolio will be treated like a single asset on the DMD.</li>
                    <li>Once back on the DMD you will be able to move assets along the transactional continuum by
                        changing
                        the Record Status.
                    </li>
                    <li>The DMD Actual Metrics for Proposals, Listings, Contracts and Completions will be automatically
                        fed into a TGD designated to be connected to an Active DMD.
                    </li>
                </ol>

            </td>
            <td class="col-xs-">
                <img src="https://cpd.powersellingtools.com/wp-content/uploads/2019/11/add_cpd.png">
            </td>
        </tr>

        <tr class="col-xs-offset-1" style="border: 1px solid rgb(225, 200, 200">
            <td class="col-xs-6" style="font-size: 180%">
                <h2>Step 4c: Building a Tactical Game Designer (TGD).</h2>
                <h3><b>Here are the steps:</b></h3>
                <ol>
                    <li>Select the Year.</li>
                    <li>Select the Industry.</li>
                    <li>Select the Profession.</li>
                    <li>Give the instance a Name.</li>
                    <li>Give the instance a Mission.</li>
                    <li>Give the instance a Condition.</li>
                    <li>Set the Floor, Target Game (FTG) for the year and quarter.</li>
                    <li>TGD is an annual Tool only. You can view the current year and the previous or coming year at
                        anytime but you can only make adjustments to the current year TGD.
                    </li>
                    <li>DMD refreshes on a Quarterly basis and feeds actual results during each quarter into the
                        Tactical Game Designer Tool.
                    </li>
                    <li>Once the annual FTG has been set the following values can be entered.
                        <ol type="a">
                            <li>Set the Quarterly FTG % Milestones.</li>
                            <li>Set the Average Size Check which automatically propagate forward.</li>
                            <li>Set the Double End Ratio which automatically propagates forward.</li>
                            <li>Set the Conversion Ratios, Annual FTG and Deal Count FTG for the following Conditions:
                                <ol type="i">
                                    <li>Deals In Contract</li>
                                    <li>Additional Listings</li>
                                    <li>Proposals in Play</li>
                                </ol>
                            </li>
                            <li>This will project a quarter by quarter calculation according to Conversion Ratios on a
                                diagonal.
                            </li>
                            <li>Next compare the projected deal count and income to the Quarterly Milestones.</li>
                            <li>If the projection is much higher than the Quarterly Milestones and the Annual FTG then
                                raise
                                your Annual FTG and adjust your Quarterly Milestones.
                            </li>
                            <li>If the projection is much less than the Quarterly Milestones and the Annual FTG then
                                lower your
                                Annual FTG and adjust your Quarterly Milestones.
                            </li>
                            <li>Otherwise you may use the Toggle Controls to move individual Targeted Deals up, Down,
                                Forward
                                and Back one level or one quarter at a time.
                            </li>
                            <li>There is an Undo Toggle control and a Toggle History to refer to if you get lost or
                                Clear altogether.
                            </li>
                            <li>The key is to reach your Quarterly Milestones and Annual FTG as well as set-up the
                                coming year
                                which you can only view by clicking on Next Year.
                            </li>
                            <li>Once you have used the Sales Game Navigator for a second year you can then view the
                                previous year.
                            </li>
                        </ol>
                    </li>
                </ol>

            </td>
            <td class="col-xs-">
                <img src="https://cpd.powersellingtools.com/wp-content/uploads/2019/11/add_tgd.png">
            </td>
        </tr>

    </table>
</div>

