<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="/resources/js/account.js"></script>

<div id="existing-accounts" class="row">
	<div class="col-xs-offset-1 col-xs-10">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title">Existing Accounts</h3>
			</div>
			<div class="panel-body">
				<p>Select one of your existing accounts.</p>

				<div class="table-responsive">
					<table id="account-table" class="table table-striped">
						<thead>
						<tr>
							<th class="text-center">Account Type</th>
							<th class="text-center">Account Name</th>
							<th class="text-center">Activated Packages</th>
							<th class="text-center">Renewal Status</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>

			</div>
		</div>
	</div>
</div>

<div id="no-existing-accounts" class="row">
	<div class="col-xs-offset-1 col-xs-10">
		<p>You do not have any existing accounts. Select an account type from below to create an account.</p>
	</div>
</div>

<div id="new-account" class="row">

	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title">Trial Account</h3>
			</div>
			<div class="panel-body">
				30-day Trial Account
			</div>
		</div>
	</div>

	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title">Basic Account</h3>
			</div>
			<div class="panel-body">
				The Advance account costs $10/month.
			</div>
		</div>
	</div>

	<div class="col-xs-12 col-sm-6 col-md-4">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title">Advanced Account</h3>
			</div>
			<div class="panel-body">
				The Advance account costs $25/month.
			</div>
		</div>
	</div>

</div>

