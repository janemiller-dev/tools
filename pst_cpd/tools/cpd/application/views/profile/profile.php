<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="row">
    <div class="col-xs-12">
        <h2 id="tsl-profile-heading" style="text-align:center">Buyer Profile</h2>
        <div class="container">
            <p style="font-size: 15px"><b>In order to maximize our ability to keep our eyes open for opportunities that
                    fit your needs we need the following information from you. The more information you
                    give us, the more we will be able to help you find the quality of opportunities you are seeking.
                    Your information is confidential and will only be used to assist in our
                    efforts to serve you.</b></p>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-md-12">
        <form id="profile-form" class="form-horizontal" style="font-size: 20px">

        </form>
        <div style="text-align: center; margin: 10px">
            <button class="btn btn-primary" id="save_profile">Submit Response</button>
        </div>
    </div>
</div>
</div>

<script src="<?php echo $this->basepath; ?>resources/app/js/profile/profile.js"></script>