<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="/resources/app/js/tool/group.js"></script>

<!-- Page Title -->
<div class="row">
	<div class="col-xs-12">
		<div class="text-left">
			<h3>Tool Group Administration</h3>
			<p>Update the tool group's name and description here. To add a new tool to the tool group, use the add
				button in the tool list. To remove a tool from the tool group, use the unlink button in the tool
				list.</p>
		</div>
	</div>
</div>

<!-- Show the current status of the subscription. -->
<div class="row">
	<div class="col-xs-12">
		<form id="heading-form" class="form-horizontal">
			<div class="form-group">
				<label class="col-xs-12 col-sm-4 control-label">
					<h3>Group Name</h3>
				</label>
				<div class="col-xs-12 col-sm-8"><h3 id="group-name" class="form-control-static"></h3><span
							class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover"
							title="Tool Group Name" data-content="The name of the tool group."></span></div>
			</div>

			<div class="form-group">
				<label class="col-xs-12 col-sm-4 control-label">
					<h4>Description</h4>
				</label>
				<div class="col-xs-12 col-sm-8"><h4 id="group-description" class="form-control-static"></h4><span
							class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover"
							title="Tool Group Description" data-content="The description of the tool group."></span>
				</div>
			</div>

			<div class="form-group">
				<label class="col-xs-12 col-sm-4 control-label">
					<h4>Admin Only?</h4>
				</label>
				<div class="col-xs-12 col-sm-8"><h4 id="group-is-admin" class="form-control-static"></h4><span
							class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover"
							title="Administrator Only?"
							data-content="Is the group only available to administrators?."></span></div>
			</div>

		</form>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<!-- Tool list row -->
		<div class="row" id="tool-list-row">
			<div class="col-xs-12">
				<div id="tool-table-div" class="top-buffer">
					<table class="table table-striped" id="tool-table">
						<thead>
						<tr>
							<th>ID</th>
							<th>Tool</th>
							<th>Description</th>
							<th>Admin?</th>
							<th>Status</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- //Tool list row -->

	</div>
</div>
