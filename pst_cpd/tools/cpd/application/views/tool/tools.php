<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/tool/tools.js"></script>

<div class="row">
	<div class="col-xs-12">
		<h3>Tools</h3>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<!-- Tool list row -->
		<div class="row" id="tool-list-row">
			<div class="col-xs-12">
				<div id="tool-table-div" class="top-buffer">
					<table class="table table-striped" id="tool-table">
						<thead>
						<tr>
							<th>ID</th>
							<th>Group ID</th>
							<th>Group</th>
							<th>Tool</th>
							<th>Description</th>
							<th>Admin?</th>
							<th>Status</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- //Tool list row -->

	</div>
</div>
