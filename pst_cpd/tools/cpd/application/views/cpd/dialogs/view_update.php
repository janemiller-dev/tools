<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/cpd/dialog/view_update.js"></script>

<div class="modal fade" id="view-update-dialog" tabindex="-1" role="dialog" aria-labelledby="view-update-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>

				<h2 id="view-update-title" class="modal-title">Updates</h2>
				<button type="button" id="add-update-button" class="btn btn-primary bottom-buffer" data-toggle="modal"
				        data-backdrop="static">Add New Update
				</button>

			</div>

			<div class="modal-body">

				<!-- Show the list of updates. -->
				<div class="row">
					<div class="col-xs-12">

						<!-- Deal update table -->
						<div class="row" id="deal-update-row">
							<div class="col-xs-12">
								<p id="no-deal-update">You have not created any update for the deal. To create one, use
									the Add update button.</p>
								<div id="deal-update-div" class="top-buffer">
									<table class="table table-striped" id="deal-update-table">
										<thead>
										<tr>
											<th>Name</th>
											<th>Time</th>
											<th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
										</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<!-- Deal update table -->
					</div>
				</div>
			</div>

			<input type="hidden" id="view-update-deal-id" name="deal_id"/>

			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
