<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/cpd/dialog/add_deal.js"></script>

<div class="modal fade" id="add-deal-dialog" tabindex="-1" role="dialog" aria-labelledby="add-deal-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<form id="add-deal-form" class="form-horizontal">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h2 id="add-deal-title" class="modal-title">Add Deal</h2>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="deal-name">Client/Property</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="deal-name" name="deal_name"/>
							<p class="form-text text-muted">Enter the name of the new Deal/Client/Property.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="deal-move-date">Move Date</label>
						<div class="col-xs-12 col-sm-9">
							<div class="input-group">
								<select class="form-control" id="deal-move-date" name="deal_move_date">
								</select>
							</div>
							<p class="form-text text-muted">Select the deal move date</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="deal-status">Status</label>
						<div class="col-xs-12 col-sm-9">
							<div class="input-group">
								<select class="form-control" id="deal-status" name="deal_status">
								</select>
							</div>
							<p class="form-text text-muted">Select the deal Status</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="deal-clu">CLU</label>
						<div class="col-xs-12 col-sm-9">
							<div class="input-group">
								<select class="form-control" id="deal-clu" name="deal_clu">
								</select>
							</div>
							<p class="form-text text-muted">Select the deal CLU</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="deal-rwt">RWT</label>
						<div class="col-xs-12 col-sm-9">
							<div class="input-group">
								<select class="form-control" id="deal-rwt" name="deal_rwt">
								</select>
							</div>
							<p class="form-text text-muted">Select the deal RWT</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="deal-lead">LEAD</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="deal-lead" name="deal_lead" maxlength="5"/>
							<p class="form-text text-muted">Enter the lead agent. This must be 5 characters or less.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="deal-gen">GEN</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="deal-gen" name="deal_gen" maxlength="5"/>
							<p class="form-text text-muted">Enter the agent who generated the deal. This must be 5 characters or less.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="deal-type">Product Type</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="deal-type" name="deal_type"/>
							<p class="form-text text-muted">Enter the type of the deal.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="deal-amount">Amount</label>
						<div class="col-xs-12 col-sm-9">
							<input type="number" min="0" class="form-control" id="deal-amount" name="deal_amount"/>
							<p class="form-text text-muted">Enter the value of the deal.</p>
						</div>
					</div>

					<input type="hidden" id="add-deal-cpd-id" name="add_deal_cpd_id"/>
					<input type="hidden" id="add-deal-status-type" name="add_deal_status_type"/>

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
