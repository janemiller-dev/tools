<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/cpd/dialog/add_cpd.js"></script>

<div class="modal fade" id="add-cpd-dialog" tabindex="-1" role="dialog" aria-labelledby="add-cpd-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<form id="add-cpd-form" class="form-horizontal">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h2 id="add-cpd-title" class="modal-title">Add Deal Management Dashboard Instance</h2>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="cpd-year">Select Year</label>
						<div class="col-xs-12 col-sm-9">
							<div class="input-group">
								<select class="form-control" id="cpd-year" name="cpd_year">
								</select>
							</div>
							<p class="form-text text-muted">Select your DMD Year</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="cpd-industry-id">Select Industry</label>
						<div class="col-xs-12 col-sm-9">
							<div class="input-group">
								<select class="form-control" id="cpd-industry-id" name="cpd_industry_id">
								</select>
							</div>
							<p class="form-text text-muted">Select your industry.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="cpd-profession-id">Select Profession</label>
						<div class="col-xs-12 col-sm-9">
							<div class="input-group">
								<select class="form-control" id="cpd-profession-id" name="cpd_profession_id" disabled>
								</select>
							</div>
							<p class="form-text text-muted">Select your industry. For more information on the available industries, see <span id="industry-list-link"></span></p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="cpd-tgd-id">Select your TGD</label>
						<div class="col-xs-12 col-sm-9">
							<div class="input-group">
								<select class="form-control" id="cpd-tgd-id" name="cpd_tgd_id" disabled>
								</select>
							</div>
							<p class="form-text text-muted">Select your TGD.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="cpd_ui_name">DMD Name</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="cpd_ui_name" name="name"/>
							<p class="form-text text-muted">Enter the name of the new Deal Management Dashboard
								Instance.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="cpd_ui_description">DMD Description</label>
						<div class="col-xs-12 col-sm-9">
							<textarea class="form-control" rows="5" width="100%" name="description"
							          id="cpd_ui_description"></textarea>
						</div>
					</div>

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>

			</form>
		</div>
	</div>
</div>
