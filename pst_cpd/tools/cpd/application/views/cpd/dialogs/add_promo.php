<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/cpd/dialog/add_promo.js"></script>

<div class="modal fade" id="add-promo-dialog" tabindex="-1" role="dialog" aria-labelledby="add-promo-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<form id="add-promo-form" class="form-horizontal">

				<div class="modal-header">
					<h2 id="add-promo-title" class="modal-title">Add promo</h2>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="promo-name">Client/Property</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="promo-name" name="promo_name"/>
							<p class="form-text text-muted">Enter the name of the new promo/Client/Property.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="promo-date">Date</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="promo-date" name="promo_date"/>
							<p class="form-text text-muted">Enter the Date for Promotion</p>
						</div>
					</div>

					<input type="hidden" id="add-promo-deal-id" name="add_promo_deal_id"/>
				</div>

				<div class="modal-footer">
<!--					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>-->
					<button type="button" class="btn btn-primary" id="close-add-promo-dialog">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
