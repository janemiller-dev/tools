<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/cpd/dialog/add_update.js"></script>

<div class="modal fade" id="add-update-dialog" tabindex="-1" role="dialog" aria-labelledby="add-update-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<form id="add-update-form" class="form-horizontal">

				<div class="modal-header">
					<h2 id="add-update-title" class="modal-title">Add update</h2>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="update-name">Client/Property</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="update-name" name="update_name"/>
							<p class="form-text text-muted">Enter the name of the new Update for your deal.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="update-date">Date</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="update-date" name="update_date"/>
							<p class="form-text text-muted">Enter the Date for Update</p>
						</div>
					</div>

					<input type="hidden" id="add-update-deal-id" name="add_update_deal_id"/>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="close-add-update-dialog">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>
