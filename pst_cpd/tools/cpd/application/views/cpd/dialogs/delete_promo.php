<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/cpd/dialog/delete_promo.js"></script>

<div class="modal fade" id="delete-promo-dialog" tabindex="-1" role="dialog" aria-labelledby="delete-promo-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

				<div class="modal-body">
					<h2 id="delete-promo-title" class="modal-title">Delete Promo?</h2>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Name</label>
						<div class="col-xs-12 col-sm-9">
							<p class="form-control-static" id="promo-name"></p>
						</div>
					</div>

				</div>

				<input type="hidden" id="promo-id" name="promo_id"/>

				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="close-delete-promo-dialog">Close</button>
					<button id="delete-promo-button" type="button" class="btn btn-primary" >Delete</button>
				</div>
		</div>
	</div>
</div>
