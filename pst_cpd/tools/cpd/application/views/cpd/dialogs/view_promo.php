<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/cpd/dialog/view_promo.js"></script>


<div class="modal fade" id="view-promo-dialog" tabindex="-1" role="dialog" aria-labelledby="view-promo-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>

					<h2 id="view-promo-title" class="modal-title">Promotions</h2>
					<button type="button" id="add-promo-button" class="btn btn-primary bottom-buffer" data-toggle="modal" data-backdrop="static">Add New Promo
					</button>

				</div>

				<div class="modal-body">

					<!-- Show the list of promos. -->
					<div class="row">
						<div class="col-xs-12">

							<!-- Deal Promo table -->
							<div class="row" id="deal-promo-row">
								<div class="col-xs-12">
									<p id="no-deal-promo">You have not created any Promo for the deal. To create one, use
										the Add Promo button.</p>
									<div id="deal-promo-div" class="top-buffer">
										<table class="table table-striped" id="deal-promo-table">
											<thead>
											<tr>
												<th>Name</th>
												<th>Time</th>
												<th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
											</tr>
											</thead>
											<tbody>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<!-- Deal Promo table -->
						</div>
					</div>
				</div>

				<input type="hidden" id="view-promo-deal-id" name="deal_id"/>

				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				</div>

<!--			</form>-->
		</div>

	</div>
</div>