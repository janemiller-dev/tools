<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/cpd/dialog/delete_cpd.js"></script>

<div class="modal fade" id="delete-cpd-dialog" tabindex="-1" role="dialog" aria-labelledby="delete-cpd-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<form id="delete-cpd-form" class="form-horizontal">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h2 id="delete-cpd-title" class="modal-title">Delete Deal Management Dashboard Instance?</h2>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Name</label>
						<div class="col-xs-12 col-sm-9">
							<p class="form-control-static" id="cpd-name"></p>
						</div>
					</div>

				</div>

				<input type="hidden" id="cpd-id" name="cpd_id"/>

				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Delete</button>
				</div>

			</form>
		</div>
	</div>
</div>
