<?php if (!defined('SUBVIEW')) exit('No direct script access allowed');

$token = base64_encode(hash('sha256', time()));
$_SESSION['token'] = $token;
?>

    <!-- Load the javascript support. -->
    <script src="<?php echo $this->basepath; ?>resources/app/js/cpd/cpd.js"></script>
    <!-- Load the custom css. -->
    <link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/minified_css/cpd/cpd.css?v=1" />
    <input type="hidden" name="csrf-token" value="<?php echo $token;?>">
    <!-- Page Title -->
    <div class="row">
        <div class="col-xs-12 text-center">
            <h1>Deal Management Dashboard</h1>
            <div class="row">
                <div class="col-xs-12 col-sm-4">
                    <span style="font-size: 1.2em;"><a href="" id="previous-quarter-cpd">Previous Quarter</a></span>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <span style="font-size: 1.2em;">Active Tactical Game Designer:&nbsp;
					<a href="" id="linked-tgd"></a>
				</span>
                    <br/>
                    <span style="font-size: 1.2em;" class="quarter-info">Quarter:&nbsp;Q<span id="quarter-number"></span> <span id="quarter-year"></span></span>
                </div>
                <div class="col-xs-12 col-sm-4">
                    <span style="font-size: 1.2em;"><a href="" id="next-quarter-cpd">Next Quarter</a></span>
                </div>
            </div>
        </div>
    </div>

    <!-- Header section -->
    <div class="row" style="border-bottom: 1px solid #e1c8c8; border-top: 1px solid #e1c8c8; padding-bottom: 5px; padding-top: 5px;">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="row" style="border-right: 1px solid #e1c8c8;">
                <h4 class="col-xs-12 text-center">Quarter</h4>
                <div class="col-xs-4">
                    <h5 class="col-xs-12 text-center">Floor</h5>
                    <input class="col-xs-12" id="qtr-floor" placeholder="Floor" style="border: 1px solid">
                </div>
                <div class="col-xs-4">
                    <h5 class="col-xs-12 text-center">Target</h5>
                    <input class="col-xs-12 bottom-buffer" id="qtr-target" placeholder="Target" style="border: 1px solid">
                    <input class="col-xs-12 bottom-buffer" id="qtr-actual" placeholder="Actual" style="border: 1px solid">
                    <input class="col-xs-12" id="qtr-percent" placeholder="Percent" style="border: 1px solid">
                </div>
                <div class="col-xs-4">
                    <h5 class="col-xs-12 text-center">Game</h5>
                    <input class="col-xs-12" id="qtr-game" placeholder="Game" style="border: 1px solid">
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="row">
                <h4 class="col-xs-12 text-center">Year</h4>
                <div class="col-xs-4">
                    <h5 class="col-xs-12 text-center">Floor</h5>
                    <input class="col-xs-12" id="year-floor" placeholder="Floor" style="border: 1px solid">
                </div>
                <div class="col-xs-4">
                    <h5 class="col-xs-12 text-center">Target</h5>
                    <input class="col-xs-12 bottom-buffer border" id="year-target" placeholder="Target" style="border: 1px solid">
                    <input class="col-xs-12 bottom-buffer" id="year-actual" placeholder="Actual" style="border: 1px solid">
                    <input class="col-xs-12" id="year-percent" placeholder="Percent" style="border: 1px solid">
                </div>
                <div class="col-xs-4">
                    <h5 class="col-xs-12 text-center">Game</h5>
                    <input class="col-xs-12" id="year-game" placeholder="Game" style="border: 1px solid">
                </div>
            </div>
        </div>
    </div>

    <!-- Show the add deal button. -->
    <div class="row top-buffer">
        <div class="col-xs-12 text-left">
            <button type="button" id="add-deal" class="btn btn-primary bottom-buffer" data-toggle="modal" data-target="#add-deal-dialog" data-backdrop="static">Add New Deal
            </button>
        </div>
    </div>

    <!-- Show the DMD Instance -->
    <div class="row">
        <div class="col-xs-12 container-scroll">

            <ul class="nav nav-tabs top-buffer" role="tablist">
                <li role="presentation" class="active">
                    <a href="#completion" role="tab" data-toggle="tab">Deal Completion</a>
                </li>
                <li role="presentation">
                    <a href="#creation" role="tab" data-toggle="tab">Deal Creation</a>
                </li>
                <li role="presentation">
                    <a href="#strategic" role="tab" data-toggle="tab">Strategic Deals</a>
                </li>
            </ul>

            <div class="tab-content">

                <!-- Completion Tab -->
                <div role="tabpanel" class="tab-pane col-xs-12 active" id="completion">
                    <div class="row">

                        <!-- Actual and Repeat column -->
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">

                            <!-- Actual table -->
                            <!--						<table class="table table-striped table-condensed deal-table" id="cpd-actual-table" data-deal-type="complete">-->
                            <div class="row">
                                <table class="table table-striped table-condensed deal-table" id="cpd-deal-complete-table">
                                    <thead style="background-color: #ededed">
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.2em;">Actual:&nbsp;
										<span class="editable" id="complete-name"></span>
                                                </span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.1em;">Description:&nbsp;
										<span class="editable" id="complete-description"></span>
                                                </span>
                                        </th>
                                    <tr>
                                        <th class="text-center"><span class="glyphicon glyphicon-transfer"></span></th>
                                        <th>CD</th>
                                        <th>Client</th>
                                        <th style="text-align: center;">CLU</th>
                                        <th>RWT</th>
                                        <th>Lead</th>
                                        <th>Gen</th>
                                        <th>Product</th>
                                        <th>Amount</th>
                                        <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                                <h5 style="float: right">
                                    <strong>
                                        Total Actual&nbsp;<span><input id="cpd-deal-complete-total" class="deal-count" readonly style="border: 1px solid"></span>
                                    </strong>
                                </h5>
                                <!-- // Actual table -->
                            </div>
                            <div class="row">
                                <!-- Repeat table -->
                                <table class="table table-striped table-condensed deal-table" id="cpd-deal-repeat-table">
                                    <thead style="background-color: #ededed">
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.2em;">Actual:&nbsp;
											<span class="editable" id="repeat-name"></span>
                                                </span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.1em;">Description:
											<span class="editable" id="repeat-description"></span>
                                                </span>
                                        </th>
                                    <tr>
                                        <th class="text-center"><span class="glyphicon glyphicon-transfer"></span></th>
                                        <th>CD</th>
                                        <th>Client</th>
                                        <th style="text-align: center;">CLU</th>
                                        <th>RWT</th>
                                        <th>Lead</th>
                                        <th>Gen</th>
                                        <th>Product</th>
                                        <th>Amount</th>
                                        <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <h5 style="float: right">
                                <strong>
                                    Total Repeat&nbsp;<span><input id="cpd-deal-repeat-total" class="deal-count" readonly style="border: 1px solid"></span>
                                </strong>
                            </h5>
                            <!-- // Repeat table -->

                        </div>
                        <!-- // Actual and Repeat column -->

                        <!-- Contract and Hard column  -->
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <!-- Contract table -->
                                <table class="table table-striped table-condensed deal-table" id="cpd-deal-contract-table" data-deal-type="contract">
                                    <thead style="background-color: #ededed">
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.2em;">Predictable:&nbsp;
											<span class="editable" id="contract-name"></span>
                                                </span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.1em;">Description:
											<span class="editable" id="contract-description"></span>
                                                </span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center"><span class="glyphicon glyphicon-transfer"></span></th>
                                        <th>MD</th>
                                        <th>Client</th>
                                        <th style="text-align: center;">CLU</th>
                                        <th>RWT</th>
                                        <th>Lead</th>
                                        <th>Gen</th>
                                        <th>Product</th>
                                        <th>Amount</th>
                                        <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <h5 style="float: right">
                                <strong>
                                    Total Predictable&nbsp;<span><input id="cpd-deal-contract-total" class="deal-count" readonly style="border: 1px solid"></span>
                                </strong>
                            </h5>
                            <!-- //Contract table -->
                            <div class="row">
                                <!-- Hard table-->
                                <table class="table table-striped table-condensed deal-table" id="cpd-deal-deposit-table">
                                    <thead style="background-color: #ededed">
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.2em;">Predictable:&nbsp;
											<span class="editable" id="deposit-name"></span>
                                                </span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.1em;">Description:&nbsp;
											<span class="editable" id="deposit-description"></span>
                                                </span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center"><span class="glyphicon glyphicon-transfer"></span></th>
                                        <th>MD</th>
                                        <th>Client</th>
                                        <th style="text-align: center;">CLU</th>
                                        <th>RWT</th>
                                        <th>Lead</th>
                                        <th>Gen</th>
                                        <th>Product</th>
                                        <th>Amount</th>
                                        <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <h5 style="float: right">
                                <strong>
                                    Total Hard&nbsp;<span><input id="cpd-deal-deposit-total" class="deal-count" readonly style="border: 1px solid"></span>
                                </strong>
                            </h5>
                            <!-- // Hard table -->

                        </div>
                        <!-- Contract and Hard column -->

                        <!-- Listings and Offers column -->
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <!-- Listings table -->
                                <table class="table table-striped table-condensed deal-table" id="cpd-deal-listed-table">
                                    <thead style="background-color: #ededed">
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.2em;">Probable:&nbsp;
										<span class="editable" id="listed-name"></span>
                                                </span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.1em;">Description:&nbsp;
										<span class="editable" id="listed-description"></span>
                                                </span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center"><span class="glyphicon glyphicon-transfer"></span></th>
                                        <th>MD</th>
                                        <th>Client</th>
                                        <th style="text-align: center;">CLU</th>
                                        <th>RWT</th>
                                        <th>Lead</th>
                                        <th>Gen</th>
                                        <th>Product</th>
                                        <th>Amount</th>
                                        <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </div>
                            <h5 style="float: right">
                                <strong>
                                    Total Probable&nbsp;<span><input id="cpd-deal-listed-total" class="deal-count" style="border: 1px solid"></span>
                                </strong>
                            </h5>
                            <!-- // Listings table -->
                            <div class="row">
                                <!-- Offers table -->
                                <table class="table table-striped table-condensed deal-table" id="cpd-deal-offer-table">
                                    <thead style="background-color: #ededed">
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.2em;">Probable:&nbsp;
										<span class="editable" id="offer-name"></span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.1em;">Description:&nbsp;
										<span class="editable" id="offer-description"></span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center"><span class="glyphicon glyphicon-transfer"></span></th>
                                        <th>MD</th>
                                        <th>Client</th>
                                        <th style="text-align: center;">CLU</th>
                                        <th>RWT</th>
                                        <th>Lead</th>
                                        <th>Gen</th>
                                        <th>Product</th>
                                        <th>Amount</th>
                                        <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <h5 style="float: right">
                                <strong>
                                    Total Offer&nbsp;<span><input id="cpd-deal-offer-total" class="deal-count" readonly style="border: 1px solid"></span>
                                </strong>
                            </h5>
                            <!-- // Offers table -->

                        </div>
                        <!-- // Listing and Offers column -->

                    </div>
                </div>
                <!-- // Deal Completion tab -->

                <!-- Deal Creation tab -->
                <div role="tabpanel" class="tab-pane col-xs-12" id="creation">
                    <div class="row">

                        <!-- Prospective column -->
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <!-- Prospective requested table -->
                                <table class="table table-striped table-condensed deal-table" id="cpd-deal-requested-table">
                                    <thead style="background-color: #ededed">
                                    <tr>
                                        <th colspan="10">
                                            <span style="font-size: 1.2em;">Prospective:&nbsp;<span class="editable" id="requested-name"></span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="10">
                                            <span style="font-size: 1.1em;">Description:&nbsp;<span class="editable" id="requested-description"></span></span </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center"><span class="glyphicon glyphicon-transfer"></span></th>
                                        <th>MD</th>
                                        <th>Client</th>
                                        <th style="text-align: center;">CLU</th>
                                        <th>RWT</th>
                                        <th>Lead</th>
                                        <th>Gen</th>
                                        <th>Product</th>
                                        <th>Amount</th>
                                        <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <h5 style="float: right">
                                <strong>
                                    Total Prospective&nbsp;<span><input id="cpd-deal-requested-total" class="deal-count" readonly style="border: 1px solid"></span>
                                </strong>
                            </h5>
                            <!-- // Prospective requested table -->
                            <div class="row">
                                <!-- Prospective presented table -->
                                <table class="table table-striped table-condensed deal-table" id="cpd-deal-presented-table">
                                    <thead style="background-color: #ededed">
                                    <tr>
                                        <th colspan="10">
                                            <span style="font-size: 1.2em;">Prospective:&nbsp;<span class="editable" id="presented-name"></span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="10">
                                            <span style="font-size: 1.1em;">Description:&nbsp;<span class="editable" id="presented-description"></span></span </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center"><span class="glyphicon glyphicon-transfer"></span></th>
                                        <th>MD</th>
                                        <th>Client</th>
                                        <th style="text-align: center;">CLU</th>
                                        <th>RWT</th>
                                        <th>Lead</th>
                                        <th>Gen</th>
                                        <th>Product</th>
                                        <th>Amount</th>
                                        <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <h5 style="float: right">
                                <strong>
                                    Total Presented&nbsp;<span><input id="cpd-deal-presented-total" class="deal-count" readonly style="border: 1px solid"></span>
                                </strong>
                            </h5>
                            <!-- // Prospective presented table -->

                        </div>
                        <!-- Prospective column -->

                        <!-- Meeting and Tours column -->
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <!-- Meetings table -->
                                <table class="table table-striped table-condensed deal-table" id="cpd-deal-meeting-table">
                                    <thead style="background-color: #ededed">
                                    <tr>
                                        <th colspan="10">
                                            <span style="font-size: 1.2em;">Possible:&nbsp;<span class="editable" id="meeting-name"></span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="10">
                                            <span style="font-size: 1.1em;">Description:&nbsp;<span class="editable" id="meeting-description"></span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center"><span class="glyphicon glyphicon-transfer"></span></th>
                                        <th>MD</th>
                                        <th>Client</th>
                                        <th style="text-align: center;">CLU</th>
                                        <th>RWT</th>
                                        <th>Lead</th>
                                        <th>Gen</th>
                                        <th>Product</th>
                                        <th>Amount</th>
                                        <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <h5 style="float: right">
                                <strong>
                                    Total Possible&nbsp;<span><input id="cpd-deal-meeting-total" class="deal-count" readonly style="border: 1px solid"></span>
                                </strong>
                            </h5>
                            <!-- // Meetings table -->
                            <div class="row">
                                <!-- Tours table -->
                                <table class="table table-striped table-condensed deal-table" id="cpd-deal-tour-table">
                                    <thead style="background-color: #ededed">
                                    <tr>
                                        <th colspan="10">
                                            <span style="font-size: 1.2em;">Possible:&nbsp;<span class="editable" id="tour-name"></span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.1em;">Description&nbsp;<span class="editable"
                                                                                                       id="tour-description"></span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center"><span class="glyphicon glyphicon-transfer"></span></th>
                                        <th>MD</th>
                                        <th>Client</th>
                                        <th style="text-align: center;">CLU</th>
                                        <th>RWT</th>
                                        <th>Lead</th>
                                        <th>Gen</th>
                                        <th>Product</th>
                                        <th>Amount</th>
                                        <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <h5 style="float: right">
                                <strong>
                                    Total Tour&nbsp;<span><input id="cpd-deal-tour-total" class="deal-count" readonly style="border: 1px solid"></span>
                                </strong>
                            </h5>
                            <!-- // Tours table -->

                        </div>
                        <!-- // Meetings and Tours column -->

                        <!-- Invited column-->
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <!-- Invited maybe table -->
                                <table class="table table-striped table-condensed deal-table" id="cpd-deal-maybe-table">
                                    <thead style="background-color: #ededed">
                                    <tr>
                                        <th colspan="10">
                                            <span style="font-size: 1.2em;">Potential:&nbsp<span class="editable" id="maybe-name"></span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="10">
                                            <span style="font-size: 1.1em;">Description:&nbsp;<span class="editable" id="maybe-description"></span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center"><span class="glyphicon glyphicon-transfer"></span></th>
                                        <th>MD</th>
                                        <th>Client</th>
                                        <th style="text-align: center;">CLU</th>
                                        <th>RWT</th>
                                        <th>Lead</th>
                                        <th>Gen</th>
                                        <th>Product</th>
                                        <th>Amount</th>
                                        <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <h5 style="float: right">
                                <strong>
                                    Total Potential&nbsp;<span><input id="cpd-deal-maybe-total" class="deal-count" readonly style="border: 1px solid"></span>
                                </strong>
                            </h5>
                            <!-- // Invited maybe table -->
                            <div class="row">
                                <!-- Invited no table -->
                                <table class="table table-striped table-condensed deal-table" id="cpd-deal-no-table">
                                    <thead style="background-color: #ededed">
                                    <tr>
                                        <th colspan="10">
                                            <span style="font-size: 1.2em;">Potential:&nbsp;<span class="editable" id="no-name"></span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="10">
                                            <span style="font-size: 1.1em;">Description:&nbsp;<span class="editable" id="no-description"></span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center"><span class="glyphicon glyphicon-transfer"></span></th>
                                        <th>MD</th>
                                        <th>Client</th>
                                        <th style="text-align: center;">CLU</th>
                                        <th>RWT</th>
                                        <th>Lead</th>
                                        <th>Gen</th>
                                        <th>Product</th>
                                        <th>Amount</th>
                                        <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <h5 style="float: right">
                                <strong>
                                    Total Declined&nbsp;<span><input id="cpd-deal-no-total" class="deal-count" readonly style="border: 1px solid"></span>
                                </strong>
                            </h5>
                            <!-- // Invited no table -->
                        </div>
                        <!-- // Invited column -->
                    </div>
                    <!-- // Invited column -->
                </div>
                <!-- Deal Creation tab -->

                <!-- Strategic tab -->
                <div role="tabpanel" class="tab-pane col-xs-12" id="strategic">
                    <div class="row">

                        <!-- Proposal & Update column -->
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <!-- In person table -->
                                <table class="table table-striped table-condensed deal-table" id="cpd-deal-pres_per-table">
                                    <thead style="background-color: #ededed">
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.2em;">Presentation:&nbsp;
												<span class="editable" id="pres_per-name">I</span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.1em;">Description:&nbsp;
												<span class="editable" id="pres_per-description"></span></span </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center"><span class="glyphicon glyphicon-transfer"></span></th>
                                        <th>MD</th>
                                        <th>Client</th>
                                        <th style="text-align: center;">CLU</th>
                                        <th>RWT</th>
                                        <th>Promos</th>
                                        <th>Updates</th>
                                        <th>Product</th>
                                        <th>Amount</th>
                                        <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <h5 style="float: right">
                                <strong>
                                    Total In Person Proposals&nbsp;<span><input id="cpd-deal-pres_per-total" class="deal-count" readonly style="border: 1px solid"></span>
                                </strong>
                            </h5>
                            <!-- // In person proposalstable -->
                            <div class="row">
                                <!-- Phone proposals table -->
                                <table class="table table-striped table-condensed deal-table" id="cpd-deal-pres_ph-table" cellpadding="100">
                                    <thead style="background-color: #ededed">
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.2em;">Presentation:&nbsp;
												<span class="editable" id="pres_ph-name"></span>
                                                </span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.1em;">Description:&nbsp;
												<span class="editable" id="pres_ph-description"></span></span </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center"><span class="glyphicon glyphicon-transfer"></span></th>
                                        <th>CD</th>
                                        <th>Client</th>
                                        <th style="text-align: center;">CLU</th>
                                        <th>RWT</th>
                                        <th>Promos</th>
                                        <th>Updates</th>
                                        <th>Product</th>
                                        <th>Amount</th>
                                        <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <h5 style="float: right">
                                <strong>
                                    Total Phone/Web Proposals&nbsp;<span><input id="cpd-deal-pres_ph-total" class="deal-count" style="border: 1px solid"></span>
                                </strong>
                            </h5>
                            <!-- // Presentation phone table -->

                        </div>
                        <!-- Presentation column -->

                        <!-- Exploratory Meeting column -->
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <!-- Meetings table -->
                                <table class="table table-striped table-condensed deal-table" id="cpd-deal-expl_per-table">
                                    <thead style="background-color: #ededed">
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.2em;">Exploratory:&nbsp;
												<span class="editable" id="expl_per-name"></span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.1em;">Description:&nbsp;
												<span class="editable" id="expl_per-description"></span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center"><span class="glyphicon glyphicon-transfer"></span></th>
                                        <th>MD</th>
                                        <th>Client</th>
                                        <th style="text-align: center;">CLU</th>
                                        <th>RWT</th>
                                        <th>Promos</th>
                                        <th>Updates</th>
                                        <th>Product</th>
                                        <th>Amount</th>
                                        <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <h5 style="float: right">
                                <strong>
                                    Total Exploratory In Person&nbsp;<span><input id="cpd-deal-expl_per-total" class="deal-count" readonly style="border: 1px solid"></span>
                                </strong>
                            </h5>
                            <!-- // Exploratory in person table -->
                            <div class="row">
                                <!-- Exploratory phone table -->
                                <table class="table table-striped table-condensed deal-table" id="cpd-deal-expl_ph-table">
                                    <thead style="background-color: #ededed">
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.2em;">Exploratory:&nbsp;
												<span class="editable" id="expl_ph-name"></span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.1em;">Description&nbsp;
												<span class="editable" id="expl_ph-description"></span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center"><span class="glyphicon glyphicon-transfer"></span></th>
                                        <th>MD</th>
                                        <th>Client</th>
                                        <th style="text-align: center;">CLU</th>
                                        <th>RWT</th>
                                        <th>Promos</th>
                                        <th>Updates</th>
                                        <th>Product</th>
                                        <th>Amount</th>
                                        <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <h5 style="float: right">
                                <strong>
                                    Total Exploratory Phone&nbsp;<span><input id="cpd-deal-expl_ph-total" class="deal-count" readonly style="border: 1px solid"></span>
                                </strong>
                            </h5>
                            <!-- // Exploratory Phone table -->

                        </div>
                        <!-- // Exploratory column -->

                        <!-- SA Invited column-->
                        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                            <div class="row">
                                <!-- Invited in person table -->
                                <table class="table table-striped table-condensed deal-table" id="cpd-deal-inv_per-table">
                                    <thead style="background-color: #ededed">
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.2em;">Invited:
												<span class="editable" id="inv_per-name"></span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.1em;">Description:&nbsp;
												<span class="editable" id="inv_per-description"></span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center"><span class="glyphicon glyphicon-transfer"></span></th>
                                        <th>MD</th>
                                        <th>Client</th>
                                        <th style="text-align: center;">CLU</th>
                                        <th>RWT</th>
                                        <th>Promos</th>
                                        <th>Updates</th>
                                        <th>Product</th>
                                        <th>Amount</th>
                                        <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <h5 style="float: right">
                                <strong>
                                    Total Invited In Person&nbsp;<span><input readonly id="cpd-deal-inv_per-total" class="deal-count" style="border: 1px solid"></span>
                                </strong>
                            </h5>
                            <!-- // Invited in person table -->
                            <div class="row">
                                <!-- Invited phone table -->
                                <table class="table table-striped table-condensed deal-table" id="cpd-deal-inv_ph-table">
                                    <thead style="background-color: #ededed">
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.2em;">Invited:&nbsp;
												<span class="editable" id="inv_ph-name"></span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th colspan="10">
                                                <span style="font-size: 1.1em;">Description:&nbsp;
												<span class="editable"
                                                      id="inv_ph-description"></span></span>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th class="text-center"><span class="glyphicon glyphicon-transfer"></span></th>
                                        <th>MD</th>
                                        <th>Client</th>
                                        <th style="text-align: center;">CLU</th>
                                        <th>RWT</th>
                                        <th>Promos</th>
                                        <th>Updates</th>
                                        <th>Product</th>
                                        <th>Amount</th>
                                        <th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                    </tfoot>
                                </table>
                            </div>
                            <h5 style="float: right">
                                <strong>
                                    Total Invited Phone&nbsp;<span><input readonly id="cpd-deal-inv_ph-total" class="deal-count" style="border: 1px solid"></span>
                                </strong>
                            </h5>
                            <!-- // Invited phone no table -->

                        </div>
                        <!-- // Invited column -->

                    </div>
                    <!-- // Invited column -->

                </div>
                <!-- Strategic tab -->

            </div>
        </div>
    </div>

    <!-- Dialogs -->
<?php $this->partial('views/cpd/dialogs/add_deal.php'); ?>
<?php $this->partial('views/cpd/dialogs/move_deal.php'); ?>
<?php $this->partial('views/cpd/dialogs/delete_deal.php'); ?>

<?php $this->partial('views/cpd/dialogs/view_promo.php'); ?>
<?php $this->partial('views/cpd/dialogs/delete_promo.php'); ?>
<?php $this->partial('views/cpd/dialogs/add_promo.php'); ?>

<?php $this->partial('views/cpd/dialogs/view_update.php'); ?>
<?php $this->partial('views/cpd/dialogs/delete_update.php'); ?>
<?php $this->partial('views/cpd/dialogs/add_update.php'); ?>