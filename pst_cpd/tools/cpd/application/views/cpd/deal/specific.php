<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/cpd/config/specific.js"></script>

<!-- Page Title -->
<div class="row">
	<div class="col-xs-12">
		<h3>Deal Management Dashboard Configuration: <span id="cfg-name"></span></h3>
		<h4>Description: <span id="cfg-description"></span></h4>
	</div>
</div>

<!-- Description -->
<div class="row">
	<div class="col-xs-12">
		<p>Use this page to update a Deal Management Dashboard Configuration.</p>.
	</div>
</div>

<!-- Show the separate configuration tabs -->
<div class="row">
	<div class="col-xs-12">

		<ul class="nav nav-tabs top-buffer" role="tablist">
			<li role="presentation" class="active">
				<a href="#ftg" role="tab" data-toggle="tab">Floor/Target/Game</a>
			</li>
			<li role="presentation">
				<a href="#phases" role="tab" data-toggle="tab">Phase Names</a>
			</li>
			<li role="presentation">
				<a href="#columns" role="tab" data-toggle="tab">Columns</a>
			</li>
		</ul>

		<div class="tab-content">

			<!-- FTG Tab -->
			<div role="tabpanel" class="tab-pane active" id="ftg">
				<div class="row">

					<!-- Floor/Target/Game table -->
					<div class="col-xs-12">
						<p class="top-buffer bottom-buffer">Set the Quarterly and Yearly Floor, Target, and Game values
							using the table below.</p>
						<table class="table table-striped table-condensed"
						       style="max-width: 500px; border-collapse: separate!important;" id="cfg-ftg-table">
							<thead style="background-color: #ededed">
							<tr>
								<th></th>
								<th>Quarterly</th>
								<th>Yearly</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td><strong>Floor</strong></td>
								<td id="cfg-q-floor">0</td>
								<td id="cfg-y-floor">0</td>
							</tr>
							<tr>
								<td><strong>Target</strong></td>
								<td id="cfg-q-target">0</td>
								<td id="cfg-y-target">0</td>
							</tr>
							<tr>
								<td><strong>Game</strong></td>
								<td id="cfg-q-game">0</td>
								<td id="cfg-y-game">0</td>
							</tr>
							</tbody>
							<tfoot>
							</tfoot>
						</table>
					</div>
					<!-- Floor/Target/Game table -->

				</div>
			</div>
			<!-- // FTG  tab -->

			<!-- Phase Tab -->
			<div role="tabpanel" class="tab-pane" id="phases">
				<div class="row">

					<!-- Phase name table -->
					<div class="col-xs-12">
						<p class="top-buffer bottom-buffer">Customize the names and descriptions of the process phases
							using the table below.</p>
						<table class="table table-striped table-condensed"
						       style="max-width: 500px; border-collapse: separate!important;" id="cfg-phase-table">
							<thead style="background-color: #ededed">
							<tr>
								<th>Phase</th>
								<th>Custom Name</th>
								<th>Description</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td><strong>Potential</strong></td>
								<td id="cfg-potential-n">Client Invitation</td>
								<td id="cfg-potential-d"><p>Targeted Conversations.</p></td>
							</tr>
							<tr>
								<td><strong>Possible</strong></td>
								<td id="cfg-possible-n">Client Meeting</td>
								<td id="cfg-possible-d"><p>Scheduled or Completed.</p></td>
							</tr>
							<tr>
								<td><strong>Prospective</strong></td>
								<td id="cfg-prospective-n">Deal Proposal</td>
								<td id="cfg-prospective-d"><p>Requested or Presented.</p></td>
							</tr>
							<tr>
								<td><strong>Probable</strong></td>
								<td id="cfg-probable-n">Deal Listed</td>
								<td id="cfg-probable-d"><p>Agreement Received.</p></td>
							</tr>
							<tr>
								<td><strong>Predictable</strong></td>
								<td id="cfg-predictable-n">Deal in Contract</td>
								<td id="cfg-predictable-d"><p>Documents Received.</p></td>
							</tr>
							<tr>
								<td><strong>Actual</strong></td>
								<td id="cfg-actual-n">Deal Closed</td>
								<td id="cfg-actual-d"><p>Payment Received.</p></td>
							</tr>
							</tbody>
							<tfoot>
							</tfoot>
						</table>
					</div>
					<!-- Phase name table -->

				</div>
			</div>
			<!-- // Phase tab -->

			<!-- Columns Tab -->
			<div role="tabpanel" class="tab-pane" id="columns">
				<div class="row">

					<!-- Column name table -->
					<div class="col-xs-12">
						<p class="top-buffer bottom-buffer">Customize the columns shown for each deal below.</p>
						<table class="table table-striped table-condensed" id="cfg-columns-table">
							<thead style="background-color: #ededed">
							<tr>
								<th>Name</th>
								<th>Abbreviation</th>
								<th>Description</th>
								<th>Type</th>
								<th>Extra</th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>Move Date</td>
								<td>MD</td>
								<td>Projected date the deal moves to the next phase.</td>
								<td>Date</td>
								<td>Format: M/Y</td>
							</tr>
							<tr>
								<td>Client and Property</td>
								<td>Client-Property</td>
								<td>The client and property.</td>
								<td>Text</td>
								<td></td>
							</tr>
							<tr>
								<td>CLU</td>
								<td>CLU</td>
								<td>What does this mean?</td>
								<td>Number</td>
								<td>Min: 1, Max: 10</td>
							</tr>
							<tr>
								<td>Tier</td>
								<td>Tier</td>
								<td>Trees, Wheat, Radish</td>
								<td>List</td>
								<td>T: Tree, W: Wheat, R: Radish</td>
							</tr>
							<tr>
								<td>Product Type</td>
								<td>P Type</td>
								<td>The product type</td>
								<td>Text</td>
								<td></td>
							</tr>
							<tr>
								<td>Amount</td>
								<td>$</td>
								<td>Project sale amount.</td>
								<td>Currency</td>
								<td>Type: US</td>
							</tr>
							</tbody>
							<tfoot>
							</tfoot>
						</table>
					</div>
					<!-- Columnname table -->

				</div>
			</div>
			<!-- // Columns tab -->

		</div>

	</div>

</div>
