<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/cpd/dashboard.js"></script>

<!-- Page Title -->
<div class="row">
  <div class="col-xs-12">
    <h2>Deal Management Dashboard</h2>
  </div>
</div>

<!-- Show the add cpd button. -->
<div class="row">
	<div class="col-xs-12 text-left">
		<button type="button" id="add-cpd" class="btn btn-primary bottom-buffer" data-toggle="modal"
		        data-target="#add-cpd-dialog" data-backdrop="static">Add New Deal Management Dashboard
		</button>
	</div>
</div>

<!-- Show the list of my DMD instances. -->
<div class="row">
	<div class="col-xs-12">

		<!-- DMD instance table -->
		<div class="row" id="cpd-instance-row">
			<div class="col-xs-12">
				<p id="no-cpd-instances">You do not have a Deal Management Dashboard Instance setup. To create one, use
					the Add Deal Management Dashboard Instance button.</p>
				<div id="cpd-instance-div" class="top-buffer">
					<table class="table table-striped" id="cpd-instance-table">
						<thead>
						<tr>
							<th>DMD ID</th>
							<th>Name</th>
							<th>Description</th>
							<th>TGD Name</th>
							<th>Created</th>
							<th>Modified</th>
							<th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- // DMD instance table -->

	</div>
</div>

<!-- Dialogs -->
<?php $this->partial('views/cpd/dialogs/add_cpd.php'); ?>
<?php $this->partial('views/cpd/dialogs/delete_cpd.php'); ?>
