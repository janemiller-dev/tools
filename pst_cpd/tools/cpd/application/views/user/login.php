<div class="container">
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-5 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-3 col-lg-offset-5 social-buttons">
			<p>Login using one of the services below.</p><br>
			<a href="login?authenticator=linkedin" class="btn btn-block btn-lg btn-social btn-linkedin">
				<i class="fa fa-linkedin"></i> Sign in with LinkedIn
			</a>
			<br/>
			<a href="login?authenticator=facebook" class="btn btn-block btn-lg btn-social btn-facebook">
				<i class="fa fa-facebook"></i> Sign in with Facebook
			</a>
			<!-- Google not working right now...
			<br/>
			<a href="/user/login?authenticator=google" class="btn btn-block btn-lg btn-social btn-google">
		  <i class="fa fa-google-plus"></i> Sign in with Google
			</a>
			-->
		</div>
	</div>
</div>
