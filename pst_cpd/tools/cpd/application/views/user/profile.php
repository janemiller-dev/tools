<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="/resources/app/js/user/profile.js"></script>

<!-- Page Title -->
<div class="row">
	<div class="col-xs-12">
		<h2>User Profile</h2>
	</div>
</div>

<!-- Show the user Information -->
<div class="row">
	<div class="col-xs-12 col-sm-3">
		<h3 id="name"></h3>
		<img id="profile-image" src="/resource/app/images/default.img" maxwidth="100%">
	</div>

	<div class="col-xs-12 col-sm-9">
		<h3>Profile Information</h3>

		<form class="form-horizontal">

			<div id="first-name-group" class="form-group">
				<label class="col-sm-2 control-label">First Name</label>
				<div class="col-sm-10" id="first-name">
				</div>
			</div>

			<div id="last-name-group" class="form-group">
				<label class="col-sm-2 control-label">Last Name</label>
				<div class="col-sm-10" id="last-name">
				</div>
			</div>

			<div class="form-group">
				<label class="col-sm-2 control-label">Email</label>
				<div class="col-sm-10" id="email-address">
				</div>
			</div>

		</form>

	</div>
</div>

<!-- Show the Authenticator Information -->
<div id="authenticators" class="row">
	<div class="col-xs-12">
		<h3>Authenticators</h3>

		<div id="authenticators" class="row">
			<div class="col-xs-12 col-sm-4">
				<div id="facebook-linked">
					<h4>Facebook</h4>
					<div class="row">
						<div class="col-xs-12">

							<form class="form-horizontal">
								<div class="form-group">
									<label class="col-sm-2 control-label">Name</label>
									<div class="col-sm-10 form-control-static" id="facebook-name">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Email</label>
									<div class="col-sm-10 form-control-static" id="facebook-email">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Profile</label>
									<div class="col-sm-10 form-control-static" id="facebook-profile">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Photo</label>
									<div class="col-sm-10 form-control-static" id="facebook-photo">
									</div>
								</div>
							</form>

						</div>
					</div>
				</div>
				<div id="facebook-link">
					<a href="/user/link_account?authenticator=facebook"
					   class="btn btn-block btn-lg btn-social btn-facebook">
						<i class="fa fa-facebook"></i> Link Account with Facebook
					</a>
				</div>
			</div>

			<div class="col-xs-12 col-sm-4">
				<div id="linkedin-linked">
					<h4>Linkedin</h4>
					<div class="row">
						<div class="col-xs-12">

							<form class="form-horizontal">
								<div class="form-group">
									<label class="col-sm-2 control-label">Name</label>
									<div class="col-sm-10 form-control-static" id="linkedin-name">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Email</label>
									<div class="col-sm-10 form-control-static" id="linkedin-email">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Profile</label>
									<div class="col-sm-10 form-control-static" id="linkedin-profile">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Photo</label>
									<div class="col-sm-10 form-control-static" id="linkedin-photo">
									</div>
								</div>
							</form>

						</div>
					</div>
				</div>
				<div id="linkedin-link">
					<a href="/user/link_account?authenticator=linkedin"
					   class="btn btn-block btn-lg btn-social btn-linkedin">
						<i class="fa fa-linkedin"></i> Link Account with LinkedIn
					</a>
				</div>
			</div>

			<div class="col-xs-12 col-sm-4">
				<div id="google-linked">
					<h4>Google+</h4>
					<div class="row">
						<div class="col-xs-12">

							<form class="form-horizontal">
								<div class="form-group">
									<label class="col-sm-2 control-label">Name</label>
									<div class="col-sm-10 form-control-static" id="google-name">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Email</label>
									<div class="col-sm-10 form-control-static" id="google-email">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Profile</label>
									<div class="col-sm-10 form-control-static" id="google-profile">
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Photo</label>
									<div class="col-sm-10 form-control-static" id="google-photo">
									</div>
								</div>
							</form>

						</div>
					</div>
				</div>
				<div id="google-link">
					<a href="/user/link_account?authenticator=google"
					   class="btn btn-block btn-lg btn-social btn-google">
						<i class="fa fa-google-plus"></i> Link Account with Google+
					</a>
				</div>
			</div>

		</div>
	</div>
</div>
