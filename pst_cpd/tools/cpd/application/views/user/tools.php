<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="/resources/app/js/user/tools.js"></script>

<!-- Page Title -->
<div class="row">
	<div class="col-xs-12">
		<h2>My Tools</h2>
	</div>
</div>

<!-- Show the list of tools in their groups. -->
<div class="row">
	<div class="col-xs-12">
		<div id="tool-groups" class="panel-group" role="tablist" aria-multiselectable="true">
		</div>
	</div>
</div>
