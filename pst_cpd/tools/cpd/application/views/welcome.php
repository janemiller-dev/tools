<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>
<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/dashboard.css"/>

<div class="row" id="welcome-page">
    <div class="" align="center">
        <button class="btn btn-secondary" data-toggle="modal" data-target="#view-welcome-dialog"
                data-title="Sales Game Navigator"
                style="margin: 2px; font-size: 5vh; width: 30vw; height: 8vh; border-radius: 12px; color: #2a3f54;"
                data-href="https://cpd.powersellingtools.com/wp-content/uploads/2019/09/AST-Welcome-1-Major-Revision-min.png">
            Sales Game Navigator
        </button>
        <h1 style="color: #2a3f54; margin-top: 1vh; font-size: 4vh;">Welcome to the Brave New World of Automation in
            Selling!</h1>
        <h4 style="color: #708090; margin-top: 40px; font-style: italic">Click any of the gray boxes above and below to
            find out more about
            Tools and Components..</h4>

        <button class="btn btn-secondary" style="font-size: 2vh; width: 18vw; white-space: nowrap; height: 4vh;"
                data-toggle="modal"
                data-target="#view-welcome-dialog" data-title="Targeted Suspect List"
                data-href="https://cpd.powersellingtools.com/wp-content/uploads/2019/09/AST-Welcome-3-Major-Revision-min.png">
            TARGETED SUSPECT LIST
        </button>

        <div class="col-xs-8 col-xs-offset-2">
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-secondary" data-toggle="modal" data-target="#view-welcome-dialog"
                        data-title="Promo Components"
                        data-href="https://cpd.powersellingtools.com/wp-content/uploads/2019/09/AST-Welcome-7-Major-Revision-min.png">
                    PROMOS
                </button>
            </div>
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-default">PROMO BUILDER
                </button>
            </div>
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-default">MESSAGE BUILDER
                </button>
            </div>
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-default">CALL SCRIPT BUILDER
                </button>
            </div>
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-default">CONFIRMATIONS BUILDER</button>
            </div>
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-default">DEAL STORY BUILDER
                </button>
            </div>
        </div>

        <div class="col-xs-8 col-xs-offset-2">
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-secondary" data-toggle="modal" data-target="#view-welcome-dialog"
                        data-title="Strategic Advisory Program"
                        data-href="https://cpd.powersellingtools.com/wp-content/uploads/2019/09/AST-Welcome-8-Major-Revision-min.png">
                    ANALYSIS
                </button>
            </div>
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-default">SURVEY BUILDER
                </button>
            </div>
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-default">PROFILE BUILDER
                </button>
            </div>
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-default">ANALYSIS BUILDER
                </button>
            </div>
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-default">UPDATES BUILDER
                </button>
            </div>
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-default">NOTICES BUILDER
                </button>
            </div>
        </div>

        <p></p>
        <button class="btn btn-secondary"
                style="font-size: 2vh; width: 18vw; white-space: nowrap; height: 4vh; margin-top: 5vh"
                data-toggle="modal"
                data-title="Deal Management Dashboard"
                data-href="https://cpd.powersellingtools.com/wp-content/uploads/2019/09/AST-Welcome-4-Major-Revision-min.png"
                data-target="#view-welcome-dialog">DEAL MANAGEMENT DASHBOARD
        </button>

        <div class="col-xs-8 col-xs-offset-2">
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-secondary" data-toggle="modal" data-title="Meeting Components"
                        data-target="#view-welcome-dialog"
                        data-href="https://cpd.powersellingtools.com/wp-content/uploads/2019/09/AST-Welcome-9-Major-Revision-min.png"
                >MEETINGS
                </button>
            </div>
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-default">EMAIL BUILDER
                </button>
            </div>
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-default">SKILLS BUILDER
                </button>
            </div>
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-default">EVENT BUILDER
                </button>
            </div>
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-default">DEBREIFING BUILDER
                </button>
            </div>
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-default">FOLLOW-UP BUILDER
                </button>
            </div>
        </div>

        <div class="col-xs-8 col-xs-offset-2">
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-secondary" data-title="Document Components" data-toggle="modal"
                        data-target="#view-welcome-dialog"
                        data-href="https://cpd.powersellingtools.com/wp-content/uploads/2019/09/AST-Welcome-10-Major-Revision-min.png">
                    DOCUMENTS
                </button>
            </div>
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-default">DOCUMENT ASSEMBLY
                </button>
            </div>
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-default">MEDIA UPLOADER
                </button>
            </div>
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-default">DOCUMENTS UPLOADER
                </button>
            </div>
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-default">AGREEMENTS UPLOADER
                </button>
            </div>
            <div class=" col-xs-2" style="padding: 1vw">
                <button class="btn btn-default">WEB PRESENTATION
                </button>
            </div>
        </div>

        <p></p>
        <button class="btn btn-secondary"
                style="font-size: 2vh; width: 18vw; white-space: nowrap; height: 4vh; margin-top: 5vh"
                data-toggle="modal"
                data-target="#view-welcome-dialog" data-title="Tactical Game Designer"
                data-href="https://cpd.powersellingtools.com/wp-content/uploads/2019/09/AST-Welcome-5-Major-Revision-min.png">
            TACTICAL GAME DESIGNER
        </button>
        <button class="btn btn-secondary"
                style="font-size: 2vh; width: 10vw; white-space: nowrap; height: 4vh; margin-top: 5vh"
                data-toggle="modal" data-title="Homebase"
                data-target="#view-welcome-dialog"
                data-href="https://cpd.powersellingtools.com/wp-content/uploads/2019/09/AST-Welcome-2-Major-Revision-min.png">
            HOMEBASE
        </button>
        <button class="btn btn-secondary"
                style="font-size: 2vh; width: 18vw; white-space: nowrap; height: 4vh; margin-top: 5vh"
                data-toggle="modal"
                data-target="#view-welcome-dialog" data-title="Sales Activity Manager"
                data-href="https://cpd.powersellingtools.com/wp-content/uploads/2019/09/AST-Welcome-6-Major-Revision-min.png">
            SALES ACTIVITY MANAGER
        </button>
    </div>
</div>
<!--// Collapse-->

<?php $this->partial('views/dialogs/view_welcome_dialog.php'); ?>
