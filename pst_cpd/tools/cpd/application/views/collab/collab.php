<!-- Strategic Advisory Document.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/collab.css"/>

<div class="container">
<div class="row">
    <!--Container.-->
    <div class="col-xs-12">
        <div id="collab-container" style="padding-left: 2px">
        </div>
    </div>
    <!--// Container.-->
</div>
</div>
<?php $this->partial('views/collab/dialogs/add_collab_details.php'); ?>
<?php $this->partial('views/collab/dialogs/send_collab_response.php'); ?>

<script src="<?php echo $this->basepath; ?>resources/app/js/collab/collab.js"></script>


