<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<div class="modal fade" id="delete-collab-dialog" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="delete-collab-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="delete-collab-form" class="form-horizontal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="delete-collab-title" class="modal-title">Delete Collaboration Message?</h2>
                </div>

                <div class="modal-body modal-small">
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="name">From</label>
                        <div class="col-xs-12 col-sm-9">
                            <p class="form-control-static" id="collab-name"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="action">Format</label>
                        <div class="col-xs-12 col-sm-9">
                            <p class="form-control-static" id="collab-format"></p>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="close-delete-collab-dialog" data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary" id="delete-collab-button">Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>
