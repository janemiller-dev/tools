<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="send-collab-response-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="send-collab-response-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 id="send-collab-response-title" class="modal-title">Collaboration Response</h2>
            </div>

            <div class="modal-body">
                <form id="collab-response-form" class="form-horizontal">
                    <div class="form-group" style="margin: 5px !important;">
                        <label class="col-xs-2 control-label">From:</label>
                        <div class="col-xs-10" style="padding: 0">
                            <input type="text" class="form-control" id="send-collab-response-sender-name"
                                   name="sender">
                        </div>
                    </div>

                    <div class="form-group" style="margin: 5px !important;">
                        <label class="col-xs-2 control-label" for="collab-response-client-email">To:</label>
                        <div class="col-xs-10" style="padding: 0">
                            <select class="form-control col-xs-10" id="collab-response-send-recipient"
                                    name="recipient_id"></select>
                        </div>
                    </div>

                    <div class="form-group" style="margin: 5px !important;">
                        <label class="col-xs-2 control-label" for="collab-response-client-email">Email:</label>
                        <div class="col-xs-10" style="padding: 0">
                            <input type="email" class="form-control collab-response-client-email" name="client_email[]">
                        </div>
                    </div>

                    <div class="form-group" style="margin: 5px !important;">
                        <label class="col-xs-2 control-label" for="collab-response-client-email">Format:</label>
                        <div class="col-xs-10" style="padding: 0">
                            <select class="form-control" id="send-collab-format" name="send_collab_format"></select>
                        </div>
                    </div>

                    <div class="form-group" style="margin: 5px !important;">
                        <label class="col-xs-2 control-label" for="collab-response-client-subject">Subject:</label>
                        <div class="col-xs-10" style="padding: 0">
                            <input type="text" class="form-control" id="collab-response-client-subject" name="subject">
                        </div>
                    </div>

                    <div id="email-content" class="form-group">
                        <label class="col-xs-2 control-label" for="collab-response-client-email"
                               style="padding-right: 0;">Message:</label>
                        <div class="col-xs-10">
                        <textarea cols="2" placeholder="Email Text" name="text" class="form-control input-group"
                                  style="width: 100%"></textarea>
                        </div>
                    </div>
                </form>
            </div>

            <input type="hidden" id="collab-response-type" name="type"/>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="close-send-collab-response-dialog"
                        data-dismiss="modal">
                    Close
                </button>
                <button type="button" class="btn btn-primary" id="send-collab-response-button">Send</button>
            </div>
        </div>
    </div>
</div>
