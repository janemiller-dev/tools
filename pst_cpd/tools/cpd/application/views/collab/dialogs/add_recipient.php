<!-- Add Collaboration Component: Recipient/Script/Format.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/collab/dialogs/add_recipient.js"></script>

<div class="modal fade" id="add-recipient-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="add-recipient-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="add-recipient-form" class="form-horizontal">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-recipient-component-title" class="modal-title">Add New Recipient.</h2>
                </div>
                <!-- // Modal Header -->

                <div class="modal-body modal-small">
                    <!-- Lead Name field. -->
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label">Recipient Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <input type="text" class="form-control" id="recipient-name" name="recipient_name"/>
                            <p class="form-text text-muted" id="recipient-name-para"></p>
                        </div>
                    </div>
                    <!--// Lead Name field. -->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="button" id="save_recipient" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
