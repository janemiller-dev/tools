<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/collab/dialogs/send_collab.js"></script>

<div class="modal fade" id="send-collab-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="send-collab-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="send-collab-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="send-collab-title" class="modal-title">Collaboration Request</h2>
                </div>

                <div class="modal-body">
                    <!-- Sender input field-->
                    <div class="form-group col-xs-4 send-collab-div">
                        <label class="control-label">Sender Name:</label>
                        <input type="text" disabled id="email-sender-name" class="form-control">
                    </div>
                    <!--// Sender input field-->

                    <!-- Recipient Select Drop down. -->
                    <div class="form-group col-xs-4 send-collab-div">
                        <label class="control-label" for="collaborator_name">Recipient Name:</label>
                        <select id="collaborator_name" class="form-control"></select>
                    </div>
                    <!--// Recipient Select Drop down. -->

                    <!-- Format field-->
                    <div class="form-group col-xs-4 send-collab-div">
                        <label class="control-label">Format:</label>
                        <select id="email-collab-format" class="form-control"></select>
                    </div>
                    <!--// Format field-->

                    <!-- Recipient Email -->
                    <div class="form-group" style="margin: 5px !important;">
                        <label class="col-sm-2" for="client-collab">Email:</label>
                        <div class="col-xs-12 col-sm-10 input-group client-collab-div">
                            <input type="collab" class="form-control client-collab" id="send-collab-id">
                            <span class="input-group-btn">
                                <span class="has-tooltip" title="Add Recipient" data-placement="bottom">
                                    <button class="btn btn-default" type="button" id="add-recipient">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </span>
                            </span>
                        </div>
                    </div>
                    <!--// Recipient Email -->

                    <!-- Subject Textbox -->
                    <div class="form-group" style="margin: 5px !important;">
                        <label class="col-xs-12 col-sm-2" for="client-subject">Subject:</label>
                        <div class="col-xs-12 col-sm-10" style="padding: 0">
                            <input type="text" class="form-control" id="client-subject">
                        </div>
                    </div>
                    <!--// Subject Textbox -->

                    <!-- Date Time Textbox -->
                    <div class="form-group" style="margin: 5px !important;">
                        <label class="col-xs-12 col-sm-2" for="client-subject">Date/Time:</label>
                        <div class="col-xs-12 col-sm-10" style="padding: 0">
                            <input type="text" class="form-control" id="send-datetime"/>
                        </div>
                    </div>
                    <!--// Date Time Textbox -->

                    <!-- Message Textarea -->
                    <div class="form-group" style="margin: 5px !important;">
                        <label class="col-xs-12 col-sm-2" for="email-text">Message:</label>
                        <div class="col-xs-12 col-sm-10" style="padding: 0">
                            <textarea class="form-control" rows="2" id="email-text"></textarea>
                        </div>
                    </div>
                    <!--// Message Textarea -->
                </div>

                <div class="modal-footer">
                    <span class="hidden" style="float: left; font-size: 150%" id="attachment_span"><i
                                class="fa fa-paperclip"></i><span id="attachment_name"></span></span>
                    <button type="button" class="btn btn-primary" id="close-send-collab-dialog" data-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary" id="send-collab-button">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>
