<!-- Add Collaboration Component: Recipient/Script/Format.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/collab/dialogs/filter_collab.js"></script>

<div class="modal fade" id="filer-collab-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="add-recipient-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="add-recipient-form" class="form-horizontal">
                <!-- Modal Header -->
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-recipient-component-title" class="modal-title">Filter Collaboration Data.</h2>
                </div>
                <!-- // Modal Header -->

                <div class="modal-body modal-small">
                    <!-- Lead Name field. -->
                    <div class="form-group">
                        <div class="col-xs-6">
                            <label> Collaborator Name</label>
                            <select class="form-control" id="filter-recipient-name"></select>
                        </div>

                        <div class="col-xs-6">
                            <label> Collaboration Action </label>
                            <select class="form-control" id="filter-collab-action"></select>
                        </div>
                    </div>
                    <!--// Lead Name field. -->
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="button" id="filter_collab" class="btn btn-primary">Filter</button>
                </div>
            </form>
        </div>
    </div>
</div>
