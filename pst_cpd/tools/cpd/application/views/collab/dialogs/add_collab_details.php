<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="add-collab-dialog" tabindex="-1" role="dialog" aria-labelledby="collab-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">

        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 class="modal-title">Add Collaboration Details</h2>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-xs-4" id="sender_div">
                        <label> Sender Name</label>
                        <input type="text" id="sender_name" class="form-control">
                    </div>

                    <div class="col-xs-4" id="collaborator_div">
                        <label> Collaborator Name</label>
                        <select class="form-control" id="recipient_name"></select>
                    </div>

                    <div class="col-xs-4" id="action_div">
                        <label> Select Format </label>
                        <select class="form-control" id="collab-action"></select>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                <button type="button" id="save_collab_details" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>

<?php $this->partial('views/collab/dialogs/add_recipient.php'); ?>


