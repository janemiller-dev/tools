<!-- Strategic Advisory Document.-->

<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/minified_cpdv1/sa.css"/>

<hr>
<div class="row">
    <!--Container.-->
    <div class="col-xs-12">
        <div id="univ-collab-container" style="padding-left: 2px" class="text-center">
            <h2>General Collaboration</h2>
        </div>
        <div class="container" id="collab-row">
            <div id="dummy-row" class="collab-box hidden" style="margin: 10px">
                <label id="title"></label>
                <div class="input-group">
                    <textarea id="collab_ques" class="form-control"></textarea>
                    <span class="input-group-addon btn btn-secondary collab-reply"><i class="fa fa-reply"></i>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <!--// Container.-->
</div>

<?php $this->partial('views/collab/dialogs/add_collab_details.php'); ?>

<script src="<?php echo $this->basepath; ?>resources/app/js/collab/univ_collab.js"></script>
<!---->

