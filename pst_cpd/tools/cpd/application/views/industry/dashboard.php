<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/industry/dashboard.js"></script>

<!-- Page Title -->
<div class="row">
	<div class="col-xs-12">
		<h2>Industry Dashboard</h2>
	</div>
</div>

<!-- Show the add industry button. -->
<div class="row">
	<div class="col-xs-12 text-left">
		<button type="button" id="add-industry" class="btn btn-primary bottom-buffer" data-toggle="modal"
		        data-target="#add-industry-dialog">Add TGD Industry
		</button>
	</div>
</div>

<!-- Show the list of my TGD industries. -->
<div class="row">
	<div class="col-xs-12">

		<!-- TGD industry table -->
		<div class="row" id="tgd-industry-row">
			<div class="col-xs-12">
				<p id="no-tgd-industries">You have not created any Tactical Game Designer Industries. To create one, use
					the Add Tactical Game Designer Industry button.</p>
				<div id="tgd-industry-div" class="top-buffer">
					<table class="table table-striped" id="tgd-industry-table">
						<thead>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th class="text-center"><span class="glyphicon glyphicon-duplicate"></span></th>
							<th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- // TGD industry table -->

	</div>
</div>

<!-- Dialogs -->
<?php $this->partial('views/industry/dialogs/add_industry.php'); ?>
<?php $this->partial('views/industry/dialogs/delete_industry.php'); ?>
<?php $this->partial('views/industry/dialogs/copy_industry.php'); ?>
