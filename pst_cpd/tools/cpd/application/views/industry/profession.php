<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

	<!-- Load the javascript support. -->
	<script src="<?php echo $this->basepath; ?>resources/app/js/industry/profession.js"></script>

	<!-- Load the custom css. -->
	<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/industry/profession.css"/>

	<!-- Page Title -->
	<div class="row">
		<div class="col-xs-12">
			<div class="text-left">
				<h4>Profession</h4>
			</div>
		</div>
	</div>

	<!-- The profession -->
	<div class="row">
		<div class="col-xs-12">
			<form id="heading-form" class="form-horizontal">

				<div class="form-group">
					<label class="col-xs-12 col-sm-4 control-label">
						<h3>Industry:</h3>
					</label>
					<div class="col-xs-12 col-sm-8">
						<h3 id="industry-name" class="form-control-static"></h3>
					</div>
				</div>

				<div class="form-group">
					<label class="col-xs-12 col-sm-4 control-label">
						<h3>Profession:</h3>
					</label>
					<div class="col-xs-12 col-sm-8">
						<h3 id="profession-name" class="form-control-static has-tip"></h3>&nbsp;<span
								class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover"
								title="The Profession Name" data-content="The name of the Profession."></span>
					</div>
				</div>

				<div class="form-group">
					<label class="col-xs-12 col-sm-4 control-label">
						<h4>Description:</h4>
					</label>
					<div class="col-xs-12 col-sm-8">
						<h4 id="profession-description" class="form-control-static has-tip"></h4>&nbsp;<span
								class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover"
								title="The Profession Description"
								data-content="The description of the Profession."></span>
					</div>
				</div>

			</form>
		</div>
	</div>

	<!-- Show the buttons. -->
	<div class="row">
		<div class="col-xs-12 buffer-top">
			<div id="tool-buttons" class="btn-toolbar">
			</div>
		</div>
	</div>

	<!-- Show the Profession conditions and multipliers -->
	<div class="row">
		<div class="col-xs-12">

			<!-- Profession condition table. -->
			<div class="row">
				<div class="col-xs-12">
					<h5>Objective and Conditions</h5>
					<div id="no-conditions">
						<p>There are no conditions or objective for the industry. To add a new condition or the
							objective, use the "Add Condition" button above</p>
					</div>

					<div id="condition-table-div" class="top-buffer">
						<p>The Industry Conditions show the steps, from the start of the sales process to the ending
							Objective.</p>
						<table class="table table-striped" id="condition-table">
							<thead class="main-table">
							<tr>
								<th>Type</th>
								<th>Name</th>
								<th>Units</th>
								<th>Description</th>
								<th>Help Message</th>
								<th class="text-center"><span class="glyphicon glyphicon-arrow-up"></span></th>
								<th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
							</tr>
							</thead>
							<tbody class="main-table">
							</tbody>
							<tfoot class="main-table">
							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<!-- // Profession condition table -->

			<!-- Profession Multiplier table. -->
			<div class="row">
				<div class="col-xs-12">
					<h5>Multipliers and Adders</h5>
					<div id="no-multipliers">
						<p>There are no multipliers or adders for the industry. To add a new multiplier or adders, use
							the "Add Multiplier/Adder" button above</p>
					</div>

					<div id="multiplier-table-div" class="top-buffer">
						<p>The Industry Multipliers/Adders show indicate values that either multiply the average income
							per achieved objective or add to the quarterly objects.</p>
						<table class="table table-striped" id="multiplier-table">
							<thead class="main-table">
							<tr>
								<th>Name</th>
								<th>Description</th>
								<th>Type</th>
								<th>Operation</th>
								<th>Propagate</th>
								<th class="text-center"><span class="glyphicon glyphicon-arrow-up"></span></th>
								<th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
							</tr>
							</thead>
							<tbody class="main-table">
							</tbody>
							<tfoot class="main-table">
							</tfoot>
						</table>
					</div>
				</div>
			</div>
			<!-- Profession condition table -->

		</div>
	</div>

	<!-- Dialogs -->
<?php $this->partial('views/industry/dialogs/add_condition.php'); ?>
<?php $this->partial('views/industry/dialogs/delete_condition.php'); ?>

<?php $this->partial('views/industry/dialogs/add_multiplier.php'); ?>
<?php $this->partial('views/industry/dialogs/delete_multiplier.php'); ?>

<?php $this->partial('views/industry/dialogs/add_profession.php'); ?>
<?php $this->partial('views/industry/dialogs/copy_profession.php'); ?>
<?php $this->partial('views/industry/dialogs/delete_profession.php'); ?>
