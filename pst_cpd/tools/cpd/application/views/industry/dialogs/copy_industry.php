<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/industry/dialog/copy_industry.js"></script>

<div class="modal fade" id="copy-industry-dialog" tabindex="-1" role="dialog" aria-labelledby="copy-industry-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<form id="copy-industry-form" class="form-horizontal">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h2 id="copy-industry-title" class="modal-title">Save as New Industry</h2>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">New Industry</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="save-as-name" name="save_as_name"/>
							<p class="form-text text-muted">Enter the name of the new Tactical Game Designer
								industry.</p>
						</div>
					</div>

					<div id="select-existing-description">
						<p>If you would like to over-write another existing industry, select the industry below. If
							there are any Tactical Game Design instances of the specified industry, you will not be able
							to overwrite the industry.</p>
					</div>

					<div id="select-existing-div" class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Existing Industry</label>
						<div class="col-xs-12 col-sm-9">
							<select class="form-control" id="save-as-id" name="save_as_id"/>
							</select>
						</div>
					</div>

				</div>

				<input type="hidden" id="copy-industry-id" name="industry_id" value=""/>

				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>

			</form>
		</div>
	</div>
</div>
