<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/industry/dialog/add_multiplier.js"></script>

<div class="modal fade" id="add-multiplier-dialog" tabindex="-1" role="dialog" aria-labelledby="add-multiplier-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<form id="add-multiplier-form" class="form-horizontal">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h2 id="add-multiplier-title" class="modal-title">Add Industry Multiplier/Adder</h2>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Name</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="industry-add-m-name" name="name"/>
							<p class="form-text text-muted">The name of the new Multiplier or Adder.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="description">Description</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="industry-add-m-description" name="description"/>
							<p class="form-text text-muted">The description of the Multiplier.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="operation">Operation</label>
						<div class="col-xs-12 col-sm-9">
							<select class="form-control" id="industry-add-m-operation" name="operation">
								<option value="mult" selected="selected">Multiply</option>
								<option value="add">Add</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="type">Type</label>
						<div class="col-xs-12 col-sm-9">
							<select class="form-control" id="industry-add-m-type" name="type">
								<option value="dollar" selected="selected">Unit</option>
								<option value="percent">Percentage</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="propagate">Propagate</label>
						<div class="col-xs-12 col-sm-9">
							<select class="form-control" id="industry-add-m-propagate" name="propagate">
								<option value="yes" selected="selected">Yes</option>
								<option value="no">No</option>
							</select>
						</div>
					</div>

				</div>

				<input type="hidden" id="industry-add-m-id" name="industry_id"/>

				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Create Multiplier</button>
				</div>

			</form>
		</div>
	</div>
</div>
