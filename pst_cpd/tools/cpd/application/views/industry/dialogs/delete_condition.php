<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/industry/dialog/delete_condition.js"></script>

<div class="modal fade" id="delete-condition-dialog" tabindex="-1" role="dialog"
     aria-labelledby="delete-condition-label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<form id="delete-condition-form" class="form-horizontal">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h2 id="delete-condition-title" class="modal-title">Delete Industry Condition/Objective?</h2>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Name</label>
						<div class="col-xs-12 col-sm-9">
							<p class="form-control-static" id="profession-delete-c-name"></p>
						</div>
					</div>

				</div>

				<input type="hidden" id="profession-delete-c-id" name="profession_c_id"/>

				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Delete</button>
				</div>

			</form>
		</div>
	</div>
</div>
