<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/industry/dialog/use_product.js"></script>

<div class="modal fade" id="add-product-dialog" tabindex="-1" role="dialog" aria-labelledby="add-product-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="add-product-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-product-title" class="modal-title">Use a New Product</h2>
                </div>

                <div class="modal-body">

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="name">Product Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <input class="form-control" id="product-name" name="product"></input>
                            </div>
                            <p class="form-text text-muted">Select the Product.</p>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Use Product</button>
                </div>
            </form>
        </div>
    </div>
</div>
