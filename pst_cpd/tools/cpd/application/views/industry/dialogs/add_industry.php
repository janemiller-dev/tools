<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/industry/dialog/add_industry.js"></script>

<div class="modal fade" id="add-industry-dialog" tabindex="-1" role="dialog" aria-labelledby="add-industry-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<form id="add-industry-form" class="form-horizontal">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h2 id="add-industry-title" class="modal-title">Add Tactical Game Designer Industry</h2>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Name</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="industry-name" name="name"/>
							<p class="form-text text-muted">Enter the name of the new Tactical Game Designer
								Industry.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Description</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="industry-description" name="description"/>
							<p class="form-text text-muted">Add a description of the Industry.</p>
						</div>
					</div>

				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Create Industry</button>
				</div>

			</form>
		</div>
	</div>
</div>
