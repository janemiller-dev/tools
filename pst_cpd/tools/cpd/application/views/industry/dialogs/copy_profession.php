<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/industry/dialog/copy_profession.js"></script>

<div class="modal fade" id="copy-profession-dialog" tabindex="-1" role="dialog" aria-labelledby="copy-profession-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<form id="copy-profession-form" class="form-horizontal">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h2 id="copy-profession-title" class="modal-title">Save as New profession</h2>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">New profession</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="save-as-name" name="save_as_name"/>
							<p class="form-text text-muted">Enter the name of the new Profession.</p>
						</div>
					</div>

					<div id="select-existing-description">
						<p>If you would like to over-write another existing profession, select the profession below. If
							there are any instances of the specified profession, you will not be able to overwrite the
							profession.</p>
					</div>

					<div id="select-existing-div" class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Existing Profession</label>
						<div class="col-xs-12 col-sm-9">
							<select class="form-control" id="save-as-id" name="save_as_id"/>
							</select>
						</div>
					</div>

				</div>

				<input type="hidden" id="copy-profession-id" name="profession_id" value=""/>

				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>

			</form>
		</div>
	</div>
</div>
