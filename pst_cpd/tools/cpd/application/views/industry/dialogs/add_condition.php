<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/industry/dialog/add_condition.js"></script>

<div class="modal fade" id="add-condition-dialog" tabindex="-1" role="dialog" aria-labelledby="add-condition-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<form id="add-condition-form" class="form-horizontal">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h2 id="add-condition-title" class="modal-title">Add Profession Condition/Objective</h2>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Type</label>
						<div class="col-xs-12 col-sm-9">
							<select class="form-control" id="profession-add-c-type" name="type">
								<option value="cond" selected="selected">Condition</option>
								<option value="obj">Objective</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Name</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="profession-add-c-name" name="name"/>
							<p class="form-text text-muted">The name of the new Condition or Objective.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="description">Description</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="profession-add-c-description"
							       name="description"/>
							<p class="form-text text-muted">The description of the Condition.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="tooltip">Help Tip</label>
						<div class="col-xs-12 col-sm-9">
							<input type="text" class="form-control" id="profession-add-c-tooltip" name="tooltip"/>
							<p class="form-text text-muted">The help line that will be given to the user for this
								Condition.</p>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="uom">Unit of Measurement</label>
						<div class="col-xs-12 col-sm-9">
							<select class="form-control" id="profession-add-c-uom" name="uom">
								<option value="n" selected="selected">Number</option>
								<option value="d">Dollars</option>
							</select>
						</div>
					</div>

				</div>

				<input type="hidden" id="profession-add-c-id" name="profession_id"/>

				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Create Condition</button>
				</div>

			</form>
		</div>
	</div>
</div>
