<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/industry/dialog/delete_profession.js"></script>

<div class="modal fade" id="delete-profession-dialog" tabindex="-1" role="dialog"
     aria-labelledby="delete-profession-label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<form id="delete-profession-form" class="form-horizontal">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h2 id="delete-profession-title" class="modal-title">Delete Profession?</h2>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Name</label>
						<div class="col-xs-12 col-sm-9">
							<p class="form-control-static" id="tgd-del-gt-name"></p>
						</div>
					</div>

				</div>

				<input type="hidden" id="profession-id" name="profession_id"/>

				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Delete</button>
				</div>

			</form>
		</div>
	</div>
</div>
