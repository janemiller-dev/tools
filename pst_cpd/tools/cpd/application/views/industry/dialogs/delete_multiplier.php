<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/industry/dialog/delete_multiplier.js"></script>

<div class="modal fade" id="delete-multiplier-dialog" tabindex="-1" role="dialog"
     aria-labelledby="delete-multiplier-label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">

			<form id="delete-multiplier-form" class="form-horizontal">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h2 id="delete-multiplier-title" class="modal-title">Delete Industry Multiplier/Adder?</h2>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label class="col-xs-12 col-sm-3 control-label" for="name">Name</label>
						<div class="col-xs-12 col-sm-9">
							<p class="form-control-static" id="industry-delete-m-name"></p>
						</div>
					</div>

				</div>

				<input type="hidden" id="industry-delete-m-id" name="industry_m_id"/>

				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Delete</button>
				</div>

			</form>
		</div>
	</div>
</div>
