<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

	<!-- Load the javascript support. -->
	<script src="<?php echo $this->basepath; ?>resources/app/js/industry/industry.js"></script>

	<!-- Load the custom css. -->
	<link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/industry/industry.css"/>

	<!-- Page Title -->
	<div class="row">
		<div class="col-xs-12">
			<div class="text-left">
				<h4>Industry</h4>
			</div>
		</div>
	</div>

	<!-- The game type -->
	<div class="row">
		<div class="col-xs-12">
			<form id="heading-form" class="form-horizontal">

				<div class="form-group">
					<label class="col-xs-12 col-sm-4 control-label">
						<h3>Industry:</h3>
					</label>
					<div class="col-xs-12 col-sm-8">
						<h3 id="industry-name" class="form-control-static has-tip"></h3>&nbsp;<span
								class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover"
								title="The Game Type Name" data-content="The name of the Game Type."></span>
					</div>
				</div>

				<div class="form-group">
					<label class="col-xs-12 col-sm-4 control-label">
						<h4>Description:</h4>
					</label>
					<div class="col-xs-12 col-sm-8">
						<h4 id="industry-description" class="form-control-static has-tip"></h4>&nbsp;<span
								class="has-popover glyphicon glyphicon-info-sign" data-toggle="popover"
								title="The Game Type Description"
								data-content="The description of the Game Type."></span>
					</div>
				</div>

			</form>
		</div>
	</div>

	<!-- Show the buttons. -->
	<div class="row">
		<div class="col-xs-12 buffer-top">
			<div id="tool-buttons" class="btn-toolbar">
			  <button type="button" id="add-profession-btn" class="btn btn-primary bottom-buffer" data-toggle="modal"
				  data-target="#add-profession-dialog">Add Industry Profession
			</div>
		</div>
	</div>

	<!-- Show the Game Type conditions and multipliers -->
	<div class="row">
		<div class="col-xs-12">

			<!-- Profession table. -->
			<div class="row">
				<div class="col-xs-12">
					<h5>Professions</h5>
					<div id="no-conditions">
						<p>There are no avaiable profession for the industry. To add a new profession, use the "Add
							Profession" button above</p>
					</div>

					<div id="profession-table-div" class="top-buffer">
						<p>The Industry Conditions show the steps, from the start of the sales process to the ending
							Objective.</p>
						<table class="table table-striped" id="profession-table">
							<thead class="main-table">
								<tr>
									<th>Name</th>
									<th>Description</th>
									<th class="text-center"><span class="glyphicon glyphicon-duplicate"></span></th>
									<th class="text-center"><span class="glyphicon glyphicon-trash"></span></th>
								</tr>
							</thead>
							<tbody class="main-table">
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- Profession table -->
		</div>
	</div>

	<!-- Dialogs -->
<?php $this->partial('views/industry/dialogs/add_condition.php'); ?>
<?php $this->partial('views/industry/dialogs/delete_condition.php'); ?>

<?php $this->partial('views/industry/dialogs/add_multiplier.php'); ?>
<?php $this->partial('views/industry/dialogs/delete_multiplier.php'); ?>

<?php $this->partial('views/industry/dialogs/add_profession.php'); ?>
<?php $this->partial('views/industry/dialogs/copy_profession.php'); ?>
<?php $this->partial('views/industry/dialogs/delete_profession.php'); ?>
