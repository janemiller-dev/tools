<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/objective/dialog/add_objective.js"></script>

<div class="modal fade" id="add-objective-dialog" tabindex="-1" role="dialog" aria-labelledby="add-objective-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="add-objective-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="add-objective-title" class="modal-title">Add Objective</h2>
                </div>

                <div class="modal-body">

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="year">Objective Year</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <select class="form-control" id="add-objective-year" name="year">
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="floor">Objective Floor</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <input type="number" name="floor" id="add-objective-floor"/>
                            </div>
                            <p class="form-text text-muted">Select the floor.</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="target">Objective Target</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <input type="number" name="target" id="add-objective-target"/>
                            </div>
                            <p class="form-text text-muted">Select the target.</p>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="game">Objective Game</label>
                        <div class="col-xs-12 col-sm-9">
                            <div class="input-group">
                                <input type="number" name="game" id="add-objective-game"/>
                            </div>
                            <p class="form-text text-muted">Select the game.</p>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add Objective</button>
                </div>
            </form>
        </div>
    </div>
</div>
