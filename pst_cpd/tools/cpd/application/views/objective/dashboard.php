<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/objective/dashboard.js"></script>

<!-- Page Title -->
<div class="row">
    <div class="col-xs-12">
        <h2>Profession Objectives</h2>
    </div>
</div>

<!-- Show the add Objective button. -->
<div class="row">
    <div class="col-xs-12 text-left">
        <button type="button" id="add-objective" class="btn btn-primary bottom-buffer" data-toggle="modal"
                data-target="#add-objective-dialog" data-backdrop="static">Add New Objective
        </button>
    </div>
</div>

<!-- Show the list of my Profession Objectives. -->
<div class="row">
    <div class="col-xs-12">

        <!-- Objective table -->
        <div class="row" id="objective-row">
            <div class="col-xs-12">
                <p id="no-objective-instances">You do not have any Profession Objective Instance setup. To create one, use the Add New Objective Instance button.</p>
                <p>To set up objectives for quarters click on the respective year below.</p>
                <div id="objective-div" class="top-buffer">
                    <table class="table table-striped" id="objective-table">
                        <thead>
                        <tr>
                            <th>Year</th>
                            <th>Floor</th>
                            <th>Target</th>
                            <th>Game</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- // Objective table -->

    </div>
</div>

<!-- Dialogs -->
<?php $this->partial('views/objective/dialog/add_objective.php'); ?>