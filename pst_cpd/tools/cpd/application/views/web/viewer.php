<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="row text-center">
    <div class="col-xs-12">
        <div class="col-xs-2">
            <img src="https://cpd.powersellingtools.com/wp-content/uploads/2019/09/Extended-Logo-1.png"
                 style="width: 100%"/>
        </div>
        <div class="col-xs-8" style="padding: 10px">
            <h2 class="modal-title" style="margin-bottom: 2vh">PRESENTATION SCREEN VIEWER</h2>
        </div>
        <div class="col-xs-2" style="padding: 10px">
            <a class="btn btn-secondary viewer-full-screen"
               style="float: right; background-color: transparent !important;">
                <span class="glyphicon glyphicon-fullscreen" style="font-size: 3vh; color: #D8D8D8"></span></a>
        </div>
    </div>

    <hr/>

    <div class="col-xs-10 col-xs-offset-1">
        <video src="#" id="web-viewer-video"
               style="width: 100%; background: #000; border-radius: 5px" autoplay></video>
    </div>

    <div class="col-xs-10 col-xs-offset-1">
        <div class="col-xs-6" style="text-align: left;">
            <label>Enter Access Code</label>
            <input type="text" id="access-code" class="form-control" style="width: 20%; display: inline-block;"/>
            <button type="button" class="btn btn-primary" id="join-room">Join</button>
        </div>
        <div class="col-xs-6" style="text-align: right;">
            <button type="button" class="btn btn-primary" id="create-room">Share Screen</button>
            <button type="button" class="btn btn-primary" id="stop-share">Stop Screen Share</button>

        </div>
    </div>

    <div class="col-xs-12">
        <h4 id="access-link"></h4>
    </div>
</div>

<!-- Load the javascript support and CSS. -->
<script src="https://rtcmulticonnection.herokuapp.com/dist/RTCMultiConnection.min.js"></script>
<script src="https://rtcmulticonnection.herokuapp.com/socket.io/socket.io.js"></script>

<script src="<?php echo $this->basepath; ?>resources/app/js/web/viewer.js"></script>
