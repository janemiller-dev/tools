<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="row">
    <div class="col-xs-12">
        <!-- Back Button-->
        <div class="col-xs-2">
            <a class="btn btn-secondary back-button" style="float: right; padding-right: 25px">
                <img src="https://cpd.powersellingtools.com/wp-content/uploads/2020/01/Component-Arrow-Back.png"
                     style="height: 5vh; width: 70%;" alt="back"></a>
        </div>

        <!--// Back Button-->
        <div class="col-xs-8 text-center">
            <h2 class="modal-title" style="margin-bottom: 2vh">PRESENTATION SCREEN SHARE</h2>
        </div>

        <div class="col-xs-2">
            <a class="btn btn-secondary web-full-screen"
               style="float: right; background-color: transparent !important;">
                <span class="glyphicon glyphicon-fullscreen" style="font-size: 3vh; color: #D8D8D8"></span></a>
        </div>

    </div>

    <div class="col-xs-10 col-xs-offset-1" id="screen-share-div">
        <video src="#" id="web-video"
               style="width: 100%; background: #000; border-radius: 5px" autoplay></video>

        <iframe class="hidden" style="height: 80vh; width: 100%" frameborder='0' id="ppt-viewer"></iframe>
    </div>
<!--    <img src="#" id="screenshot-image"/>-->

    <div class="col-xs-10 col-xs-offset-1">
        <div style="text-align: left;" class="col-xs-6">
            <label>Enter Access Code</label>
            <input type="text" id="access-code" class="form-control" style="width: 20%; display: inline-block;"/>
            <button type="button" class="btn btn-primary" id="join-room">Join</button>
            <button type="button" class="btn btn-primary" data-toggle="modal"
                    data-target="#invite-viewers-dialog">Invite Viewers</button>
            <button type="button" class="btn btn-primary">Allow Viewer Share</button>
        </div>
        <div style="text-align: right;" class="col-xs-6">
            <button class="btn btn-primary" type="button" id="select-presentation" data-toggle="modal"
                    data-target="#view-presentation-list-dialog">Select Presentation</button>
            <button type="button" class="btn btn-primary" id="create-room">Share Screen</button>
            <button type="button" class="btn btn-primary" id="stop-share">Stop Screen Share</button>

        </div>
    </div>

    <div class="col-xs-12 text-center">
        <h4 id="access-link"></h4>
    </div>
</div>
<?php $this->partial('views/web/view_presentation_list.php'); ?>
<?php $this->partial('views/web/delete_presentation.php'); ?>
<?php $this->partial('views/web/invite_viewers.php'); ?>

<!-- Load the javascript support and CSS. -->
<script src="https://rtcmulticonnection.herokuapp.com/dist/RTCMultiConnection.min.js"></script>
<script src="https://rtcmulticonnection.herokuapp.com/socket.io/socket.io.js"></script>
<script src="//cdn.webrtc-experiment.com/screenshot.js"></script>

<script src="<?php echo $this->basepath; ?>resources/app/js/web/web.js"></script>

