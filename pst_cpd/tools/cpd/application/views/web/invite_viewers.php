<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="invite-viewers-dialog" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="send-viewers-label"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form id="send-viewers-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="send-viewers-title" class="modal-title">Invite Viewers</h2>
                </div>

                <div class="modal-body">

                    <div class="form-group" style="margin: 5px !important;">
                        <label class="col-xs-12 col-sm-3 control-label" for="viewers-client-email">To:</label>
                        <div class="col-xs-12 col-sm-9 input-group client-email">
                            <input type="email" class="form-control viewers-client-email">
                            <span class="input-group-btn">
                                <span class="has-tooltip" title="Add Recipient" data-placement="bottom">
                                    <button class="btn btn-default" type="button" id="add-recipient">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                </span>
                            </span>
                        </div>
                    </div>

                    <div class="form-group" style="margin: 5px !important;">
                        <label class="col-xs-12 col-sm-3 control-label" for="viewers-client-subject">Subject:</label>
                        <div class="col-xs-12 col-sm-9" style="padding: 0">
                            <input type="text" class="form-control" id="viewers-client-subject">
                        </div>
                    </div>

                    <div id="invite-email-content">
                    </div>
                </div>

                <input type="hidden" id="client-id" name="client_id"/>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="close-send-viewers-dialog" data-dismiss="modal">
                        Close
                    </button>
                    <button type="button" class="btn btn-primary" id="send-viewers-button">Send</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/web/invite_viewers.js"></script>
