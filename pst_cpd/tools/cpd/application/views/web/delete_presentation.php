<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<!-- Load the javascript support. -->
<!--<script src="--><?php //echo $this->basepath; ?><!--resources/app/js/tgd/dialog/delete_presentation.js"></script>-->

<div class="modal fade" id="delete-presentation-dialog" tabindex="-1" role="dialog" aria-labelledby="delete-presentation-label"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <form id="delete-presentation-form" class="form-horizontal">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h2 id="delete-presentation-title" class="modal-title">Delete Presentation?</h2>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-xs-12 col-sm-3 control-label" for="name">Name</label>
                        <div class="col-xs-12 col-sm-9">
                            <p class="form-control-static" id="delete-presentation-name"></p>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="button" id="delete-presentation-instance" class="btn btn-primary">Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>
