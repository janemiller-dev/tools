<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>

<div class="modal fade" id="view-presentation-list-dialog"
     tabindex="-1" role="dialog" aria-labelledby="dial-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 class="modal-title">Presentation Items List</h2>
            </div>

            <div class="modal-body">
                <table class="table" id="presentation-list-table">
                    <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">Presentation Name</th>
                        <th class="text-center">View</th>
                        <th class="text-center">Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            <div class="modal-footer">
                <input id="upload-ppt" type="file" name="files[]"
                       data-url= <?= "https://" . $_SERVER['SERVER_NAME'] . "/tools/web/upload_ppt" ?> style="visibility:
                       hidden; float:left; width:0;">
                <button class="btn btn-primary" id="add-new-presentation">Add New</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo $this->basepath; ?>resources/app/js/web/view_presentation_list.js"></script>
