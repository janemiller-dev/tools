<!DOCTYPE html>
<html lang="en">
<?php
ini_set('post_max_size', '64M');
ini_set('upload_max_filesize', '64M');
?>

<head>
    <title>
        <?php if (isset($page_title)) {
            echo $page_title;
        } else {
            echo "Advisory Selling Tools";
        } ?>
    </title>
    <link rel="icon" href="<?php echo $this->basepath; ?>resources/app/media/Icon-4.1.png">

    <meta charset="utf-8">


    <!--  Tiny MCE    -->
    <script src='https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=bq4ugrpdxuguwobk9o5um0rvpu8cpugxqt9fpxqjjsfj79n6'></script>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Let Javascript know the base URL. -->
    <script type="text/javascript">
        var jsglobals = {
            base_url: "<?php echo $this->basepath; ?>"
        }
    </script>

    <!-- JQuery -->
    <script src="<?php echo $this->basepath; ?>resources/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap. -->
    <script src="<?php echo $this->basepath; ?>resources/bootstrap/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/bootstrap/dist/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/bootstrap/dist/css/bootstrap-theme.min.css"/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"></script>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/css/bootstrap-select.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.2/js/bootstrap-select.min.js"></script>

    <!-- Font awesome and social bottons. -->
    <link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/font-awesome/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/bootstrap-social/bootstrap-social.css"/>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

    <!-- Auto Cue Prompter -->
    <script src="<?php echo $this->basepath; ?>resources/autocue/js/scrollocue.js"></script>

    <!--Teleprompter-->
    <script src="<?php echo $this->basepath; ?>resources/prompter/static/endcredits.js"></script>
    <link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/prompter/static/endcredits.css"/>

    <!--Form Cache-->
    <script src="<?php echo $this->basepath; ?>resources/form-cache/src/formcache.js"></script>

    <!-- x-editable. -->
    <link rel="stylesheet"
          href="<?php echo $this->basepath; ?>resources/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css"/>
    <script src="<?php echo $this->basepath; ?>resources/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

    <!--File Upload-->
    <script src="<?php echo $this->basepath; ?>resources/jQuery-File-Upload/js/vendor/jquery.ui.widget.js"></script>
    <script src="<?php echo $this->basepath; ?>resources/jQuery-File-Upload/js/jquery.iframe-transport.js"></script>
    <script src="<?php echo $this->basepath; ?>resources/jQuery-File-Upload/js/jquery.fileupload.js"></script>

    <!-- Moment. -->
    <script src="<?php echo $this->basepath; ?>resources/moment/moment.js"></script>

    <!-- Datatables and related plugins. -->
    <script src="<?php echo $this->basepath; ?>resources/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo $this->basepath; ?>resources/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo $this->basepath; ?>resources/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo $this->basepath; ?>resources/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <link rel="stylesheet"
          href="<?php echo $this->basepath; ?>resources/datatables.net-bs/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet"
          href="<?php echo $this->basepath; ?>resources/datatables.net-responsive-bs/css/responsive.bootstrap.min.css"/>

    <!--    Double Scroll-->
    <script src="<?php echo $this->basepath; ?>resources/jquery-doublescroll/jquery.doubleScroll.js"></script>

    <!-- Form validation. -->
    <link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/form-validation/formValidation.min.css"/>
    <script src="<?php echo $this->basepath; ?>resources/form-validation/formValidation.min.js"></script>
    <script src="<?php echo $this->basepath; ?>resources/form-validation/framework/bootstrap.min.js"></script>

    <script src="<?php echo $this->basepath; ?>resources/datetimepicker/build/jquery.datetimepicker.full.js"></script>
    <link rel="stylesheet"
          href="<?php echo $this->basepath; ?>resources/datetimepicker/build/jquery.datetimepicker.min.css"/>

    <!-- Java Script Toastr -->
    <link href="<?php echo $this->basepath; ?>resources/toastr/toastr.min.css" rel="stylesheet"/>
    <script src="<?php echo $this->basepath; ?>resources/toastr/toastr.min.js"></script>

    <!-- Number formatter (and required library). -->
    <script src="<?php echo $this->basepath; ?>resources/numeral/numeral.js"></script>

    <!-- Geocomplete -->
    <script src="<?php echo $this->basepath; ?>resources/geocomplete/jquery.geocomplete.min.js"></script>

    <!-- Google reCaptcha -->
    <script src="https://www.google.com/recaptcha/api.js"></script>

    <!-- Common styles. -->
    <link rel="stylesheet" href="<?php echo $this->basepath; ?>resources/app/css/style.css"/>

</head>

<body>

<!-- Main navigation -->
<!--<header class="navbar navbar-default" id="header-navbar">-->
<!--    <div class="container-fluid">-->
<!--        <div class="navbar-header">-->
<!--            <a class="navbar-brand" href="/">-->
<!--                <img src=--><? //= "https://" . $_SERVER['SERVER_NAME'] . "/wp-content/uploads/2019/02/000-AST-LOGO-1.png" ?>
<!---->
<!--                     <img src=--><? //= "https://" . $_SERVER['SERVER_NAME']
//                . "/wp-content/uploads/2019/02/000-AS-LOGO-2-BIGLESSLESS-LESS-TOP-BOTTOM-1.png" ?>
<!--                     alt="Advisory Selling Tools" height="80px">-->
<!--            </a>-->
<!--            <button class="navbar-toggle collapsed" type="button" data-target="#main-menu" data-toggle="collapse">-->
<!--                <span class="sr-only">Toggle navigation</span>-->
<!--                <span class="icon-bar"></span>-->
<!--                <span class="icon-bar"></span>-->
<!--                <span class="icon-bar"></span>-->
<!--            </button>-->
<!--        </div>-->
<!--        <form action="https://goblueswipe.transactiongateway.com/cart/cart.php" style="display: none;" method="POST">-->
<!--            <input type="hidden" name="plan_id" value="20415271">-->
<!--            <input type="hidden" name="key_id" value="11806842"/>-->
<!--            <input type="hidden" name="action" value="process_recurring"/>-->
<!--            <input type="hidden" name="url_finish" value="https://local-pst/tools/dashboard"/>-->
<!--            <input type="hidden" name="customer_receipt" value="true"/>-->
<!--            <input type="hidden" name="hash" value="action|5a0ff38aae423f19e393126e8f8855ac"/>-->
<!--            <input type="submit" name="submit" value="Sign up"/>-->
<!--        </form>-->
<!---->
<!-- Main Menu -->
<!--        <div class="collapse navbar-collapse" id="main-menu">-->
<!--            <ul class="nav navbar-nav navbar-right" id="main-nav-ul">-->
<!--                <li><a href="/">Home</a></li>-->
<!--                --><?php //if (isset($this->main_menu)) { ?>
<!--                    --><?php //foreach ($this->main_menu as $key => $item) { ?>
<!--                        --><?php //if (isset($item['children'])) { ?>
<!--                            <li class="dropdown">-->
<!--                                <a class="dropdown-toggle" href="#" id="main-dd---><?php //echo $key; ?><!--"-->
<!--                                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" role="button">-->
<!--                                    --><?php //echo $item['name']; ?>
<!--                                    <span class="caret"></span>-->
<!--                                </a>-->
<!--                                <ul class="dropdown-menu" aria-labelledby="main-dd--->
<?php //echo $key; ?><!--" role="menu">-->
<!--                                    --><?php //foreach ($item['children'] as $child) { ?>
<!--                                        <li>-->
<!--                                            <a href="--><?php //echo $this->basepath . $child['link']; ?><!--">-->
<!--                                                --><?php //echo $child['name']; ?>
<!--                                            </a>-->
<!--                                        </li>-->
<!--                                    --><?php //} ?>
<!--                                </ul>-->
<!--                            </li>-->
<!--                        --><?php //} else { ?>
<!--                            <li>-->
<!--                                <a href="--><?php //echo $this->basepath . $item['link']; ?><!--">-->
<!--                                    --><?php //echo $item['name']; ?>
<!--                                </a>-->
<!--                            </li>-->
<!--                        --><?php //} ?>
<!--                    --><?php //} ?>
<!--                --><?php //} ?>
<!--            </ul>-->
<!--        </div>-->
<!-- //Main Menu -->
<!---->
<!--    </div>-->
<!--</header>-->
<!-- //Main navigation -->

<!-- Tools menu -->
<?php if ((strpos($_SERVER['REQUEST_URI'], "tools/cpdv1/play_doc") === false) &&
    (strpos($_SERVER['REQUEST_URI'], "tools/survey") === false) &&
    (strpos($_SERVER['REQUEST_URI'], "tools/profile") === false) &&
    (strpos($_SERVER['REQUEST_URI'], "tools/cpdv1/reading_file") === false) &&
    (strpos($_SERVER['REQUEST_URI'], "tools/web/viewer") === false) &&
    (strpos($_SERVER['REQUEST_URI'], "tools/web/view_ppt") === false) &&
    (strpos($_SERVER['REQUEST_URI'], "tools/collab") === false) &&
    isset($this->tool_menu) && !empty($this->tool_menu)) { ?>
    <div class="container-fluid tool-menu" style="padding: 0px">
        <div class="navbar navbar-default">
            <div class="collapsed navbar-collapse" id="tool-menu-nav" style="padding-left: 0">
                <a class="nav navbar-nav" href="/">
                    <img src=<?= "https://cpd.powersellingtools.com/wp-content/uploads/2019/09/Extended-Logo-1.png" ?>
                         <img src=<?= "https://" . $_SERVER['SERVER_NAME']
                    . "/wp-content/uploads/2019/02/000-AS-LOGO-2-BIGLESSLESS-LESS-TOP-BOTTOM-1.png" ?>
                         alt="Advisory Selling Tools" height="49.5px" style="background:white; padding-right: 3px"
                    width="250vw">
                </a>
                <ul class="nav navbar-nav navbar-left" id="app-menu-list">
                    <!-- List Ends -->

                    <?php foreach ($this->tool_menu['groups'] as $group) { ?>
                        <?php if (isset($group->tools)) { ?>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		<span class="has-tooltip" data-toggle="tooltip" title="<?php echo $group->tg_name; ?> Tool Set"
              data-placement="bottom">
		  <?php echo $group->tg_name; ?>
		</span>
                                    <b class="caret"></b>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="<?php echo $this->basepath . 'homebase'; ?>" id="homebase-component">
                                        <span class="has-tooltip" data-toggle="tooltip"
                                              title="This tool contains information about every client.">HOMEBASE DATA CENTER</span>
                                        </a>
                                    </li>

                                    <?php foreach ($group->tools as $tool) { ?>
                                        <li>
                                            <a href="<?php echo $this->basepath . $tool->tool_controller; ?>">
                                                <?php echo $tool->tool_name; ?></a>
                                        </li>
                                    <?php } ?>
                                </ul>

                            </li>
                        <?php } ?>
                    <?php } ?>

                    <!--                    <li>-->
                    <?php $_SESSION['components'] = $this->tool_menu['components'] ?>

                    <!--                        <a href="#" class="component_button" onclick="return false;" data-toggle="popover"-->
                    <!--                           title="Communication Components"-->
                    <!--                           data-placement="bottom"-->
                    <!--                           data-html="true" data-content='-->
                    <!--                           <div class="row col-xs-12">-->
                    <!--                               <div class="col-xs-4">-->
                    <!--                                   <table style="width:100%" class="table table-striped">-->
                    <!--                                        <tr>-->
                    <!--                                            <thead>-->
                    <!--                                                <th>PROMO</th>-->
                    <!--                                                <th>SURVEY</th>-->
                    <!--                                                <th>DOCS</th>-->
                    <!--                                                <th>SKILLS</th>-->
                    <!--                                                <th>SAP</th>-->
                    <!--                                                <th>WEB</th>-->
                    <!--                                            </thead>-->
                    <!--                                        </tr>-->
                    <!--                                        <tbody>-->
                    <!--                                            <tr>-->
                    <!--                                               --><?php //foreach ($this->tool_menu['components'] as $component) { ?>
                    <!--                                               --><?php //?>
                    <!--                                                    <td class="text-center"><a class="btn btn-default btn-sm" id="-->
                    <?php //echo $component->uc_name; ?><!--"-->
                    <!--                                                     href="-->
                    <?php //echo $this->basepath . 'cpdv1' . $component->uc_controller ?><!--">-->
                    <!--                                                            <span class="glyphicon glyphicon--->
                    <?php //echo $component->uc_icon; ?><!--"></span></a></td>-->
                    <!--                                               --><?php //} ?>
                    <!--                                           </tr>-->
                    <!--                                        </tbody>-->
                    <!--                                   </table>-->
                    <!--                               </div>-->
                    <!--                           </div>'-->
                    <!--                        >Components</a>-->
                    <!--                        --><?php //?>
                    <!--                    </li>-->

                    <!--                                        <li>-->
                    <!--                                            <a id="archive-tool" href="#" data-toggle="modal" data-target="#view-promo-archive-dialog">-->
                    <!--                                                <span class="has-tooltip" data-toggle="tooltip" data-placement="bottom"-->
                    <!--                                                      title="Lists Archive Components.">Archive</span>-->
                    <!--                                            </a>-->
                    <!--                                        </li>-->


                    <!--                    <li class="dropdown">-->
                    <!--                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">-->
                    <!--		<span class="has-tooltip" data-toggle="tooltip" title="-->
                    <?php //echo $group->tg_name; ?><!-- Tool Set"-->
                    <!--              data-placement="bottom">-->
                    <!--		  --><?php //echo $group->tg_name; ?>
                    <!--		</span>-->
                    <!--                            <b class="caret"></b>-->
                    <!--                        </a>-->
                    <!--                        <ul class="dropdown-menu">-->
                    <!--                            --><?php //foreach ($group->tools as $tool) { ?>
                    <!--                                <li>-->
                    <!--                                    <a href="-->
                    <?php //echo $this->basepath . $tool->tool_controller; ?><!--">-->
                    <!--                                        --><?php //echo $tool->tool_name; ?><!--</a>-->
                    <!--                                </li>-->
                    <!--                            --><?php //} ?>
                    <!---->
                    <!--                        </ul>-->

                    <!--                    <li class="dropdown">-->
                    <!--                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">-->
                    <!--		                <span class="has-tooltip" data-toggle="tooltip" title="Get Started Tools"-->
                    <!--                              data-placement="bottom">-->
                    <!--                            Get Started</span><b class="caret"></b></a>-->
                    <!---->
                    <!--                        <ul class="dropdown-menu" id="welcome-menu">-->
                    <!--                            <li>-->
                    <!--                                <a href="-->
                    <?php //echo $this->basepath; ?><!--dashboard/getting_started"-->
                    <!--                                   id="user-get-started-button" data-toggle="modal">-->
                    <!--                                                <span class="has-tooltip" data-toggle="tooltip"-->
                    <!--                                                      title="Getting Started Instructions" data-placement="bottom">-->
                    <!--                                                    Get Started</span>-->
                    <!--                                </a>-->
                    <!--                            </li>-->
                    <!--                            <li>-->
                    <!--                                <a href="#" id="user-profile-button" data-toggle="modal"-->
                    <!--                                   data-target="#user-profile-dialog">-->
                    <!--                                                <span class="has-tooltip" data-toggle="tooltip"-->
                    <!--                                                      title="User Profile." data-placement="bottom">User Profile</span>-->
                    <!--                                </a>-->
                    <!--                            </li>-->
                    <!--                            <li>-->
                    <!--                                <a href="-->
                    <?php //echo $this->basepath; ?><!--dashboard/welcome" id="user-profile-button">-->
                    <!--                                                <span class="has-tooltip" data-toggle="tooltip"-->
                    <!--                                                      title="User Welcome Page" data-placement="bottom">Welcome</span>-->
                    <!--                                </a>-->
                    <!--                            </li>-->
                    <!---->
                    <!--                        </ul>-->
                    <!--                    </li>-->


                </ul>

                <!-- Side Panel-->
                <div id="tools-side-nav" class="sidenav" style="float: right;">


                    <ul class="nav navbar-nav navbar-right" id="email-menu">
                        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

                        <!-- SGD Drop down-->
                        <li>
                            <a href="#" id="sgd" data-toggle="modal" data-target="#view-sgd-dialog">
                            <span class="has-tooltip" data-toggle="tooltip"
                                  title="Strategic Game Designer.">GAME DESIGN</span>
                            </a>
                        </li>
                        <!--// SGD Drop down-->

                        <!-- TIME component -->
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                <span class="has-tooltip" data-toggle="tooltip"
                              title="Time Navigation">
                            Time Navigator</span></a>
                        </li>
                        <!--// TIME component -->

                        <!-- DCB component -->
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                <span class="has-tooltip" data-toggle="tooltip"
                              title="DEAL CONVERSION BOARD ">
                            DEAL BOARD</span></a>
                        </li>
                        <!--// DCB component -->

                        <!-- JOURNAL component -->
                        <li style="padding-top: 20px">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                <span class="has-tooltip" data-toggle="tooltip"
                              title="JOURNAL ">
                            JOURNAL</span></a>
                        </li>
                        <!--// JOURNAL component -->

                        <!-- DICTIONARY component -->
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                <span class="has-tooltip" data-toggle="tooltip"
                              title="DICTIONARY ">
                            DICTIONARY</span></a>
                        </li>
                        <!--// DICTIONARY component -->

                        <!-- ARCHIVE component -->
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                <span class="has-tooltip" data-toggle="tooltip"
                              title="ARCHIVE ">
                            ARCHIVE</span></a>
                        </li>
                        <!--// ARCHIVE component -->

                        <!-- TOOLTIPS component -->
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                <span class="has-tooltip" data-toggle="tooltip"
                              title="TOOLTIPS ">
                            TOOLTIPS</span></a>
                        </li>
                        <!--// TOOLTIPS component -->

                        <!-- TUTORIALS component -->
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                <span class="has-tooltip" data-toggle="tooltip"
                              title="TUTORIALS ">
                            TUTORIALS</span></a>
                        </li>
                        <!--// TUTORIALS component -->

                        <!-- COMMUNICATIONS component -->
                        <li style="padding-top: 20px">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                <span class="has-tooltip" data-toggle="tooltip"
                              title="COMMUNICATIONS ">
                            COMMUNICATIONS</span></a>
                        </li>
                        <!--// COMMUNICATIONS component -->

                        <!-- MARKETING component -->
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                <span class="has-tooltip" data-toggle="tooltip"
                              title="MARKETING ">
                            MARKETING</span></a>
                        </li>
                        <!--// MARKETING component -->

                        <!-- TEAM TOOLS component -->
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                <span class="has-tooltip" data-toggle="tooltip"
                              title="TEAM MANAGEMENT">
                            TEAM MANAGEMENT</span></a>
                        </li>
                        <!--// TEAM TOOLS component -->
                        <!--GRANT ACCESS component -->
                        <li style="padding-top: 20px">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                <span class="has-tooltip" data-toggle="tooltip"
                              title="GRANT ACCESS ">
                            GRANT ACCESS</span></a>
                        </li>
                        <!--// GRANT ACCESS component -->

                        <!-- WEB component -->
                        <!--                        <li>-->
                        <!--                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">-->
                        <!--		                <span class="has-tooltip" data-toggle="tooltip"-->
                        <!--                              title="Web Presentations">-->
                        <!--                            WEB Presentations</span><b class="caret"></b></a>-->
                        <!---->
                        <!--                            <ul class="dropdown-menu" id="web-menu">-->
                        <!--                                <li>-->
                        <!--                                    <a href="#">-->
                        <!--                            <span class="has-tooltip" data-toggle="tooltip"-->
                        <!--                                  title="Initial Contact Presentation">Contact</span>-->
                        <!--                                    </a>-->
                        <!--                                </li>-->
                        <!--                                <li>-->
                        <!--                                    <a href="#">-->
                        <!--                            <span class="has-tooltip" data-toggle="tooltip"-->
                        <!--                                  title="Exploratory Meeting Presentation">Meeting</span>-->
                        <!--                                    </a>-->
                        <!--                                </li>-->
                        <!--                                <li>-->
                        <!--                                    <a href="#">-->
                        <!--                            <span class="has-tooltip" data-toggle="tooltip"-->
                        <!--                                  title="Analysis Presentation">Analysis</span>-->
                        <!--                                    </a>-->
                        <!--                                </li>-->
                        <!--                                <li>-->
                        <!--                                    <a href="#">-->
                        <!--                            <span class="has-tooltip" data-toggle="tooltip"-->
                        <!--                                  title="Proposal Presentation">Proposal</span>-->
                        <!--                                    </a>-->
                        <!--                                </li>-->
                        <!--                                <li>-->
                        <!--                                    <a href="#">-->
                        <!--                            <span class="has-tooltip" data-toggle="tooltip"-->
                        <!--                                  title="Contract Negotiation Presentation">Negotiation</span>-->
                        <!--                                    </a>-->
                        <!--                                </li>-->
                        <!--                            </ul>-->
                        <!---->
                        <!--                        </li>-->
                        <!--// WEB component -->


                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
		                <span class="has-tooltip" data-toggle="tooltip"
                              title="Send Email for Comments, Questions or Feedback">
                            CONTACT SUPPORT</span><b class="caret"></b></a>

                            <ul class="dropdown-menu" id="support-menu">
                                <li>
                                    <a href="#" id="email-support-button" data-toggle="modal"
                                       data-target="#email-support-dialog">
                            <span class="has-tooltip" data-toggle="tooltip"
                                  title="Send Email for Comments, Questions or Feedback">Support</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" id="user-outlook-button" data-toggle="modal"
                                       data-target="#outlook-email-client-dialog">
                                                <span class="has-tooltip" data-toggle="tooltip"
                                                      title="User Outlook Account."
                                                      data-placement="bottom">Outlook</span>
                                    </a>
                                </li>
                            </ul>

                        </li>
                    </ul>

                </div>
                <span style="font-size:30px;cursor:pointer; float: right; color: #fff"
                      onclick="openNav()">&#9776;</span>
                <!--// Side Panel-->

                <!-- Right Nav Bar-->
                <ul class="nav navbar-nav navbar-right" style="margin-right: 20px">

                    <!-- Welcome Drop Down-->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    		<span class="has-tooltip" data-toggle="tooltip" title="User Account Components"
                                  data-placement="bottom">
                    		  Account
                    		</span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo $this->basepath; ?>dashboard/welcome" id="user-profile-button">
                                                <span class="has-tooltip" data-toggle="tooltip"
                                                      title="User Welcome Page" data-placement="bottom">Welcome</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $this->basepath; ?>dashboard/getting_started"
                                   id="user-get-started-button" data-toggle="modal">
                                                <span class="has-tooltip" data-toggle="tooltip"
                                                      title="Getting Started Instructions" data-placement="bottom">
                                                    Get Started</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" id="user-profile-button" data-toggle="modal"
                                   data-target="#user-profile-dialog">
                                                <span class="has-tooltip" data-toggle="tooltip"
                                                      title="User Profile." data-placement="bottom">User Profile</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" data-toggle="modal" data-target="#view-agent-profile-dialog">Agent
                                    Profile</a>
                            </li>
                            <li>
                                <a href="#">Team Profile</a>
                            </li>
                            <li><a id="quick-email" href="#" data-toggle="modal"
                                   data-target="#outlook-email-client-dialog">
                                    Outlook</a></li>
                        </ul>
                    </li>
                    <!--// Welcome Drop Down-->

                    <!-- Quick Tools Drop Down-->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    		<span class="has-tooltip" data-toggle="tooltip" title="Quick Tool Set"
                                  data-placement="bottom">
                    		  Quick Tools
                    		</span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="#" id="quick-project" data-toggle="modal"
                                   data-target="#view-quick-project-dialog">Quick Projects</a></li>
                            <li><a href="#" id="quick-contact" data-toggle="modal"
                                   data-target="#view-quick-contact-dialog">Quick Contacts</a></li>
                            <li><a href="#" id="quick-action" data-toggle="modal"
                                   data-target="#view-quick-action-dialog">Quick Actions</a></li>
                            <li><a id="acm-reminder" href="#" data-toggle="modal"
                                   data-target="#view-reminder-dialog">
                                    Quick Reminders</a></li>
                            <li><a id="quick-email" href="#" data-toggle="modal"
                                   data-target="#outlook-email-client-dialog">
                                    Quick Email</a></li>
                            <li style="color: #000; padding: 20px; text-decoration: underline;"><b>Daily Tools..</b>
                            </li>
                            <li><a href="#" id="quick-project" data-toggle="modal"
                                   data-target="#view-quick-project-dialog">Daily Preparation</a></li>
                            <li><a href="#" id="quick-contact" data-toggle="modal"
                                   data-target="#view-quick-contact-dialog">Daily Tasks</a></li>
                            <li><a href="#" id="quick-action" data-toggle="modal"
                                   data-target="#view-quick-action-dialog">Daily Schedule</a></li>
                            <li><a id="acm-reminder" href="#" data-toggle="modal" data-target="#view-reminder-dialog">
                                    Daily Details</a></li>
                            <li><a id="quick-email" href="#" data-toggle="modal"
                                   data-target="#outlook-email-client-dialog">
                                    Daily Completion</a></li>
                            <!--                                                <li id="quick-calculator">-->
                            <!--                                                    <a href="#">Quick Calculator</a>-->
                            <!--                                                </li>-->
                            <!--                                                <li>-->
                            <!--                                                    <a href="#">Quick Calendar</a>-->
                            <!--                                                </li>-->
                        </ul>

                    </li>
                    <!--// Quick Tools Drop Down-->

                    <!--Web Presentation-->
                    <li>
                        <a href="/tools/web" class="has-tooltip" title="Web Presentation">Web</a>
                    </li>
                    <!--// Web Presentation-->
                    <!-- Daily Tools Drop Down-->
                    <!--                    <li class="dropdown">-->
                    <!--                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">-->
                    <!--                    		<span class="has-tooltip" data-toggle="tooltip" title="Daily Tool Set"-->
                    <!--                                  data-placement="bottom">-->
                    <!--                    		  Daily Tools-->
                    <!--                    		</span>-->
                    <!--                            <b class="caret"></b>-->
                    <!--                        </a>-->
                    <!--                        <ul class="dropdown-menu">-->
                    <!--                            <li><a href="#" id="quick-project" data-toggle="modal"-->
                    <!--                                   data-target="#view-quick-project-dialog">Daily Preparation</a></li>-->
                    <!--                            <li><a href="#" id="quick-contact" data-toggle="modal"-->
                    <!--                                   data-target="#view-quick-contact-dialog">Daily Tasks</a></li>-->
                    <!--                            <li><a href="#" id="quick-action" data-toggle="modal"-->
                    <!--                                   data-target="#view-quick-action-dialog">Daily Schedule</a></li>-->
                    <!--                            <li><a id="acm-reminder" href="#" data-toggle="modal" data-target="#view-reminder-dialog">-->
                    <!--                                    Daily Details</a></li>-->
                    <!--                            <li><a id="quick-email" href="#" data-toggle="modal"-->
                    <!--                                   data-target="#outlook-email-client-dialog">-->
                    <!--                                    Daily Completion</a></li>-->
                    <!---->
                    <!--                        </ul>-->
                    <!--                    </li>-->
                    <!--// Daily Tools Drop Down-->

                    <li>
                        <a href="<?php echo $this->basepath; ?>../?action=logout">
                                                <span class="has-tooltip" data-toggle="tooltip"
                                                      title="Log Out.">Log Out</span>
                        </a>
                    </li>
                </ul>
                <!--// Right Nav Bar-->
            </div>
        </div>
    </div>
    <div id="calculator"
         style="display: none; float: right; height: 0; margin-right: 2%; z-index: 10; position: relative">
        <form name="calc">
            <table border=4>
                <tr>
                    <td>
                        <input type="text" name="input" size="16">
                        <br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="button" name="one" value="  1  " onclick="calc.input.value += '1'">
                        <input type="button" name="two" value="  2  " onclick="calc.input.value += '2'">
                        <input type="button" name="three" value="  3  " onclick="calc.input.value += '3'">
                        <input type="button" name="plus" value="  +  " onclick="calc.input.value += ' + '">
                        <br>
                        <input type="button" name="four" value="  4  " onclick="calc.input.value += '4'">
                        <input type="button" name="five" value="  5  " onclick="calc.input.value += '5'">
                        <input type="button" name="six" value="  6  " onclick="calc.input.value += '6'">
                        <input type="button" name="minus" value="  -  " onclick="calc.input.value += ' - '">
                        <br>
                        <input type="button" name="seven" value="  7  " onclick="calc.input.value += '7'">
                        <input type="button" name="eight" value="  8  " onclick="calc.input.value += '8'">
                        <input type="button" name="nine" value="  9  " onclick="calc.input.value += '9'">
                        <input type="button" name="times" value="  x  " onclick="calc.input.value += ' * '">
                        <br>
                        <input type="button" name="clear" value="  c  " onclick="calc.input.value = ''">
                        <input type="button" name="zero" value="  0  " onclick="calc.input.value += '0'">
                        <input type="button" name="doit" value="  =  "
                               onclick="calc.input.value = eval(calc.input.value)">
                        <input type="button" name="div" value="  /  " onclick="calc.input.value += ' / '">
                        <br>
                    </td>
                </tr>
            </table>
        </form>
    </div>
<?php } ?>
<!-- // Tools menu -->
<!-- Flash Messages -->
<?php $flash = new \Plasticbrain\FlashMessages\FlashMessages();
if ($flash->hasMessages()) { ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <?php $flash->display(); ?>
            </div>
        </div>
    </div>
<?php } ?>

<!-- Dummy Content for income popover-->
<div style='width:0px;' class="dummy_content hidden">
    <div>
        <span><b> Price </b></span>
        <input type='text' class='form-control amount_detail price' style='width: 120px; display: inline-block'
               data-name='hca_price'>
        <span><b> Comm </b></span>
        <input type='text' class='form-control amount_detail rate' style='width: 60px; display: inline-block'
               data-name='hca_rate'>
        <span><b> Gross </b></span>
        <input type='text' class='form-control amount_detail gross' style='width: 120px;
         display: inline-block ' readonly>
        <span><b> Split </b></span>
        <input type='text' placeholder='GSplit' class='form-control amount_detail gsplit'
               style='width: 60px; display: inline-block; margin-right: 5px ' data-name='hca_gsplit '>
        <span><b> GNet </b></span>
        <input type='text ' placeholder='GNet ' class='form-control amount_detail gnet'
               style='width: 100px; display: inline-block'
               readonly>
    </div>
    <div style='margin-top: 5px' class='text-center'>
        <span><b> CSplit </b></span>
        <input type='text' class='form-control amount_detail csplit' style='width: 60px; display: inline-block'
               data-name='hca_split'>
        <span><b> Net </b></span>
        <input type='text' class='form-control amount_detail net' style='width: 120px; display: inline-block'
               data-name='hca_net'
               readonly>
    </div>
</div>
<!-- // Dummy Content for income popover-->

<!-- Dummy Content for value popover-->
<div style='width:0px;' class="dummy_value_content hidden">
    <table style="border-collapse: separate; border-spacing: 10px;">
        <tr>
            <td><b> Total Price </b></td>
            <td><input type='text' class='form-control value_detail price' style='width: 120px; display: inline-block'
                       data-name='hca_price'></td>
            <td><b> &divide; Usable Building SF </b></td>
            <td><input type='text' class='form-control value_detail usable_sf'
                       style='width: 120px; display: inline-block'
                       data-name='hca_usable_sf'></td>
            <td><b> = Livable SF Price </b></td>
            <td><input type='text' class='form-control value_detail livable_sf_price' style='width: 120px;
         display: inline-block'></td>
        </tr>
        <tr style='margin-top: 5px' class='text-center'>
            <td><b> Total Price </b></td>
            <td><input type='text' class='form-control value_detail price' style='width: 120px; display: inline-block'
                       data-name='hca_price'></td>
            <td><b> &divide; Gross Building SF </b></td>
            <td><input type='text' class='form-control value_detail gross_sf'
                       style='width: 120px; display: inline-block'
                       data-name='hca_gross_sf'></td>
            <td><b> = Gross SF Price </b></td>
            <td><input type='text' class='form-control value_detail gross_sf_price' style='width: 120px;
         display: inline-block'></td>
        </tr>
    </table>
</div>
<!-- // Dummy Content for value popover-->

<!-- Dummy Content for Revenue popover-->
<div style='width:0px;' class="dummy_revenue_content hidden">
    <table style="border-collapse: separate; border-spacing: 10px;">
        <tr>
            <td rowspan="2"><b> Number of Units Type A </b></td>
            <td><input type='text' class='form-control revenue_detail number' style='width: 120px; display: inline-block'
                       data-name='har_number' data-type="a"></td>
            <td><b> X Sq. Ft of Unit Type A </b></td>
            <td><input type='text' class='form-control revenue_detail sq_ft'
                       style='width: 120px; display: inline-block'
                       data-name='har_sq_ft'  data-type="a"></td>
            <td><b> = Total Sq Ft of Unit Type A </b></td>
            <td><input type='text' class='form-control revenue_detail total_sq_ft' style='width: 120px;
         display: inline-block' data-type="a" data-name="har_total_sq_ft"></td>
        </tr>
        <tr style='margin-top: 5px' class='text-center'>
            <td><input type='text' class='form-control revenue_detail number' style='width: 120px; display: inline-block'
                       data-name='har_number' data-type="a"></td>
            <td><b> X Unit Type A Rent/Mo </b></td>
            <td><input type='text' class='form-control revenue_detail rent_per_mo'
                       style='width: 120px; display: inline-block'
                       data-name='har_rent_per_mo' data-type="a"></td>
            <td><b> = Total Unit Type A Income/Mo </b></td>
            <td><input type='text' class='form-control revenue_detail total_rent_per_mo' style='width: 120px;
         display: inline-block' data-type="a" data-name="har_total_rent_per_mo"></td>
        </tr>
        <tr>
            <td rowspan="2"><b> Number of Units Type B </b></td>
            <td><input type='text' class='form-control revenue_detail number' style='width: 120px; display: inline-block'
                       data-name='har_number' data-type="b"></td>
            <td><b> X Sq. Ft of Unit Type B </b></td>
            <td><input type='text' class='form-control revenue_detail sq_ft'
                       style='width: 120px; display: inline-block'
                       data-name='har_sq_ft' data-type="b"></td>
            <td><b> = Total Sq Ft of Unit Type B </b></td>
            <td><input type='text' class='form-control revenue_detail total_sq_ft' style='width: 120px;
         display: inline-block' data-type="b" data-name="har_total_sq_ft"></td>
        </tr>
        <tr style='margin-top: 5px' class='text-center'>
            <td><input type='text' class='form-control revenue_detail number' style='width: 120px; display: inline-block'
                       data-name='har_number' data-type="b"></td>
            <td><b> X Unit Type B Rent/Mo </b></td>
            <td><input type='text' class='form-control revenue_detail rent_per_mo'
                       style='width: 120px; display: inline-block'
                       data-name='har_rent_per_mo' data-type="b"></td>
            <td><b> = Total Unit Type B Income/Mo </b></td>
            <td><input type='text' class='form-control revenue_detail total_rent_per_mo' style='width: 120px;
         display: inline-block' data-type="b" data-name="har_total_rent_per_mo"></td>
        </tr>
    </table>
    <div style="text-align: right">
        <div style="margin: 10px">
            <label>Total Number of Units</label>
            <input type='text' class='form-control total_number_units' style='width: 120px;
         display: inline-block'>
        </div>
        <div style="margin: 10px">
            <label>Total Livable Building SF</label>
            <input type='text' class='form-control total_livable_sf' style='width: 120px;
         display: inline-block'>
        </div>
        <div style="margin: 10px">
            <label>Total Gross Livable & Non-Livable Square Footage</label>
            <input type='text' class='form-control total_gross_sf' style='width: 120px;
         display: inline-block'>
        </div>
        <div style="margin: 10px">
            <label>Monthly Total Gross Potential Rental Income</label>
            <input type='text' class='form-control revenue_detail monthly_total_gross_income' style='width: 120px;
         display: inline-block'>
        </div>
        <div style="margin: 10px">
            <label>Yearly Total Gross Potential Rental Income</label>
            <input type='text' class='form-control revenue_detail yearly_total_gross_income' style='width: 120px;
         display: inline-block'>
        </div>
    </div>
</div>
<!-- // Dummy Content for Revenue popover-->

<!-- Application -->
<div class="container-fluid">
    <?php define('SUBVIEW', 1);
    echo $this->yieldView(); ?>
</div>
<!-- //Application -->

<!-- Footer -->
<div class="container-fluid" id="footer">
    <div class="row">
        <div class="col-xs-12" id="copyright_text">
            <hr/>
            <div class="col-xs-2"></div>
            <div class="col-xs-8">
                <p class="text-center">Copyright &copy; <?php echo date("Y"); ?> RothMethods, Inc. All rights
                    reserved</p>
            </div>
        </div>
    </div>
</div>
<!-- //Footer -->

<!-- Import View Image-->
<div id="view_import" class="hidden"><a href="#" class="pull-left"><img
                src="https://cpd.powersellingtools.com/wp-content/uploads/2020/04/import-form.png"
                style="height: 50vh; width: 90vw;"></a>
</div>

<!-- Import Buyer View Image-->
<div id="view_buyer_import" class="hidden"><a href="#" class="pull-left"><img
                src="https://cpd.powersellingtools.com/wp-content/uploads/2020/04/buyer_import.png"
                style="height: 350px; width: 800px;"></a>
</div>

</html>

<!-- Common functions. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/common.js"></script>
<?php if ((strpos($_SERVER['REQUEST_URI'], "tools/survey") === false) &&
    (strpos($_SERVER['REQUEST_URI'], "tools/profile") === false) &&
    (strpos($_SERVER['REQUEST_URI'], "tools/cpdv1/play_doc") === false) &&
    (strpos($_SERVER['REQUEST_URI'], "tools/cpdv1/reading_file") === false) &&
    (strpos($_SERVER['REQUEST_URI'], "tools/web/view_ppt") === false) &&
    (strpos($_SERVER['REQUEST_URI'], "tools/web/viewer") === false) &&
    (strpos($_SERVER['REQUEST_URI'], "tools/collab") === false)) { ?>
    <script src="<?php echo $this->basepath; ?>resources/app/js/outlook_common.js"></script>

    <script>


        // Set the initial list of products
        ((window.location.pathname === '/tools/cpdv1') ||
            (window.location.pathname === '/tools/tsl') ||
            (window.location.pathname === '/tools/tgd')) ?
            refreshProducts(refreshPage) : refreshProducts();
    </script>

    <!-- Dialogs -->
    <?php $this->partial('views/dialogs/email_support.php'); ?>
    <?php $this->partial('views/dialogs/user_profile.php'); ?>
    <?php $this->partial('views/industry/dialogs/add_product.php'); ?>
    <?php $this->partial('views/dialogs/view_sent.php'); ?>

<?php } ?>
<?php $this->partial('views/homebase/dialogs/add_client.php'); ?>
<?php $this->partial('views/homebase/dialogs/owner_profile.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_promo_archive_dialog.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_promo_archive_instance.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_target_audience.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_tsl_criteria.php'); ?>
<?php $this->partial('views/collab/dialogs/add_collab_details.php'); ?>
<?php $this->partial('views/tsl/dialogs/delete_archive.php'); ?>
<?php $this->partial('views/collab/dialogs/delete_collab.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_action.php'); ?>
<?php $this->partial('views/dialogs/view_alert.php'); ?>
<?php $this->partial('views/dialogs/delete_alert.php'); ?>
<?php $this->partial('views/dialogs/view_quick_project.php'); ?>
<?php $this->partial('views/dialogs/view_quick_contact.php'); ?>
<?php $this->partial('views/dialogs/view_quick_action.php'); ?>
<?php $this->partial('views/tsl/dialogs/read_promo.php'); ?>
<?php $this->partial('views/cpdv1/dialogs/view_lead.php'); ?>
<?php $this->partial('views/cpdv1/dialogs/view_source.php'); ?>
<?php $this->partial('views/dialogs/view_asset_info.php'); ?>
<?php $this->partial('views/homebase/dialogs/view_asset_info.php'); ?>
<?php $this->partial('views/cpdv1/dialogs/view_collab.php'); ?>
<?php $this->partial('views/cpdv1/dialogs/send_univ_collab.php'); ?>
<?php $this->partial('views/collab/dialogs/send_collab.php'); ?>
<?php $this->partial('views/dialogs/reminder.php'); ?>
<?php $this->partial('views/dialogs/view_agent_profile_dialog.php'); ?>
<?php $this->partial('views/tsl/dialogs/view_assignee.php'); ?>
<?php $this->partial('views/dialogs/outlook_email_client.php'); ?>
<?php $this->partial('views/dialogs/view_sgd_dialog.php'); ?>



