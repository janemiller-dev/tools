<?php if (!defined('SUBVIEW')) {
    exit('No direct script access allowed');
} ?>
<div id="cap" class="container-fluid">
    <img src="https://cpd.powersellingtools.com/wp-content/uploads/2019/09/Extended-Logo-1.png" style="width: 20vw;">
    <img src="https://cpd.powersellingtools.com/wp-content/uploads/2020/03/AST-Home-Slider.jpg" width="80%"  style="margin:20px auto 0 auto; display: block;">
    <p style="font-size:2.5em; color: #5B8D76;">A New World of Innovation in Selling!</p>
    <p style="font-size: 1em;">Go to Welcome To Get Started.</p>
    <br/>
    <p>*Our application currently supports Chrome, Firefox, MS edge.
        This application does not function well with Internet Explorer.</p>
    <span><a href="mailto:support@advisorysellingtools.com"
             style="font-size: 1em">support@advisorysellingtools.com</a></span>
</div>