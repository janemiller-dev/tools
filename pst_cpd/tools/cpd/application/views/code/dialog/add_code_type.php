<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Include the supporting javascript -->
<script src="<?php echo $this->basepath; ?>resources/app/js/code/dialog/add_code_type.js"></script>

<!-- Dialog to add a new code type. -->
<div class="modal fade" id="add-code-type-dialog" tabindex="-1" role="dialog" aria-labelledby="add-code-type-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="add-code-type-form" class="form-horizontal">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h2 id="ac-title" class="modal-title">Add a New Code Type</h2>
					<p>Once you enter the node code type, you can use the code type list to add codes.</p>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label class="col-xs-3 control-label" for="address">Name</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="act-name" name="name"
							       placeholder="Code Type Name"/>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-3 control-label" for="address">Abbreviation</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="act-abbrev" name="abbrev"
							       placeholder="Code Type Abbreviation"/>
							<small class="text-muted">The abbreviation must be unique.</small>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-3 control-label" for="address">Description</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="act-description" name="description"
							       placeholder="Code Type Description"/>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>

			</form>
		</div>
	</div>
</div>
<!-- // Add code type dialog -->
