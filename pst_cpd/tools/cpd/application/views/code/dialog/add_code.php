<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Include the supporting javascript -->
<script src="<?php echo $this->basepath; ?>resources/app/js/code/dialog/add_code.js"></script>

<!-- Dialog to add a new code type. -->
<div class="modal fade" id="add-code-dialog" tabindex="-1" role="dialog" aria-labelledby="add-code-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="add-code-form" class="form-horizontal">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h2 id="ac-title" class="modal-title">Add a New Code</h2>
					<h3 id="ac-subtitle" class="modal-title"></h3>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label class="col-xs-3 control-label" for="name">Name</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="ac-name" name="name" placeholder="Code Name"/>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-3 control-label" for="abbreviation">Abbreviation</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="ac-abbrev" name="abbrev"
							       placeholder="Code Abbreviation"/>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-3 control-label" for="description">Description</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="ac-description" name="description"
							       placeholder="Code Type Description"/>
						</div>
					</div>

				</div>

				<input type="hidden" id="ac-ct-id" name="ct_id" value=""/>

				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>

			</form>
		</div>
	</div>
</div>
<!-- // Add code dialog -->
