<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Include the supporting javascript -->
<script src="/resources/app/js/code/dialog/delete_code.js"></script>

<!-- Dialog to delete a new code type. -->
<div class="modal fade" id="delete-code-dialog" tabindex="-1" role="dialog" aria-labelledby="delete-code-label"
     aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<form id="delete-code-form" class="form-horizontal">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h2 id="dc-title" class="modal-title">Delete a Code</h2>
				</div>

				<div class="modal-body">

					<div class="form-group">
						<label class="col-xs-3 control-label" for="name">Name</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="dc-name" name="name" readonly="readonly"/>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-3 control-label" for="abbrev">Abbreviation</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="dc-abbrev" name="abbrev" readonly="readonly"/>
						</div>
					</div>

					<div class="form-group">
						<label class="col-xs-3 control-label" for="description">Description</label>
						<div class="col-xs-9">
							<input type="text" class="form-control" id="dc-description" name="description"
							       readonly="readonly"/>
						</div>
					</div>

				</div>

				<input type="hidden" id="dc-code-id" name="code_id" value=""/>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Delete</button>
				</div>

			</form>
		</div>
	</div>
</div>
<!-- // Delete code dialog -->
