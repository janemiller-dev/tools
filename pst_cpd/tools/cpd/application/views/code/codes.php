<?php if (!defined('SUBVIEW')) { exit('No direct script access allowed'); }?>

<!-- Load the javascript support. -->
<script src="<?php echo $this->basepath; ?>resources/app/js/code/codes.js"></script>

<div class="row">
	<div class="col-xs-12">
		<h3>Code Types</h3>
	</div>
</div>

<div class="row">
	<div class="col-xs-12">

		<!-- Add add code type button -->
		<div class="row" id="add-code-type-row">
			<div class="col-xs-12">
				<button class="btn btn-primary" data-toggle="modal" data-target="#add-code-type-dialog"
				        data-operation="insert">Add Code Type
				</button>
			</div>
		</div>
		<!-- // Add code type button -->

		<!-- Code type list row -->
		<div class="row" id="code-type-list-row">
			<div class="col-xs-12">
				<div id="no-code-types">
					<p>There are no code types. Use the Add Code Type button to add a code type.</p>
				</div>

				<!-- Code typetable. -->
				<div id="code-type-table-div" class="top-buffer">
					<div class="table-responsive">
						<table class="table table-striped" id="code-type-table">
							<thead>
							<tr>
								<th class="text-center">Name</th>
								<th class="text-center">Abbreviation</th>
								<th class="text-center">Description</th>
								<th class="text-center">Codes</th>
							</tr>
							</thead>
							<tbody id="main-body">
							</tbody>
						</table>
					</div>
				</div>
				<!-- /Code type table. -->

			</div>
		</div>
		<!-- /Code type list row -->
	</div>
</div>

<!-- Load the dialog views. -->
<?php $this->partial("views/code/dialog/add_code_type.php"); ?>
<?php $this->partial("views/code/dialog/delete_code_type.php"); ?>

<?php $this->partial("views/code/dialog/add_code.php"); ?>
<?php $this->partial("views/code/dialog/delete_code.php"); ?>
