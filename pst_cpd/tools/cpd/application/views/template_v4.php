<!DOCTYPE html>
<html lang="en">
<head>
	<title>
		<?php if (isset($page_title)) echo $page_title; else echo "Roth Methods Toolpadz"; ?>
	</title>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<!-- Bootstrap Stylesheet. -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css"
	      integrity="sha384-AysaV+vQoT3kOAXZkl02PThvDr8HYKPZhNT5h/CXfBThSRXQ6jW5DO2ekP5ViFdi" crossorigin="anonymous"/>

	<!-- JQuery -->
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"
	        integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>

	<!-- Tether - required by bootstrap. -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/css/tether.min.css"
	      integrity="sha256-y4TDcAD4/j5o4keZvggf698Cr9Oc7JZ+gGMax23qmVA=" crossorigin="anonymous"/>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.7/js/tether.min.js"
	        integrity="sha256-/5pHDZh2fv1eZImyfiThtB5Ag4LqDjyittT7fLjdT/8=" crossorigin="anonymous"></script>

	<!-- Bootstrap. -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/js/bootstrap.min.js"
	        integrity="sha384-BLiI7JTZm+JWlgKa0M0kGRpJbF2J8q+qreVrKBC47e3K6BW78kGLrCkeRX6I9RoK"
	        crossorigin="anonymous"></script>

	<!-- Font awesome and social bottons. -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	/>
	<link rel="stylesheet" href="/css/bootstrap-social.css"/>

	<!-- NEED FORM VALIDATION -->
	<!--
	<link rel="stylesheet" href="/css/bootstrap/dataTables.bootstrap.css" />

	<link rel="stylesheet" href="/css/jquery/jquery.datetimepicker.min.css" />
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" />
	<link rel="stylesheet" href="/css/select2/select2.min.css" />
	<link rel="stylesheet" href="/css/styles.css" />
	-->

	<?php if (isset($css_files)) {
		foreach ($css_files as $css_file) { ?>
			<link rel="stylesheet" href="/css/<?php echo $css_file; ?>"/>
		<?php }
	} ?>

	<?php if (file_exists(FCPATH . "/css/application/" . $main . ".css")) { ?>
		<link rel="stylesheet" href="/css/application/<?php echo $main; ?>.css"/>
	<?php } ?>

	<!--
	<script src="/js/jquery/formValidation.min.js"></script>

	<script src="//cdn.datatables.net/1.10.4/js/jquery.dataTables.min.js"></script>
	<script src="//cdn.datatables.net/plug-ins/3cfcc339e89/integration/bootstrap/3/dataTables.bootstrap.js"></script>
	<script src="/js/moment/moment.js"></script>
	<script src="//cdn.datatables.net/plug-ins/1.10.11/sorting/datetime-moment.js"></script>
	<script src="/js/jquery/jquery.datetimepicker.full.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
	<script src="/js/jquery/hashtable.js"></script>
	<script src="/js/jquery/jquery.numberformatter.js"></script>
	<script src="/js/jquery/jquery-sortable.js"></script>
	<script src="/js/libs/jquery-tokeninput/jquery.tokeninput.js"></script>
	<script src="/js/photoswipe/photoswipe.min.js"></script>
	<script src="/js/photoswipe/photoswipe-ui-default.min.js"></script>
	<script src="/js/select2/select2.full.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug 
	<script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8NUObZhz4Od4CVJXdcMHSUbMSJEI_bvo&libraries=places"></script>
	<script src="/js/jquery/jquery.geocomplete.js"></script>
	<script src="/js/maps/marker.with.label.js"></script>
	-->
</head>

<body>

<!-- Main navigation -->
<nav class="navbar navbar-fixed-top navbar-dark bg-inverse">
	<div class="container">
		<a class="navbar-brand" href="/">Roth Methods Toolpadz</a>

		<!-- Main Menu -->
		<button class="navbar-toggler hidden-lg-up" type="botton" data-toggle="collapse"
		        data-target="#navbar-responsive" area-controls="#navbar-responsive" area-expanded="false"
		        aria-label="Toggle Navigation"></button>
		<div class="collapse navbar-toggleable-md" id="navbar-responsive">
			<ul class="nav navbar-nav float-lg-right">
				<li class="nav-item active">
					<a class="nav-link" href="/dashboard">Dashboard <span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="http://example.com" id="responsiveNavbarDropdown"
					   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Dropdown</a>
					<div class="dropdown-menu" aria-labelledby="responsiveNavbarDropdown">
						<a class="dropdown-item" href="#">Action</a>
						<a class="dropdown-item" href="#">Another action</a>
						<a class="dropdown-item" href="#">Something else here</a>
					</div>
				</li>
			</ul>
		</div>
		<!-- //Main Menu -->

	</div>
</nav>
<!-- //Main navigation -->

<!-- Application -->
<div class="container-fluid">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<?php echo $this->yieldView(); ?>
		</div>
	</div>
</div>
<!-- //Application -->

<!-- Footer -->
<div class="container-fluid" id="footer">
	<div class="row">
		<div class="col-xs-12">
			<p class="navbar-text">Copyright (c) 2014-201g Cardinal Technologies, LLC. All rights reserved.</p>
		</div>
	</div>
</div>
<!-- //Footer -->

</body>
</html>

