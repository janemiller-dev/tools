<?php

use PHPUnit\Framework\TestCase;

class CpdTest extends TestCase
{

//    public function testGetCpd()
//    {
//        $app = new \Application\Application();
//        $obj = new \Controller\Cpd($app);
//        $model = new \Model\CPD();
//
//        $this->assertNotEmpty($model->get_cpd(1, 6, 2));
//    }

    /** @test */
//    public function addCPD()
//    {
//        $app = new \Application\Application();
////        $obj = new \Controller\Cpd($app);
//        $model = new \Model\CPD();
//
//        $input['cpd_year'] = 2019;
//        $input['cpd_industry_id'] = 8;
//        $input['cpd_profession_id'] = 2;
//        $input['cpd_tgd_id'] = 193;
//        $input['name'] = 'cpd_php_unit';
//        $input['description'] = 'Cpd using php unit';
//
//
//        $this->assertEquals($model->add_cpd(1, $input),22);
//    }

//    /** @test */
//    public function deletCPD()
//    {
//        $app = new \Application\Application();
//        $model = new \Model\CPD();
//
//        $this->assertTrue($model->delete_cpd(22), true);
//
//    }

    /** @test */
//    public function searchCPD()
//    {
//        $app = new \Application\Application();
//        $model = new \Model\CPD();
//
//        $input = (object) array('draw' => 1, 'start' => 0, 'length' => 50);
//        $this->assertTrue($model->search_cpd_instances(1, 1, $input), true);
//
//    }

//    /** @test */
//    public function moveDeal()
//    {
//        $app = new \Application\Application();
//        $model = new \Model\CPD();
//
//        $array['new_status'] = 'complete';
//        $array['new_move_date'] = 5;
//        $array['id'] =14;
//        $array['current_status'] = 'maybe';
//        $data =  (object)array('deal_status_id' => 122);
//        $data =  (object)array('status' => 'success');
//        $this->assertEquals($model->move_deal($array),$data);
//    }

//    /** @test */
//    public function deleteDeal()
//    {
//        $app = new \Application\Application();
//        $model = new \Model\CPD();
//
//        $this->assertTrue($model->delete_deal(14),true);
//    }

//    /** @test */
//    public function updateLead()
//    {
//        $app = new \Application\Application();
//        $model = new \Model\CPD();
//
//        $this->assertTrue($model->update_lead(13,'Test'),true);
//    }

//    /** @test */
//    public function updateGen()
//    {
//        $app = new \Application\Application();
//        $model = new \Model\CPD();
//
//        $this->assertTrue($model->update_gen(25,'test'),true);
//    }

    /** @test */
    public function getFTG()
    {
        $app = new \Application\Application();
        $model = new \Model\CPD();

        $data[] =  (object) array('upo_floor' => 1000, 'upo_target' => 10000, 'upo_game' => 100000);

        $this->assertEquals($model->get_ftg(1, 2, 2018), $data);
        $pdo = $model->get_ftg(1, 2, 2018);
        foreach ($pdo as $item)
        $this->assertTrue(is_object($item));
    }

//    /** @test */
//    public function addDeal()
//    {
//        $app = new \Application\Application();
//        $model = new \Model\CPD();
//
//        $deal['name'] = 'PHPunit';
//        $deal['cpd_id'] = 6;
//        $deal['user_id'] = 1;
//        $deal['status'] = 'expl_per';
//        $deal['move_date'] = 5;
//        $deal['clu'] = 'C';
//        $deal['rwt'] = 'R';
//        $deal['lead'] = '';
//        $deal['gen'] = '';
//        $deal['type'] = '100U';
//        $deal['amount'] = 1000;
//
//        $this->assertEquals($model->add_deal($deal), 26 );
//    }

//    /** @test */
//    public function addUpdate()
//    {
//        $app = new \Application\Application();
//        $model = new \Model\CPD();
//
//        $update['name'] = 'Phpunit';
//        $update['update_deal_id'] = 26;
//        $update['datetime'] = '';
//
//        $this->assertEquals($model->add_update($update), 13);
//    }

//    /** @test */
//    public function deleteUpdate()
//    {
//        $app = new \Application\Application();
//        $model = new \Model\CPD();
//
//        $this->assertTrue($model->delete_update(12),true);
//    }

//    /** @test */
//    public function addPromo()
//    {
//        $app = new \Application\Application();
//        $model = new \Model\CPD();
//
//        $promo['name'] = 'Phpunit';
//        $promo['promo_deal_id'] = 26;
//        $promo['datetime'] = '';
//
//        $this->assertEquals($model->add_promo($promo), 6);
//    }

//    /** @test */
//    public function deletePromo()
//    {
//        $app = new \Application\Application();
//        $model = new \Model\CPD();
//
//        $this->assertTrue($model->delete_promo(6),true);
//    }

//    /** @test */
//    public function updateDealInfo()
//    {
//        $app = new \Application\Application();
//        $model = new \Model\CPD();
//
//        $this->assertTrue($model->update_deal_table_info('cpd_complete_desc', 6, 'PHPunit'), true);
//    }

    /** @test  */
    public function getUpdates()
    {
        $app = new \Application\Application();
        $model = new \Model\CPD();

        $output[] = (object) array('du_datetime' => "2018-05-24 15:08:10", 'du_deal_id' => "26", 'du_id' =>"13", 'du_name' =>"Phpunit");
        $this->assertEquals($model->get_updates(26), $output);
        $this->assertTrue(is_array($model->get_updates(26)));
    }

    /** @test  */
    public function getPromos()
    {
        $app = new \Application\Application();
        $model = new \Model\CPD();

        $output[] = (object) array('dp_datetime' => "2018-05-24 15:33:15", 'dp_deal_id' => "26", 'dp_id' =>"7", 'dp_name' =>"Promo");
        $this->assertEquals($model->get_promos(26), $output);
        $pdo = $model->get_promos(26);
        $this->assertTrue(is_array($pdo));
        foreach ($pdo as $item)
        $this->assertTrue(is_object($item));
    }

    /** @test */
    public function deleteCPD()
    {
        $app = new \Application\Application();
        $model = new \Model\CPD();

        $this->assertFalse($model->delete_cpd(8));
        $this->assertTrue(is_bool($model->delete_cpd(8)));
    }

    /** @test */
    public function getCPD()
    {
        $app = new \Application\Application();
        $model = new \Model\CPD();

        $this->assertNotEmpty($model->get_cpd(1, 17, 2));
        $this->assertTrue(is_object($model->get_cpd(1, 17, 2)));
    }


}