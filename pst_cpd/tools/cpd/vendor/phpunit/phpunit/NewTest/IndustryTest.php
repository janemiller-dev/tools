<?php
/**
 * Created by PhpStorm.
 * User: sumit
 * Date: 28/5/18
 * Time: 1:33 PM
 */

use \PHPUnit\Framework\TestCase;

class IndustryTest extends TestCase
{
    /** @test */
    public function getActiveProfession()
    {
        $model = new \Model\Industry();

        $this->assertEquals($model->get_active_profession(1), 5);
        $this->assertTrue(is_numeric($model->get_active_profession(1)));
//        $this->assertNan($model->get_active_profession(1));
    }

    /** @test */
    public function getMyProfession()
    {
        $model = new \Model\Industry();

        $this->assertTrue(is_array($model->get_my_professions(1)));
        $this->assertNotEmpty($model->get_my_professions(1));
    }

    /** @test */
    public function getUnusedProfession()
    {
        $model = new \Model\Industry();
        $data = array();
        $this->assertEquals($model->get_unused_professions(9, 1), $data);
        $this->assertTrue(is_array($model->get_unused_professions(9,1)));
    }

    /** @test */
    public function getIndustry()
    {
        $model = new \Model\Industry();
        $data = (object)array('industry_id'=>9, 'industry_name'=>'industry1', 'industry_description'=>'Some text', 'profession'=>array());

        $this->assertEquals($model->get_industry(9), $data);
        $this->assertTrue(is_object($model->get_industry(9)));
    }

    /** @test */
    public function getIndustries()
    {
        $model = new \Model\Industry();

        $this->assertTrue(is_array($model->get_industries()));
    }

    /** @test */
//    public function addProfession()
//    {
//        $model = new \Model\Industry();
//
//        $this->assertEquals($model->add_profession(8, 'phpunit', 'phpunit'), 7);
//    }

    /** @test */
    public function deleteProfession()
    {
        $model = new \Model\Industry();

//        $this->ExpectException(Exception::class);
        $this->assertEquals($model->delete_profession(7), null);
    }
}