<?php

use PHPUnit\Framework\TestCase;

class ObjectiveTest extends TestCase
{
    public function testGetUsedYears()
    {
//        $model = new \Model\Objective();
//        $app = new \Application\Application();
//        $obj = new \Controller\Objective($app);
        $model = new \Model\Objective();

//        $obj->getObjective('Sumit');

        $array[] =  (object) array('upo_year' => 2018);
        $array[] =  (object) array('upo_year' => 2019);
        $array[] =  (object) array('upo_year' => 2020);


        $this->assertEquals($model->get_used_years(1,1),$array);
    }

    public function testGetObjective()
    {
//        $model = new \Model\Objective();
//        $app = new \Application\Application();
//        $obj = new \Controller\Objective($app);
        $model = new \Model\Objective();

//        $obj->getObjective('Sumit');
        $data[] =  (object) array('year' => 2018, 'floor' => 10000, "target" => 2000, "game" =>3000);
        $data[] =  (object) array('year' => 2019 , "floor" => 1000, "target" => 2000000, "game" => 3000);
        $data[] =  (object) array('year' => 2020, "floor" => 2000, "target" => 2000, "game" => 3000);
        $array['recordsTotal'] = 17;
        $array['recordsFiltered'] = 3;
        $array['data'] = $data ;

        $this->assertEquals($model->get_objective(1,1),$array);
    }

//    public function testAddObjective()
//    {
//        $app = new \Application\Application();
//        $obj = new \Controller\Objective($app);
//        $model = new \Model\Objective();
//
//        $input['user_id'] = 1;
//        $input['profession_id'] = 2;
//        $input['year'] = 2019;
//        $input['floor'] = 2000;
//        $input['target'] = 2000;
//        $input['game'] = 2000;
//
//        $this->assertNotEquals($model->add_objective($input));
//        $this->expectException();
//    }

    /** @test */
    public function updateObjective() {
//        $app = new \Application\Application();
//        $obj = new \Controller\Objective($app);
        $model = new \Model\Objective();

        $this->assertFalse($model->update_objective(1,3,'upo_floor',2018,10),true);
    }
}